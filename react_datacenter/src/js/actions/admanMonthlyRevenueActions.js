export function fetchAdmanMonthlyRevenue() {
    return function(dispatch) {   
        
        setTimeout(function(){                        
            dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE', payload: {}});
                    
            var adman_month_revenue=0;
            var adman_revenue_last_month=0;
            var adman_revenue_last_month_day=0;
            var nowDate = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
            var now=parseInt(new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000).toISOString().replace(/-/g,''));
            var startOfMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth(),1).toISOString().replace(/-/g,''));
            var startOfLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth()-1,1).toISOString().replace(/-/g,''));
            var nowLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth()-1,nowDate.getDate()).toISOString().replace(/-/g,''));
            var endOfLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth(),0).toISOString().replace(/-/g,''));
            //console.log(now,startOfMonth,startOfLastMonth,endOfLastMonth,nowLastMonth);

            var adman_month_revenue_promises = [];
            for (var i = startOfMonth; i < now; i++) {
                adman_month_revenue_promises.push(
                    $.ajax({
                        url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+i+'000000&pmos=&products=&countries=&devices=&pmbs=',
                        type: 'get',
                        async: true,
                        success: function(response){
                            if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                                adman_month_revenue += response.totals.amounts.impression_USD/1000;
                            }else{
                                dispatch({
                                    type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                                    payload: 'Could not load Adman month revenue'
                                 });
                            }
                        },
                        error: function(){
                            dispatch({
                                type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                                payload:  'Could not load Adman revenue'
                             });
                        }
                    })
                );
            }
            $.when.apply($, adman_month_revenue_promises).then(function() {
                dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE', 
                    payload: {
                        admanMonthlyRevenue: adman_month_revenue
                    }
                 });
            }, function() {
                dispatch({
                    type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                    payload: 'Could not load Adman month revenue'
                 });
            });

            var adman_last_month_revenue_promises = [];
            for(var j=startOfLastMonth;j<=endOfLastMonth;j++){
                adman_last_month_revenue_promises.push(
                    $.ajax({
                        url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+j+'000000&pmos=&products=&countries=&devices=&pmbs=',
                        type: 'get',
                        async: true,
                        success: function(response){
                            if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                                adman_revenue_last_month += response.totals.amounts.impression_USD/1000;
                            }else{
                                dispatch({
                                    type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                                    payload: 'Could not load Adman month revenue'
                                 });
                            }
                        },
                        error: function(){
                            dispatch({
                                type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                                payload: {error: 'Could not load Adman last month revenue'}
                             });
                        }
                    })
                );
            }
            $.when.apply($, adman_last_month_revenue_promises).then(function() {
                compareAdmanRevenues();
            }, function() {
                console.log('failed to get last month\'s adman revenue');
            });

            function compareAdmanRevenues(){
                var adman_last_month_day_revenue_promises = [];
                for(var i=startOfLastMonth;i<nowLastMonth;i++){
                    adman_last_month_day_revenue_promises.push(
                        $.ajax({
                            url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+i+'000000&pmos=&products=&countries=&devices=&pmbs=',
                            type: 'get',
                            async: true,
                            success: function(response){
                                if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                                    adman_revenue_last_month_day += response.totals.amounts.impression_USD/1000;
                                }else{
                                    dispatch({
                                        type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                                        payload: {error: 'Could not load Adman last month revenue'}
                                     });
                                }
                            },
                            error: function(){
                                dispatch({
                                    type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                                    payload: {error: 'Could not load Adman last month revenue'}
                                 });
                            }
                        })
                    );
                }
                $.when.apply($, adman_last_month_day_revenue_promises).then(function() {
                    if(adman_month_revenue-adman_revenue_last_month_day > 0){
                        dispatch({type: 'FETCH_ADMAN_LAST_MONTHLY_REVENUE', 
                            payload: {
                                admanRevenueLastMonth: adman_revenue_last_month_day,
                                admanRevenueLastMonthTotal: adman_revenue_last_month,
                                increase: true,
                                badge: "badge badge-success pull-left m-t-20",
                                trend: "zmdi zmdi-trending-up",
                                change: ((adman_month_revenue/adman_revenue_last_month_day-1)*100).toFixed(1)
                            }
                         });
                    }else{
                        dispatch({type: 'FETCH_ADMAN_LAST_MONTHLY_REVENUE', 
                            payload: {
                                admanRevenueLastMonth: adman_revenue_last_month_day,
                                admanRevenueLastMonthTotal: adman_revenue_last_month,
                                increase: false,
                                badge: "badge badge-danger pull-left m-t-20",
                                trend: "zmdi zmdi-trending-down",
                                change: ((1-adman_month_revenue/adman_revenue_last_month_day)*100).toFixed(1)
                            }
                         });
                    }
                }, function() {
                    dispatch({
                        type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', 
                        payload: {error: 'Could not load Adman last month revenue'}
                     });
                });
            }
        },1000*10);
        
        setInterval(function(){                        
            dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE', payload: {}});
                    
            var revenue=0, yesterday_revenue=0;
            
            var today=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000-24*60*60*1000);
            var yesterday=new Date(today.getTime()-24*60*60*1000);
            //console.log(today,yesterday);
            $.ajax({
                url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+today.toISOString().substr(0,10).replace(/-/g,'')+'000000&pmos=&products=&countries=&devices=&pmbs=',
                type: 'get',
                success: function(response){
                    if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                        revenue = response.totals.amounts.impression_USD/1000;
                        $.ajax({
                            url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+yesterday.toISOString().substr(0,10).replace(/-/g,'')+'000000&pmos=&products=&countries=&devices=&pmbs=',
                            type: 'get',
                            success: function(response){
                                if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                                    yesterday_revenue = response.totals.amounts.impression_USD/1000;
                                    if(revenue > yesterday_revenue){
                                        dispatch({
                                            type: 'FETCH_ADMAN_MONTHLY_REVENUE_FULFILLED', 
                                            payload: {
                                                increase: true, 
                                                yesterdaysRevenue: yesterday_revenue, 
                                                todaysRevenue: revenue,
                                                trend: 'zmdi zmdi-trending-up', 
                                                badge: 'badge badge-success pull-left m-t-20', 
                                                change: ((revenue/yesterday_revenue-1)*100).toFixed(1)
                                            }
                                        });   
                                    }else{
                                        dispatch({
                                            type: 'FETCH_ADMAN_MONTHLY_REVENUE_FULFILLED', 
                                            payload: {
                                                increase: false, 
                                                yesterdaysRevenue: yesterday_revenue, 
                                                todaysRevenue: revenue,
                                                trend: 'zmdi zmdi-trending-down', 
                                                badge: 'badge badge-danger pull-left m-t-20', 
                                                change: ((1-revenue/yesterday_revenue)*100).toFixed(1)
                                            }
                                        });                    
                                    }
                                }else{
                                    dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', payload: response.errors});
                                }
                            },
                            error: function(response){
                                dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', payload: response.responseText});
                            }
                        });
                    }else{
                        dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', payload: response.errors});
                    }
                },
                error: function(response){
                    dispatch({type: 'FETCH_ADMAN_MONTHLY_REVENUE_ERROR', payload: response.responseText});
                }
            });
        },1000*60*30);
    }
}