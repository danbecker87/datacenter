export function fetchFBDailyRevenue() {
    return function(dispatch) {    
        setInterval(function(){                        
            dispatch({type: 'FETCH_FB_DAILY_REVENUE', payload: {}});
                    
            var revenue=0, yesterday_revenue=0;
	       
            FB.api(
                '/980493918670682/app_insights/app_event/',
                'GET',
                {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":'tomorrow',"until":'tomorrow', access_token: FB.getAccessToken()},
                function(response) {
                    if(response.error){
                        dispatch({type: 'FETCH_FB_DAILY_REVENUE_ERROR', payload: response.error.message});
                    }else{
                        for(var i=0;i<response.data.length;i++){
                            revenue+=parseFloat(response.data[i].value);
                        }
                        FB.api(
                            '/980493918670682/app_insights/app_event/',
                            'GET',
                            {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":'today',"until":'today', access_token: FB.getAccessToken()},
                            function(response) {
                                if(response.error){
                                    dispatch({type: 'FETCH_FB_DAILY_REVENUE_ERROR', payload: response.error.message});
                                }else{
                                    for(var i=0;i<response.data.length;i++){
                                        yesterday_revenue+=parseFloat(response.data[i].value);
                                    }
                                    dispatch({type: 'FETCH_FB_DAILY_REVENUE_FULFILLED', payload: {fbRevenueToday: revenue, fbRevenueYesterday: yesterday_revenue}});
                                }
                            }
                        );
                    }
                }
            );
        },1000*10);
    }
}