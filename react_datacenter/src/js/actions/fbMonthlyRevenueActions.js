export function fetchFBMonthlyRevenue() {
    return function(dispatch) {    
        
        setTimeout(function(){
            dispatch({type: 'FETCH_FB_MONTHLY_REVENUE', payload: {}});

            var FBrevenue=0;
            var last_month_FBrevenue=0;
            var last_month_total_FBrevenue=0;
            var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
            var start = new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+'02';
            var last_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+1)+'-'+'02';
            var this_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+2)+'-'+'01';
            var last_month_day = new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000+24*60*60*1000);
            FB.api(
                '/980493918670682/app_insights/app_event/',
                'GET',
                {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":start},
                function(response) {
                    if(response.error){
                        dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_ERROR', payload: response.error.message});
                    }else{
                        for(var i=0;i<response.data.length;i++){
                            FBrevenue+=parseFloat(response.data[i].value);
                        }
                        //console.log(FBrevenue);                        
                        FB.api(
                            '/980493918670682/app_insights/app_event/',
                            'GET',
                            {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': last_month_day.toISOString().substr(0,10)},
                            function(response) {
                                if(response.error){
                                    dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_ERROR', payload: response.error.message});
                                }else{
                                    for(var i=0;i<response.data.length;i++){
                                        last_month_FBrevenue+=parseFloat(response.data[i].value);
                                    }
                                    FB.api(
                                        '/980493918670682/app_insights/app_event/',
                                        'GET',
                                        {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': this_month_start},
                                        function(response) {
                                            if(response.error){
                                                dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_ERROR', payload: response.error.message});
                                            }else{
                                                for(var i=0;i<response.data.length;i++){
                                                    last_month_total_FBrevenue+=parseFloat(response.data[i].value);
                                                }
                                                if(FBrevenue-last_month_FBrevenue > 0){
                                                    dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_FULFILLED', 
                                                        payload: {
                                                            fbMonthlyRevenue: FBrevenue, 
                                                            fbRevenueLastMonth: last_month_FBrevenue, 
                                                            fbReveneuLastMonthTotal: last_month_total_FBrevenue,
                                                            trend: 'zmdi zmdi-trending-up', 
                                                            badge: 'badge badge-success pull-left m-t-20', 
                                                            change: ((FBrevenue/last_month_FBrevenue-1)*100).toFixed(1)
                                                          }
                                                     });
                                                }else{
                                                    dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_FULFILLED', 
                                                        payload: {
                                                            fbMonthlyRevenue: FBrevenue, 
                                                            fbRevenueLastMonth: last_month_FBrevenue, 
                                                            fbRevenueLastMonthTotal: last_month_total_FBrevenue,
                                                            trend: 'zmdi zmdi-trending-down', 
                                                            badge: 'badge badge-danger pull-left m-t-20', 
                                                            change: ((1-FBrevenue/last_month_FBrevenue)*100).toFixed(1)
                                                          }
                                                     });
                                                }
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            );
        },10*1000);
        
        setInterval(function(){                        
            dispatch({type: 'FETCH_FB_MONTHLY_REVENUE', payload: {}});
            
            var FBrevenue=0;
            var last_month_FBrevenue=0;
            var last_month_total_FBrevenue=0;
            var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
            var start = new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+'02';
            var last_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+1)+'-'+'02';
            var this_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+2)+'-'+'01';
            var last_month_day = new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000+24*60*60*1000);
            FB.api(
                '/980493918670682/app_insights/app_event/',
                'GET',
                {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":start},
                function(response) {
                    if(response.error){
                        dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_ERROR', payload: response.error.message});
                    }else{
                        for(var i=0;i<response.data.length;i++){
                            FBrevenue+=parseFloat(response.data[i].value);
                        }
                        //console.log(FBrevenue);                        
                        FB.api(
                            '/980493918670682/app_insights/app_event/',
                            'GET',
                            {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': last_month_day.toISOString().substr(0,10)},
                            function(response) {
                                if(response.error){
                                    dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_ERROR', payload: response.error.message});
                                }else{
                                    for(var i=0;i<response.data.length;i++){
                                        last_month_FBrevenue+=parseFloat(response.data[i].value);
                                    }
                                    FB.api(
                                        '/980493918670682/app_insights/app_event/',
                                        'GET',
                                        {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': this_month_start},
                                        function(response) {
                                            if(response.error){
                                                dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_ERROR', payload: response.error.message});
                                            }else{
                                                for(var i=0;i<response.data.length;i++){
                                                    last_month_total_FBrevenue+=parseFloat(response.data[i].value);
                                                }
                                                if(FBrevenue-last_month_FBrevenue > 0){
                                                    dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_FULFILLED', 
                                                        payload: {
                                                            fbMonthlyRevenue: FBRevenue, 
                                                            fbRevenueLastMonth: last_month_FBrevenue, 
                                                            fbReveneuLastMonthTotal: last_month_total_FBrevenue,
                                                            trend: 'zmdi zmdi-trending-up', 
                                                            badge: 'badge badge-success pull-left m-t-20', 
                                                            change: ((FBrevenue/last_month_FBrevenue-1)*100).toFixed(1)
                                                          }
                                                     });
                                                }else{
                                                    dispatch({type: 'FETCH_FB_MONTHLY_REVENUE_FULFILLED', 
                                                        payload: {
                                                            fbRevenue: FBRevenue, 
                                                            fbRevenueLastMonth: last_month_FBrevenue, 
                                                            fbReveneuLastMonthTotal: last_month_total_FBrevenue,
                                                            trend: 'zmdi zmdi-trending-down', 
                                                            badge: 'badge badge-danger pull-left m-t-20', 
                                                            change: ((1-FBrevenue/last_month_FBrevenue)*100).toFixed(1)
                                                          }
                                                     });
                                                }
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            );	
        },1000*60*30);
    }
}