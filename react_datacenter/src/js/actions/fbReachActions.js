export function fetchFBReach() {
    return function(dispatch) {    
        setTimeout(function(){                        
            dispatch({type: 'FETCH_FB_REACH', payload: {}});
                    
            var reach_today, reach_yesterday;
            
            FB.api(
              '/eldeforma/insights',
              'GET',
              {"fields":"values","since":"yesterday","until":"tomorrow+1days","metric":"['page_impressions_unique']","period":"day"},
                function(response) {
                    if(response.error){
                        dispatch({type: 'FETCH_FB_REACH_ERROR', payload: response.error.message});
                    }else{
                        reach_today = response.data[0].values[1].value;
                        reach_yesterday = response.data[0].values[0].value;
                        if(reach_today-reach_yesterday > 0){
                            dispatch({
                                type: 'FETCH_FB_REACH_FULFILLED', 
                                payload: {
                                    increase: true, 
                                    fbReach: reach_today,
                                    fbReachYesterday: reach_yesterday, 
                                    trend: 'zmdi zmdi-trending-up', 
                                    badge: 'badge badge-success pull-left m-t-20', 
                                    change: ((reach_today/reach_yesterday-1)*100).toFixed(1)
                                }
                            });
                        }else{
                            dispatch({
                                type: 'FETCH_FB_REACH_FULFILLED', 
                                payload: {
                                    increase: false, 
                                    fbReachYesterday: reach_yesterday, 
                                    fbReach: reach_today,
                                    trend: 'zmdi zmdi-trending-down', 
                                    badge: 'badge badge-danger pull-left m-t-20', 
                                    change: ((1-reach_today/reach_yesterday)*100).toFixed(1)
                                }
                            });
                        }
                    }
                }
            );
        },1000*10);
        
        setInterval(function(){                        
            dispatch({type: 'FETCH_FB_REACH', payload: {}});
                    
            var reach_today, reach_yesterday;
            
            FB.api(
              '/eldeforma/insights',
              'GET',
              {"fields":"values","since":"yesterday","until":"tomorrow+1days","metric":"['page_impressions_unique']","period":"day"},
                function(response) {
                    if(response.error){
                        dispatch({type: 'FETCH_FB_REACH_ERROR', payload: response.error.message});
                    }else{
                        reach_today = response.data[0].values[1].value;
                        reach_yesterday = response.data[0].values[0].value;
                        if(reach_today-reach_yesterday > 0){
                            dispatch({
                                type: 'FETCH_FB_REACH_FULFILLED', 
                                payload: {
                                    increase: true, 
                                    fbReach: reach_today,
                                    fbReachYesterday: reach_yesterday, 
                                    trend: 'zmdi zmdi-trending-up', 
                                    badge: 'badge badge-success pull-left m-t-20', 
                                    change: ((reach_today/reach_yesterday-1)*100).toFixed(1)
                                }
                            });
                        }else{
                            dispatch({
                                type: 'FETCH_FB_REACH_FULFILLED', 
                                payload: {
                                    increase: false, 
                                    fbReachYesterday: reach_yesterday, 
                                    fbReach: reach_today,
                                    trend: 'zmdi zmdi-trending-down', 
                                    badge: 'badge badge-danger pull-left m-t-20', 
                                    change: ((1-reach_today/reach_yesterday)*100).toFixed(1)
                                }
                            });
                        }
                    }
                }
            );
        },1000*60*30);
    }
}