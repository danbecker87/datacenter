export function fetchActiveUsers() {
    return function(dispatch) {
        
        setInterval(function(){
            gapi.client.load('analytics','v3', function(){
                var requestUsers = gapi.client.analytics.data.realtime.get({
                    ids: "ga:41142760",
                    metrics: "rt:activeUsers",
                    dimensions: "rt:pagePath",
                    "max-results": 10,
                    sort: "-rt:activeUsers",
                    fields: "rows,totalsForAllResults",
                    output: 'json'
                });
                requestUsers.then(function(resp){
                    dispatch({type: 'FETCH_ACTIVE_USERS_FULFILLED', payload: {activeUsers: parseInt(resp.result['totalsForAllResults']['rt:activeUsers']), topPages: resp.result['rows']}});
                }, function(reason){
                    dispatch({type: 'FETCH_ACTIVE_USERS_ERROR', payload: reason.result.error.message});
                });
                
                var requestUsersOrganic = gapi.client.analytics.data.realtime.get({
                    ids: "ga:41142760",
                    metrics: "rt:activeUsers",
                    dimensions: "rt:pagePath",
                    "max-results": 10,
                    filters: "rt:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@SRE=1",
                    sort: "-rt:activeUsers",
                    fields: "rows,totalsForAllResults",
                    output: 'json'
                });
                requestUsersOrganic.then(function(resp){
                    dispatch({type: 'FETCH_ACTIVE_USERS_ORGANIC_FULFILLED', payload: {activeUsersOrganic: parseInt(resp.result['totalsForAllResults']['rt:activeUsers']), topPagesOrganic: resp.result['rows']}});
                }, function(reason){
                    dispatch({type: 'FETCH_ACTIVE_USERS_ORGANIC_ERROR', payload: reason.result.error.message});
                });
                
                var requestUsersPaid = gapi.client.analytics.data.realtime.get({
                    ids: "ga:41142760",
                    metrics: "rt:activeUsers",
                    dimensions: "rt:pagePath",
                    "max-results": 10,
                    filters: "rt:pagePath=@p1=true,ga:pagePath=@GUA=1,ga:pagePath=@SRE=1",
                    sort: "-rt:activeUsers",
                    fields: "rows,totalsForAllResults",
                    output: 'json'
                });
                requestUsersPaid.then(function(resp){
                    dispatch({type: 'FETCH_ACTIVE_USERS_PAID_FULFILLED', payload: {activeUsersPaid: parseInt(resp.result['totalsForAllResults']['rt:activeUsers']), topPagesPaid: resp.result['rows']}});
                }, function(reason){
                    dispatch({type: 'FETCH_ACTIVE_USERS_PAID_ERROR', payload: reason.result.error.message});
                });
            });
        },1000*10);
    }
}