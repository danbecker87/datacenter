export function fetchAdmanDailyRevenue() {
    return function(dispatch) {   
        
        setTimeout(function(){                        
            dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE', payload: {}});
                    
            var revenue=0, yesterday_revenue=0;
            
            var today=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000-24*60*60*1000);
            var yesterday=new Date(today.getTime()-24*60*60*1000);
            //console.log(today,yesterday);
            $.ajax({
                url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+today.toISOString().substr(0,10).replace(/-/g,'')+'000000&pmos=&products=&countries=&devices=&pmbs=',
                type: 'get',
                success: function(response){
                    if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                        revenue = response.totals.amounts.impression_USD/1000;
                        $.ajax({
                            url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+yesterday.toISOString().substr(0,10).replace(/-/g,'')+'000000&pmos=&products=&countries=&devices=&pmbs=',
                            type: 'get',
                            success: function(response){
                                if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                                    yesterday_revenue = response.totals.amounts.impression_USD/1000;
                                    if(revenue > yesterday_revenue){
                                        dispatch({
                                            type: 'FETCH_ADMAN_DAILY_REVENUE_FULFILLED', 
                                            payload: {
                                                increase: true, 
                                                yesterdaysRevenue: yesterday_revenue, 
                                                todaysRevenue: revenue,
                                                trend: 'zmdi zmdi-trending-up', 
                                                badge: 'badge badge-success pull-left m-t-20', 
                                                change: ((revenue/yesterday_revenue-1)*100).toFixed(1)
                                            }
                                        });   
                                    }else{
                                        dispatch({
                                            type: 'FETCH_ADMAN_DAILY_REVENUE_FULFILLED', 
                                            payload: {
                                                increase: false, 
                                                yesterdaysRevenue: yesterday_revenue, 
                                                todaysRevenue: revenue,
                                                trend: 'zmdi zmdi-trending-down', 
                                                badge: 'badge badge-danger pull-left m-t-20', 
                                                change: ((1-revenue/yesterday_revenue)*100).toFixed(1)
                                            }
                                        });                    
                                    }
                                }else{
                                    dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.errors});
                                }
                            },
                            error: function(response){
                                dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.responseText});
                            }
                        });
                    }else{
                        dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.errors});
                    }
                },
                error: function(response){
                    dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.responseText});
                }
            });
        },1000*10);
        
        setInterval(function(){                        
            dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE', payload: {}});
                    
            var revenue=0, yesterday_revenue=0;
            
            var today=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000-24*60*60*1000);
            var yesterday=new Date(today.getTime()-24*60*60*1000);
            //console.log(today,yesterday);
            $.ajax({
                url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+today.toISOString().substr(0,10).replace(/-/g,'')+'000000&pmos=&products=&countries=&devices=&pmbs=',
                type: 'get',
                success: function(response){
                    if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                        revenue = response.totals.amounts.impression_USD/1000;
                        $.ajax({
                            url: 'http://irvine.admanmedia.com/api/v1/public_charlies?pmus=1dedb463&signature=monikakogonzalezz&tag_from='+yesterday.toISOString().substr(0,10).replace(/-/g,'')+'000000&pmos=&products=&countries=&devices=&pmbs=',
                            type: 'get',
                            success: function(response){
                                if(!typeof response.errors !== 'undefined' && typeof response.totals.amounts.impression_USD !== 'undefined'){
                                    yesterday_revenue = response.totals.amounts.impression_USD/1000;
                                    if(revenue > yesterday_revenue){
                                        dispatch({
                                            type: 'FETCH_ADMAN_DAILY_REVENUE_FULFILLED', 
                                            payload: {
                                                increase: true, 
                                                yesterdaysRevenue: yesterday_revenue, 
                                                todaysRevenue: revenue,
                                                trend: 'zmdi zmdi-trending-up', 
                                                badge: 'badge badge-success pull-left m-t-20', 
                                                change: ((revenue/yesterday_revenue-1)*100).toFixed(1)
                                            }
                                        });   
                                    }else{
                                        dispatch({
                                            type: 'FETCH_ADMAN_DAILY_REVENUE_FULFILLED', 
                                            payload: {
                                                increase: false, 
                                                yesterdaysRevenue: yesterday_revenue, 
                                                todaysRevenue: revenue,
                                                trend: 'zmdi zmdi-trending-down', 
                                                badge: 'badge badge-danger pull-left m-t-20', 
                                                change: ((1-revenue/yesterday_revenue)*100).toFixed(1)
                                            }
                                        });                    
                                    }
                                }else{
                                    dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.errors});
                                }
                            },
                            error: function(response){
                                dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.responseText});
                            }
                        });
                    }else{
                        dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.errors});
                    }
                },
                error: function(response){
                    dispatch({type: 'FETCH_ADMAN_DAILY_REVENUE_ERROR', payload: response.responseText});
                }
            });
        },1000*60*30);
    }
}