export function getAllDealsTimeout() {
    return function(dispatch) {
        
        setTimeout(function(){
            var revenue, revenue_yesterday;
	
            gapi.client.load('adexchangeseller','v2.0', function() {
                var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
                    accountId: "pub-7894268851903152",
                    startDate: "today-1d",
                    endDate: "today",
                    metric: ["Earnings"],
                    dimension: 'date',
                    fields: "rows"
                });
                requestRevenue.execute(function(resp) {
                    console.log(resp);
                    revenue_yesterday=parseFloat(resp.rows[0][1]);
                    revenue=parseFloat(resp.rows[1][1]);

                    if(revenue > revenue_yesterday){
                        dispatch({
                            type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                            payload: {
                                increase: true, 
                                ADXDealRevenueYesterday: revenue_yesterday, 
                                ADXDealRevenue: revenue,
                                trend: 'zmdi zmdi-trending-up', 
                                badge: 'badge badge-success pull-left m-t-20', 
                                change: ((revenue/revenue_yesterday-1)*100).toFixed(1),
                                deal: 'All Deals'
                            }
                        });   
                    }else{
                        dispatch({
                            type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                            payload: {
                                increase: false, 
                                ADXDealRevenueYesterday: revenue_yesterday, 
                                ADXDealRevenue: revenue,
                                trend: 'zmdi zmdi-trending-down', 
                                badge: 'badge badge-danger pull-left m-t-20', 
                                change: ((1-revenue/revenue_yesterday)*100).toFixed(1),
                                deal: 'All Deals'
                            }
                        });                    
                    }
                }, function(reason){
                    dispatch({type: 'FETCH_ADX_DEALS_REVENUE_ERROR', payload: reason.error.message});
                });
            });
        },1000*10);
    }
}

export function getAllDeals() {
    return function(dispatch) {

        //document.getElementById("adx_deal_type").innerHTML='All Deals';
        var revenue, revenue_yesterday;

        gapi.client.load('adexchangeseller','v2.0', function() {
            var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
                accountId: "pub-7894268851903152",
                startDate: "today-1d",
                endDate: "today",
                metric: ["Earnings"],
                dimension: 'date',
                fields: "rows"
            });
            requestRevenue.execute(function(resp) {
                revenue_yesterday=parseFloat(resp.rows[0][1]);
                revenue=parseFloat(resp.rows[1][1]);
                //document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(revenue.toFixed(2));

                if(revenue > revenue_yesterday){
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: true, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-up', 
                            badge: 'badge badge-success pull-left m-t-20', 
                            change: ((revenue/revenue_yesterday-1)*100).toFixed(1),
                            deal: 'All Deals'
                        }
                    });   
                }else{
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: false, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-down', 
                            badge: 'badge badge-danger pull-left m-t-20', 
                            change: ((1-revenue/revenue_yesterday)*100).toFixed(1),
                            deal: 'All Deals'
                        }
                    });                    
                }
            }, function(reason){
                dispatch({type: 'FETCH_ADX_DEALS_REVENUE_ERROR', payload: reason.error.message});
            });
        });
    }
}

export function getOpenDeal() {
    return function(dispatch) {
        
        //document.getElementById("adx_deal_type").innerHTML='All Deals';
        var revenue, revenue_yesterday;

        gapi.client.load('adexchangeseller','v2.0', function() {
            var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
                accountId: "pub-7894268851903152",
                startDate: "today-1d",
                endDate: "today",
                metric: ["Earnings"],
                dimension: 'date',
                filter: "TRANSACTION_TYPE_NAME==Subasta abierta,TRANSACTION_TYPE_NAME==Open auction",
                fields: "rows"
            });
            requestRevenue.execute(function(resp) {
                revenue_yesterday=parseFloat(resp.rows[0][1]);
                revenue=parseFloat(resp.rows[1][1]);

                if(revenue > revenue_yesterday){
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: true, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-up', 
                            badge: 'badge badge-success pull-left m-t-20', 
                            change: ((revenue/revenue_yesterday-1)*100).toFixed(1),
                            deal: 'Open Auction'
                        }
                    });   
                }else{
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: false, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-down', 
                            badge: 'badge badge-danger pull-left m-t-20', 
                            change: ((1-revenue/revenue_yesterday)*100).toFixed(1),
                            deal: 'Open Auction'
                        }
                    });                    
                }
            }, function(reason){
                dispatch({type: 'FETCH_ADX_DEALS_REVENUE_ERROR', payload: reason.error.message});
            });
        });
    }
}

export function getPrivateDeals() {
    return function(dispatch) {
        
        //document.getElementById("adx_deal_type").innerHTML='All Deals';
        var revenue, revenue_yesterday;

        gapi.client.load('adexchangeseller','v2.0', function() {
            var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
                accountId: "pub-7894268851903152",
                startDate: "today-1d",
                endDate: "today",
                metric: ["Earnings"],
                dimension: 'date',
                filter: "TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction",
                fields: "rows"
            });
            requestRevenue.execute(function(resp) {
                revenue_yesterday=parseFloat(resp.rows[0][1]);
                revenue=parseFloat(resp.rows[1][1]);

                if(revenue > revenue_yesterday){
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: true, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-up', 
                            badge: 'badge badge-success pull-left m-t-20', 
                            change: ((revenue/revenue_yesterday-1)*100).toFixed(1),
                            deal: 'Private Auction'
                        }
                    });   
                }else{
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: false, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-down', 
                            badge: 'badge badge-danger pull-left m-t-20', 
                            change: ((1-revenue/revenue_yesterday)*100).toFixed(1),
                            deal: 'Private Auction'
                        }
                    });                    
                }
            }, function(reason){
                dispatch({type: 'FETCH_ADX_DEALS_REVENUE_ERROR', payload: reason.error.message});
            });
        });
    }
}

export function getPreferredDeals() {
    return function(dispatch) {
        
        var revenue, revenue_yesterday;

        gapi.client.load('adexchangeseller','v2.0', function() {
            var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
                accountId: "pub-7894268851903152",
                startDate: "today-1d",
                endDate: "today",
                metric: ["Earnings"],
                dimension: 'date',
                filter: "TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal",
                fields: "rows"
            });
            requestRevenue.execute(function(resp) {
                revenue_yesterday=parseFloat(resp.rows[0][1]);
                revenue=parseFloat(resp.rows[1][1]);

                if(revenue > revenue_yesterday){
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: true, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-up', 
                            badge: 'badge badge-success pull-left m-t-20', 
                            change: ((revenue/revenue_yesterday-1)*100).toFixed(1),
                            deal: 'Preferred Deals'
                        }
                    });   
                }else{
                    dispatch({
                        type: 'FETCH_ADX_DEALS_REVENUE_FULFILLED', 
                        payload: {
                            increase: false, 
                            ADXDealRevenueYesterday: revenue_yesterday, 
                            ADXDealRevenue: revenue,
                            trend: 'zmdi zmdi-trending-down', 
                            badge: 'badge badge-danger pull-left m-t-20', 
                            change: ((1-revenue/revenue_yesterday)*100).toFixed(1),
                            deal: 'Preferred Deals'
                        }
                    });                    
                }
            }, function(reason){
                dispatch({type: 'FETCH_ADX_DEALS_REVENUE_ERROR', payload: reason.error.message});
            });
        });
    }
}