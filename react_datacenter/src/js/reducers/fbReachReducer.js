import {numberWithCommas} from '../functions/utility'

export default function reducer(state={
    todaysReach: 0,
    yesterdaysReach: -1,
    increase: true,
    trend: "zmdi zmdi-trending-up",
    badge: "badge badge-success pull-left m-t-0",
    change: 0,
    fetching: false,
    fetched: false,
    error: null
  }, action) {

    switch (action.type) {
        case "FETCH_FB_REACH": {
            return {...state, fetching: true}
        }
        case "FETCH_FB_REACH_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_FB_REACH_FULFILLED": {
            
            document.getElementById("reach").innerHTML=numberWithCommas(action.payload.fbReach);
            document.getElementById("reach_trend").className = action.payload.trend;
            document.getElementById("reach_badge").className = action.payload.badge;
            document.getElementById("reach_change").innerHTML = action.payload.change;
            
            return {
                ...state,
                fetching: false,
                fetched: true,
                todaysReach: action.payload.fbReach,
                yesterdaysReach: action.payload.fbReachYesterday,
                increase: action.payload.fbReachToday >= action.payload.fbReachYesterday ? true : false,
                trend: action.payload.trend,
                badge: action.payload.badge,
                change: action.payload.change,
            }
        }    
    }

    return state
}