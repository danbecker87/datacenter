import { combineReducers } from "redux"

import tweets from "./tweetsReducer"
import user from "./userReducer"
import activeUsers from "./activeUsersReducer"
import fbDailyRevenue from "./fbDailyRevenueReducer"
import fbReach from "./fbReachReducer"
import ADXRevenueReducer from "./ADXRevenueReducer"
import admanDailyRevenueReducer from "./admanDailyRevenueReducer"
import fbMonthlyRevenueReducer from "./fbMonthlyRevenueReducer"
import admanMonthlyRevenueReducer from "./admanMonthlyRevenueReducer"


export default combineReducers({
    tweets,
    user,
    activeUsers,
    fbDailyRevenue,
    fbReach,
    ADXRevenueReducer,
    admanDailyRevenueReducer,
    fbMonthlyRevenueReducer,
    admanMonthlyRevenueReducer
})
