import {numberWithCommas} from '../functions/utility'

export default function reducer(state={
    todaysRevenue: 0,
    yesterdaysRevenue: -1,
    increase: true,
    fetching: false,
    fetched: false,
    error: null
  }, action) {

    switch (action.type) {
        case "FETCH_FB_DAILY_REVENUE": {
            return {...state, fetching: true}
        }
        case "FETCH_FB_DAILY_REVENUE_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_FB_DAILY_REVENUE_FULFILLED": {
            const revenue = action.payload.fbRevenueToday;
            const yesterday_revenue = action.payload.fbRevenueYesterday;
            
            $('#FBDailyRevenue')[0].innerHTML = numberWithCommas(revenue.toFixed(2));
            $('#FB_yesterday_revenue')[0].innerHTML = numberWithCommas(yesterday_revenue.toFixed(2));
            if(revenue-yesterday_revenue > 0){
                document.getElementById("FB_daily_revenue_trend").className = "zmdi zmdi-trending-up";
                document.getElementById("FB_daily_revenue_badge").className = "badge badge-success pull-left m-t-20";
                document.getElementById("FB_daily_revenue_change").innerHTML = ((revenue/yesterday_revenue-1)*100).toFixed(2);
            }else{
                document.getElementById("FB_daily_revenue_trend").className = "zmdi zmdi-trending-down";
                document.getElementById("FB_daily_revenue_badge").className = "badge badge-danger pull-left m-t-20";
                document.getElementById("FB_daily_revenue_change").innerHTML = ((1-revenue/yesterday_revenue)*100).toFixed(2);
            }
            return {
              ...state,
              fetching: false,
              fetched: true,
              todaysRevenue: revenue,
              yesterdaysRevenue: yesterday_revenue,
              increase: action.payload.fbRevenueToday >= action.payload.fbRevenueYesterday ? true : false
            }
        }    
    }

    return state
}