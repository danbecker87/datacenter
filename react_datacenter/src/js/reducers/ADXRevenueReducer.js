import {numberWithCommas} from '../functions/utility'

export default function reducer(state={
    ADXDealRevenue: 0,
    ADXDealRevenueYesterday: -1,
    increase: true,
    trend: 'zmdi zmdi-trending-up', 
    badge: 'badge badge-success pull-left m-t-20', 
    change: 0,
    error: null,
    deal: 'All Deals'
  }, action) {

    switch (action.type) {
        case "FETCH_ADX_DEALS_REVENUE_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_ADX_DEALS_REVENUE_FULFILLED": {
            const revenue = action.payload.ADXDealRevenue;
            const yesterday_revenue = action.payload.ADXDealRevenueYesterday;
            
            document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(revenue.toFixed(2));
            document.getElementById("adx_deal_type").innerHTML=action.payload.deal;
            document.getElementById("revenue_trend").className = action.payload.trend;
            document.getElementById("adx_revenue_badge").className = action.payload.badge;
            document.getElementById("adx_percentage").innerHTML = action.payload.change;
            
            return {
              ...state,
              ADXDealRevenue: revenue,
              ADXDealRevenueYesterday: yesterday_revenue,
              increase: action.payload.increase,
              trend: action.payload.trend, 
              badge: action.payload.badge, 
              change: action.payload.change,
              deal: action.payload.deal,
            }
        }    
    }

    return state
}