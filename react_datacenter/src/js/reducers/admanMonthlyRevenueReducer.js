import {numberWithCommas} from '../functions/utility'

export default function reducer(state={
    admanRevenue: 0,
    admanRevenueLastMonth: -1,
    admanRevenueLastMonthTotal: 0,
    increase: true,
    fetching: false,
    fetched: false,
    error: null
  }, action) {

    switch (action.type) {
        case "FETCH_ADMAN_MONTHLY_REVENUE": {
            return {...state, fetching: true}
        }
        case "FETCH_ADMAN_MONTHLY_REVENUE_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_ADMAN_MONTHLY_REVENUE_FULFILLED": {
            const revenue = action.payload.todaysRevenue;
            
            $('#Adman_month_revenue')[0].innerHTML = '$ '+numberWithCommas(revenue.toFixed(2));
            
            document.getElementById("Adman_revenue_trend").className = action.payload.trend;
            document.getElementById("Adman_revenue_badge").className = action.payload.badge;
            document.getElementById("Adman_revenue_change").innerHTML = action.payload.change;
            
            return {
              ...state,
              fetching: false,
              fetched: true,
              admanRevenue: revenue,
            }
        }  
        case "FETCH_ADMAN_MONTHLY_REVENUE_FULFILLED": {
            const revenue_last_month = action.payload.admanRevenueLastMonth;
            const revenue_last_month_total = action.payload.admanRevenueLastMonthTotal;
            
            $('#Adman_last_month_revenue')[0].innerHTML = numberWithCommas(adman_revenue_last_month.toFixed(2));
            
            document.getElementById("Adman_month_revenue_trend").className = action.payload.trend;
            document.getElementById("Adman_month_revenue_badge").className = action.payload.badge;
            document.getElementById("Adman_month_revenue_change").innerHTML = action.payload.change;
            
            return {
                ...state,
                fetching: false,
                fetched: true,
                admanRevenueLastMonth: revenue_last_month,
                admanRevenueLastMonthTotal: revenue_last_month_total,
            }
        }
    }

    return state
}