export default function reducer(state={
    previousActiveUsers: 0,
    currentActiveUsers: -1,
    increase: true,
    previousActiveUsersOrganic: 0,
    currentActiveUsersOrganic: -1,
    increaseOrganic: true,
    previousActiveUsersPaid: 0,
    currentActiveUsersPaid: -1,
    increasePaid: true,
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
        case "FETCH_ACTIVE_USERS": {
            return {...state, fetching: true}
        }
        case "FETCH_ACTIVE_USERS_ERROR": {
            return {...state, fetching: false, error: action.payload, increase: false}
        }
        case "FETCH_ACTIVE_USERS_FULFILLED": {
            document.getElementById("active_users").innerHTML = action.payload.activeUsers;
            return {
              ...state,
              fetching: false,
              fetched: true,
              previousActiveUsers: state.currentActiveUsers,
              currentActiveUsers: action.payload.activeUsers,
              increase: action.payload.activeUsers >= state.currentActiveUsers ? true : false
            }
        }    
        case "FETCH_ACTIVE_USERS_ORGANIC": {
            return {...state, fetching: true}
        }
        case "FETCH_ACTIVE_USERS_ORGANIC_ERROR": {
            return {...state, fetching: false, error: action.payload, increaseOrganic: false}
        }
        case "FETCH_ACTIVE_USERS_ORGANIC_FULFILLED": {
            document.getElementById("active_users_organic").innerHTML = action.payload.activeUsersOrganic;
            return {
              ...state,
              fetching: false,
              fetched: true,
              previousActiveUsersOrganic: state.currentActiveUsersOrganic,
              currentActiveUsersOrganic: action.payload.activeUsersOrganic,
              increaseOrganic: action.payload.activeUsersOrganic >= state.currentActiveUsersOrganic ? true : false
            }
        }
        case "FETCH_ACTIVE_USERS_PAID": {
            return {...state, fetching: true}
        }
        case "FETCH_ACTIVE_USERS_PAID_ERROR": {
            return {...state, fetching: false, error: action.payload, increasePaid: false}
        }
        case "FETCH_ACTIVE_USERS_PAID_FULFILLED": {
            document.getElementById("active_users_paid").innerHTML = action.payload.activeUsersPaid;
            return {
              ...state,
              fetching: false,
              fetched: true,
              previousActiveUsersPaid: state.currentActiveUsersPaid,
              currentActiveUsersPaid: action.payload.activeUsersPaid,
              increasePaid: action.payload.activeUsersPaid >= state.currentActiveUsersPaid ? true : false
            }
        }
    }

    return state
}
