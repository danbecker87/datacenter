import {numberWithCommas} from '../functions/utility'

export default function reducer(state={
    todaysRevenue: 0,
    yesterdaysRevenue: -1,
    increase: true,
    fetching: false,
    fetched: false,
    error: null
  }, action) {

    switch (action.type) {
        case "FETCH_ADMAN_DAILY_REVENUE": {
            return {...state, fetching: true}
        }
        case "FETCH_ADMAN_DAILY_REVENUE_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_ADMAN_DAILY_REVENUE_FULFILLED": {
            const revenue = action.payload.todaysRevenue;
            const yesterday_revenue = action.payload.yesterdaysRevenue;
            
            $('#Adman_revenue')[0].innerHTML = numberWithCommas(revenue.toFixed(2));
            $('#Adman_yesterday_revenue')[0].innerHTML = numberWithCommas(yesterday_revenue.toFixed(2));
            
            document.getElementById("Adman_revenue_trend").className = action.payload.trend;
            document.getElementById("Adman_revenue_badge").className = action.payload.badge;
            document.getElementById("Adman_revenue_change").innerHTML = action.payload.change;
            
            return {
              ...state,
              fetching: false,
              fetched: true,
              todaysRevenue: revenue,
              yesterdaysRevenue: yesterday_revenue,
              increase: action.payload.increase
            }
        }    
    }

    return state
}