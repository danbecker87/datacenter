import {numberWithCommas} from '../functions/utility'

export default function reducer(state={
    fbMonthlyRevenue: 0,
    fbRevenueLastMonth: -1,
    fbRevenueLastMonthTotal: 0,
    increase: true,
    fetching: false,
    fetched: false,
    error: null
  }, action) {

    switch (action.type) {
        case "FETCH_FB_MONTHLY_REVENUE": {
            return {...state, fetching: true}
        }
        case "FETCH_FB_MONTHLY_REVENUE_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_FB_MONTHLY_REVENUE_FULFILLED": {
            const revenue = action.payload.fbMonthlyRevenue;
            const last_month_revenue = action.payload.fbRevenueLastMonth;
            const last_month_revenue_total = action.payload.fbRevenueLastMonthTotal;
            
            document.getElementById("FB_last_month_revenue").innerHTML = numberWithCommas(action.payload.fbRevenueLastMonthTotal.toFixed(2));
            $('#FBRevenue')[0].innerHTML = '$'+numberWithCommas(action.payload.fbMonthlyRevenue.toFixed(2));
            
            document.getElementById("FB_revenue_trend").className = action.payload.trend;
            document.getElementById("FB_revenue_badge").className = action.payload.badge;
            document.getElementById("FB_revenue_change").innerHTML = action.payload.change;
                    
            return {
                ...state,
                fetching: false,
                fetched: true,
                fbMonthlyRevenue: revenue,
                fbRevenueLastMonth: last_month_revenue,
                fbRevenueLastMonthTotal: last_month_revenue_total,
                increase: action.payload.increase
            }
        }    
    }

    return state
}