import React from 'react'
import { connect } from "react-redux"

import {getAllDeals, getAllDealsTimeout, getOpenDeal, getPrivateDeals, getPreferredDeals} from '../actions/ADXRevenueActions'

@connect((store) => {
    return {
        
    };
})
export default class ADXRevenueCardBox extends React.Component{

    constructor(){
        super();
    }
    
    componentWillMount(){
        //this.props.dispatch(getAllDealsTimeout());
    }
    
    getAllDeals(){
        this.props.dispatch(getAllDeals());
    }
    
    getOpenDeal(){
        this.props.dispatch(getOpenDeal());
    }
    
    getPrivateDeals(){
        this.props.dispatch(getPrivateDeals());
    }
    
    getPreferredDeals(){
        this.props.dispatch(getPreferredDeals());
    }
    
    render(){
        return(
            <div class="col-lg-3 col-md-3">
                <div class="card-box">
                    <div class="dropdown pull-right">
                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu" id="deals_selector">
                            <li><a onClick={this.getAllDeals.bind(this)} >All Deals</a></li>
                            <li class="divider"></li>
                            <li><a onClick={this.getOpenDeal.bind(this)}>Open Auction</a></li>
                            <li><a onClick={this.getPrivateDeals.bind(this)}>Private Auction</a></li>
                            <li><a onClick={this.getPreferredDeals.bind(this)}>Preferred Deals</a></li>
                        </ul>
                    </div>

                    <h4 class="header-title m-t-0 m-b-0">ADX Revenue Today</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                            <span class="badge badge-success pull-left m-t-20" id="adx_revenue_badge"> <span id="adx_percentage"></span>% <i id="revenue_trend" class="zmdi zmdi-trending-up"></i> </span>
                            <h2 class="m-b-0"> $ <span id="adx_revenue_amount"></span> </h2>
                            <p class="text-muted m-b-0" id="adx_deal_type">Deal</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}