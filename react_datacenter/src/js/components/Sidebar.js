import React from "react"

import SidebarMenuItem from './SidebarMenuItem'

export default class Sidebar extends React.Component{
    constructor(){
        super();
    }
    
    render(){
        return (
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    {/* User */}
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/logodeforma.png" alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive"/>
                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                        </div>
                        <h5><a href="index.html">El Deforma</a> </h5>
                        <a href="logout.php"><button class="btn btn-danger">Logout</button></a>
                    </div>

                    {/*Sidembar TODO turn this into a component*/}
                    <div id='sidebar-menu'>
                        <ul>
                            <li class="text-muted menu-title">Navigation</li>
                            <SidebarMenuItem linkTo='full_report.html' icon='zmdi zmdi-check-all' title='Daily Report'/>
                            <SidebarMenuItem linkTo='index.html' icon='zmdi zmdi-view-dashboard' title='Secure Dashboard'/>
                            <SidebarMenuItem linkTo='CPM.html' icon='zmdi zmdi-money' title='CPM/RPM'/>
                            <SidebarMenuItem linkTo='fb_data_center' icon='zmdi zmdi-facebook-box' title='Facebook Datacenter'/>
                            <SidebarMenuItem linkTo='CXW.html' icon='zmdi zmdi-trending-up' title='CXW Analysis'/>
                            <SidebarMenuItem linkTo='historic_campaigns,html' icon='zmdi zmdi-time-restore' title='Campaign History'/>
                            <SidebarMenuItem linkTo='google_datacenter.html' icon='zmdi zmdi-google' title='Google Analytics'/>
                            <SidebarMenuItem linkTo='leaderboard.html' icon='zmdi zmdi-format-list-numbered' title='Leaderboard'/>
                            <SidebarMenuItem linkTo='open_leaderboard.html' icon='zmdi zmdi-edit' title='Desempeño'/>
                            <SidebarMenuItem linkTo='metricas.html' icon='ti-bar-chart' title='Métricas Notas'/>
                            <SidebarMenuItem linkTo='reporte.html' icon='fa fa-clipboard' title='Reporte de Nota'/>
                            <SidebarMenuItem linkTo='invoicing_manual.html' icon='zmdi zmdi-money' title='Invoicing Manuals'/>
                            <SidebarMenuItem linkTo='report_repository.html' icon='zmdi zmdi-file-text' title='Reports'/>
                            <SidebarMenuItem linkTo='ventas.html' icon='zmdi zmdi-money-box' title='ventas'/>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        )
    }
}