import React from 'react'

export default class WidgetCardBox extends React.Component{

    constructor(){
        super();
    }
    
    render(){
        return(
            <div class={"col-lg-"+this.props.size+" col-md-"+this.props.size}>
                <div class="card-box">

                    <h4 class="header-title m-t-0 m-b-10">{this.props.cardTitle}</h4>

                    <div class="widget-box-2">
                        <div class="widget-detail-2">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    
}