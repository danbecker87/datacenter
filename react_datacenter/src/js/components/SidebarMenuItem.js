import React from "react"

export default class SidebarMenuItem extends React.Component{
    constructor(){
        super();
    }
    
    render(){
        return (
            <li>
                <a href={this.props.linkTo} class="waves-effect"><i class={this.props.icon}></i> <span> {this.props.title} </span> </a>
            </li>
        )
    }
}