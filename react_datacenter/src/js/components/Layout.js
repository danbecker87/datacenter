import React from "react"
import { connect } from "react-redux"

import { fetchUser } from "../actions/userActions"
import { fetchTweets } from "../actions/tweetsActions"
import { fetchActiveUsers } from "../actions/activeUsersActions"
import { fetchFBDailyRevenue } from "../actions/fbDailyRevenueActions"
import { fetchFBReach } from "../actions/fbReachActions"
import { fetchAdmanDailyRevenue } from "../actions/admanDailyRevenueActions"
import { fetchFBMonthlyRevenue } from "../actions/fbMonthlyRevenueActions"
import { fetchAdmanMonthlyRevenue } from "../actions/admanMonthlyRevenueActions"

import Topbar from "./Topbar"
import Sidebar from "./Sidebar"
import ModalSpend from "./ModalSpend"
import WidgetCardBox from "./WidgetCardBox"
import ADXRevenueCardBox from './ADXRevenueCardBox'

@connect((store) => {
    return {
        user: store.user.user,
        tweets: store.tweets.tweets,
        activeUsers: store.activeUsers,
        fbDailyRevenue: store.fbDailyRevenue,
        fbReach: store.fbReach,
        admanDailyRevenue: store.admanDailyRevenue,
        fbMonthlyRevenue: store.fbMonthlyRevenue,
        admanMonthlyRevenue: store.admanMonthlyRevenue
        
    };
})
export default class Layout extends React.Component {
    componentWillMount() {
        //this.props.dispatch(fetchActiveUsers());
        //this.props.dispatch(fetchFBDailyRevenue());
        //this.props.dispatch(fetchFBReach());
        //this.props.dispatch(fetchAdmanDailyRevenue());
        //this.props.dispatch(fetchFBMonthlyRevenue());
        this.props.dispatch(fetchAdmanMonthlyRevenue());
        
    }
    
    componentDidUpdate(){
        //this.props.dispatch(fetchActiveUsers());
    }
    
    componentDidMount(){
        //this.props.dispatch(fetchActiveUsers());
    }

    fetchTweets() {
        //this.props.dispatch(fetchTweets())
    }
    
    /*fetchActiveUsers(){
        this.props.dispatch(fetchActiveUsers());
    }*/

    render() {
        const { user, tweets, activeUsers, fbDailyRevenue, fbReach, admanDailyRevenue, fbMonthlyRevenue, admanMonthlyRevenue } = this.props;
        
        return (
            /*Begin page*/
            <div id="wrapper">

                {/*Top Bar Start*/}
                <Topbar title='DataCenter'/>
                {/*Top Bar End */}

                {/*========== Left Sidebar Start ========== */}
                <Sidebar />
                {/*Left Sidebar End */}

                {/* ============================================================== 
                Start right Content here 
                ============================================================== */}
                <div class="content-page">
                    {/*TODO implement resume all calls button with an action*/}
                    {/*Start content */}
                    <div class="content">
                        <div class="container">

                            {/* Modal Spend*/} 
                            <ModalSpend />

                            <div class="row">

                                <WidgetCardBox size='4' cardTitle='Active Users' >
                                    <span class="badge badge-success pull-left m-t-20" id="active_users_badge"><span id="active_users_change">-</span><i class="zmdi zmdi-trending-up" id="active_users_trend"></i> </span>
                                    <h2 class="m-b-0" id="active_users"> - </h2>
                                    <p class="text-muted m-b-0">Active Users</p>
                                </WidgetCardBox>
                                {/*Active Users */}

                                <WidgetCardBox size='4' cardTitle='Active Users'>
                                    <div class='col-md-5'>
                                        <div class='row'>
                                            <span class="badge badge-success pull-left m-t-20" id="active_users_organic_badge"><span id="active_users_organic_change">-</span><i class="zmdi zmdi-trending-up" id="active_users_organic_trend"></i></span>
                                            <h2 class="m-b-0 pull-right" id="active_users_organic"> - </h2>
                                        </div>
                                        <div class='row'>
                                            <p class="text-muted m-b-5">Organic Active Users</p>
                                        </div>
                                    </div>
                                    <div class='col-md-2'></div>
                                    <div class='col-md-5'>
                                        <div class='row'>
                                            <span class="badge badge-success pull-left m-t-20" id="active_users_paid_badge"><span id="active_users_paid_change">-</span><i class="zmdi zmdi-trending-up" id="active_users_paid_trend"></i> </span>
                                            <h2 class="m-b-0 pull-right" id="active_users_paid"> - </h2>
                                        </div>
                                        <div class='row'>
                                            <p class="text-muted m-b-5">Paid Active Users</p>
                                        </div>
                                    </div>
                                    <p class="text-muted m-b-5">&nbsp;</p>
                                </WidgetCardBox>{/* Organic-Paid Active Users */}

                                <WidgetCardBox size='4' cardTitle='FB Reach'>
                                    <div class='row'>
                                        <span class={fbReach.badge} id="reach_badge"><span id="reach_change">{fbReach.change}</span>%<i class={fbReach.trend} id="reach_trend"></i> </span>
                                        <h2 class="m-b-0 pull-right" id="reach"> {fbReach.todyasReach} </h2>
                                    </div>
                                    <div class='row'>
                                        <p class="text-muted m-b-0"> Reach Today </p>
                                    </div>
                                </WidgetCardBox>{/*Reach FB*/} 

                            </div>{/*Active Users & Reach */}

                            <div class="row">

                                <ADXRevenueCardBox></ADXRevenueCardBox>{/* ADX Revenue */}

                                <WidgetCardBox size='3' cardTitle='FB Revenue Today'>
                                    <span class="badge badge-success pull-left m-t-0" id="FB_daily_revenue_badge"><span id="FB_daily_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="FB_daily_revenue_trend"></i> </span>
                                    <h2 class="m-b-0"> $ <span id="FBDailyRevenue"></span> </h2>
                                    <p class="text-muted m-b-0"> FB Revenue Yesterday $ <span id='FB_yesterday_revenue'></span> </p>
                                </WidgetCardBox>{/*FB Revenue TODO change time based on timezone*/}

                                <WidgetCardBox size="3" cardTitle='Adman Revenue Yesterday'>
                                    <span class="badge badge-success pull-left m-t-0" id="Adman_revenue_badge"><span id="Adman_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="Adman_revenue_trend"></i> </span>
                                    <h2 class="m-b-0"> $ <span id="Adman_revenue"></span> </h2>
                                    <p class="text-muted m-b-0"> Adman Day Before Yesterday Revenue $ <span id='Adman_yesterday_revenue'></span> </p>
                                </WidgetCardBox>{/* Adman Revenue */}

                                <WidgetCardBox size="3" cardTitle='Taboola Revenue Yesterday'>
                                    <span class="badge badge-success pull-left m-t-0" id="Taboola_revenue_badge"><span id="Taboola_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="Taboola_revenue_trend"></i> </span>
                                    <h2 class="m-b-0"> $ <span id="Taboola_revenue"></span> </h2>
                                    <p class="text-muted m-b-0"> Taboola Day Before Yesterday Revenue $ <span id='Taboola_yesterday_revenue'></span> </p>
                                </WidgetCardBox>{/*Taboola Revenue TODO */}

                            </div> {/*Revenues */}

                            <div class='row'>

                                <WidgetCardBox size="2" cardTitle='FB Month Revenue'>
                                    <span class="badge badge-success pull-left m-t-0" id="FB_revenue_badge"><span id="FB_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="FB_revenue_trend"></i> </span>
                                    <h2 class="p-t-0 m-b-5" id="FBRevenue"> - </h2>
                                    <p class="text-muted m-b-0"> FB Monthly Revenue $<span id='FB_last_month_revenue'></span> </p>
                                </WidgetCardBox> {/*FB Month Revenue */}

                                <WidgetCardBox size="2" cardTitle='Adman Month Revenue'>
                                    <span class="badge badge-success pull-left m-t-0" id="Adman_month_revenue_badge"><span id="Adman_month_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="Adman_month_revenue_trend"></i> </span>
                                    <h2 class="p-t-0 m-b-5" id="Adman_month_revenue"> - </h2>
                                    <p class="text-muted m-b-0"> Adman Last Month Revenue $<span id='Adman_last_month_revenue'></span> </p>
                                </WidgetCardBox>{/* Adman Month Revenue */}

                                <WidgetCardBox size="2" cardTitle='Taboola Month Revenue'>
                                    <span class="badge badge-success pull-left m-t-0" id="Taboola_month_revenue_badge"><span id="Taboola_month_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="Taboola_month_revenue_trend"></i> </span>
                                    <h2 class="p-t-0 m-b-5" id="Taboola_month_revenue"> - </h2>
                                    <p class="text-muted m-b-0"> Taboola Last Month Revenue $<span id='Taboola_last_month_revenue'></span> </p>
                                </WidgetCardBox>{/* Taboola Month Revenue TODO */}

                                <div class="col-lg-3 col-md-4">
                                    <div class="card-box">
                                        <button class="btn-xs btn-primary btn-rounded w-md waves-effect waves-light m-b-0 pull-right" onclick="refreshADXMetrics();">Refresh</button>

                                        <h4 class="header-title m-t-0 m-b-0">ADX Month Metrics</h4>

                                        <div class="widget-box-2">
                                            <div class="widget-detail-2">
                                                <span class="badge badge-success pull-left m-t-0" id="month_revenue_badge"><span id="month_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="month_revenue_trend"></i> </span>
                                                <h2 class="p-t-0 m-b-5" id="ADX_month_metric_value"> - </h2>
                                                <p class="text-muted m-b-0" id="ADX_month_metric"> - </p>
                                            </div>
                                        </div>
                                    </div>
                                </div> {/*ADX Month Metrics */}

                                <WidgetCardBox size="3" cardTitle='Total Month Revenue'>
                                    <span class="badge badge-success pull-left m-t-0" id="total_revenue_badge"><span id="total_revenue_change">-</span>%<i class="zmdi zmdi-trending-up" id="total_revenue_trend"></i> </span>
                                    <h2 class="p-t-0 m-b-5" id="total_month_revenue"> - </h2>
                                    <p class="text-muted m-b-0" id="total_last_month_revenue"> - </p>
                                </WidgetCardBox>{/*Total Month Revenue */}

                            </div>{/* Month Revenue */}

                            <div class='row'>

                                <div class="col-lg-4 col-md-4">
                                    <div class="card-box">
                                        <button class="btn-xs btn-primary btn-rounded w-md waves-effect waves-light m-b-0 pull-right" onclick="refreshADXMetrics();">Refresh</button>

                                        <h4 class="header-title m-t-0 m-b-0">ADX Metrics</h4>

                                        <div class="widget-box-2">
                                            <div class="widget-detail-2">
                                                <h2 class="p-t-0 m-b-5" id="ADX_metric_value"> - </h2>
                                                <p class="text-muted m-b-0" id="ADX_metric"> - </p>
                                            </div>
                                        </div>
                                    </div>
                                </div> {/*ADX Metrics */}

                                <div class="col-lg-4 col-md-4">
                                    <div class="card-box">
                                        <div class="dropdown pull-right">
                                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu" id="fb_spend_dropdown">
                                            </ul>
                                        </div>

                                        <h4 class="header-title m-t-0 m-b-0">FB Spend</h4>

                                        <div class="widget-box-2">
                                            <div class="widget-detail-2">
                                                <h2 class="m-b-0"> $<span id="fb_spend"></span> </h2>
                                                <p class="text-muted m-b-0" id="fb_account"> - </p>
                                                <button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#spend-cap-modal">$$$</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> {/*FBSpend */}

                                <div class="col-lg-4 col-md-4">
                                    <div class="card-box">
                                        <div class="dropdown pull-right">
                                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu" id="pages_list">
                                                <li><a onclick="monthShares(this)" id="eldeforma">El Deforma</a></li>
                                                <li><a onclick="monthShares(this)" id='repsodia'>Repsodia</a></li>
                                            </ul>
                                        </div>

                                        <h4 class="header-title m-t-0 m-b-10">Month Shares</h4>

                                        <div class="widget-box-2">
                                            <div class="widget-detail-2">
                                                <h2 class="m-b-0"><span id="shares_amount"></span> </h2>
                                                <p class="text-muted m-b-5" id="shares_page">Page</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> {/*Shares */}

                            </div>{/*ADX Metrics, FB Spend & Shares */}

                            <div class='row'>

                                <WidgetCardBox size="4" cardTitle='Audience'>
                                    <h2 class="p-t-0 m-b-5" id="audience_metric_value"> - </h2>
                                    <p class="text-muted m-b-0" id="audience_metric"> - </p>
                                </WidgetCardBox>{/* Analytics Metrics */}

                                <WidgetCardBox size="4" cardTitle='Month Audience'>
                                    <span class="badge badge-success pull-left m-t-0" id="month_badge"><span id="month_change">-</span>%<i class="zmdi zmdi-trending-up" id="month_trend"></i> </span>
                                    <h2 class="p-t-0 m-b-5" id="month_audience_metric_value"> - </h2>
                                    <p class="text-muted m-b-0" id="month_audience_metric"> - </p>
                                </WidgetCardBox>{/* Month Analytics Metrics */}

                                <div class="col-lg-4 col-md-4">
                                    <div class="card-box">
                                        <div class="dropdown pull-right">
                                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu" id="pages_list">
                                                <li><a onclick="monthVideoViews(this)" id="eldeforma">El Deforma</a></li>
                                                <li><a onclick="monthVideoViews(this)" id='repsodia'>Repsodia</a></li>
                                            </ul>
                                        </div>

                                        <h4 class="header-title m-t-0 m-b-10">Video Views</h4>

                                        <div class="widget-box-2">
                                            <div class="widget-detail-2">
                                                <h2 class="m-b-0"><span id="video_views_amount"></span> </h2>
                                                <p class="text-muted m-b-5" id="video_views_page">Page</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* Video Views */}

                            </div>{/* Audience, Month Audience & VV*/}

                            <div class='row'>
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-color panel-tabs panel-primary">
                                        <div class="panel-heading">
                                            <ul class="nav nav-pills pull-right">
                                                <li class="">
                                                    <a href="#navpills-1" data-toggle="tab" aria-expanded="false">ADX Page CPM</a>
                                                </li>
                                                <li class="active">
                                                    <a href="#navpills-6" data-toggle="tab" aria-expanded="true">FAN + ADX CPM</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-2" data-toggle="tab" aria-expanded="false">Earnings</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-3" data-toggle="tab" aria-expanded="false">Coverage</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-4" data-toggle="tab" aria-expanded="false">Ad CPM</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-5" data-toggle="tab" aria-expanded="false">Real CPM</a>
                                                </li>
                                            </ul>
                                            <h3 class="panel-title">Métricas ADX & FAN</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <label for="from">From</label>
                                                <input type="text" id="from" name="from"></input>
                                                <label for="to">to</label>
                                                <input type="text" id="to" name="to"></input>
                                                <button class='btn btn-rounded btn-primary' id="graph" style={{visibility: 'hidden'}} onClick="getADXRangeMetrics()">Get Metrics</button>
                                                <select id="selector_deal" class="form-control" style={{width: 200}}>
                                                    <option value="all">All Deals</option>
                                                    <option value="TRANSACTION_TYPE_NAME==Subasta abierta,TRANSACTION_TYPE_NAME==Open auction">Open</option>
                                                    <option value="TRANSACTION_TYPE_NAME==First look,TRANSACTION_TYPE_NAME==First look">First Look</option>
                                                    <option value="TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction">Private</option>
                                                    <option value="TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal">Preferred</option>
                                                </select>
                                                <select id="selector_device" class="form-control" style={{width: 200}}>
                                                    <option value="all">All Devices</option>
                                                    <option value="PLATFORM_TYPE_NAME==Desktop">Desktop</option>
                                                    <option value="PLATFORM_TYPE_NAME==High-end mobile devices">Mobile</option>
                                                    <option value="PLATFORM_TYPE_NAME==Tablets">Tablets</option>
                                                    <option value="PLATFORM_TYPE_NAME!=Desktop">All Mobile</option>
                                                </select>
                                                <select id="selector_country" class="form-control" style={{width: 200}}>
                                                    <option value="all">All Countries</option>
                                                    <option value="COUNTRY_CODE==US">United States</option>
                                                    <option value="COUNTRY_CODE==MX">Mexico</option>
                                                </select>
                                                <select id="selector_branding" class="form-control" style={{width: 200}}>
                                                    <option value="all">All Brandings</option>
                                                    <option value="BRANDING_TYPE_CODE==Anonymous">Anonymous</option>
                                                    <option value="BRANDING_TYPE_CODE==Semi-Transparent">Semi-Transparent</option>
                                                    <option value="BRANDING_TYPE_CODE==Branded">Branded</option>
                                                </select>
                                                <div calss='row'>
                                                    $ <span id='total_earnings'> - </span> MXN
                                                </div>
                                                <div id="navpills-1" class="tab-pane fade">
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-12">
                                                            <div id="chart_div_PageCPM" class="col-lg-12" style={{height: 300}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-2" class="tab-pane fade">
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-12">
                                                            <div id="chart_div_Earnings" class="col-lg-12" style={{height: 300}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-3" class="tab-pane fade">
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-12">
                                                            <div id="chart_div_Coverage" class="col-lg-12" style={{height: 300}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-4" class="tab-pane fade">
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-12">
                                                            <div id="chart_div_AdCPM" class="col-lg-12" style={{height: 300}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-5" class="tab-pane fade">
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-12">
                                                            <div id="chart_div_RealCPM" class="col-lg-12" style={{height: 300}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-6" class="tab-pane fade in active">
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-12">
                                                            <div id="chart_div_FAN_ADX_pageCPM" class="col-lg-12" style={{height: 300}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* ADX Range Metrics */}

                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-color panel-tabs panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Reach FB</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class='row'>
                                                <label for="from_fb">From</label>
                                                <input type="text" id="from_fb" name="from_fb"></input>
                                                <label for="to_fb">to</label>
                                                <input type="text" id="to_fb" name="to_fb"></input>
                                                <button class='btn btn-rounded btn-primary' id="graph_reach" style={{visibility: 'hidden'}} onClick="getReach()">Get Reach</button>
                                            </div>
                                            <div class='row'>
                                                Unpaid: <span id='paid_reach'></span>&nbsp;&nbsp;&nbsp;
                                                Paid: <span id='unpaid_reach'></span>&nbsp;&nbsp;&nbsp;
                                                Total: <span id='total_reach'></span>
                                            </div>
                                            <div class='row'>
                                                <div id='chart_div_reach' class='col-lg-12' style={{height: 300}}></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* FB Reach */}

                            </div>{/*ADX & FAN Metrics Date Query */}

                            <div class='row'>
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-color panel-tabs panel-primary">
                                        <div class="panel-heading">
                                            <ul class="nav nav-pills pull-right">
                                                <li class="active">
                                                    <a href="#navpills-social-1" data-toggle="tab" aria-expanded="true">El Deforma</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-social-2" data-toggle="tab" aria-expanded="false">Repsodia</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-social-3" data-toggle="tab" aria-expanded="false">Tech Cult</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-social-4" data-toggle="tab" aria-expanded="false">Holixir</a>
                                                </li>
                                                <li class="">
                                                    <a href="#navpills-social-5" data-toggle="tab" aria-expanded="false">Sofanimo</a>
                                                </li>
                                            </ul>
                                            <h3 class="panel-title">Social Metrics</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="navpills-social-1" class="tab-pane fade in active">
                                                    <label for="month_social_metrics_deforma">Month</label>
                                                    <input type="text" id="month_social_metrics_deforma" name="month_social_metrics_deforma"></input>
                                                    <button class='btn btn-rounded btn-primary' id="table_social_metrics_deforma" style={{visibility: 'hidden'}} onClick="getSocialMetricsDeforma()">Get Metrics</button>
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-3">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="shares_deforma">-</span>
                                                                    Shares:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="video_views_deforma">-</span>
                                                                    Video Views:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-purple" id="reach_deforma">-</span>
                                                                    Reach: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_ctr_deforma">-</span>
                                                                    Daily Avg. CTR: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-success" id="users_deforma">-</span>
                                                                    Unique Users: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-warning" id="sessions_deforma">-</span>
                                                                    Sessions: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-inverse" id="pageviews_deforma">-</span>
                                                                    Pageviews: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-inverse" id="shares_on_site_deforma">-</span>
                                                                    Shares on site: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_deforma_click">-</span>
                                                                    Daily Avg. VTR (click to play): 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_deforma">-</span>
                                                                    Daily Avg. VTR (views): 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-success" id="ACPE">-</span>
                                                                    ACPE (paid): 
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-social-2" class="tab-pane fade">
                                                    <label for="month_social_metrics_repsodia">Month</label>
                                                    <input type="text" id="month_social_metrics_repsodia" name="month_social_metrics_repsodia"></input>
                                                    <button class='btn btn-rounded btn-primary' id="table_social_metrics_repsodia" style={{visibility: 'hidden'}} onClick="getSocialMetricsRepsodia()">Get Metrics</button>
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-3">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="shares_repsodia">-</span>
                                                                    Shares:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="video_views_repsodia">-</span>
                                                                    Video Views:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-purple" id="reach_repsodia">-</span>
                                                                    Reach: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_repsodia_click">-</span>
                                                                    Daily Avg. VTR (click to play): 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_repsodia">-</span>
                                                                    Daily Avg. VTR (views): 
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-social-3" class="tab-pane fade">
                                                    <label for="month_social_metrics_cult">Month</label>
                                                    <input type="text" id="month_social_metrics_cult" name="month_social_metrics_cult"></input>
                                                    <button class='btn btn-rounded btn-primary' id="table_social_metrics_cult" style={{visibility: 'hidden'}} onClick="getSocialMetricsCult()">Get Metrics</button>
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-3">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="shares_cult">-</span>
                                                                    Shares:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="video_views_cult">-</span>
                                                                    Video Views:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-purple" id="reach_cult">-</span>
                                                                    Reach: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_cult_click">-</span>
                                                                    Daily Avg. VTR (click to play): 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_cult">-</span>
                                                                    Daily Avg. VTR (views): 
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-social-4" class="tab-pane fade">
                                                    <label for="month_social_metrics_holixir">Month</label>
                                                    <input type="text" id="month_social_metrics_holixir" name="month_social_metrics_holixir"></input>
                                                    <button class='btn btn-rounded btn-primary' id="table_social_metrics_holixir" style={{visibility: 'hidden'}} onClick="getSocialMetricsHolixir()">Get Metrics</button>
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-3">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="shares_holixir">-</span>
                                                                    Shares:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="video_views_holixir">-</span>
                                                                    Video Views:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-purple" id="reach_holixir">-</span>
                                                                    Reach: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_holixir_click">-</span>
                                                                    Daily Avg. VTR (click to play): 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_holixir">-</span>
                                                                    Daily Avg. VTR (views): 
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="navpills-social-5" class="tab-pane fade">
                                                    <label for="month_social_metrics_sofanimo">Month</label>
                                                    <input type="text" id="month_social_metrics_sofanimo" name="month_social_metrics_sofanimo"></input>
                                                    <button class='btn btn-rounded btn-primary' id="table_social_metrics_sofanimo" style={{visibility: 'hidden'}} onClick="getSocialMetricsSofanimo()">Get Metrics</button>
                                                    <div class="row">
                                                        <div class="wrap_div col-lg-3">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="shares_sofanimo">-</span>
                                                                    Shares:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-primary" id="video_views_sofanimo">-</span>
                                                                    Video Views:
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-purple" id="reach_sofanimo">-</span>
                                                                    Reach: 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_sofanimo_click">-</span>
                                                                    Daily Avg. VTR (click to play): 
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span class="badge badge-info" id="avg_vtr_sofanimo">-</span>
                                                                    Daily Avg. VTR (views): 
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>{/* FB Reach & Social Metrics Date Query */}

                            <div class="row">

                                <div class="col-lg-8">
                                    <div class="card-box">

                                        <h4 class="header-title m-t-0 m-b-10">Active Sites</h4>

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Path</th>
                                                    <th>Active Users</th>
                                                </tr>
                                                </thead>
                                                <tbody id="active_table_rows">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class='row'>
                                        <div class="user-box col-lg-3" id='box_0'>
                                            <div class="user-img">
                                                <img id='no0' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive"/>
                                                <div class="user-status offline"><i class="zmdi zmdi-dot-circle" style={{color: '#FFD700', fontSize: 30}}></i></div>
                                            </div>
                                        </div>
                                        <div class="user-box col-lg-3" id='box_1'>
                                            <div class="user-img">
                                                <img id='no1' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive"/>
                                                <div class="user-status offline"><i class="zmdi zmdi-dot-circle" style={{color: '#FFD700', fontSize: 30}}></i></div>
                                            </div>
                                        </div>
                                        <div class="user-box col-lg-3" id='box_2'>
                                            <div class="user-img">
                                                <img id='no2' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive"/>
                                                <div class="user-status offline"><i class="zmdi zmdi-dot-circle" style={{color: '#CD7F32', fontSize: 30}}></i></div>
                                            </div>
                                        </div>
                                        <div class="user-box col-lg-3" id='box_3'>
                                            <div class="user-img">
                                                <img id='no3' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive"/>
                                                <div class="user-status offline"><i class="zmdi zmdi-dot-circle" style={{color: '#000000', fontSize: 30}}></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* Top Sites */}

                                <div class="col-lg-4">
                                    <div class="card-box">

                                        <h4 class="header-title m-t-0 m-b-10">Twitter</h4>

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Trending Topics Mexico</th>
                                                </tr>
                                                </thead>
                                                <tbody id="twitter_table_rows">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>{/* Twitter Trends */}
                            </div>{/*Top Sites, Medals and Twitter Trends  */}
                        </div> 
                    </div> 

                    <footer class="footer text-right">
                        2016 © ElDeforma.
                    </footer>

                </div>

            </div>
            // END wrapper 
        )
    }
}
