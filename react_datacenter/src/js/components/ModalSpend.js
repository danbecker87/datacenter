import React from 'react'

export default class ModalSpend extends React.Component{
    constructor(){
        super();
    }
    
    render(){
        return(
            <div id="spend-cap-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style={{display: 'none'}}>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action='#' id='form'>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Facebook spend cap status</h4>
                            </div>
                            <div class="modal-body">
                                Promote Interno
                                <div class="progress progress-bar-primary-alt" style={{height: 40}}>
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style={{width: 0+'%', height: 40, paddingTop: 3+'%'}} id='interno-balance'>
                                        <span id='number-interno-balance' style={{fontSize: 32}}>0</span>
                                    </div>
                                </div>
                                Branded Content
                                <div class="progress progress-bar-success-alt" style={{height: 40}}>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100" style={{width: 0+'%', height: 40, paddingTop: 3+'%'}} id='branded-balance'>
                                        <span id='number-branded-balance' style={{fontSize: 32}}>0</span>
                                    </div>
                                </div>
                                Repsodia
                                <div class="progress progress-bar-info-alt" style={{height: 40}}>
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style={{width: 0+'%', height: 40, paddingTop: 3+'%'}} id='repsodia-balance'>
                                        <span id='number-repsodia-balance' style={{fontSize: 32}}>0</span>
                                    </div>
                                </div>
                                Foreclose
                                <div class="progress progress-bar-warning-alt" style={{height: 40}}>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style={{width: 0+'%', height: 40, paddingTop: 3+'%'}} id='foreclose-balance'>
                                        <span id='number-foreclose-balance' style={{fontSize: 32}}>0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}