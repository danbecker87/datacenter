import React from "react" 

export default class Topbar extends React.Component {
    
    constructor(){
        super();
    }
    
    render(){
        return(
            <div class="topbar">
                {/*LOGO*/}
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                {/*Button mobile view to collapse sidebar menu */}
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        {/*Page title*/}
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">{this.props.title}</h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}