var access_token;
var version = '2.10';
var url_base = "https://graph.facebook.com/v"+version+"/";
var query = {edge: "", fields: ""};
var url="";
function buildURL(query,limit){
    url="";
    url+=url_base;
    url+=query.edge;
    url+="?fields=";
    url+=query.fields;
    url+="&limit="+limit;
    url+="&"+access_token;
    return url;
}
function statusChangeCallback(response) {
    
    access_token="access_token="+response.authResponse.accessToken;
    if (response.status === 'connected') {
        var cutoffTime=new Date();
        if(cutoffTime.getHours()==11 && cutoffTime.getMinutes()>=30 && cutoffTime.getMinutes()<=40){
            //makeApiCall();
        }	
    }
    else if (response.status === 'not_authorized') {
      document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
     }
    else {
      document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
     }
}
function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}
window.fbAsyncInit = function() {
    FB.init({
        appId      : '957927100941199',
        cookie     : true,
        xfbml      : true,
        version    : 'v'+version
    });

    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });

};
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));