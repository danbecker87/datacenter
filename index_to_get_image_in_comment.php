<?php
$miedos=array('a las arañas','a los payasos','a las serpientes');
$number = rand(1,3);
$title = substr($_SERVER[REQUEST_URI],1).' le tienes miedo a '.$miedos[$number-1];
$source = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        
        <title>¿A qué le tienes miedo?</title>
        <meta name="author" content="fear"/>
        
        <meta property="og:title" content=<?php echo '"'.$title.'"' ?> />
        <meta property="og:type" content="article" />
        <meta property="og:url" content=<?php echo '"'.$source.'"' ?>/>
        <meta property="og:image" content="http://deforma2.com/fear/images/f<?php echo $number ?>.jpg"/>
        <meta property="og:image:width" content='275'/>
        <meta property="og:image:height" content='183'/>
        <meta property="og:description" content="Descubre a qué le temes" />
        
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>

    </head>


    <body>

    Hello world

    </body>
</html>