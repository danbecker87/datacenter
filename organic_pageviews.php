<?php

// Load the Google API PHP Client Library.
require_once __DIR__ . '/vendor/autoload.php';

$PV_URL = $_GET['text'];
$PV_URL = str_replace('http://eldeforma.com/','',$PV_URL);
$PV_URL = substr($PV_URL,11);
$PV_URL = substr($PV_URL,0,-1);

$start =  date("Y-m-d", strtotime("first day of previous month"));
$date = date("Y-m-d", strtotime("last day of previous month"));
$end = date('Y-m-d', strtotime($date. ' + 4 days'));

$analytics = initializeAnalytics();
$response = getReport($analytics,$PV_URL,$start,$end);
printResults($response);


function initializeAnalytics()
{

  // Use the developers console and download your service account
  // credentials in JSON format. Place them in this directory or
  // change the key file location if necessary.
  $KEY_FILE_LOCATION = __DIR__.'/service-account-credentials.json';

  // Create and configure a new client object.
  $client = new Google_Client();
  $client->setApplicationName("Hello Analytics Reporting");
  $client->setAuthConfig($KEY_FILE_LOCATION);
    
  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
  $analytics = new Google_Service_AnalyticsReporting($client);

  return $analytics;
}


function getReport($analytics,$URL,$start,$end) {

    // Replace with your view ID, for example XXXX.
    $VIEW_ID = "41142760";

    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate($start);
    $dateRange->setEndDate($end);

    // Create the Metrics object.
    $PV = new Google_Service_AnalyticsReporting_Metric();
    $PV->setExpression("ga:pageviews");
    $PV->setAlias("pageviews");
    
    //Create the Dimensions
    $page = new Google_Service_AnalyticsReporting_Dimension();
    $page->setName('ga:pagePath');
        
    //Dimension filter
    $pathFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $pathFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "operator" => 'PARTIAL',
                "expressions" => array($URL)
            )
        )
        
    );
    
    $organicFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $organicFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "not" => true,
                "operator" => 'PARTIAL',
                "expressions" => array('p1=true')
            )
        )   
    );
    
    $GUAFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $GUAFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "not" => true,
                "operator" => 'PARTIAL',
                "expressions" => array('GUA=1')
            )
        )
    );
    
    $SREFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $SREFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "not" => true,
                "operator" => 'PARTIAL',
                "expressions" => array('SRE=1')
            )
        )
    );
    

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($VIEW_ID);
    $request->setDateRanges($dateRange);
    $request->setDimensions(array($page));
    $request->setDimensionFilterClauses(array($pathFilter,$organicFilter,$GUAFilter,$SREFilter));
    $request->setMetrics(array($PV));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}

function printResults($reports) {
    for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
    $report = $reports[ $reportIndex ];
    $header = $report->getColumnHeader();
    $dimensionHeaders = $header->getDimensions();
    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
    $rows = $report->getData()->getRows();
    $totals = $report->getData()->getTotals();

    for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
      $row = $rows[ $rowIndex ];
      $dimensions = $row->getDimensions();
      $metrics = $row->getMetrics();
      for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
        //print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
      }

      for ($j = 0; $j < count($metrics); $j++) {
        $values = $metrics[$j]->getValues();
        for ($k = 0; $k < count($values); $k++) {
          $entry = $metricHeaders[$k];
          //print($entry->getName() . ": " . $values[$k] . "</br>");
        }
      }
    }
    }
    
    echo number_format((float)$totals[0]['values'][0],0,'.',',');
}
?>