-- MySQL dump 10.13  Distrib 5.5.59, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: deforma_posts
-- ------------------------------------------------------
-- Server version	5.5.59-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `post_names`
--

DROP TABLE IF EXISTS `post_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_names` (
  `age` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(40) NOT NULL,
  `posted_date` datetime NOT NULL,
  `campaign` varchar(40) DEFAULT NULL,
  `ad_account` varchar(40) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `promoted` tinyint(4) DEFAULT NULL,
  `historical_promoted` int(11) DEFAULT NULL,
  `currency` varchar(4) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'POST',
  `goal` int(11) DEFAULT NULL,
  `likes_goal` int(11) DEFAULT NULL,
  `spend_limit` int(11) DEFAULT NULL,
  `GCS` tinyint(1) NOT NULL,
  `star` tinyint(4) NOT NULL DEFAULT '0',
  `twitter_id` varchar(40) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `client_campaign` varchar(200) DEFAULT NULL,
  `never_promote` int(11) NOT NULL DEFAULT '0',
  `last_promote` datetime DEFAULT NULL,
  PRIMARY KEY (`age`)
) ENGINE=InnoDB AUTO_INCREMENT=24200 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-13 19:39:07
