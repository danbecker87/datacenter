-- MySQL dump 10.13  Distrib 5.5.59, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: deforma_posts
-- ------------------------------------------------------
-- Server version	5.5.59-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `174058432636166_1613699648672030`
--

DROP TABLE IF EXISTS `174058432636166_1613699648672030`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `174058432636166_1613699648672030` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_time` datetime NOT NULL,
  `campaign` varchar(40) DEFAULT NULL,
  `spend` float DEFAULT NULL,
  `paid_link_clicks` int(11) DEFAULT NULL,
  `cxw` float DEFAULT NULL,
  `paid_ctr` float DEFAULT NULL,
  `cpm` float DEFAULT NULL,
  `link_cpm` float DEFAULT NULL,
  `penetration` float DEFAULT NULL,
  `organic_ctr` float DEFAULT NULL,
  `viral_amp` float DEFAULT NULL,
  `imp` int(11) DEFAULT NULL,
  `paid_imp` int(11) DEFAULT NULL,
  `viral_imp` int(11) DEFAULT NULL,
  `unique_imp` int(11) DEFAULT NULL,
  `unpaid_imp` int(11) DEFAULT NULL,
  `organic_imp` int(11) DEFAULT NULL,
  `link_clicks` int(11) DEFAULT NULL,
  `shares` int(11) DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  `comments` int(11) DEFAULT NULL,
  `reach` int(11) DEFAULT NULL,
  `fans` int(11) DEFAULT NULL,
  `engagement` float DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-13 18:40:25
