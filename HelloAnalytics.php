<?php

// Load the Google API PHP Client Library.
require_once __DIR__ . '/vendor/autoload.php';

//echo $_GET['text'];
$PV_URL = $_GET['text'];
$PV_URL = str_replace('http://eldeforma.com/','',$PV_URL);
$PV_URL = substr($PV_URL,11);
$PV_URL = substr($PV_URL,0,-1);
//echo $PV_URL.'</br>';

//echo $_SERVER['HTTP_HOST'].'/service-account-credentials.json';

$analytics = initializeAnalytics();
$response = getReport($analytics,$PV_URL);
printResults($response);


/**
 * Initializes an Analytics Reporting API V4 service object.
 *
 * @return An authorized Analytics Reporting API V4 service object.
 */
function initializeAnalytics()
{

  // Use the developers console and download your service account
  // credentials in JSON format. Place them in this directory or
  // change the key file location if necessary.
  $KEY_FILE_LOCATION = __DIR__.'/service-account-credentials.json';

  // Create and configure a new client object.
  $client = new Google_Client();
  $client->setApplicationName("Hello Analytics Reporting");
  $client->setAuthConfig($KEY_FILE_LOCATION);
    
  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
  $analytics = new Google_Service_AnalyticsReporting($client);

  return $analytics;
}


/**
 * Queries the Analytics Reporting API V4.
 *
 * @param service An authorized Analytics Reporting API V4 service object.
 * @return The Analytics Reporting API V4 response.
 */
function getReport($analytics,$URL) {

    // Replace with your view ID, for example XXXX.
    $VIEW_ID = "41142760";

    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("365daysAgo");
    $dateRange->setEndDate("today");

    // Create the Metrics object.
    $sessions = new Google_Service_AnalyticsReporting_Metric();
    $sessions->setExpression("ga:sessions");
    $sessions->setAlias("sessions");

    $pageviews = new Google_Service_AnalyticsReporting_Metric();
    $pageviews->setExpression("ga:pageviews");
    $pageviews->setAlias("pageviews");
    
    //Create the Dimensions
    $page = new Google_Service_AnalyticsReporting_Dimension();
    $page->setName('ga:pagePath');
        
    //Dimension filter
    $pathFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $pathFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "operator" => 'PARTIAL',
                "expressions" => array($URL)
            )
        )
        
    );
    
    $organicFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $organicFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "not" => true,
                "operator" => 'PARTIAL',
                "expressions" => array('p1=true')
            )
        )   
    );
    
    $GUAFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $GUAFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "not" => true,
                "operator" => 'PARTIAL',
                "expressions" => array('GUA=1')
            )
        )
    );
    
    $SREFilter = new Google_Service_AnalyticsReporting_DimensionFilterClause();
    $SREFilter->setFilters(
        array(
            array(
                'dimensionName' => 'ga:pagePath',
                "not" => true,
                "operator" => 'PARTIAL',
                "expressions" => array('SRE=1')
            )
        )
    );
    

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($VIEW_ID);
    $request->setDateRanges($dateRange);
    $request->setDimensions(array($page));
    $request->setDimensionFilterClauses(array($pathFilter,$organicFilter,$GUAFilter,$SREFilter));
    $request->setMetrics(array($pageviews));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    //print_r($request);
    return $analytics->reports->batchGet( $body );
}


/**
 * Parses and prints the Analytics Reporting API V4 response.
 *
 * @param An Analytics Reporting API V4 response.
 */
function printResults($reports) {
  for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
    $report = $reports[ $reportIndex ];
    $header = $report->getColumnHeader();
    $dimensionHeaders = $header->getDimensions();
    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
    $rows = $report->getData()->getRows();
    $totals = $report->getData()->getTotals();

    for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
      $row = $rows[ $rowIndex ];
      $dimensions = $row->getDimensions();
      $metrics = $row->getMetrics();
      for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
        //print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
      }

      for ($j = 0; $j < count($metrics); $j++) {
        $values = $metrics[$j]->getValues();
        for ($k = 0; $k < count($values); $k++) {
          $entry = $metricHeaders[$k];
          //print($entry->getName() . ": " . $values[$k] . "</br>");
        }
      }
    }
  }
    echo '<pre>';
    print_r($totals[0]['values'][0]);
    echo '</pre>';
}
?>