<?php
mysql_connect("localhost","root","54710033187");

mysql_select_db("deforma_posts");

$sql_posts=sprintf("SELECT * FROM post_names WHERE promoted=1 ORDER BY age DESC LIMIT 20");

$res_posts=mysql_query($sql_posts);

$posts=[];

while($row = mysql_fetch_assoc($res_posts))
{
	$posts[] = $row;
}
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
var data=[];
var metric=0;
google.load('visualization', '1', {'packages':['corechart']});

function UpdateData(){
	var time_lapse = document.getElementById("time_lapse").value;
	var post_id=document.getElementById("combo_box").value;
	$.ajax({
		url: "get_post_data.php",
		dataType: "json",
		data: {post_id: post_id, time_lapse: time_lapse},
		method: 'get',
		async: false,
		success: function(response){
			//console.log("success");
			//console.log(response);
			data=response;
			for(var g=0;g<data.length;g++)
			{
				if(parseFloat(data[g].link_cpm)>0){
					document.getElementById("table_paid").style.visibility = 'visible';
					//console.log("paid"+" "+g+" "+data[g].link_cpm);
					break;
				}
				else{
					document.getElementById("table_paid").style.visibility = 'hidden';
				}
			}
		}
	});
}

function selectPost(){
	UpdateData();
	drawChart(metric);
}

function UpdatePosts(){
	var selector=document.getElementById("combo_box");
	while(selector.length>0){
		selector.remove(0);
	}
	$.ajax({
		url: "latest_promoted_posts.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			//console.log(response);
			for(var j=0;j<response.length;j++)
			{
				var option = document.createElement("option");
				option.text = response[j].name;
				option.value = response[j].id; 
				selector.add(option);
			}
		}
	});
}
</script>
<title>Promoted Posts Performance</title>
<link href="css/dashboard.css" rel="stylesheet" type="text/css">
</head>

<body>

<select id="combo_box" onchange="selectPost()">
	<option value=""></option>
	<?php 
		foreach($posts as $selector){
			echo "<option value='{$selector['id']}'>{$selector['name']}</option>";
		}
	?>
</select>&nbsp;&nbsp;
<select id="time_lapse" onchange="selectPost()">
	<option value="2">2</option>
    <option value="6">6</option>
    <option value="12">12</option>
    <option value="24">24</option>
    <option value="36">36</option>
    <option value="48">48</option>
</select><br>

<script>
function addZero(i)
{
	if(i<10){
		i="0"+i;	
	}
	return i;
}

var formating_promoted;
function drawChart(measure){
  metric=measure;
  var data_table = new google.visualization.DataTable();
    
  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', 'Gasto');
		break;
	case 2:
		data_table.addColumn('number', 'CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', 'Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', 'Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', 'Penetration');
		break;
	case 6:
		data_table.addColumn('number', 'CXW');
		break;
	case 7:
		data_table.addColumn('number', 'CPM');
		break;
	case 8:
		data_table.addColumn('number', 'CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', 'Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', 'Shares');
		break;
	case 11:
		data_table.addColumn('number', 'Likes');
		break;
	case 12:
		data_table.addColumn('number', 'Comments');
		break;
	case 13:
		data_table.addColumn('number', 'Link CPM');
		break;
	default:
		data_table.addColumn('number', 'CTR Organico');
		break;
  }
    	
  for(var n=0;n<data.length;n++){
	data[n].record_time=data[n].record_time.replace(' ','T');
	data[n].record_time+='-05:00';
	var data_date=new Date(data[n].record_time);
	
	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data[n].spend)]);
		  formating = {
		  	title: 'Gasto',
			curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data[n].organic_ctr)/100, f: data[n].organic_ctr+'%'}]);
		  formating = {
		  	title: 'CTR Orgánico',
			curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data[n].paid_link_clicks)]);
		  formating = {
		  	title: 'Link Clicks Pagados',
			curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,parseFloat(data[n].viral_amp)]);
		  formating = {
		  	title: 'Viral AMP',
			curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data[n].penetration)/100, f: data[n].penetration}]);
		  formating = {
		  	title: 'Penetration',
			curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,parseFloat(data[n].cxw)]);
		  formating = {
		  	title: 'CXW',
			curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,parseFloat(data[n].cpm)]);
		  formating = {
		  	title: 'CPM',
			curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data[n].paid_ctr)/100, f: data[n].paid_ctr}]);
		  formating = {
		  	title: 'CTR Pagado',
			curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data[n].link_clicks)]);
		  formating = {
		  	title: 'Link Clicks',
			curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data[n].shares)]);
		  formating = {
		  	title: 'Shares',
			curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data[n].likes)]);
		  formating = {
		  	title: 'Likes',
			curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data[n].comments)]);
		  formating = {
		  	title: 'Comments',
			curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,parseFloat(data[n].link_cpm)]);
		  formating = {
		  	title: 'Link CPM',
			curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  default:
		  data_table.addRow([
		  	data_date,
		  	{v: parseFloat(data[n].organic_ctr)/100, f: data[n].organic_ctr}
		  ]);
		  formating = {
		  	title: 'CTR Orgánico',
			curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Set chart options
  /*var options = {
    'width':'100%',
    'height':'60%'
  };*/

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div'));
  table.draw(data_table,formating);
}

var refreshData = setInterval(function(){
	selectPost();
},1000*60*10);

</script>

<div id="chart_div" style="width:100%; height:400px"></div>
<table id="table_organic">
	<tr>
        <td onClick="UpdateData(); drawChart(2);">
        <span>CTR Organico</span>
        </td>
        <td onClick="UpdateData(); drawChart(9);">
        <span>Link Clicks</span>
        </td>
        <td onClick="UpdateData(); drawChart(10);">
        <span>Shares</span>
        </td>
    </tr>
    <tr>
	    <td onClick="UpdateData(); drawChart(4);">
        <span>Viral Amp</span>
        </td>
        <td></td>
        <td onClick="UpdateData(); drawChart(11);">
        <span>Likes</span>
        </td>
    </tr>
    <tr>
	    <td onClick="UpdateData(); drawChart(5);">
        <span>Penetration</span>
        </td>
        <td></td>
        <td onClick="UpdateData(); drawChart(12);">
        <span>Comments</span>
        </td>
    </tr>
</table>
<br>
<table id="table_paid">
	<tr>
		<td onClick="UpdateData(); drawChart(1);">
        <span>Gasto</span>
        </td>
        <td onClick="UpdateData(); drawChart(3);">
        <span>Link Clicks Pagados</span>
        </td>
        <td onClick="UpdateData(); drawChart(6);">
        <span>CXW</span>
        </td>
    </tr>
    <tr>
        <td onClick="UpdateData(); drawChart(7);">
        <span>CPM</span>
        </td>
        <td onClick="UpdateData(); drawChart(8);">
        <span>CTR Pagado</span>
        </td>
        <td onClick="UpdateData(); drawChart(13);">
        <span>Link CPM</span>
        </td>
    </tr>
</table>
<div id="data"></div>
</body>
</html>