<?
session_start();

require_once __DIR__ . '/vendor/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\AdUser;
use FacebookAds\Object\CustomAudience;
use FacebookAds\Object\Fields\CustomAudienceFields;
use FacebookAds\Object\Values\CustomAudienceSubtypes;

$fb = new Facebook([
  'app_id' => '957927100941199',
  'app_secret' => '65c7ab03c9e94d98c4ad2ca73295bfe8',
]);

$helper = $fb->getRedirectLoginHelper();

if (!isset($_SESSION['facebook_access_token'])) {
  $_SESSION['facebook_access_token'] = null;
}

if (!$_SESSION['facebook_access_token']) {
  $helper = $fb->getRedirectLoginHelper();
  try {
    $_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
  } catch(FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
  } catch(FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }
}

if ($_SESSION['facebook_access_token']) {
  echo "You are logged in!";

// Initialize a new Session and instantiate an Api object
Api::init(
  '957927100941199', // App ID
  '65c7ab03c9e94d98c4ad2ca73295bfe8',
  $_SESSION['facebook_access_token'] // Your user access token
);

$audience = new CustomAudience(null, 'act_31492866');
$audience->setData(array(
  CustomAudienceFields::PIXEL_ID => 286492281506212,
  CustomAudienceFields::NAME => 'My new CA',
  CustomAudienceFields::SUBTYPE => CustomAudienceSubtypes::WEBSITE,
  CustomAUdienceFields::DESCRIPTION => 'Testing',
  CustomAUdienceFields::RETENTION_DAYS => 180,
  CustomAUdienceFields::DESCRIPTION => 'Testing',
  CustomAudienceFields::RULE => array('url' => array('contains' => 'http://eldeforma.com/2016/02/09/pena-nieto')),
  CustomAudienceFields::PREFILL => true,
));
$audience->create();

 
} else {
  $permissions = ['ads_management'];
  $loginUrl = $helper->getLoginUrl('http://23.251.156.61/sdk_test.php', $permissions);
  echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
} 

?>