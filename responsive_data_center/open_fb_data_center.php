<?
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='deforma') || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
	redirect("login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Responsive Data Center - Facebook G</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var version='2.12';
    var access_token;
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {
			//getUnpublishedPosts();
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.php" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Facebook Data</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                    
                    	<div class='row'>    
							<div class="col-lg-12 col-md-12">
                        		<div class="panel panel-color panel-tabs panel-purple">
                        		                                   
                                    <!-- Modal Quickview -->
                                    <div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog" style='width: 80%'>
                                            <div class="modal-content">
                                            	<div class="card-box">
													<div class="row" id="quickview_selectors">
														<div class="form-group">
															<div class="col-sm-2">
																<select id="time_lapse_quickview" onchange="selectPostQuickview($('#quickview_post_id')[0].innerHTML,$('#quickview_title')[0].innerHTML)" class="form-control">
																	<option value="2">2 horas</option>
																	<option value="6">6 horas</option>
																	<option value="12">12 horas</option>
																	<option value="24">24 horas</option>
																	<option value="36">36 horas</option>
																	<option value="48">48 horas</option>
																	<option value="60">60 horas</option>
																	<option value="72">72 horas</option>
																	<option value="all">Lifetime</option>
																</select>
															</div>
															<div class='row'>
																<div class="col-sm-10">
																	<span id='quickview_title'></span>
																</div>
																<div class="col-sm-10">
																	<span id='quickview_post_id'></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row" id="quickview_chart">
														<div id="chart_div_quickview" style="height: 500px;" class="col-sm-8"></div>
														<div class="col-md-2">
															<div class="btn-group-vertical m-b-6">
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(2);">CTR Organico</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(9);">Link Clicks/</br>Video Views</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(10);">Shares</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(4);">Viral Amp</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(11);">Likes</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(5);">Penetration</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawChartQuickview(12);">Comments</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(1);">Gasto</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(3);">Link Clicks/</br>Video Views Pagados</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(6);">CXW</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(14);">RT CXW</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(7);">CPM</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(8);">CTR Pagado</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawChartQuickview(13);">Link CPM</button>
															</div>
														</div>
														<div class="col-md-2">
															<div class="btn-group-vertical m-b-6">
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(2);">&#916; CTR Organico</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(9);">&#916; Link Clicks/</br>Video Views</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(10);">&#916; Shares</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(4);">&#916; Viral Amp</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(11);">&#916; Likes</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(5);">&#916; Penetration</button>
																<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(12);">&#916; Comments</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(1);">&#916; Gasto</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(3);">&#916; Link Clicks/</br>Video Views Pagados</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(6);">&#916; CXW</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(14);">&#916; RT CXW</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(7);">&#916; CPM</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(8);">&#916; CTR Pagado</button>
																<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuickview(); drawDeltaChartQuickview(13);">&#916; Link CPM</button>
															</div>
														</div>
													</div>
												</div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal quickview-->
                                                                                              		
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-1" data-toggle="tab" aria-expanded="true">El Deforma</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Datacenter</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-1" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_open()">Refresh</button>
                                                        <div id="komfo_table_deforma_open" class="col-lg-12" style="height: 700px;"></div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col --><!-- Better Komfo -->
						</div>
                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        
        <!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

		<!-- Validation js (Parsleyjs) -->
        <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <script>	
			//posts query
			</script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
			<script src="//code.jquery.com/jquery-1.10.2.js"></script>
			<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			
			<script>
			jq1102 = jQuery.noConflict( true );
			//posts query	
			
			//interval calls
			var initialCalls = setTimeout(function(){
				loadKomfo_open();
				//UpdatePostsPromoted();
			},1000);
			
			var refreshData = setInterval(function(){
				loadKomfo_open();
				//selectPostPromoted();
				//getUnpublishedPosts();
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>