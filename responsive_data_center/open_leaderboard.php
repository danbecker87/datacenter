<?php
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: https://$host$path/$file");
	exit;	
}

if(isset($_COOKIE['user_id']) && ($_COOKIE['user_id']=='admin' || $_COOKIE['user_id']=='editor')){
	
}else{
	if(isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true){
		if(isset($_SESSION['admin']) && $_SESSION['admin']==true){
		
			}else{
				redirect("login.php");
			}
	}
	else{
		redirect("login.php");
		exit();
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Desempeño de las notas de los escritores del Deforma">
        <meta name="Dan Becker" content="Metricas Escritores">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>
        <script type="text/javascript" src="js/desempeno_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Leaderboard</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

		 <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="css/main.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
        
    <?php if(isset($_COOKIE['user_id']) && ($_COOKIE['user_id']=='admin' || $_COOKIE['user_id']=='editor')){ ?>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

         <!--Google Flow--><script type="text/javascript">
        google.load('visualization', '1.0', {'packages':['line','table', 'corechart','controls']});
        var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
        var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';

        function makeApiCall() {
          var params = {

            spreadsheetId: '15-wy-vmUlDNK7aD8wYXMbuG0CnuxLLQbo1xT6NxE-So',
            range: 'B3:V600',
            valueRenderOption: 'UNFORMATTED_VALUE',  
            dateTimeRenderOption: 'FORMATTED_STRING',        
            majorDimension: 'ROWS'
          };

          var request = gapi.client.sheets.spreadsheets.values.get(params);
          request.then(function(response) {
            //console.log(response.result);
            handleVentas(response.result.values);
          }, function(reason) {
            console.error('error: ' + reason.result.error.message);
          });
        }

        function initClient() {
            //console.log('in init client');    
            var API_KEY = apiKey;  

            var CLIENT_ID = clientId;  

            var SCOPE = 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/gmail.compose https://www.googleapis.com/auth/adexchange.seller https://www.googleapis.com/auth/adexchange.seller.readonly https://www.googleapis.com/auth/analytics https://www.googleapis.com/auth/analytics.readonly';

            gapi.client.init({
                'apiKey': API_KEY,
                'clientId': CLIENT_ID,
                'scope': SCOPE,
                'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4', 'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'],
            }).then(function() {
                //console.log('In thenable client init');
                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
                updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            });
        }

        function handleClientLoad() {
          gapi.load('client:auth2', initClient);
        }

        function updateSignInStatus(isSignedIn) {
          if (isSignedIn) {
            //console.log('making api call');
            makeApiCall();
          }else{
            //console.log('Not signed in');
            handleSignInClick();
          }
        }

        function handleSignInClick(event) {
          gapi.auth2.getAuthInstance().signIn();
        }

        function handleSignOutClick(event) {
          gapi.auth2.getAuthInstance().signOut();
        }
        </script><!--Google Auth Flow-->

        <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>

    <?php } ?>    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script>
    
    //Colabs
    var links;
	var posts, week_posts=[], select_posts=[], pay_weeks=[], fb_posts=[], bonus_posts=[];
	var authors={}, select_authors={}, pay_authors={};
	var week_authors={};
	var dates=[];
    
    </script>
    
    <!--FB Flow--><script>
    var access_token;
    var pageTokens = [];
    var version='2.12';
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {
            getAccessTokens();
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
        
    function getAccessTokens(){
        FB.api(
            '/me/accounts',
            'GET',
            {},
            function(response) {
                for(var i=0;i<response.data.length;i++){
                    pageTokens[response.data[i].name] = response.data[i].access_token;
                    pageTokens[response.data[i].id] = response.data[i].access_token;
                }
            }
        );
    }

    </script><!--FB Flow-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Leaderboard</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-color panel-tabs panel-success">
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-1" data-toggle="tab" aria-expanded="true">Métricas</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Escritores</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-1" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button id='authorize-button'></button>
														<label for="from">From</label>
														<input type="text" id="from" name="from">
														<label for="to">to</label>
														<input type="text" id="to" name="to">
														<button class="btn btn-success btn-rounded w-md waves-effect waves-light disabled" id="refresh_btn">Refresh</button>
														<input type='checkbox' id='collabs_only'>Remove Non-Collaborators
														<fb:login-button scope="read_insights,manage_pages" onlogin="checkLoginState();"></fb:login-button>
														<div class='row'>
															<div id="chart_div_select_posts" class="col-lg-4" style="height: 500px; width: 33.3333%">
                                                                <div id='myloader1'  style="display:none;">
                                                                    <img class="loader_logo" src="https://deforma2.com/assets/images/users/logodeforma.png"/>
                                                                    <div class='myloader'></div>
                                                                </div>
                                                            </div>
															<div id="chart_div_select_authors" class="col-lg-8" style="height: 500px; width: 66.6666%">
                                                                <div id='myloader2'  style="display:none;">
                                                                    <img class="loader_logo" src="https://deforma2.com/assets/images/users/logodeforma.png"/>
                                                                    <div class='myloader'></div>
                                                                </div>
                                                            </div>
														</div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->
                        <?php if(isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin'){?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-color panel-tabs panel-success">
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-creativos" data-toggle="tab" aria-expanded="true">Creativos</a>
											</li>
                                            <li class="">
												<a href="#navpills-ventas" data-toggle="tab" aria-expanded="true">Ventas</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Desempeño</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-creativos" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
                                                        <div class='row'>
                                                            <div class='row'>
                                                                <label for="creativos_from">From</label>
                                                                <input type="text" id="creativos_from" name="creativos_from">
                                                                <label for="creativos_to">to</label>
                                                                <input type="text" id="creativos_to" name="creativos_to">
                                                                <!--<input type='checkbox' id='creativos_collabs_only'>Remove Non-Collaborators-->
                                                                <input type='checkbox' id='creativos_percentage'>Colaboracion Relativa
                                                                <input type='checkbox' id='creativos_bar'>Gráfica de barras
                                                            </div>
                                                            <div class='row'>
                                                                <select id='creativos_kpi'>
                                                                    <option value='wp_posts'>WP Posts</option>
                                                                    <option value='fb_posts'>FB Posts</option>
                                                                    <option value='link_clicks'>Link Clicks</option>
                                                                    <option value='pageviews'>Pageviews</option>
                                                                    <option value='likes'>Likes</option>
                                                                    <option value='shares'>Shares</option>
                                                                    <option value='comments'>Comments</option>
                                                                </select>
                                                                <button class="btn btn-success btn-rounded w-md waves-effect waves-light disabled" id="creativos_refresh_btn" onclick="GraphCreativos();">2D</button>
                                                                <button class="btn btn-purple btn-rounded w-md waves-effect waves-light disabled" id="creativos_refresh_btn_3way" onclick="GraphTotalesCreativos3Way();">3D</button>
                                                                <select id='creativos_kpi_1'>
                                                                    <option value='count_wp_posts'>WP Posts</option>
                                                                    <option value='count_fb_posts'>FB Posts</option>
                                                                    <option value='link_clicks'>Link Clicks</option>
                                                                    <option value='pageviews'>Pageviews</option>
                                                                    <option value='likes'>Likes</option>
                                                                    <option value='shares'>Shares</option>
                                                                    <option value='avg_pageviews'>Avg. Pageviews</option>
                                                                    <option value='avg_shares'>Avg. Shares</option>
                                                                    <option value='avg_likes'>Avg. Likes</option>
                                                                    <option value='avg_link_clicks'>Avg. Link Clicks</option>
                                                                </select>
                                                                <select id='creativos_kpi_2'>
                                                                    <option value='count_wp_posts'>WP Posts</option>
                                                                    <option value='count_fb_posts'>FB Posts</option>
                                                                    <option value='link_clicks'>Link Clicks</option>
                                                                    <option value='pageviews'>Pageviews</option>
                                                                    <option value='likes'>Likes</option>
                                                                    <option value='shares'>Shares</option>
                                                                    <option value='comments'>Comments</option>
                                                                    <option value='avg_pageviews'>Avg. Pageviews</option>
                                                                    <option value='avg_shares'>Avg. Shares</option>
                                                                    <option value='avg_likes'>Avg. Likes</option>
                                                                    <option value='avg_link_clicks'>Avg. Link Clicks</option>
                                                                </select>
                                                                <select id='creativos_kpi_3'>
                                                                    <option value='count_wp_posts'>WP Posts</option>
                                                                    <option value='count_fb_posts'>FB Posts</option>
                                                                    <option value='link_clicks'>Link Clicks</option>
                                                                    <option value='pageviews'>Pageviews</option>
                                                                    <option value='likes'>Likes</option>
                                                                    <option value='shares'>Shares</option>
                                                                    <option value='comments'>Comments</option>
                                                                    <option value='avg_pageviews'>Avg. Pageviews</option>
                                                                    <option value='avg_shares'>Avg. Shares</option>
                                                                    <option value='avg_likes'>Avg. Likes</option>
                                                                    <option value='avg_link_clicks'>Avg. Link Clicks</option>
                                                                </select>
                                                                <button class="btn btn-primary btn-rounded w-md waves-effect waves-light disabled" id="TotalesCreativos_refresh_btn" onclick="GraphTotalesCreativos();">Totales</button>
                                                            </div>
                                                        </div>
														<div class='row' id='creativos_chart'>
															<div id="creativos_chart_div" class="col-lg-10" style="height: 700px">
                                                                <div id='myloader'  style="display:none;">
                                                                    <img class="loader_logo" src="https://deforma2.com/assets/images/users/logodeforma.png"/>
                                                                    <div class='myloader'></div>
                                                                </div>
                                                            </div>
														</div>
                                                    </div>
												</div>
											</div>
                                            <div id="navpills-ventas" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
                                                        <select id='ventas_kpi1'>
                                                            <option value='agent'>Ejecutivo</option>
                                                            <option value='status'>Status</option>
                                                            <option value='agency'>Agencia</option>
                                                            <option value='client'>Cliente</option>
                                                            <option value='sell_year'>Año</option>
                                                            <option value='sell_q'>Q</option>
                                                            <option value='sell_date'>Fecha</option>
                                                        </select>
                                                        <select id='ventas_kpi2'>
                                                            <option value='agent'>Ejecutivo</option>
                                                            <option value='status'>Status</option>
                                                            <option value='agency'>Agencia</option>
                                                            <option value='client'>Cliente</option>
                                                            <option value='sell_year'>Año</option>
                                                            <option value='sell_q'>Q</option>
                                                            <option value='sell_date'>Fecha</option>
                                                        </select>
                                                        <input type='checkbox' id='ventas_percentage'>Colaboracion Relativa
                                                        <input type='checkbox' id='ventas_bar'>Gráfica de barras
														<button class="btn btn-success btn-rounded w-md waves-effect waves-light" id="ventas_refresh_btn" onclick="GraphVentas();">Refresh</button>
														<div class='row'>
															<div id="ventas_chart_div" class="col-lg-12" style="height: 500px; width: 100%"></div>
														</div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <?}?>

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		
		<!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->
        
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script>
			jq1102 = jQuery.noConflict( true );
			jq1102(function() {
				jq1102( "#from" ).datepicker({
					defaultDate: "0",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				jq1102( "#to" ).datepicker({
					defaultDate: "+1d",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#from" ).datepicker( "option", "maxDate", selectedDate );
						$('#refresh_btn')[0].className = "btn btn-success btn-rounded w-md waves-effect waves-light";
						$('#refresh_btn')[0].onclick = function(){getOpenArticlesWPSelect()};
                        $('#refresh_btn').prop("disabled",true);
                        $('#refresh_btn').addClass("disabled");
						getOpenArticlesWPSelect();
					}
				});
                jq1102( "#creativos_from" ).datepicker({
					defaultDate: "0",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#creativos_to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				jq1102( "#creativos_to" ).datepicker({
					defaultDate: "+1d",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#creativos_from" ).datepicker( "option", "maxDate", selectedDate );
						$('#refresh_btn')[0].className = "btn btn-success btn-rounded w-md waves-effect waves-light";
						$('#refresh_btn')[0].onclick = function(){getPerformanceArticlesWPSelect()};
                        $('#creativos_refresh_btn').prop("disabled",true);
                        $('#creativos_refresh_btn').addClass("disabled");
                        $('#creativos_refresh_btn_3way').prop("disabled",true);
                        $('#creativos_refresh_btn_3way').addClass("disabled");
                        $('#TotalesCreativos_refresh_btn').prop("disabled",true);
                        $('#TotalesCreativos_refresh_btn').addClass("disabled");
                        //document.getElementById("myloader").style.display = "block";
						getPerformanceArticlesWPSelect();
					}
				});
			});
			
			//interval calls
			var initialCalls = setTimeout(function(){
				
			},3000);
			
			var refreshData = setInterval(function(){
				
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>