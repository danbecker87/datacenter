<?
session_start();

function redirect($file,$redirect_after_login){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: https://$host$path/$file?red=$redirect_after_login");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
    $tokens = ( explode ( '/', $_SERVER['PHP_SELF'] ));
	redirect("login.php",$tokens[count($tokens)-1]);
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Leaderboard</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

		 <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script>
    
    //Colabs
    var links;
	var posts, week_posts=[], select_posts=[], pay_bonus={};
	var authors={}, select_authors={}, pay_authors={};
	var week_authors={};
	var dates=[];
	
	//Bonus
	var posts, bonus_posts=[];
	var bonus_authors={};
        
    //Slack
    
    </script>
        
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart','controls']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	
    /*function makeApiCall() {
      var params = {

        spreadsheetId: '15-wy-vmUlDNK7aD8wYXMbuG0CnuxLLQbo1xT6NxE-So',
        range: 'B3:T600',
        valueRenderOption: 'UNFORMATTED_VALUE',  
        dateTimeRenderOption: 'FORMATTED_STRING',        
        majorDimension: 'ROWS'
      };

      var request = gapi.client.sheets.spreadsheets.values.get(params);
      request.then(function(response) {
        //console.log(response.result);
        handleVentas(response.result.values);
      }, function(reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }*/

    function initClient() {
		//console.log('in init client');    
		var API_KEY = apiKey;  

		var CLIENT_ID = clientId;  

		var SCOPE = 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/gmail.compose https://www.googleapis.com/auth/adexchange.seller https://www.googleapis.com/auth/adexchange.seller.readonly https://www.googleapis.com/auth/analytics https://www.googleapis.com/auth/analytics.readonly';

		gapi.client.init({
			'apiKey': API_KEY,
			'clientId': CLIENT_ID,
			'scope': SCOPE,
			'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4', 'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'],
		}).then(function() {
			//console.log('In thenable client init');
			gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
			updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
		});
    }

    function handleClientLoad() {
      gapi.load('client:auth2', initClient);
    }

    function updateSignInStatus(isSignedIn) {
      if (isSignedIn) {
      	//console.log('making api call');
        //makeApiCall();
      }else{
      	//console.log('Not signed in');
      	handleSignInClick();
      }
    }

    function handleSignInClick(event) {
      gapi.auth2.getAuthInstance().signIn();
    }

    function handleSignOutClick(event) {
      gapi.auth2.getAuthInstance().signOut();
    }
    </script><!--Google Auth Flow-->
    
    <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
    
    <!--FB Flow--><script>
    var access_token;
    var pageTokens = [];
    var version='2.12';
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {
            getAccessTokens();
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
        
    function getAccessTokens(){
        FB.api(
            '/me/accounts',
            'GET',
            {},
            function(response) {
                for(var i=0;i<response.data.length;i++){
                    pageTokens[response.data[i].name] = response.data[i].access_token;
                    pageTokens[response.data[i].id] = response.data[i].access_token;
                }
            }
        );
    }
    </script><!--FB Flow-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Leaderboard</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div id="mail-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Agencies emails</h4>
                                    </div>
                                    <div class="modal-body" id='emails-modal-body'>
                                        <!--<div class="row">
                                            <div class='col-md-10'>
                                                agency, executive, and number of items
                                            </div>
                                            <div class='col-md-2'>
                                                <button class='btn btn-rounded btn-success' onclick='snedEmail({index})'>Send</button>
                                            </div>
                                        </div>-->
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        
                        </div><!-- /.modal send Emails-->                        
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-color panel-tabs panel-success">
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-1" data-toggle="tab" aria-expanded="true">All Month posts</a>
											</li>
											<li class="">
												<a href="#navpills-2" data-toggle="tab" aria-expanded="false">Week Posts</a>
											</li>
											<li class="">
												<a href="#navpills-3" data-toggle="tab" aria-expanded="false">Select Dates</a>
											</li>
											<li class="">
												<a href="#navpills-4" data-toggle="tab" aria-expanded="false">Payments</a>
											</li>
											<li class="">
												<a href="#navpills-5" data-toggle="tab" aria-expanded="false">Bonos</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Leaderboard</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-1" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<div class='row'>
															<label for="month">Month</label>
															<input type="text" id="month" name="month">
															<input type="checkbox" id="officeWriters">Office Writers
															<input type="checkbox" id="removeNonCreativePosts">Remove IPC, Viral Videos and Memes
															<input type="checkbox" id="organic">Organic
															<button class="btn btn-success btn-rounded w-md waves-effect waves-light" id="get_bonus" onclick='getArticlesWP()'>Get Articles</button>
															<button id="authorize-button">Authorize</button>
														</div>
                                                        <div id="chart_div_posts" class="col-lg-4"></div>
                                                        <div id="chart_div_authors" class="col-lg-4"></div>
                                                        <div id="chart_div_average" class="col-lg-4"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-2" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
                                                        <div id="chart_div_week_posts" class="col-lg-4"></div>
                                                        <div id="chart_div_week_authors" class="col-lg-4"></div>
                                                        <div id="chart_div_week_average" class="col-lg-4"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-3" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<label for="from">From</label>
														<input type="text" id="from" name="from">
														<label for="to">to</label>
														<input type="text" id="to" name="to">
														<button class="btn btn-success btn-rounded w-md waves-effect waves-light disabled" id="refresh_btn">Refresh</button>
														<input type='checkbox' id='collabs_only'>Remove Non-Collaborators
                                                        <div id="chart_div_select_posts" class="col-lg-12"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-4" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<div class='row'>
															<!--<label for="pay_date">From</label>
															<input type="text" id="pay_date" name="pay_date">-->
															<!--<div class="col-md-2">
																<label class="control-label">Weeks</label>
																<input id="weeks" type="text" value="4" name="weeks" data-bts-min="1" data-bts-max="5" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-default" data-bts-button-up-class="btn btn-default"/>
															</div>-->
															<label for="pay_date">Month</label>
															<input type="text" id="pay_month" name="pay_month">
															<button class="btn btn-success btn-rounded w-md waves-effect waves-light" id="get_payments" onclick='getArticlesWPPay()'>Get Payments</button>
															<button class="btn btn-success btn-rounded w-md waves-effect waves-light" id="clear_payments" onclick='clearPayTables()'>Clear</button>
                                                            <button class='btn btn-rounded btn-danger' id="paymentEmails" data-toggle="modal" data-target="#mail-modal" style="display: none;" onclick='paymentEmails()'>Send Emails</button>
															<!--<button class="btn btn-success btn-rounded w-md waves-effect waves-light" id="clear_payments" onclick='PrintElem("#table_payments")'>Print</button>)-->
														</div>
														<div id="table_payments" class="col-lg-12"></div>
                                                        <div id="chart_div_pay_posts" class="col-lg-12"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-5" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<div class='row'>
															<label for="bonus_month">Mes </label>
															<input type="text" id="bonus_month" name="bonus_month">
															<button class="btn btn-success btn-rounded w-md waves-effect waves-light" id="get_bonus" onclick='getMonthBonus()'>Get Bonus</button>
                                                            <button class="btn btn-danger btn-rounded w-md waves-effect waves-light" id="notifySlackBonus" style="display: none;" onclick='notifySlackBonus()'>Notify by Slack</button>
                                                            <button class="btn btn-warning btn-rounded w-md waves-effect waves-light" id="printTemplate" style="display: none;" onclick='printTemplate()'>Print Template</button>
														</div>
														<div class='row'>
															<div id="chart_div_bonus_authors" class="col-lg-3"></div>
															<div id="chart_div_bonus_posts" class="col-lg-9"></div>
														</div>
														<div class='row'>
															<div id="chart_div_bonus_video_deforma" class="col-lg-4"></div>
															<div id="chart_div_bonus_video_repsodia" class="col-lg-4"></div>
															<div id="chart_div_bonus_video_cult" class="col-lg-4"></div>
														</div>
                                                        <div class='row'>
                                                            <div id="chart_div_bonus_comercial" class="col-lg-6"></div>
                                                            <div id="chart_div_bonus_totals" class="col-lg-3"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div id="chart_div_gua" class="col-lg-12"></div>
                                                        </div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>                      
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		
		<!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <!--<script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>--><!--Google API library-->
        
        <script>
        $("input[name='weeks']").TouchSpin({
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary"
		});
        </script>
        
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script>
			jq1102 = jQuery.noConflict( true );
			jq1102(function() {
				jq1102( "#from" ).datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				jq1102( "#to" ).datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#from" ).datepicker( "option", "maxDate", selectedDate );
						//console.log($('#refresh_btn'));
						$('#refresh_btn')[0].className = "btn btn-success btn-rounded w-md waves-effect waves-light";
						$('#refresh_btn')[0].onclick = function(){getArticlesWPSelect()};
						getArticlesWPSelect();
					}
				});
				jq1102( "#pay_date" ).datepicker({
					defaultDate: "-4w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						
					}
				});
			});
			jq1102(function() {
				jq1102( "#month" ).datepicker({
					defaultDate: 0,
					dateFormat: "yy-mm",
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					onChangeMonthYear: function(year, month, widget) {
						setTimeout(function() {
						   jq1102('.ui-datepicker-calendar').hide();
						});
					},
					onClose: function( dateText, inst ) {
						jq1102(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
						//getArticlesWP( $("#month")[0].value );
					}
				}).click(function(){
					jq1102('.ui-datepicker-calendar').hide();
				});
			});
			jq1102(function() {
				jq1102( "#pay_month" ).datepicker({
					defaultDate: "-1m",
					dateFormat: "yy-mm",
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					numberOfMonths: 1,
					onChangeMonthYear: function(year, month, widget) {
						setTimeout(function() {
						   jq1102('.ui-datepicker-calendar').hide();
						});
					},
					onClose: function( dateText, inst  ) {
						jq1102(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
					}
				}).click(function(){
					jq1102('.ui-datepicker-calendar').hide();
				});	
			});
			jq1102(function() {
				jq1102( "#bonus_month" ).datepicker({
					defaultDate: '-1m',
					dateFormat: "yy-mm",
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					onChangeMonthYear: function(year, month, widget) {
						setTimeout(function() {
						   jq1102('.ui-datepicker-calendar').hide();
						});
					},
					onClose: function( dateText, inst ) {
						jq1102(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
						//getArticlesWPBonus( $("#bonus_month")[0].value );
					}
				}).click(function(){
					jq1102('.ui-datepicker-calendar').hide();
				});
			});
			//$('.ui-datepicker-calendar')[0].css('display', 'none');
			
			//interval calls
			var initialCalls = setTimeout(function(){
				
			},3000);
			
			var refreshData = setInterval(function(){
				
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>