<?
session_start();

function redirect($file,$redirect_after_login){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file?red=$redirect_after_login");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) && (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
    $tokens = ( explode ( '/', $_SERVER['PHP_SELF'] ));
	redirect("login.php",$tokens[count($tokens)-1]);
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <!-- App title -->
        <title>Reporte de Video - Repsodia</title>

		<!-- Editatable  Css-->
        <link rel="stylesheet" href="assets/plugins/magnific-popup/dist/magnific-popup.css" />
        <link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />

        <!-- App CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
	var selected_posts=[];
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var access_token;
    var version = '2.12';
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {

		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
    </script><!--FB Flow-->

    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.php" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Reporte</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidebar -->
                    <? require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
									<div id="search_selectors">
										<input type="text" id="demo-input" name="blah" width="300px">
										<input type="button" class='btn btn-success' id="get_stats" value="Generar Reporte">
										<input type="button" class='btn btn-success' id="refresh" value="Recalcular reach" onclick='refreshReach();'>
										<input type="button" class='btn btn-success' id="get_stats" value="Imprimir" onclick="PrintElem('#reportTable')">
										<input type="checkbox" id="check_complete_views">Calcular % de views completos</br>
										<a href="reporte.php"><input type="button" class='btn btn-danger' value="Nuevo Reporte"></a>
										
									</div>
									<div class='table-responsive' id='reportTable'>

										<table id="mainTable" class="table table-striped table-bordered">
										
											<thead>
												<tr>
													<th colspan=2><img src='assets/images/deforma.png' width='250px' height="50px"></th>
												</tr>
												<tr>
													<th colspan=2><b>Reporte de resultados y reach por nota</b></th>
												</tr>
												<tr>
													<td>Cliente</td>
													<td>{Cliente}</td>
												</tr>
												<tr>
													<td>Nota</td>
													<td id='titulo_nota'></td>
												</tr>
												<tr>
													<td>Campaña</td>
													<td>{Campaña}</td>
												</tr>
											</thead>
																				
											<tbody>
												<tr>
													<th bgcolor='000000' colspan=2><font color='FFFFFF'>Estadísticas en el sitio</font></th>
												</tr>
												<tr>
													<td>Pageviews</td>
													<td id='pageviews'></td>
												</tr>
												<tr>
													<td>Time on page</td>
													<td id='time'></td>
												</tr>
												<tr>
													<td>Shares</td>
													<td id='site_shares'></td>
												</tr>
												<tr>
													<td>Tweets</td>
													<td id='site_tweets'></td>
												</tr>
												<tr>
													<td>Otros (mail, G+, Whatsapp, etc.)</td>
													<td id='site_whatsapps'></td>
												</tr>
												<tr>
													<td>Unique users</td>
													<td id='unique_users'></td>
												</tr>
												<tr>
													<th bgcolor='000000' colspan=2><font color='FFFFFF'>Estadísticas Facebook</font></th>
												</tr>
												<tr>
													<td>Shares</td>
													<td id='shares'></td>
												</tr>
												<tr>
													<td>Likes</td>
													<td id='likes'></td>
												</tr>
												<tr>
													<td>Comments</td>
													<td id='comments'></td>
												</tr>
												<tr>
													<td>People reach total (total de gente que vio el post en Facebook)</td>
													<td id='reach'></td>
												</tr>
												<tr>
													<td>Hide post</td>
													<td id='hide'></td>
												</tr>
												<tr>
													<td>Report spam</td>
													<td id='spam'></td>
												</tr>
												<tr>
													<th bgcolor='000000' colspan=2><font color='FFFFFF'>Estadísticas Twitter</font></th>
												</tr>
												<tr>
													<td>Retweets</td>
													<td>0</td>
												</tr>
												<tr>
													<td>Estimated reach</td>
													<td id="tweets_reach">0</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td>Estimado total de reach<sup>*</sup></td>
													<td id="total_reach"></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td colspan=2>*Se estima moderadamente que por cada share, 50 amigos de esa persona vieron el post </td>
												</tr>
											</tbody>
										
										</table>
									</div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer">
                    2016 © Adminto.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <!--<div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="zmdi zmdi-close-circle-o"></i>
                </a>
                <h4 class="">Notifications</h4>
                <div class="notification-list nicescroll">
                    <ul class="list-group list-no-border user-list">
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">Michael Zenaty</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-info">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Signup</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">5 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-pink">
                                    <i class="zmdi zmdi-comment"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Message received</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">James Anderson</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 days ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-warning">
                                    <i class="zmdi zmdi-settings"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">Settings</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>-->
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- Editable js -->
	    <script src="assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
	    <script src="assets/plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
	    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
	    <script src="assets/plugins/tiny-editable/mindmup-editabletable.js"></script>
	    <script src="assets/plugins/tiny-editable/numeric-input-example.js"></script>
		<!-- init -->
	    <script src="assets/pages/datatables.editable.init.js"></script>
	    
        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

		<script>
			$('#mainTable').editableTableWidget();//.numericInputExample().find('td:first').focus();
		</script>

		<script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

		</script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script type="text/javascript" src="js/jquery.tokeninput.js"></script>

		<link rel="stylesheet" href="css/token-input.css" type="text/css" />
		<link rel="stylesheet" href="css/token-input-facebook.css" type="text/css" />
		<script>
        
			jq1102 = jQuery.noConflict( true );
			jq1102(document).ready(function() {
				jq1102("#get_stats").click(function () {
					console.log(jq1102(this).siblings("input[type=text]").val());
					$.ajax({
						url: "php_scripts/get_all_posts_stats_Select_repsodia.php",
						data: {ids: jq1102(this).siblings("input[type=text]").val()},
						dataType: 'json',
						async: false,
						success: function(response){
							selected_posts=response;
							handleClientLoad();
							getFacebookStatsReport();
						}
					});
				});
			});
			
			$(document).ready(function() {
				$.ajax({
					url: "php_scripts/get_all_posts_Select_repsodia.php",
					dataType: "json",
					method: "get",
					success: function(response){
						jq1102("#demo-input").tokenInput(
						response,
						{
							hintText: "Escribe el título de una nota",
							noResultsText: "No hay registro de ese título",
							searchingText: "Buscando...",
							preventDuplicates: true,
						});
					}
				});
				jq1102("#plugin-methods-clear").click(function () {
					jq1102("#demo-input").tokenInput("clear");
					return false;
				});
			});
			
			var initialCalls = setTimeout(function(){
			
			},1000);
			
			var refreshData = setInterval(function(){
			
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>