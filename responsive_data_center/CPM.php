<?php
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file");
	exit;	
}

if(isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin'){
	
}else{
	if(isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true){
		if(isset($_SESSION['admin']) && $_SESSION['admin']==true){
		
			}else{
				redirect("login.php");
			}
	}
	else{
		redirect("login.php");
		exit();
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/cpm_main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - CPM</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

		 <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
     <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var version='2.10';
    var access_token;
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {
			
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">CPM</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                	<div class='row'>
										<input type="text" id="from" name="from">
										<input type="text" id="to" name="to">
									</div>
									<div class='row'>
										<div id='cpm_table' style="width:100%; height:80vh;">
                                	</div>
                                	</div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		
		<!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <script>
        $("input[name='weeks']").TouchSpin({
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary"
		});
        </script>
        
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script>
			jq1102 = jQuery.noConflict( true );
			jq1102(function() {
				jq1102( "#from" ).datepicker({
					defaultDate: "-1d",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					maxDate: 0,
					onClose: function( selectedDate ) {
						jq1102( "#to" ).datepicker( "option", "minDate", selectedDate );
					}
				});	
				jq1102( "#to" ).datepicker({
					defaultDate: "0",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					maxDate: 0,
					onClose: function( selectedDate ) {
						getCPM($('#from')[0].value,$('#to')[0].value);
					}
				});	
			});
			//$('.ui-datepicker-calendar')[0].css('display', 'none');
			
			//interval calls
			var initialCalls = setTimeout(function(){
				getCPM(new Date(new Date(new Date()-new Date().getTimezoneOffset()*60*1000).setDate(1)).toISOString().substr(0,10), new Date(new Date(new Date()-new Date().getTimezoneOffset()*60*1000)).toISOString().substr(0,10));
			},4000);
			
			var refreshData = setInterval(function(){
				
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>