<?
session_start();

function redirect($file,$redirect_after_login){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file?red=$redirect_after_login");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
    $tokens = ( explode ( '/', $_SERVER['PHP_SELF'] ));
	redirect("login.php",$tokens[count($tokens)-1]);
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/full_report_main_js.js"></script>
		<script type="text/javascript" src="js/math.min.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Full Report</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

		<!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        <!-- Sweet Alert css -->
        <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
        <!-- TODO add manifest here DONE-->
	    <link rel="manifest" href="/manifest.json">

	    <!-- Add to home screen for Safari on iOS -->
	    <meta name="apple-mobile-web-app-capable" content="no">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="apple-mobile-web-app-title" content="Deforma Datacenter">
	    <link rel="apple-touch-icon" href="assets/images/users/logodeforma_favicon_192.png">

    </head>

    <body class="fixed-left">
    
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <!--Google Flow--><script type="text/javascript">
        google.load('visualization', '1', {'packages':['line','table', 'corechart']});
        var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
        var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
        var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
        var me={'id':'eldeforma','innerHTML':'El Deforma'};
        function handleClientLoad() {
          gapi.client.setApiKey(apiKey);
          window.setTimeout(checkAuth,1);
        }
        function checkAuth() {
          gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
        }
        function handleAuthResult(authResult) {
          var authorizeButton = document.getElementById('authorize-button');
          if (authResult && !authResult.error) {

          } else {
            //authorizeButton.style.visibility = '';
            //authorizeButton.onclick = handleAuthClick;
          }
        }
        function handleAuthClick(event) {
            gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
            return false;
          }
        </script><!--Google Auth Flow-->

        <!--FB Flow--><script>
        var access_token;
        var version = '2.12';
        var url_base = "https://graph.facebook.com/v"+version+"/";
        var query = {edge: "", fields: ""};
        var url="";
        function buildURL(query,limit){
            url="";
            url+=url_base;
            url+=query.edge;
            url+="?fields=";
            url+=query.fields;
            url+="&limit="+limit;
            url+="&"+access_token;
            return url;
        }

        function statusChangeCallback(response) {
            access_token="access_token="+response.authResponse.accessToken;
            if (response.status === 'connected') {
                var cutoffTime=new Date();
                if(cutoffTime.getHours()==11 && cutoffTime.getMinutes()>=30 && cutoffTime.getMinutes()<=40){
                    //makeApiCall();
                }		
            }
            else if (response.status === 'not_authorized') {
              document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
             }
            else {
              document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
             }
        }

        function checkLoginState() {
            FB.getLoginStatus(function(response) {
              statusChangeCallback(response);
            });
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '957927100941199',
                cookie     : true,
                xfbml      : true,
                version    : 'v'+version
            });

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });

        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        </script><!--FB Flow-->

        <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->
    
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                        </ul>
                        <label for="from_report_day">Day</label>
						<input type="text" id="from_report_day" name="from_report_day">
						
						<label for="to_report_day">Day</label>
						<input type="text" id="to_report_day" name="to_report_day">

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page" style='height: 100vh'>
                <!-- Start content -->
                <div class="content" style='height: 100%'>
                    <div class="container" style='height: 100%'>
                    
                    	<div class='col-lg-4 col-md-3 scroll-column'>
                    		<div class='row'>
                    			<div class="panel panel-color panel-success m-l-5 m-r-5">
                    				<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-0">ADX & FAN Revenues (Selected Range)</h4>
									</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
                                        	<div class='inbox-item'>
												<h3 class="m-b-0">ADX MXN</h3><h3> $ <span id="adx_revenue_amount"></span> </h3>
												<h3 class="m-b-0">This Month MXN</h3><h3> $ <span id="adx_month_revenue"></span> </h3>
												<p class="text-muted m-b-0"> Last Month MXN $ <span id="adx_revenue_last_month"></span></p>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">FAN USD</h3><h3> $ <span id="FBRevenue"></span> </h3>
												<h3 class="m-b-0">This Month USD</h3><h3> $ <span id="month_FBRevenue"></span> </h3>
												<p class="text-muted m-b-0"> Last Month USD $ <span id="FBRevenue_last_month"></span></p>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">ADMAN USD</h3><h3> $ <span id="admanRevenue"></span> </h3>
												<h3 class="m-b-0">This Month USD</h3><h3> $ <span id="month_admanRevenue"></span> </h3>
												<p class="text-muted m-b-0"> Last Month USD $ <span id="admanRevenue_last_month"></span></p>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">Taboola USD</h3><h3> $ <span id="taboolaRevenue"></span> </h3>
												<h3 class="m-b-0">This Month USD</h3><h3> $ <span id="month_taboolaRevenue"></span> </h3>
												<p class="text-muted m-b-0"> Last Month USD $ <span id="taboolaRevenue_last_month"></span></p>
											</div>
                                            <div class='inbox-item'>
                                            	<button class='btn btn-success waves-effect waves-light btn-sm pull-right' onclick='totalRevenue()'>Refetch Total</button>
												<h3 class="m-b-0">TOTAL MXN $ <span id="totalRevenue"></span> </h3>
												<h3 class="m-b-0">This Month MXN $ <span id="month_totalRevenue"></span> </h3>
												<p class="text-muted m-b-0"> Last Month MXN $ <span id="totalRevenue_last_month"></span></p>
											</div>
										</div>
									</div>
                        		</div>
                    		</div><!-- Revenue --> 
                    		
                    		<div class='row'>
                    			<div class="panel panel-color panel-warning m-l-5 m-r-5">
                        			<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-10">Analytics</h4>
									</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
                                        	<div class='inbox-item'>
												<h3 class="m-b-0">Pageviews (Range) </h3><h3><span id="pageviews_range"> - </span></h3>
												<h3 class="m-b-0">Pageviews Month</h3><h3> <span id="pageviews_month"> - </span></h3>
												<p class="text-muted m-b-0">Pageviews Last Month <span id="pageviews_last_month"> - </span></p></br>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">Users (Range) </h3><h3><span id="users_range"> - </span></h3>
												<h3 class="m-b-0">Users Month </h3><h3><span id="users_month"> - </span></h3>
												<p class="text-muted m-b-0">Users Last Month <span id="users_last_month"> - </span></p>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- Analytics --> 
                    		
                    		<div class='row'>
                    			<div class="panel panel-color panel-primary m-l-5 m-r-5">
                        			<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-0">Reach FB</h4>
                        			</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
                                        	<div class='inbox-item'>
												<h3 class="m-b-0">Range Reach (Statistical)</h3>
												<h4>Average <span id="mean_reach"> - </span></h4>
												<h4>Max <span id="max_reach"> - </span></h4>
												<h4>Min <span id="min_reach"> - </span></h4>
												<h4>Reach SD <span id="sd_reach"> - </span></h4>
												<h3 class="m-b-0">Month Reach <span id="reach_month"> - </span></h3>
												<p class="text-muted m-b-0">Reach Last Month <span id="reach_last_month"> - </span></p>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- Reach --> 
                    	</div>
                    	
                    	<div class='col-lg-4 col-md-4 scroll-column'>
							<div class='row'>
								<div class="panel panel-color panel-primary m-l-5 m-r-5">
									<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-10">Month Shares</h4>
									</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2  inbox-widget">
											<div class='inbox-item'>
												<h3 class="m-b-0">Shares Range <span id="shares_amount"></span> </h3>
												<h3 class="m-b-0">Shares Month <span id="shares_month"></span> </h3>
												<p class="text-muted m-b-5">Last month shares <span id="shares_last_month"></span></p>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- Shares --> 
                    		
                    		<div class='row'>
                    			<div class="panel panel-color panel-primary m-l-5 m-r-5">
                    				<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-0">FB Spend (Month)</h4>
									</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
                                        	<div class='inbox-item'>
												<h2 class="m-b-0">Content</h2>
												<h3 class="m-b-0">Range USD </h3><h3>$ <span id="yesterday_fb_spend_content"></span> </h3>
												<h3 class="m-b-0">This Month USD </h3><h3>$ <span id="fb_spend_content"></span> </h3>
												<p class="text-muted m-b-0">Last Month USD $ <span id="fb_spend_content_last_month"></span></p>
											</div>
											<div class='inbox-item'>
												<h2 class="m-b-0">Growth</h2>											
												<h3 class="m-b-0"> Range MXN </h3><h3>$ <span id="yesterday_fb_spend_growth"></span> </h3>
												<h3 class="m-b-0"> This Month MXN </h3><h3>$ <span id="fb_spend_growth"></span> </h3>
												<p class="text-muted m-b-0"> Last Month MXN $ <span id="fb_spend_growth_last_month"></span></p>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- FB Spend --> 
                    		
                    		<div class='row'>
                    			<div class="panel panel-color panel-primary m-l-5 m-r-5">
                    				<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-10">Video Views</h4>
									</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
											<div class='inbox-item'>
												<h3 class="m-b-0">Deforma VV (Range)</h3><h3> <span id="range_deforma_video_views_amount"></span> </h3>
												<h3 class="m-b-0">Deforma VV (Month)</h3><h3> <span id="deforma_video_views_amount"></span> </h3>
												<p class="text-muted m-b-5">Deforma Last Month VV <span id="deforma_video_views_last_month"></span></p>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">Repsodia VV (Range) </h3><h3><span id="range_repsodia_video_views_amount"></span> </h3>
												<h3 class="m-b-0">Repsodia VV (Month) </h3><h3><span id="repsodia_video_views_amount"></span> </h3>
												<p class="text-muted m-b-5">Repdosia Last Month VV <span id="repsodia_video_views_last_month"></span></p>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">TechCult VV (Range) </h3><h3><span id="range_techcult_video_views_amount"></span> </h3>
												<h3 class="m-b-0">TechCult VV (Month) </h3><h3><span id="techcult_video_views_amount"></span> </h3>
												<p class="text-muted m-b-5">TechCult Last Month VV <span id="techcult_video_views_last_month"></span></p>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- VV --> 
                    	</div>
                    	
                    	<div class='col-lg-4 col-md-4 scroll-column'>
							<div class='row'>
								<div class="panel panel-color panel-success m-l-5 m-r-5">
									<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-0">ADX & FAN Metrics</h4>
                        			</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
                                        	<!-- ADX -->
                                        	<div class='row'>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">ADX CPM (Range)</h3><h3>$<span id="ADX_CPM"></span></h3>
													<h3 class="p-t-0 m-b-5">ADX CPM (Month)</h3><h3>$<span id="month_ADX_CPM"></span></h3>
													<p class="text-muted m-b-0">Last Month ADX CPM $<span id="last_month_ADX_CPM"></span> </p>
												</div>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">ADX CTR (Range)</h3><h3> %<span id="ADX_CTR"></span></h3>
													<h3 class="p-t-0 m-b-5">ADX CTR (Month) </h3><h3>%<span id="month_ADX_CTR"></span></h3>
													<p class="text-muted m-b-0">Last Month ADX CTR %<span id="last_month_ADX_CTR"></span> </p>
												</div>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">ADX Ad impressions (Range)</h3><h3> <span id="ADX_ads"></span></h3>
													<h3 class="p-t-0 m-b-5">ADX Ad impressions (Month)</h3><h3> <span id="month_ADX_ads"></span></h3>
													<p class="text-muted m-b-0">Last Month Ad impressions <span id="last_month_ADX_ads"></span> </p>
												</div>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">ADX Coverage (Range)</h3><h3> %<span id="ADX_coverage"></span></h3>
													<h3 class="p-t-0 m-b-5">ADX Coverage (Month)</h3><h3> %<span id="month_ADX_coverage"></span></h3>
													<p class="text-muted m-b-0">Last Month Coverage %<span id="last_month_ADX_coverage"></span> </p>
												</div>
											</div>
                                            <!-- FAN -->
                                            <div class='row'>
                                            	<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">FAN CPM (Range)</h3><h3> $<span id="FAN_CPM"></span></h3>
													<h3 class="p-t-0 m-b-5">FAN CPM (Month)</h3><h3> $<span id="month_FAN_CPM"></span></h3>
													<p class="text-muted m-b-0">Last Month FAN CPM $<span id="last_month_FAN_CPM"></span> </p>
												</div>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">FAN CTR (Range)</h3><h3> %<span id="FAN_CTR"></span></h3>
													<h3 class="p-t-0 m-b-5">FAN CTR (Month)</h3><h3> %<span id="month_FAN_CTR"></span></h3>
													<p class="text-muted m-b-0">Last Month FAN CTR %<span id="last_month_FAN_CTR"></span> </p>
												</div>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">FAN Ad impressions (Range)</h3><h3> <span id="FAN_ads"></span></h3>
													<h3 class="p-t-0 m-b-5">FAN Ad impressions (Month)</h3><h3> <span id="month_FAN_ads"></span></h3>
													<p class="text-muted m-b-0">Last Month Ad impressions <span id="last_month_FAN_ads"></span> </p>
												</div>
												<div class='inbox-item'>
													<h3 class="p-t-0 m-b-5">FAN Coverage (Range)</h3><h3> %<span id="FAN_coverage"></span></h3>
													<h3 class="p-t-0 m-b-5">FAN Coverage (Month)</h3><h3> %<span id="month_FAN_coverage"></span></h3>
													<p class="text-muted m-b-0">Last Month Coverage %<span id="last_month_FAN_coverage"></span> </p>
												</div>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- ADX & FAN Metrics --> 
                    		
                    		<div class='row'>
                    			<div class="panel panel-color panel-inverse m-l-5 m-r-5">                                       
                    				<div class='panel-heading'>
										<h4 class="panel-title m-t-0 m-b-0">Preferred Deals & Private Auctions</h4>
									</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
											<div class='inbox-item'>
												<h3 class="p-t-0 m-b-5">Preferred Deals revenue (Range)</h3><h3> $<span id="preferred_ADX_revenue"></span></h3>
												<h3 class="p-t-0 m-b-5">Preferred Deals revenue (Month)</h3><h3> $<span id="preferred_month_ADX_revenue"></span></h3>
												<p class="text-muted m-b-0">Last Month Preferred Deals revenue $<span id="preferred_last_month_ADX_revenue"></span> </p>
											</div>
											<div class='inbox-item'>                                            
												<h3 class="p-t-0 m-b-5">Preferred Deals CPM (Range)</h3><h3> $<span id="preferred_ADX_CPM"></span></h3>
												<h3 class="p-t-0 m-b-5">Preferred Deals CPM (Month)</h3><h3> $<span id="preferred_month_ADX_CPM"></span></h3>
												<p class="text-muted m-b-0">Last Month Preferred Deals CPM $<span id="preferred_last_month_ADX_CPM"></span> </p>
											</div>
											<div class='inbox-item'>                                            
												<h3 class="p-t-0 m-b-5">Preferred Deals Impressions (Range)</h3><h3> <span id="preferred_ADX_ads"></span></h3>
												<h3 class="p-t-0 m-b-5">Preferred Deals Impressions (Month)</h3><h3> <span id="preferred_month_ADX_ads"></span></h3>
												<p class="text-muted m-b-0">Last Month Preferred Deals Impressions <span id="preferred_last_month_ADX_ads"></span> </p>
											</div>
											<div class='inbox-item'>                                            
												<h3 class="p-t-0 m-b-5">Preferred Deals Coverage (Range)</h3><h3> %<span id="preferred_ADX_coverage"></span></h3>
												<h3 class="p-t-0 m-b-5">Preferred Deals Coverage (Month)</h3><h3> %<span id="preferred_month_ADX_coverage"></span></h3>
												<p class="text-muted m-b-0">Last Month Preferred Deals Coverage %<span id="preferred_last_month_ADX_coverage"></span> </p>
											</div>
											<div class='inbox-item'>                                            
												<h3 class="p-t-0 m-b-5">Private Auctions (Range)</h3><h3> $<span id="private_ADX_revenue"></span></h3>
												<h3 class="p-t-0 m-b-5">Private Auctions revenue (Month)</h3><h3> $<span id="private_month_ADX_revenue"></span></h3>
												<p class="text-muted m-b-0">Last Month Private Auctions revenue $<span id="private_last_month_ADX_revenue"></span> </p>
											</div>
											<div class='inbox-item'>                                            
												<h3 class="p-t-0 m-b-5">Private Auctions CPM (Range)</h3><h3> $<span id="private_ADX_CPM"></span></h3>
												<h3 class="p-t-0 m-b-5">Private Auctions CPM (Month) </h3><h3>$<span id="private_month_ADX_CPM"></span></h3>
												<p class="text-muted m-b-0">Last Month Private Auctions CPM $<span id="private_last_month_ADX_CPM"></span> </p>
											</div>
											<div class='inbox-item'>
												<h3 class="p-t-0 m-b-5">Private Auctions Impressions (Range)</h3><h3> <span id="private_ADX_ads"></span></h3>
												<h3 class="p-t-0 m-b-5">Private Auctions Impressions (Month)</h3><h3> <span id="private_month_ADX_ads"></span></h3>
												<p class="text-muted m-b-0">Last Month Private Auctions Impressions <span id="private_last_month_ADX_ads"></span> </p>
											</div>
											<div class='inbox-item'>                                            
												<h3 class="p-t-0 m-b-5">Private Auctions coverage (Range)</h3><h3> %<span id="private_ADX_coverage"></span></h3>
												<h3 class="p-t-0 m-b-5">Private Auctions coverage (Month)</h3><h3> %<span id="private_month_ADX_coverage"></span></h3>
												<p class="text-muted m-b-0">Last Month Private Auctions coverage %<span id="private_last_month_ADX_coverage"></span> </p>
											</div>
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- Private and Preferred deals --> 
                    		
                    		<div class='row'>
                    			<div class="panel panel-color panel-purple m-l-5 m-r-5">
                    				<div class='panel-heading'>
                    					<h4 class="panel-title m-t-0 m-b-10">Posts and Publications (Selected Range)</h4>
                    				</div>

                                    <div class="panel-body">
                                        <div class="widget-detail-2 inbox-widget">
											<div class='inbox-item'>                                   	
												<h3 class="m-b-0">WP Publications (Range) <span id="WP_publications"> - </span></h3>
												<h3 class="m-b-0">WP Publications (Month) <span id="WP_month_publications"> - </span></h3>
												<p class="text-muted m-b-0">Last Month WP Publications <span id="WP_last_month_publications"></span> </p>
											</div>
											<div class='inbox-item'>
												<h3 class="m-b-0">FB Posts (Range) <span id="FB_posts"> - </span></h3>
												<h3 class="m-b-0">FB Posts (Month) <span id="FB_month_posts"> - </span></h3>
												<p class="text-muted m-b-0">Last Month FB Posts <span id="FB_last_month_posts"></span> </p>
											</div>
                                            <div class='inbox-item'>
												<div id='FB_authors'></div>
											</div>
											<!--<div class='inbox-item'>
												<div id='posts'></div>
											</div>-->
                                        </div>
                                    </div>
                        		</div>
                    		</div><!-- WP, FB publications and Authors --> 
                    	</div>
                    	
                    </div> <!-- container --> 
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->
        
        <!-- Sweet Alert js -->
        <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        
        <script>
        	jq1102 = jQuery.noConflict( true );
			jq1102(function() {
				jq1102( "#from_report_day" ).datepicker({
					defaultDate: "-1d",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					maxDate: 0,
					onClose: function( selectedDate ) {
						jq1102( "#to_report_day" ).datepicker( "option", "minDate", selectedDate );
					}
				});	
				jq1102( "#to_report_day" ).datepicker({
					defaultDate: "0",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					maxDate: -1,
					onClose: function( selectedDate ) {
						getAllMetrics($('#from_report_day')[0].value,$('#to_report_day')[0].value);
					}
				});	
			});
        
			//inital calls
			var initialCallLive = setTimeout(function(){
				yesterday=new Date();
				yesterday.setTime(yesterday.getTime()-yesterday.getTimezoneOffset()*60*1000-24*60*60*1000);
				today=new Date();
				today.setTime(today.getTime()-today.getTimezoneOffset()*60*1000);
				
				getNetADXRevenue(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				getPrivateAuctionADXRevenue(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				getPreferredDealsADXRevenue(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				getFBRevenue(yesterday.toISOString().substr(0,10)+'T0'+(yesterday.getTimezoneOffset()/60+3)+':00:00',yesterday.toISOString().substr(0,10)+'T0'+(yesterday.getTimezoneOffset()/60+3)+':00:00');
				getMonthsFBRevenue();
				getMonthADXRevenue();
				getFBReach();
				getFBReachRange(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				monthShares();
				rangeShares(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				getFBSpend();
				monthVideoViews();
				rangeVideoViews('yesterday','today');
				getADXMonthMetrics();
				getMonthsFANMetrics();
				getFANMetrics(yesterday.toISOString().substr(0,10)+'T0'+(yesterday.getTimezoneOffset()/60+3)+':00:00',yesterday.toISOString().substr(0,10)+'T0'+(yesterday.getTimezoneOffset()/60+3)+':00:00');
				getPrivateAuctionMonthADXMetrics();
				getPreferredDealsMonthADXMetrics();
				getAudienceMetrics(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				getAudienceMonthMetrics();
				WPMonthsPublications();
				FBMonthsPosts();
				FBLastMonthsPosts();
				WPPublications(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				FBPosts(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				getOpenArticlesWPSelect(yesterday.toISOString().substr(0,10),today.toISOString().substr(0,10));
				getMonthTaboolaRevenue();
				getTaboolaRevenue(yesterday.toISOString().substr(0,10),yesterday.toISOString().substr(0,10));
				admanRevenue(yesterday.toISOString().substr(0,10),today.toISOString().substr(0,10));
				admanRevenueMonth();
			},3000);//initial calls
			//},3000*24*60*60*1000);//initial calls
			
			function getAllMetrics(since,until){
				getNetADXRevenue(since,until);
				getPrivateAuctionADXRevenue(since,until);
				getPreferredDealsADXRevenue(since,until);
				getFBRevenue(since+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00',until+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00');
				getFBSpendRange(since,until);
				getTaboolaRevenue(since,until);
				admanRevenue(since,until);
				getFANMetrics(since+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00',until+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00');
				getFBReachRange(since+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00',until+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00');
				getAudienceMetrics(since,until);
				WPPublications(since,until);
				FBPosts(since,until);
				getOpenArticlesWPSelect(since,until);
				rangeVideoViews(since,until);
				rangeShares(since,until);
			}
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>
