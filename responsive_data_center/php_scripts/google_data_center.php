<?
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='deforma') || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
	redirect("login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma.png">

        <title>Deforma - Responsive Data Center - Start</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->
		
		<!-- Bootstrap Date picker css -->
        <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var access_token;
	var url_base = "https://graph.facebook.com/v2.5/";
	var query = {edge: "", fields: ""};
	var url="";
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {

		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v2.5'
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Google Data</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/logodeforma.png" alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive">
                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                        </div>
                        <h5><a href="index.html">El Deforma</a> </h5>

                    </div>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                                                
                        <div class="row">
                            <div class="col-lg-12">
								<div class="card-box">
                                    <div class="btn-group pull-right">
                                        <input type="text" class="" id="active_datepicker"></input>
                                    </div>

                                    <h4 class="header-title m-b-30">Active Users</h4>

                                    <div class="row">
                                        <div class="col-md-12">
											<div id="chart_div_active" style="height: 500px;" class="col-sm-12">
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        
        <!-- Bootstrap Datepicker -->
        <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        
        

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
		<script src="assets/js/jquery.app.js"></script>
       
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
       
        <!-- Page js -->
        <script type="text/javascript">
			jq1102 = jQuery.noConflict( true );
        	var date = new Date();
        	date.setTime(date.getTime()-1000*60*60*5);
        	date = date.toISOString().replace(/-/g,'').substring(0,8);
        	var active_timeout = setTimeout(function(){ getActiveUsersHistory(date); console.log('here');}, 1);
        	var active_interval = setInterval(function(){ getActiveUsersHistory(date); }, 1000*60*3);
        
            // Date Picker
            jq1102('#active_datepicker').datepicker({
			  defaultDate: "+1w",
			  dateFormat: "yy-mm-dd",
			  changeMonth: true,
			  maxDate: 0,
			  minDate: new Date('2016-04-09'),
			  numberOfMonths: 1,
			  onClose: function( selectedDate ) {
			    date = active_datepicker.value.replace(/-/g,'');
			    active_interval = setTimeout(function(){ getActiveUsersHistory(date) }, 1);
			  }
			});
        </script>
        
        <script>	
			//posts query

			function queryAnalytics(post){
				post.link = post.link.replace('http://eldeforma.com','');
				if(post.link.indexOf('?utm')!=-1){
					post.link = post.link.substring(0,post.link.indexOf('?utm'));
				}
				var date= new Date();
				var month = date.getMonth()>=9 ? (date.getMonth()+1) : "0"+(date.getMonth()+1);
				var day = date.getDate()>=10 ? (date.getDate()) : "0"+(date.getDate());
				gapi.client.load('analytics','v3', function(){
					var requestPageviews = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:pageviews",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestPageviews.execute(function(resp){
						//console.log(resp);
						post.pageviews = resp.totalsForAllResults["ga:pageviews"];
						document.getElementById("site_pageviews").innerHTML = numberWithCommas(post.pageviews);
						var requestSocial = gapi.client.analytics.data.ga.get({
							ids: "ga:41142760",
							"start-date": "365daysAgo",
							"end-date": "today",
							metrics: "ga:totalEvents",
							dimensions: "ga:eventAction",
							fields: "rows",
							output: 'json',
							filters: "ga:eventLabel=@"+post.link
						});
						requestSocial.execute(function(resp){
							post.site_shares = 0;
							post.site_tweets = 0
							post.site_whatsapp = 0;
							for(var l=0;l<resp.rows.length;l++){
								switch(resp.rows[l][0])
								{
									case "Facebook Share Class":
										post.site_shares = resp.rows[l][1];
										break;
									case "Twitter Tweet Class":
										post.site_tweets = resp.rows[l][1];
										break;
									case "WhatsApp Message Class":
										post.site_whatsapp = resp.rows[l][1];
										break;
									default:
										break;
								}
							}
							document.getElementById("site_shares").innerHTML = numberWithCommas(post.site_shares);
							document.getElementById("site_tweets").innerHTML = numberWithCommas(post.site_tweets);
							document.getElementById("site_whatsapp").innerHTML = numberWithCommas(post.site_whatsapp);
							document.getElementById("site_shares_per_pageview").innerHTML = parseFloat(((parseFloat(post.site_shares)/parseFloat(post.pageviews)*100))).toFixed(4)+'%';
							//console.log(post);
						});
					});
					var requestUsers = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:users",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestUsers.execute(function(resp){
						//console.log(resp);
						post.users = resp.totalsForAllResults["ga:users"];
						document.getElementById("site_users").innerHTML = numberWithCommas(post.users);	
					});
					var requestTime = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:avgTimeOnPage",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestTime.execute(function(resp){
						//console.log(resp);
						post.time_on_page = resp.totalsForAllResults["ga:avgTimeOnPage"];
						document.getElementById("time_on_page").innerHTML = Math.floor(parseFloat(post.time_on_page)/60)+":"+getSeconds(parseFloat(post.time_on_page));
					});
				});
				//console.log(post);	
			}
			//report
			
			//interval calls
			var initialCalls = setTimeout(function(){

			},1000);
			
			var refreshData = setInterval(function(){

			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				//checkLoginState();
			},1000*60*30);
        </script>

    </body>
</html>