<?
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
	redirect("login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma.png">

        <title>Deforma - Responsive Data Center - Start</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var access_token;
	var url_base = "https://graph.facebook.com/v2.5/";
	var query = {edge: "", fields: ""};
	var url="";
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {

		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v2.5'
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Facebook Data</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/logodeforma.png" alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive">
                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                        </div>
                        <h5><a href="index.html">El Deforma</a> </h5>

                    </div>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
						<div class="row">
							<div class="col-lg-12">
                                <div class="card-box" id="komfo">

                        			<h4 class="header-title m-t-0 m-b-30">Better Komfo</h4>

                                    <div id="komfo_table" style="height: 300px;"></div>

                        		</div>
                            </div><!-- end col-->
                        
                        </div>
                        <!-- end row Komfo-->
                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-color panel-tabs panel-purple">
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-1" data-toggle="tab" aria-expanded="true">Query</a>
											</li>
											<li class="">
												<a href="#navpills-2" data-toggle="tab" aria-expanded="false">Promoted</a>
											</li>
											<li class="">
												<a href="#navpills-3" data-toggle="tab" aria-expanded="false">Latest</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Facebook Posts - Data Center</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-1" class="tab-pane fade in active">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="query_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="select_posts" id="select_posts_label">Posts: </label>
																	<div class="col-sm-9">
																		<input id="select_posts" class="ui-widget form-control">
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_query" onchange="selectPostQuery()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="36">36 horas</option>
																			<option value="48">48 horas</option>
																			<option value="60">60 horas</option>
																			<option value="72">72 horas</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="query_chart">
																<div id="chart_div_query" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(2);">CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(9);">Link Clicks/</br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(10);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(11);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(5);">Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(12);">Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(1);">Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(3);">Link Clicks/</br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(6);">CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(14);">RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(7);">CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(8);">CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(13);">Link CPM</button>
																	</div>
																</div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(2);">&#916; CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(9);">&#916; Link Clicks/</br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(10);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(11);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(5);">&#916; Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(12);">&#916; Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(1);">&#916; Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(3);">&#916; Link Clicks/</br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(6);">&#916; CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(14);">&#916; RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(7);">&#916; CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(8);">&#916; CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(13);">&#916; Link CPM</button>
																	</div>
																</div>
															</div>
		
															<!-- Metricas -->
															<div class="row">
																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30">Site Statistics</h4>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-primary" id="site_pageviews">-</span>
																			Pageviews:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-primary" id="time_on_page">-</span>
																			Time on Page:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="site_shares">-</span>
																			Shares: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="site_tweets">-</span>
																			Tweets: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-success" id="site_whatsapp">-</span>
																			Whatsapps: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-warning" id="site_users">-</span>
																			Unique Users: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-inverse" id="site_shares_per_pageview">-</span>
																			Shares/Pageviews: 
																		</li>
																	</ul>
																</div>
																<!-- end col -->

																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30" id="post_id">Facebook Statistics</h4>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_shares">-</span>
																			Shares:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_likes">-</span>
																			Likes:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_comments">-</span>
																			Comments: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="fb_reach">-</span>
																			Reach: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-warning" id="fb_hide">-</span>
																			Hide post: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-danger" id="fb_spam">-</span>
																			Spam: 
																		</li>
																	</ul>
																</div>
																<!-- end col -->
															</div>
															<!-- end row -->

														</div>
	
													</div><!-- end col-->
												</div>
											</div>
											<div id="navpills-2" class="tab-pane fade">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="promoted_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="promoted_posts" id="promoted_posts_label">Posts: </label>
																	<div class="col-sm-9">
																		<select class="form-control" id="combo_box_promoted" onchange="selectPostPromoted();" onclick="UpdatePostsPromoted();">
																		</select>
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_promoted" onchange="selectPostPromoted()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="36">36 horas</option>
																			<option value="48">48 horas</option>
																			<option value="60">60 horas</option>
																			<option value="72">72 horas</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="promoted_chart">
																<div id="chart_div_promoted" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(2);">CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(9);">Link Clicks/</br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(10);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(11);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(5);">Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(12);">Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(1);">Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(3);">Link Clicks/</br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(6);">CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(14);">RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(7);">CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(8);">CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(13);">Link CPM</button>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(2);">&#916; CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(9);">&#916; Link Clicks/</br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(10);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(11);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(5);">&#916; Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(12);">&#916; Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(1);">&#916; Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(3);">&#916; Link Clicks/</br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(6);">&#916; CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(14);">&#916; RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(7);">&#916; CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(8);">&#916; CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(13);">&#916; Link CPM</button>
																	</div>
																</div>
															</div>

														</div>
													</div><!-- end col-->
												</div>
											</div>
											<div id="navpills-3" class="tab-pane fade">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="latest_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="combo_box_latest" id="latest_posts_label">Posts: </label>
																	<div class="col-sm-9">
																		<select class="form-control" id="combo_box_latest" onchange="selectPostDashboard();" onclick="UpdatePostsDashboard();">
																		</select>
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_latest" onchange="selectPostDashboard()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="36">36 horas</option>
																			<option value="48">48 horas</option>
																			<option value="60">60 horas</option>
																			<option value="72">72 horas</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="latest_chart">
																<div id="chart_div_latest" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(2);">CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(9);">Link Clicks/</br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(10);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(11);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(5);">Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(12);">Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(1);">Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(3);">Link Clicks/</br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(6);">CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(14);">RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(7);">CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(8);">CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(13);">Link CPM</button>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(2);">&#916; CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(9);">&#916; Link Clicks/</br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(10);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(11);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(5);">&#916; Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(12);">&#916; Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(1);">&#916; Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(3);">&#916; Link Clicks/</br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(6);">&#916; CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(14);">&#916; RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(7);">&#916; CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(8);">&#916; CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(13);">&#916; Link CPM</button>
																	</div>
																</div>
															</div>

														</div>
													</div><!-- end col-->
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <script>	
			//posts query
			</script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
			<script src="//code.jquery.com/jquery-1.10.2.js"></script>
			<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			
			<script>	
			jq1102 = jQuery.noConflict( true );
			var data_query=[];
			var metric_query=0;
			var formating_query;
			var post_id_query;
			var query_currency;
			var delta_link_clicks=[];
			var post;

			jq1102(function() {
				var select_posts=[];
				$.ajax({
					url: "php_scripts/get_all_post_names.php",
					dataType: 'json',
					method: 'get',
					async: false,
					success: function(response){
						select_posts=response;
						//console.log(select_posts);
					}
				});
 
				jq1102( "#select_posts" ).autocomplete({
				  minLength: 0,
				  source: select_posts,
				  focus: function( event, ui ) {
					jq1102( "#select_posts" ).val( ui.item.label );
					return false;
				  },
				  select: function( event, ui ) {
					jq1102( "#select_posts" ).val( ui.item.label );
					//$( "#select_value" ).val( ui.item.value );
					post_id_query = ui.item.value;
					//console.log(post_id_query);
					var stats = document.getElementsByClassName("stats");
					for(var m=0;m<stats.length;m++)
					{
						stats.item(m).innerHTML = '';
					}
					//checkLoginState(post_id_query);
					getReportStats(post_id_query);
					selectPostQuery();
					return false;
				  }
				});
			});
			function UpdateDataQuery(){
				var time_lapse = document.getElementById("time_lapse_query").value;
				//var post_id=document.getElementById("combo_box_promoted").value;
				$.ajax({
					url: "php_scripts/get_post_data.php",
					dataType: "json",
					data: {post_id: post_id_query, time_lapse: time_lapse},
					method: 'get',
					async: false,
					success: function(response){
						data_query=response;
						for(var g=0;g<data_query.length;g++)
						{
							if(parseFloat(data_query[g].spend)>0){
								var paid_cels = document.getElementsByClassName("paid");
								for(var i=0;i<paid_cels.length;i++){
									paid_cels.item(i).style.visibility = "visible";
								}
								break;
							}
							else{
								var paid_cels = document.getElementsByClassName("paid");
								for(var i=0;i<paid_cels.length;i++){
									paid_cels.item(i).style.visibility = "hidden";
								}
							}

						}
					}
				});	
				$.ajax({
					url: "php_scripts/get_post_currency.php",
					dataType: "text",
					data: {post_id: post_id_query},
					method: 'get',
					async: false,
					success: function(response){
						query_currency=response;
					}
				});
			}
			function selectPostQuery(){
				//console.log(post_id_query);
				UpdateDataQuery();
				drawChartQuery(metric_query);
			}
			function drawChartQuery(measure){
			  metric_query=measure;
			  var data_table = new google.visualization.DataTable();
			  var currency_factor;
  
			  if(query_currency=="MXN"){
				currency_factor=1/17;
			  }else{
				currency_factor=1;
			  }
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 1:
					data_table.addColumn('number', 'Gasto');
					break;
				case 2:
					data_table.addColumn('number', 'CTR Organico');
					break;
				case 3:
					data_table.addColumn('number', 'Link Clicks Pagados');
					break;
				case 4:
					data_table.addColumn('number', 'Viral AMP');
					break;
				case 5:
					data_table.addColumn('number', 'Penetration');
					break;
				case 6:
					data_table.addColumn('number', 'CXW');
					break;
				case 7:
					data_table.addColumn('number', 'CPM');
					break;
				case 8:
					data_table.addColumn('number', 'CTR Pagado');
					break;
				case 9:
					data_table.addColumn('number', 'Link Clicks');
					break;
				case 10:
					data_table.addColumn('number', 'Shares');
					break;
				case 11:
					data_table.addColumn('number', 'Likes');
					break;
				case 12:
					data_table.addColumn('number', 'Comments');
					break;
				case 13:
					data_table.addColumn('number', 'Link CPM');
					break;
				case 14:
					data_table.addColumn('number', 'RT CXW');
					break;
				default:
					data_table.addColumn('number', 'Link Clicks');
					break;
			  }
  
			  var formating;
			  for(var n=0;n<data_query.length;n++){
				data_query[n].record_time=data_query[n].record_time.replace(' ','T');
				data_query[n].record_time+='-05:00';
				var data_date=new Date(data_query[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].spend)*currency_factor]);
					  formating = {
						title: 'Gasto',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].organic_ctr)/100, f: data_query[n].organic_ctr+'%'}]);
					  formating = {
						title: 'CTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 3:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].paid_link_clicks)]);
					  formating = {
						title: 'Link Clicks Pagados',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,parseFloat(data_query[n].viral_amp)]);
					  formating = {
						title: 'Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,{v: parseFloat(data_query[n].penetration)/100, f: data_query[n].penetration}]);
					  formating = {
						title: 'Penetration',
						//curveType: 'function',
						vAxis: {
							format: '##.####%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,parseFloat(data_query[n].cxw)*currency_factor]);
					  formating = {
						title: 'CXW',
						//curveType: 'function',
						vAxis: {
							format: '$0.######'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,parseFloat(data_query[n].cpm)*currency_factor]);
					  formating = {
						title: 'CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,{v: parseFloat(data_query[n].paid_ctr)/100, f: data_query[n].paid_ctr}]);
					  formating = {
						title: 'CTR Pagado',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_clicks)]);
					  formating = {
						title: 'Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query[n].shares)]);
					  formating = {
						title: 'Shares',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 11:
					  data_table.addRow([data_date,parseFloat(data_query[n].likes)]);
					  formating = {
						title: 'Likes',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 12:
					  data_table.addRow([data_date,parseFloat(data_query[n].comments)]);
					  formating = {
						title: 'Comments',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 13:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_cpm)*currency_factor]);
					  formating = {
						title: 'Link CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 14:
					  if(n<data_query.length-1){
						  var rt_cxw=(parseFloat(data_query[n+1].spend)-parseFloat(data_query[n].spend))/(parseFloat(data_query[n+1].paid_link_clicks)-parseFloat(data_query[n].paid_link_clicks))*currency_factor;
						  data_table.addRow([
							  data_date,
							  rt_cxw
						  ]);
						  formating = {
							title: 'RT CXW',
							//curveType: 'function',
							vAxis: {
								format: 'currency'
							},
							hAxis: {
								format: 'M/d HH:mm',
								gridlines:{
									count: -1
								},
								title: 'Hora'
							},
							legend:{
								position: 'none'
							},
							pointSize: 3,
							trendlines: { 
								0: {
									lineWidth: 10,
									color: 'green',
									opacity: 1
								} 
							}
						  };
					  }
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_clicks)]);
					  formating = {
						title: 'Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query'));
			  table.draw(data_table,formating);
			}    
			function drawDeltaChartQuery(measure){
			  metric_query=measure;
			  var data_table = new google.visualization.DataTable();
			  var currency_factor;
  
			  if(query_currency=="MXN"){
				currency_factor=1/17;
			  }else{
				currency_factor=1;
			  }
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 1:
					data_table.addColumn('number', '\u0394 Gasto');
					break;
				case 2:
					data_table.addColumn('number', '\u0394 CTR Organico');
					break;
				case 3:
					data_table.addColumn('number', '\u0394 Link Clicks Pagados');
					break;
				case 4:
					data_table.addColumn('number', '\u0394 Viral AMP');
					break;
				case 5:
					data_table.addColumn('number', '\u0394 Penetration');
					break;
				case 6:
					data_table.addColumn('number', '\u0394 CXW');
					break;
				case 7:
					data_table.addColumn('number', '\u0394 CPM');
					break;
				case 8:
					data_table.addColumn('number', '\u0394 CTR Pagado');
					break;
				case 9:
					data_table.addColumn('number', '\u0394 Link Clicks');
					break;
				case 10:
					data_table.addColumn('number', '\u0394 Shares');
					break;
				case 11:
					data_table.addColumn('number', '\u0394 Likes');
					break;
				case 12:
					data_table.addColumn('number', '\u0394 Comments');
					break;
				case 13:
					data_table.addColumn('number', '\u0394 Link CPM');
					break;
				default:
					data_table.addColumn('number', '\u0394 Link Clicks');
					break;
			  }
  
			  var formating;
			  for(var n=1;n<data_query.length;n++){
				data_query[n].record_time=data_query[n].record_time.replace(' ','T');
				data_query[n].record_time+='-05:00';
				var data_date=new Date(data_query[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].spend)*currency_factor-parseFloat(data_query[n-1].spend)*currency_factor]);
					  formating = {
						title: '\u0394 Gasto',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].organic_ctr)/100-parseFloat(data_query[n-1].organic_ctr)/100, f: parseFloat(data_query[n].organic_ctr)-parseFloat(data_query[n-1].organic_ctr)+'%'}]);
					  formating = {
						title: '\u0394 CTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 3:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].paid_link_clicks)-parseFloat(data_query[n-1].paid_link_clicks)]);
					  formating = {
						title: '\u0394 Link Clicks Pagados',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].viral_amp)-parseFloat(data_query[n-1].viral_amp)]);
					  formating = {
						title: '\u0394 Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].penetration)/100-parseFloat(data_query[n-1].penetration)/100, f: parseFloat(data_query[n].penetration)-parseFloat(data_query[n-1].penetration)}]);
					  formating = {
						title: '\u0394 Penetration',
						//curveType: 'function',
						vAxis: {
							format: '##.####%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].cxw)*currency_factor-parseFloat(data_query[n-1].cxw)*currency_factor]);
					  formating = {
						title: '\u0394 CXW',
						//curveType: 'function',
						vAxis: {
							format: '$0.######'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].cpm)*currency_factor-parseFloat(data_query[n-1].cpm)*currency_factor]);
					  formating = {
						title: '\u0394 CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].paid_ctr)/100-parseFloat(data_query[n-1].paid_ctr)/100, f: parseFloat(data_query[n].paid_ctr)-parseFloat(data_query[n-1].paid_ctr)}]);
					  formating = {
						title: '\u0394 CTR Pagado',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].link_clicks)-parseFloat(data_query[n-1].link_clicks)]);
					  delta_link_clicks[n]=parseFloat(data_query[n].link_clicks)-parseFloat(data_query[n-1].link_clicks);
					  formating = {
						title: '\u0394 Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query[n].shares)-parseFloat(data_query[n-1].shares)]);
					  formating = {
						title: '\u0394 Shares',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 11:
					  data_table.addRow([data_date,parseFloat(data_query[n].likes)-parseFloat(data_query[n-1].likes)]);
					  formating = {
						title: '\u0394 Likes',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 12:
					  data_table.addRow([data_date,parseFloat(data_query[n].comments)-parseFloat(data_query[n-1].comments)]);
					  formating = {
						title: '\u0394 Comments',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 13:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_cpm)*currency_factor-parseFloat(data_query[n-1].link_cpm)*currency_factor]);
					  formating = {
						title: '\u0394 Link CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_clicks)-parseFloat(data_query[n-1].link_clicks)]);
					  formating = {
						title: '\u0394 Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query'));
			  table.draw(data_table,formating);
			}
			function insertQueryView(){
				var myNode = document.getElementById("fb_graphs");
				while (myNode.firstChild) {
					myNode.removeChild(myNode.firstChild);
				}
				$.ajax({
					url: "views/query_view.html",
					dataType: "html",
					method: 'get',
					async: false,
					success: function(response){
						myNode.innerHTML = response;
						jq1102(function() {
							var select_posts=[];
							$.ajax({
								url: "php_scripts/get_all_post_names.php",
								dataType: 'json',
								method: 'get',
								async: false,
								success: function(response){
									select_posts=response;
									//console.log(select_posts);
								}
							});
 
							jq1102( "#select_posts" ).autocomplete({
							  minLength: 0,
							  source: select_posts,
							  focus: function( event, ui ) {
								jq1102( "#select_posts" ).val( ui.item.label );
								return false;
							  },
							  select: function( event, ui ) {
								jq1102( "#select_posts" ).val( ui.item.label );
								//$( "#select_value" ).val( ui.item.value );
								post_id_query = ui.item.value;
								//console.log(post_id_query);
								var stats = document.getElementsByClassName("stats");
								for(var m=0;m<stats.length;m++)
								{
									stats.item(m).innerHTML = '';
								}
								getReportStats(post_id_query);
								selectPostQuery();
								return false;
							  }
							});
						});
					}
				});
			}    
			//posts query
			
			//Report
			function getReportStats(post_id) {
				post=[];
   
				query.edge = post_id;//query latest posts
				query.fields = "link";
				$.ajax({
					url: buildURL(query,access_token,1),
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response){
						post.link=response.link;
					}
				});
				query.edge=post_id+"/insights";
				query.fields="values,name";
				$.ajax({
					url: buildURL(query,access_token,1),
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response) {
					  var metrics=response.data;
					  for(var p=0;p<metrics.length;p++){
						switch(metrics[p].name){
						  case "post_impressions_unique":
							post.unique_imp=metrics[p].values[0].value;
							break;
						  case "post_stories_by_action_type":
							post.likes=metrics[p].values[0].value.like;
							post.comments=metrics[p].values[0].value.comment;
							post.shares=metrics[p].values[0].value.share;
							break;
						  case "post_negative_feedback_by_type":
							post.hide = typeof metrics[p].values[0].value.hide_clicks === 'undefined' ? 0 : metrics[p].values[0].value.hide_clicks;
							post.spam = typeof metrics[p].values[0].value.report_spam_clicks === 'undefined' ? 0 : metrics[p].values[0].value.report_spam_clicks;
						  default:
							break;
						}
					  }
					}
				});//ajax query to find all metrics with a switch
				document.getElementById("fb_shares").innerHTML = numberWithCommas(post.shares);
				document.getElementById("fb_likes").innerHTML = numberWithCommas(post.likes);
				document.getElementById("fb_comments").innerHTML = numberWithCommas(post.comments);
				document.getElementById("fb_reach").innerHTML = numberWithCommas(post.unique_imp);
				document.getElementById("fb_hide").innerHTML = numberWithCommas(post.hide);
				document.getElementById("fb_spam").innerHTML = numberWithCommas(post.spam);
				document.getElementById("post_id").innerHTML = '';
				var aTag=document.createElement('a');
				aTag.setAttribute('href',"http://www.facebook.com/"+post_id);
				aTag.setAttribute('target','_blank');
				aTag.innerHTML = post_id;
				document.getElementById("post_id").appendChild(aTag);
				queryAnalytics(post);
			}
			function getSeconds(seconds){
				var sec;
				if(Math.floor(parseFloat(post.time_on_page)%60)>9){
					return  Math.floor(parseFloat(post.time_on_page)%60);
				}else{
					return "0"+Math.floor(parseFloat(post.time_on_page)%60);
				}
			}
			function queryAnalytics(post){
				post.link = post.link.replace('http://eldeforma.com','');
				if(post.link.indexOf('?utm')!=-1){
					post.link = post.link.substring(0,post.link.indexOf('?utm'));
				}
				var date= new Date();
				var month = date.getMonth()>=9 ? (date.getMonth()+1) : "0"+(date.getMonth()+1);
				var day = date.getDate()>=10 ? (date.getDate()) : "0"+(date.getDate());
				gapi.client.load('analytics','v3', function(){
					var requestPageviews = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:pageviews",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestPageviews.execute(function(resp){
						//console.log(resp);
						post.pageviews = resp.totalsForAllResults["ga:pageviews"];
						document.getElementById("site_pageviews").innerHTML = numberWithCommas(post.pageviews);
						var requestSocial = gapi.client.analytics.data.ga.get({
							ids: "ga:41142760",
							"start-date": "365daysAgo",
							"end-date": "today",
							metrics: "ga:totalEvents",
							dimensions: "ga:eventAction",
							fields: "rows",
							output: 'json',
							filters: "ga:eventLabel=@"+post.link
						});
						requestSocial.execute(function(resp){
							post.site_shares = 0;
							post.site_tweets = 0
							post.site_whatsapp = 0;
							for(var l=0;l<resp.rows.length;l++){
								switch(resp.rows[l][0])
								{
									case "Facebook Share Class":
										post.site_shares = resp.rows[l][1];
										break;
									case "Twitter Tweet Class":
										post.site_tweets = resp.rows[l][1];
										break;
									case "WhatsApp Message Class":
										post.site_whatsapp = resp.rows[l][1];
										break;
									default:
										break;
								}
							}
							document.getElementById("site_shares").innerHTML = numberWithCommas(post.site_shares);
							document.getElementById("site_tweets").innerHTML = numberWithCommas(post.site_tweets);
							document.getElementById("site_whatsapp").innerHTML = numberWithCommas(post.site_whatsapp);
							document.getElementById("site_shares_per_pageview").innerHTML = parseFloat(((parseFloat(post.site_shares)/parseFloat(post.pageviews)*100))).toFixed(4)+'%';
							//console.log(post);
						});
					});
					var requestUsers = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:users",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestUsers.execute(function(resp){
						//console.log(resp);
						post.users = resp.totalsForAllResults["ga:users"];
						document.getElementById("site_users").innerHTML = numberWithCommas(post.users);	
					});
					var requestTime = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:avgTimeOnPage",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestTime.execute(function(resp){
						//console.log(resp);
						post.time_on_page = resp.totalsForAllResults["ga:avgTimeOnPage"];
						document.getElementById("time_on_page").innerHTML = Math.floor(parseFloat(post.time_on_page)/60)+":"+getSeconds(parseFloat(post.time_on_page));
					});
				});
				//console.log(post);	
			}
			//report
			
			//interval calls
			var initialCalls = setTimeout(function(){
				UpdatePostsDashboard();
				loadKomfo();
				UpdatePostsPromoted();
			},1000);
			
			var refreshData = setInterval(function(){
				selectPostDashboard();
				loadKomfo();
				selectPostPromoted();
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
        </script>

    </body>
</html>