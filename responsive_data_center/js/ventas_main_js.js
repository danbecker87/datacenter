//active page
function getActivePage() {
	$(document).ready(function(){
		var page_url=window.location.href;
		//console.log($('#sidebar-menu')[0].childNodes[1].childNodes[1]);
		for(var i=3;i<$('#sidebar-menu')[0].childNodes[1].childNodes.length;i+=2){
			var sidebar_tag=$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].href;
			//console.log(sidebar_tag,page_url);
			if(sidebar_tag.indexOf(page_url) > -1){
				$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].className = "waves-effect active";
			}
			//console.log(i);
		}
	});
}
//active page

//General
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
var ejecutivos_slack ={
	'CHARYTIN CALDERON': '@manuel',
	'DAN BECKER': '@dan',
	'FERNANDA BERNAL': '@fernanda.bernal',
	'FERNANDO MORANDI': '@manuel',
	'IVAN ALVAREZ': '@manuel',
	'JACOBO FASJA': '@centraldeforma',
	'MANUEL ACOSTA': '@manuel',
	'MARIA CHAPARRO': '@marisa',
	'MIGUEL MARIN': '@centraldeforma',
	'SEAN MELIA': '@sean',
	'AURORA RAMIREZ': '@auro'
};
var arr_dont_notify_status =[
	'Pagado',
	'Facturado',
	'Facturado (ingresar sistema)',
	'Pagado -IVA',
	'Facturado (ingresar fisico)',
	'PTE DO',
	'Detenido'
];
var arr_dont_notify_status_admin =[
	'Pagado',
	'Facturado',
	'Facturado (ingresar sistema)',
	'Facturado (ingresar fisico)',
	'Pagado -IVA',
	'PTE DO',
	'Detenido'
];

var global_metas;

var monthName = [
	"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
];
//General

//Ventas
var ventas, ventasFiltered;
var ventasTable;
var Filters;
function handleVentas(arr_ventas){
	ventas = [];
	for(var i=0;i<arr_ventas.length;i++){
		ventas.push(
			{
				'client': arr_ventas[i][0],
				'campaign': arr_ventas[i][1],
				'agency': arr_ventas[i][2],
				'agent': arr_ventas[i][3],
				'sell_date': arr_ventas[i][4],
				'sell_year': arr_ventas[i][5],
				'sell_q': arr_ventas[i][6],
				'run_date': arr_ventas[i][7],
				'invoice_date': arr_ventas[i][8],
				'pay_date': arr_ventas[i][9],
				'ammount': arr_ventas[i][10], 
				'status': arr_ventas[i][11], 
				'days_due': arr_ventas[i][12], 
				'days_left': arr_ventas[i][13],
				'invoice_num': arr_ventas[i][14],
				'odc': arr_ventas[i][15],
				'note': arr_ventas[i][16],
                'contact': arr_ventas[i][17],
                'cc': arr_ventas[i][18],
                'rebate': arr_ventas[i][20],
                'source': arr_ventas[i][21],
                'rebate_status': arr_ventas[i][22]
			}
		)
	}
	drawTableVentas(ventas);
}

function drawTableVentas(ventas){
    
    var dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));
    
    var ventas_data = new google.visualization.DataTable();	
  
	ventas_data.addColumn({type: 'string', label: 'Clientes'});
	ventas_data.addColumn({type: 'string', label: 'Campaña'});
	ventas_data.addColumn({type: 'string', label: 'Agencia'});
	ventas_data.addColumn({type: 'string', label: 'Ejecutivo'});
	ventas_data.addColumn({type: 'number', label: 'Monto'});
    ventas_data.addColumn({type: 'number', label: 'Año de venta'});
	ventas_data.addColumn({type: 'date', label: 'Mes de venta'});
	ventas_data.addColumn({type: 'string', label: 'Q de venta'});
	ventas_data.addColumn({type: 'date', label: 'Fecha corrida'});
	ventas_data.addColumn({type: 'string', label: 'Estatus'});
	ventas_data.addColumn({type: 'string', label: 'Factura'});
	ventas_data.addColumn({type: 'date', label: 'Fecha de factura'});
	ventas_data.addColumn({type: 'date', label: 'Fecha de pago'});
	ventas_data.addColumn({type: 'number', label: 'Plazo restante'});
    ventas_data.addColumn({type: 'number', label: 'Rebate'});
    ventas_data.addColumn({type: 'string', label: 'Nota'});
    ventas_data.addColumn({type: 'string', label: 'Fuente'});
    ventas_data.addColumn({type: 'string', label: 'Rebate Liquidado'});
	

	for(var i=0;i<ventas.length;i++){
		if(ventas[i].client == '' || ventas[i].client == null || ventas[i].client == ' ' || ventas[i].client == 'Total'){
		
		}else{
			ventas_data.addRow([
				ventas[i].client,
				ventas[i].campaign,
				ventas[i].agency,
				ventas[i].agent,
				{v: ventas[i].ammount, f: '$ '+numberWithCommas(Math.round(ventas[i].ammount))},
                {v: new Date(ventas[i].sell_date).getFullYear(), f: String(new Date(ventas[i].sell_date).getFullYear())},
                {v: new Date(new Date(ventas[i].sell_date).getFullYear(), new Date(ventas[i].sell_date).getMonth(), new Date(ventas[i].sell_date).getDate()), f: monthName[new Date(ventas[i].sell_date).getMonth()]+'-'+new Date(ventas[i].sell_date).getFullYear()},
				ventas[i].sell_q,
				new Date(new Date(ventas[i].run_date).getFullYear(), new Date(ventas[i].run_date).getMonth(),new Date(ventas[i].run_date).getDate()),
				ventas[i].status,
				''+ventas[i].invoice_num,
				ventas[i].invoice_date != 'PTE' ? new Date(new Date(ventas[i].invoice_date).getFullYear(), new Date(ventas[i].invoice_date).getMonth(),new Date(ventas[i].invoice_date).getDate()) : null,
				ventas[i].pay_date != 'PTE' ? new Date(new Date(ventas[i].pay_date).getFullYear(), new Date(ventas[i].pay_date).getMonth(),new Date(ventas[i].pay_date).getDate()) : null,
				ventas[i].days_left == '-' ? null : ventas[i].days_left,
                {v: ventas[i].rebate, f: '$ '+numberWithCommas(Math.round(ventas[i].rebate))},
                String(ventas[i].note),
                ventas[i].source,
                ventas[i].rebate_status
			]);
		}
	}
    
    Filters = []; 
    var i=0;
    
    //estatus
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Estatus',
           'ui': {
               'label': "Estatus",
               'labelSeparator': ':',
               'caption': 'Seleccionar Estatus...',
               'sortValues': false
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[0].append(divTag);
    i++;
    
    //agencia
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Agencia',
           'ui': {
               'label': "Agencia",
               'labelSeparator': ':',
               'caption': 'Seleccionar Agencia...',
               'sortValues': true
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[0].append(divTag);
    i++;
    
    //cliente
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
            'filterColumnLabel': 'Clientes',
            'matchType': 'any',
            'ui': {
                'label': "Cliente",
                'labelSeparator': ':',
                'caption': 'Seleccionar Cliente...',
            }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[0].append(divTag);
    i++;
    
    //ejecutivo
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Ejecutivo',
           'ui': {
               'label': "Ejecutivo",
               'labelSeparator': ':',
               'caption': 'Seleccionar Ejecutivo...',
               'sortValues': true
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[0].append(divTag);
    i++;
    
    //Año de venta
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Año de venta',
           'ui': {
               'label': "Año de Venta",
               'labelSeparator': ':',
               'caption': 'Seleccionar año...',
               'sortValues': true
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[1].append(divTag);
    i++;
    
    //q de venta
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Q de venta',
           'ui': {
               'label': "Q de Venta",
               'labelSeparator': ':',
               'caption': 'Seleccionar Q...',
               'sortValues': false
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[1].append(divTag);
    i++;
    
    //mes de venta
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'CategoryFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Mes de venta',
            'useFormattedValue': true,
           'ui': {
               'label': "Mes de Venta",
               'caption': 'Seleccionar Mes...',
               'labelSeparator': ':',
               'sortValues': false
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[1].append(divTag);
    i++;
    
    //campaña
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'StringFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
            'filterColumnLabel': 'Campaña',
            'matchType': 'any',
            'ui': {
                'label': "Campaña",
                'labelSeparator': ':',
            }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[1].append(divTag);
    i++;
    
    //monto
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'NumberRangeFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Monto',
           'ui': {
               'label': "Monto",
               'labelSeparator': ':',
               'step': 10000,
               'format': {
                   prefix: '$ ',
                   fractionDigits: 1
               }
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[2].append(divTag);
    i++;
    
    //factura
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'StringFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
            'filterColumnLabel': 'Factura',
            'matchType': 'prefix',
            'ui': {
                'label': "Factura",
                'labelSeparator': ':',
            }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[2].append(divTag);
    i++;
    
    //plazo
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'NumberRangeFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
           'filterColumnLabel': 'Plazo restante',
           'ui': {
               'label': "Plazo restante",
               'labelSeparator': ':',
           }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[2].append(divTag);
    i++;
    
    //Fuente
    Filters.push(new google.visualization.ControlWrapper({ 
        controlType: 'StringFilter',
        'containerId': 'filter_div_'+i, 
        'options': {
            'filterColumnLabel': 'Fuente',
            'matchType': 'exact',
            'ui': {
                'label': "Fuente",
                'labelSeparator': ':',
            }
        }
    })) 
    var divTag = document.createElement("div");
    divTag.id = "filter_div_"+i;
    $('#filters_div').children()[2].append(divTag);
    
        
    ventasTable = new google.visualization.ChartWrapper({
        'chartType': 'Table',
        'containerId': 'ventas_table',
        'options': {
            'width': '100%',
            'height': '50vh',
            'sortColumn': 6,
            'sortAscending': true
        }
    });
    
    google.visualization.events.addListener(ventasTable, 'ready', function(){
        var selection = JSON.parse(ventasTable.toJSON());
        //console.log(selection);
        var monto_total=0;
        for(var i=0;i<selection.dataTable.rows.length;i++){
            monto_total+=selection.dataTable.rows[i].c[4].v;
        }
        $('#monto_total')[0].innerHTML = '$ '+numberWithCommas(monto_total.toFixed(0));
        
        var rebate_total=0;
        for(var i=0;i<selection.dataTable.rows.length;i++){
            rebate_total+=selection.dataTable.rows[i].c[14].v;
        }
        $('#rebate_total')[0].innerHTML = '$ '+numberWithCommas(rebate_total.toFixed(0));
        
        var columns = [9,16,17,2,3,5,7,6,0];
        
        for(var i=0;i<columns.length;i++){
            summarizeColumn(columns[i],i);
        }
        
        function summarizeColumn(column,div){
            var obj_status={};
        
            for(var i=0;i<selection.dataTable.rows.length;i++){
                if( typeof obj_status[selection.dataTable.rows[i].c[column].v] === 'undefined' && typeof obj_status[selection.dataTable.rows[i].c[column].f] === 'undefined'){
                    if(typeof selection.dataTable.rows[i].c[column].f === 'undefined'){
                        obj_status[selection.dataTable.rows[i].c[column].v] = {};
                        obj_status[selection.dataTable.rows[i].c[column].v].count = 1;
                        obj_status[selection.dataTable.rows[i].c[column].v].amount = selection.dataTable.rows[i].c[4].v;
                        obj_status[selection.dataTable.rows[i].c[column].v].rebate = selection.dataTable.rows[i].c[14].v;
                    }else{
                        obj_status[selection.dataTable.rows[i].c[column].f] = {};
                        obj_status[selection.dataTable.rows[i].c[column].f].count = 1;
                        obj_status[selection.dataTable.rows[i].c[column].f].amount = selection.dataTable.rows[i].c[4].v;
                        obj_status[selection.dataTable.rows[i].c[column].f].rebate = selection.dataTable.rows[i].c[14].v;
                    }
                }else{
                    if(typeof selection.dataTable.rows[i].c[column].f === 'undefined'){
                        obj_status[selection.dataTable.rows[i].c[column].v].count++;
                        obj_status[selection.dataTable.rows[i].c[column].v].amount+=selection.dataTable.rows[i].c[4].v;
                        obj_status[selection.dataTable.rows[i].c[column].v].rebate += selection.dataTable.rows[i].c[14].v;
                    }else{
                        obj_status[selection.dataTable.rows[i].c[column].f].count++;
                        obj_status[selection.dataTable.rows[i].c[column].f].amount+=selection.dataTable.rows[i].c[4].v;
                        obj_status[selection.dataTable.rows[i].c[column].f].rebate += selection.dataTable.rows[i].c[14].v;
                    }
                }
            }

            $('#'+$('#summary_rows>div>span')[div].id).children().remove();
            $.each(obj_status,function(key,value){
                var current=$('#'+$('#summary_rows>div>span')[div].id).append($('<span />').html(key+': '+obj_status[key].count+' $'+numberWithCommas(obj_status[key].amount.toFixed(0)))).append($('<br />'));
                if(key.indexOf('Q4') != -1){
                    current.append($('<br />'));
                }
                if(key.indexOf('Diciembre') != -1){
                    current.append($('<br />'));
                }
                //console.log(key,'    ',value);
            });  
            
            $('#'+$('#rebate_summary_rows>div>span')[div].id).children().remove();
            $.each(obj_status,function(key,value){
                if(obj_status[key].rebate != 0){
                    var current=$('#'+$('#rebate_summary_rows>div>span')[div].id).append($('<span />').html(key+': '+obj_status[key].count+' $'+numberWithCommas(obj_status[key].rebate.toFixed(0)))).append($('<br />'));
                    if(key.indexOf('Q4') != -1){
                        current.append($('<br />'));
                    }
                    if(key.indexOf('Diciembre') != -1){
                        current.append($('<br />'));
                    }
                    //console.log(key,'    ',value);
                }
            });
        }
        
        $('#dickWithAgencies').show();
        $('#slack').show();
    });
    
    google.visualization.events.addListener(ventasTable, 'select', function(){
        var selection = ventasTable.getChart().getSelection();
        if(selection.length > 0){
            var monto_total=0;
            for(var i=0;i<selection.length;i++){
                monto_total+=ventasTable.getDataTable().getValue(selection[i].row, 4);
            }
            $('#monto_total')[0].innerHTML = '$ '+numberWithCommas(monto_total.toFixed(0));
            
            var rebate_total=0;
            for(var i=0;i<selection.length;i++){
                rebate_total+=ventasTable.getDataTable().getValue(selection[i].row, 4);
            }
            $('#rebate_total')[0].innerHTML = '$ '+numberWithCommas(rebate_total.toFixed(0));
            
            var columns = [9,16,17,2,3,5,7,6,0,16];
        
            for(var i=0;i<columns.length;i++){
                summarizeColumn(columns[i],i);
            }

            function summarizeColumn(column,div){
                var obj_status={};

                for(var i=0;i<selection.length;i++){
                    if( typeof obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)] === 'undefined'){
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)] = {};
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)].count = 1;
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)].amount = ventasTable.getDataTable().getValue(selection[i].row, 4);
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)].rebate = ventasTable.getDataTable().getValue(selection[i].row, 14);
                    }else{
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)].count++;
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)].amount+=ventasTable.getDataTable().getValue(selection[i].row, 4);
                        obj_status[ventasTable.getDataTable().getFormattedValue(selection[i].row, column)].rebate += ventasTable.getDataTable().getValue(selection[i].row, 14);
                    }
                }

                $('#'+$('#summary_rows>div>span')[div].id).children().remove();
                $.each(obj_status,function(key,value){
                    var current=$('#'+$('#summary_rows>div>span')[div].id).append($('<span />').html(key+': '+obj_status[key].count+' $'+numberWithCommas(obj_status[key].amount.toFixed(0)))).append($('<br />'));
                    if(key.indexOf('Q4') != -1){
                        current.append($('<br />'));
                    }
                    if(key.indexOf('Diciembre') != -1){
                        current.append($('<br />'));
                    }
                    //console.log(key,'    ',value);
                });  

                $('#'+$('#rebate_summary_rows>div>span')[div].id).children().remove();
                $.each(obj_status,function(key,value){
                    if(obj_status[key].rebate != 0){
                        var current=$('#'+$('#rebate_summary_rows>div>span')[div].id).append($('<span />').html(key+': '+obj_status[key].count+' $'+numberWithCommas(obj_status[key].rebate.toFixed(0)))).append($('<br />'));
                        if(key.indexOf('Q4') != -1){
                            current.append($('<br />'));
                        }
                        if(key.indexOf('Diciembre') != -1){
                            current.append($('<br />'));
                        }
                        //console.log(key,'    ',value);
                    }
                });
            }
        }else{
            google.visualization.events.trigger(this, 'select', {});
        }
    });
    
    //Bonos
    
    
    google.visualization.events.addListener(ventasTable, 'ready', function(){
        var metas={
            'MANUEL ACOSTA MANAGER': {
                '2017': {
                    'Q1': {
                        total: 0,
                        meta: 2437500,
                        bono: 65000
                    },
                    'Q2': {
                        total: 0,
                        meta: 3356250,
                        bono: 89500
                    },
                    'Q3': {
                        total: 0,
                        meta: 4406250,
                        bono: 105750
                    },
                    'Q4': {
                        total: 0,
                        meta: 4800000,
                        bono: 128000
                    },
                total: 0,
                meta: 15000000,
                bono: 110000
                },
                '2018': {
                    'Q1': {
                        total: 0,
                        meta: 3600000,
                        bono: 90000
                    },
                    'Q2': {
                        total: 0,
                        meta: 4807500,
                        bono: 120000
                    },
                    'Q3': {
                        total: 0,
                        meta: 7110000,
                        bono: 140000
                    },
                    'Q4': {
                        total: 0,
                        meta: 7644000,
                        bono: 160000
                    },
                total: 0,
                meta: 23161500,
                bono: 250000
                }
            },
            'MANUEL ACOSTA': {
                '2017': {
                    'Q1': {
                    total: 0,
                    meta: 625000,
                    bono: 21875
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                total: 0,
                meta: 1,
                bono: 0
                },
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                total: 0,
                meta: 1,
                bono: 0
                }
            },
            'ANGELA CALDERON': {
                '2017': {
                    'Q1': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 1,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                total: 0,
                meta: 1,
                bono: 0
                },
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 1,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                total: 0,
                meta: 1,
                bono: 0
                }
            },
            'FERNANDA BERNAL': {
                '2017': {
                    'Q1': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000000,
                    bono: 0
                    },
                total: 0,
                meta: 1,
                bono: 0
                }
            },
            'MAJO OREA': {
                '2017': {
                    'Q1': {
                    total: 0,
                    meta: 795000,
                    bono: 23000
                    },
                    'Q2': {
                    total: 0,
                    meta: 1166000,
                    bono: 35000
                    },
                    'Q3': {
                    total: 0,
                    meta: 1590000,
                    bono: 39750
                    },
                    'Q4': {
                    total: 0,
                    meta: 1749000,
                    bono: 43725
                    },
                total: 0,
                meta: 5300000,
                bono: 50000
                }
            },
            'SEAN MELIA': {
                '2017': {
                    'Q1': {
                    total: 0,
                    meta: 500000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 1870000,
                    bono: 65500
                    },
                    'Q3': {
                    total: 0,
                    meta: 2940000,
                    bono: 88000
                    },
                    'Q4': {
                    total: 0,
                    meta: 3192000,
                    bono: 96000
                    },
                total: 0,
                meta: 8502000,
                bono: 150000
                },
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 1650000,
                    bono: 48000,
                    awardedBonus: 37500
                    },
                    'Q2': {
                    total: 0,
                    meta: 2270000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 3270000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 3500000,
                    bono: 0
                    },
                total: 0,
                meta: 10690000,
                bono: 0
                }
            },
            'MARIA CHAPARRO': {
                '2017': {
                    'Q1': {
                    total: 0,
                    meta: 500000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 2270000,
                    bono: 65450
                    },
                    'Q3': {
                    total: 0,
                    meta: 3270000,
                    bono: 88125
                    },
                    'Q4': {
                    total: 0,
                    meta: 3500000,
                    bono: 95775
                    },
                total: 0,
                meta: 9540000,
                bono: 112500
                },
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 1650000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 2270000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 3270000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 3500000,
                    bono: 0
                    },
                total: 0,
                meta: 10690000,
                bono: 0
                }
            },
            'FERNANDO GONZALEZ': {
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 1500000,
                    bono: 45000
                    },
                    'Q2': {
                    total: 0,
                    meta: 1870000,
                    bono: 65000
                    },
                    'Q3': {
                    total: 0,
                    meta: 2940000,
                    bono: 88000
                    },
                    'Q4': {
                    total: 0,
                    meta: 3192000,
                    bono: 96000
                    },
                total: 0,
                meta: 9502000,
                bono: 150000
                }
            },
            'JOSELYN DE LA PAZ': {
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 2270000,
                    bono: 70000
                    },
                    'Q3': {
                    total: 0,
                    meta: 3270000,
                    bono: 90000
                    },
                    'Q4': {
                    total: 0,
                    meta: 3500000,
                    bono: 96000
                    },
                total: 0,
                meta: 9040000,
                bono: 112000
                }
            },
            'YADO': {
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                total: 0,
                meta: 100000000,
                bono: 0
                }
            },
            'JUAN FRANCISCO': {
                '2018': {
                    'Q1': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q2': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q3': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                    'Q4': {
                    total: 0,
                    meta: 10000000,
                    bono: 0
                    },
                total: 0,
                meta: 100000000,
                bono: 0
                }
            }
        }
    
        var overheadPercentages ={
            'MANUEL ACOSTA MANAGER': {
                '2017': 0.04,
                '2018': 0.03
            },
            'MANUEL ACOSTA': {
                '2017': 0.04,
                '2018': 0.04
            },
            'ANGELA CALDERON': {
                '2017': 0,
                '2018': 0
            },
            'FERNANDA BERNAL': {
                '2017': 0,
                '2018': 0
            },
            'MAJO OREA': {
                '2017': 0,
                '2018': 0
            },
            'SEAN MELIA': {
                '2017': 0.04,
                '2018': 0
            },
            'MARIA CHAPARRO': {
                '2017': 0.04,
                '2018': 0
            },
            'FERNANDO GONZALEZ': {
                '2018': 0.04
            },
            'JOSELYN DE LA PAZ': {
                '2018': 0.04
            },
            'YADO': {
                '2018': 0
            },
            'JUAN FRANCISCO': {
                '2018': 0
            }
        }
        
        $('#bonus_year_div').children().remove();
        $('#bonus_q_div').children().remove();
        $('#bonus_executive_div').children().remove();
        $('#bonus_executive_q_div').children().remove();
        
        var selection = JSON.parse(ventasTable.toJSON());
                           
        for(var i=0;i<selection.dataTable.rows.length;i++){
            if(typeof metas[selection.dataTable.rows[i].c[3].v] !== 'undefined' && selection.dataTable.rows[i].c[5].v > 2016){
                metas['MANUEL ACOSTA MANAGER'][selection.dataTable.rows[i].c[5].f].total+=selection.dataTable.rows[i].c[4].v;
                metas['MANUEL ACOSTA MANAGER'][selection.dataTable.rows[i].c[5].f][selection.dataTable.rows[i].c[7].v.replace('-'+selection.dataTable.rows[i].c[5].f,'')].total+=selection.dataTable.rows[i].c[4].v;
                metas[selection.dataTable.rows[i].c[3].v][selection.dataTable.rows[i].c[5].f].total+=selection.dataTable.rows[i].c[4].v;
                metas[selection.dataTable.rows[i].c[3].v][selection.dataTable.rows[i].c[5].f][selection.dataTable.rows[i].c[7].v.replace(('-'+selection.dataTable.rows[i].c[5].f),'')].total+=selection.dataTable.rows[i].c[4].v;
            }
        }
        
        function determineBonus(meta,total,bono){
            switch(true){
                case total/meta >= 1:
                    return bono;
                    break;
                case total/meta > 0.9:
                    return bono*0.75;
                    break;
                case total/meta > 0.8:
                    return bono*0.5;
                    break;
                case total/meta > 0.7:
                    return bono*0.25;
                    break;
                default:
                    return 0;
                    break;
                
            }
        }
        
        function determineAnualBonus(meta,total,bono){
            switch(true){
                case total/meta >= 1:
                    return bono;
                    break;
                default:
                    return 0;
                    break;
                
            }
        }
        
        function determineOverhead(meta,total,salesman,year){
            switch(true){
                case total/meta > 1:
                    return (total-meta)*overheadPercentages[salesman][year];
                    break;
                default:
                    return 0;
                    break;
                
            }
        }
        
        $.each(metas,function(salesman,years){
            $.each(years,function(year,qs){
                //console.log(salesman,year,q)
                metas[salesman][year].awardedBonus = determineAnualBonus(metas[salesman][year].meta,metas[salesman][year].total,metas[salesman][year].bono);
                $.each(qs,function(q,sales){
                   if(typeof metas[salesman][year][q].awardedBonus !== 'undefined'){
                        metas[salesman][year][q].awardedBonus += determineBonus(metas[salesman][year][q].meta,metas[salesman][year][q].total,metas[salesman][year][q].bono);
                   }else{
                        metas[salesman][year][q].awardedBonus = determineBonus(metas[salesman][year][q].meta,metas[salesman][year][q].total,metas[salesman][year][q].bono);
                   }
                    metas[salesman][year][q].overhead = determineOverhead(metas[salesman][year][q].meta,metas[salesman][year][q].total,salesman,year);
                });
            });
        });
        
        global_metas = metas;
        
        //console.log(metas);
        
        var totalSalesmenBonus = {
            'MANUEL ACOSTA MANAGER': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                },
                '2018': {
                    'Q1': 0,
                    'Q2': 15000,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 15000
                }
            },
            'MANUEL ACOSTA': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                },
                '2018': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                }
            },
            'ANGELA CALDERON': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                },
                '2018': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                }
            },
            'FERNANDA BERNAL': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                }
            },
            'MAJO OREA': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                }
            },
            'SEAN MELIA': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                   bono: 0
                },
                '2018': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                }
            },
            'MARIA CHAPARRO': {
                '2017': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                },
                '2018': {
                    'Q1': 0,
                    'Q2': 0,
                    'Q3': 0,
                    'Q4': 0,
                    'Y': 0,
                    bono: 0
                }
            },
            'FERNANDO GONZALEZ': {
                '2018': {
                    'Q1': 0, //Done
                    'Q2': 0, //Done
                    'Q3': 0, //Done
                    'Q4': 0, //Done
                    'Y': 0, //Done
                    bono: 0 //Done
                }
            },
            'JOSELYN DE LA PAZ': {
                '2018': {
                    'Q1': 0, //Done
                    'Q2': 0, //Done
                    'Q3': 0, //Done
                    'Q4': 0, //Done
                    'Y': 0, //Done
                    bono: 0 //Done
                }
            },
            'YADO': {
                '2018': {
                    'Q1': 0, //Done
                    'Q2': 0, //Done
                    'Q3': 0, //Done
                    'Q4': 0, //Done
                    'Y': 0, //Done
                    bono: 0 //Done
                }
            },
            'JUAN FRANCISCO': {
                '2018': {
                    'Q1': 0, //Done
                    'Q2': 0, //Done
                    'Q3': 0, //Done
                    'Q4': 0, //Done
                    'Y': 0, //Done
                    bono: 0 //Done
                }
            }
        };
        
        var totalYearBonus = {
            '2017': {
                'Q1': 0,
                'Q2': 0,
                'Q3': 0,
                'Q4': 0,
                'Y': 0,
                bono: 0
            },
            '2018':{
                'Q1': 0, //Done
                'Q2': 15000, //Done
                'Q3': 0, //Done
                'Q4': 0, //Done
                'Y': 0,  //Dona
                bono: 15000 //Done
            }
        }
        
        $.each(metas,function(salesman,years){
            $.each(years,function(year,qs){
                totalYearBonus[year].bono+=metas[salesman][year].awardedBonus; // Total year adding Annual bonus
                
                totalYearBonus[year]['Y']+=metas[salesman][year].awardedBonus; // Total Annuals adding Annual bonus
                
                totalSalesmenBonus[salesman][year]['Y']+=metas[salesman][year].awardedBonus; // Total Salesman Annual bonus 
                totalSalesmenBonus[salesman][year]['bono']+=metas[salesman][year].awardedBonus; // Total Salesman bonus adding annual bonus
                $.each(qs,function(q,sales){
                    if(q.indexOf('Q') > -1){
                        totalYearBonus[year].bono+=metas[salesman][year][q].awardedBonus+metas[salesman][year][q].overhead; // Total year adding Q's bonus

                        totalYearBonus[year][q]+=metas[salesman][year][q].awardedBonus+metas[salesman][year][q].overhead; // Total Yearly Q adding Q's bonus

                        totalSalesmenBonus[salesman][year]['bono']+=metas[salesman][year][q].awardedBonus+metas[salesman][year][q].overhead; // Total Salesman bonus adding q bonus
                        totalSalesmenBonus[salesman][year][q]+=metas[salesman][year][q].awardedBonus+metas[salesman][year][q].overhead; // Total Salesman Q bonus
                    }
                });
            });
        });
        
        $('#bonus_year_div').append($('<ul />'));
        $.each(totalYearBonus,function(year,qs){
            if(totalYearBonus[year].bono > 0){
               $('#bonus_year_div').append($('<li />').html(year+': '+' $ '+numberWithCommas(totalYearBonus[year].bono.toFixed(0))));
            }
        });
        
        $('#bonus_q_div').append($('<ul />'));
        $.each(totalYearBonus,function(year,qs){
            if(totalYearBonus[year].bono > 0){
                $('#bonus_q_div>ul').append($('<li />').attr('id','year_q'+year).html(year));
                $('#year_q'+year).append($('<ul />'))
                $.each(qs,function(q,value){
                    if(q!='bono'){
                        if(value > 0){
                            $('#year_q'+year+'>ul').append($('<li />').html(q+': '+' $ '+numberWithCommas(value.toFixed(0))));
                        }
                    }
                });
            }
        });
        
        $('#bonus_executive_div').append($('<ul />'));
        $.each(totalYearBonus,function(year,qs){
            if(totalYearBonus[year].bono > 0){
                $('#bonus_executive_div>ul').append($('<li />').attr('id','year_salesman_'+year).html(year));
                $('#year_salesman_'+year).append($('<ul />'))
                $.each(totalSalesmenBonus,function(salesman,years){
                    if(typeof totalSalesmenBonus[salesman][year] !== 'undefined' && totalSalesmenBonus[salesman][year]['bono'] > 0){
                        $('#year_salesman_'+year+'>ul').append($('<li />').html(salesman+': '+' $ '+numberWithCommas(totalSalesmenBonus[salesman][year]['bono'].toFixed(0))));
                    }
                });
            }
        });
        
        $('#bonus_executive_q_div').append($('<ul />'));
        $.each(totalYearBonus,function(year,qs){
            if(totalYearBonus[year].bono > 0){
                $('#bonus_executive_q_div>ul').append($('<li />').attr('id','year_salesman_q_'+year).html(year));
                $('#year_salesman_q_'+year).append($('<ul />'));
                $.each(totalSalesmenBonus,function(salesman,salesmen_years){
                    if(typeof totalSalesmenBonus[salesman][year] !== 'undefined' && totalSalesmenBonus[salesman][year]['bono'] > 0){
                        $('#year_salesman_q_'+year+'>ul').append($('<li />').html(salesman).attr('id','year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')));
                        $('#year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')).append($('<ul />'));
                        if(totalSalesmenBonus[salesman][year]['Q1'] > 0){
                            $('#year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')+'>ul').append($('<li />').html('Q1'+': '+' $ '+numberWithCommas(totalSalesmenBonus[salesman][year]['Q1'].toFixed(0))));
                        }
                        if(totalSalesmenBonus[salesman][year]['Q2'] > 0){
                            $('#year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')+'>ul').append($('<li />').html('Q2'+': '+' $ '+numberWithCommas(totalSalesmenBonus[salesman][year]['Q2'].toFixed(0))));
                        }
                        if(totalSalesmenBonus[salesman][year]['Q3'] > 0){
                            $('#year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')+'>ul').append($('<li />').html('Q3'+': '+' $ '+numberWithCommas(totalSalesmenBonus[salesman][year]['Q3'].toFixed(0))));
                        }
                        if(totalSalesmenBonus[salesman][year]['Q4'] > 0){
                            $('#year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')+'>ul').append($('<li />').html('Q4'+': '+' $ '+numberWithCommas(totalSalesmenBonus[salesman][year]['Q4'].toFixed(0))));
                        }
                        if(totalSalesmenBonus[salesman][year]['Y'] > 0){
                            $('#year_salesman_q_'+year+'_'+salesman.replace(/\s/g, '')+'>ul').append($('<li />').html('Y'+': '+' $ '+numberWithCommas(totalSalesmenBonus[salesman][year]['Y'].toFixed(0))));
                        }
                    }
                });
            }
        });
        
        
    });
    //Bonos
    
    var num_filter = 7;
    for(var j=0;j<num_filter;j++){
        for(var k=j+1;k<num_filter;k++){
            dashboard.bind(Filters[j], Filters[k]);
        }
    }
   
    dashboard.bind(Filters, ventasTable);
    
	dashboard.draw(ventas_data);
	
}


//Bonos
var agente_ventas={};
var agente_admin={};
var arr_agente_ventas=[];
var arr_agente_admin=[];
function notifySlack(){
	agente_ventas={};
	agente_admin={};
	arr_agente_ventas=[];
	$.each(ejecutivos_slack,function(key, value){
		if(key != 'DAN BECKER' && key != 'JACOBO FASJA' && key != 'MIGUEL MARIN'){
			agente_ventas[ejecutivos_slack[key]]=[];
			if($.inArray(key,arr_agente_ventas) == -1){
				arr_agente_ventas.push(key);
			}
		}else if(key != 'DAN BECKER'){
			agente_admin[ejecutivos_slack[key]]=[];
			if($.inArray(key,arr_agente_admin) == -1){
				arr_agente_admin.push(key);
			}
		}else{
			///TO DO what about me?
		}
	});
	for(var i=0;i<ventas.length;i++){
		if($.inArray(ventas[i].agent, arr_agente_ventas) != -1 && $.inArray(ventas[i].status, arr_dont_notify_status) == -1){
			agente_ventas[ejecutivos_slack[ventas[i].agent]].push(
				ventas[i].client+'\t\t\t'+ventas[i].agency+'\t\t\t'+ventas[i].campaign+'\t\t\t'+ventas[i].status+'\n'
			);
		}
		if($.inArray(ventas[i].agent, arr_agente_admin) != -1 && $.inArray(ventas[i].status, arr_dont_notify_status_admin) == -1){
			agente_admin[ejecutivos_slack[ventas[i].agent]].push(
				ventas[i].client+'\t\t\t'+ventas[i].agency+'\t\t\t'+ventas[i].campaign+'\t\t\t'+ventas[i].status+'\n'
			);
		}
	}
	$.each(agente_ventas,function(key, value){
		var text='';
		for(var i=0;i<agente_ventas[key].length;i++){
			text+=(i+1)+'.-  '+agente_ventas[key][i];
		}
		if(agente_ventas[key].length > 0){
			$.post(
				"https://hooks.slack.com/services/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",
				JSON.stringify({
					"channel": key,
					"username": 'Revisión de campañas',
					"attachments": [{
						"fallback": 'Revisión de campañas',
						"color": "danger",
						"author_name": 'Campaign Bot',
						"title": 'Revisión de campañas',
						"text": text
					}]
				})
			);//Send Slack Messages
		}
	});
	$.each(agente_admin,function(key, value){
		var text='';
		for(var i=0;i<agente_admin[key].length;i++){
			text+=(i+1)+'.-  '+agente_admin[key][i];
		}
		if(agente_admin[key].length > 0){
			$.post(
				"https://hooks.slack.com/services/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",
				JSON.stringify({
					"channel": key,
					"username": 'Revisión de campañas',
					"attachments": [{
						"fallback": 'Revisión de campañas',
						"color": "danger",
						"author_name": 'Campaign Bot',
						"title": 'Revisión de campañas',
						"text": text
					}]
				})
			);//Send Slack Messages
		}
	});
}

var mails;
var mails_requests;
function dickWithAgencies(){
    mails={};
    var distinct_contacts = [...new Set(ventas.map(item => item.contact))];
    //const dont_send_mail_status = ['Pagado 1/4','Pagado','Pagado -IVA','Facturado (ingresar sistema)','Facturado (revision)','PTE Pago','Sin respuesta','ODC Cerrada','Detenido','PTE DO','Indicacion para facturar'];
    const dont_send_mail_status = ['Pagado','Pagado -IVA','Facturado (revision)','PTE Pago','Facturado','Facturado (ingresar)'];
    
    for(var i=0;i<distinct_contacts.length;i++){
        if(typeof distinct_contacts[i] !== 'undefined' && distinct_contacts[i] !== 'pending'){
            mails[distinct_contacts[i]] = [];
        }
    }
    
    for(var i=0;i<ventas.length;i++){
        if($.inArray(ventas[i].status,dont_send_mail_status) == -1 && typeof ventas[i].contact !== 'undefined' && ventas[i].contact !== 'pending'){
            mails[ventas[i].contact].push(
                ventas[i]
            );
        }else{
            if((ventas[i].status == 'Facturado' || ventas[i].status == 'PTE Pago') && ventas[i].days_left < -30){
                mails[ventas[i].contact].push(
                    ventas[i]
                );    
            }
        }
    }
    
    $.each(mails,function(key,value){
        if(mails[key].length ==0){
            delete mails[key];
        }
    });
    
    mails_requests = [];
    var j=0;
    $('#emails-modal-body').empty();
    $.each(mails,function(key,value){
        mails[key].RFCmessage = 'to: '+mails[key][0].contact+'\r\n';
//        mails[key].RFCmessage = 'to: danbecker87@gmail.com\r\n';
        mails[key].RFCmessage += 'cc: '+(typeof mails[key][0].cc !== 'undefined' ? mails[key][0].cc : '')+'\r\n';
        mails[key].RFCmessage += 'from: me\r\n';
        mails[key].RFCmessage += 'Subject: Pendientes '+mails[key][0].agency+' para la campañas con NNM\r\n\r\n';
        mails[key].RFCmessage += 'Buenas tardes,\r\n\r\n Esta recibiendo este correo para dar seguimiento a los siguientes asuntos pendientes por actividad con Now New Media:\r\n\r\n';
        mails[key].RFCmessage += '\t\t'+'Cliente'+'\t\t'+'Fecha de venta'+'\t\t'+'Campaña'+'\t\t'+'Monto'+'\t\t'+'Status\r\n\r\n';
        for(var i=0;i<mails[key].length;i++){
            mails[key].RFCmessage += 
                (i+1)+'.-'+'\t\t'+
                mails[key][i].client+'\t\t'+
                monthName[new Date(mails[key][i].sell_date).getMonth()]+'-'+new Date(mails[key][i].sell_date).getFullYear()+'\t\t'+
                mails[key][i].campaign+'\t\t'+
                '$ '+numberWithCommas(mails[key][i].ammount)+'\t\t'+
                mails[key][i].note+'\r\n\r\n';
        }
        mails[key].RFCmessage += 'Un cordial saludo,\r\n\r\nDan Becker\r\n\r\nData & Accounting | Now New Media\r\n\r\nT. 5245-1911\r\nT. 3693-4591';
        mails_requests.push(
            {
                mail: gapi.client.gmail.users.messages.send({
                    'userId': 'me',
                    'resource': {
                        'raw': window.btoa(mails[key].RFCmessage).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '')
                    }
                }),
                agency: mails[key][0].agency
            }
        );
        
        //Add row to modal window
        $('#emails-modal-body').append(
            '<div class="row">'+
                '<div class="col-md-10">'+
                    mails[key][0].agency+'&nbsp&nbsp'+mails[key][0].contact.substr(0,mails[key][0].contact.indexOf('<'))+'&nbsp&nbsp'+mails[key].length+
                '</div>'+
                '<div class="col-md-2">'+
                    '<button class="btn btn-rounded btn-success" onclick="sendEmail('+j+')">Send</button>'+
                '</div>'+
            '</div>'
        );
        j++;
    });    
}

function sendEmail(index){
    mails_requests[index].mail.execute(function(resp){
        if (resp.error && resp.error.status) {
            // The API encountered a problem before the script started executing.
            alert('Error calling mail API');
            console.log('Error calling API: ' + JSON.stringify(resp, null, 2));
            console.log('Failed to sent mail to '+mails_requests[index].agency);
        } else if (resp.error) {
            // The API executed, but the script returned an error.
            alert('Mail failed to send: '+resp.error.message);
            console.log('Script error! Message: ' + resp.error.message);
            console.log('Failed to sent mail to '+mails_requests[index].agency);
        } else {
            console.log('Succesfully sent mail to '+mails_requests[index].agency);
        }
    });
}
