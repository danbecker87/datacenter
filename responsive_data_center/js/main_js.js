//active page
function getActivePage(){
	$(document).ready(function(){
		var page_url=window.location.href;
		//console.log($('#sidebar-menu')[0].childNodes[1].childNodes[1]);
		for(var i=3;i<$('#sidebar-menu')[0].childNodes[1].childNodes.length;i+=2){
			var sidebar_tag=$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].href;
			//console.log(sidebar_tag,page_url);
			if(sidebar_tag.indexOf(page_url) > -1){
				$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].className = "waves-effect active";
			}
			//console.log(i);
		}
	});
}
//active page

//ColapseSidebar
function colapseSidebar(){
    $('.button-menu-mobile')[0].click();
}
//ColapseSidebar

//Revenue
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
function Yesterday(){
	var date=new Date(new Date().getTime()-1000*60*60*(6+24));
	return date.toISOString().substring(0,new Date().toISOString().indexOf("T"))
}
function changeDeal(me){
	document.getElementById("adx_deal_type").innerHTML=me.innerHTML;
}
function getNetADXRevenue(){
	var revenue, revenue_yesterday;
	
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: "today",
			endDate: "today",
			metric: ["Earnings"],
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			revenue=parseFloat(resp.totals[0]);
			document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(revenue.toFixed(2));
							
			var requestYesterdaysRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
				accountId: "pub-7894268851903152",
				startDate: Yesterday(),
				endDate: Yesterday(),
				metric: ["Earnings"],
				fields: "totals"
			});
			requestYesterdaysRevenue.execute(function(resp) {
				revenue_yesterday=parseFloat(resp.totals[0]);
				if(revenue - revenue_yesterday > 0){
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-up";
					document.getElementById("adx_revenue_badge").className = "badge badge-success pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((revenue/revenue_yesterday-1)*100).toFixed(1);
				}else{
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-down";
					document.getElementById("adx_revenue_badge").className = "badge badge-danger pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((1-revenue/revenue_yesterday)*100).toFixed(1);
				}
			});
		});
	});
}
function getOpenAuctionADXRevenue(){
	document.getElementById("adx_revenue_amount").innerHTML="Open";
	
	var revenue, revenue_yesterday;
	
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: "today",
			endDate: "today",
			metric: ["Earnings"],
			filter: "TRANSACTION_TYPE_NAME==Subasta abierta,TRANSACTION_TYPE_NAME==Open auction",
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			revenue=parseFloat(resp.totals[0]);
			document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(revenue.toFixed(2));
		
			var requestYesterdaysRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
				accountId: "pub-7894268851903152",
				startDate: Yesterday(),
				endDate: Yesterday(),
				metric: ["Earnings"],
				filter: "TRANSACTION_TYPE_NAME==Subasta abierta,TRANSACTION_TYPE_NAME==Open auction",
				fields: "totals"
			});
			requestYesterdaysRevenue.execute(function(resp) {
				revenue_yesterday=parseFloat(resp.totals[0]);
				if(revenue - revenue_yesterday > 0){
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-up";
					document.getElementById("adx_revenue_badge").className = "badge badge-success pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((revenue/revenue_yesterday-1)*100).toFixed(1);
				}else{
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-down";
					document.getElementById("adx_revenue_badge").className = "badge badge-danger pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((1-revenue/revenue_yesterday)*100).toFixed(1);
				}
			});
		});
	});
}
function getPrivateAuctionADXRevenue(){
	document.getElementById("adx_revenue_amount").innerHTML="Private";
	
	var revenue, revenue_yesterday;
	
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: "today",
			endDate: "today",
			metric: ["Earnings"],
			filter: "TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction",
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			revenue=parseFloat(resp.totals[0]);
			document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(revenue.toFixed(2));
		
			var requestYesterdaysRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
				accountId: "pub-7894268851903152",
				startDate: Yesterday(),
				endDate: Yesterday(),
				metric: ["Earnings"],
				filter: "TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction",
				fields: "totals"
			});
			requestYesterdaysRevenue.execute(function(resp) {
				revenue_yesterday=parseFloat(resp.totals[0]);
				if(revenue - revenue_yesterday > 0){
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-up";
					document.getElementById("adx_revenue_badge").className = "badge badge-success pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((revenue/revenue_yesterday-1)*100).toFixed(1);
				}else{
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-down";
					document.getElementById("adx_revenue_badge").className = "badge badge-danger pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((1-revenue/revenue_yesterday)*100).toFixed(1);
				}
			});
		});
	});
}
function getPreferredDealsADXRevenue(){
	document.getElementById("adx_revenue_amount").innerHTML="Preferred";
	var revenue, revenue_yesterday;
	
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: "today",
			endDate: "today",
			metric: ["Earnings"],
			filter: "TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal",
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			revenue=parseFloat(resp.totals[0]);
			document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(revenue.toFixed(2));
		
			var requestYesterdaysRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
				accountId: "pub-7894268851903152",
				startDate: Yesterday(),
				endDate: Yesterday(),
				metric: ["Earnings"],
				filter: "TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal",
				fields: "totals"
			});
			requestYesterdaysRevenue.execute(function(resp) {
				revenue_yesterday=parseFloat(resp.totals[0]);
				if(revenue - revenue_yesterday > 0){
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-up";
					document.getElementById("adx_revenue_badge").className = "badge badge-success pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((revenue/revenue_yesterday-1)*100).toFixed(1);
				}else{
					document.getElementById("revenue_trend").className = "zmdi zmdi-trending-down";
					document.getElementById("adx_revenue_badge").className = "badge badge-danger pull-left m-t-20";
					document.getElementById("adx_percentage").innerHTML = ((1-revenue/revenue_yesterday)*100).toFixed(1);
				}
			});
		});
	});
}
//revenue

//AdMan Revenue
var adman_revenue_today, adman_revenue_yersterday, adman_month_revenue=0, adman_revenue_last_month=0, adman_revenue_last_month_day=0;
function admanRevenueToday(){
	var today=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var yesterday=new Date(today.getTime()-24*60*60*1000);
	//console.log(today,yesterday);
    
	$.ajax({
		url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+today.toISOString().substr(0,10).replace(/-/g,'')+'&to='+today.toISOString().substr(0,10).replace(/-/g,'')+'&page=1&per_page=40',
		type: 'get',
		async: false,
		success: function(response){
			if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
				adman_revenue_today = response.totals.impression_USD/1000;
			}
		}
	});
	$.ajax({
		url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+yesterday.toISOString().substr(0,10).replace(/-/g,'')+'&to='+yesterday.toISOString().substr(0,10).replace(/-/g,'')+'&page=1&per_page=40',
		type: 'get',
		async: false,
		success: function(response){
			if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
				adman_revenue_yesterday = response.totals.impression_USD/1000;
			}
		}
	});
	$('#Adman_revenue')[0].innerHTML = numberWithCommas(adman_revenue_today.toFixed(2));
	$('#Adman_yesterday_revenue')[0].innerHTML = numberWithCommas(adman_revenue_yesterday.toFixed(2));
	if(adman_revenue_today-adman_revenue_yesterday > 0){
		document.getElementById("Adman_revenue_trend").className = "zmdi zmdi-trending-up";
		document.getElementById("Adman_revenue_badge").className = "badge badge-success pull-left m-t-20";
		document.getElementById("Adman_revenue_change").innerHTML = ((adman_revenue_today/adman_revenue_yesterday-1)*100).toFixed(1);
	}else{
		document.getElementById("Adman_revenue_trend").className = "zmdi zmdi-trending-down";
		document.getElementById("Adman_revenue_badge").className = "badge badge-danger pull-left m-t-20";
		document.getElementById("Adman_revenue_change").innerHTML = ((1-adman_revenue_today/adman_revenue_yesterday)*100).toFixed(1);
	}
	
}
function admanRevenueMonth(){
	adman_month_revenue=0;
	adman_revenue_last_month=0;
	adman_revenue_last_month_day=0;
	var nowDate = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var now=parseInt(new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000).toISOString().replace(/-/g,''));
	var startOfMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth(),1).toISOString().replace(/-/g,''));
	var startOfLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth()-1,1).toISOString().replace(/-/g,''));
	var nowLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth()-1,nowDate.getDate()).toISOString().replace(/-/g,''));
	var endOfLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth(),0).toISOString().replace(/-/g,''));
	//console.log(now,startOfMonth,startOfLastMonth,endOfLastMonth,nowLastMonth);
	
    $.ajax({
		url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+startOfMonth+'&to='+now+'&page=1&per_page=40',
		type: 'get',
		async: false,
		success: function(response){
			if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
				adman_month_revenue = response.totals.impression_USD/1000;
                $('#Adman_month_revenue')[0].innerHTML = '$ '+numberWithCommas(adman_month_revenue.toFixed(2));
			}
		}
	});
	
    $.ajax({
		url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+startOfLastMonth+'&to='+endOfLastMonth+'&page=1&per_page=40',
		type: 'get',
		async: false,
		success: function(response){
			if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
				adman_revenue_last_month = response.totals.impression_USD/1000;
                compareAdmanRevenues();
			}
		}
	});
	
	function compareAdmanRevenues(){
        
        $.ajax({
            url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+startOfLastMonth+'&to='+nowLastMonth+'&page=1&per_page=40',
            type: 'get',
            async: false,
            success: function(response){
                if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
                    adman_revenue_last_month_day = response.totals.impression_USD/1000;
                    $('#Adman_last_month_revenue')[0].innerHTML = numberWithCommas(adman_revenue_last_month.toFixed(2));
                    if(adman_month_revenue-adman_revenue_last_month_day > 0){
                        document.getElementById("Adman_month_revenue_trend").className = "zmdi zmdi-trending-up";
                        document.getElementById("Adman_month_revenue_badge").className = "badge badge-success pull-left m-t-20";
                        document.getElementById("Adman_month_revenue_change").innerHTML = ((adman_month_revenue/adman_revenue_last_month_day-1)*100).toFixed(1);
                    }else{
                        document.getElementById("Adman_month_revenue_trend").className = "zmdi zmdi-trending-down";
                        document.getElementById("Adman_month_revenue_badge").className = "badge badge-danger pull-left m-t-20";
                        document.getElementById("Adman_month_revenue_change").innerHTML = ((1-adman_month_revenue/adman_revenue_last_month_day)*100).toFixed(1);
                        getTotalRevenue();
                    }
                }
            }
        });
	}
}
//AdMan Revenue

//Taboola Revenue
var taboola_month, taboola_last_month, taboola_yesterdays, taboola_last_month_day;
var taboola_revenue_today, taboola_revenue_yersterday, taboola_month_revenue=0, taboola_last_month_revenue=0, taboola_revenue_last_month_day=0;
function getTaboolaRevenueToday(){
	var since = new Date((new Date()-new Date().getTimezoneOffset()*60*1000)-2*24*60*60*1000).toISOString().substr(0,10);
	var until = new Date((new Date()-new Date().getTimezoneOffset()*60*1000)-1*24*60*60*1000).toISOString().substr(0,10);
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				data: {'start_date': since, 'end_date': until, 'access_token': response.access_token},
				success: function(response){
					taboola_yesterdays = JSON.parse(response);
					//console.log(taboola_yesterdays);
					$('#Taboola_revenue')[0].innerHTML = numberWithCommas(taboola_yesterdays.results[0].ad_revenue.toFixed(2));
					$('#Taboola_yesterday_revenue')[0].innerHTML = numberWithCommas(taboola_yesterdays.results[1].ad_revenue.toFixed(2));
					if(taboola_yesterdays.results[0].ad_revenue-taboola_yesterdays.results[1].ad_revenue > 0){
						document.getElementById("Taboola_revenue_trend").className = "zmdi zmdi-trending-up";
						document.getElementById("Taboola_revenue_badge").className = "badge badge-success pull-left m-t-20";
						document.getElementById("Taboola_revenue_change").innerHTML = ((taboola_yesterdays.results[0].ad_revenue/taboola_yesterdays.results[1].ad_revenue-1)*100).toFixed(1);
					}else{
						document.getElementById("Taboola_revenue_trend").className = "zmdi zmdi-trending-down";
						document.getElementById("Taboola_revenue_badge").className = "badge badge-danger pull-left m-t-20";
						document.getElementById("Taboola_revenue_change").innerHTML = ((1-taboola_yesterdays.results[0].ad_revenue/taboola_yesterdays.results[1].ad_revenue)*100).toFixed(1);
					}
				}
			});
		}
	});
}
function getMonthTaboolaRevenue(){
	var nowDate = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var now=nowDate.toISOString().substr(0,10);
	var startOfMonth = new Date(nowDate.getFullYear(),nowDate.getMonth(),1).toISOString().substr(0,10);
	var startOfLastMonth = new Date(nowDate.getFullYear(),nowDate.getMonth()-1,1).toISOString().substr(0,10);
	var nowLastMonth = new Date(nowDate.getFullYear(),nowDate.getMonth()-1,nowDate.getDate()-1).toISOString().substr(0,10);
	var endOfLastMonth = new Date(nowDate.getFullYear(),nowDate.getMonth(),0).toISOString().substr(0,10);
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				async: false,
				data: {'start_date': startOfMonth, 'end_date': now, 'access_token': response.access_token},
				success: function(response){
					taboola_month = JSON.parse(response);
					//console.log(taboola_month);
					taboola_month_revenue=0;
					for(var i=0;i<taboola_month.results.length;i++){
						taboola_month_revenue+=taboola_month.results[i].ad_revenue;
					}
					$('#Taboola_month_revenue')[0].innerHTML = numberWithCommas(taboola_month_revenue.toFixed(2));
				}
			});
		}
	});
	
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				async: false,
				data: {'start_date': startOfLastMonth, 'end_date': nowLastMonth, 'access_token': response.access_token},
				success: function(response){
					taboola_last_month_day = JSON.parse(response);
					//console.log(taboola_month);
					taboola_revenue_last_month_day=0;
					for(var i=0;i<taboola_last_month_day.results.length;i++){
						taboola_revenue_last_month_day+=taboola_last_month_day.results[i].ad_revenue;
						if(taboola_month_revenue-taboola_revenue_last_month_day > 0){
							document.getElementById("Taboola_month_revenue_trend").className = "zmdi zmdi-trending-up";
							document.getElementById("Taboola_month_revenue_badge").className = "badge badge-success pull-left m-t-20";
							document.getElementById("Taboola_month_revenue_change").innerHTML = ((taboola_month_revenue/taboola_revenue_last_month_day-1)*100).toFixed(1);
						}else{
							document.getElementById("Taboola_month_revenue_trend").className = "zmdi zmdi-trending-down";
							document.getElementById("Taboola_month_revenue_badge").className = "badge badge-danger pull-left m-t-20";
							document.getElementById("Taboola_month_revenue_change").innerHTML = ((1-taboola_month_revenue/taboola_revenue_last_month_day)*100).toFixed(1);
							getTotalRevenue();
						}
					}
					
				}
			});
		}
	});
		
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				async: false,
				data: {'start_date': startOfLastMonth, 'end_date': endOfLastMonth, 'access_token': response.access_token},
				success: function(response){
					taboola_last_month = JSON.parse(response);
					//console.log(taboola_last_month);
					taboola_last_month_revenue=0;
					for(var i=0;i<taboola_last_month.results.length;i++){
						taboola_last_month_revenue+=taboola_last_month.results[i].ad_revenue;
					}
					$('#Taboola_last_month_revenue')[0].innerHTML = numberWithCommas(taboola_last_month_revenue.toFixed(2));
				}
			});
		}
	});
}
//Taboola Revenue

//Active Users
var active_users=[];
active_users[0]=0;
active_users[1]=0;
var active_users_paid=[];
active_users_paid[0]=0;
active_users_paid[1]=0;
var active_users_organic=[];
active_users_organic[0]=0;
active_users_organic[1]=0;

var active_sites=[];
active_sites[0]=[];
active_sites[1]=[['',0],['',0],['',0],['',0],['',0],['',0],['',0],['',0],['',0],['',0]];
var active_sites_organic=[];
active_sites_organic[0]=[];
active_sites_organic[1]=[['',0],['',0],['',0],['',0],['',0],['',0],['',0],['',0],['',0],['',0]];
function getActiveUsers(){
	gapi.client.load('analytics','v3', function(){
		var requestUsers = gapi.client.analytics.data.realtime.get({
			ids: "ga:41142760",
			metrics: "rt:activeUsers",
			dimensions: "rt:pagePath",
			"max-results": 10,
			sort: "-rt:activeUsers",
			fields: "rows,totalsForAllResults",
			output: 'json'
		});
		requestUsers.then(function(resp){
			//console.log(resp.result.rows);
			active_users[1]= active_users[0];
			active_users[0]=resp.result.totalsForAllResults["rt:activeUsers"];
			if(active_sites[0].length > 0){
				active_sites[1]=active_sites[0];
			}else{
			
			}
			active_sites[0]=resp.result.rows;
			//document.getElementById("active_users").innerHTML=active_users[0];
			jq1102('#active_users').numerator({
				easing: 'swing',
				duration: 1000,
				toValue: parseInt(active_users[0])
			});
			if(active_users[0]-active_users[1] > 0){
				document.getElementById("active_users_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("active_users_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("active_users_change").innerHTML = parseInt(active_users[0])-parseInt(active_users[1]);
				var glow = jq1102('#active_users');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('increase_users',true);
			}else{
				document.getElementById("active_users_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("active_users_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("active_users_change").innerHTML = parseInt(active_users[1])-parseInt(active_users[0]);
				var glow = jq1102('#active_users');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('decrease_users',true);
			}
			var myNode = document.getElementById("active_table_rows");
			while (myNode.firstChild) {
				myNode.removeChild(myNode.firstChild);
			}
			for(var i=0;i<resp.result.rows.length;i++){
				var tr=document.createElement("tr");
				i%2 == 0 ? tr.setAttribute('style','background-color: white;') : tr.setAttribute('style','background-color: #E0E0E0;');
				var td1=document.createElement("td");
				td1.appendChild(document.createTextNode((i+1)));
				tr.appendChild(td1);
				var td2=document.createElement("td");
				var a=document.createElement("a");
				a.setAttribute('title',resp.result.rows[i][0]);
				a.setAttribute('href','https://eldeforma.com'+resp.result.rows[i][0]);
				a.setAttribute('target','_blank');
				a.appendChild(document.createTextNode(shortenedURL(resp.result.rows[i][0])));
				td2.appendChild(a);
				tr.appendChild(td2);
				var td3=document.createElement("td");
				td3.appendChild(document.createTextNode(resp.result.rows[i][1]));
				tr.appendChild(td3);
				var td4=document.createElement("td");
				var span=document.createElement('span');
				var span2=document.createElement('span');
				var i_tag=document.createElement('i');
				if(active_sites[0][i][1]-active_sites[1][i][1] > 0){
					span.setAttribute('class','badge badge-success');
					i_tag.setAttribute('class','zmdi zmdi-trending-up');
					tr.setAttribute('class','top_site_up');
				}else{
					if(active_sites[0][i][1]-active_sites[1][i][1] < 0){
						span.setAttribute('class','badge badge-danger');
						i_tag.setAttribute('class','zmdi zmdi-trending-down');
						tr.setAttribute('class','top_site_down');
					}else{
						span.setAttribute('class','badge badge-warning');
					}
				}
				var top_up = jq1102('.top_site_up');
				top_up.toggleClass('animate_top_site_up',false);
				top_up.toggleClass('animate_top_site_down',false);
				top_up.toggleClass('animate_top_site_up',true);
				var top_down = jq1102('.top_site_down');
				top_down.toggleClass('animate_top_site_up',false);
				top_down.toggleClass('animate_top_site_down',false);
				top_down.toggleClass('animate_top_site_down',true);
				span.appendChild(span2);
				span2.innerHTML = active_sites[0][i][1]-active_sites[1][i][1];
				span.appendChild(i_tag);
				td4.appendChild(span);			
				tr.appendChild(td4);
				myNode.appendChild(tr);
			}
			//getActivePageviews();
			//getActiveUsersToday();
			getActiveUsersPaid();
			getLiveTopPostAuthors(resp.result.rows);
		}, function(reason){
			console.log('Error: ' + reason.result.error.message);
		});
	});
}
function getActiveUsersOrganic(){
	gapi.client.load('analytics','v3', function(){
		var requestUsers = gapi.client.analytics.data.realtime.get({
			ids: "ga:41142760",
			metrics: "rt:activeUsers",
			dimensions: "rt:pagePath",
			"max-results": 10,
			sort: "-rt:activeUsers",
			filters: "rt:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@SRE=1;ga:pagePath!@GUIA=1;ga:pagePath!@CST=1",
			fields: "rows,totalsForAllResults",
			output: 'json'
		});
		requestUsers.then(function(resp){
			//console.log(resp.result.rows);
			active_users_organic[1]= active_users_organic[0];
			active_users_organic[0]=resp.result.totalsForAllResults["rt:activeUsers"];
			if(active_sites_organic[0].length > 0){
				active_sites_organic[1]=active_sites_organic[0];
			}else{
			
			}
			active_sites_organic[0]=resp.result.rows;
			jq1102("#active_users_organic").numerator({
				easing: 'swing',
				duration: 1000,
				toValue: parseInt(active_users_organic[0])
			});
			//console.log(parseInt(active_users_organic[0]));
			//Hue(parseInt(active_users_organic[0]));
			if(active_users_organic[0]-active_users_organic[1] > 0){
				document.getElementById("active_users_organic_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("active_users_organic_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("active_users_organic_change").innerHTML = parseInt(active_users_organic[0])-parseInt(active_users_organic[1]);
				var glow = jq1102('#active_users_organic');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('increase_users',true);
			}else{
				document.getElementById("active_users_organic_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("active_users_organic_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("active_users_organic_change").innerHTML = parseInt(active_users_organic[1])-parseInt(active_users_organic[0]);
				var glow = jq1102('#active_users_organic');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('decrease_users',true);
			}
			var myNode = document.getElementById("active_table_rows");
			while (myNode.firstChild) {
				myNode.removeChild(myNode.firstChild);
			}
			for(var i=0;i<resp.result.rows.length;i++){
				var tr=document.createElement("tr");
				var td1=document.createElement("td");
				td1.appendChild(document.createTextNode((i+1)));
				tr.appendChild(td1);
				i%2 == 0 ? tr.setAttribute('style','background-color: white;') : tr.setAttribute('style','background-color: #E0E0E0;');
				var td2=document.createElement("td");
				var a=document.createElement("a");
				a.setAttribute('title',resp.result.rows[i][0]);
				a.setAttribute('href','https://eldeforma.com'+resp.result.rows[i][0]);
				a.setAttribute('target','_blank');
				a.appendChild(document.createTextNode(shortenedURL(resp.result.rows[i][0])));
				td2.appendChild(a);
				tr.appendChild(td2);
				var td3=document.createElement("td");
				td3.appendChild(document.createTextNode(resp.result.rows[i][1]));
				tr.appendChild(td3);
				var td4=document.createElement("td");
				var span=document.createElement('span');
				var span2=document.createElement('span');
				var i_tag=document.createElement('i');
				if(active_sites_organic[0][i][1]-active_sites_organic[1][i][1] > 0){
					span.setAttribute('class','badge badge-success');
					i_tag.setAttribute('class','zmdi zmdi-trending-up');
					tr.setAttribute('class','top_site_up');
				}else{
					if(active_sites_organic[0][i][1]-active_sites_organic[1][i][1] < 0){
						span.setAttribute('class','badge badge-danger');
						i_tag.setAttribute('class','zmdi zmdi-trending-down');
						tr.setAttribute('class','top_site_down');
					}else{
						span.setAttribute('class','badge badge-warning');
					}
				}
				var top_up = jq1102('.top_site_up');
				top_up.toggleClass('animate_top_site_up',false);
				top_up.toggleClass('animate_top_site_down',false);
				top_up.toggleClass('animate_top_site_up',true);
				var top_down = jq1102('.top_site_down');
				top_down.toggleClass('animate_top_site_up',false);
				top_down.toggleClass('animate_top_site_down',false);
				top_down.toggleClass('animate_top_site_down',true);
				span.appendChild(span2);
				span2.innerHTML = active_sites_organic[0][i][1]-active_sites_organic[1][i][1];
				span.appendChild(i_tag);
				td4.appendChild(span);			
				tr.appendChild(td4);
				myNode.appendChild(tr);
			}
			//getActivePageviews();
			//getActiveUsersToday();
			//getActiveUsersPaid();
			getLiveTopPostAuthors(resp.result.rows);
		}, function(reason){
			console.log('Error: ' + reason.result.error.message);
		});
	});
}
function shortenedURL(text){
	if(text.length<70){
		return text.substring(12,text.length);
	}
	else{
		return text.substring(12,40)+'...'+text.substring(text.length-30,text.length)
	}
}
//active users

//HUE
function Hue(users){
	//console.log('in hue');
	var now = new Date();
	
	var get_light_state = "https://192.168.0.11/api/JwW3yGKlEfylGBG5itVYM-L8jV7g5mS2YTjluAJP/groups/3";
	var set_light_state = "https://192.168.0.11/api/JwW3yGKlEfylGBG5itVYM-L8jV7g5mS2YTjluAJP/groups/3/action";	
	
	//var get_light_state = "https://192.168.1.67/api/JwW3yGKlEfylGBG5itVYM-L8jV7g5mS2YTjluAJP/groups/3";
	//var set_light_state = "https://192.168.1.67/api/JwW3yGKlEfylGBG5itVYM-L8jV7g5mS2YTjluAJP/groups/3/action";	
	
	var brightness;
	
	if(now.getHours() >= 9 && now.getHours() <= 18){
		brightness = 160;
	}else{
		brightness = 255;
	}
	
	if(now.getDay() < 6 && now.getDay() > 0){
		if(now.getHours() >= 9 && now.getHours() <= 18){
			//console.log('in schedule for hue');
			$.ajax({
				url: get_light_state,
				type: 'GET',
				dataType: 'json',
				async: false,
				success: function(response){
					if(!response.action.on || !response.state.all_on){
						$.ajax({
							url: set_light_state,
							type: 'PUT',
							dataType: 'json',
							data: JSON.stringify({
								"on": true
							}),
							async: false,
							success: function(response){
				
							}
						});
					}
				}
			});//turn light on if off
			if(users<3000){
				var x=(users-500)*((0.4-0.66)/(3000-500))+0.66;///red to green {[0.66,0.32],[0.4,0.51]}
				var y=(x-0.66)*((0.51-0.32)/(0.4-0.66))+0.32;//red to green
				$.ajax({
					url: set_light_state,
					type: 'PUT',
					dataType: 'json',
					data: JSON.stringify({
						"sat": 120, 
						"xy": [x,y], 
						"bri": brightness
					}),
					async: false,
					success: function(response){
				
					}
				});
			}
			else{
				if(users<6000){
					var x=(users-3000)*((0.18-0.4)/(6000-3000))+0.4;///green to blue {[0.4,0.51],[0.18,0.05]}
					var y=(x-0.4)*((0.05-0.51)/(0.18-0.4))+0.51;//// green to blue
				
					$.ajax({
						url: set_light_state,
						type: 'PUT',
						dataType: 'json',
						data: JSON.stringify({
							"sat": 120, 
							"xy": [x,y],
							//"effect": "colorloop",
							"bri": brightness
						}),
						async: false,
						success: function(response){
				
						}
					});
				}//// lots of users
				else{
					$.ajax({
						url: set_light_state,
						type: 'PUT',
						dataType: 'json',
						data: JSON.stringify({
							"sat": 120, 
							"xy": [0.0,0.0], 
							"bri": brightness
						}),
						async: false,
						success: function(response){
				
						}
					});
				}/// great amount of users
			}
		}
		else{
            if(now.getHours() >= 19 && now.getHours() <= 23){
                $.ajax({
                    url: get_light_state,
                    type: 'GET',
                    dataType: 'json',
                    async: false,
                    success: function(response){
                        if(response.state.all_on){
                            $.ajax({
                                url: set_light_state,
                                type: 'PUT',
                                dataType: 'json',
                                data: JSON.stringify({
                                    "on": false
                                }),
                                async: false,
                                success: function(response){
                                    console.log('turned office lights off');
                                }
                            });
                        }
                    }
                });//turn light off if on   
            }else{
                $.ajax({
                    url: get_light_state,
                    type: 'GET',
                    dataType: 'json',
                    async: false,
                    success: function(response){
                        if(response.state.any_on){
                            $.ajax({
                                url: set_light_state,
                                type: 'PUT',
                                dataType: 'json',
                                data: JSON.stringify({
                                    "on": false
                                }),
                                async: false,
                                success: function(response){
                                    console.log('turned office lights off');
                                }
                            });
                        }
                    }
                });//turn light off if on   
            }
		}//check if its working hours
	}
	else{
		$.ajax({
			url: get_light_state,
			type: 'GET',
			dataType: 'json',
			async: false,
			success: function(response){
				if(response.state.any_on){
					$.ajax({
						url: set_light_state,
						type: 'PUT',
						dataType: 'json',
						data: JSON.stringify({
							"on": false
						}),
						async: false,
						success: function(response){
							console.log('turned lights off');
						}
					});
				}
			}
		});//turn light off if on
	}
}
//HUE

//active organic-promoted
function getActiveUsersPaid(){
	gapi.client.load('analytics','v3', function(){
		var requestUsers = gapi.client.analytics.data.realtime.get({
			ids: "ga:41142760",
			metrics: "rt:activeUsers",
			dimensions: "rt:pagePath",
			//"max-results": 10,
			sort: "-rt:activeUsers",
			fields: "rows,totalsForAllResults",
			filters: "rt:pagePath=@p1=true,rt:pagePath=@GUA=1,rt:pagePath=@SRE=1,rt:pagePath=@GUIA=1,rt:pagePath=@CST=1",
			output: 'json'
		});
		requestUsers.execute(function(resp){
			active_users_paid[1]=active_users_paid[0];
			active_users_paid[0]=resp.totalsForAllResults["rt:activeUsers"];
			jq1102('#active_users_paid').numerator({
				easing: 'swing',
				duration: 1000,
				toValue: parseInt(active_users_paid[0])
			});
			if(active_users_paid[0]-active_users_paid[1] > 0){
				document.getElementById("active_users_paid_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("active_users_paid_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("active_users_paid_change").innerHTML = parseInt(active_users_paid[0])-parseInt(active_users_paid[1]);
				var glow = jq1102('#active_users_paid');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('increase_users',true);
			}else{
				document.getElementById("active_users_paid_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("active_users_paid_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("active_users_paid_change").innerHTML = parseInt(active_users_paid[1])-parseInt(active_users_paid[0]);
				var glow = jq1102('#active_users_paid');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('decrease_users',true);
			}
			//////////Organic
			active_users_organic[1]=active_users_organic[0];
			active_users_organic[0]=active_users[0]-active_users_paid[0];
			//console.log(active_users_organic[0]);
			jq1102('#active_users_organic').numerator({
				easing: 'swing',
				duration: 1000,
				toValue: parseInt(active_users_organic[0])
			});
			if(active_users_organic[0]-active_users_organic[1] > 0){
				document.getElementById("active_users_organic_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("active_users_organic_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("active_users_organic_change").innerHTML = parseInt(active_users_organic[0])-parseInt(active_users_organic[1]);
				var glow = jq1102('#active_users_organic');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('increase_users',true);
			}else{
				document.getElementById("active_users_organic_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("active_users_organic_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("active_users_organic_change").innerHTML = parseInt(active_users_organic[1])-parseInt(active_users_organic[0]);
				var glow = jq1102('#active_users_organic');
				glow.toggleClass('decrease_users',false);
				glow.toggleClass('increase_users',false);
				glow.toggleClass('decrease_users',true);
			}
		});
	});
}
//active organic-promoted

//realtime Pageviews last 30 min
function getActivePageviews(){
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.realtime.get({
			ids: "ga:41142760",
			metrics: "rt:pageviews",
			dimensions: "rt:minutesAgo",
			sort: "-rt:minutesAgo",
			fields: "rows"
		});
		requestPageviews.execute(function(resp){
			drawChartPageviews(resp.rows);
		});
	});
}
function getActivePageviewsOrganic(){
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.realtime.get({
			ids: "ga:41142760",
			metrics: "rt:pageviews",
			dimensions: "rt:minutesAgo",
			filters: "rt:pagePath!@p1=true,rt:pagePath!@GUA=1;rt:pagePath!@SRE=1;rt:pagePath!@GUIA=1;rt:pagePath!@CST=1",
			sort: "-rt:minutesAgo",
			fields: "rows"
		});
		requestPageviews.execute(function(resp){
			drawChartPageviews(resp.rows);
		});
	});
}
function drawChartPageviews(minutes){

  var data_table = new google.visualization.DataTable();

  data_table.addColumn('string', 'Minutes ago');
  data_table.addColumn('number', 'Pageviews');

  var formating;
  for(var n=0;n<minutes.length;n++){
	data_table.addRow(['-'+minutes[n][0],
	parseInt(minutes[n][1])]);
  }

	formating = {
		title: 'Pageviews',
		//curveType: 'function',
		vAxis: {
			Title: 'Pageviews',
			gridlines: {
				count: 5
			}
		},
		hAxis: {
			title: 'Minutes Ago',
			ticks: [-29,-26,-21,-16,-11,-6,-1]
			//ticks: [{v: minutes[0][0], f:'-29'},{v: minutes[0][4] ,f:'-26'},{v: minutes[0][9] ,f:'-21'},{v: minutes[0][14] ,f:'-16'},{v: minutes[0][19] ,f:'-11'},{v: minutes[0][24] ,f:'-6'},{v: minutes[0][29] ,f:'-1'}]
		},
		legend:{
			position: 'none'
		},
		connectSteps: false
	};

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.SteppedAreaChart(document.getElementById('chart_div_pageviews'));
  table.draw(data_table,formating);	
}
//realtime Pageviews last 30 min

//active Users Today
function getActiveUsersToday(){
	//console.log(date);
	date = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000).toISOString().substr(0,10).replace(/-/g,'');
	$.ajax({
		url: "php_scripts/get_active_users.php",
		type: 'get',
		dataType: "json",
		data: {'date': 'date'+date},
		async: true,
		success: function(response){
			//console.log(response);	
			drawChartActiveUsersToday(response);//time and number
			//console.log(response);
		}
	});//get active users according to selected date
}
function getActiveUsersTodayOrganic(){
	//console.log(date);
	date = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000).toISOString().substr(0,10).replace(/-/g,'');
	$.ajax({
		url: "php_scripts/get_active_users_organic.php",
		type: 'get',
		dataType: "json",
		data: {'date': 'date'+date},
		async: true,
		success: function(response){
			//console.log(response);	
			drawChartActiveUsersTodayOrganic(response);//time and number
			//console.log(response);
		}
	});//get active users according to selected date
}
function drawChartActiveUsersToday(record){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn('datetime', 'Hora');
  data_table.addColumn('number', 'Active Users');

  var formating;
  for(var n=0;n<record.length;n++){
	record[n].record_time=record[n].record_time.replace(' ','T');
	record[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(record[n].record_time);

	data_table.addRow([data_date,
	parseInt(record[n].active)]);
  }

	formating = {
		title: 'Active users '+data_date.toDateString(),
		//curveType: 'function',
		vAxis: {
			//format: 'number'
		},
		hAxis: {
			format: 'M/d HH:mm',
			gridlines:{
				count: -1
			},
			title: 'Hora'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_users'));
  table.draw(data_table,formating);
}
function drawChartActiveUsersTodayOrganic(record){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn('datetime', 'Hora');
  data_table.addColumn('number', 'Active Users');

  var formating;
  for(var n=0;n<record.length;n++){
	record[n].record_time=record[n].record_time.replace(' ','T');
	record[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(record[n].record_time);

	data_table.addRow([data_date,
	parseInt(record[n].active_organic)]);
  }

	formating = {
		title: 'Active users '+data_date.toDateString(),
		//curveType: 'function',
		vAxis: {
			//format: 'number'
		},
		hAxis: {
			format: 'M/d HH:mm',
			gridlines:{
				count: -1
			},
			title: 'Hora'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_users'));
  table.draw(data_table,formating);
}
//active Users Hisotry

//Analytics Metrics
var pageviews, users, metric_flag=true, interval, month_users, month_sessions, month_pageviews, month_metric_flag=1, last_month_users, last_month_sessions, last_month_pageviews, last_full_month_users, last_full_month_sessions, last_full_month_pageviews;
function getAudienceMetrics(){
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "today",
			"end-date": "today",
			metrics: "ga:pageviews,ga:Users",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestPageviews.execute(function(resp){
			pageviews = resp.totalsForAllResults["ga:pageviews"];
			users = resp.totalsForAllResults["ga:Users"];
			//interval = setInterval(function (){changeMetric()},5000);
		});
	});
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": new Date(now.getFullYear(), now.getMonth(), 1).toISOString().substr(0,10),
			"end-date": "today",
			metrics: "ga:pageviews,ga:Users,ga:Sessions",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestPageviews.execute(function(resp){
			month_pageviews = resp.totalsForAllResults["ga:pageviews"];
			month_users = resp.totalsForAllResults["ga:Users"];
			month_sessions = resp.totalsForAllResults["ga:Sessions"];
			//interval = setInterval(function (){changeMetric()},5000);
		});
	});
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			"end-date": new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000).toISOString().substr(0,10),
			metrics: "ga:pageviews,ga:Users,ga:Sessions",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestPageviews.execute(function(resp){
			last_month_pageviews = resp.totalsForAllResults["ga:pageviews"];
			last_month_users = resp.totalsForAllResults["ga:Users"];
			last_month_sessions = resp.totalsForAllResults["ga:Sessions"];
		});
	});
	gapi.client.load('analytics','v3', function(){
		var requestLastFullMonthPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			"end-date": new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10),
			metrics: "ga:pageviews,ga:Users,ga:Sessions",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestLastFullMonthPageviews.execute(function(resp){
			last_full_month_pageviews = resp.totalsForAllResults["ga:pageviews"];
			last_full_month_users = resp.totalsForAllResults["ga:Users"];
			last_full_month_sessions = resp.totalsForAllResults["ga:Sessions"];
			interval = setInterval(function (){changeMetric()},5000);
		});
	});
}
function changeMetric(){
	if(metric_flag){
		document.getElementById("audience_metric").innerHTML = "Pageviews";
		document.getElementById("audience_metric_value").innerHTML = numberWithCommas(pageviews);
		metric_flag=false;
	}else{
		document.getElementById("audience_metric").innerHTML = "Users";
		document.getElementById("audience_metric_value").innerHTML = numberWithCommas(users);
		metric_flag=true;
	}
	switch(month_metric_flag){
		case 1:
			$("#month_audience_metric")[0].innerHTML = "Users "+numberWithCommas(last_full_month_users);
			$("#month_audience_metric_value")[0].innerHTML = numberWithCommas(month_users);
			if(month_users-last_month_users > 0){
				document.getElementById("month_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("month_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("month_change").innerHTML = ((parseInt(month_users)/parseInt(last_month_users)-1)*100).toFixed(2);
			}else{
				document.getElementById("month_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("month_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("month_change").innerHTML = ((1-parseInt(month_users)/parseInt(last_month_users))*100).toFixed(2);
			}
			month_metric_flag=2;
			break;
		case 2:
			$("#month_audience_metric")[0].innerHTML = "Pageviews "+numberWithCommas(last_full_month_pageviews);
			$("#month_audience_metric_value")[0].innerHTML = numberWithCommas(month_pageviews);
			if(month_pageviews-last_month_pageviews > 0){
				document.getElementById("month_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("month_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("month_change").innerHTML = ((parseInt(month_pageviews)/parseInt(last_month_pageviews)-1)*100).toFixed(2);
			}else{
				document.getElementById("month_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("month_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("month_change").innerHTML = ((1-parseInt(month_pageviews)/parseInt(last_month_pageviews))*100).toFixed(2);
			}
			month_metric_flag=3;
			break;
		case 3:	
			$("#month_audience_metric")[0].innerHTML = "Sessions "+numberWithCommas(last_full_month_sessions);
			$("#month_audience_metric_value")[0].innerHTML = numberWithCommas(month_sessions);
			if(month_sessions-last_month_sessions > 0){
				document.getElementById("month_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("month_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("month_change").innerHTML = ((parseInt(month_sessions)/parseInt(last_month_sessions)-1)*100).toFixed(2);
			}else{
				document.getElementById("month_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("month_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("month_change").innerHTML = ((1-parseInt(month_sessions)/parseInt(last_month_sessions))*100).toFixed(2);
			}
			month_metric_flag=1;
			break;
		default:
			month_metric_flag=1;
			break;		
	}
}
function refreshAudienceMetrics(){
	clearInterval(interval);
	getAudienceMetrics();
}
//analytics Metrics

//Facebook balance
function getFBBalance(){
	FB.api(
		'/act_31492866',
		'GET',
		{'fields':'balance'},
		function(response){
			var balance = parseFloat(response.balance)/100;
			document.getElementById('branded-balance').style.width=(balance/8000*100)+'%';
			document.getElementById('number-branded-balance').innerHTML='$'+numberWithCommas(balance);
		}
	);
	FB.api(
		'/act_958749654167036',
		'GET',
		{'fields':'balance'},
		function(response){
			var balance = parseFloat(response.balance)/100;
			document.getElementById('foreclose-balance').style.width=(balance/750*100)+'%';
			document.getElementById('number-foreclose-balance').innerHTML='$'+numberWithCommas(balance);
		}
	);
	FB.api(
		'/act_986080914767243',
		'GET',
		{'fields':'balance'},
		function(response){
			var balance = parseFloat(response.balance)/100;
			document.getElementById('interno-balance').style.width=(balance/10000*100)+'%';
			document.getElementById('number-interno-balance').innerHTML='$'+numberWithCommas(balance);
		}
	);
	FB.api(
		'/act_955415581167110',
		'GET',
		{'fields':'balance'},
		function(response){
			var balance = parseFloat(response.balance)/100;
			document.getElementById('repsodia-balance').style.width=(balance/40000*100)+'%';
			document.getElementById('number-repsodia-balance').innerHTML='$'+numberWithCommas(balance);
		}
	);
    FB.api(
		'/act_1813540365354623',
		'GET',
		{'fields':'balance'},
		function(response){
			var balance = parseFloat(response.balance)/100;
			document.getElementById('partido-balance').style.width=(balance/400*100)+'%';
			document.getElementById('number-partido-balance').innerHTML='$'+numberWithCommas(balance);
		}
	);
    FB.api(
		'/act_191281401594032',
		'GET',
		{'fields':'balance'},
		function(response){
			var balance = parseFloat(response.balance)/100;
			document.getElementById('button-balance').style.width=(balance/750*100)+'%';
			document.getElementById('number-button-balance').innerHTML='$'+numberWithCommas(balance);
		}
	);
}
//Facebook balance

//Month Shares
function monthShares(me){
	var fb_page=me.id;
	var now = new Date();
	var first = new Date(now.getFullYear(), now.getMonth()-0, 1);
	var last = new Date(now.getFullYear(), now.getMonth()+1, 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	document.getElementById('shares_page').innerHTML = me.innerHTML;
	FB.api(
		'/'+fb_page+'/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPage(response.paging.next,posts);
			}
		}
	);
}
function nextPage(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPage(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				document.getElementById('shares_amount').innerHTML = numberWithCommas(shares);
			}
		}
	});
}
//Month Shares

//Facebook reach
function getFBReach(){
	FB.api(
	  '/eldeforma/insights',
	  'GET',
	  {"fields":"values","since":"yesterday","until":"tomorrow 1days","metric":"['page_impressions_unique']","period":"day",'access_token':pageTokens['El Deforma']},
		function(response) {
			var reach_today, reach_yesterday;
			reach_today = response.data[0].values[1].value;
			reach_yesterday = response.data[0].values[0].value;
			document.getElementById("reach").innerHTML=numberWithCommas(reach_today);
			if(reach_today-reach_yesterday > 0){
				document.getElementById("reach_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("reach_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("reach_change").innerHTML = ((reach_today/reach_yesterday-1)*100).toFixed(1);
			}else{
				document.getElementById("reach_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("reach_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("reach_change").innerHTML = ((1-reach_today/reach_yesterday)*100).toFixed(1);
			}
		}
	);
}
//facebook reach

//Facebook Spend
var accounts;
var spend=[];
function getFBSpend(){
	spend=[];
	accounts=[];
	
    FB.api(
      '/me',
      'GET',
      {"fields":"businesses.fields(owned_ad_accounts.fields(name,id,currency,insights.fields(spend).date_preset(this_month)))"},
      function(response) {
          var our_businesses = response.businesses.data.map(busines => busines.owned_ad_accounts.data.map(account => account));
          accounts = [].concat.apply([], our_businesses);
          setFBSpend();
      }
    );
}
function setFBSpend(){
	for(var l=0;l<accounts.length;l++){
		for(var j=0;j<spend.length;j++){
			if(typeof spend[j]!=='undefined'){
				if(accounts[l].name.localeCompare(spend[j].account_name)==0){
					accounts[l].spend=spend[j].spend;
				}
			}
		}
	}
	var myNode = document.getElementById("fb_spend_dropdown");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
	for(var i=0;i<accounts.length;i++){
		addLI(accounts[i].name);
	}
}
function addLI(text){
	var ul = document.getElementById("fb_spend_dropdown");
	var li = document.createElement("li");
	var a = document.createElement("a");
	li.appendChild(a);
	a.appendChild(document.createTextNode(text));
	a.setAttribute('onclick', "printSpend(this);");
	ul.appendChild(li);
}
function printSpend(me){
	var account_name=me.innerHTML;
	document.getElementById("fb_account").innerHTML = account_name;
	for(var i=0;i<accounts.length;i++){
		if(account_name.localeCompare(accounts[i].name)==0){
			if(accounts[i].insights != undefined){
				document.getElementById("fb_spend").innerHTML = numberWithCommas(accounts[i].insights.data[0].spend)+' '+accounts[i].currency;
			}
			else{
				document.getElementById("fb_spend").innerHTML = numberWithCommas(0)+' '+accounts[i].currency;
			}
		}
	}
}
//Facebook Spend

//Month Video Views
function monthVideoViews(me){
	var now = new Date();
	var first = new Date(now.getFullYear(), now.getMonth()-0, 2);
	var last = new Date(now.getFullYear(), now.getMonth()+1, 0);
	last = new Date(last.getTime() + 1000*60*60*24*2);
	document.getElementById('video_views_page').innerHTML = me.innerHTML;
	FB.api(
		'/'+me.id+'/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",'access_token':pageTokens[me.innerHTML]},
		function(response) {
			var videos=[];
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				videos.push(response.data[0].values[i]);
				total_videos+=videos[i].value;
			}
			document.getElementById('video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
}
//Month Video Views

//Daily Video Views
var videos_flag=0;
function dailyVideoViews(){
	var now = new Date();
	var now = new Date(now.getTime()+1000*60*60*24*2-now.getTimezoneOffset()*60*1000);
	var page;
	//document.getElementById('daily_video_views_page').innerHTML = me.innerHTML;
	switch(videos_flag){
		case 0:
			page='eldeforma';
			break;
		case 1:
			page='repsodia';
			break;
		case 2:
			page='techcult';
			break;
		default:
			page='eldeforma';
			break;
	}
	//videos_flag ? page='eldeforma' : page='repsodia';
	FB.api(
		'/'+page+'/insights/page_video_views/day',
		'GET',
		{"since":now.toISOString().substr(0,10)},
		function(response) {
			changeVideos(response.data[0].values[0].value);
		}
	);
}
function changeVideos(videoViews){
	switch(videos_flag){
		case 0:
			document.getElementById("daily_video_views_page").innerHTML = "El Deforma";
			document.getElementById("daily_video_views_amount").innerHTML = numberWithCommas(videoViews);
			videos_flag=1;
			break;
		case 1:
			document.getElementById("daily_video_views_page").innerHTML = "Repsodia";
			document.getElementById("daily_video_views_amount").innerHTML = numberWithCommas(videoViews);
			videos_flag=2;
			break;
		case 2:
			document.getElementById("daily_video_views_page").innerHTML = "Tech Cult";
			document.getElementById("daily_video_views_amount").innerHTML = numberWithCommas(videoViews);
			videos_flag=0;
			break;
		default:
			document.getElementById("daily_video_views_page").innerHTML = "Error";
			document.getElementById("daily_video_views_amount").innerHTML = '-';
			videos_flag=0;
			break;
	}	
}
//Daily Video Views

//Better komfo
var posts_komfo=[];
var posts_komfo_open=[];
var posts_komfo_repsodia=[];
var posts_komfo_deformatv=[];
var posts_komfo_boom=[];
var posts_komfo_repsodia_posts=[];
var adgroups_komfo=[];
var adgroups_comercial = [];
var posts_komfo_cult=[];
var posts_komfo_cult_posts=[];
var posts_komfo_partido_videos=[];
var posts_komfo_partido_posts=[];
var posts_komfo_holixir=[];
var posts_komfo_holixir_posts=[];
var posts_chingonas=[];
var adgroups_qp = [];

function loadKomfo(){
	$.ajax({
		url: "php_scripts/komfo_promoted_posts.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			adgroups_komfo=response;
            adgroups_comercial = adgroups_komfo.filter(ad => ad.ad_account == 'act_31492866');
            adgroups_komfo=adgroups_komfo.filter(ad => ad.ad_account != 'act_31492866');
            adgroups_qp = adgroups_komfo.filter(ad => ad.ad_account == 'act_1824216990953627');
            adgroups_komfo=adgroups_komfo.filter(ad => ad.ad_account != 'act_1824216990953627');
            adgroups_komfo=adgroups_komfo.filter(ad => ad.ad_account != 'act_955415581167110');
            
            //console.log(response);
		}
	});
	$.ajax({
		url: "php_scripts/get_komfo_data.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo=response;
		}
	});	
	if(adgroups_komfo!=null){
		for(var j=0;j<posts_komfo.length;j++){
			for(var k=0;k<adgroups_komfo.length;k++){
				if(posts_komfo[j].id.localeCompare(adgroups_komfo[k].id)==0){
					posts_komfo[j].promoted=true;
				}
			}
		}
        getLinkClicksVSPageviewsPromoted(adgroups_komfo,drawTableKomfo);
	}else{
        drawTableKomfo();
    }
    if(adgroups_comercial!=null){
		for(var j=0;j<posts_komfo.length;j++){
			for(var k=0;k<adgroups_comercial.length;k++){
				if(posts_komfo[j].id.localeCompare(adgroups_comercial[k].id)==0){
					posts_komfo[j].promoted=true;
				}
			}
		}
	}
}
function drawTableKomfo(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	data_table.addColumn({type: 'number', label: 'Orden'});
    data_table.addColumn({type: 'number', label: 'Star'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Time'});
	data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    data_table.addColumn({type: 'number', label: 'Yest $CL/PV'});
	data_table.addColumn({type: 'number', label: 'Vira l Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Org'});
	data_table.addColumn({type: 'number', label: 'Like/Share'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pago'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
    data_table.addColumn({type: 'number', label: 'Penetr'});
	data_table.addColumn({type: 'string', label: 'GCS'});
	data_table.addColumn({type: 'string', label: 'QuickV'});
	data_table.addColumn({type: 'string', label: 'Edit'});
    
	if(adgroups_komfo!=null){
        for(var r=0;r<adgroups_komfo.length-1;r++){
            for(var s=0;s<adgroups_komfo[adgroups_komfo.length-1].click_percent_array.length;s++){
                if(adgroups_komfo[r].id == adgroups_komfo[adgroups_komfo.length-1].click_percent_array[s].id){
                    adgroups_komfo[r].click_percent = adgroups_komfo[adgroups_komfo.length-1].click_percent_array[s].pageviews === 0 ? 0 : adgroups_komfo[r].yesterdays_paid_clicks/adgroups_komfo[adgroups_komfo.length-1].click_percent_array[s].pageviews;
                }
            }
        }
        
        adgroups_komfo.pop();
        
		for(var n=0;n<adgroups_komfo.length;n++){
            adgroups_komfo[n].name=adgroups_komfo[n].name.replace(/"/g, '');
            if(adgroups_komfo[n].ad_account !== 'act_31492866'){
                if(adgroups_komfo[n].currency=="MXN"){
                    currency_factor=1/19.5;
                }
                else{
                    currency_factor=1;
                }
                if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'active'){
                     data_table.addRow([
                         n+1,
                         {v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                         (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                         new Date(adgroups_komfo[n].posted_date),
                         {v: parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor).toFixed(2)},
                         {v: adgroups_komfo[n].click_percent, f: "%"+(100*adgroups_komfo[n].click_percent).toFixed(2)},
                         {v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                         {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                         {v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                         {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                         {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                         {v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                         {v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                         {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                        '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_komfo[n].id+'\')">New Promote</button>'+(parseFloat(adgroups_komfo[n].paid_ctr) > 0 ? '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#predict-modal" onclick="selectPredictCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\',\''+adgroups_komfo[n].name.replace(/"/g, '')+'\',\''+parseFloat(adgroups_komfo[n].paid_ctr)+'\',\''+parseInt(adgroups_komfo[n].paid_imp)+'\')">Predict</button>' : ''),
                         {v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                         adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                         //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                         '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                         '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                   ]);
                }else{
                    if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'pending'){
                        data_table.addRow([
                            n+1,
                            {v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                            (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                            new Date(adgroups_komfo[n].posted_date),
                            null,
                            null,
                            null,
                            null,
                            {v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                            {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                            {v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                            {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                            {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                            {v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                            {v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                            {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                            '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>',
                            {v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                            adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                            '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                            '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                       ]);  
                    }else{
                        adgroups_komfo.splice(n,1);
                        n--;  
                    }
                }
            }
		}
	}

	for(var n=0;n<posts_komfo.length;n++){
		if(posts_komfo[n].promoted==true){

		}
		else
		{		
			data_table.addRow([
				n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)+1,
                {v: parseInt(posts_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+posts_komfo[n].id+'\');toggleKomfoStar('+n+')"><i class="fa '+(posts_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
				(titleShortener(posts_komfo[n].name).link("https://facebook.com/"+posts_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                new Date(posts_komfo[n].posted_date),
                null,
                null,
                null,
                null,
				{v: parseFloat(posts_komfo[n].viral_amp), f: parseFloat(posts_komfo[n].viral_amp)+"x"},
				{v: parseFloat(posts_komfo[n].organic_ctr), f: parseFloat(posts_komfo[n].organic_ctr)+"%"},
				{v: parseInt(posts_komfo[n].shares)+parseInt(posts_komfo[n].likes), f: numberWithCommas(posts_komfo[n].likes)+' / '+numberWithCommas(posts_komfo[n].shares)},
				{v: parseInt(posts_komfo[n].link_clicks), f: numberWithCommas(posts_komfo[n].link_clicks)},
				{v: parseFloat(posts_komfo[n].paid_ctr), f: parseFloat(posts_komfo[n].paid_ctr)+"%"},
				{v: parseFloat(posts_komfo[n].cxw), f: "$"+parseFloat(posts_komfo[n].cxw)},
				{v: parseFloat(posts_komfo[n].cpm), f: "$"+parseFloat(posts_komfo[n].cpm)},
				null,
				'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo[n].id+'\')">Promote</button>',
				{v: parseFloat(posts_komfo[n].penetration), f: parseFloat(posts_komfo[n].penetration)+"%"},
                posts_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+posts_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+posts_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+posts_komfo[n].id+'\')">Hybrid GCS</button>',
				'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+posts_komfo[n].id+'\',\''+posts_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
				'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
			]);
		}
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 3
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_deforma'));

	var formatter_penetration = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 50});
	var formatter_viral_amp = new google.visualization.BarFormat({width: 30, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});
    var formatter_date = new google.visualization.DateFormat({pattern: "MMM-d-yy H:mm"});

    formatter_date.format(data_table, 3);
	formatter_penetration.format(data_table, 17);
	formatter_viral_amp.format(data_table, 8);
	formatter_ctr.format(data_table, 9);
	formatter_paid_ctr.format(data_table, 12);
	formatter_cxw.format(data_table, 13);
	formatter_cpm.format(data_table, 14);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	if(adgroups_komfo!=null){
		for(var n=0;n<adgroups_komfo.length;n++){
			links[n].childNodes[0].setAttribute('title',adgroups_komfo[n].name);
		}
	}
	
	var l=0;
	for(var n=0;n<posts_komfo.length;n++){
		if(posts_komfo[n].promoted==true){
			l++;
		}
		else
		{
			links[n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)-l].childNodes[0].setAttribute('title',posts_komfo[n].name);
		}
	}
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}
function goalClicks(post_data){
	if(post_data.goal!==null){
		return numberWithCommas(post_data.link_clicks)+' / '+numberWithCommas(post_data.goal)+' / '+numberWithCommas(post_data.paid_link_clicks)+' '+'<button class="btn btn-success" data-toggle="modal" data-target="#goal-modal" waves-effect waves-light" onclick="setGoalPostId(\''+post_data.id+'\')">Goal</button>';
	}else{
		return numberWithCommas(post_data.link_clicks)+' / '+numberWithCommas(post_data.paid_link_clicks)+' '+'<button class="btn btn-success" data-toggle="modal" data-target="#goal-modal" waves-effect waves-light" onclick="setGoalPostId(\''+post_data.id+'\')">Goal</button>';
	}
}
function goalLikes(post_data){
	if(post_data.likes_goal!==null){
		return numberWithCommas(post_data.likes)+' / '+numberWithCommas(post_data.likes_goal)+'   -   '+numberWithCommas(post_data.shares);
	}else{
		return numberWithCommas(post_data.likes)+'   -   '+numberWithCommas(post_data.shares);
	}
}
function goalSpend(post_data,factor){
	if(post_data.spend_limit!==null){
		return '$'+numberWithCommas((parseFloat(post_data.spend)*factor).toFixed(2))+' / '+'$'+numberWithCommas((parseFloat(post_data.spend_limit)*factor).toFixed(2));
	}else{
		return '$'+numberWithCommas((parseFloat(post_data.spend)*factor).toFixed(2));
	}
}
function campaignIsCurrentlyActive(campaign){
	var flag;
	$.ajax({
		url: 'https://graph.facebook.com/v'+version+'/'+campaign+'?fields=effective_status,ads.fields(effective_status)&'+access_token,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(response){
            //console.log(response);
            if(response.effective_status != 'ACTIVE'){
               flag={status: false, effective_status: 'not active'};
            }
            else{
                switch(response.ads.data[0].effective_status){
                    case 'ACTIVE':
                        flag={status: true, effective_status: 'active'};
                        break;
                    case 'PENDING_REVIEW':
                        flag={status: true, effective_status: 'pending'};
                        break;
                    default:
                        flag={status: false, effective_status: 'not active'};
                        break;      
                }
            }		
        }
	});
	return flag;	
}
function getDayCXW(campaign){
    var day_cxw;
    $.ajax({
		url: 'https://graph.facebook.com/v'+version+'/'+campaign+'?fields=insights.fields(spend,actions,objective).date_preset(today),name&'+access_token,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(response){
            if(typeof response.insights !== 'undefined'){
                for(var i=0;i<response.insights.data[0].actions.length;i++){
                    if(response.insights.data[0].objective == 'LINK_CLICKS' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'POST_ENGAGEMENT' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'CONVERSIONS' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'PAGE_LIKES' && response.insights.data[0].actions[i].action_type == 'like'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'VIDEO_VIEWS' && response.insights.data[0].actions[i].action_type == 'video_view'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    day_spend = response.insights.data[0].spend;
                }
            }
            else{
                day_cxw=0;
                day_spend=0;
            }
		}
	});
	return {cxw: day_cxw, spend: day_spend};
}
function getLinkClicksVSPageviewsPromoted(ads,next){
    var promises=[];
    ads[ads.length]={click_percent_array: []};
    for(var j=0;j<ads.length-1;j++){
       promises.push(
            $.ajax({
                url: 'https://graph.facebook.com/v'+version+'/'+ads[j].campaign+'?fields=insights.fields(actions).date_preset(yesterday)&'+access_token,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function(response){
                    var click_percent;
                    if(typeof response.insights === 'undefined'){
                        ads[ads.length-1].click_percent_array.push(
                            {
                                pageviews: 0,
                                id: ads[j].id
                            }
                        );
                    }
                    else{
                        for(var i=0;i<response.insights.data[0].actions.length;i++){
                            if(response.insights.data[0].actions[i].action_type == 'link_click'){
                                ads[j].yesterdays_paid_clicks = parseInt(response.insights.data[0].actions[i].value);
                            }
                        }
                        $.ajax({
                            url: 'https://graph.facebook.com/v'+version+'/'+ads[j].id+'?fields=link&'+access_token,
                            type: 'GET',
                            dataType: 'json',
                            async: false,
                            success: function(response) {
                                //console.log(response);
                                var link = response.link.substr(32,response.link.length-32-1);
                                var id = response.id;
                                //console.log(link);
                                if(link.length > 0){
                                    gapi.client.load('analytics','v3').then(function(){
                                       var requestPageviews = gapi.client.analytics.data.ga.get({
                                            ids: "ga:41142760",
                                            "start-date": 'yesterday',
                                            "end-date": "yesterday",
                                            metrics: "ga:pageviews",
                                            dimensions: "ga:pagePath",
                                            filters: "ga:pagePath=@p1=true;ga:pagePath=@"+link,
                                            fields: 'totalsForAllResults'
                                        });
                                        requestPageviews.execute(function(resp){
                                            //console.log(resp);
                                            ads[ads.length-1].click_percent_array.push(
                                                {
                                                    pageviews: parseInt(resp.totalsForAllResults['ga:pageviews']),
                                                    id: id
                                                }
                                            );
                                        });
                                    });
                                }else{
                                    ads[ads.length-1].click_percent_array.push(
                                        {
                                            pageviews: 0,
                                            id: id
                                        }
                                    );
                                }
                            }
                       });
                    }
                }
            })
        ); 
    }
    $.when.apply($,promises).done(function(){
        //drawTableKomfo();
        function marryClickPercentage(){
            if(ads[ads.length-1].click_percent_array.length != ads.length-1){
                setTimeout(function(){marryClickPercentage()},2000);
            }
            else{
                next();
            }
        }
        marryClickPercentage();
        //console.log('promise success')
    });
}
function toggleStarPost(obj,post_id){
    var fav;
    obj.childNodes[0].classList.toggle('fa-starred');
    obj.childNodes[0].classList.toggle('fa-star-o');
    
    if(obj.childNodes[0].classList.contains('fa-starred')){
        fav = 1;
    }else{
        fav = 0;
    }
    
    $.ajax({
		url: 'php_scripts/updateStarred.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': post_id,
            'fav': fav
		},
		success: function(response){
			//   console.log(response);
		}
	});  
}
function toggleKomfoStar(n){
    if(parseInt(posts_komfo[n].star)){
        posts_komfo[n].star = '0';
    }else{
        posts_komfo[n].star = '1';
    }
}
function toggleComercialKomfoStar(n){
    if(parseInt(adgroups_komfo[n].star)){
        adgroups_komfo[n].star = '0';
    }else{
        adgroups_komfo[n].star = '1';
    }
}

function loadStandaloneKomfo(){
	$.ajax({
		url: "php_scripts/komfo_promoted_posts.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			adgroups_komfo=response;
		}
	});
	$.ajax({
		url: "php_scripts/get_komfo_data.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo=response.splice(0,30);
            adgroups_comercial = adgroups_komfo.filter(ad => ad.ad_account == 'act_31492866');
            adgroups_komfo=adgroups_komfo.filter(ad => ad.ad_account != 'act_31492866');
            adgroups_komfo=adgroups_komfo.filter(ad => ad.ad_account != 'act_1824216990953627');
		}
	});	
	if(adgroups_komfo!=null){
		for(var j=0;j<posts_komfo.length;j++){
			for(var k=0;k<adgroups_komfo.length;k++){
				if(posts_komfo[j].id.localeCompare(adgroups_komfo[k].id)==0){
					posts_komfo[j].promoted=true;
				}
			}
		}
        drawTableStandaloneKomfo();
        //getLinkClicksVSPageviewsPromoted(adgroups_komfo,drawTableStandaloneKomfo);
	}else{
        drawTableStandaloneKomfo();
    }
    if(adgroups_comercial!=null){
		for(var j=0;j<posts_komfo.length;j++){
			for(var k=0;k<adgroups_comercial.length;k++){
				if(posts_komfo[j].id.localeCompare(adgroups_comercial[k].id)==0){
					posts_komfo[j].promoted=true;
				}
			}
		}
	}
}
function drawTableStandaloneKomfo(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	//data_table.addColumn({type: 'number', label: 'Orden'});
    //data_table.addColumn({type: 'number', label: 'Star'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    //data_table.addColumn({type: 'datetime', label: 'Time'});
	//data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    //data_table.addColumn({type: 'number', label: 'Yest $CL/PV'});
	//data_table.addColumn({type: 'number', label: 'Vira l Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Org'});
	//data_table.addColumn({type: 'number', label: 'Like/Share'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pago'});
	//data_table.addColumn({type: 'number', label: 'CXW'});
	//data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
    //data_table.addColumn({type: 'number', label: 'Penetr'});
	//data_table.addColumn({type: 'string', label: 'GCS'});
	//data_table.addColumn({type: 'string', label: 'QuickV'});
	//data_table.addColumn({type: 'string', label: 'Edit'});

	if(adgroups_komfo!=null){
		for(var n=0;n<adgroups_komfo.length;n++){
			if(adgroups_komfo[n].currency=="MXN"){
				currency_factor=1/19.5;
			}
			else{
				currency_factor=1;
			}
			if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'active'){
				 data_table.addRow([
					 //n+1,
                     //{v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
					 (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))/*+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>'*/,
                     //new Date(adgroups_komfo[n].posted_date),
					 //{v: parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor).toFixed(5)},
                     {v: getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor).toFixed(5)},
                     {v: getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor).toFixed(2)},
					 //{v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
					 {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
					 //{v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
					 {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
					 {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
					 //{v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
					 //{v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
					 {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
					 '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_komfo[n].id+'\')">New Promote</button>',
                     //{v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
					 //adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
					 //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
					 //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
					 //'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
			   ]);
			}else{
                if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'pending'){
                    data_table.addRow([
                        //n+1,
                        //{v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                        (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))/*+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>'*/,
                        //new Date(adgroups_komfo[n].posted_date),
                        //null,
                        null,
                        null,
                        //{v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                        {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                        //{v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                        {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                        {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                        //{v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                        //{v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                        {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                        '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>',
                        //{v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                        //adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                        //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                        //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                        //'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                   ]);  
                }else{
                    adgroups_komfo.splice(n,1);
                    n--;  
                }
			}
		}
	}

	for(var n=0;n<posts_komfo.length;n++){
		if(posts_komfo[n].promoted==true){

		}
		else
		{		
			data_table.addRow([
				//n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)+1,
                //{v: parseInt(posts_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+posts_komfo[n].id+'\');toggleKomfoStar('+n+')"><i class="fa '+(posts_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
				(titleShortener(posts_komfo[n].name).link("https://facebook.com/"+posts_komfo[n].id))/*+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>'*/,
				//new Date(posts_komfo[n].posted_date),
                //null,
                null,
                null,
				//{v: parseFloat(posts_komfo[n].viral_amp), f: parseFloat(posts_komfo[n].viral_amp)+"x"},
				{v: parseFloat(posts_komfo[n].organic_ctr), f: parseFloat(posts_komfo[n].organic_ctr)+"%"},
				//{v: parseInt(posts_komfo[n].shares)+parseInt(posts_komfo[n].likes), f: numberWithCommas(posts_komfo[n].likes)+' / '+numberWithCommas(posts_komfo[n].shares)},
				{v: parseInt(posts_komfo[n].link_clicks), f: numberWithCommas(posts_komfo[n].link_clicks)},
				{v: parseFloat(posts_komfo[n].paid_ctr), f: parseFloat(posts_komfo[n].paid_ctr)+"%"},
				//{v: parseFloat(posts_komfo[n].cxw), f: "$"+parseFloat(posts_komfo[n].cxw)},
				//{v: parseFloat(posts_komfo[n].cpm), f: "$"+parseFloat(posts_komfo[n].cpm)},
				null,
				'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo[n].id+'\')">Promote</button>',
                //{v: parseFloat(posts_komfo[n].penetration), f: parseFloat(posts_komfo[n].penetration)+"%"},
				//posts_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+posts_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+posts_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+posts_komfo[n].id+'\')">Hybrid GCS</button>',
				//'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+posts_komfo[n].id+'\',\''+posts_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
				//'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
			]);
		}
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': false,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 1
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_deforma'));

	//var formatter_penetration = new google.visualization.BarFormat({width: 15, colorPositive: 'red', max: 50});
	//var formatter_viral_amp = new google.visualization.BarFormat({width: 15, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 15, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 15, colorPositive: 'green', max: 5});
	//var formatter_cxw = new google.visualization.BarFormat({width: 15, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	//var formatter_cpm = new google.visualization.BarFormat({width: 15, colorPositive: 'green'});
    //var formatter_date = new google.visualization.DateFormat({pattern: "H:mm"});

    //formatter_date.format(data_table, 3);
	//formatter_penetration.format(data_table, 17);
	//formatter_viral_amp.format(data_table, 8);
	formatter_ctr.format(data_table, 3);
	formatter_paid_ctr.format(data_table, 5);
	//formatter_cxw.format(data_table, 13);
	//formatter_cpm.format(data_table, 14);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	if(adgroups_komfo!=null){
		for(var n=0;n<adgroups_komfo.length;n++){
			links[n].childNodes[0].setAttribute('title',adgroups_komfo[n].name);
		}
	}
	
	var l=0;
	for(var n=0;n<posts_komfo.length;n++){
		if(posts_komfo[n].promoted==true){
			l++;
		}
		else
		{
			links[n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)-l].childNodes[0].setAttribute('title',posts_komfo[n].name);
		}
	}
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}

function loadKomfoStarred(){
    drawTableKomfoStarred();
}
function drawTableKomfoStarred(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	data_table.addColumn({type: 'number', label: 'Orden'});
    data_table.addColumn({type: 'number', label: 'Star'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Time'});
	data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    data_table.addColumn({type: 'number', label: 'Yest $CL/PV'});
	data_table.addColumn({type: 'number', label: 'Vira l Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Org'});
	data_table.addColumn({type: 'number', label: 'Like/Share'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pago'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
    data_table.addColumn({type: 'number', label: 'Penetr'});
	data_table.addColumn({type: 'string', label: 'GCS'});
	data_table.addColumn({type: 'string', label: 'QuickV'});
	data_table.addColumn({type: 'string', label: 'Edit'});
    
	if(adgroups_komfo!=null){
        for(var r=0;r<adgroups_komfo.length-1;r++){
            for(var s=0;s<adgroups_komfo[adgroups_komfo.length-1].click_percent_array.length;s++){
                if(adgroups_komfo[r].id == adgroups_komfo[adgroups_komfo.length-1].click_percent_array[s].id){
                    adgroups_komfo[r].click_percent = adgroups_komfo[adgroups_komfo.length-1].click_percent_array[s].pageviews === 0 ? 0 : adgroups_komfo[r].yesterdays_paid_clicks/adgroups_komfo[adgroups_komfo.length-1].click_percent_array[s].pageviews;
                }
            }
        }
        
        adgroups_komfo.pop();
        
		for(var n=0;n<adgroups_komfo.length;n++){
            if(adgroups_komfo[n].star == '1'){
                if(adgroups_komfo[n].ad_account !== 'act_31492866'){
                    if(adgroups_komfo[n].currency=="MXN"){
                        currency_factor=1/19.5;
                    }
                    else{
                        currency_factor=1;
                    }
                    if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'active'){
                         data_table.addRow([
                             n+1,
                             {v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                             (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                             new Date(adgroups_komfo[n].posted_date),
                             {v: parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor).toFixed(5)},
                             {v: getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor).toFixed(5)},
                             {v: getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor).toFixed(2)},
                             {v: adgroups_komfo[n].click_percent, f: "%"+(100*adgroups_komfo[n].click_percent).toFixed(2)},
                             {v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                             {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                             {v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                             {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                             {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                             {v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                             {v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                             {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                             '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_komfo[n].id+'\')">New Promote</button>',
                             {v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                             adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                             //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                             '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                             '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                       ]);
                    }else{
                        if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'pending'){
                            data_table.addRow([
                                n+1,
                                {v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                                (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                                new Date(adgroups_komfo[n].posted_date),
                                null,
                                null,
                                null,
                                null,
                                {v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                                {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                                {v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                                {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                                {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                                {v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                                {v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                                {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                                '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>',
                                {v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                                adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                                //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                                '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                                '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                           ]);  
                        }else{
                            adgroups_komfo.splice(n,1);
                            n--;  
                        }
                    }
                }
            }
		}
	}

	for(var n=0;n<posts_komfo.length;n++){
		if(posts_komfo[n].star == '1'){
            if(posts_komfo[n].promoted==true){

            }
            else{		
                data_table.addRow([
                    n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)+1,
                    {v: parseInt(posts_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+posts_komfo[n].id+'\');toggleKomfoStar('+n+')"><i class="fa '+(posts_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                    (titleShortener(posts_komfo[n].name).link("https://facebook.com/"+posts_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                    new Date(posts_komfo[n].posted_date),
                    null,
                    null,
                    null,
                    null,
                    {v: parseFloat(posts_komfo[n].viral_amp), f: parseFloat(posts_komfo[n].viral_amp)+"x"},
                    {v: parseFloat(posts_komfo[n].organic_ctr), f: parseFloat(posts_komfo[n].organic_ctr)+"%"},
                    {v: parseInt(posts_komfo[n].shares)+parseInt(posts_komfo[n].likes), f: numberWithCommas(posts_komfo[n].likes)+' / '+numberWithCommas(posts_komfo[n].shares)},
                    {v: parseInt(posts_komfo[n].link_clicks), f: numberWithCommas(posts_komfo[n].link_clicks)},
                    {v: parseFloat(posts_komfo[n].paid_ctr), f: parseFloat(posts_komfo[n].paid_ctr)+"%"},
                    {v: parseFloat(posts_komfo[n].cxw), f: "$"+parseFloat(posts_komfo[n].cxw)},
                    {v: parseFloat(posts_komfo[n].cpm), f: "$"+parseFloat(posts_komfo[n].cpm)},
                    null,
                    '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo[n].id+'\')">Promote</button>',
                    {v: parseFloat(posts_komfo[n].penetration), f: parseFloat(posts_komfo[n].penetration)+"%"},
                    posts_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+posts_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+posts_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+posts_komfo[n].id+'\')">Hybrid GCS</button>',
                    '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+posts_komfo[n].id+'\',\''+posts_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                    '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                ]);
            }
        }
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 3
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_starred'));

	var formatter_penetration = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 50});
	var formatter_viral_amp = new google.visualization.BarFormat({width: 30, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});
    var formatter_date = new google.visualization.DateFormat({pattern: "MMM-d-yy H:mm"});

    formatter_date.format(data_table, 3);
	formatter_penetration.format(data_table, 17);
	formatter_viral_amp.format(data_table, 8);
	formatter_ctr.format(data_table, 9);
	formatter_paid_ctr.format(data_table, 12);
	formatter_cxw.format(data_table, 13);
	formatter_cpm.format(data_table, 14);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	if(adgroups_komfo!=null){
		for(var n=0;n<adgroups_komfo.length;n++){
            if(adgroups_komfo[n].star == '1'){
                links[n].childNodes[0].setAttribute('title',adgroups_komfo[n].name);
            }
		}
	}
	
	var l=0;
	for(var n=0;n<posts_komfo.length;n++){
        if(posts_komfo[n].star == '1'){
            if(posts_komfo[n].promoted==true){
                l++;
            }
            else
            {
                links[n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)-l].childNodes[0].setAttribute('title',posts_komfo[n].name);
            }
        }
	}
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}

function loadStandaloneKomfoStarred(){
    drawTableStandaloneKomfoStarred();
}
function drawTableStandaloneKomfoStarred(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	data_table.addColumn({type: 'number', label: 'Orden'});
    data_table.addColumn({type: 'number', label: 'Star'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Time'});
	data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    //data_table.addColumn({type: 'number', label: 'Yest $CL/PV'});
	data_table.addColumn({type: 'number', label: 'Viral Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Org'});
	data_table.addColumn({type: 'number', label: 'Like/Share'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pago'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
    data_table.addColumn({type: 'number', label: 'Penetr'});
	//data_table.addColumn({type: 'string', label: 'GCS'});
	data_table.addColumn({type: 'string', label: 'QuickV'});
	//data_table.addColumn({type: 'string', label: 'Edit'});

	if(adgroups_komfo!=null){
		for(var n=0;n<adgroups_komfo.length;n++){
            if(adgroups_komfo[n].star == '1'){
                if(adgroups_komfo[n].currency=="MXN"){
                    currency_factor=1/19.5;
                }
                else{
                    currency_factor=1;
                }
                if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'active'){
                     data_table.addRow([
                         n+1,
                         {v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                         (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                         new Date(adgroups_komfo[n].posted_date),
                         {v: parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].rt_cxw)*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).cxw*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor, f: "$"+(getDayCXW(adgroups_komfo[n].campaign).spend*currency_factor).toFixed(2)},
                         {v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                         {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                         {v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                         {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                         {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                         {v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                         {v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                         {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                         '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_komfo[n].id+'\')">New Promote</button>',
                         {v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                         //adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                         //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                         '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                         //'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                   ]);
                }else{
                    if(campaignIsCurrentlyActive(adgroups_komfo[n].campaign).status && campaignIsCurrentlyActive(adgroups_komfo[n].campaign).effective_status == 'pending'){
                        data_table.addRow([
                            n+1,
                            {v: parseInt(adgroups_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+adgroups_komfo[n].id+'\');toggleComercialKomfoStar('+n+')"><i class="fa '+(adgroups_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                            (titleShortener(adgroups_komfo[n].name).link("https://facebook.com/"+adgroups_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                            new Date(adgroups_komfo[n].posted_date),
                            null,
                            null,
                            null,
                            {v: parseFloat(adgroups_komfo[n].viral_amp), f: parseFloat(adgroups_komfo[n].viral_amp)+"x"},
                            {v: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr), f: isNaN(parseFloat(adgroups_komfo[n].organic_ctr)) ? 0 : parseFloat(adgroups_komfo[n].organic_ctr)+"%"},
                            {v: parseInt(adgroups_komfo[n].shares)+parseInt(adgroups_komfo[n].likes), f: numberWithCommas(adgroups_komfo[n].likes)+' / '+numberWithCommas(adgroups_komfo[n].shares)},
                            {v: parseInt(adgroups_komfo[n].link_clicks), f: goalClicks(adgroups_komfo[n])},
                            {v: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr), f: isNaN(parseFloat(adgroups_komfo[n].paid_ctr)) ? 0 : parseFloat(adgroups_komfo[n].paid_ctr)+"%"},
                            {v: parseFloat(adgroups_komfo[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cxw)*currency_factor).toFixed(5)},
                            {v: parseFloat(adgroups_komfo[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_komfo[n].cpm)*currency_factor).toFixed(2)},
                            {v: parseFloat(adgroups_komfo[n].spend)*currency_factor, f: goalSpend(adgroups_komfo[n],currency_factor)},
                            '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].campaign+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_komfo[n].campaign+'\')">Edit Promote</button>',
                            {v: parseFloat(adgroups_komfo[n].penetration), f: parseFloat(adgroups_komfo[n].penetration)+"%"},
                            //adgroups_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_komfo[n].id+'\')">Hybrid GCS</button>',
                            //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                            '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_komfo[n].id+'\',\''+adgroups_komfo[n].name.replace(/\"/g,'')+'\',\''+adgroups_komfo[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                            //'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                       ]);  
                    }else{
                        adgroups_komfo.splice(n,1);
                        n--;  
                    }
                }
            }
		}
	}

	for(var n=0;n<posts_komfo.length;n++){
        if(posts_komfo[n].star == '1'){
            if(posts_komfo[n].promoted==true){

            }
            else
            {		
                data_table.addRow([
                    n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)+1,
                    {v: parseInt(posts_komfo[n].star), f: '<button class="btn btn-primary waves-effect waves-light" onclick="toggleStarPost(this,\''+posts_komfo[n].id+'\');toggleKomfoStar('+n+')"><i class="fa '+(posts_komfo[n].star == '1' ? 'fa-starred' : 'fa-star-o')+'" style="color: #FFFFFF; font-size: 12px;"></i></button>'},
                    (titleShortener(posts_komfo[n].name).link("https://facebook.com/"+posts_komfo[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                    new Date(posts_komfo[n].posted_date),
                    null,
                    null,
                    null,
                    {v: parseFloat(posts_komfo[n].viral_amp), f: parseFloat(posts_komfo[n].viral_amp)+"x"},
                    {v: parseFloat(posts_komfo[n].organic_ctr), f: parseFloat(posts_komfo[n].organic_ctr)+"%"},
                    {v: parseInt(posts_komfo[n].shares)+parseInt(posts_komfo[n].likes), f: numberWithCommas(posts_komfo[n].likes)+' / '+numberWithCommas(posts_komfo[n].shares)},
                    {v: parseInt(posts_komfo[n].link_clicks), f: numberWithCommas(posts_komfo[n].link_clicks)},
                    {v: parseFloat(posts_komfo[n].paid_ctr), f: parseFloat(posts_komfo[n].paid_ctr)+"%"},
                    {v: parseFloat(posts_komfo[n].cxw), f: "$"+parseFloat(posts_komfo[n].cxw)},
                    {v: parseFloat(posts_komfo[n].cpm), f: "$"+parseFloat(posts_komfo[n].cpm)},
                    null,
                    '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo[n].id+'\')">Promote</button>',
                    {v: parseFloat(posts_komfo[n].penetration), f: parseFloat(posts_komfo[n].penetration)+"%"},
                    //posts_komfo[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+posts_komfo[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+posts_komfo[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+posts_komfo[n].id+'\')">Hybrid GCS</button>',
                    '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+posts_komfo[n].id+'\',\''+posts_komfo[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                    //'<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+posts_komfo[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                ]);
            }
        }
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 3
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_starred'));

	var formatter_penetration = new google.visualization.BarFormat({width: 15, colorPositive: 'red', max: 50});
	var formatter_viral_amp = new google.visualization.BarFormat({width: 15, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 15, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 15, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 15, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 15, colorPositive: 'green'});
    var formatter_date = new google.visualization.DateFormat({pattern: "H:mm"});

    formatter_date.format(data_table, 3);
	formatter_penetration.format(data_table, 17);
	formatter_viral_amp.format(data_table, 8);
	formatter_ctr.format(data_table, 9);
	formatter_paid_ctr.format(data_table, 12);
	formatter_cxw.format(data_table, 13);
	formatter_cpm.format(data_table, 14);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	/*if(adgroups_komfo!=null){
        for(var n=0;n<adgroups_komfo.length;n++){
            if(adgroups_komfo[n].star == '1'){
                links[n].childNodes[0].setAttribute('title',adgroups_komfo[n].name);
            }
        }
	}
	
	var l=0;
	for(var n=0;n<posts_komfo.length;n++){
        if(posts_komfo[n].star == '1'){
            if(posts_komfo[n].promoted==true){
                l++;
            }
            else
            {
                links[n+(adgroups_komfo!=null ?  adgroups_komfo.length : 0)-l].childNodes[0].setAttribute('title',posts_komfo[n].name);
            }
        }
	}*/
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}

function loadKomfo_comercial(){
    $.ajax({
		url: "php_scripts/komfo_promoted_posts.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
            adgroups_comercial = response.filter(ad => (ad.ad_account == 'act_31492866' || ad.ad_account == 'act_955415581167110'));
            //console.log(response);
		}
	});
    if(adgroups_comercial!=null){
        getLinkClicksVSPageviewsPromoted(adgroups_comercial,drawTableKomfoComercial);
	}else{
        drawTableKomfoComercial();      
    }
}
function drawTableKomfoComercial(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	data_table.addColumn({type: 'number', label: 'Orden'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Posted at'});
	data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    data_table.addColumn({type: 'number', label: 'Yest $clicks/PV'});
	data_table.addColumn({type: 'number', label: 'Penetration'});
	data_table.addColumn({type: 'number', label: 'Viral Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Organico'});
	data_table.addColumn({type: 'number', label: 'Likes/Shares'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pagado'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
	data_table.addColumn({type: 'string', label: 'GCS'});
	data_table.addColumn({type: 'string', label: 'Quickview'});
	data_table.addColumn({type: 'string', label: 'Edit'});
    
	if(adgroups_comercial!=null){
        for(var r=0;r<adgroups_comercial.length-1;r++){
            for(var s=0;s<adgroups_comercial[adgroups_comercial.length-1].click_percent_array.length;s++){
                if(adgroups_comercial[r].id == adgroups_comercial[adgroups_comercial.length-1].click_percent_array[s].id){
                    adgroups_comercial[r].click_percent = adgroups_comercial[adgroups_comercial.length-1].click_percent_array[s].pageviews === 0 ? 0 : adgroups_comercial[r].yesterdays_paid_clicks/adgroups_comercial[adgroups_comercial.length-1].click_percent_array[s].pageviews;
                }
            }
        }
        
        adgroups_comercial.pop();
        
		for(var n=0;n<adgroups_comercial.length;n++){
                currency_factor=1;
                if(campaignIsCurrentlyActive(adgroups_comercial[n].campaign).status && campaignIsCurrentlyActive(adgroups_comercial[n].campaign).effective_status == 'active'){
                     data_table.addRow([
                         n+1,
                         (titleShortener(adgroups_comercial[n].name).link("https://facebook.com/"+adgroups_comercial[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                         new Date(adgroups_comercial[n].posted_date),
                         {v: parseFloat(adgroups_comercial[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].rt_cxw)*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_comercial[n].campaign).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_comercial[n].campaign).cxw*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_comercial[n].campaign).spend*currency_factor, f: "$"+(getDayCXW(adgroups_comercial[n].campaign).spend*currency_factor).toFixed(2)},
                         {v: adgroups_comercial[n].click_percent, f: "%"+(100*adgroups_comercial[n].click_percent).toFixed(2)},
                         {v: parseFloat(adgroups_comercial[n].penetration), f: parseFloat(adgroups_comercial[n].penetration)+"%"},
                         {v: parseFloat(adgroups_comercial[n].viral_amp), f: parseFloat(adgroups_comercial[n].viral_amp)+"x"},
                         {v: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? 0 : parseFloat(adgroups_comercial[n].organic_ctr), f: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? 0 : parseFloat(adgroups_comercial[n].organic_ctr)+"%"},
                         {v: parseInt(adgroups_comercial[n].shares)+parseInt(adgroups_comercial[n].likes), f: goalLikes(adgroups_comercial[n])},
                         {v: parseInt(adgroups_comercial[n].link_clicks), f: goalClicks(adgroups_comercial[n])},
                         {v: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? 0 : parseFloat(adgroups_comercial[n].paid_ctr), f: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? 0 : parseFloat(adgroups_comercial[n].paid_ctr)+"%"},
                         {v: parseFloat(adgroups_comercial[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cxw)*currency_factor).toFixed(5)},
                         {v: parseFloat(adgroups_comercial[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cpm)*currency_factor).toFixed(2)},
                         {v: parseFloat(adgroups_comercial[n].spend)*currency_factor, f: goalSpend(adgroups_comercial[n],currency_factor)},
                         '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].campaign+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_comercial[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_comercial[n].id+'\')">New Promote</button>'+((adgroups_comercial[n].client ==  null || adgroups_comercial[n].client_campaign == null) ? '<button class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#client-modal" onclick="getClientKomfo(\''+adgroups_comercial[n].id+'\')">Set Client</button>' : '<button class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#client-modal" onclick="getClientKomfo(\''+adgroups_comercial[n].id+'\')">'+adgroups_comercial[n].client+'</button>'),
                         adgroups_comercial[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_comercial[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_comercial[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_comercial[n].id+'\')">Hybrid GCS</button>',
                         //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                         '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\',\''+adgroups_comercial[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                         '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                   ]);
                }else{
                    if(campaignIsCurrentlyActive(adgroups_comercial[n].campaign).status && campaignIsCurrentlyActive(adgroups_comercial[n].campaign).effective_status == 'pending'){
                        data_table.addRow([
                            n+1,
                            (titleShortener(adgroups_comercial[n].name).link("https://facebook.com/"+adgroups_comercial[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                            new Date(adgroups_comercial[n].posted_date),
                            null,
                            null,
                            null,
                            null,
                            {v: parseFloat(adgroups_comercial[n].penetration), f: parseFloat(adgroups_comercial[n].penetration)+"%"},
                            {v: parseFloat(adgroups_comercial[n].viral_amp), f: parseFloat(adgroups_comercial[n].viral_amp)+"x"},
                            {v: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? 0 : parseFloat(adgroups_comercial[n].organic_ctr), f: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? 0 : parseFloat(adgroups_comercial[n].organic_ctr)+"%"},
                            {v: parseInt(adgroups_comercial[n].shares)+parseInt(adgroups_comercial[n].likes), f: goalLikes(adgroups_comercial[n])},
                            {v: parseInt(adgroups_comercial[n].link_clicks), f: goalClicks(adgroups_comercial[n])},
                            {v: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? 0 : parseFloat(adgroups_comercial[n].paid_ctr), f: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? 0 : parseFloat(adgroups_comercial[n].paid_ctr)+"%"},
                            {v: parseFloat(adgroups_comercial[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cxw)*currency_factor).toFixed(5)},
                            {v: parseFloat(adgroups_comercial[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cpm)*currency_factor).toFixed(2)},
                            {v: parseFloat(adgroups_comercial[n].spend)*currency_factor, f: goalSpend(adgroups_comercial[n],currency_factor)},
                            '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].campaign+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_comercial[n].campaign+'\')">Edit Promote</button>'+((adgroups_comercial[n].client ==  null || adgroups_comercial[n].client_campaign == null) ? '<button class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#client-modal" onclick="getClientKomfo(\''+adgroups_comercial[n].id+'\')">Set Client</button>' : '<button class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#client-modal" onclick="getClientKomfo(\''+adgroups_comercial[n].id+'\')">'+adgroups_comercial[n].client+'</button>'),
                            adgroups_comercial[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_comercial[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_comercial[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_comercial[n].id+'\')">Hybrid GCS</button>',
                            //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                            '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\',\''+adgroups_comercial[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
                            '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                       ]);  
                    }
                }
		}
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 2
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_comercial'));

	var formatter_penetration = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 50});
	var formatter_viral_amp = new google.visualization.BarFormat({width: 30, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});
    var formatter_date = new google.visualization.DateFormat({pattern: "MMM-d-yy H:mm"});

    formatter_date.format(data_table, 2);
	formatter_penetration.format(data_table, 7);
	formatter_viral_amp.format(data_table, 8);
	formatter_ctr.format(data_table, 9);
	formatter_paid_ctr.format(data_table, 12);
	formatter_cxw.format(data_table, 13);
	formatter_cpm.format(data_table, 14);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	if(adgroups_comercial!=null){
		for(var n=0;n<adgroups_comercial.length;n++){
			links[n].childNodes[0].setAttribute('title',adgroups_comercial[n].name);
		}
	}
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}

function setGoalPostId(id){
	$('#goal_post_id')[0].innerHTML = id;
}
function changeGoal(){
	var id = $('#goal_post_id')[0].innerHTML;
	var goal = $('#goal_post_goal')[0].value;
	$.ajax({
		url: 'php_scripts/updateGoal.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'goal': goal,
			'type': $('[name=radioGoal]:checked').val()
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);},1000);
}
function removeGoal(){
	var id = $('#goal_post_id')[0].innerHTML;
	var goal = $('#goal_post_goal')[0].value;
	$.ajax({
		url: 'php_scripts/removeGoal.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'goal': goal,
			'type': $('[name=radioGoal]:checked').val()
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);},1000);
}
function setNamePostId(id){
	$('#name_post_id')[0].innerHTML = id;
}
function changeName(){
	var id = $('#name_post_id')[0].innerHTML;
	var name = $('#post_name')[0].value;
	$.ajax({
		url: 'php_scripts/updateName.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'name': name
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);},1000);
}

function loadKomfo_repsodia(){
	$.ajax({
		url: "php_scripts/get_komfo_data_repsodia.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_repsodia=response;
		}
	});	
	drawTableKomfo_repsodia();
}
function drawTableKomfo_repsodia(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_komfo_repsodia.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_repsodia[n].name)).link("https://facebook.com/"+posts_komfo_repsodia[n].id),
		{v: parseFloat(posts_komfo_repsodia[n].organic_ctr), f: parseFloat(posts_komfo_repsodia[n].organic_ctr)+"%"},
		{v: parseFloat(posts_komfo_repsodia[n].penetration), f: parseFloat(posts_komfo_repsodia[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_repsodia[n].viral_amp), f: parseFloat(posts_komfo_repsodia[n].viral_amp)+"x"},
		parseInt(posts_komfo_repsodia[n].link_clicks),
		parseInt(posts_komfo_repsodia[n].shares),
		parseInt(posts_komfo_repsodia[n].likes),
		parseInt(posts_komfo_repsodia[n].reach),
		parseInt(posts_komfo_repsodia[n].comments),
		parseInt(posts_komfo_repsodia[n].engagement),
		parseInt(posts_komfo_repsodia[n].clicked_to_play),
		parseInt(posts_komfo_repsodia[n]['10s_sound']),
		parseInt(posts_komfo_repsodia[n].reaction_like),
		parseInt(posts_komfo_repsodia[n].reaction_love),
		parseInt(posts_komfo_repsodia[n].reaction_wow),
		parseInt(posts_komfo_repsodia[n].reaction_haha),
		parseInt(posts_komfo_repsodia[n].reaction_sorry),
		parseInt(posts_komfo_repsodia[n].reaction_anger),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_repsodia[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_repsodia'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_komfo_repsodia.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_komfo_repsodia[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_deformatv(){
	$.ajax({
		url: "php_scripts/get_komfo_data_deformatv.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_deformatv=response;
		}
	});	
	drawTableKomfo_deformatv();
}
function drawTableKomfo_deformatv(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_komfo_deformatv.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_deformatv[n].name)).link("https://facebook.com/"+posts_komfo_deformatv[n].id),
		{v: parseFloat(posts_komfo_deformatv[n].penetration), f: parseFloat(posts_komfo_deformatv[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_deformatv[n].viral_amp), f: parseFloat(posts_komfo_deformatv[n].viral_amp)+"x"},
		{v: parseFloat(posts_komfo_deformatv[n].organic_ctr), f: parseFloat(posts_komfo_deformatv[n].organic_ctr)+"%"},
		parseInt(posts_komfo_deformatv[n].link_clicks),
		parseInt(posts_komfo_deformatv[n].shares),
		parseInt(posts_komfo_deformatv[n].likes),
		parseInt(posts_komfo_deformatv[n].comments),
		parseInt(posts_komfo_deformatv[n].reach),
		parseInt(posts_komfo_deformatv[n].engagement),
        '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_deformatv[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_deformatv'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_boom(){
	$.ajax({
		url: "php_scripts/get_komfo_data_boom.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_boom=response;
		}
	});	
	drawTableKomfo_boom();
}
function drawTableKomfo_boom(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  
  

  for(var n=0;n<posts_komfo_boom.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_boom[n].name)).link("https://facebook.com/"+posts_komfo_boom[n].id),
		{v: parseFloat(posts_komfo_boom[n].penetration), f: parseFloat(posts_komfo_boom[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_boom[n].viral_amp), f: parseFloat(posts_komfo_boom[n].viral_amp)+"x"},
		{v: parseFloat(posts_komfo_boom[n].organic_ctr), f: parseFloat(posts_komfo_boom[n].organic_ctr)+"%"},
		parseInt(posts_komfo_boom[n].link_clicks),
		parseInt(posts_komfo_boom[n].shares),
		parseInt(posts_komfo_boom[n].likes),
		parseInt(posts_komfo_boom[n].comments),
		parseInt(posts_komfo_boom[n].reach),
		parseInt(posts_komfo_boom[n].engagement)
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_boom'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_repsodiaPosts(){
	$.ajax({
		url: "php_scripts/get_komfo_data_repsodia_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_repsodia_posts=response;
		}
	});	
	drawTableKomfo_repsodia_posts();
}
function drawTableKomfo_repsodia_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_komfo_repsodia_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_repsodia_posts[n].name)).link("https://facebook.com/"+posts_komfo_repsodia_posts[n].id),
		{v: parseFloat(posts_komfo_repsodia_posts[n].penetration), f: parseFloat(posts_komfo_repsodia_posts[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_repsodia_posts[n].viral_amp), f: parseFloat(posts_komfo_repsodia_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_komfo_repsodia_posts[n].organic_ctr), f: parseFloat(posts_komfo_repsodia_posts[n].organic_ctr)+"%"},
		parseInt(posts_komfo_repsodia_posts[n].link_clicks),
		parseInt(posts_komfo_repsodia_posts[n].shares),
		parseInt(posts_komfo_repsodia_posts[n].likes),
		parseInt(posts_komfo_repsodia_posts[n].comments),
		parseInt(posts_komfo_repsodia_posts[n].reach),
		parseInt(posts_komfo_repsodia_posts[n].engagement),
        '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_repsodia_posts[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_repsodia_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_open(){
	$.ajax({
		url: "php_scripts/get_komfo_data_promoted.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_open=response;
		}
	});	
	drawTableKomfo_open();
}
function drawTableKomfo_open(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'string', label: 'Activa'});

  for(var n=0;n<posts_komfo_open.length;n++){
	if(posts_komfo_open[n].promoted==true){
		data_table.addRow([
			n+1,
			((posts_komfo_open[n].name).link("https://facebook.com/"+posts_komfo_open[n].id)),
			'<span class="label label-success">Active</span>'
		]);
	}
	else
	{		
		data_table.addRow([
			n+1,
			((posts_komfo_open[n].name).link("https://facebook.com/"+posts_komfo_open[n].id)),
			''
		]);
	}
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': false,
	'width':'100%',
	'height':'100%',
	'sortColumn': 2,
	'sortAscending': false
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_deforma_open'));

  table.draw(data_table, options);
    
  var pre_links = document.getElementsByClassName('google-visualization-table-td');
  
  links=[];
  
  for(var i=0;i<pre_links.length;i++){
  	if(i%3 == 1){
  		links.push(pre_links[i]);
  	}
  }
  	
  for(var n=0;n<posts_komfo_open.length;n++){
	links[n].childNodes[0].setAttribute('title',posts_komfo_open[n].name);
  }
			
  for(var o=0;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_cult(){
	$.ajax({
		url: "php_scripts/get_komfo_data_cult.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_cult=response;
		}
	});	
	drawTableKomfo_cult();
}
function drawTableKomfo_cult(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_komfo_cult.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_cult[n].name)).link("https://facebook.com/"+posts_komfo_cult[n].id),
		{v: parseFloat(posts_komfo_cult[n].organic_ctr), f: parseFloat(posts_komfo_cult[n].organic_ctr)+"%"},
		{v: parseFloat(posts_komfo_cult[n].penetration), f: parseFloat(posts_komfo_cult[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_cult[n].viral_amp), f: parseFloat(posts_komfo_cult[n].viral_amp)+"x"},
		parseInt(posts_komfo_cult[n].link_clicks),
		parseInt(posts_komfo_cult[n].shares),
		parseInt(posts_komfo_cult[n].likes),
		parseInt(posts_komfo_cult[n].reach),
		parseInt(posts_komfo_cult[n].comments),
		parseInt(posts_komfo_cult[n].engagement),
		parseInt(posts_komfo_cult[n].clicked_to_play),
		parseInt(posts_komfo_cult[n]['10s_sound']),
		parseInt(posts_komfo_cult[n].reaction_like),
		parseInt(posts_komfo_cult[n].reaction_love),
		parseInt(posts_komfo_cult[n].reaction_wow),
		parseInt(posts_komfo_cult[n].reaction_haha),
		parseInt(posts_komfo_cult[n].reaction_sorry),
		parseInt(posts_komfo_cult[n].reaction_anger),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_cult[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_cult'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_komfo_cult.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_komfo_cult[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_cultPosts(){
	$.ajax({
		url: "php_scripts/get_komfo_data_cult_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_cult_posts=response;
		}
	});	
	drawTableKomfo_cult_posts();
}
function drawTableKomfo_cult_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});
  
  

  for(var n=0;n<posts_komfo_cult_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_cult_posts[n].name)).link("https://facebook.com/"+posts_komfo_cult_posts[n].id),
		{v: parseFloat(posts_komfo_cult_posts[n].penetration), f: parseFloat(posts_komfo_cult_posts[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_cult_posts[n].viral_amp), f: parseFloat(posts_komfo_cult_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_komfo_cult_posts[n].organic_ctr), f: parseFloat(posts_komfo_cult_posts[n].organic_ctr)+"%"},
		parseInt(posts_komfo_cult_posts[n].link_clicks),
		parseInt(posts_komfo_cult_posts[n].shares),
		parseInt(posts_komfo_cult_posts[n].likes),
		parseInt(posts_komfo_cult_posts[n].comments),
		parseInt(posts_komfo_cult_posts[n].reach),
		parseInt(posts_komfo_cult_posts[n].engagement),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_cult_posts[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_cult_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_holixir(){
	$.ajax({
		url: "php_scripts/get_komfo_data_holixir.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_holixir=response;
		}
	});	
	drawTableKomfo_holixir();//
}
function drawTableKomfo_holixir(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});

  for(var n=0;n<posts_komfo_holixir.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_holixir[n].name)).link("https://facebook.com/"+posts_komfo_holixir[n].id),
		{v: parseFloat(posts_komfo_holixir[n].organic_ctr), f: parseFloat(posts_komfo_holixir[n].organic_ctr)+"%"},
		{v: parseFloat(posts_komfo_holixir[n].penetration), f: parseFloat(posts_komfo_holixir[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_holixir[n].viral_amp), f: parseFloat(posts_komfo_holixir[n].viral_amp)+"x"},
		parseInt(posts_komfo_holixir[n].link_clicks),
		parseInt(posts_komfo_holixir[n].shares),
		parseInt(posts_komfo_holixir[n].likes),
		parseInt(posts_komfo_holixir[n].reach),
		parseInt(posts_komfo_holixir[n].comments),
		parseInt(posts_komfo_holixir[n].engagement),
		parseInt(posts_komfo_holixir[n].clicked_to_play),
		parseInt(posts_komfo_holixir[n]['10s_sound']),
		parseInt(posts_komfo_holixir[n].reaction_like),
		parseInt(posts_komfo_holixir[n].reaction_love),
		parseInt(posts_komfo_holixir[n].reaction_wow),
		parseInt(posts_komfo_holixir[n].reaction_haha),
		parseInt(posts_komfo_holixir[n].reaction_sorry),
		parseInt(posts_komfo_holixir[n].reaction_anger)
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_holixir'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_komfo_holixir.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_komfo_holixir[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_holixirPosts(){
	$.ajax({
		url: "php_scripts/get_komfo_data_holixir_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_holixir_posts=response;
		}
	});	
	drawTableKomfo_holixir_posts();
}
function drawTableKomfo_holixir_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  
  

  for(var n=0;n<posts_komfo_holixir_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_holixir_posts[n].name)).link("https://facebook.com/"+posts_komfo_holixir_posts[n].id),
		{v: parseFloat(posts_komfo_holixir_posts[n].penetration), f: parseFloat(posts_komfo_holixir_posts[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_holixir_posts[n].viral_amp), f: parseFloat(posts_komfo_holixir_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_komfo_holixir_posts[n].organic_ctr), f: parseFloat(posts_komfo_holixir_posts[n].organic_ctr)+"%"},
		parseInt(posts_komfo_holixir_posts[n].link_clicks),
		parseInt(posts_komfo_holixir_posts[n].shares),
		parseInt(posts_komfo_holixir_posts[n].likes),
		parseInt(posts_komfo_holixir_posts[n].comments),
		parseInt(posts_komfo_holixir_posts[n].reach),
		parseInt(posts_komfo_holixir_posts[n].engagement)
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_holixir_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_partidoPosts(){
	$.ajax({
		url: "php_scripts/get_komfo_data_partido_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_partido_posts=response;
		}
	});	
	drawTableKomfo_partido_posts();
}
function drawTableKomfo_partido_posts(){
    var data_table = new google.visualization.DataTable();

    data_table.addColumn({type: 'number', label: 'Orden'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Time'});
	data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
	data_table.addColumn({type: 'number', label: 'Viral Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Org'});
	data_table.addColumn({type: 'number', label: 'Like/Share'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pago'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
    data_table.addColumn({type: 'number', label: 'Penetr'});
	
    if(adgroups_qp!=null){
        for(var n=0;n<adgroups_qp.length;n++){
            currency_factor=1/19.5;
            if(campaignIsCurrentlyActive(adgroups_qp[n].campaign).status && campaignIsCurrentlyActive(adgroups_qp[n].campaign).effective_status == 'active'){
                 data_table.addRow([
                     n+1,
                     (titleShortener(adgroups_qp[n].name).link("https://facebook.com/"+adgroups_qp[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_qp[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                     new Date(adgroups_qp[n].posted_date),
                     {v: parseFloat(adgroups_qp[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_qp[n].rt_cxw)*currency_factor).toFixed(5)},
                     {v: getDayCXW(adgroups_qp[n].campaign).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_qp[n].campaign).cxw*currency_factor).toFixed(5)},
                     {v: getDayCXW(adgroups_qp[n].campaign).spend*currency_factor, f: "$"+(getDayCXW(adgroups_qp[n].campaign).spend*currency_factor).toFixed(2)},
                     {v: parseFloat(adgroups_qp[n].viral_amp), f: parseFloat(adgroups_qp[n].viral_amp)+"x"},
                     {v: isNaN(parseFloat(adgroups_qp[n].organic_ctr)) ? 0 : parseFloat(adgroups_qp[n].organic_ctr), f: isNaN(parseFloat(adgroups_qp[n].organic_ctr)) ? 0 : parseFloat(adgroups_qp[n].organic_ctr)+"%"},
                     {v: parseInt(adgroups_qp[n].shares)+parseInt(adgroups_qp[n].likes), f: numberWithCommas(adgroups_qp[n].likes)+' / '+numberWithCommas(adgroups_qp[n].shares)},
                     {v: parseInt(adgroups_qp[n].link_clicks), f: goalClicks(adgroups_qp[n])},
                     {v: isNaN(parseFloat(adgroups_qp[n].paid_ctr)) ? 0 : parseFloat(adgroups_qp[n].paid_ctr), f: isNaN(parseFloat(adgroups_qp[n].paid_ctr)) ? 0 : parseFloat(adgroups_qp[n].paid_ctr)+"%"},
                     {v: parseFloat(adgroups_qp[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_qp[n].cxw)*currency_factor).toFixed(5)},
                     {v: parseFloat(adgroups_qp[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_qp[n].cpm)*currency_factor).toFixed(2)},
                     {v: parseFloat(adgroups_qp[n].spend)*currency_factor, f: goalSpend(adgroups_qp[n],currency_factor)},
                     '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_qp[n].id+'\',\''+adgroups_qp[n].campaign+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_qp[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_qp[n].id+'\')">New Promote</button>',
                     {v: parseFloat(adgroups_qp[n].penetration), f: parseFloat(adgroups_qp[n].penetration)+"%"}
               ]);
            }else{
                if(campaignIsCurrentlyActive(adgroups_qp[n].campaign).status && campaignIsCurrentlyActive(adgroups_qp[n].campaign).effective_status == 'pending'){
                    data_table.addRow([
                        n+1,
                        (titleShortener(adgroups_qp[n].name).link("https://facebook.com/"+adgroups_qp[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+adgroups_qp[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
                        new Date(adgroups_qp[n].posted_date),
                        null,
                        null,
                        null,
                        {v: parseFloat(adgroups_qp[n].viral_amp), f: parseFloat(adgroups_qp[n].viral_amp)+"x"},
                        {v: isNaN(parseFloat(adgroups_qp[n].organic_ctr)) ? 0 : parseFloat(adgroups_qp[n].organic_ctr), f: isNaN(parseFloat(adgroups_qp[n].organic_ctr)) ? 0 : parseFloat(adgroups_qp[n].organic_ctr)+"%"},
                        {v: parseInt(adgroups_qp[n].shares)+parseInt(adgroups_qp[n].likes), f: numberWithCommas(adgroups_qp[n].likes)+' / '+numberWithCommas(adgroups_qp[n].shares)},
                        {v: parseInt(adgroups_qp[n].link_clicks), f: goalClicks(adgroups_qp[n])},
                        {v: isNaN(parseFloat(adgroups_qp[n].paid_ctr)) ? 0 : parseFloat(adgroups_qp[n].paid_ctr), f: isNaN(parseFloat(adgroups_qp[n].paid_ctr)) ? 0 : parseFloat(adgroups_qp[n].paid_ctr)+"%"},
                        {v: parseFloat(adgroups_qp[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_qp[n].cxw)*currency_factor).toFixed(5)},
                        {v: parseFloat(adgroups_qp[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_qp[n].cpm)*currency_factor).toFixed(2)},
                        {v: parseFloat(adgroups_qp[n].spend)*currency_factor, f: goalSpend(adgroups_qp[n],currency_factor)},
                        '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_qp[n].id+'\',\''+adgroups_qp[n].campaign+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_qp[n].campaign+'\')">Edit Promote</button>',
                        {v: parseFloat(adgroups_qp[n].penetration), f: parseFloat(adgroups_qp[n].penetration)+"%"}
                   ]);  
                }else{
                    adgroups_qp.splice(n,1);
                    n--;  
                }
            }
        }
    }


    for(var n=0;n<posts_komfo_partido_posts.length;n++){
        data_table.addRow([
            n+1+adgroups_qp.length,
            (titleShortener(posts_komfo_partido_posts[n].name)).link("https://facebook.com/"+posts_komfo_partido_posts[n].id),
            new Date(posts_komfo_partido_posts[n].posted_date),
            null,
            null,
            null,
            {v: parseFloat(posts_komfo_partido_posts[n].viral_amp), f: parseFloat(posts_komfo_partido_posts[n].viral_amp)+"x"},
            {v: parseFloat(posts_komfo_partido_posts[n].organic_ctr), f: parseFloat(posts_komfo_partido_posts[n].organic_ctr)+"%"},
            {v: parseInt(posts_komfo_partido_posts[n].shares)+parseInt(posts_komfo_partido_posts[n].likes), f: numberWithCommas(posts_komfo_partido_posts[n].likes)+' / '+numberWithCommas(posts_komfo_partido_posts[n].shares)},
            {v: parseInt(posts_komfo_partido_posts[n].link_clicks), f: numberWithCommas(posts_komfo_partido_posts[n].link_clicks)},
            null,
            null,
            null,
            null,
            '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_partido_posts[n].id+'\')">Promote</button>',
            {v: parseFloat(posts_komfo_partido_posts[n].penetration), f: parseFloat(posts_komfo_partido_posts[n].penetration)+"%"}
        ]);
    }

    // Set chart options
    var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'80%',
    'frozenColumns': 2
    };


    // Instantiate and draw our chart, passing in some options.
    var table = new google.visualization.Table(document.getElementById('komfo_table_partido_posts'));

    var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
    var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
    var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

    formatter_penetration.format(data_table, 15);
    formatter_viral_amp.format(data_table, 7);
    formatter_ctr.format(data_table, 8);

    table.draw(data_table, options);

    links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');

    for(var o=1;o < links.length;o++){
    links[o].childNodes[0].setAttribute('target','_blank');
    }

    google.visualization.events.addListener(table, 'sort', function() {
    for(var o=1;o < links.length;o++){
        links[o].childNodes[0].setAttribute('target','_blank');
      }
    });
}

function loadKomfo_partidoVideos(){
	$.ajax({
		url: "php_scripts/get_komfo_data_partido.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_komfo_partido_videos=response;
		}
	});	
	drawTableKomfo_partidoVideos();
}
function drawTableKomfo_partidoVideos(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_komfo_partido_videos.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_komfo_partido_videos[n].name)).link("https://facebook.com/"+posts_komfo_partido_videos[n].id),
		{v: parseFloat(posts_komfo_partido_videos[n].organic_ctr), f: parseFloat(posts_komfo_partido_videos[n].organic_ctr)+"%"},
		{v: parseFloat(posts_komfo_partido_videos[n].penetration), f: parseFloat(posts_komfo_partido_videos[n].penetration)+"%"},
		{v: parseFloat(posts_komfo_partido_videos[n].viral_amp), f: parseFloat(posts_komfo_partido_videos[n].viral_amp)+"x"},
		parseInt(posts_komfo_partido_videos[n].link_clicks),
		parseInt(posts_komfo_partido_videos[n].shares),
		parseInt(posts_komfo_partido_videos[n].likes),
		parseInt(posts_komfo_partido_videos[n].reach),
		parseInt(posts_komfo_partido_videos[n].comments),
		parseInt(posts_komfo_partido_videos[n].engagement),
		parseInt(posts_komfo_partido_videos[n].clicked_to_play),
		parseInt(posts_komfo_partido_videos[n]['10s_sound']),
		parseInt(posts_komfo_partido_videos[n].reaction_like),
		parseInt(posts_komfo_partido_videos[n].reaction_love),
		parseInt(posts_komfo_partido_videos[n].reaction_wow),
		parseInt(posts_komfo_partido_videos[n].reaction_haha),
		parseInt(posts_komfo_partido_videos[n].reaction_sorry),
		parseInt(posts_komfo_partido_videos[n].reaction_anger),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_komfo_partido_videos[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_partido_videos'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_komfo_partido_videos.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_komfo_partido_videos[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_chingonas(){
	posts_chingonas=[];
    FB.api(
        '/imaginemoscosaschingonas/posts',
        'GET',
        {"fields":"id,name,created_time,message,link,type,shares,likes.limit(0).summary(1).as(likes),comments.limit(0).summary(1).as(comments),insights.metric(post_impressions_viral).fields(values).as(viral_imp),insights.metric(post_impressions_organic).fields(values).as(organic_imp),insights.metric(post_impressions_unique).fields(values).as(reach),insights.metric(post_impressions).fields(values).as(impressions),insights.metric(post_impressions_fan).fields(values).as(fan_reach),insights.metric(post_clicks_by_type).fields(values).as(consumptions),insights.metric(post_impressions_paid).fields(values).as(impressions_paid),insights.metric(post_video_views).period(lifetime).fields(values).as(video_views),insights.metric(post_reactions_by_type_total).period(lifetime).fields(values).as(reactions_total)","since":"3daysAgo","access_token":pageTokens['Imaginemos Cosas Chingonas'],"limit":100},
        function(response) {
            //console.log(response);
            posts_chingonas=response.data;
            for(j=0;j<posts_chingonas.length;j++){
                if(posts_chingonas[j].type=="status"){
                    posts_chingonas.splice(j,1);
                    j--;
                }
            }//for loop to remove all status posts
            for(j=0;j<posts_chingonas.length;j++){
                posts_chingonas[j].created_time = new Date(new Date(posts_chingonas[j].created_time).getTime()-(new Date().getTimezoneOffset()/60*60*60*1000)).toISOString().substr(0,19).replace('T',' ');
                posts_chingonas[j].shares = typeof posts_chingonas[j].shares === 'undefined' ? 0 : posts_chingonas[j].shares.count;
                posts_chingonas[j].organic_imp=posts_chingonas[j].organic_imp.data[0].values[0].value;
                posts_chingonas[j].imp=posts_chingonas[j].impressions.data[0].values[0].value;
                posts_chingonas[j].unique_imp=posts_chingonas[j].reach.data[0].values[0].value;
                posts_chingonas[j].paid_imp=posts_chingonas[j].impressions_paid.data[0].values[0].value;
                posts_chingonas[j].unpaid_imp=posts_chingonas[j].imp-posts_chingonas[j].paid_imp;
                if(posts_chingonas[j].type == 'link' || posts_chingonas[j].type == 'photo'){
                    posts_chingonas[j].link_clicks=posts_chingonas[j].consumptions.data[0].values[0].value['link clicks'];
                }
                if(posts_chingonas[j].type == 'video'){
                    posts_chingonas[j].link_clicks=posts_chingonas[j].video_views.data[0].values[0].value;
                }
                posts_chingonas[j].likes=typeof posts_chingonas[j].likes.summary === 'undefined' ? 0 : posts_chingonas[j].likes.summary.total_count;
                posts_chingonas[j].comments=typeof posts_chingonas[j].comments.summary === 'undefined' ? 0 : posts_chingonas[j].comments.summary.total_count;
                posts_chingonas[j].organic_ctr=(posts_chingonas[j].link_clicks/posts_chingonas[j].imp*100).toFixed(2);
                posts_chingonas[j].num=j;
                if(posts_chingonas[j].name == '' || posts_chingonas[j].name == undefined){
                    posts_chingonas[j].name = posts_chingonas[j].message;
                }
            }//for loop to assigne metrics to post object
            drawTableKomfo_chingonas();
        }//successful facebook API call for posts
    );
}
function drawTableKomfo_chingonas(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks/VV'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'string', label: 'Promote'});

    
  for(var n=0;n<posts_chingonas.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_chingonas[n].name)).link("https://facebook.com/"+posts_chingonas[n].id),
		{v: parseFloat(posts_chingonas[n].organic_ctr), f: parseFloat(posts_chingonas[n].organic_ctr)+"%"},
		parseInt(posts_chingonas[n].link_clicks),
		parseInt(posts_chingonas[n].shares),
		parseInt(posts_chingonas[n].likes),
		parseInt(posts_chingonas[n].comments),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_chingonas[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_chingonas'));

  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_chingonas.length;n++){
		links[n].childNodes[0].setAttribute('title',posts_chingonas[n].name);				
		links[n].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function titleShortener(text){
	if(text.length<50){
		return text;
	}
	else{
		return text.substring(0,20)+'...'+text.substring(text.length-20,text.length);
	}
}
//better komfo

//One click promote
var adCreative = {};
var adCampaign = {};
var adSet = {};
var adAd = {};
var promoteParameters = {adCampaign, adSet, adCreative, adAd};
function getPromotePostId(id){
	var now =new Date();
	$('#promote-datepicker').val(now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate());
	//console.log(id);
	$('#promote-timepicker').timepicker('setTime', new Date());
	if(!document.getElementById('mobile_placement').checked){console.log('reset mobile');document.getElementById('mobile_placement').click();}
	if(!document.getElementById('desktop_placement').checked){document.getElementById('desktop_placement').click();}
	if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
	promoteParameters.adCreative.object_story_id = id;
	promoteParameters.adCreative.url_tags = 'p1=true';
	var selector=document.getElementById("exclude");
	selector.selectedIndex = getExcludedDate();
	getObjective(id);
}
function deleteLimit(){
	$('#limit_value')[0].value = '';
}
function resetForm(){
	document.getElementById('name').value = '';
	document.getElementById('budget').value = '';
	document.getElementById('bid').value = '0.01';
	document.getElementById('min_age').value = '16';
	document.getElementById('max_age').value = '65';
	$('#ad_account')[0].selectedIndex = 0;
	$('#excluded_group')[0].style.visibility = 'visible';
	$('#gcs_group')[0].style.visibility = 'visible';
	if(!document.getElementById('mobile_placement').checked){console.log('reset mobile');document.getElementById('mobile_placement').click();}
	if(!document.getElementById('desktop_placement').checked){document.getElementById('desktop_placement').click();}
	if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
	$('#bid_label')[0].innerHTML = 'Bid (USD)';
	$('#budget_label')[0].innerHTML = 'Budget (USD)';
	var budget = $('#budget').parsley();
	budget.options.range = [20,5000];
	var bid = $('#bid').parsley();
	bid.options.range = [0.01,0.05];
	$('#no_limit')[0].click();
    $('#do-not-notify')[0].checked = false;
}
function adAccountSelected(id){
	typeof id === 'undefined' ? id=promoteParameters.adCreative.object_story_id : id=id;
	//console.log('In adAccountSelected',id);
	switch(true){
		case ($('#ad_account').val() == 'act_958749654167036' || $('#ad_account').val() == 'act_986080914767243' || $('#ad_account').val() == 'act_191281401594032'):
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'hidden';
            $('#do-not-notify')[0].checked = true;
			if(!isVideo(id)){
				if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
				$('#gcs_group')[0].style.visibility = 'visible';
			}else{
				document.getElementById('radio_GCS_None').checked = true;
				$('#gcs_group')[0].style.visibility = 'hidden';
			}
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : ($('#ad_account').val() == 'act_191281401594032' ? [{name: 'El deforma 5 days', id: '23842843732900399'},{name: 'El deforma 10 days', id: '23842843733770399'},{name: 'El deforma 15 days',id: '23842843733790399'},{name: 'El deforma 20 days', id: '23842843733950399'},{name: 'El deforma 25 days', id: '23842843734300399'},{name: 'El deforma 30 days', id: '23842843734330399'}] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]);
            var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 0;
			var budget = $('#budget').parsley();
			budget.options.range = [20,5000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = false;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case ($('#ad_account').val() == 'act_31492866' && $('#ad_account option:selected').text().indexOf('Branded') > -1):
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'visible';
            $('#do-not-notify')[0].checked = false;
            $('#objective')[0].selectedIndex = 2;
            document.getElementById('billing_event').selectedIndex = $('#objective')[0].selectedIndex;
            document.getElementById('optimization').selectedIndex = $('#objective')[0].selectedIndex;
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 1;
			var budget = $('#budget').parsley();
			budget.options.range = [20,2000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true&b1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = false;
            $('#latam')[0].checked = true;
			break;
		case ($('#ad_account').val() == 'act_955415581167110'):
			$('#bid_label')[0].innerHTML = 'Bid (MXN)';
			$('#budget_label')[0].innerHTML = 'Budget (MXN)';
            $('#notify-group')[0].style.visibility = 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			selector.selectedIndex = 0;
			//$('#excluded_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			var budget = $('#budget').parsley();
			budget.options.range = [500,8000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.2,0.5];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case ($('#ad_account').val() == 'act_1813540365354623'):
			$('#bid_label')[0].innerHTML = 'Bid (MXN)';
			$('#budget_label')[0].innerHTML = 'Budget (MXN)';
            $('#notify-group')[0].style.visibility = 'hidden';
			//$('#excluded_group')[0].style.visibility = !isVideo ? 'visible' : 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
            $('#objective')[0].selectedIndex = 2;
            $('#billing_event')[0].selectedIndex = 2;
            $('#optimization')[0].selectedIndex = 2;
			/*var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6034155584927'},{name: 'El deforma 10 days', id: '6034155576527'},{name: 'El deforma 15 days',id: '6034155568527'},{name: 'El deforma 20 days', id: '6034155515127'},{name: 'El deforma 25 days', id: '6034155490727'},{name: 'El deforma 30 days', id: '6020024675727'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = getExcludedDate();*/
			var budget = $('#budget').parsley();
			budget.options.range = [20,40000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,5];
			promoteParameters.adCreative.url_tags = 'p1=true&b1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case ($('#ad_account').val() == 'act_31492866' && $('#ad_account option:selected').text().indexOf('Branded') < 0):
            $('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'hidden';
            $('#do-not-notify')[0].checked = true;
            $('#objective')[0].selectedIndex = 0;
            document.getElementById('billing_event').selectedIndex = $('#objective')[0].selectedIndex;
            document.getElementById('optimization').selectedIndex = $('#objective')[0].selectedIndex;
			if(!isVideo(id)){
				if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
				$('#gcs_group')[0].style.visibility = 'visible';
			}else{
				document.getElementById('radio_GCS_None').checked = true;
				$('#gcs_group')[0].style.visibility = 'hidden';
			}
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 0;
			var budget = $('#budget').parsley();
			budget.options.range = [20,5000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = false;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
	}
}
function getPromoteParameters(){
	promoteParameters.adCampaign.name = document.getElementById('name').value;
	promoteParameters.adCampaign.buying_type = 'AUCTION';
	promoteParameters.adCampaign.objective = document.getElementById('objective').value;
	promoteParameters.adCampaign.status = 'ACTIVE';	
	
	promoteParameters.adSet.name = document.getElementById('name').value;
	promoteParameters.adSet.bid_amount = parseFloat(document.getElementById('bid').value)*100;
	promoteParameters.adSet.billing_event = document.getElementById('billing_event').value;
	promoteParameters.adSet.campaign_id = '';
	promoteParameters.adSet.daily_budget = parseFloat(document.getElementById('budget').value)*100;
	promoteParameters.adSet.is_autobid= false;
	promoteParameters.adSet.optimization_goal = document.getElementById('optimization').value;
	promoteParameters.adSet.status= 'ACTIVE';
	promoteParameters.adSet.start_time =new Date($('#promote-datepicker').val()+' '+$('#promote-timepicker').data('timepicker').hour.toString()+':'+$('#promote-timepicker').data('timepicker').minute.toString()+' '+$('#promote-timepicker').data('timepicker').meridian).getTime()/1000;
	promoteParameters.adSet.targeting = {
		geo_locations: {
			countries: getCountries()
		},
		age_min: parseInt(document.getElementById('min_age').value),
		age_max: parseInt(document.getElementById('max_age').value),
		publisher_platforms: ['facebook'],
		device_platforms: getPlacements(),
		facebook_positions: ['feed']
	};
	if($("#exclude")[0].value !== ''){
		promoteParameters.adSet.targeting.excluded_custom_audiences = [{'id': document.getElementById('exclude').value}];
	}
    if($("#connections")[0].value !== ''){
		promoteParameters.adSet.targeting.connections = [{'id': document.getElementById('connections').value}];
	}
    if($('#objective')[0].value == 'CONVERSIONS'){
        promoteParameters.adSet.attribution_spec = [{
            event_type: "CLICK_THROUGH",
            window_days: 1
        }];
        promoteParameters.adSet.promoted_object = {
            pixel_id: $('#ad_account').val() == 'act_191281401594032' ? '201069333861826' : "286492281506212",
            custom_event_type: "CONTENT_VIEW"
        };
    }
    
	promoteParameters.adAd.name = document.getElementById('name').value;
	promoteParameters.adAd.adset_id = '';
	promoteParameters.adAd.creative = {'creative_id': ''};
	promoteParameters.adAd.status = 'ACTIVE';
	
	createCampaign();
}
function createCampaign(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/campaigns',
		'POST',
		promoteParameters.adCampaign,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adSet.campaign_id = response.id;
				setTimeout(createAdset(),2000);
			}else{
				alert(response.error.error_user_msg);
			}
		}
	);
}
function createAdset(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/adsets',
		'POST',
		promoteParameters.adSet,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adAd.adset_id = response.id;
				setTimeout(createCreative(),500);
			}else{
				alert(response.error.error_user_msg);
			}
		}
	);
}
function createCreative(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/adcreatives',
		'POST',
		promoteParameters.adCreative,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adAd.creative.creative_id = response.id;
				setTimeout(createAd(),500);
			}
			else{
				alert(response.error.error_user_msg);
			}
		}
	);
}
function createAd(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/ads',
		'POST',
		promoteParameters.adAd,
		function(response) {
            console.log(response);
			if(response.hasOwnProperty('id')){
				alert('Campaign successfully created!');
				updatePromotedPosts();
				setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
				if($('input[name=radio]:checked').val() == 'full'){
					activateGCS(promoteParameters.adCreative.object_story_id);
				}else if($('input[name=radio]:checked').val() == 'hybrid'){
					activateHybridGCS(promoteParameters.adCreative.object_story_id);
				}
				if($('#ad_account')[0].value == 'act_31492866' && !$('#do-not-notify')[0].checked){
					$.post(
						"https://hooks.slack.com/services/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",
						JSON.stringify({
							"channel": '#implementar_comercial',
							"username": 'Alerta de Campaña',
							"attachments": [{
								"fallback": 'Campaña iniciada',
								"color": "green",
								"author_name": 'Campaign Bot',
								"title": 'Campaña iniciada',
								"text": 'La campaña para: '+promoteParameters.adCampaign.name+'\nha empezado a promoverse\n'
							}]
						})
					);
				}
			}else{
				alert(response.error.error_user_msg);
			}
			resetForm();
            selected_unpublished_post = {};
		}
	);
}
function activateGCS(id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link;
			var slug = link.substr(32,link.length).substr(0,link.substr(32,link.length).indexOf('/'));
			console.log(slug);
			getPostIdForGCS(slug,id);
		}
	);
}
function getPostIdForGCS(slug,id){
	$.ajax({
		url: 'https://eldeforma.com/api/get_post/?slug='+slug+'&include=id',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			if(!data.hasOwnProperty('error')){
				console.log(data.post.id);
				updatePostGCS(data.post.id,id);
			}else{
				alert('find post: '+data.error);
			}
		}
	});
}
function updatePostGCS(id,fb_id){
	var wp_nonce;
	$.ajax({
		url: 'https://eldeforma.com/api/get_nonce/?controller=posts&method=update_post',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			wp_nonce = data.nonce;
			$.ajax({
				url: 'https://eldeforma.com/api/get_post/?post_id='+id,
				headers: {
					'Authorization':'Basic dan:Db5471033187'
				},
				method: 'GET',
				async: false,
				dataType: 'jsonp',
				success: function(data){
					var previousContent = data.post.content;
					$.ajax({
						url: 'https://eldeforma.com/api/posts/update_post/?id='+id,
						headers: {
							'Authorization':'Basic dan:Db5471033187'
						},
						method: 'POST',
						async: false,
						dataType: 'jsonp',
						data: {
							content: '<div class="p402_premium">\n'+previousContent+'<\/div>\n<script type="text\/javascript">\n\ttry { _402_Show(); } catch(e) {}\n<\/script>',
							nonce: wp_nonce
						},
						success: function(data){
							removeInstant(fb_id);
							alert('GCS successfully added');
							updateDatabaseGCS(fb_id);
						}
					});
				}
			});
		}
	});
}
function updateDatabaseGCS(id){
	$.ajax({
		url: 'php_scripts/updateGCS.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			id: id
		},
		success: function(response){
			
		}
	});
	setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
}
function removeInstant(fb_id){
	FB.api(
		'/'+fb_id,
		'GET',
		{"fields":"link"},
		function(response) {
			console.log('in remove instant',response);
			var link = response.link.substr(0,response.link.indexOf('?'));
			FB.api(
				'/',
				'GET',
				{"id":link,"fields":"instant_article"},
				function(answer) {
				  console.log('in getting instant article for deletion',answer);
				  unpublish(answer.instant_article);
				}
			);
		}
	);
}
function unpublish(instant_article){
	console.log(instant_article,' in instant articles deletion');
	setTimeout(function(){
		FB.api(
			'/eldeforma/instant_articles',
			'POST',
			{"html_source": instant_article.html_source,'published':false,'development_mode':false},
			function(result) {
			  console.log('unpublished ',result.canonical_url);
			}		
		);
	},10000);
}
function getPlacements(){
	var placements = [];
	if(document.getElementById('desktop_placement').checked){
		placements.push('desktop');
	}
	if(document.getElementById('mobile_placement').checked){
		placements.push('mobile');
	}
	if(placements.length == 0){
		placements = ['desktop','mobile'];
	}
	return placements;
}
function stopCampaign(post_id,campaign){
	updateStoppedCampaign(post_id);
	/*$.ajax({
		url: 'php_scripts/get_campaign.php',
		method: 'GET',
		async: false,
		dataType: 'json',
		data: {post_id: post_id},
		success: function(response){
			console.log(response);
			/*if(response.ad_account == 'act_31492866'){
				$.post(
					"https://hooks.slack.com/services/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",
					JSON.stringify({
						"channel": '#implementar_comercial',
						"username": 'Alerta de Campaña',
						"attachments": [{
							"fallback": 'Campaña interrumpida',
							"color": "red",
							"author_name": 'Campaign Bot',
							"title": 'Campaña interrumpida',
							"text": 'La campaña para: '+response.name+'\nse ha interrumpido\n'
						}]
					})
				);
			}*/
			FB.api(
				'/'+campaign,
				'POST',
				{status: 'PAUSED'},
				function(response) {
					if(!response.hasOwnProperty('error')){
						console.log(response);
						alert('Campaign successfully paused!');
						setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
					}else{
						alert(response.error.message);
					}
				}
			);
		/*}
	});*/
}
function getExcludedDate(){
	var now = new Date().getDate();
	switch(true){
		case now<=5:
			return 1;
			break;
		case now<=10:
			return 2;
			break;
		case now<=15:
			return 3;
			break;
		case now<=20:
			return 4;
			break;
		case now<=25:
			return 5;
			break;
		case now<=31:
			return 6;
			break;
	}
}
function getObjective(id){
	var selector_objective = document.getElementById("objective");
	var selector_billing = document.getElementById("billing_event");
	var selector_optimization = document.getElementById("optimization");
	if(isVideo(id)){
		selector_objective.selectedIndex = 2;
		selector_billing.selectedIndex = 2;
		selector_optimization.selectedIndex = 2;
		document.getElementById('radio_GCS_None').checked = true;
		$('#gcs_group')[0].style.visibility = 'hidden';
	}else{
		selector_objective.selectedIndex = 0;
		selector_billing.selectedIndex = 0;
		selector_optimization.selectedIndex = 0;
	}
	console.log('In get Objective',id);
	adAccountSelected(id);
}
function updatePromotedPosts(){
	$.ajax({
		url: 'php_scripts/update_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
            account: $('#ad_account')[0].selectedIndex == 1 ? 'act_986080914767243' : $('#ad_account').val(),
            unpublished: $.isEmptyObject(selected_unpublished_post) ? 0 : selected_unpublished_post,
			campaign_id: promoteParameters.adSet.campaign_id,
			post_id: promoteParameters.adCreative.object_story_id,
            client: document.getElementById('client').value,
            client_campaign: document.getElementById('client_campaign').value
		},
		success: function(response){
			console.log(response);
            console.log('successfully inserted unpublished post');
		}
	});
	if($('input[name=radioLimit]:checked', '#form').val() == 'spend_limit' || $('input[name=radioLimit]:checked', '#form').val() == 'goal'){
		$.ajax({
			url: 'php_scripts/update_campaign_goal.php',
			method: 'POST',
			async: false,
			dataType: 'json',
			data: {
				post_id: promoteParameters.adCreative.object_story_id,
				type: $('input[name=radioLimit]:checked', '#form').val(),
				value: parseInt($('#limit_value')[0].value)
			},
			success: function(response){
				console.log(response);
			}
		});
	}
}
function updateStoppedCampaign(id){
	$.ajax({
		url: 'php_scripts/update_stopped_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			post_id: id
		},
		success: function(response){
			console.log(response);
		}
	});
}
function isVideo(id){
	var	flag;
	$.ajax({
		url: 'https://graph.facebook.com/v2.12/'+id+'?fields=type'+"&"+access_token,
		async: false,
		method: 'get',
		success: function(response){
			if(response.type == 'video'){
				flag = true;
			}else{
				flag = false;
			}
		}
	});
	return flag;
}
function changeObjective(){
	var index = document.getElementById('objective').selectedIndex;
	document.getElementById('billing_event').selectedIndex = index;
	document.getElementById('optimization').selectedIndex = index;
}
function getCountries(){
    var countries=[];
    if($('#mexico')[0].checked){
       countries.push('MX');
    }
    if($('#latam')[0].checked){
        countries.push('BZ', 'PA', 'AR', 'CL', 'PE', 'CO', 'EC', 'BO', 'GT', 'CR', 'SV', 'HN', 'NI', 'PY', 'UY');
    }
    if(countries.length == 0){
        return ['MX'];
    }
    else{
        return countries;
    }
}
//One click promote

//Edit Promote
var campaign_to_edit;
var editAdSet = {};
var editAdAd = {};
var editPromoteParameters = {editAdSet: {'targeting':{}}, editAdAd:{}};
function getCurrentCampaignParameters(campaign){
    FB.api(
        '/'+campaign,
        'GET',
        {"fields":"adsets.fields(daily_budget,bid_amount,targeting,excluded_custom_audiences),ads.fields(name,creative.fields(effective_object_story_id)),account_id"},
        function(response){
            campaign_to_edit=response;
            $('#edit-ad-name').attr("placeholder", response.ads.data[0].name);
            $('#edit-budget').attr("placeholder", parseFloat(response.adsets.data[0].daily_budget)/100);
            $('#edit-bid').attr("placeholder", response.adsets.data[0].bid_amount/100);
            $('#edit-min-age').attr("placeholder", response.adsets.data[0].targeting.age_min);
            $('#edit-max-age').attr("placeholder", response.adsets.data[0].targeting.age_max);
            var selector=document.getElementById("edit-exclude");
            while(selector.length>0){
                selector.remove(0);
            }
            switch(response.account_id){
                case '986080914767243':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                case '31492866':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                case '958749654167036':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                default:
                    var audiences = [];
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    break;
            }
            var selector=document.getElementById("edit-exclude");
            if(typeof response.adsets.data[0].targeting.excluded_custom_audiences !== 'undefined'){
                $('#edit-exclude').val(response.adsets.data[0].targeting.excluded_custom_audiences[0].id);    
            }else{
                $('#edit-exclude').val('');
            }
            
            if(typeof response.adsets.data[0].targeting.connections !== 'undefined'){
                $('#edit-connections').val(response.adsets.data[0].targeting.connections[0].id);
            }else{
                $('#edit-connections').val(null);
            }
            
            if(typeof response.adsets.data[0].targeting.excluded_connections !== 'undefined'){
                $('#edit-excluded-connections').val(response.adsets.data[0].targeting.excluded_connections[0].id);
            }else{
                $('#edit-excluded-connections').val(null);
            }
            
            $('#edit-genders').prop('selectedIndex',typeof response.adsets.data[0].targeting.genders === 'undefined' ? 0 : response.adsets.data[0].targeting.genders[0]);
            
            if($.inArray('desktop', response.adsets.data[0].targeting.device_platforms) > -1){
                $('#edit-desktop-placement')[0].checked = true;
            }else{
                $('#edit-desktop-placement')[0].checked = false;
            }
        
            if($.inArray('mobile', response.adsets.data[0].targeting.device_platforms) > -1){
                $('#edit-mobile-placement')[0].checked = true;
            }else{
                $('#edit-mobile-placement')[0].checked = false;
            }
            
            if(typeof response.adsets.data[0].targeting.device_platforms === 'undefined'){
                $('#edit-mobile-placement')[0].checked = true;
                $('#edit-desktop-placement')[0].checked = true;
            }
            
            if($.inArray('MX', response.adsets.data[0].targeting.geo_locations.countries) > -1){
                $('#edit-mexico')[0].checked = true;
            }else{
                $('#edit-mexico')[0].checked = false;
            }
            
            if($.inArray('BZ', response.adsets.data[0].targeting.geo_locations.countries) > -1){
                $('#edit-latam')[0].checked = true;
            }else{
                $('#edit-latam')[0].checked = false;
            }
        }
    );
}
function setEditPromoteParameters(){
    var ad_change_flag = false;
    var adset_change_flag = false;
    editPromoteParameters = {editAdSet: {'targeting':{}}, editAdAd:{}};
    
    if($('#edit-bid').val() !== $('#edit-bid').attr('placeholder') && $('#edit-bid').val() !== ''){
        editPromoteParameters.editAdSet.bid_amount = parseFloat(document.getElementById('edit-bid').value)*100;
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.bid_amount = campaign_to_edit.adsets.data[0].bid_amount;
    }
    if($('#edit-budget').val() !== $('#edit-budget').attr('placeholder') && $('#edit-budget').val() !== ''){
        editPromoteParameters.editAdSet.daily_budget = parseFloat(document.getElementById('edit-budget').value)*100;
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.daily_budget = campaign_to_edit.adsets.data[0].daily_budget;
    }
    if($('#edit-min-age').val() !== $('#edit-min-age').attr('placeholder') && $('#edit-min-age').val() !== ''){
       editPromoteParameters.editAdSet.targeting.age_min = parseInt(document.getElementById('edit-min-age').value);
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.targeting.age_min = campaign_to_edit.adsets.data[0].targeting.age_min;
    }
    if($('#edit-max-age').val() !== $('#edit-max-age').attr('placeholder') && $('#edit-max-age').val() !== ''){
       editPromoteParameters.editAdSet.targeting.age_max = parseInt(document.getElementById('edit-max-age').value);
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.targeting.age_max = campaign_to_edit.adsets.data[0].targeting.age_max;
    }
       
    if(typeof campaign_to_edit.adsets.data[0].targeting.genders === 'undefined'){
        if($('#edit-genders').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.genders = [$('#edit-genders').prop('selectedIndex')];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-genders').prop('selectedIndex') != campaign_to_edit.adsets.data[0].targeting.genders){
            if($('#edit-genders').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.genders = [$('#edit-genders').prop('selectedIndex')];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.genders = [];
                adset_change_flag = true;
            }
        }
    }
        
    if(typeof campaign_to_edit.adsets.data[0].targeting.excluded_custom_audiences === 'undefined'){
        if($('#edit-exclude').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [{id: $('#edit-exclude').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-exclude').val() != campaign_to_edit.adsets.data[0].targeting.excluded_custom_audiences[0].id){
            if($('#edit-exclude').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [{id: $('#edit-exclude').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.connections === 'undefined'){
        if($('#edit-connections').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.connections = [{id: $('#edit-connections').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-connections').val() != campaign_to_edit.adsets.data[0].targeting.connections[0].id){
            if($('#edit-connections').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.connections = [{id: $('#edit-connections').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.connections = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.excluded_connections === 'undefined'){
        if($('#edit-excluded-connections').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.excluded_connections = [{id: $('#edit-excluded-connections').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-excluded-connections').val() != campaign_to_edit.adsets.data[0].targeting.excluded_connections[0].id){
            if($('#edit-excluded-connections').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.excluded_connections = [{id: $('#edit-excluded-connections').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.excluded_connections = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.device_platforms !== 'undefined'){
        
    }
    else{
        editPromoteParameters.editAdSet.targeting.device_platforms = getEditPlacements();
        editPromoteParameters.editAdSet.targeting.publisher_platforms = ['facebook'];
        editPromoteParameters.editAdSet.targeting.facebook_positions = ['feed'];
        adset_change_flag = true;
    }
    
    if($('#edit-ad-name').val() !== $('#edit-ad-name').attr('placeholder') && $('#edit-ad-name').val() !== ''){
        editPromoteParameters.editAdAd.name = $('#edit-ad-name').val();   
        ad_change_flag = true;
    }
    
    var edit_countries=[];
    if($('#edit-mexico')[0].checked){
       edit_countries.push('MX');
    }
    if($('#edit-latam')[0].checked){
        edit_countries.push('BZ', 'PA', 'AR', 'CL', 'PE', 'CO', 'EC', 'BO', 'GT', 'CR', 'SV', 'HN', 'NI', 'PY', 'UY');
    }
    if(edit_countries.length == 0){
        edit_countries.push('MX');
    }
    if(campaign_to_edit.adsets.data[0].targeting.geo_locations.countries.length != edit_countries.length){
        if($('#leave-targeting')[0].checked){
           editPromoteParameters.editAdSet.targeting.geo_locations = {countries : campaign_to_edit.adsets.data[0].targeting.geo_locations.countries};
        }
        else{
            editPromoteParameters.editAdSet.targeting.geo_locations = {countries : edit_countries};
        }
        adset_change_flag = true;
    }
    
    if(adset_change_flag){
        if($.isEmptyObject(editPromoteParameters.editAdSet.targeting)){
            delete editPromoteParameters.editAdSet.targeting;
        }
        else{
            editPromoteParameters.editAdSet.targeting.geo_locations = {countries : edit_countries};
            editPromoteParameters.editAdSet.targeting.publisher_platforms = ['facebook'];
            editPromoteParameters.editAdSet.targeting.facebook_positions = ['feed'];
        }
        FB.api(
            '/'+campaign_to_edit.adsets.data[0].id,
            'POST',
            editPromoteParameters.editAdSet,
            function(response) {
                console.log(response);
                if(response.success){
                    alert('Adset updated succesfully');
                    resetEditForm();
                    editPromoteParameters.editAdSet = {'targeting':{}};
                    setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
                }else{
                    alert('Adset update failed');
                }
            }
        );
    }
    
    if(ad_change_flag){
        FB.api(
            '/'+campaign_to_edit.ads.data[0].id,
            'POST',
            editPromoteParameters.editAdAd,
            function(response) {
                console.log(response);
                if(response.success){
                    alert('Ad updated succesfully');
                    resetEditForm();
                    editPromoteParameters.editAdAd = {};
                    setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
                }else{
                    alert('Ad update failed');
                }
            }
        );
    }
}
function getEditPlacements(){
	var placements = [];
    if($('#edit-mobile-placement')[0].checked){
		placements.push('mobile');
	}
	if($('#edit-desktop-placement')[0].checked){
		placements.push('desktop');
	}
	if(placements.length == 0){
		placements = ['mobile','desktop'];
	}
	return placements;
}
function resetEditForm(){
	document.getElementById('edit-ad-name').value = '';
	document.getElementById('edit-budget').value = '';
	document.getElementById('edit-bid').value = '';
	document.getElementById('edit-min-age').value = '';
	document.getElementById('edit-max-age').value = '';
	
//	$('#bid_label')[0].innerHTML = 'Bid (USD)';
//	$('#budget_label')[0].innerHTML = 'Budget (USD)';
//	var budget = $('#budget').parsley();
//	budget.options.range = [20,2000];
//	var bid = $('#bid').parsley();
//	bid.options.range = [0.01,0.05];
}
function hideTargeting(){
    if($('#leave-targeting')[0].checked){
        $('#edit-locations').hide();
    }else{
        $('#edit-locations').show();
    }
}
function getClientKomfo(id){
    $('#client_post_id').html(id);
    $.ajax({
        url: "php_scripts/get_client.php",
		dataType:"json",
		async:false,
		method:'get',
        data: {'id': id},
		success: function(response){
			$('#client_name').val(response.client);
            $('#client_campaign_name').val(response.client_campaign);
		}
    });
}
function setClient(){
    $.ajax({
        url: "php_scripts/set_client.php",
		dataType:"json",
		async:false,
		method:'POST',
        data: {client: $('#client_name').val(), client_campaign: $('#client_campaign_name').val(), id: $('#client_post_id').html()},
		success: function(response){
			console.log('client update succesful');
            setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
		}
    });
}
//Edit Promote

//HybridGCS
function activateHybridGCS(id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link.substr(0,response.link.indexOf('?'));
			FB.api(
				'/',
				'GET',
				{"id":link,"fields":"instant_article"},
				function(response) {
				  addGCStoArticle(response.instant_article,id);
				}
			);
		}
	);
}
function addGCStoArticle(instant,id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link;
			var slug = link.substr(32,link.length).substr(0,link.substr(32,link.length).indexOf('/'));
			console.log(slug);
			getPostIdForGCSHybrid(instant,slug,id);
		}
	);
}
function getPostIdForGCSHybrid(instant,slug,id){
	$.ajax({
		url: 'https://eldeforma.com/api/get_post/?slug='+slug+'&include=id',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			if(!data.hasOwnProperty('error')){
				console.log(data.post.id);
				updatePostGCSHybrid(instant,data.post.id,id);
			}else{
				alert('find post: '+data.error);
			}
		}
	});
}
function updatePostGCSHybrid(instant,id,fb_id){
	var wp_nonce;
	$.ajax({
		url: 'https://eldeforma.com/api/get_nonce/?controller=posts&method=update_post',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			wp_nonce = data.nonce;
			$.ajax({
				url: 'https://eldeforma.com/api/get_post/?post_id='+id,
				headers: {
					'Authorization':'Basic dan:Db5471033187'
				},
				method: 'GET',
				async: false,
				dataType: 'jsonp',
				success: function(data){
					var previousContent = data.post.content;
					$.ajax({
						url: 'https://eldeforma.com/api/posts/update_post/?id='+id,
						headers: {
							'Authorization':'Basic dan:Db5471033187'
						},
						method: 'POST',
						async: false,
						dataType: 'jsonp',
						data: {
							content: '<div class="p402_premium">\n'+previousContent+'<\/div>\n<script type="text\/javascript">\n\ttry { _402_Show(); } catch(e) {}\n<\/script>',
							nonce: wp_nonce
						},
						success: function(data){
							//setTimeout(function() {publishHybridGCS(instant)},10000);
							alert('Hybrid GCS successfully added');
							updateDatabaseGCS(fb_id);
						}
					});
				}
			});
		}
	});
}
function publishHybridGCS(instant_article){
	FB.api(
		'/eldeforma/instant_articles',
		'POST',
		{"html_source": instant_article.html_source,'published':'true'},
		function(response) {
			console.log(response);
			setTimeout(function(){
				FB.api(
					'/'+response.id,
					'GET',
					{},
					function(response) {
						console.log(response);
						alert(response.status);
					}
				);
			},10000);
		}
	);
}
//HybridGCS

//Remove GCS
function removeGCS(id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link;
			var slug = link.substr(32,link.length).substr(0,link.substr(32,link.length).indexOf('/'));
			console.log(slug);
			getPostIdForGCSRemoval(slug,id);
			//restoreInstant(link);
		}
	);
}
function getPostIdForGCSRemoval(slug,id){
	$.ajax({
		url: 'https://eldeforma.com/api/get_post/?slug='+slug+'&include=id',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			if(!data.hasOwnProperty('error')){
				console.log(data.post.id);
				removePostGCS(data.post.id,id);
			}else{
				alert('find post: '+data.error);
			}
		}
	});
}
function removePostGCS(id,fb_id){
	var wp_nonce;
	$.ajax({
		url: 'https://eldeforma.com/api/get_nonce/?controller=posts&method=update_post',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			wp_nonce = data.nonce;
			$.ajax({
				url: 'https://eldeforma.com/api/get_post/?post_id='+id,
				headers: {
					'Authorization':'Basic dan:Db5471033187'
				},
				method: 'GET',
				async: false,
				dataType: 'jsonp',
				success: function(data){
					var previousContent = data.post.content;
					$.ajax({
						url: 'https://eldeforma.com/api/posts/update_post/?id='+id,
						headers: {
							'Authorization':'Basic dan:Db5471033187'
						},
						method: 'POST',
						async: false,
						dataType: 'jsonp',
						data: {
							content: previousContent.replace('<div class=\"p402_premium\">\n','').replace('<\/div>\n<p><script type=\"text\/javascript\">\n\ttry { _402_Show(); } catch(e) {}\n<\/script><\/p>',''),
							nonce: wp_nonce
						},
						success: function(data){
							alert('GCS successfully removed');
							updateDatabaseGCSRemoval(fb_id);
						}
					});
				}
			});
		}
	});
}
function updateDatabaseGCSRemoval(id){
	$.ajax({
		url: 'php_scripts/updateGCSRemoval.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			id: id
		},
		success: function(response){

		}
	});
	setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
}
/*function restoreInstant(link){
	FB.api(
		'/',
		'GET',
		{"id":link,"fields":"instant_article"},
		function(response) {
		  //publish(response.instant_article);
		}
	);
}*/
function publish(instant_article){
	FB.api(
		'/eldeforma/instant_articles',
		'POST',
		{"html_source": instant_article.html_source,'published':'true'},
		function(response) {
			console.log(response);
			setTimeout(function(){
				FB.api(
					'/'+response.id,
					'GET',
					{},
					function(response) {
						console.log(response);
						alert(response.status);
					}
				);
			},10000);
		}
	);
}
//Remove GCS

//posts latest
function insertLatestView(){
	var myNode = document.getElementById("fb_graphs");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
	$.ajax({
		url: "views/latest_view.html",
		dataType: "html",
		method: 'get',
		async: false,
		success: function(response){
			myNode.innerHTML = response;
			UpdatePostsDashboard();
		}
	});
}
var data_dashboard=[];
var metric_dashboard=0;
var formating_dashboard;
var dashboard_currency;
function UpdateDataDashboard(){
	var post_id=document.getElementById("combo_box_latest").value;
	var time_lapse = document.getElementById("time_lapse_latest").value;
	$.ajax({
		url: "php_scripts/get_post_data.php",
		dataType: "json",
		data: {post_id: post_id, time_lapse: time_lapse},
		method: 'get',
		async: false,
		success: function(response){
			data_dashboard=response;
			for(var g=0;g<data_dashboard.length;g++)
			{
				if(parseFloat(data_dashboard[g].spend)>0){
					var paid_cels = document.getElementsByClassName("paid");
					for(var i=0;i<paid_cels.length;i++){
						paid_cels.item(i).style.visibility = "visible";
					}
					break;
				}
				else{
					var paid_cels = document.getElementsByClassName("paid");
					for(var i=0;i<paid_cels.length;i++){
						paid_cels.item(i).style.visibility = "hidden";
					}
				}
			}
		}
	});
	$.ajax({
		url: "php_scripts/get_post_currency.php",
		dataType: "text",
		data: {post_id: post_id},
		method: 'get',
		async: false,
		success: function(response){
			dashboard_currency=response;
		}
	});
}
function selectPostDashboard(){
	UpdateDataDashboard();
	drawChartDashboard(metric_dashboard);
}
function UpdatePostsDashboard(){
	var selector=document.getElementById("combo_box_latest");
	while(selector.length>0){
		selector.remove(0);
	}
	var option = document.createElement("option");
	option.text = "";
	option.value = ""; 
	selector.add(option);
	$.ajax({
		url: "php_scripts/latest_posts.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			for(var j=0;j<response.length;j++)
			{
				var option = document.createElement("option");
				option.text = response[j].name;
				option.value = response[j].id; 
				selector.add(option);
			}
		}
	});
}
function addZero(i){
	if(i<10){
		i="0"+i;	
	}
	return i;
}
function drawChartDashboard(measure){
  metric_dashboard=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(dashboard_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', 'Gasto');
		break;
	case 2:
		data_table.addColumn('number', 'CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', 'Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', 'Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', 'Penetration');
		break;
	case 6:
		data_table.addColumn('number', 'CXW');
		break;
	case 7:
		data_table.addColumn('number', 'CPM');
		break;
	case 8:
		data_table.addColumn('number', 'CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', 'Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', 'Shares');
		break;
	case 11:
		data_table.addColumn('number', 'Likes');
		break;
	case 12:
		data_table.addColumn('number', 'Comments');
		break;
	case 13:
		data_table.addColumn('number', 'Link CPM');
		break;
	case 14:
		data_table.addColumn('number', 'RT CXW');
		break;
	default:
		data_table.addColumn('number', 'CTR Organico');
		break;
  }

  for(var n=0;n<data_dashboard.length;n++){
	data_dashboard[n].record_time=data_dashboard[n].record_time.replace(' ','T');
	data_dashboard[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_dashboard[n].record_time)

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_dashboard[n].spend)*currency_factor]);
		  formating_dashboard = {
			title: 'Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_dashboard[n].organic_ctr)/100, f: data_dashboard[n].organic_ctr+'%'}]);
		  formating_dashboard = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_dashboard[n].paid_link_clicks)]);
		  formating_dashboard = {
			title: 'Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].viral_amp), f: data_dashboard[n].viral_amp+'x'}]);
		  formating_dashboard = {
			title: 'Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].penetration)/100, f: data_dashboard[n].penetration+'%'}]);
		  formating_dashboard = {
			title: 'Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].cxw)*currency_factor]);
		  formating_dashboard = {
			title: 'CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].cpm)*currency_factor]);
		  formating_dashboard = {
			title: 'CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].paid_ctr)/100, f:data_dashboard[n].paid_ctr+'%'}]);
		  formating_dashboard = {
			title: 'CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].link_clicks)]);
		  formating_dashboard = {
			title: 'Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].shares)]);
		  formating_dashboard = {
			title: 'Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].likes)]);
		  formating_dashboard = {
			title: 'Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].comments)]);
		  formating_dashboard = {
			title: 'Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].link_cpm)*currency_factor]);
		  formating_dashboard = {
			title: 'Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 14:
		  if(n<data_dashboard.length-1){
			  var rt_cxw=(parseFloat(data_dashboard[n+1].spend)-parseFloat(data_dashboard[n].spend))/(parseFloat(data_dashboard[n+1].paid_link_clicks)-parseFloat(data_dashboard[n].paid_link_clicks))*currency_factor;
			  data_table.addRow([
				  data_date,
				  rt_cxw
			  ]);
			  formating = {
				title: 'RT CXW',
				//curveType: 'function',
				vAxis: {
					format: 'currency'
				},
				hAxis: {
					format: 'M/d HH:mm',
					gridlines:{
						count: -1
					},
					title: 'Hora'
				},
				legend:{
					position: 'none'
				},
				pointSize: 3,
				trendlines: { 
					0: {
						lineWidth: 10,
						color: 'green',
						opacity: 1
					} 
				}
			  };
		  }
		  break;
	  default:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].organic_ctr)/100, f: data_dashboard[n].organic_ctr+'%'}]);
		  formating_dashboard = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_latest'));

  table.draw(data_table,formating_dashboard);
}
function drawDeltaChartDashboard(measure){
  metric_dashboard=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(dashboard_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', '\u0394 Gasto');
		break;
	case 2:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', '\u0394 Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', '\u0394 Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', '\u0394 Penetration');
		break;
	case 6:
		data_table.addColumn('number', '\u0394 CXW');
		break;
	case 7:
		data_table.addColumn('number', '\u0394 CPM');
		break;
	case 8:
		data_table.addColumn('number', '\u0394 CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', '\u0394 Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', '\u0394 Shares');
		break;
	case 11:
		data_table.addColumn('number', '\u0394 Likes');
		break;
	case 12:
		data_table.addColumn('number', '\u0394 Comments');
		break;
	case 13:
		data_table.addColumn('number', '\u0394 Link CPM');
		break;
	default:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
  }

  for(var n=1;n<data_dashboard.length;n++){
	data_dashboard[n].record_time=data_dashboard[n].record_time.replace(' ','T');
	data_dashboard[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_dashboard[n].record_time)

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_dashboard[n].spend)*currency_factor-parseFloat(data_dashboard[n-1].spend)*currency_factor]);
		  formating_dashboard = {
			title: '\u0394 Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_dashboard[n].organic_ctr)/100-parseFloat(data_dashboard[n-1].organic_ctr)/100, f: (parseFloat(data_dashboard[n].organic_ctr)-parseFloat(data_dashboard[n-1].organic_ctr))+'%'}]);
		  formating_dashboard = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_dashboard[n].paid_link_clicks)-parseFloat(data_dashboard[n-1].paid_link_clicks)]);
		  formating_dashboard = {
			title: '\u0394 Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].viral_amp)-parseFloat(data_dashboard[n-1].viral_amp), f: (parseFloat(data_dashboard[n].viral_amp)-parseFloat(data_dashboard[n-1].viral_amp))+'x'}]);
		  formating_dashboard = {
			title: '\u0394 Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].penetration)/100-parseFloat(data_dashboard[n-1].penetration)/100, f: (parseFloat(data_dashboard[n].penetration)-parseFloat(data_dashboard[n-1].penetration))+'%'}]);
		  formating_dashboard = {
			title: '\u0394 Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,(parseFloat(data_dashboard[n].cxw)-parseFloat(data_dashboard[n-1].cxw))*currency_factor]);
		  formating_dashboard = {
			title: '\u0394 CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,(parseFloat(data_dashboard[n].cpm)-parseFloat(data_dashboard[n-1].cpm))*currency_factor]);
		  formating_dashboard = {
			title: '\u0394 CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].paid_ctr)/100-parseFloat(data_dashboard[n-1].paid_ctr)/100, f: (parseFloat(data_dashboard[n].paid_ctr)-parseFloat(data_dashboard[n-1].paid_ctr))+'%'}]);
		  formating_dashboard = {
			title: '\u0394 CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].link_clicks)-parseFloat(data_dashboard[n-1].link_clicks)]);
		  formating_dashboard = {
			title: '\u0394 Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].shares)-parseFloat(data_dashboard[n-1].shares)]);
		  formating_dashboard = {
			title: '\u0394 Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].likes)-parseFloat(data_dashboard[n-1].likes)]);
		  formating_dashboard = {
			title: '\u0394 Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_dashboard[n].comments)-parseFloat(data_dashboard[n-1].comments)]);
		  formating_dashboard = {
			title: '\u0394 Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,(parseFloat(data_dashboard[n].link_cpm)-parseFloat(data_dashboard[n-1].link_cpm))*currency_factor]);
		  formating_dashboard = {
			title: '\u0394 Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  default:
		  data_table.addRow([data_date,{v: parseFloat(data_dashboard[n].organic_ctr)/100-parseFloat(data_dashboard[n-1].organic_ctr)/100, f: (parseFloat(data_dashboard[n].organic_ctr)-parseFloat(data_dashboard[n].organic_ctr))+'%'}]);
		  formating_dashboard = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_latest'));

  table.draw(data_table,formating_dashboard);
}
//posts latest

//posts promoted
function build_campaign_url(adgroup_id,breakdown){
	var url="";
	url+=url_base;
	url+=adgroup_id+"/insights?breakdowns=";
	url+=breakdown;
	url+='&fields=actions,cost_per_action_type';
	url+="&"+access_token;
	return url;
}
function insertPromotedView(){
	var myNode = document.getElementById("fb_graphs");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
	$.ajax({
		url: "views/promoted_view.html",
		dataType: "html",
		method: 'get',
		async: false,
		success: function(response){
			myNode.innerHTML = response;
			UpdatePostsPromoted();
		}
	});
}
var promoted_currency;
var data_promoted=[];
var metric_promoted=0;
var formating_promoted;
function UpdateDataPromoted(){
	var time_lapse = document.getElementById("time_lapse_promoted").value;
	var post_id=document.getElementById("combo_box_promoted").value;
	if(post_id !== ''){
		$.ajax({
			url: "php_scripts/get_post_data.php",
			dataType: "json",
			data: {post_id: post_id, time_lapse: time_lapse},
			method: 'get',
			async: false,
			success: function(response){
				data_promoted=response;
				for(var g=0;g<data_promoted.length;g++)
				{
					if(parseFloat(data_promoted[g].spend)>0){
						var paid_cels = document.getElementsByClassName("paid");
						for(var i=0;i<paid_cels.length;i++){
							paid_cels.item(i).style.visibility = "visible";
						}
						break;
					}
					else{
						var paid_cels = document.getElementsByClassName("paid");
						for(var i=0;i<paid_cels.length;i++){
							paid_cels.item(i).style.visibility = "hidden";
						}
					}

				}
			}
		});
		$.ajax({
			url: "php_scripts/get_post_currency.php",
			dataType: "text",
			data: {post_id: post_id},
			method: 'get',
			async: false,
			success: function(response){
				promoted_currency=response;
			}
		});
		$.ajax({
            url: "php_scripts/get_campaign.php",
            dataType: "json",
            data: {post_id: post_id},
            method: 'get',
            async: false,
            success: function(response){
                campaign=response.campaign;
                UpdateDataGender(campaign);
                UpdateDataAge(campaign);
                UpdateDataAgeGender(campaign);
                UpdateDataDevice(campaign);
            }
        });//ajax to get breakdowns
	}
}
function selectPostPromoted(){
	UpdateDataPromoted();
	drawChartPromoted(metric_promoted);
}
function UpdatePostsPromoted(){
	var selector=document.getElementById("combo_box_promoted");
	while(selector.length>0){
		selector.remove(0);
	}
	var option = document.createElement("option");
	option.text = "";
	option.value = ""; 
	selector.add(option);
	$.ajax({
		url: "php_scripts/latest_promoted_posts.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			for(var j=0;j<response.length;j++)
			{
				var option = document.createElement("option");
				option.text = response[j].name;
				option.value = response[j].id; 
				selector.add(option);
			}
		}
	});
}
function drawChartPromoted(measure){
  metric_promoted=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(promoted_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', 'Gasto');
		break;
	case 2:
		data_table.addColumn('number', 'CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', 'Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', 'Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', 'Penetration');
		break;
	case 6:
		data_table.addColumn('number', 'CXW');
		break;
	case 7:
		data_table.addColumn('number', 'CPM');
		break;
	case 8:
		data_table.addColumn('number', 'CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', 'Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', 'Shares');
		break;
	case 11:
		data_table.addColumn('number', 'Likes');
		break;
	case 12:
		data_table.addColumn('number', 'Comments');
		break;
	case 13:
		data_table.addColumn('number', 'Link CPM');
		break;
	case 14:
		data_table.addColumn('number', 'RT CXW');
		break;
	default:
		data_table.addColumn('number', 'Link Clicks');
		break;
  }

  var formating;
  for(var n=0;n<data_promoted.length;n++){
	data_promoted[n].record_time=data_promoted[n].record_time.replace(' ','T');
	data_promoted[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_promoted[n].record_time);

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].spend)*currency_factor]);
		  formating = {
			title: 'Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_promoted[n].organic_ctr)/100, f: data_promoted[n].organic_ctr+'%'}]);
		  formating = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].paid_link_clicks)]);
		  formating = {
			title: 'Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].viral_amp)]);
		  formating = {
			title: 'Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_promoted[n].penetration)/100, f: data_promoted[n].penetration}]);
		  formating = {
			title: 'Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].cxw)*currency_factor]);
		  formating = {
			title: 'CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].cpm)*currency_factor]);
		  formating = {
			title: 'CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_promoted[n].paid_ctr)/100, f: data_promoted[n].paid_ctr}]);
		  formating = {
			title: 'CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].link_clicks)]);
		  formating = {
			title: 'Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].shares)]);
		  formating = {
			title: 'Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].likes)]);
		  formating = {
			title: 'Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].comments)]);
		  formating = {
			title: 'Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].link_cpm)*currency_factor]);
		  formating = {
			title: 'Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 14:
		  if(n<data_promoted.length-1){
			  var rt_cxw=(parseFloat(data_promoted[n+1].spend)-parseFloat(data_promoted[n].spend))/(parseFloat(data_promoted[n+1].paid_link_clicks)-parseFloat(data_promoted[n].paid_link_clicks))*currency_factor;
			  data_table.addRow([
				  data_date,
				  rt_cxw
			  ]);
			  formating = {
				title: 'RT CXW',
				//curveType: 'function',
				vAxis: {
					format: 'currency'
				},
				hAxis: {
					format: 'M/d HH:mm',
					gridlines:{
						count: -1
					},
					title: 'Hora'
				},
				legend:{
					position: 'none'
				},
				pointSize: 3,
				trendlines: { 
					0: {
						lineWidth: 10,
						color: 'green',
						opacity: 1
					} 
				}
			  };
		  }
		  break;
	  default:
		  data_table.addRow([data_date,parseFloat(data_promoted[n].link_clicks)]);
		  formating = {
			title: 'Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_promoted'));
  table.draw(data_table,formating);
}
function drawDeltaChartPromoted(measure){
  metric_promoted=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(promoted_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', '\u0394 Gasto');
		break;
	case 2:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', '\u0394 Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', '\u0394 Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', '\u0394 Penetration');
		break;
	case 6:
		data_table.addColumn('number', '\u0394 CXW');
		break;
	case 7:
		data_table.addColumn('number', '\u0394 CPM');
		break;
	case 8:
		data_table.addColumn('number', '\u0394 CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', '\u0394 Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', '\u0394 Shares');
		break;
	case 11:
		data_table.addColumn('number', '\u0394 Likes');
		break;
	case 12:
		data_table.addColumn('number', '\u0394 Comments');
		break;
	case 13:
		data_table.addColumn('number', '\u0394 Link CPM');
		break;
	default:
		data_table.addColumn('number', '\u0394 Link Clicks');
		break;
  }

  var formating;
  for(var n=1;n<data_promoted.length;n++){
	data_promoted[n].record_time=data_promoted[n].record_time.replace(' ','T');
	data_promoted[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_promoted[n].record_time);

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].spend)*currency_factor-parseFloat(data_promoted[n-1].spend)*currency_factor]);
		  formating = {
			title: '\u0394 Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_promoted[n].organic_ctr)/100-parseFloat(data_promoted[n-1].organic_ctr)/100, f: (parseFloat(data_promoted[n].organic_ctr)-parseFloat(data_promoted[n-1].organic_ctr))+'%'}]);
		  formating = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].paid_link_clicks)-parseFloat(data_promoted[n-1].paid_link_clicks)]);
		  formating = {
			title: '\u0394 Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].viral_amp)-parseFloat(data_promoted[n-1].viral_amp)]);
		  formating = {
			title: '\u0394 Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_promoted[n].penetration)/100-parseFloat(data_promoted[n-1].penetration)/100, f: parseFloat(data_promoted[n].penetration)-parseFloat(data_promoted[n-1].penetration)}]);
		  formating = {
			title: '\u0394 Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].cxw)*currency_factor-parseFloat(data_promoted[n-1].cxw)*currency_factor]);
		  formating = {
			title: '\u0394 CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].cpm)*currency_factor-parseFloat(data_promoted[n-1].cpm)*currency_factor]);
		  formating = {
			title: '\u0394 CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_promoted[n].paid_ctr)/100-parseFloat(data_promoted[n-1].paid_ctr)/100, f: parseFloat(data_promoted[n].paid_ctr)-parseFloat(data_promoted[n-1].paid_ctr)}]);
		  formating = {
			title: '\u0394 CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].link_clicks)-parseFloat(data_promoted[n-1].link_clicks)]);
		  formating = {
			title: '\u0394 Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].shares)-parseFloat(data_promoted[n-1].shares)]);
		  formating = {
			title: '\u0394 Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].likes)-parseFloat(data_promoted[n-1].likes)]);
		  formating = {
			title: '\u0394 Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].comments)-parseFloat(data_promoted[n-1].comments)]);
		  formating = {
			title: '\u0394 Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].link_cpm)*currency_factor-parseFloat(data_promoted[n-1].link_cpm)*currency_factor]);
		  formating = {
			title: '\u0394 Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  default:
		  data_table.addRow([data_date,
		  parseFloat(data_promoted[n].link_clicks)-parseFloat(data_promoted[n-1].link_clicks)]);
		  formating = {
			title: '\u0394 Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_promoted'));
  table.draw(data_table,formating);
}

////
var data_gender=[];
var data_age=[];
var data_age_gender=[];
var data_device=[];

//Max Values
function maxValueLinkClicks(data){
	var max=0;
	for(var k=0;k<data.length;k++){
		if(data[k].link_clicks>max){
			max=data[k].link_clicks;
		}
	}
	return max;
}
function maxValueCTR(data){
	var max=0;
	for(var k=0;k<data.length;k++){
		if(data[k].ctr>max){
			max=data[k].ctr;
		}
	}
	return max;
}
//Max Values

//campaigns by gender
function UpdateDataGender(adgroup_id){
	$.ajax({
		url: build_campaign_url(adgroup_id, "['gender']"),
		dataType: "json",
		method: 'get',
		async: false,
		success: function(response){
			data_gender=response.data;
			for(var g=0;g<data_gender.length;g++)
			{
				for(var h=0;h<data_gender[g].actions.length;h++){
					if(data_gender[g].actions[h].action_type=="link_click"){
						data_gender[g].link_clicks=data_gender[g].actions[h].value;
						break;
					}
				}
				for(var h=0;h<data_gender[g].cost_per_action_type.length;h++){
					if(data_gender[g].cost_per_action_type[h].action_type=="link_click"){
						data_gender[g].cxw=data_gender[g].cost_per_action_type[h].value;
						break;
					}
				}
				data_gender[g].ctr=0;
				if(typeof data_gender[g].website_ctr !== 'undefined'){
					data_gender[g].ctr=data_gender[g].website_ctr[0].value;
				}
			}
		}
	});
	drawTableGender();
}
function drawTableGender(){
  var formating_gender;
  var data_table = new google.visualization.DataTable();
  
  data_table.addColumn({type: 'string', label: 'Género'});
  data_table.addColumn({type: 'number', label: 'Link Clicks'});
  data_table.addColumn({type: 'number', label: 'CXW'});
  data_table.addColumn({type: 'number', label: 'CTR'});
  
  if(data_gender!=null)
  {
	  for(var n=0;n<data_gender.length;n++){
  	 	 data_table.addRow([
			 data_gender[n].gender,
			 data_gender[n].link_clicks,
			 {v: data_gender[n].cxw, f: '$ '+data_gender[n].cxw.toFixed(5)},
			 {v: data_gender[n].ctr, f: data_gender[n].ctr.toFixed(2)+'%'}
   	   ]);
  	}
  }

  // Set chart options
  var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'100%',
  };

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('chart_div_gender'));
  
  var formatter_link_clicks = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: maxValueLinkClicks(data_gender)});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: maxValueCTR(data_gender)});
  var formatter_cxw = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
  
  formatter_link_clicks.format(data_table, 1);
  formatter_ctr.format(data_table, 3);
  formatter_cxw.format(data_table, 2);
  
  table.draw(data_table, options);
}
//campaigns by gender

//campaigns by age
function UpdateDataAge(adgroup_id){
	$.ajax({
		url: build_campaign_url(adgroup_id, "['age']"),
		dataType: "json",
		method: 'get',
		async: false,
		success: function(response){
			data_age=response.data;
			for(var g=0;g<data_age.length;g++)
			{
				for(var h=0;h<data_age[g].actions.length;h++){
					if(data_age[g].actions[h].action_type=="link_click"){
						data_age[g].link_clicks=data_age[g].actions[h].value;
						break;
					}
				}
				for(var h=0;h<data_age[g].cost_per_action_type.length;h++){
					if(data_age[g].cost_per_action_type[h].action_type=="link_click"){
						data_age[g].cxw=data_age[g].cost_per_action_type[h].value;
						break;
					}
				}
				data_age[g].ctr=0;
				if(typeof data_age[g].website_ctr !== 'undefined'){
					data_age[g].ctr=data_age[g].website_ctr[0].value;
				}
			}
		}
	});
	drawTableAge();
}
function drawTableAge(){
  var formating_age;
  var data_table = new google.visualization.DataTable();
  
  data_table.addColumn({type: 'string', label: 'Género'});
  data_table.addColumn({type: 'number', label: 'Link Clicks'});
  data_table.addColumn({type: 'number', label: 'CXW'});
  data_table.addColumn({type: 'number', label: 'CTR'});
  
  if(data_age!=null)
  {
	  for(var n=0;n<data_age.length;n++){
  	 	 data_table.addRow([
			 data_age[n].age,
			 data_age[n].link_clicks,
			 {v: data_age[n].cxw, f: '$ '+data_age[n].cxw.toFixed(5)},
			 {v: data_age[n].ctr, f: data_age[n].ctr.toFixed(2)+'%'}
   	   ]);
  	}
  }

  // Set chart options
  var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'100%',
  };

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('chart_div_age'));
  
  var formatter_link_clicks = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: maxValueLinkClicks(data_age)});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: maxValueCTR(data_age)});
  var formatter_cxw = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
  
  
  formatter_link_clicks.format(data_table, 1);
  formatter_ctr.format(data_table, 3);
  formatter_cxw.format(data_table, 2);
  
  table.draw(data_table, options);
}
//campaigns by age

//campaigns by age-gender
function UpdateDataAgeGender(adgroup_id){
	$.ajax({
		url: build_campaign_url(adgroup_id, "['age','gender']"),
		dataType: "json",
		method: 'get',
		async: false,
		success: function(response){
			data_age_gender=response.data;
			for(var g=0;g<data_age_gender.length;g++)
			{
				data_age_gender[g].link_clicks=0;
				data_age_gender[g].cxw=0;
				if(typeof data_age_gender[g].actions !== 'undefined'){
					for(var h=0;h<data_age_gender[g].actions.length;h++){
						if(data_age_gender[g].actions[h].action_type=="link_click"){
							data_age_gender[g].link_clicks=data_age_gender[g].actions[h].value;
							break;
						}
					}
					for(var h=0;h<data_age_gender[g].cost_per_action_type.length;h++){
						if(data_age_gender[g].cost_per_action_type[h].action_type=="link_click"){
							data_age_gender[g].cxw=data_age_gender[g].cost_per_action_type[h].value;
							break;
						}
					}
				}
				data_age_gender[g].ctr=0;
				if(typeof data_age_gender[g].website_ctr !== 'undefined'){
					data_age_gender[g].ctr=data_age_gender[g].website_ctr[0].value;
				}
				data_age_gender[g].category=data_age_gender[g].age+' '+data_age_gender[g].gender;
			}
		}
	});
	drawTableAgeGender();
}
function drawTableAgeGender(){
  var formating_age;
  var data_table = new google.visualization.DataTable();
  
  data_table.addColumn({type: 'string', label: 'Género'});
  data_table.addColumn({type: 'number', label: 'Link Clicks'});
  data_table.addColumn({type: 'number', label: 'CXW'});
  data_table.addColumn({type: 'number', label: 'CTR'});
  
  if(data_age_gender!=null)
  {
	  for(var n=0;n<data_age_gender.length;n++){
  	 	 data_table.addRow([
			 data_age_gender[n].category,
			 data_age_gender[n].link_clicks,
			 {v: data_age_gender[n].cxw, f: '$ '+data_age_gender[n].cxw.toFixed(5)},
			 {v: data_age_gender[n].ctr, f: data_age_gender[n].ctr.toFixed(2)+'%'}
   	   ]);
  	}
  }

  // Set chart options
  var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'100%',
  };

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('chart_div_age_gender'));
  
  var formatter_link_clicks = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: maxValueLinkClicks(data_age_gender)});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: maxValueCTR(data_age_gender)});
  var formatter_cxw = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
  
  formatter_link_clicks.format(data_table, 1);
  formatter_ctr.format(data_table, 3);
  formatter_cxw.format(data_table, 2);
  
  table.draw(data_table, options);
}
//campaigns by age-gender
	
//campaigns by device
function UpdateDataDevice(adgroup_id){
	$.ajax({
		url: build_campaign_url(adgroup_id, "['placement']"),
		dataType: "json",
		method: 'get',
		async: false,
		success: function(response){
			data_device=response.data;
			for(var g=0;g<data_device.length;g++)
			{
				for(var h=0;h<data_device[g].actions.length;h++){
					if(data_device[g].actions[h].action_type=="link_click"){
						data_device[g].link_clicks=data_device[g].actions[h].value;
						break;
					}
				}
				for(var h=0;h<data_device[g].cost_per_action_type.length;h++){
					if(data_device[g].cost_per_action_type[h].action_type=="link_click"){
						data_device[g].cxw=data_device[g].cost_per_action_type[h].value;
						break;
					}
				}
				data_device[g].ctr=0;
				if(typeof data_device[g].website_ctr !== 'undefined'){
					data_device[g].ctr=data_device[g].website_ctr[0].value;
				}
			}
		}
	});
	drawTableDevice();
}
function drawTableDevice(){
  var formating_device;
  var data_table = new google.visualization.DataTable();
  
  data_table.addColumn({type: 'string', label: 'Device'});
  data_table.addColumn({type: 'number', label: 'Link Clicks'});
  data_table.addColumn({type: 'number', label: 'CXW'});
  data_table.addColumn({type: 'number', label: 'CTR'});
  
  if(data_device!=null)
  {
	  for(var n=0;n<data_device.length;n++){
  	 	 data_table.addRow([
			 data_device[n].placement,
			 data_device[n].link_clicks,
			 {v: data_device[n].cxw, f: '$ '+data_device[n].cxw.toFixed(5)},
			 {v: data_device[n].ctr, f: data_device[n].ctr.toFixed(2)+'%'}
   	   ]);
  	}
  }

  // Set chart options
  var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'100%',
  };

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('chart_div_device'));
  
  var formatter_link_clicks = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: maxValueLinkClicks(data_age)});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: maxValueCTR(data_age)});
  var formatter_cxw = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
  
  
  formatter_link_clicks.format(data_table, 1);
  formatter_ctr.format(data_table, 3);
  formatter_cxw.format(data_table, 2);
  
  table.draw(data_table, options);
}
//campaigns by device
////
//posts promoted

//Twitter Trends
var topics;
function getTwitterTrends(){
	var myNode = document.getElementById("twitter_table_rows");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
	$.ajax({
		url: "php_scripts/twitter_trends_mexico.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			topics=response[0].trends;
            topics = topics.slice(0,10);
		}
	});
	for(var i=0;i<topics.length;i++){
		var tr=document.createElement("tr");
		var td1=document.createElement("td");
		td1.appendChild(document.createTextNode((i+1)+'.-   '+topics[i].name));
		tr.appendChild(td1);	
		myNode.appendChild(tr);
	}
}
//Twitter Trends

//active Users Hisotry
function getActiveUsersHistory(date){
	//console.log(date);
	$.ajax({
		url: "php_scripts/get_active_users.php",
		type: 'get',
		dataType: "json",
		data: {'date': 'date'+date},
		async: true,
		success: function(response){
			//console.log(response);	
			drawChartActiveUsers(response);//time and number
			//console.log(response);
		}
	});//get active users according to selected date
}
function drawChartActiveUsers(record){
  var data_table = new google.visualization.DataTable();
  var data_table_type = new google.visualization.DataTable();

  data_table.addColumn('datetime', 'Hora');
  data_table.addColumn('number', 'Active Organic Users');
  data_table.addColumn('number', 'Active Paid Users');
  
  data_table_type.addColumn('datetime', 'Hora');
  data_table_type.addColumn('number', 'Active Organic Users');
  data_table_type.addColumn('number', 'Active Paid Users');
  data_table_type.addColumn('number', 'Active Total Users');


  var formating, formating_percentage, formating_type;
  for(var n=0;n<record.length;n++){
	record[n].record_time=record[n].record_time.replace(' ','T');
	record[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(record[n].record_time);

	data_table.addRow([
		data_date,
		parseInt(record[n].active_organic),
		parseInt(record[n].active)-parseInt(record[n].active_organic)
	]);
	
	data_table_type.addRow([
		data_date,
		parseInt(record[n].active_organic),
		parseInt(record[n].active)-parseInt(record[n].active_organic),
		parseInt(record[n].active)
	]);
  }

	formating = {
		isStacked: true,
		title: 'Active users by paid-unpaid '+data_date.toDateString(),
		//curveType: 'function',
		vAxis: {
			//format: 'number'
		},
		hAxis: {
			format: 'M/d HH:mm',
			gridlines:{
				count: -1
			},
			title: 'Hora'
		},
		pointSize: 3,
	};
	
	formating_percentage = {
		isStacked: 'percent',
		title: 'Active users by paid-unpaid percentage '+data_date.toDateString(),
		//curveType: 'function',
		vAxis: {
			//format: 'number'
		},
		hAxis: {
			format: 'M/d HH:mm',
			gridlines:{
				count: -1
			},
			title: 'Hora'
		},
		pointSize: 3,
	};
	
	formating_type = {
		title: 'Active users '+data_date.toDateString(),
		//curveType: 'function',
		vAxis: {
			//format: 'number'
		},
		hAxis: {
			format: 'M/d HH:mm',
			gridlines:{
				count: -1
			},
			title: 'Hora'
		},
		pointSize: 3,
	};

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.AreaChart(document.getElementById('chart_div_active'));
  table.draw(data_table,formating);
  
  // Instantiate and draw our percentage chart, passing in some options.
  var table_percent = new google.visualization.AreaChart(document.getElementById('chart_div_active_percent'));
  table_percent.draw(data_table,formating_percentage);
  
  var table_type = new google.visualization.LineChart(document.getElementById('chart_div_active_type'));
  table_type.draw(data_table_type,formating_type);
}
function getActiveUsersHistoryOpen(date){
	//console.log(date);
	$.ajax({
		url: "php_scripts/get_active_users_open.php",
		type: 'get',
		dataType: "json",
		data: {'date': 'date'+date},
		async: true,
		success: function(response){
			//console.log(response);	
			drawChartActiveUsersOpen(response);//time and number
			//console.log(response);
		}
	});//get active users according to selected date
}
function drawChartActiveUsersOpen(record){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn('datetime', 'Hora');
  data_table.addColumn('number', 'Active Users');


  var formating;
  for(var n=0;n<record.length;n++){
	record[n].record_time=record[n].record_time.replace(' ','T');
	record[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(record[n].record_time);

	data_table.addRow([
		data_date,
		parseInt(record[n].active)
	]);
	
  }

	formating = {
		isStacked: true,
		title: 'Active users '+data_date.toDateString(),
		//curveType: 'function',
		vAxis: {
			//format: 'number'
		},
		hAxis: {
			format: 'M/d HH:mm',
			gridlines:{
				count: -1
			},
			title: 'Hora'
		},
		pointSize: 3,
	};

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.AreaChart(document.getElementById('chart_div_active_open'));
  table.draw(data_table,formating);
}
//active Users Hisotry

//Leaderboard
var pages;
function getArticlesWP(){
	authors={};
	week_authors={};
	week_posts=[];  

	gapi.client.load('analytics','v3');

	//console.log(gapi.client.analytics);

	$.ajax({
		url: 'https://eldeforma.com/?json=get_date_posts&date='+month.value+'&count=800&status=published&include=author,url,date,status,categories',
		dataType: 'jsonp',
		async: false,
		success: function(response){

			posts=response.posts;
			if($('#removeNonCreativePosts')[0].checked){
				for(var i=0;i<posts.length;i++){
					if(!forbiddenCategories(posts[i].categories)){
						posts.splice(i,1);
						i--;
					}
				}
			}

			//i=posts.length-1;
			if(posts.length>0){
				getPageviews();
			}

		}
	});
	/*$.ajax({
		url: 'https://eldeforma.com/?json=get_latest_posts&count=800&include=author,url,date,status',
		dataType: 'jsonp',
		async: false,
		success: function(response){

			posts=response.posts;

			var date, now = new Date();
			var diffDays;
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds

			if(now.getDay() > 0){
				var week_start=new Date(now.getTime() - (now.getDay()-1)*oneDay);
			}
			else{
				var week_start=new Date(now.getTime() - oneDay*6);
			}

			week_start.setHours(0);
			week_start.setMinutes(0);
			week_start.setSeconds(0);

			for(var j=0;j<posts.length;j++){
				date = new Date(posts[j].date);

				if(date.getTime() > week_start.getTime())
				{
					week_posts.push(posts[j]);
				}
				else{
					continue;
				}
			}

			//j=week_posts.length-1;
			if(week_posts.length>0){
				getWeekPageviews(j);
			}
		}
	});*/
}
function getPageviews(){
	var now = new Date();
	if(document.getElementById('organic').checked){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": month.value+'-01',
			"end-date": "today",
			metrics: "ga:pageviews",
			dimensions: "ga:pagePath",
			filters: "ga:pagePath=@"+month.value.replace("-","/")+";ga:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@SRE=1;ga:pagePath!@GUIA=1;ga:pagePath!@CST=1",
			sort: '-ga:pageviews',
			fields: 'rows,totalsForAllResults'
		});
		requestPageviews.execute(function(resp){
			//console.log(resp.totalsForAllResults['ga:pageviews']);
			pages=resp.rows;
			//console.log(pages.length,pages);

			for(var i=0;i<posts.length;i++){
				posts[i].pageviews=0;
				for(var j=0;j<pages.length;j++){
					if(posts[i].url.indexOf(pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/'))) > -1  && pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/')).length >= 6){
						posts[i].pageviews+=parseInt(pages[j][1]);
					}
				}
			}
			//console.log('got organic posts',pages);
			sortPosts();
		});
	}else{
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": month.value+'-01',
			"end-date": "today",
			metrics: "ga:pageviews",
			dimensions: "ga:pagePath",
			filters: "ga:pagePath=@"+month.value.replace("-","/"),
			sort: '-ga:pageviews',
			fields: 'rows,totalsForAllResults'
		});
		requestPageviews.execute(function(resp){
			//console.log(resp.totalsForAllResults['ga:pageviews']);
			pages=resp.rows;
			//console.log(pages.length,pages);

			for(var i=0;i<posts.length;i++){
				posts[i].pageviews=0;
				for(var j=0;j<pages.length;j++){
					if(posts[i].url.indexOf(pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/'))) > -1){
						posts[i].pageviews+=parseInt(pages[j][1]);
					}
				}
			}
			//console.log('got all posts',pages);
			sortPosts();
		});
	}
}
function getWeekPageviews(j){
	var now = new Date();
	var requestPageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": "8daysAgo",
		"end-date": "today",
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath",
		filters: "ga:pagePath=@"+now.toISOString().substring(0,now.toISOString().indexOf("T")-3).replace("-","/")+";ga:pagePath!@?",
		fields: 'rows',
	});
	requestPageviews.execute(function(resp){
		var pages=resp.rows;

		for(var i=0;i<week_posts.length;i++){
			week_posts[i].pageviews=0;
			for(var j=0;j<pages.length;j++){
				if(week_posts[i].url.indexOf(pages[j][0]) > -1){
					week_posts[i].pageviews+=parseInt(pages[j][1]);
				}
			}
		}
		sortWeekPosts();
	});
}
function sortPosts(){
	var totals;
	var nonZero;
	
	for(var i=0;i<posts.length;i++){
		var nickname = posts[i].author.nickname;
		authors[nickname] = [];
	}

	for(var i=0;i<posts.length;i++){
		authors[posts[i].author.nickname].push({"url": posts[i].url, "pageviews": posts[i].pageviews});
	}
	
	$.each(authors, function(key, value) {
		//console.log(authors[key]);
		totals=0;
		nonZero=0;
		for(var j=0;j<authors[key].length;j++)
		{
			totals+=authors[key][j].pageviews;
			if(authors[key][j].pageviews>0){
				nonZero++;
			}
			
		}
		authors[key].total=totals;
		if(nonZero>0){
			authors[key].average=totals/nonZero;
		}else{
			authors[key].average=0;
		}
	});
	
	drawTableLeaderboard();
}
function sortWeekPosts(){
	var totals;
	var nonZero;
	
	for(var i=0;i<week_posts.length;i++){
		var nickname = week_posts[i].author.nickname;
		week_authors[nickname] = [];
	}

	for(var i=0;i<week_posts.length;i++){
		week_authors[week_posts[i].author.nickname].push({"url": week_posts[i].url, "pageviews": week_posts[i].pageviews});
	}
	
	$.each(week_authors, function(key, value) {
		//console.log(authors[key]);
		totals=0;
		nonZero=0;
		for(var j=0;j<week_authors[key].length;j++)
		{
			totals+=week_authors[key][j].pageviews;
			if(week_authors[key][j].pageviews>0){
				nonZero++;
			}
			
		}
		week_authors[key].total=totals;
		if(nonZero>0){
			week_authors[key].average=totals/nonZero;
		}else{
			week_authors[key].average=0;
		}
	});
	
	drawWeekTableLeaderboard();
}
function drawTableLeaderboard(){
	var posts_data = new google.visualization.DataTable();
	var authors_data = new google.visualization.DataTable();
	var average_data = new google.visualization.DataTable();
  
	posts_data.addColumn({type: 'string', label: 'Autor'});
	posts_data.addColumn({type: 'string', label: 'Nota'});
	posts_data.addColumn({type: 'number', label: 'PV'});

	authors_data.addColumn({type: 'string', label: 'Autor'});
	authors_data.addColumn({type: 'number', label: 'Notas'});
	authors_data.addColumn({type: 'number', label: 'PV (totales)'});

	average_data.addColumn({type: 'string', label: 'Autor'});
	average_data.addColumn({type: 'number', label: 'PV (promedio)'});
  
  	var Office = ["Yado","Gazapotl","PAQ","Pamela","Alan","Dany Trejo","Maestro Bergstrom","jovinhoestrada","majo","Lui","Rulo", "Nanu", "aldeni","Are"];
	for(var n=0;n<posts.length;n++){
		if($('#officeWriters')[0].checked && $.inArray(posts[n].author.nickname,Office) != -1){
			posts_data.addRow([
				posts[n].author.nickname,
				(posts[n].url.substring(32,52)+'...').link(posts[n].url),
				{v: posts[n].pageviews, f: numberWithCommas(posts[n].pageviews)},
			]);
		}else if($('#officeWriters')[0].checked){
		
		}
		else{
			posts_data.addRow([
				posts[n].author.nickname,
				(posts[n].url.substring(32,52)+'...').link(posts[n].url),
				{v: posts[n].pageviews, f: numberWithCommas(posts[n].pageviews)},
			]);
		}
	}

	$.each(authors, function(key, value) {
		authors_data.addRow([
			key,
			authors[key].length,
			{v: authors[key].total, f: numberWithCommas(authors[key].total)}
		]);
	});

	$.each(authors, function(key, value) {
		average_data.addRow([
			key,
			{v: authors[key].average, f: numberWithCommas((authors[key].average).toFixed(0))}
		]);
	});

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'sortColumn': 1,
		'sortAscending': false
	};
	
	var options_a = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 2,
		'sortAscending': false
	};

	// Instantiate and draw our chart, passing in some options.
	var posts_table = new google.visualization.Table(document.getElementById('chart_div_posts'));
	var authors_table = new google.visualization.Table(document.getElementById('chart_div_authors'));
	var average_table = new google.visualization.Table(document.getElementById('chart_div_average'));
  
	google.visualization.events.addListener(posts_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(posts_table, 'sort', function() {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var o=0;o < links.length;o++){
			links[o].setAttribute('target','_blank');
		}
	});
  
	posts_table.draw(posts_data, options_a);
	authors_table.draw(authors_data, options);
	average_table.draw(average_data, options);
	
	//console.log("done");
}
function drawWeekTableLeaderboard(){
	var posts_data = new google.visualization.DataTable();
	var authors_data = new google.visualization.DataTable();
	var average_data = new google.visualization.DataTable();
  
	posts_data.addColumn({type: 'string', label: 'Autor'});
	posts_data.addColumn({type: 'string', label: 'Nota'});
	posts_data.addColumn({type: 'number', label: 'PV'});

	authors_data.addColumn({type: 'string', label: 'Autor'});
	authors_data.addColumn({type: 'number', label: 'PV (totales)'});

	average_data.addColumn({type: 'string', label: 'Autos'});
	average_data.addColumn({type: 'number', label: 'PV (promedio)'});
  
	for(var n=0;n<week_posts.length;n++){
		posts_data.addRow([
			week_posts[n].author.nickname,
			(week_posts[n].url.substring(32,62)+'...').link(week_posts[n].url),
			{v: week_posts[n].pageviews, f: numberWithCommas(week_posts[n].pageviews)},
		]);
	}

	$.each(week_authors, function(key, value) {
		authors_data.addRow([
			key,
			{v: week_authors[key].total, f: numberWithCommas(week_authors[key].total)}
		]);
	});

	$.each(week_authors, function(key, value) {
		average_data.addRow([
			key,
			{v: week_authors[key].average, f: numberWithCommas((week_authors[key].average).toFixed(0))}
		]);
	});

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'sortColumn': 1,
		'sortAscending': false
	};
	
	var options_a = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 2,
		'sortAscending': false
	};

	// Instantiate and draw our chart, passing in some options.
	var posts_table = new google.visualization.Table(document.getElementById('chart_div_week_posts'));
	var authors_table = new google.visualization.Table(document.getElementById('chart_div_week_authors'));
	var average_table = new google.visualization.Table(document.getElementById('chart_div_week_average'));
  
	google.visualization.events.addListener(posts_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(posts_table, 'sort', function() {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var o=0;o < links.length;o++){
			links[o].setAttribute('target','_blank');
		}
	});
  
	posts_table.draw(posts_data, options_a);
	authors_table.draw(authors_data, options);
	average_table.draw(average_data, options);
	
	console.log("done");
}
function forbiddenCategories(categories){
	if(typeof categories === 'undefined'){
		return true;
	}
	for(var i=0;i<categories.length;i++){
		if(categories[i].slug == 'videos' || categories[i].slug == 'la-duda' || categories[i].slug == 'memes'){
			return false;
		}
	}
	return true;
}
//leaderboard

//leaderboard select
function getArticlesWPSelect(){
	//$('#chart_div_select_posts')[0].removeChild($('#chart_div_select_posts')[0].firstChild);
	select_authors={};
	select_posts=[];
	dates=[];  

	gapi.client.load('analytics','v3');
	
	var start=new Date($('#from')[0].value);
	var end=new Date($('#to')[0].value);
	
	console.log(start);
	
	var current_date;
	var start_cushion=new Date(start.getTime()-1000*60*60*24*3), end_cushion=new Date(end.getTime()+1000*60*60*24*3);

	current_date=start_cushion;
	while(current_date <= end_cushion){
		//console.log(current_date.toISOString().substr(0,10));
		dates.push(current_date.toISOString().substr(0,10));
		current_date.setTime(current_date.getTime()+1000*60*60*24);
	}
	

	for(var j=0;j<dates.length;j++){
		$.ajax({
			url: 'https://eldeforma.com/?json=get_date_posts&date='+dates[j]+'&count=100&status=published&include=author,url,date,status',
			dataType: 'jsonp',
			async: false,
			indexValue: j,
			success: function(response){
				for(var i=0;i<response.posts.length;i++){
					select_posts.push(response.posts[i]);
				}
				//console.log(this.indexValue);
				if(this.indexValue==dates.length-1){
					getPageviewsSelect(start,end);
				}
			}
		});
	}

}
function getPageviewsSelect(start,end){
	//console.log(start,end);
	var now = new Date();
	var requestPageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": start.toISOString().substr(0,10),
		"end-date": end.toISOString().substr(0,10),
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath",
		filters: "ga:pagePath!@?;ga:pagePath!=/",
		sort: '-ga:pageviews',
		fields: 'rows',
	});
	requestPageviews.execute(function(resp){
		var pages=resp.rows;
		console.log(pages.length);

		for(var i=0;i<select_posts.length;i++){
			select_posts[i].pageviews=0;
			for(var j=0;j<pages.length;j++){
				if(select_posts[i].url.indexOf(pages[j][0]) > -1){
					select_posts[i].pageviews+=parseInt(pages[j][1]);
				}
			}
		}
		sortPostsSelect();
	});
}
function sortPostsSelect(){
	var totals;
	var nonZero;
	
	for(var i=0;i<select_posts.length;i++){
		var nickname = select_posts[i].author.nickname;
		select_authors[nickname] = [];
	}

	for(var i=0;i<select_posts.length;i++){
		select_authors[select_posts[i].author.nickname].push({"url": select_posts[i].url, "pageviews": select_posts[i].pageviews});
	}
	
	$.each(select_authors, function(key, value) {
		//console.log(authors[key]);
		totals=0;
		nonZero=0;
		for(var j=0;j<select_authors[key].length;j++)
		{
			totals+=select_authors[key][j].pageviews;
			if(select_authors[key][j].pageviews>0){
				nonZero++;
			}
			
		}
		select_authors[key].total=totals;
		if(nonZero>0){
			select_authors[key].average=totals/nonZero;
		}else{
			select_authors[key].average=0;
		}
	});
	
	drawTableSelect();
}
function drawTableSelect(){
	var posts_data = new google.visualization.DataTable();
  
	posts_data.addColumn({type: 'string', label: 'Autor'});
	posts_data.addColumn({type: 'string', label: 'Nota'});
	posts_data.addColumn({type: 'number', label: 'PV'});
  
  	var Colabs = ['Adolfo Santino','Ddtorresf','Gordolobo','Jos','Omar','Ugesaurio','Emisan','Valenzuela','Fercosta','Alde','DonPotasio'];
	for(var n=0;n<select_posts.length;n++){
		if($('#collabs_only')[0].checked && $.inArray(select_posts[n].author.nickname,Colabs) > -1){
			posts_data.addRow([
				select_posts[n].author.nickname,
				(select_posts[n].url.substring(32,100)+'...').link(select_posts[n].url),
				{v: select_posts[n].pageviews, f: numberWithCommas(select_posts[n].pageviews)},
			]);
		}
		if(!$('#collabs_only')[0].checked){
			posts_data.addRow([
				select_posts[n].author.nickname,
				(select_posts[n].url.substring(32,100)+'...').link(select_posts[n].url),
				{v: select_posts[n].pageviews, f: numberWithCommas(select_posts[n].pageviews)},
			]);
		}
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'sortColumn': 1,
		'sortAscending': false
	};
	
	var options_a = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 2,
		'sortAscending': false
	};

	// Instantiate and draw our chart, passing in some options.
	var posts_table = new google.visualization.Table(document.getElementById('chart_div_select_posts'));  
	
	google.visualization.events.addListener(posts_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(posts_table, 'sort', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
    
	posts_table.draw(posts_data, options_a);	
	//console.log("done");
}
//leaderboard select

/////multiple post select
function getFacebookStats(){
	for(var i=0;i<selected_posts.length;i++){
		query.edge = selected_posts[i].id;//query latest posts
		query.fields = "link"+
			",shares"+
			",likes.limit(0).summary(1).as(likes)"+
			",comments.limit(0).summary(1).as(comments)"+
			",insights.metric(post_impressions_unique).fields(values).as(reach)"+
			",insights.metric(post_impressions).fields(values).as(impressions)"+
			",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
			",insights.metric(post_negative_feedback_by_type).fields(values).as(negative_feedback)"+
			",insights.metric(post_consumptions).fields(values).as(interactions)"+
			",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
		$.ajax({
			url: buildURL(query,pageTokens['El Deforma'],1),
		  	type: 'get',
		    dataType: 'json',
		    async: false,
  			success: function(response){
		    	selected_posts[i].link=response.link;
		    	selected_posts[i].shares=typeof response.shares === 'undefined' ? 0 : response.shares.count;
		    	selected_posts[i].unique_imp=response.reach.data[0].values[0].value;
		    	selected_posts[i].link_clicks=response.consumptions.data[0].values[0].value["link clicks"];
		    	selected_posts[i].hide = typeof response.negative_feedback.data[0].values[0].value.hide_clicks === 'undefined' ? 0 : response.negative_feedback.data[0].values[0].value.hide_clicks;
				selected_posts[i].spam = typeof response.negative_feedback.data[0].values[0].value.report_spam_clicks === 'undefined' ? 0 : response.negative_feedback.data[0].values[0].value.report_spam_clicks;
				selected_posts[i].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
				selected_posts[i].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
	    	}
	    });
	}
	
	j=0;
	queryAnalyticsSelected(selected_posts[j]);
	
	/*var showButton = setTimeout(function(){
		document.getElementById("show_table").style.visibility = 'visible';
	},1000*7);*/
}
function queryAnalyticsSelected(post){
	post.link = post.link.replace('https://eldeforma.com','');
	if(post.link.indexOf('?utm')!=-1){
		post.link = post.link.substring(0,post.link.indexOf('?utm'));
	}
	var date= new Date();
	var month = date.getMonth()>=9 ? (date.getMonth()+1) : "0"+(date.getMonth()+1);
	var day = date.getDate()>=10 ? (date.getDate()) : "0"+(date.getDate());
	//console.log((parseInt(date.getFullYear())-1)+'-'+month+'-'+day);
	gapi.client.load('analytics','v3', function(){
		if(!$('#organicOnly')[0].checked){
			var requestPageviews = gapi.client.analytics.data.ga.get({
				ids: "ga:41142760",
				"start-date": "365daysAgo",
				"end-date": "today",
				metrics: "ga:pageviews",
				fields: "totalsForAllResults",
				output: 'json',
				filters: "ga:pagePath=@"+post.link
			});
			requestPageviews.execute(function(resp){
				post.pageviews = resp.totalsForAllResults["ga:pageviews"];
			});
		}else{
			var requestPageviews = gapi.client.analytics.data.ga.get({
				ids: "ga:41142760",
				"start-date": "365daysAgo",
				"end-date": "today",
				metrics: "ga:pageviews",
				fields: "totalsForAllResults",
				output: 'json',
				filters: "ga:pagePath=@"+post.link+";ga:pagePath!@promoted=true;ga:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@SRE=1;ga:pagePath!@GUIA=1;ga:pagePath!@CST=1"
			});
			requestPageviews.execute(function(resp){
				post.pageviews = resp.totalsForAllResults["ga:pageviews"];
			});
		}
		var requestUsers = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "365daysAgo",
			"end-date": "today",
			metrics: "ga:users",
			fields: "totalsForAllResults",
			output: 'json',
			filters: "ga:pagePath=@"+post.link
		});
		requestUsers.execute(function(resp){
			post.users = resp.totalsForAllResults["ga:users"];
		});
		var requestTime = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "365daysAgo",
			"end-date": "today",
			metrics: "ga:avgTimeOnPage",
			fields: "totalsForAllResults",
			output: 'json',
			filters: "ga:pagePath=@"+post.link
		});
		requestTime.execute(function(resp){
			post.time_on_page = resp.totalsForAllResults["ga:avgTimeOnPage"];
		});
		var requestSocial = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "90daysAgo",
			"end-date": "today",
			metrics: "ga:totalEvents",
			dimensions: "ga:eventAction",
			fields: "rows",
			output: 'json',
			filters: "ga:eventLabel=@"+post.link
		});
		requestSocial.execute(function(resp){
			//console.log(resp);
			post.site_shares = 0;
			post.site_tweets = 0
			post.site_whatsapp = 0;

			if(typeof resp.rows === 'undefined')
			{

			}
			else{
				for(var l=0;l<resp.rows.length;l++){
					switch(resp.rows[l][0])
					{
						case "Facebook Share Class":
							post.site_shares = resp.rows[l][1];
							break;
						case "Twitter Tweet Class":
							post.site_tweets = resp.rows[l][1];
							break;
						case "WhatsApp Message Class":
							post.site_whatsapp = resp.rows[l][1];
							break;
						default:
							break;
					}
				}
			}
		});
	});
	
	if(j<selected_posts.length){
		setTimeout(function(){
			queryAnalyticsSelected(selected_posts[j++]);
		},2500);//recursive call to get metrics
	}
	else{
		setTimeout(function(){
			clickToTable();
		},4000);
	}
	
}
function clickToTable(){
	drawSelectTable(selected_posts);
}
function drawSelectTable(data){
  var data_table = new google.visualization.DataTable();
  var data_tableTotals = new google.visualization.DataTable();
  
  data_table.addColumn({type: 'number', label: 'Orden', id: 'orden'});
  data_table.addColumn({type: 'string', label: 'Nombre', id: 'nombre'});
  data_table.addColumn({type: 'number', label: 'Pageviews', id: 'pageviews'});
  data_table.addColumn({type: 'number', label: 'Tiempo en el Sitio', id: 'time_on_site'});
  data_table.addColumn({type: 'number', label: 'Shares en Sitio', id: 'site_shares'});
  data_table.addColumn({type: 'number', label: 'Tweets en Sitio', id: 'site_tweets'});
  data_table.addColumn({type: 'number', label: 'Whatsapps en Sitio', id: 'site_whatsapp'});
  data_table.addColumn({type: 'number', label: 'Usuarios únicos', id: 'users'});
  data_table.addColumn({type: 'number', label: 'FB Link Clicks', id: 'clicks'});
  data_table.addColumn({type: 'number', label: 'FB Reach (Imp. únicas)', id: 'reach'});
  data_table.addColumn({type: 'number', label: 'FB Shares', id: 'shares', id: 'shares'});
  data_table.addColumn({type: 'number', label: 'FB Likes', id: 'likes'});
  data_table.addColumn({type: 'number', label: 'FB Comments', id: 'comments'});
  data_table.addColumn({type: 'string', label: 'FB Hide Post', id: 'hide'});
  data_table.addColumn({type: 'string', label: 'FB Spam', id: 'spam'});

  data_tableTotals.addColumn({type: 'number', label: 'Orden', id: 'orden'});
  data_tableTotals.addColumn({type: 'string', label: 'Nombre', id: 'nombre'});
  data_tableTotals.addColumn({type: 'number', label: 'Pageviews', id: 'pageviews'});
  data_tableTotals.addColumn({type: 'number', label: 'Tiempo en el Sitio', id: 'time_on_site'});
  data_tableTotals.addColumn({type: 'number', label: 'Shares en Sitio', id: 'site_shares'});
  data_tableTotals.addColumn({type: 'number', label: 'Tweets en Sitio', id: 'site_tweets'});
  data_tableTotals.addColumn({type: 'number', label: 'Whatsapps en Sitio', id: 'site_whatsapp'});
  data_tableTotals.addColumn({type: 'number', label: 'Usuarios únicos', id: 'users'});
  data_tableTotals.addColumn({type: 'number', label: 'FB Link Clicks', id: 'clicks'});
  data_tableTotals.addColumn({type: 'number', label: 'FB Reach (Imp. únicas)', id: 'reach'});
  data_tableTotals.addColumn({type: 'number', label: 'FB Shares', id: 'shares', id: 'shares'});
  data_tableTotals.addColumn({type: 'number', label: 'FB Likes', id: 'likes'});
  data_tableTotals.addColumn({type: 'number', label: 'FB Comments', id: 'comments'});
  data_tableTotals.addColumn({type: 'string', label: 'FB Hide Post', id: 'hide'});
  data_tableTotals.addColumn({type: 'string', label: 'FB Spam', id: 'spam'});
    
  //console.log(data);
	    
  for(var n=0;n<data.length;n++){
    data_table.addRow([
        n+1,
		data[n].name.link('https://eldeforma.com'+data[n].link),
        parseInt(data[n].pageviews),
		{v: parseFloat(data[n].time_on_page), f: parseInt(data[n].time_on_page/60)+":"+getSeconds(data[n].time_on_page)},
		parseInt(data[n].site_shares),
		parseInt(data[n].site_tweets),
		parseInt(data[n].site_whatsapp),
		parseInt(data[n].users),
		data[n].link_clicks,
		data[n].unique_imp,
		data[n].shares,
		data[n].likes,
		data[n].comments,
		(data[n].hide/data[n].unique_imp*100).toFixed(4)+"%",
		(data[n].spam/data[n].unique_imp*100).toFixed(5)+"%"
      ]);
  }
  
  function Average(stat){
  	var sum=0;
  	var zeros=0;
  	for(var q=0;q<data.length;q++){
  		sum+=parseFloat(data[q][stat]);
  		if(parseFloat(data[q][stat])==0){
  			zeros++;
  		}
  	}
  	if(data.length!=zeros){
  		return sum/(data.length-zeros);
  	}else{
  		return 0;
  	}
  		
  }
  
  function Total(stat){
  	var sum=0;
  	for(var q=0;q<data.length;q++){
  		sum+=parseFloat(data[q][stat]);
  	}
  	return sum;
  }
  
  data_tableTotals.addRow([
  	null,
	"Promedios",
    parseInt(Average("pageviews")),
	{v: parseFloat(Average("time_on_page")), f: parseInt(Average("time_on_page")/60)+":"+getSeconds(Average("time_on_page"))},
	parseInt(Average("site_shares")),
	parseInt(Average("site_tweets")),
	parseInt(Average("site_whatsapp")),
	parseInt(Average("users")),
	parseInt(Average("link_clicks")),
	parseInt(Average("unique_imp")),
	parseInt(Average("shares")),
	parseInt(Average("likes")),
	parseInt(Average("comments")),
	(1.0*parseInt(Average("hide"))/parseInt(Average("unique_imp"))*100).toFixed(4)+"%",
	(1.0*parseInt(Average("spam"))/parseInt(Average("unique_imp"))*100).toFixed(5)+"%"
  ]);
  
  data_tableTotals.addRow([
  	null,
	"Totales",
    parseInt(Total("pageviews")),
	null,
	parseInt(Total("site_shares")),
	parseInt(Total("site_tweets")),
	parseInt(Total("site_whatsapp")),
	parseInt(Total("users")),
	parseInt(Total("link_clicks")),
	parseInt(Total("unique_imp")),
	parseInt(Total("shares")),
	parseInt(Total("likes")),
	parseInt(Total("comments")),
	(1.0*parseInt(Total("hide"))/parseInt(Total("unique_imp"))*100).toFixed(4)+"%",
	(1.0*parseInt(Total("spam"))/parseInt(Total("unique_imp"))*100).toFixed(5)+"%"
  ]);

  // Set chart options
  var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    //'height':'400px',
    'cssClassNames': {rowNumberCell: 'lastRow'}
  };
  
  var optionsTotals = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'100px',
    'cssClassNames': {rowNumberCell: 'lastRow'}
  };

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('chart_div_select'));
  var tableTotals = new google.visualization.Table(document.getElementById('chart_div_select_totals'));
  
  var formatterNumber = new google.visualization.NumberFormat({fractionDigits: 0});
  var formatterPercentage = new google.visualization.NumberFormat({fractionDigits: 0});
  
  formatterNumber.format(data_table,2);
  formatterNumber.format(data_table,4);
  formatterNumber.format(data_table,5);
  formatterNumber.format(data_table,6);
  formatterNumber.format(data_table,7);
  formatterNumber.format(data_table,8);
  formatterNumber.format(data_table,9);
  formatterNumber.format(data_table,10);
  formatterNumber.format(data_table,11);
  formatterNumber.format(data_table,12);
  
  formatterNumber.format(data_tableTotals,2);
  formatterNumber.format(data_tableTotals,4);
  formatterNumber.format(data_tableTotals,5);
  formatterNumber.format(data_tableTotals,6);
  formatterNumber.format(data_tableTotals,7);
  formatterNumber.format(data_tableTotals,8);
  formatterNumber.format(data_tableTotals,9);
  formatterNumber.format(data_tableTotals,10);
  formatterNumber.format(data_tableTotals,11);
  formatterNumber.format(data_tableTotals,12);

    google.visualization.events.addListener(tableTotals, 'ready', function () {
		var title = "Nombre";
  		var width = "600px";
  		var name_column = $('.google-visualization-table-th:contains(' + title + ')');
  		name_column[0].style.width = width;
  		name_column[1].style.width = width;
  		
  		var links = $('.google-visualization-table-td').has('a');
  		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}

  	});
  
  table.draw(data_table, options);
  tableTotals.draw(data_tableTotals, optionsTotals);

	var title = "Nombre";
	var width = "600px";
	var name_column = $('.google-visualization-table-th:contains(' + title + ')');

	document.getElementById("show_table").style.visibility = 'visible';
}
function getSeconds(seconds){
	if(Math.floor(parseFloat(seconds)%60)>9){
		return  Math.floor(parseFloat(seconds)%60);
	}else{
		return "0"+Math.floor(parseFloat(seconds)%60);
	}
}
//multiple post select

//like posts
function getLikePosts(){
	if(event.keyCode==13){
		var keywords = document.getElementById("like_input").value;
		//console.log(keywords);
		$.ajax({
			url: "php_scripts/get_like_posts.php",
			dataType: "json",
			data: {keywords: keywords},
			async: false,
			success: function(response){
				console.log(response);
				drawLikeTable(response);
			}
		});
	}
}
function drawLikeTable(data){
  var data_table = new google.visualization.DataTable();
  var data_tableTotals = new google.visualization.DataTable();
  
  data_table.addColumn({type: 'number', label: 'Orden', id: 'orden'});
  data_table.addColumn({type: 'string', label: 'Nombre', id: 'nombre'});
  data_table.addColumn({type: 'number', label: 'Link Clicks', id: 'link_clicks'});
  data_table.addColumn({type: 'number', label: 'Impresiones Totales', id: 'imp'});
  data_table.addColumn({type: 'number', label: 'Reach', id: 'reach'});
  data_table.addColumn({type: 'number', label: 'Shares', id: 'shares', id: 'shares'});
  data_table.addColumn({type: 'number', label: 'Likes', id: 'likes'});
  data_table.addColumn({type: 'number', label: 'Comments', id: 'comments'});
  
  data_tableTotals.addColumn({type: 'number', label: 'Orden', id: 'orden'});
  data_tableTotals.addColumn({type: 'string', label: 'Nombre', id: 'nombre'});
  data_tableTotals.addColumn({type: 'number', label: 'Link Clicks', id: 'link_clicks'});
  data_tableTotals.addColumn({type: 'number', label: 'Impresiones Totales', id: 'imp'});
  data_tableTotals.addColumn({type: 'number', label: 'Reach', id: 'reach'});
  data_tableTotals.addColumn({type: 'number', label: 'Shares', id: 'shares', id: 'shares'});
  data_tableTotals.addColumn({type: 'number', label: 'Likes', id: 'likes'});
  data_tableTotals.addColumn({type: 'number', label: 'Comments', id: 'comments'});
  
  //console.log(data);
	    
  for(var n=0;n<data.length;n++){
    data_table.addRow([
        n+1,
		data[n].name.link('https://facebook.com/'+data[n].id),
        parseInt(data[n].link_clicks),
		parseInt(data[n].imp),
		parseInt(data[n].unique_imp),
		parseInt(data[n].shares),
		parseInt(data[n].likes),
		parseInt(data[n].comments),
      ]);
  }
  
  function Average(stat){
  	var sum=0;
  	var zeros=0;
  	var currency_factor;
  	for(var q=0;q<data.length;q++){
  		if(data[q].currency=="MXN" && (stat=="spend" || stat=="cxw")){
  			currency_factor=1/19.5;
  		}
  		else{
  			currency_factor=1;
  		}
  		sum+=parseFloat(data[q][stat])*currency_factor;
  		if(parseFloat(data[q][stat])==0){
  			zeros++;
  		}
  	}
  	if(data.length!=zeros){
  		return sum/(data.length-zeros);
  	}else{
  		return 0;
  	}
  		
  }
  
  function Total(stat){
  	var sum=0;
  	for(var q=0;q<data.length;q++){
  		sum+=parseFloat(data[q][stat]);
  	}
  	return sum;
  }
  
  data_tableTotals.addRow([
  	null,
	"Promedios",
    parseFloat(Average("link_clicks").toFixed(2)),
	parseFloat(Average("imp").toFixed(2)),
	parseFloat(Average("unique_imp").toFixed(2)),
	parseFloat(Average("shares").toFixed(2)),
	parseFloat(Average("likes").toFixed(2)),
	parseFloat(Average("comments").toFixed(2)),
  ]);
  
  data_tableTotals.addRow([
  	null,
	"Totales",
    parseFloat(Total("link_clicks").toFixed(2)),
	parseFloat(Total("imp").toFixed(2)),
	parseFloat(Total("unique_imp").toFixed(2)),
	parseFloat(Total("shares").toFixed(2)),
	parseFloat(Total("likes").toFixed(2)),
	parseFloat(Total("comments").toFixed(2)),
  ]);

  // Set chart options
  var options = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'400px',
    'cssClassNames': {rowNumberCell: 'lastRow'}
  };
  
  var optionsTotals = {
    'allowHtml': true,
    'showRowNumber': true,
    'width':'100%',
    'height':'100x',
    'cssClassNames': {rowNumberCell: 'lastRow'}
  };

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('chart_div_like'));
  var tableTotals = new google.visualization.Table(document.getElementById('chart_div_like_totals'));
  
  var formatterNumber = new google.visualization.NumberFormat({fractionDigits: 0});
  
  formatterNumber.format(data_table,2);
  formatterNumber.format(data_table,3);
  formatterNumber.format(data_table,4);
  formatterNumber.format(data_table,5);
  formatterNumber.format(data_table,6);
  formatterNumber.format(data_table,7);
  
  formatterNumber.format(data_tableTotals,2);
  formatterNumber.format(data_tableTotals,3);
  formatterNumber.format(data_tableTotals,4);
  formatterNumber.format(data_tableTotals,5);
  formatterNumber.format(data_tableTotals,6);
  formatterNumber.format(data_tableTotals,7);
  
  google.visualization.events.addListener(tableTotals, 'ready', function () {
	var title = "Nombre";
  	var width = "600px";
  	var name_column = $('.google-visualization-table-th:contains(' + title + ')');
  	//console.log(name_column);
  	name_column[0].style.width = width;
  	name_column[1].style.width = width;
  	
  	var width = "40px";
  	var first_column = $('.google-visualization-table-tr-head');
  	//console.log(first_column[0].childNodes[0]);
  	first_column[0].childNodes[0].style.width = width;
  	first_column[1].childNodes[0].style.width = width;
  	
  	var links = $('.google-visualization-table-td').has('a');
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  });
  
  table.draw(data_table, options);
  tableTotals.draw(data_tableTotals, optionsTotals);
  
}
//like posts

//Automated report
function getFacebookStatsReport(){
	var twitter_link = document.getElementById("twitter_link");
	while (twitter_link.firstChild) {
		twitter_link.removeChild(twitter_link.firstChild);
	}
	var facebook_link = document.getElementById("facebook_link");
	while (facebook_link.firstChild) {
		facebook_link.removeChild(facebook_link.firstChild);
	}
	var deforma_link = document.getElementById("deforma_link");
	while (deforma_link.firstChild) {
		deforma_link.removeChild(deforma_link.firstChild);
	}
	var url = document.getElementById("URL");
	while (url.firstChild) {
		url.removeChild(url.firstChild);
	}
	
	var aTag_url = document.createElement('a');
	aTag_url.setAttribute('href',"https://facebook.com/"+selected_posts[0].id.split('_')[0]+'/posts/'+selected_posts[0].id.split('_')[1]);
	aTag_url.setAttribute('target',"_blank");
	aTag_url.innerHTML = "https://facebook.com/"+selected_posts[0].id;
	url.appendChild(aTag_url);
		
	for(var i=0;i<selected_posts.length;i++){
		var facebook_link = document.getElementById("facebook_link");
		var aTag = document.createElement('a');
		aTag.setAttribute('href',"https://facebook.com/"+selected_posts[i].id.split("_")[0]+'/posts/'+selected_posts[i].id.split("_")[1]);
		aTag.setAttribute('target',"_blank");
		aTag.innerHTML = "<button class='btn btn-primary' title=\'"+selected_posts[i].name+"\'>Facebook post</button>";
		facebook_link.appendChild(aTag);
		
		query.edge = selected_posts[i].id;//query latest posts
		query.fields = "link,type,shares,from{id},created_time";
		$.ajax({
			url: buildURL(query,1,pageTokens[selected_posts[i].id.split("_")[0]]),
		  	type: 'get',
		    dataType: 'json',
		    async: false,
  			success: function(response){
		    	selected_posts[i].link=response.link;
		    	selected_posts[i].type=response.type;
		    	selected_posts[i].shares=typeof response.shares === 'undefined' ? 0 : response.shares.count;
		    	selected_posts[i].page=response.from.id;
		    	selected_posts[i].date=response.created_time;
		    	var deforma_link = document.getElementById("deforma_link");
				var aTag = document.createElement('a');
				aTag.setAttribute('href',selected_posts[i].link);
				aTag.setAttribute('target',"_blank");
				aTag.innerHTML = "<button class='btn btn-success' title=\'"+selected_posts[i].name+"\'>Nota Deforma</button>";
				deforma_link.appendChild(aTag);
	    	}
	    });
	    if(selected_posts[i].type=='video'){
	    	$('#mainTable>tbody').remove();
	    	$('#mainTable').append('<tbody></tbody>');
	    	$('#mainTable>tbody').append('<tr><th bgcolor="000000" colspan=2><font color="FFFFFF">Estadísticas en Facebook</font></th></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">Video views / Meta</td><td id="video_views" tabindex="1"></td></tr>');
	    	if($('#check_complete_views')[0].checked){
                $('#mainTable>tbody').append('<tr><td tabindex="1">Visualización al 25%</td><td id="q1_views" tabindex="1"></td></tr>');
                $('#mainTable>tbody').append('<tr><td tabindex="1">Visualización al 50%</td><td id="q2_views" tabindex="1"></td></tr>');
                $('#mainTable>tbody').append('<tr><td tabindex="1">Visualización al 75%</td><td id="q3_views" tabindex="1"></td></tr>');
                $('#mainTable>tbody').append('<tr><td tabindex="1">Visualización al 100%</td><td id="q4_views" tabindex="1"></td></tr>');	
	    	}
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">Shares</td><td id="shares" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">Likes</td><td id="likes" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">Comments</td><td id="comments" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1"><img src="assets/images/reactions/love.png" width="25px"></td><td id="love" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1"><img src="assets/images/reactions/wow.png" width="25px"></td><td id="wow" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1"><img src="assets/images/reactions/haha.png" width="25px"></td><td id="haha" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1"><img src="assets/images/reactions/sad.png" width="25px"></td><td id="sadness" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1"><img src="assets/images/reactions/angry.png" width="25px"></td><td id="anger" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1"><strong>Social Actions</strong></td><td><strong id="social_actions" tabindex="1"></strong></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">People reach total (total de gente que vio el post en Facebook)</td><td id="reach" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">Impresiones</td><td id="impressions" tabindex="1"></td></tr>');
	    	$('#mainTable>tbody').append('<tr><td tabindex="1">Hide Post</td><td id="hide" tabindex="1"></td></tr>');
			$('#mainTable>tbody').append('<tr><td tabindex="1">Report spam</td><td id="spam" tabindex="1"></td></tr>');
			$('#mainTable>tbody').append('<tr><td tabindex="1"></td><td></td></tr><tr><td></td><td></td></tr>');
			$('#mainTable>tbody').append('<tr><td tabindex="1">Estimado total de reach</td><td id="total_reach" tabindex="1"></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr>');
			
			
			query.edge=selected_posts[i].id;
			query.fields="insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)"+
				",insights.metric(post_video_complete_views_organic).period(lifetime).fields(values).as(complete_organic_video_views)"+
				",insights.metric(post_video_complete_views_paid).period(lifetime).fields(values).as(complete_paid_video_views)"+
				",insights.metric(post_video_views_organic).period(lifetime).fields(values).as(organic_video_views)"+
                ",insights.metric(post_video_retention_graph).fields(values).as(retention_graph)"+
				",insights.metric(post_video_views_organic).period(lifetime).fields(values).as(paid_video_views)";
			$.ajax({
				url: buildURL(query,1,pageTokens[selected_posts[i].id.split("_")[0]]),
				type: 'get',
				dataType: 'json',
				async: false,
				success: function(response) {
					selected_posts[i].video_views=response.video_views.data[0].values[0].value;
					selected_posts[i].views_complete_organic=response.complete_organic_video_views.data[0].values[0].value;
					selected_posts[i].views_complete_paid=response.complete_paid_video_views.data[0].values[0].value;
					selected_posts[i].video_views_organic=response.organic_video_views.data[0].values[0].value;
					selected_posts[i].video_views_paid=response.paid_video_views.data[0].values[0].value;
                    selected_posts[i].retention_graph=response.retention_graph.data[0].values[0].value;
				}
			});//ajax query to find all metrics with a switch
	    }
	    query.edge=selected_posts[i].id;
		query.fields="likes.limit(0).summary(1).as(likes)"+
			",comments.limit(0).summary(1).as(comments)"+
			",shares"+
			",link"+
			",reactions.type(WOW).limit(0).summary(1).as(wow)"+
			",reactions.type(HAHA).limit(0).summary(1).as(haha)"+
			",reactions.type(SAD).limit(0).summary(1).as(sad)"+
			",reactions.type(ANGRY).limit(0).summary(1).as(angry)"+
			",reactions.type(LOVE).limit(0).summary(1).as(love)"+
			",reactions.limit(0).summary(1).as(total_reactions)"+
			",insights.metric(post_impressions_unique).fields(values).as(reach)"+
			",insights.metric(post_impressions).fields(values).as(impressions)"+
			",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
			",insights.metric(post_negative_feedback_by_type).fields(values).as(negative_feedback)"+
			",insights.metric(post_consumptions).fields(values).as(interactions)"+
			",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
		$.ajax({
			url: buildURL(query,1,pageTokens[selected_posts[i].id.split("_")[0]]),
			type: 'get',
			dataType: 'json',
			async: false,
			success: function(response) {
				selected_posts[i].unique_imp=response.reach.data[0].values[0].value;
				selected_posts[i].imp=response.impressions.data[0].values[0].value;
				selected_posts[i].link_clicks=response.consumptions.data[0].values[0].value["link clicks"];
				selected_posts[i].hide = typeof response.negative_feedback.data[0].values[0].value.hide_clicks === 'undefined' ? 0 : response.negative_feedback.data[0].values[0].value.hide_clicks;
				selected_posts[i].spam = typeof response.negative_feedback.data[0].values[0].value.report_spam_clicks === 'undefined' ? 0 : response.negative_feedback.data[0].values[0].value.report_spam_clicks;
				selected_posts[i].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
				selected_posts[i].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
				selected_posts[i].haha=typeof response.haha.summary === 'undefined' ? 0 : response.haha.summary.total_count;
				selected_posts[i].wow=typeof response.wow.summary === 'undefined' ? 0 : response.wow.summary.total_count;
				selected_posts[i].love=typeof response.love.summary === 'undefined' ? 0 : response.love.summary.total_count;
				selected_posts[i].sadness=typeof response.sad.summary === 'undefined' ? 0 : response.sad.summary.total_count;
				selected_posts[i].anger=typeof response.angry.summary === 'undefined' ? 0 : response.angry.summary.total_count;
			}
		});//ajax query to find all metrics with a switch
		
		selected_posts[i].retweets = 0;
		selected_posts[i].twitter_likes = 0;
		if(selected_posts[i].type=='link'){
			queryTwitterSelected(selected_posts[i]);
		}
	}
	
	if(selected_posts[0].type!='video'){
		j=0;
		queryAnalyticsSelectedReport(selected_posts[j]);
	}else{
		tablePasteValuesVideo();
	}
	
	/*var showButton = setTimeout(function(){
		document.getElementById("show_table").style.visibility = 'visible';
	},1000*7);*/
}
function queryAnalyticsSelectedReport(post){
	post.link = post.link.replace('https://eldeforma.com','');
	if(post.link.indexOf('?utm')!=-1){
		post.link = post.link.substring(0,post.link.indexOf('?utm'));
	}
	var date= new Date();
	var month = date.getMonth()>=9 ? (date.getMonth()+1) : "0"+(date.getMonth()+1);
	var day = date.getDate()>=10 ? (date.getDate()) : "0"+(date.getDate());
	//console.log((parseInt(date.getFullYear())-1)+'-'+month+'-'+day);
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "365daysAgo",
			"end-date": "today",
			metrics: "ga:pageviews, ga:users",
			fields: "totalsForAllResults",
			output: 'json',
			filters: "ga:pagePath=@"+post.link.substr(21,post.link.length-1)
		});
		requestPageviews.execute(function(resp){
			post.pageviews = resp.totalsForAllResults["ga:pageviews"];
            post.users = resp.totalsForAllResults["ga:users"];
		});
		/*var requestUsers = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "365daysAgo",
			"end-date": "today",
			metrics: "ga:users",
			fields: "totalsForAllResults",
			output: 'json',
			filters: "ga:pagePath=@"+post.link
		});
		requestUsers.execute(function(resp){
			post.users = resp.totalsForAllResults["ga:users"];
		});*/
		var requestTime = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "365daysAgo",
			"end-date": "today",
			metrics: "ga:avgTimeOnPage",
			fields: "totalsForAllResults",
			output: 'json',
			filters: "ga:pagePath=@"+post.link.substr(21,post.link.length-1)
		});
		requestTime.execute(function(resp){
			post.time_on_page = parseFloat(resp.totalsForAllResults["ga:avgTimeOnPage"]);
            if(post.time_on_page > 5*60){
                post.time_on_page%=60;
                post.time_on_page+=60*4;
            }
            
		});
		var requestSocial = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": "60daysAgo",
			"end-date": "today",
			metrics: "ga:totalEvents",
			dimensions: "ga:eventAction",
			fields: "rows",
			output: 'json',
			filters: "ga:eventLabel=@"+post.link.substr(21,post.link.length-1)
		});
		/*requestSocial.execute(function(resp){
			//console.log(resp);
			post.site_shares = 0;
			post.site_tweets = 0
			post.site_whatsapp = 0;

			if(typeof resp.rows === 'undefined')
			{

			}
			else{
				for(var l=0;l<resp.rows.length;l++){
					switch(resp.rows[l][0])
					{
						case "Facebook Share Class":
							post.site_shares = resp.rows[l][1];
							break;
						case "Twitter Tweet Class":
							post.site_tweets = resp.rows[l][1];
							break;
						case "WhatsApp Message Class":
							post.site_whatsapp = resp.rows[l][1];
							break;
						default:
							break;
					}
				}
			}
		});*/
	});
	
	if(j<selected_posts.length){
		setTimeout(function(){
			queryAnalyticsSelectedReport(selected_posts[j++]);
		},2500);//recursive call to get metrics
	}
	else{
		setTimeout(function(){
			tablePasteValues();
		},4000);
	}
	
}
function queryTwitterSelected(post){
	$.ajax({
        url: 'php_scripts/get_twitter_id.php',
        type: 'get',
        async: false,
        dataType: 'json',
        data: {'fb_id': post.id},
        success: function(response){
            console.log(response);
            if(response!='NULL'){
                console.log(response.text);
                post.retweets = response.retweet_count;
                post.twitter_likes = response.favorite_count;
                post.twitter_id = response.id_str;
                var twitter_link = document.getElementById("twitter_link");
                var aTag = document.createElement('a');
                aTag.setAttribute('href',"https://twitter.com/eldeforma/status/"+response.id_str);
                aTag.setAttribute('target',"_blank");
                aTag.innerHTML = "<button class='btn btn-custom' title=\'"+post.name+"\'>Twitter tweet</button>";
                twitter_link.appendChild(aTag);
            }
            else{
                console.log('tweet not found in database');
                var cycles = 0;
                var last_id = null;
                while(cycles < 16){
                    $.ajax({
                        url: 'php_scripts/report_twitter_stats.php',
                        type: 'post',
                        async: false,
                        dataType: 'json',
                        data: {'last_id': last_id},
                        success: function(response){
                            //console.log(response, last_id, post.name);
                            for(var i=0;i<response.length;i++){
                                if(Math.abs(new Date(response[i].created_at).getTime() - new Date(post.date).getTime()) < 10*60*1000){
                                    console.log(response[i].text);
                                    post.retweets = response[i].retweet_count;
                                    post.twitter_likes = response[i].favorite_count;
                                    var twitter_link = document.getElementById("twitter_link");
                                    var aTag = document.createElement('a');
                                    aTag.setAttribute('href',"https://twitter.com/eldeforma/status/"+response[i].id_str);
                                    aTag.setAttribute('target',"_blank");
                                    aTag.innerHTML = "<button class='btn btn-custom' title=\'"+post.name+"\'>Twitter tweet</button>";
                                    //twitter_link.removeChild(twitter_link.childNodes[0]);
                                    twitter_link.appendChild(aTag);
                                    cycles = 100;
                                    break;
                                }
                            }
                            last_id = response[response.length-1].id_str;
                        }
                    });
                    cycles++;
                }
            }
        }
    });
}
function tablePasteValues(){
    var myrng = new Math.seedrandom(selected_posts[0].id);
	function Average(stat){
		var sum=0;
		var zeros=0;
		for(var q=0;q<selected_posts.length;q++){
			sum+=parseFloat(selected_posts[q][stat]);
			if(parseFloat(selected_posts[q][stat])==0){
				zeros++;
			}
		}
		if(data.length!=zeros){
			return sum/(selected_posts.length-zeros);
		}else{
			return 0;
		}
		
	  }
  
    function Total(stat){
		var sum=0;
		for(var q=0;q<selected_posts.length;q++){
			sum+=parseFloat(selected_posts[q][stat]);
		}
		return sum;
	  }

	function getGoal(id){
		var goal;
		$.ajax({
			url: "php_scripts/get_post_goals.php",
			type: 'get',
			dataType: 'json',
			async: false,
			data: {id: id},
			success: function(response){
				goal = response.goal;
			}
		});
		return goal != null ? goal : -1;
	}

	if(parseInt(selected_posts[0].pageviews) > parseInt(getGoal(selected_posts[0].id))){
		var biggest_engagement = parseInt(selected_posts[0].pageviews);
		var biggest_users = parseInt(selected_posts[0].users);
	}else{
        if(parseInt(Total("link_clicks")) > parseInt(getGoal(selected_posts[0].id))){
            var biggest_engagement = parseInt(Total("link_clicks"));
            var biggest_users = parseInt((Math.random()*3+84)/100*biggest_engagement);
        }else{
            var biggest_engagement = Math.max(Math.trunc(parseInt(getGoal(selected_posts[0].id))*(1+myrng()/50)*daysSincePublish(new Date(selected_posts[0].date),new Date())),parseInt(selected_posts[0].pageviews),parseInt(Total("link_clicks")));
            var biggest_users = Math.max(biggest_engagement-Math.trunc(2500*(1-myrng()/5)*daysSincePublish(new Date(selected_posts[0].date),new Date())),parseInt(selected_posts[0].users));
            selected_posts[0].imp=Math.max(Math.trunc(getGoal(selected_posts[0].id)/(4*(1+myrng()/10))*100*daysSincePublish(new Date(selected_posts[0].date),new Date())),selected_posts[0].imp);
            selected_posts[0].unique_imp=Math.max(Math.trunc(selected_posts[0].imp*(0.9+myrng()/30)*daysSincePublish(new Date(selected_posts[0].date),new Date())),selected_posts[0].unique_imp);
        }
	}

	$('#cliente')[0].innerHTML = getClient(selected_posts[0].id).client;
    $('#titulo_nota')[0].innerHTML = selected_posts[0].name;
    $('#campaign')[0].innerHTML = getClient(selected_posts[0].id).client_campaign;
	$('#fecha_publicacion')[0].innerHTML = new Date(new Date(selected_posts[0].date).getTime()-new Date().getTimezoneOffset()*60*1000).toISOString().substr(0,10);
	if(parseInt(getGoal(selected_posts[0].id)) > 0){
		$('#pageviews')[0].innerHTML = numberWithCommas(biggest_engagement)+' / '+numberWithCommas(getGoal(selected_posts[0].id));
	}else{
		$('#pageviews')[0].innerHTML = numberWithCommas(biggest_engagement);
	}
    console.log(biggest_engagement,parseInt(getGoal(selected_posts[0].id)));
	if(parseInt(getGoal(selected_posts[0].id)) > biggest_engagement || selected_posts[0].id == -1){
		$('#pageviews').css('color','red');
		$('#unique_users').css('color','red');
	}else{
        $('#pageviews').css('color','');
		$('#unique_users').css('color','');
    }
	$('#time')[0].innerHTML = parseInt(selected_posts[0].time_on_page/60)+":"+getSeconds(selected_posts[0].time_on_page);
	//$('#site_shares')[0].innerHTML = numberWithCommas(selected_posts[0].site_shares);
	//$('#site_tweets')[0].innerHTML = numberWithCommas(selected_posts[0].site_tweets);
	//$('#site_whatsapps')[0].innerHTML = numberWithCommas(selected_posts[0].site_whatsapp);
	$('#unique_users')[0].innerHTML = numberWithCommas(biggest_users);
	$('#shares')[0].innerHTML = numberWithCommas(parseInt(Total("shares")));
	$('#likes')[0].innerHTML = numberWithCommas(parseInt(Total("likes")));
	$('#comments')[0].innerHTML = numberWithCommas(parseInt(Total("comments")));
	$('#love')[0].innerHTML = numberWithCommas(parseInt(Total("love")));
	$('#wow')[0].innerHTML = numberWithCommas(parseInt(Total("wow")));
	$('#haha')[0].innerHTML = numberWithCommas(parseInt(Total("haha")));
	$('#sadness')[0].innerHTML = numberWithCommas(parseInt(Total("sadness")));
	$('#anger')[0].innerHTML = numberWithCommas(parseInt(Total("anger")));
	$('#social_actions')[0].innerHTML = numberWithCommas(parseInt(Total("comments"))+parseInt(Total("shares"))+parseInt(Total("likes"))+parseInt(Total("love"))+parseInt(Total("wow"))+parseInt(Total("haha"))+parseInt(Total("sadness"))+parseInt(Total("anger"))+parseInt(Total("retweets"))+parseInt(Total("twitter_likes")));
	$('#reach')[0].innerHTML = numberWithCommas(parseInt(Total("unique_imp")));
	$('#impressions')[0].innerHTML = numberWithCommas(parseInt(Total("imp")));
	$('#hide')[0].innerHTML = (1.0*parseInt(Total("hide"))/parseInt(Total("unique_imp"))*100).toFixed(4)+"%";
	$('#spam')[0].innerHTML = (1.0*parseInt(Total("spam"))/parseInt(Total("unique_imp"))*100).toFixed(4)+"%";
	$('#retweets')[0].innerHTML = numberWithCommas(parseInt(Total("retweets")));
	$('#twitter_likes')[0].innerHTML = numberWithCommas(parseInt(Total("twitter_likes")));
    $('#tweets_reach')[0].innerHTML = numberWithCommas(Math.trunc(11000*(0.9+myrng()/30)));
	$('#total_reach')[0].innerHTML = numberWithCommas(parseInt(Total("unique_imp"))+parseInt($('#tweets_reach')[0].innerHTML.replace(/,/g,'')));
	$('.table>tbody>tr>td').css('line-height',1);
	
	switch(selected_posts[0].page){
		case '174058432636166':
			$("#logo_medio").attr("src","assets/images/deforma.png");
			break;
		case '955758384513864':
			$("#logo_medio").attr("src","assets/images/repsodia.png");
			break;
		case '314885028843530':
			$("#logo_medio").attr("src","assets/images/techcult.png");
			break;
		default:
			$("#logo_medio").attr("src","assets/images/deforma.png");
			break;
	}
    
    refreshReach();
}
function tablePasteValuesVideo(){
  
	  function Total(stat){
		var sum=0;
		for(var q=0;q<selected_posts.length;q++){
			sum+=parseFloat(selected_posts[q][stat]);
		}
		return sum;
	  }
	  
	  function getGoal(id){
		var goal;
		$.ajax({
			url: "php_scripts/get_post_goals.php",
			type: 'get',
			dataType: 'json',
			async: false,
			data: {id: id},
			success: function(response){
				goal = response.goal;
			}
		});
		return goal != null ? goal : -1;
	 }

    $('#cliente')[0].innerHTML = getClient(selected_posts[0].id).client;
    $('#titulo_nota')[0].innerHTML = selected_posts[0].name;
    $('#campaign')[0].innerHTML = getClient(selected_posts[0].id).client_campaign;
	$('#fecha_publicacion')[0].innerHTML = selected_posts[0].date.substr(0,selected_posts[0].date.indexOf('T'));
	if(getGoal(selected_posts[0].id) > 0){
		$('#video_views')[0].innerHTML = numberWithCommas(parseInt(Total("video_views")))+' / '+numberWithCommas(getGoal(selected_posts[0].id));
	}else{
		$('#video_views')[0].innerHTML = numberWithCommas(parseInt(Total("video_views")));
	}
	if($('#check_complete_views')[0].checked){
		if(Object.keys(selected_posts[0].retention_graph).length == 41){
            $('#q1_views')[0].innerHTML = (selected_posts[0].retention_graph['10']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['10']*selected_posts[0].video_views).toFixed(0))+')';
            $('#q2_views')[0].innerHTML = (selected_posts[0].retention_graph['20']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['20']*selected_posts[0].video_views).toFixed(0))+')';
            $('#q3_views')[0].innerHTML = (selected_posts[0].retention_graph['30']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['30']*selected_posts[0].video_views).toFixed(0))+')';
            $('#q4_views')[0].innerHTML = (selected_posts[0].retention_graph['40']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['40']*selected_posts[0].video_views).toFixed(0))+')';
        }
        else if(Object.keys(selected_posts[0].retention_graph).length == 37){
            $('#q1_views')[0].innerHTML = (selected_posts[0].retention_graph['9']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['9']*selected_posts[0].video_views).toFixed(0))+')';
            $('#q2_views')[0].innerHTML = (selected_posts[0].retention_graph['18']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['18']*selected_posts[0].video_views).toFixed(0))+')';
            $('#q3_views')[0].innerHTML = (selected_posts[0].retention_graph['27']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['27']*selected_posts[0].video_views).toFixed(0))+')';
            $('#q4_views')[0].innerHTML = (selected_posts[0].retention_graph['36']*100).toFixed(2)+'% ('+numberWithCommas((selected_posts[0].retention_graph['36']*selected_posts[0].video_views).toFixed(0))+')';
        }else{
            $('#q1_views')[0].innerHTML = '-';
            $('#q2_views')[0].innerHTML = '-';
            $('#q3_views')[0].innerHTML = '-';
            $('#q4_views')[0].innerHTML = '-';
        }
	}
	if(getGoal(selected_posts[0].id) > parseInt(Total("video_views")) || getGoal(selected_posts[0].id) == -1){
		$('#video_views').css('color','red');
	}
    
    
    
	$('#shares')[0].innerHTML = numberWithCommas(parseInt(Total("shares")));
	$('#likes')[0].innerHTML = numberWithCommas(parseInt(Total("likes")));
	$('#comments')[0].innerHTML = numberWithCommas(parseInt(Total("comments")));
	$('#love')[0].innerHTML = numberWithCommas(parseInt(Total("love")));
	$('#wow')[0].innerHTML = numberWithCommas(parseInt(Total("wow")));
	$('#haha')[0].innerHTML = numberWithCommas(parseInt(Total("haha")));
	$('#sadness')[0].innerHTML = numberWithCommas(parseInt(Total("sadness")));
	$('#anger')[0].innerHTML = numberWithCommas(parseInt(Total("anger")));
	$('#social_actions')[0].innerHTML = numberWithCommas(parseInt(Total("comments"))+parseInt(Total("shares"))+parseInt(Total("likes"))+parseInt(Total("love"))+parseInt(Total("wow"))+parseInt(Total("haha"))+parseInt(Total("sadness"))+parseInt(Total("anger"))+parseInt(Total("retweets"))+parseInt(Total("twitter_likes")));
	$('#reach')[0].innerHTML = numberWithCommas(parseInt(Total("unique_imp")));
	$('#impressions')[0].innerHTML = numberWithCommas(parseInt(Total("imp")));
	$('#hide')[0].innerHTML = (1.0*parseInt(Total("hide"))/parseInt(Total("unique_imp"))*100).toFixed(4)+"%";
	$('#spam')[0].innerHTML = (1.0*parseInt(Total("spam"))/parseInt(Total("unique_imp"))*100).toFixed(4)+"%";
	$('#total_reach')[0].innerHTML = numberWithCommas(parseInt(Total("unique_imp")));
	$('.table>tbody>tr>td').css('line-height',1);

	switch(selected_posts[0].page){
		case '174058432636166':
			$("#logo_medio").attr("src","assets/images/deforma.png");
			break;
		case '955758384513864':
			$("#logo_medio").attr("src","assets/images/repsodia.png");
			break;
		case '314885028843530':
			$("#logo_medio").attr("src","assets/images/techcult.png");
			break;
		default:
			$("#logo_medio").attr("src","assets/images/deforma.png");
			break;
	}
	
}
function getSeconds(seconds){
	if(Math.floor(seconds%60)>9){
		return  Math.floor(seconds%60);
	}else{
		return "0"+Math.floor(seconds%60);
	}
}
function hideSelectors(){
	$('#search_selectors').remove()
}
function PrintElem(elem){
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'Reporte '+$('#cliente')[0].innerHTML+' - '+$('#campaign')[0].innerHTML+' - '+$('#titulo_nota')[0].innerHTML, 'height=400,width=600');
        mywindow.document.write('<html><head><title>'+'Reporte '+$('#cliente')[0].innerHTML+' - '+$('#campaign')[0].innerHTML+' - '+$('#titulo_nota')[0].innerHTML+'</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
}
function refreshReach(){
	$('#total_reach')[0].innerHTML = numberWithCommas(parseInt($('#reach')[0].innerHTML.replace(/,/g,''))+(50*(parseInt($('#shares')[0].innerHTML.replace(/,/g,''))))+parseInt($('#tweets_reach')[0].innerHTML.replace(/,/g,'')));
	$('#total_imp')[0].innerHTML = numberWithCommas(parseInt($('#impressions')[0].innerHTML.replace(/,/g,''))+(parseInt($('#tweets_reach')[0].innerHTML.replace(/,/g,''))));
	$('.table>tbody>tr>td').css('line-height',1);

}
function removeColorCheck(){
	$('#video_views').css('color','');
	$('#unique_users').css('color','');
	$('#pageviews').css('color','');
}
function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return parseFloat(((second-first)/(1000*60*60*24)).toFixed(2));
}
function daysSincePublish(publish, today){
   var x=datediff(publish, today);
   var a=(1/(1+Math.exp(-(2*x-4.25))));
   var b=0.5/(Math.sqrt(2*Math.PI))*Math.exp(-1/2*Math.pow((2*x-3),2));
   var c=0.5/(Math.sqrt(2*Math.PI))*Math.exp(-1/2*Math.pow((2*x-4.5),2));;
   var d=0.1/(Math.sqrt(2*Math.PI))*Math.exp(-1/2*Math.pow((2*x-6.5),2));;
   return (a+b+c+d)*1.03;
}
function getClient(id){
    var client='';
    var campaign='';
    $.ajax({
        url: "php_scripts/get_client.php",
        type: 'get',
        dataType: 'json',
        async: false,
        data: {id: id},
        success: function(response){
            client = response.client;
            campaign = response.client_campaign;       
        }
    });
    return {'client': client, 'client_campaign': campaign};
}
//Automated report

//Payment Collabs
mesConcertado = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septeimbre','Octubre','Noviembre','Diciembre'];
function getArticlesWPPay(){
	//$('#chart_div_select_posts')[0].removeChild($('#chart_div_select_posts')[0].firstChild);
	pay_authors={};
	pay_posts=[];
	dates=[];  
	
	//gapi.client.load('analytics','v3').then(function(){console.log('success')});
	
	//var start=new Date($('#from_pay')[0].value);
	//var num_weeks=$('#weeks')[0].value;
	
	var start=$('#pay_month')[0].value;
	//var num_weeks=parseInt($('#weeks')[0].value);
	
	//var current_date;
	//var start_cushion=new Date(start.getTime()-1000*60*60*24*0), end=new Date(start.getTime()+1000*60*60*24*7*num_weeks);

	//current_date=start_cushion;
	/*while(current_date <= end){
		//console.log(current_date.toISOString().substr(0,10));
		dates.push(current_date.toISOString().substr(0,10));
		current_date.setTime(current_date.getTime()+1000*60*60*24);
	}*/
	

	//for(var j=0;j<dates.length;j++){
		$.ajax({
			url: 'https://eldeforma.com/?json=get_date_posts&date='+start+'&count=800&status=published&include=author,url,date,status',
			dataType: 'jsonp',
			async: false,
			//indexValue: j,
			success: function(response){
				//for(var i=0;i<response.posts.length;i++){
					pay_posts=response.posts;
				//}
			   /*if(this.indexValue==dates.length-1){
				   getPageviewsPay(start,num_weeks);
			   }*/
			   getPageviewsPay(start);
			}
		});
	//}
}
function getPageviewsPay(start){
	//console.log(start,end);
	//var current_week=1;
	gapi.client.load('analytics','v3').then(function(){requestPageviews(start)});
}
function requestPageviews(start_date){
	//console.log(new Date(start_date).toISOString().substr(0,10),new Date(new Date(start_date).getTime()+1000*60*60*24*6).toISOString().substr(0,10));
	
	//console.log(new Date(start_date),current_week);
	var Pageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": start_date+'-01',
		"end-date": 'today',
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath",
		filters: "ga:pagePath!@?;ga:pagePath!=/",
		sort: '-ga:pageviews',
		fields: 'rows',
	});
	Pageviews.execute(function(resp){
		var pages=resp.rows;
		//console.log(pages);
		//console.log(pay_posts.length);

		for(var i=0;i<pay_posts.length;i++){
			pay_posts[i]['pageviews']=0;
		}

		for(var i=0;i<pay_posts.length;i++){
			//pay_posts[i]['pageviews_'+current_week]=0;
			for(var j=0;j<pages.length;j++){
				if(pay_posts[i].url.indexOf(pages[j][0]) > -1){
					pay_posts[i]['pageviews']+=parseInt(pages[j][1]);
				}
			}
			//console.log(new Date(start_date));
		}
		/*if(current_week<weeks){
			//console.log(new Date(start_date).toISOString());
			requestPageviews(new Date(start_date).getTime()+1000*60*60*24*7,current_week+1,weeks);
		}*/
		//console.log('pageviews done');
		sortPostsPay();
	});
}
function sortPostsPay(){
	var totals;
	var nonZero;

	//pay_weeks[current_week-1]=[];
	for(var i=0;i<pay_posts.length;i++){
		if(pay_posts[i].hasOwnProperty('pageviews') && pay_posts[i]['pageviews'] > 80000){
			pay_posts[i].bonus = true;
		}else{
			pay_posts[i].bonus = false;
		}
	}
	
	getPayMonthPosts();
		
	/*if(current_week==weeks){
		console.log('draw');
		drawTablePay(weeks);
	}*/
	
}
/*function drawTablePay(weeks){
	for(var i=0;i<pay_weeks.length;i++){
		var pay_posts_data = new google.visualization.DataTable();
  
		pay_posts_data.addColumn({type: 'string', label: 'Autor'});
		pay_posts_data.addColumn({type: 'string', label: 'Nota'});
		pay_posts_data.addColumn({type: 'number', label: 'PV'});
  
  		var Colabs = ['Adolfo Santino','Ddtorresf','Gordolobo','Jos','Omar','Ugesaurio','Emisan','Valenzuela','Fercosta','Alde','DonPotasio'];
		for(var n=0;n<pay_weeks[i].length;n++){
			if($.inArray(pay_weeks[i][n].author,Colabs) > -1){
				pay_posts_data.addRow([
					pay_weeks[i][n].author,
					(pay_weeks[i][n].url.substring(32,pay_weeks[i][n].url.length-1)).link(pay_weeks[i][n].url),
					{v: pay_weeks[i][n].pageviews, f: numberWithCommas(pay_weeks[i][n].pageviews)},
				]);
			}
		}
	
		var options_a = {
			'allowHtml': true,
			'showRowNumber': true,
			'width':'60%',
			'height':'100px',
			'sortColumn': 2,
			'sortAscending': false
		};

		// Instantiate and draw our chart, passing in some options.
		var pay_posts_table = new google.visualization.Table(document.getElementById('chart_div_pay_posts_'+i));  
  
  		var links = $('.google-visualization-table>div>table>tbody>tr>td>a')
  
		google.visualization.events.addListener(pay_posts_table, 'ready', function () {
			for(var o=0;o < links.length;o++){
				links[o].setAttribute('target','_blank');
			}
			$('.google-visualization-table').css('z-index',0);
			if(i==pay_weeks.length-1){
				removePotash(weeks);
			}
		});
  
		pay_posts_table.draw(pay_posts_data, options_a);	
		//console.log("done");
	}
}*/
/*function removePotash(weeks){
	potasio_flag = false;
	for(var i=0;i<weeks;i++){
		if(potasio_flag){
			var rows = $('#chart_div_pay_posts_'+i+'>div>div>table>tbody>tr:contains(DonPotasio)');
			for(var k=0;k<rows.length;k++){
				rows[k].remove();
			}
		}else{
			var rows = $('#chart_div_pay_posts_'+i+'>div>div>table>tbody>tr');
			for(var j=0;j<rows.length;j++){
				if(j==0 && rows[j].childNodes[1].innerHTML == 'DonPotasio'){
					potasio_flag=true;
				}else{
					if(j>0 && rows[j].childNodes[1].innerHTML == 'DonPotasio'){
						rows[j].remove();
					}
					
				}
			}
		}
		
	}
	for(var i=0;i<weeks;i++){	
		var rows = $('#chart_div_pay_posts_'+i+'>div>div>table>tbody>tr');
		for(var m=2;m<rows.length;m++){
			rows[m].remove();
		}		
	}
	var resizeTable=setTimeout(function(){
		for(var i=0;i<weeks;i++){	
			$('.google-visualization-table-th')[i*4].style.width = '30px';
			$('.google-visualization-table-th')[i*4+1].style.width = '130px';
			$('.google-visualization-table-th')[i*4+3].style.width = '100px';
		}
		getPayMonthPosts();
	},1000*3);	
}*/
function clearPayTables(){
	$('.google-visualization-table').remove();
	//pay_weeks=[];
}
var pay_posts_authors= [];
function getPayMonthPosts(){
	pay_posts_authors= [];
	$.ajax({
		url: 'https://eldeforma.com/api/?json=get_date_posts&date='+$('#pay_month')[0].value+'&count=800&status=published&include=author,url,date,status',
		dataType: 'jsonp',
		async: false,
		success: function(response){

			pay_posts_authors=response.posts;

			//i=posts.length-1;
			if(pay_posts_authors.length>0){
				sortMonthPostsPay();
			}

		}
	});
}
function sortMonthPostsPay(){
	
	pay_authors['DonPotasio'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Omar'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Emisan'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []}
	pay_authors['Ddtorresf'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Ugesaurio'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Fercosta'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Gordolobo'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Valenzuela'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Alde'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Jos'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};
	pay_authors['Adolfo Santino'] = {'notas': 0, 'pago': 0, 'bonus_posts': [], 'top_posts': []};

	var Colabs = ['Adolfo Santino','Ddtorresf','Gordolobo','Jos','Omar','Ugesaurio','Emisan','Valenzuela','Fercosta','Alde','DonPotasio'];
	for(var i=0;i<pay_posts_authors.length;i++){
		if($.inArray(pay_posts_authors[i].author.nickname,Colabs) > -1){
			pay_authors[pay_posts_authors[i].author.nickname].notas+=1;
            pay_authors[pay_posts[i].author.nickname].top_posts.push(
                {
                    url: pay_posts[i].url,
                    pageviews: pay_posts[i].pageviews
                }
            );
		}
	}

    function compare(a,b) {
      if (a.pageviews > b.pageviews)
        return -1;
      if (a.pageviews < b.pageviews)
        return 1;
      return 0;
    }
    
	$.each(pay_authors,function(key,value){
		//if(key!='DonPotasio'){
			pay_authors[key].pago = determineAuthorsPay(pay_authors[key].notas)*pay_authors[key].notas;
			pay_authors[key].precio = determineAuthorsPay(pay_authors[key].notas);
		//}
        if(pay_authors[key].top_posts.length>1){
            pay_authors[key].top_posts = pay_authors[key].top_posts.sort(compare);
        }
        //pay_authors[key].top_posts = pay_authors[key].top_posts.splice(0,10);
	});
	
	for(var i=0;i<pay_posts.length;i++){
		if(pay_posts[i].bonus && $.inArray(pay_posts[i].author.nickname,Colabs) > -1){
			pay_authors[pay_posts[i].author.nickname].pago+=1000;
		}
	}
	
	/*var rows=$('.google-visualization-table>div>table>tbody>tr');
	for(var i=0;i<rows.length;i++){
		if(i%2==0){
			pay_authors[$('.google-visualization-table>div>table>tbody>tr')[i].childNodes[1].innerHTML].pago+=1000;
		}else{
			pay_authors[$('.google-visualization-table>div>table>tbody>tr')[i].childNodes[1].innerHTML].pago+=200;
		}
	}*///////Used to determine bonuses for most pageviews by week
	
	/*var sortable = [];
	for (var author in pay_authors)
		  sortable.push([author, pay_authors[author].notas])
	sortable.sort(function(a, b) {return a[1] - b[1]})
	pay_authors[sortable[sortable.length-2][0]].pago+=1000;*/////Used to determine which author had the most notes
	
	drawTableMonthPay();
}
function drawTableMonthPay(){
	var pay_authors_data = new google.visualization.DataTable();
	var pay_posts_data = new google.visualization.DataTable();
	var total_notas=0, total_pago=0;
	
	pay_authors['DonPotasio'].nombre = 'Roberto Garza Landa';
	pay_authors['DonPotasio'].tarjeta = '';
	pay_authors['DonPotasio'].banco = '';
    pay_authors['DonPotasio'].email = 'Roberto Garza Landa <donpotasio@gmail.com>';
    
	pay_authors['Emisan'].nombre = 'Jose Luis Martin Caparroso';
	pay_authors['Emisan'].tarjeta = 'cuenta: 209748659';
	pay_authors['Emisan'].banco = 'Banorte';
    pay_authors['Emisan'].email = 'Emilio Sanchez <emisanchez87@hotmail.com>';
	
    pay_authors['Ddtorresf'].nombre = 'David Guadalupe Torres Flores';
	pay_authors['Ddtorresf'].tarjeta = 'cuenta: 456533594';
	pay_authors['Ddtorresf'].banco = 'Banamex';
    pay_authors['Ddtorresf'].email = 'David Torres <ddtorresf@gmail.com>'
	
    pay_authors['Ugesaurio'].nombre = 'Eugenio Daniel Flores Alatorre';
	pay_authors['Ugesaurio'].tarjeta = 'cuenta: 1478233837';
	pay_authors['Ugesaurio'].banco = 'Bancomer';
    pay_authors['Ugesaurio'].email = 'Ugesaurio <ugesaurio@gmail.com>';
	
    pay_authors['Fercosta'].nombre = 'Fernando Costa Palazuelos';
	pay_authors['Fercosta'].tarjeta = 'cuenta: 2954650816';
	pay_authors['Fercosta'].banco = 'Bancomer';
    pay_authors['Fercosta'].email = 'Fernando Costa <fernandocostap@gmail.com>';
	
    pay_authors['Gordolobo'].nombre = 'José Alvaro Llovet Abascal';
	pay_authors['Gordolobo'].tarjeta = 'cuenta: 6419059351';
	pay_authors['Gordolobo'].banco = 'HSBC';
    pay_authors['Gordolobo'].mail = 'Alvaro Llovet <alvaro.llov@gmail.com>';
	
    pay_authors['Valenzuela'].nombre = 'Fransisco Javier Valenzuela Martinez';
	pay_authors['Valenzuela'].tarjeta = 'cuenta: 1236523609';
	pay_authors['Valenzuela'].banco = 'Bancomer';
    pay_authors['Valenzuela'].email = 'Francisco Valenzuela <valenzuelareves@gmail.com>';
	
    pay_authors['Alde'].nombre = 'Aldo Mendoza';
	pay_authors['Alde'].tarjeta = 'cuenta: 2958162964';
	pay_authors['Alde'].banco = 'Bancomer';
    pay_authors['Alde'].email = 'Alde <gggeckodj@gmail.com>';
	
    pay_authors['Adolfo Santino'].nombre = 'Aldo Esteban Fernández Guerra';
	pay_authors['Adolfo Santino'].tarjeta = 'cuenta: 014010605782676353';
	pay_authors['Adolfo Santino'].banco = 'Santander';
    pay_authors['Adolfo Santino'].email = 'Adolfo Santino <estebang33451975@gmail.com>';
	

	pay_authors_data.addColumn({type: 'string', label: 'Autor'});
	pay_authors_data.addColumn({type: 'number', label: 'Notas'});
	pay_authors_data.addColumn({type: 'number', label: 'Precio'});
	pay_authors_data.addColumn({type: 'number', label: 'Pago'});
	pay_authors_data.addColumn({type: 'string', label: 'Nombre'});
	pay_authors_data.addColumn({type: 'string', label: 'Banco'});
	pay_authors_data.addColumn({type: 'string', label: 'Tarjeta'});
	
	pay_posts_data.addColumn({type: 'string', label: 'Notas con bono'});
	pay_posts_data.addColumn({type: 'string', label: 'Autor'});
	pay_posts_data.addColumn({type: 'number', label: 'Pageviews'});

	$.each(pay_authors,function(key,value){
		if(value.notas>0){
			pay_authors_data.addRow([
				key,
				value.notas,
				{v: value.precio,f: '$'+numberWithCommas(value.precio.toFixed(2))},
				{v: value.pago,f: '$'+numberWithCommas(value.pago.toFixed(2))},
				value.nombre,
				value.banco,
				value.tarjeta
			]);
			//if(key!='DonPotasio'){
				total_notas+=value.notas;
				total_pago+=value.pago;
			//}
		}
	});
	
	pay_posts_data.addRow([
		null,
		null,
		null
	]);
	
	var Colabs_bono = ['Adolfo Santino','Ddtorresf','Gordolobo','Omar','Ugesaurio','Emisan','Valenzuela','Fercosta','Alde','DonPotasio'];
	for(var i=0;i<pay_posts.length;i++){
		if(pay_posts[i].bonus && $.inArray(pay_posts[i].author.nickname,Colabs_bono) > -1){
			pay_posts_data.addRow([
				pay_posts[i].url,
				pay_posts[i].author.nickname,
				pay_posts[i].pageviews
			]);
            pay_authors[pay_posts[i].author.nickname].bonus_posts.push(
                {             
                    'url': pay_posts[i].url,
                    'pageviews': pay_posts[i].pageviews
                }
            );
		}
	}
	
	pay_authors_data.addRow([
		{v: 'Total', f: '<b>Total</b>'},
		{v: total_notas,f: '<b>'+total_notas+'</b>'},
		null,
		{v: total_pago,f: '<b>$'+numberWithCommas(total_pago.toFixed(2))+'</b>'},
		'',
		'',
		''
	]);


	var options_a = {
		'allowHtml': true,
		'showRowNumber': false,
		'width':'100%',
		'height':'300px',
	};

	// Instantiate and draw our chart, passing in some options.
	var pay_authors_table = new google.visualization.Table(document.getElementById('table_payments'));  
	var pay_posts_table = new google.visualization.Table(document.getElementById('chart_div_pay_posts'));  

	google.visualization.events.addListener(pay_authors_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
        $('#paymentEmails').show();
	});

	pay_authors_table.draw(pay_authors_data, options_a);	
	pay_posts_table.draw(pay_posts_data, options_a);	
	//console.log("done");
}
function determineAuthorsPay(notas){
	switch(true){
		case notas<20:
			return 50;
			break;
		case notas<30:
			return 70;
			break;
		case notas<40:
			return 80;
			break;
		default:
			return 100;
			break;
	}
}
var mails_requests;
function paymentEmails(){
    mails_requests = [];
    var j=0;
    $('#emails-modal-body').empty();
    $.each(pay_authors,function(key,value){
        pay_authors[key].RFCmessage = 'to: '+pay_authors[key].email+'\r\n';
//        pay_authors[key].RFCmessage = 'to: danbecker87@gmail.com\r\n';
        pay_authors[key].RFCmessage += 'from: me\r\n';
        pay_authors[key].RFCmessage += 'Subject: Resumen de Cuentas de '+mesConcertado[new Date($('#pay_month')[0].value+'-03').getMonth()]+'\r\n\r\n';
        pay_authors[key].RFCmessage += 'Hola '+key+',\r\n\r\n Adjunto el resumen de cuenta para este mes:\r\n\r\n';
        pay_authors[key].RFCmessage += 'Notas'+'\t\t'+'Precio'+'\t\t'+'Pago'+'\r\n\r\n';
        pay_authors[key].RFCmessage += pay_authors[key].notas+'\t\t\t'+'$'+pay_authors[key].precio+'\t\t\t'+'$'+numberWithCommas(pay_authors[key].pago)+'\r\n\r\n';
        pay_authors[key].RFCmessage += 'Notas:\r\n\r\n';
        for(var i=0;i<pay_authors[key].top_posts.length;i++){
            pay_authors[key].RFCmessage += 
                (i+1)+'.-'+'\t\t'+
                pay_authors[key].top_posts[i].url+'\t\t'+
                /*numberWithCommas(pay_authors[key].top_posts[i].pageviews)+*/'\r\n\r\n';
        }
        pay_authors[key].RFCmessage += 'Notas con bono:\r\n\r\n';
        for(var i=0;i<pay_authors[key].bonus_posts.length;i++){
            pay_authors[key].RFCmessage += 
                (i+1)+'.-'+'\t\t'+
                pay_authors[key].bonus_posts[i].url+'\t\t'+
                numberWithCommas(pay_authors[key].bonus_posts[i].pageviews)+'\r\n\r\n';
        }
        pay_authors[key].RFCmessage += 'Un cordial saludo,\r\n\r\nDan Becker\r\n\r\nData & Accounting\r\n\r\nT. 5530-8671\r\nCel. 55-2270-7376';
        mails_requests.push(
            {
                mail: gapi.client.gmail.users.messages.send({
                    'userId': 'me',
                    'resource': {
                        'raw': window.btoa(pay_authors[key].RFCmessage).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '')
                    }
                }),
                colab: key
            }
        );
        
        //Add row to modal window
        if(pay_authors[key].pago > 0){
            $('#emails-modal-body').append(
                '<div class="row">'+
                    '<div class="col-md-10">'+
                        key+'&nbsp&nbsp'+'$'+numberWithCommas(pay_authors[key].pago)+'&nbsp&nbsp'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<button class="btn btn-rounded btn-success" onclick="sendColabEmail('+j+')">Send</button>'+
                    '</div>'+
                '</div>'
            );
        }
        j++;
    });
}
function sendColabEmail(index){
    mails_requests[index].mail.execute(function(resp){
        if (resp.error && resp.error.status) {
            // The API encountered a problem before the script started executing.
            alert('Mail failed to send');
            console.log('Error calling API: ' + JSON.stringify(resp, null, 2));
            console.log('Failed to sent mail to '+mails_requests[index].colab);
        } else if (resp.error) {
            // The API executed, but the script returned an error.
            alert('Mail failed to send');
            console.log('Script error! Message: ' + error.errorMessage);
            console.log('Failed to sent mail to '+mails_requests[index].colab);
        } else {
            console.log('Succesfully sent mail to '+mails_requests[index].colab);
        }
    });
}
//Payment Collabs

//Google Analytics Widget
var revenue, coverage, matched, adCPM, pageCPM, ADX_metric_flag=1, ADX_interval;
function getADXMetrics(){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestADX = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: "today",
			endDate: "today",
			//metric: ["Earnings","AD_REQUESTS_COVERAGE",'MATCHED_AD_REQUESTS'],
			metric: ["Earnings","AD_REQUESTS_COVERAGE",'AD_IMPRESSIONS'],
			fields: "totals"
		});
		requestADX.execute(function(resp) {
			revenue=parseFloat(resp.totals[0]);
			coverage = parseFloat(resp.totals[1])*100;
			matched = parseInt(resp.totals[2]);
			adCPM = revenue/matched*1000;
			gapi.client.load('analytics','v3', function() {
				var Pageviews = gapi.client.analytics.data.ga.get({
					ids: "ga:41142760",
					"start-date": 'today',
					"end-date": 'today',
					metrics: "ga:pageviews",
					fields: 'totalsForAllResults',
				});
				Pageviews.execute(function(resp){
					pageCPM=revenue/parseInt(resp['totalsForAllResults']['ga:pageviews'])*1000;
					ADX_interval = setInterval(function (){changeADXMetric();},5000);
				});
			});
		});
	});
}
function changeADXMetric(){
	switch(ADX_metric_flag){
		case 1:
			$("#ADX_metric")[0].innerHTML = "Revenue";
			$("#ADX_metric_value")[0].innerHTML = '$'+numberWithCommas(revenue.toFixed(2));
			ADX_metric_flag=2;
			break;
		case 2:
			$("#ADX_metric")[0].innerHTML = "Coverage";
			$("#ADX_metric_value")[0].innerHTML = numberWithCommas(coverage.toFixed(2))+'%';
			ADX_metric_flag=3;
			break;
		case 3:	
			$("#ADX_metric")[0].innerHTML = "Page CPM";
			$("#ADX_metric_value")[0].innerHTML = '$'+numberWithCommas(pageCPM.toFixed(2));
			ADX_metric_flag=4;
			break;
		case 4:	
			$("#ADX_metric")[0].innerHTML = "Ad CPM";
			$("#ADX_metric_value")[0].innerHTML = '$'+numberWithCommas(adCPM.toFixed(2));
			ADX_metric_flag=1;
			break;	
		default:
			ADX_metric_flag=1;
			break;		
	}
}
function refreshADXMetrics(){
	clearInterval(ADX_interval);
	getADXMetrics();
	getADXMonthMetrics();
}
//Google Analytics Widget

//ADX Range Metrics
var ADXdata;
function getADXRangeMetrics(){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestADX = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: from.value,
			endDate: to.value,
			dimension: 'date',
			//metric: ["Earnings","AD_REQUESTS_COVERAGE",'MATCHED_AD_REQUESTS','AD_REQUESTS'],
			metric: ["Earnings","AD_REQUESTS_COVERAGE",'AD_IMPRESSIONS','AD_REQUESTS'],
			filter: ADXFilters(),
			fields: "rows"
		});
		requestADX.execute(function(resp) {
			ADXdata = resp.rows;
			gapi.client.load('analytics','v3', function() {
				var Pageviews = gapi.client.analytics.data.ga.get({
					ids: "ga:41142760",
					"start-date": from.value,
					"end-date": to.value,
					dimensions: 'ga:date',
					metrics: "ga:pageviews",
					filters: AnalyticsFilters(),
					fields: 'rows',
				});
				Pageviews.execute(function(resp){
					for(var i=0;i<ADXdata.length;i++){
						ADXdata[i].push(resp.rows[i][1]);
					}
					
					/*FB.api(
						'/980493918670682/app_insights/app_event/',
						'GET',
						{"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":from.value+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00',"until":to.value+'T0'+(new Date().getTimezoneOffset()/60+3)+':00:00'},
						function(response) {
							var revenue=0;
							for(var i=0;i<response.data.length;i++){
                                if(typeof ADXdata[i] === 'undefined'){
                                
                                }else{
                                    ADXdata[i].FAN=parseFloat(response.data[i].value)*19.5;
                                }
								
							}
							drawADXRangeTable();
						}
					);*/
                    
                    FB.api(
                      '/980493918670682/adnetworkanalytics/',
                      'POST',
                      {"metrics":"['fb_ad_network_revenue']","aggregation_period":"day","since":from.value,"until":to.value},
                      function(query) {
                          var response_query_id = query.query_id;
                          setTimeout(function(){
                            FB.api(
                                '/980493918670682/adnetworkanalytics_results/',
                                'GET',
                                {"query_ids":"[\'"+response_query_id+"\']"},
                                function(response) {
                                    var revenue=0;
                                    for(var i=0;i<response.data[0].results.length;i++){
                                        if(typeof ADXdata[i] === 'undefined'){

                                        }else{
                                            ADXdata[i].FAN=parseFloat(response.data[0].results[i].value)*19.5;
                                        }

                                    }
                                    drawADXRangeTable();
                                }
                            );
                          },2000);
                      }
                    );
				});
			});
		});
	});
}
function ADXFilters(){
	var filters = [];
	if($('#selector_deal')[0].value != 'all'){
		filters.push($('#selector_deal')[0].value);
	}
	if($('#selector_country')[0].value != 'all'){
		filters.push($('#selector_country')[0].value);
	}
	if($('#selector_device')[0].value != 'all'){
		filters.push($('#selector_device')[0].value);
	}
	if($('#selector_branding')[0].value != 'all'){
		filters.push($('#selector_branding')[0].value);
	}
	if(filters.length==0){
		return null;
	}else{
		return filters;
	}
}
function AnalyticsFilters(){
	var filters = "";
	switch($('#selector_country')[0].selectedIndex){
		case 0:
			break;
		case 1: 
			filters+='ga:country==United States';
			break;
		case 2:
			filters+='ga:country==Mexico';
			break;
		default:
			break;
	}
	switch($('#selector_device')[0].selectedIndex){
		case 0:
			break;
		case 1: 
			filters+='ga:deviceCategory==desktop';
			break;
		case 2:
			filters+='ga:deviceCategory==mobile';
			break;
		case 3:
			filters+='ga:deviceCategory==tablet';	
			break;
		case 4:
			filters+='ga:deviceCategory!=desktop';
			break;
		default:
			break;
	}
	if(filters == ""){
		return null;
	}else{
		return filters;
	}
}
function drawADXRangeTable(){
	var PageCPM = new google.visualization.DataTable();
	var Earnings = new google.visualization.DataTable();
	var Coverage = new google.visualization.DataTable();
	var AdCPM = new google.visualization.DataTable();
	var RealCPM = new google.visualization.DataTable();
	var PageCPMADXFAN = new google.visualization.DataTable();
	var TotalEarnings = 0;

	PageCPM.addColumn({type: 'date', label: 'Fecha'});
	PageCPM.addColumn({type: 'number', label: 'Page CPM'});

	Earnings.addColumn({type: 'date', label: 'Fecha'});
	Earnings.addColumn({type: 'number', label: 'Earnings'});

	Coverage.addColumn({type: 'date', label: 'Fecha'});
	Coverage.addColumn({type: 'number', label: 'Coverage'});
	
	AdCPM.addColumn({type: 'date', label: 'Fecha'});
	AdCPM.addColumn({type: 'number', label: 'Ad CPM'});
	
	RealCPM.addColumn({type: 'date', label: 'Fecha'});
	RealCPM.addColumn({type: 'number', label: 'Real CPM'});
	
	PageCPMADXFAN.addColumn({type: 'date', label: 'Fecha'});
	PageCPMADXFAN.addColumn({type: 'number', label: 'Page CPM ADX + Fan'});

	for(var i=0;i<ADXdata.length;i++){
		Earnings.addRow([
			new Date(new Date(ADXdata[i][0]).getTime()+(12+new Date().getTimezoneOffset()/60)*60*60*1000),
			{v: parseFloat(ADXdata[i][1]), f: '$'+numberWithCommas(ADXdata[i][1])}
		]);
		TotalEarnings+=parseFloat(ADXdata[i][1]);
		Coverage.addRow([
			new Date(new Date(ADXdata[i][0]).getTime()+(12+new Date().getTimezoneOffset()/60)*60*60*1000),
			{v: parseFloat(ADXdata[i][2]), f: (parseFloat(ADXdata[i][2])*100)+'%'}
		]);
		PageCPM.addRow([
			new Date(new Date(ADXdata[i][0]).getTime()+(12+new Date().getTimezoneOffset()/60)*60*60*1000),
			{v: parseFloat(ADXdata[i][1])/parseFloat(ADXdata[i][5])*1000, f: '$'+(parseFloat(ADXdata[i][1])/parseFloat(ADXdata[i][5])*1000).toFixed(2)}
		]);
		AdCPM.addRow([
			new Date(new Date(ADXdata[i][0]).getTime()+(12+new Date().getTimezoneOffset()/60)*60*60*1000),
			{v: parseFloat(ADXdata[i][1])/parseFloat(ADXdata[i][3])*1000, f: '$'+(parseFloat(ADXdata[i][1])/parseFloat(ADXdata[i][3])*1000).toFixed(2)}
		]);
		RealCPM.addRow([
			new Date(new Date(ADXdata[i][0]).getTime()+(12+new Date().getTimezoneOffset()/60)*60*60*1000),
			{v: parseFloat(ADXdata[i][1])/parseFloat(ADXdata[i][4])*1000, f: '$'+(parseFloat(ADXdata[i][1])/parseFloat(ADXdata[i][4])*1000).toFixed(2)}
		]);
		PageCPMADXFAN.addRow([
			new Date(new Date(ADXdata[i][0]).getTime()+(12+new Date().getTimezoneOffset()/60)*60*60*1000),
			{v: (parseFloat(ADXdata[i][1])+ADXdata[i].FAN)/parseFloat(ADXdata[i][5])*1000, f: '$'+((parseFloat(ADXdata[i][1])+ADXdata[i].FAN)/parseFloat(ADXdata[i][5])*1000).toFixed(2)}
		]);
	}

	$('#total_earnings')[0].innerHTML = numberWithCommas(TotalEarnings.toFixed(2));

	formating_Earnings = {
		title: 'Earnings',
		//curveType: 'function',
		vAxis: {
			format: 'decimal'
		},
		hAxis: {
			format: 'M/d',
			gridlines:{
				count: -1
			},
			title: 'Fecha'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};
	formating_PageCPM = {
		title: 'Page CPM',
		//curveType: 'function',
		vAxis: {
			format: 'decimal'
		},
		hAxis: {
			format: 'M/d',
			gridlines:{
				count: -1
			},
			title: 'Fecha'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};
	formating_AdCPM = {
		title: 'Ad CPM',
		//curveType: 'function',
		vAxis: {
			format: 'decimal'
		},
		hAxis: {
			format: 'M/d',
			gridlines:{
				count: -1
			},
			title: 'Fecha'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};
	formating_RealCPM = {
		title: 'Real CPM',
		//curveType: 'function',
		vAxis: {
			format: 'decimal'
		},
		hAxis: {
			format: 'M/d',
			gridlines:{
				count: -1
			},
			title: 'Fecha'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};
	formating_Coverage = {
		title: 'Coverage',
		//curveType: 'function',
		vAxis: {
			format: 'percent'
		},
		hAxis: {
			format: 'M/d',
			gridlines:{
				count: -1
			},
			title: 'Fecha'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};

	//var formatter = new google.visualization.NumberFormat({pattern:'#,###'});
	//formatter.format(data_table, 1);

  // Instantiate and draw our chart, passing in some options.
  var table_Earnings = new google.visualization.LineChart(document.getElementById('chart_div_Earnings'));
  var table_AdCPM = new google.visualization.LineChart(document.getElementById('chart_div_AdCPM'));
  var table_RealCPM = new google.visualization.LineChart(document.getElementById('chart_div_RealCPM'));
  var table_PageCPM = new google.visualization.LineChart(document.getElementById('chart_div_PageCPM'));
  var table_PageCPMFANADX = new google.visualization.LineChart(document.getElementById('chart_div_FAN_ADX_pageCPM'));
  var table_Coverage = new google.visualization.LineChart(document.getElementById('chart_div_Coverage'));
  
  
  table_Earnings.draw(Earnings,formating_Earnings);
  table_AdCPM.draw(AdCPM,formating_AdCPM);
  table_PageCPM.draw(PageCPM,formating_PageCPM);
  table_Coverage.draw(Coverage,formating_Coverage);
  table_RealCPM.draw(RealCPM,formating_RealCPM);
  table_PageCPMFANADX.draw(PageCPMADXFAN,formating_RealCPM);
}	
//ADX Range Metrics

//ADX Month Metrics
var month_coverage, month_revenue, ADX_month_metrics_flag=1, last_month_coverage, last_month_revenue, last_full_month_coverage, last_full_month_revenue;
function getADXMonthMetrics(){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestADX = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: 'startOfMonth',
			endDate: 'today',
			metric: ["Earnings","AD_REQUESTS_COVERAGE"],
			fields: "totals"
		});
		requestADX.execute(function(resp) {
			month_revenue = parseFloat(resp.totals[0]);
			month_coverage = parseFloat(resp.totals[1]);
			//ADX_month_interval = setInterval(function (){changeADXMonthMetric();},5000);
		});
		var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
		var requestADXLastMonth = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			endDate: new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000).toISOString().substr(0,10),
			metric: ["Earnings","AD_REQUESTS_COVERAGE"],
			fields: "totals"
		});
		requestADXLastMonth.execute(function(resp) {
			last_month_revenue = parseFloat(resp.totals[0]);
			last_month_coverage = parseFloat(resp.totals[1]);
		});
		var requestADXFullLastMonth = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			endDate: new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10),
			metric: ["Earnings","AD_REQUESTS_COVERAGE"],
			fields: "totals"
		});
		requestADXFullLastMonth.execute(function(resp) {
			last_full_month_revenue = parseFloat(resp.totals[0]);
			last_full_month_coverage = parseFloat(resp.totals[1]);
			ADX_month_interval = setInterval(function (){changeADXMonthMetric();},5000);
			getTotalRevenue();
		});
	});
}
function changeADXMonthMetric(){
	switch(ADX_month_metrics_flag){
		case 1:
			$("#ADX_month_metric")[0].innerHTML = "Month Revenue $"+numberWithCommas(last_full_month_revenue.toFixed(2));
			$("#ADX_month_metric_value")[0].innerHTML = '$'+numberWithCommas(month_revenue.toFixed(2));
			if(month_revenue-last_month_revenue > 0){
				document.getElementById("month_revenue_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("month_revenue_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("month_revenue_change").innerHTML = ((month_revenue/last_month_revenue-1)*100).toFixed(2);
			}else{
				document.getElementById("month_revenue_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("month_revenue_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("month_revenue_change").innerHTML = ((1-month_revenue/last_month_revenue)*100).toFixed(2);
			}
			ADX_month_metrics_flag=2;
			break;
		case 2:
			$("#ADX_month_metric")[0].innerHTML = "Average Coverage %"+numberWithCommas((100*last_full_month_coverage).toFixed(2));
			$("#ADX_month_metric_value")[0].innerHTML = numberWithCommas((100*month_coverage).toFixed(2))+'%';
			if(month_coverage-last_month_coverage > 0){
				document.getElementById("month_revenue_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("month_revenue_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("month_revenue_change").innerHTML = ((month_coverage/last_month_coverage-1)*100).toFixed(2);
			}else{
				document.getElementById("month_revenue_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("month_revenue_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("month_revenue_change").innerHTML = ((1-month_coverage/last_month_coverage)*100).toFixed(2);
			}

			ADX_month_metrics_flag=1;
			break;
		default:
			ADX_month_metrics_flag=1;
			break;		
	}
	
}
//ADX Month Metrics

//FB Reach
function getReach(){
	var start_date=new Date(from_fb.value);
	start_date.setTime( start_date.getTime() + 86400000 );

	var end_date=new Date(to_fb.value);
	end_date.setTime( end_date.getTime() + 86400000*2 );
	//console.log(end_date.toISOString());
	
	$.ajax({
		url: "https://graph.facebook.com/v2.12/eldeforma/insights?since="+start_date.toISOString()+"&until="+end_date.toISOString()+"&metric=['page_impressions_unique']&period=day"+"&"+access_token,
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var days_reach = response.data[0].values;
	    	var total=0;
	    	for(var i=0;i<days_reach.length;i++){
	    		//console.log(days_reach[i].value);
	    		total+=days_reach[i].value;
	    	}
	    	//console.log(total);
	    	drawChartReach(days_reach);
    	}
    });
    
    end_date.setTime( end_date.getTime() - 86400000*2 );
    $.ajax({
		url: "https://graph.facebook.com/v2.12/eldeforma/insights/page_impressions_by_paid_non_paid_unique/days_28?since="+end_date.getFullYear()+"-"+(end_date.getMonth()+1)+"-28&until="+end_date.getFullYear()+"-"+(end_date.getMonth()+2)+"-02&fields=values"+"&"+access_token,
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var reach = response.data[0].values;
	    	console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value.total;
				return values;
			});
			console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
	    	document.getElementById("paid_reach").innerHTML= numberWithCommas(reach[max].value.paid)+" ("+(reach[max].value.paid/reach[max].value.total*100).toFixed(1)+"%)";
	    	document.getElementById("unpaid_reach").innerHTML = numberWithCommas(reach[max].value.unpaid)+" ("+(reach[max].value.unpaid/reach[max].total*100).toFixed(1)+"%)";
	    	document.getElementById("total_reach").innerHTML = numberWithCommas(reach[max].value.total);
    	}
    });
}
function drawChartReach(days_reach){

  var data_table = new google.visualization.DataTable();
  
  data_table.addColumn('datetime', 'Fecha');
  data_table.addColumn('number', 'Reach');
  
  var total=0;
    
  for(var n=0;n<days_reach.length;n++){
	var data_date=new Date(days_reach[n].end_time)
	data_date.setTime(data_date.getTime() - 86400000);
	
	data_table.addRow([data_date, days_reach[n].value]);
	formating_reach = {
		'width':'100%',
		'height':'300px',
		title: 'Reach',
		//curveType: 'function',
		vAxis: {
			format: 'decimal'
		},
		hAxis: {
			format: 'M/d',
			gridlines:{
				count: -1
			},
			title: 'Fecha'
		},
		legend:{
			position: 'none'
		},
		pointSize: 3,
	};
  }

	var formatter = new google.visualization.NumberFormat({pattern:'#,###'});
	formatter.format(data_table, 1);

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_reach'));
  
  table.draw(data_table,formating_reach);
}
//FB Reach

//FB Revenue
var FBrevenue=0;
var last_month_FBrevenue=0, last_month_total_FBrevenue=0;
function getFBRevenue(){
	FBrevenue=0;
	last_month_FBrevenue=0;
	last_month_total_FBrevenue=0;
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var start=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
    start.setDate(1);
    start = start.toISOString().substr(0,10);
	var last_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+1)+'-'+'01';
	var last_month_end = new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10);
	var last_month_day = new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000).toISOString().substr(0,10);
	/*FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":start},
	  function(response) {
		for(var i=0;i<response.data.length;i++){
			FBrevenue+=parseFloat(response.data[i].value);
		}
		//console.log(FBrevenue);
		$('#FBRevenue')[0].innerHTML = '$'+numberWithCommas(FBrevenue.toFixed(2));
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': last_month_day.toISOString().substr(0,10)},
		  function(response) {
			for(var i=0;i<response.data.length;i++){
				last_month_FBrevenue+=parseFloat(response.data[i].value);
			}
			if(FBrevenue-last_month_FBrevenue > 0){
				document.getElementById("FB_revenue_trend").className = "zmdi zmdi-trending-up";
				document.getElementById("FB_revenue_badge").className = "badge badge-success pull-left m-t-20";
				document.getElementById("FB_revenue_change").innerHTML = ((FBrevenue/last_month_FBrevenue-1)*100).toFixed(2);
			}else{
				document.getElementById("FB_revenue_trend").className = "zmdi zmdi-trending-down";
				document.getElementById("FB_revenue_badge").className = "badge badge-danger pull-left m-t-20";
				document.getElementById("FB_revenue_change").innerHTML = ((1-FBrevenue/last_month_FBrevenue)*100).toFixed(2);
			}
		  }
		);
	  }
	);*/
    
    FB.api(
      '/980493918670682/adnetworkanalytics/',
      'POST',
      {"metrics":"['fb_ad_network_revenue']","aggregation_period":"total","since":start},
      function(query) {
          var response_query_id = query.query_id;
          setTimeout(function(){
            FB.api(
                '/980493918670682/adnetworkanalytics_results/',
                'GET',
                {"query_ids":"[\'"+response_query_id+"\']"},
                function(response) {
                    FBrevenue=parseFloat(response.data[0].results[0].value);
                    $('#FBRevenue')[0].innerHTML = '$'+numberWithCommas(FBrevenue.toFixed(2));
                    FB.api(
                      '/980493918670682/adnetworkanalytics/',
                      'POST',
                      {"metrics":"['fb_ad_network_revenue']","aggregation_period":"total","since":last_month_start,"until":last_month_day},
                      function(last_month_query) {
                          var response_last_month_query_id = last_month_query.query_id;
                          setTimeout(function(){
                            FB.api(
                                '/980493918670682/adnetworkanalytics_results/',
                                'GET',
                                {"query_ids":"[\'"+response_last_month_query_id+"\']"},
                                function(last_month_response) {
                                    last_month_FBrevenue=parseFloat(last_month_response.data[0].results[0].value);
                                    if(FBrevenue-last_month_FBrevenue > 0){
                                        document.getElementById("FB_revenue_trend").className = "zmdi zmdi-trending-up";
                                        document.getElementById("FB_revenue_badge").className = "badge badge-success pull-left m-t-20";
                                        document.getElementById("FB_revenue_change").innerHTML = ((FBrevenue/last_month_FBrevenue-1)*100).toFixed(2);
                                    }else{
                                        document.getElementById("FB_revenue_trend").className = "zmdi zmdi-trending-down";
                                        document.getElementById("FB_revenue_badge").className = "badge badge-danger pull-left m-t-20";
                                        document.getElementById("FB_revenue_change").innerHTML = ((1-FBrevenue/last_month_FBrevenue)*100).toFixed(2);
                                    }
                                }
                            );
                          },4000);
                      }
                    );
                }
            );
          },4000);
      }
    );
   
	/*FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': this_month_start},
	  function(response) {
	  	for(var i=0;i<response.data.length;i++){
			last_month_total_FBrevenue+=parseFloat(response.data[i].value);
		}
		document.getElementById("FB_last_month_revenue").innerHTML = numberWithCommas(last_month_total_FBrevenue.toFixed(2));
		getTotalRevenue();
	  }
	);*/
    
    FB.api(
      '/980493918670682/adnetworkanalytics/',
      'POST',
      {"metrics":"['fb_ad_network_revenue']","aggregation_period":"total","since":last_month_start,"until":last_month_end},
      function(query) {
          var query_id = query.query_id;
          setTimeout(function(){
            FB.api(
                '/980493918670682/adnetworkanalytics_results/',
                'GET',
                {"query_ids":"[\'"+query_id+"\']"},
                function(response) {
                    last_month_total_FBrevenue=parseFloat(response.data[0].results[0].value);
                    document.getElementById("FB_last_month_revenue").innerHTML = numberWithCommas(last_month_total_FBrevenue.toFixed(2));
                    getTotalRevenue();
                }
            );
          },4000);
      }
    );
}
function getFBDailyRevenue(){
	//console.log(since);
	var revenue=0, yesterday_revenue=0;
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
    var yesterday=new Date(now.getTime()-24*60*60*1000);
	/*FB.api(
		'/980493918670682/app_insights/app_event/',
		'GET',
		{"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":'tomorrow',"until":'tomorrow'},
		function(response) {
			for(var i=0;i<response.data.length;i++){
				revenue+=parseFloat(response.data[i].value);
			}
			$('#FBDailyRevenue')[0].innerHTML = numberWithCommas(revenue.toFixed(2));
			FB.api(
				'/980493918670682/app_insights/app_event/',
				'GET',
				{"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":'today',"until":'today'},
				function(response) {
					for(var i=0;i<response.data.length;i++){
						yesterday_revenue+=parseFloat(response.data[i].value);
					}
					$('#FB_yesterday_revenue')[0].innerHTML = numberWithCommas(yesterday_revenue.toFixed(2));
					if(revenue-yesterday_revenue > 0){
						document.getElementById("FB_daily_revenue_trend").className = "zmdi zmdi-trending-up";
						document.getElementById("FB_daily_revenue_badge").className = "badge badge-success pull-left m-t-20";
						document.getElementById("FB_daily_revenue_change").innerHTML = ((revenue/yesterday_revenue-1)*100).toFixed(2);
					}else{
						document.getElementById("FB_daily_revenue_trend").className = "zmdi zmdi-trending-down";
						document.getElementById("FB_daily_revenue_badge").className = "badge badge-danger pull-left m-t-20";
						document.getElementById("FB_daily_revenue_change").innerHTML = ((1-revenue/yesterday_revenue)*100).toFixed(2);
					}
				}
			);
		}
	);*/
    
    FB.api(
      '/980493918670682/adnetworkanalytics/',
      'POST',
      {"metrics":"['fb_ad_network_revenue']","aggregation_period":"total","since":now.toISOString().substr(0,10)},
      function(query) {
          var response_query_id = query.query_id;
          setTimeout(function(){
            FB.api(
                '/980493918670682/adnetworkanalytics_results/',
                'GET',
                {"query_ids":"[\'"+response_query_id+"\']"},
                function(response) {
                    revenue=parseFloat(response.data[0].results[0].value);
                    $('#FBDailyRevenue')[0].innerHTML = numberWithCommas(revenue.toFixed(2));
                    FB.api(
                      '/980493918670682/adnetworkanalytics/',
                      'POST',
                      {"metrics":"['fb_ad_network_revenue']","aggregation_period":"total","since":yesterday.toISOString().substr(0,10),"until":yesterday.toISOString().substr(0,10)},
                      function(yesterday_query) {
                          var yesterday_query_id = yesterday_query.query_id;
                          setTimeout(function(){
                            FB.api(
                                '/980493918670682/adnetworkanalytics_results/',
                                'GET',
                                {"query_ids":"[\'"+yesterday_query_id+"\']"},
                                function(yesterday_response) {
                                    yesterday_revenue=parseFloat(yesterday_response.data[0].results[0].value);
                                    $('#FB_yesterday_revenue')[0].innerHTML = numberWithCommas(yesterday_revenue.toFixed(2));
                                    if(revenue-yesterday_revenue > 0){
                                        document.getElementById("FB_daily_revenue_trend").className = "zmdi zmdi-trending-up";
                                        document.getElementById("FB_daily_revenue_badge").className = "badge badge-success pull-left m-t-20";
                                        document.getElementById("FB_daily_revenue_change").innerHTML = ((revenue/yesterday_revenue-1)*100).toFixed(2);
                                    }else{
                                        document.getElementById("FB_daily_revenue_trend").className = "zmdi zmdi-trending-down";
                                        document.getElementById("FB_daily_revenue_badge").className = "badge badge-danger pull-left m-t-20";
                                        document.getElementById("FB_daily_revenue_change").innerHTML = ((1-revenue/yesterday_revenue)*100).toFixed(2);
                                    }
                                }
                            );
                          },4000);
                      }
                    );
                }
            );
          },4000);
      }
    );
}
//FB Revenue

//Total Revenue
var total_revenue, last_month_total_revenue, last_full_month_total_revenue;
function getTotalRevenue(){
	total_revenue = month_revenue+(FBrevenue+adman_month_revenue+taboola_month_revenue)*19.5;
	$('#total_month_revenue')[0].innerHTML = '$ '+numberWithCommas(total_revenue.toFixed(2));

	last_month_total_revenue = last_month_revenue+(last_month_FBrevenue+adman_revenue_last_month_day+taboola_revenue_last_month_day)*19.5;

	if(total_revenue-last_month_total_revenue > 0){
		document.getElementById("total_revenue_trend").className = "zmdi zmdi-trending-up";
		document.getElementById("total_revenue_badge").className = "badge badge-success pull-left m-t-20";
		document.getElementById("total_revenue_change").innerHTML = ((total_revenue/last_month_total_revenue-1)*100).toFixed(1);
	}else{
		document.getElementById("total_revenue_trend").className = "zmdi zmdi-trending-down";
		document.getElementById("total_revenue_badge").className = "badge badge-danger pull-left m-t-20";
		document.getElementById("total_revenue_change").innerHTML = ((1-total_revenue/last_month_total_revenue)*100).toFixed(1);
	}

	last_full_month_total_revenue = last_full_month_revenue+(last_month_total_FBrevenue+adman_revenue_last_month+taboola_last_month_revenue)*19.5;
	$('#total_last_month_revenue')[0].innerHTML = '$ '+numberWithCommas(last_full_month_total_revenue.toFixed(2));
}
//Total Revenue

//Social Month Metrics KPI's
function getSocialMetricsDeforma(){
	//Shares
	var now = new Date();
	var first = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7))-1, 1);
	var last = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/eldeforma/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageDeforma(response.paging.next,posts);
			}
		}
	);
	//VideoViews
	var now = new Date();
	var first = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7))-1, 2);
	var last = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24*2);
	document.getElementById('video_views_page').innerHTML = me.innerHTML;
	FB.api(
		'/eldeforma/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",access_token: pageTokens['El Deforma']},
		function(response) {
			//console.log(response);
			var videos=[];
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				videos.push(response.data[0].values[i]);
				total_videos+=videos[i].value;
			}
			document.getElementById('video_views_deforma').innerHTML = numberWithCommas(total_videos);
		}
	);
	//Reach
    var since = new Date($('#month_social_metrics_deforma')[0].value+'-28T00:00:00');
    var until = new Date($('#month_social_metrics_deforma')[0].value+'-28T00:00:00');
    until.setDate(until.getDate() + 5);
    $.ajax({
		url: "https://graph.facebook.com/v2.12/eldeforma?fields=insights.metric(page_impressions_unique).fields(values).period(days_28).since("+since.toISOString().substr(0,10)+").until("+until.toISOString().substr(0,10)+")&access_token="+pageTokens['El Deforma'],
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var reach = response.insights.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
	    	document.getElementById("reach_deforma").innerHTML = numberWithCommas(reach[max].value);
    	}
    });
    //CTR
    var first = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7))-1, 28);
	var last = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7)), 2);    
    FB.api(
		'/eldeforma',
		'GET',
		{"fields":"insights.metric(page_consumptions_by_consumption_type).fields(values).as(clicks).period(days_28).since("+first.toISOString().substr(0,10)+")"+".until("+last.toISOString().substr(0,10)+"),insights.metric(page_impressions).fields(values).as(imp).period(days_28).since("+first.toISOString().substr(0,10)+")"+".until("+last.toISOString().substr(0,10)+")",access_token: pageTokens['El Deforma']},
		function(response) {
			var CTR=[];
			var imp=response.imp.data[0].values;
			var clicks=response.clicks.data[0].values;
			for(var i=0;i<imp.length;i++){
				CTR.push(clicks[i].value['link clicks']/imp[i].value*100);
			}
			var max = CTR.indexOf(Math.max(...CTR));
			document.getElementById("avg_ctr_deforma").innerHTML = numberWithCommas(CTR[max].toFixed(2))+'%';
		}
	);
	//Analytics Metrics
	var start = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7))-1, 1);
	var end = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7)), 0);
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": start.toISOString().substr(0,10),
			"end-date": end.toISOString().substr(0,10),
			metrics: "ga:pageviews,ga:Users,ga:Sessions",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestPageviews.execute(function(resp){
			$('#pageviews_deforma')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:pageviews"]);
			$('#users_deforma')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:Users"]);
			$('#sessions_deforma')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:Sessions"]);
		});
		var requestSocial = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": start.toISOString().substr(0,10),
			"end-date": end.toISOString().substr(0,10),
			metrics: "ga:totalEvents",
			dimensions: "ga:eventAction",
			fields: "totalsForAllResults",
			output: 'json',
			filters: 'ga:eventAction==Facebook Share Class'
		});
		requestSocial.execute(function(resp){
			$('#shares_on_site_deforma')[0].innerHTML = numberWithCommas(resp.totalsForAllResults['ga:totalEvents']);
		});
	});
	//VTR clicked
    var first_vtr = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7))-1, 28);
	var last_vtr = new Date($('#month_social_metrics_deforma')[0].value.substr(0,4), parseInt($('#month_social_metrics_deforma')[0].value.substr(5,7)), 2);
    FB.api(
		'/eldeforma',
		'GET',
		{"fields":"insights.metric(page_consumptions_by_consumption_type).fields(values).as(views).period(days_28),insights.metric(page_impressions).fields(values).as(imp).period(days_28)","since":first_vtr.toISOString().substr(0,10),"until": last_vtr.toISOString().substr(0,10),access_token: pageTokens['El Deforma']},
		function(response) {
            console.log(response);
			var VTR=[];
			var imp=response.imp.data[0].values;
			var views=response.views.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value['video play']/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_deforma_click").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
	//VTR views
	FB.api(
		'/eldeforma/insights',
		'GET',
		{"metric":"['page_video_views','page_impressions']","period":"days_28","since":first_vtr.toISOString().substr(0,10),"until": last_vtr.toISOString().substr(0,10),access_token: pageTokens['El Deforma']},
		function(response) {
			var VTR=[];
			var imp=response.data[1].values;
			var views=response.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_deforma").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
	//ACPE
	FB.api(
		'/953559291352739/owned_ad_accounts',
		'GET',
		{"fields":"id,currency"},
		function(response) {
			var accounts=response.data;
			for(var i=0;i<accounts.length;i++){
				$.ajax({
					url: 'https://graph.facebook.com/v2.12/'+accounts[i].id+'/insights?fields=spend,actions&time_range[since]='+start.toISOString().substr(0,10)+'&time_range[until]='+end.toISOString().substr(0,10)+'&access_token='+access_token,
					type: 'get',
					async: false,
					success: function(resp){
						//console.log(resp);
						if(resp.data.length > 0){
							accounts[i].engagement = resp.data[0].actions;
							accounts[i].spend = resp.data[0].spend;
						}else{
							accounts.splice(i,1);
							i--;
						}
					}
				});
			}
			//console.log(accounts);
			var netACPE = 0;
			for(var i=0;i<accounts.length;i++){
				if(accounts[i].currency == 'USD'){
					accounts[i].spend*=19.5;
				}
				accounts[i].ACPE = accounts[i].spend/getEngagements(accounts[i].engagement);
				netACPE += accounts[i].ACPE;
			}
			netACPE/=accounts.length;
			$('#ACPE')[0].innerHTML = netACPE.toFixed(4);
		}
	);
}
function nextPageDeforma(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageDeforma(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$("#shares_deforma")[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}
function getEngagements(actions){
	//console.log(actions);
	var total=0;
	for(var i=0;i<actions.length;i++){
		switch(actions[i].action_type){
			case 'post_engagement':
				total+=parseInt(actions[i].value);
				break;
			case 'unlike':
				total-=parseInt(actions[i].value);
				break;
			case 'post_like':
				total-=parseInt(actions[i].value);
				break;
			case 'post':
				total-=parseInt(actions[i].value);
				break;
			case 'photo_view':
				total-=parseInt(actions[i].value);
				break;
			default:
				break;
		}
	}
	return total;
}

function getSocialMetricsRepsodia(){
	//Shares
	var now = new Date();
	var first = new Date($('#month_social_metrics_repsodia')[0].value.substr(0,4), parseInt($('#month_social_metrics_repsodia')[0].value.substr(5,7))-1, 1);
	var last = new Date($('#month_social_metrics_repsodia')[0].value.substr(0,4), parseInt($('#month_social_metrics_repsodia')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/repsodia/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageRepsodia(response.paging.next,posts);
			}
            else{
                var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$("#shares_repsodia")[0].innerHTML = numberWithCommas(shares);
            }
		}
	);
	//Video Views
	var now = new Date();
	var first = new Date($('#month_social_metrics_repsodia')[0].value.substr(0,4), parseInt($('#month_social_metrics_repsodia')[0].value.substr(5,7))-1, 2);
	var last = new Date($('#month_social_metrics_repsodia')[0].value.substr(0,4), parseInt($('#month_social_metrics_repsodia')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24*2);
	document.getElementById('video_views_page').innerHTML = me.innerHTML;
	FB.api(
		'/repsodia/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",'access_token': pageTokens['Repsodia']},
		function(response) {
			//console.log(response);
			var videos=[];
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				//console.log(response.data[0].values[i].value,total_videos);
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('video_views_repsodia').innerHTML = numberWithCommas(total_videos);
		}
	);
	//Reach
	var since = new Date($('#month_social_metrics_repsodia')[0].value+'-28T00:00:00');
    var until = new Date($('#month_social_metrics_repsodia')[0].value+'-28T00:00:00');
    until.setDate(until.getDate() + 5);
    $.ajax({
		url: "https://graph.facebook.com/v2.12/repsodia?fields=insights.metric(page_impressions_unique).fields(values).period(days_28).since("+since.toISOString().substr(0,10)+").until("+until.toISOString().substr(0,10)+")&access_token="+pageTokens['Repsodia'],
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var reach = response.insights.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
	    	document.getElementById("reach_repsodia").innerHTML = numberWithCommas(reach[max].value);
    	}
    });
    //VTR clicked
    var first = new Date($('#month_social_metrics_repsodia')[0].value.substr(0,4), parseInt($('#month_social_metrics_repsodia')[0].value.substr(5,7))-1, 28);
	var last = new Date($('#month_social_metrics_repsodia')[0].value.substr(0,4), parseInt($('#month_social_metrics_repsodia')[0].value.substr(5,7)), 2);
    FB.api(
		'/repsodia',
		'GET',
		{"fields":"insights.metric(page_consumptions_by_consumption_type).fields(values).as(views).period(days_28),insights.metric(page_impressions).fields(values).as(imp).period(days_28)","since":first.toISOString().substr(0,10),'until': last.toISOString().substr(0,10),'access_token':pageTokens['Repsodia']},
		function(response) {
            //console.log(response);
			var VTR=[];
			var imp=response.imp.data[0].values;
			var views=response.views.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value['video play']/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_repsodia_click").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
	//VTR views
	FB.api(
		'/repsodia/insights',
		'GET',
		{"metric":"['page_video_views','page_impressions']","period":"days_28","since":first.toISOString().substr(0,10),"until": last.toISOString().substr(0,10),'access_token': pageTokens['Repsodia']},
		function(response) {
			var VTR=[];
			var imp=response.data[1].values;
			var views=response.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_repsodia").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
}
function nextPageRepsodia(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageRepsodia(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$("#shares_repsodia")[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}

function getSocialMetricsCult(){
	//Shares
	var now = new Date();
	var first = new Date($('#month_social_metrics_cult')[0].value.substr(0,4), parseInt($('#month_social_metrics_cult')[0].value.substr(5,7))-1, 1);
	var last = new Date($('#month_social_metrics_cult')[0].value.substr(0,4), parseInt($('#month_social_metrics_cult')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/techcult/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageCult(response.paging.next,posts);
           }else{
               var shares=0;
               for(var j=0;j<posts.length;j++){
                   if(typeof posts[j].shares !== 'undefined'){
                       shares+=posts[j].shares.count;
                       //console.log(posts[j].shares.count);
                   }
               }
               $("#shares_cult")[0].innerHTML = numberWithCommas(shares);
           }
		}
	);
	//Video Views
	var now = new Date();
	var first = new Date($('#month_social_metrics_cult')[0].value.substr(0,4), parseInt($('#month_social_metrics_cult')[0].value.substr(5,7))-1, 2);
	var last = new Date($('#month_social_metrics_cult')[0].value.substr(0,4), parseInt($('#month_social_metrics_cult')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24*2);
	document.getElementById('video_views_page').innerHTML = me.innerHTML;
	FB.api(
		'/techcult/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",access_token: pageTokens['Tech Cult']},
		function(response) {
			//console.log(response);
			var videos=[];
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				//console.log(response.data[0].values[i].value,total_videos);
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('video_views_cult').innerHTML = numberWithCommas(total_videos);
		}
	);
	//Reach
	var since = new Date($('#month_social_metrics_cult')[0].value+'-28T00:00:00');
    var until = new Date($('#month_social_metrics_cult')[0].value+'-28T00:00:00');
    until.setDate(until.getDate() + 5);
    $.ajax({
		url: "https://graph.facebook.com/v2.12/techcult?fields=insights.metric(page_impressions_unique).fields(values).period(days_28).since("+since.toISOString().substr(0,10)+").until("+until.toISOString().substr(0,10)+")&access_token="+pageTokens['Tech Cult'],
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var reach = response.insights.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
	    	document.getElementById("reach_cult").innerHTML = numberWithCommas(reach[max].value);
    	}
    });
    //VTR clicked
    var first = new Date($('#month_social_metrics_cult')[0].value.substr(0,4), parseInt($('#month_social_metrics_cult')[0].value.substr(5,7))-1, 28);
	var last = new Date($('#month_social_metrics_cult')[0].value.substr(0,4), parseInt($('#month_social_metrics_cult')[0].value.substr(5,7)), 2);
    FB.api(
		'/techcult',
		'GET',
		{"fields":"insights.metric(page_consumptions_by_consumption_type).fields(values).as(views).period(days_28),insights.metric(page_impressions).fields(values).as(imp).period(days_28)",since: first.toISOString().substr(0,10), until: last.toISOString().substr(0,10),access_token: pageTokens['Tech Cult']},
		function(response) {
			var VTR=[];
			var imp=response.imp.data[0].values;
			var views=response.views.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value['video play']/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_cult_click").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
	//VTR views
	FB.api(
		'/techcult/insights',
		'GET',
		{"metric":"['page_video_views','page_impressions']","period":"days_28","since":first.toISOString().substr(0,10),"until": last.toISOString().substr(0,10),access_token: pageTokens['Tech Cult']},
		function(response) {
			var VTR=[];
			var imp=response.data[1].values;
			var views=response.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_cult").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
}
function nextPageCult(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageCult(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$("#shares_cult")[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}

function getSocialMetricsHolixir(){
	//Shares
	var now = new Date();
	var first = new Date($('#month_social_metrics_holixir')[0].value.substr(0,4), parseInt($('#month_social_metrics_holixir')[0].value.substr(5,7))-1, 1);
	var last = new Date($('#month_social_metrics_holixir')[0].value.substr(0,4), parseInt($('#month_social_metrics_holixir')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/holixir/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",access_token:pageTokens['Holixir']},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				nextPageHolixir(response.paging.next,posts);
           }else{
               var shares=0;
               for(var j=0;j<posts.length;j++){
                   if(typeof posts[j].shares !== 'undefined'){
                       shares+=posts[j].shares.count;
                       //console.log(posts[j].shares.count);
                   }
               }
               $("#shares_holixir")[0].innerHTML = numberWithCommas(shares);
           }
		}
	);
	//Video Views
	var now = new Date();
	var first = new Date($('#month_social_metrics_holixir')[0].value.substr(0,4), parseInt($('#month_social_metrics_holixir')[0].value.substr(5,7))-1, 2);
	var last = new Date($('#month_social_metrics_holixir')[0].value.substr(0,4), parseInt($('#month_social_metrics_holixir')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24*2);
	document.getElementById('video_views_page').innerHTML = me.innerHTML;
	FB.api(
		'/holixir/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",access_token: pageTokens['Holixir']},
		function(response) {
			//console.log(response);
			var videos=[];
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				//console.log(response.data[0].values[i].value,total_videos);
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('video_views_holixir').innerHTML = numberWithCommas(total_videos);
		}
	);
	//Reach
	var since = new Date($('#month_social_metrics_holixir')[0].value+'-28T00:00:00');
    var until = new Date($('#month_social_metrics_holixir')[0].value+'-28T00:00:00');
    until.setDate(until.getDate() + 5);
    $.ajax({
		url: "https://graph.facebook.com/v2.12/holixir/insights/page_impressions_unique/days_28?since="+since.toISOString().substr(0,10)+"&until="+until.toISOString().substr(0,10)+"&fields=values"+"&access_token="+pageTokens['Holixir'],
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var reach = response.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
	    	document.getElementById("reach_holixir").innerHTML = numberWithCommas(reach[max].value);
    	}
    });
    //VTR clicked
    var first = new Date($('#month_social_metrics_holixir')[0].value.substr(0,4), parseInt($('#month_social_metrics_holixir')[0].value.substr(5,7))-1, 28);
	var last = new Date($('#month_social_metrics_holixir')[0].value.substr(0,4), parseInt($('#month_social_metrics_holixir')[0].value.substr(5,7)), 2);
    FB.api(
		'/holixir',
		'GET',
		{"fields":"insights.metric(page_consumptions_by_consumption_type).fields(values).as(views).period(days_28).since("+first.toISOString().substr(0,10)+")"+".until("+last.toISOString().substr(0,10)+"),insights.metric(page_impressions).fields(values).as(imp).period(days_28).since("+first.toISOString().substr(0,10)+")"+".until("+last.toISOString().substr(0,10)+")",access_token:pageTokens['Holixir']},
		function(response) {
			var VTR=[];
			var imp=response.imp.data[0].values;
			var views=response.views.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value['video play']/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_holixir_click").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
	//VTR views
	FB.api(
		'/holixir/insights',
		'GET',
		{"metric":"['page_video_views','page_impressions']","period":"days_28","since":first.toISOString().substr(0,10),"until": last.toISOString().substr(0,10),access_token:pageTokens['Holixir']},
		function(response) {
			var VTR=[];
			var imp=response.data[1].values;
			var views=response.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_holixir").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
}
function nextPageHolixir(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageHolixir(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$("#shares_holixir")[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}

function getSocialMetricsSofanimo(){
	//Shares
	var now = new Date();
	var first = new Date($('#month_social_metrics_sofanimo')[0].value.substr(0,4), parseInt($('#month_social_metrics_sofanimo')[0].value.substr(5,7))-1, 1);
	var last = new Date($('#month_social_metrics_sofanimo')[0].value.substr(0,4), parseInt($('#month_social_metrics_sofanimo')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/sofanimo/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				nextPageSofanimo(response.paging.next,posts);
           }else{
               var shares=0;
               for(var j=0;j<posts.length;j++){
                   if(typeof posts[j].shares !== 'undefined'){
                       shares+=posts[j].shares.count;
                       //console.log(posts[j].shares.count);
                   }
               }
               $("#shares_sofanimo")[0].innerHTML = numberWithCommas(shares);
           }
		}
	);
	//Video Views
	var now = new Date();
	var first = new Date($('#month_social_metrics_sofanimo')[0].value.substr(0,4), parseInt($('#month_social_metrics_sofanimo')[0].value.substr(5,7))-1, 2);
	var last = new Date($('#month_social_metrics_sofanimo')[0].value.substr(0,4), parseInt($('#month_social_metrics_sofanimo')[0].value.substr(5,7)), 0);
	last = new Date(last.getTime() + 1000*60*60*24*2);
	document.getElementById('video_views_page').innerHTML = me.innerHTML;
	FB.api(
		'/sofanimo/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100",access_token:pageTokens['Sofanimo']},
		function(response) {
			//console.log(response);
			var videos=[];
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				//console.log(response.data[0].values[i].value,total_videos);
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('video_views_sofanimo').innerHTML = numberWithCommas(total_videos);
		}
	);
	//Reach
	var since = new Date($('#month_social_metrics_sofanimo')[0].value+'-28T00:00:00');
    var until = new Date($('#month_social_metrics_sofanimo')[0].value+'-28T00:00:00');
    until.setDate(until.getDate() + 5);
    $.ajax({
		url: "https://graph.facebook.com/v2.12/sofanimo/insights/page_impressions_unique/days_28?since="+since.toISOString().substr(0,10)+"&until="+until.toISOString().substr(0,10)+"&fields=values"+"&access_token="+pageTokens['Sofanimo'],
	  	type: 'get',
	    dataType: 'json',
	    async: false,
	    success: function(response){
	    	var reach = response.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
	    	document.getElementById("reach_sofanimo").innerHTML = numberWithCommas(reach[max].value);
    	}
    });
    //VTR clicked
    var first = new Date($('#month_social_metrics_sofanimo')[0].value.substr(0,4), parseInt($('#month_social_metrics_sofanimo')[0].value.substr(5,7))-1, 28);
	var last = new Date($('#month_social_metrics_sofanimo')[0].value.substr(0,4), parseInt($('#month_social_metrics_sofanimo')[0].value.substr(5,7)), 2);
    FB.api(
		'/sofanimo',
		'GET',
		{"fields":"insights.metric(page_consumptions_by_consumption_type).fields(values).as(views).period(days_28).since("+first.toISOString().substr(0,10)+")"+".until("+last.toISOString().substr(0,10)+"),insights.metric(page_impressions).fields(values).as(imp).period(days_28).since("+first.toISOString().substr(0,10)+")"+".until("+last.toISOString().substr(0,10)+")",access_token:pageTokens['Sofanimo']},
		function(response) {
			var VTR=[];
			var imp=response.imp.data[0].values;
			var views=response.views.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value['video play']/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_sofanimo_click").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
	//VTR views
	FB.api(
		'/sofanimo/insights',
		'GET',
		{"metric":"['page_video_views','page_impressions']","period":"days_28","since":first.toISOString().substr(0,10),"until": last.toISOString().substr(0,10),access_token:pageTokens['Sofanimo']},
		function(response) {
			var VTR=[];
			var imp=response.data[1].values;
			var views=response.data[0].values;
			for(var i=0;i<imp.length;i++){
				VTR.push(views[i].value/imp[i].value*100);
			}
			var max = VTR.indexOf(Math.max(...VTR));
			document.getElementById("avg_vtr_sofanimo").innerHTML = numberWithCommas(VTR[max].toFixed(2))+'%';
		}
	);
}
function nextPageSofanimo(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageSofanimo(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$("#shares_sofanimo")[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}
//Social Month Metrics KPI's

//Index avatar leaderboard
var wp_posts_authors;
function getLiveWPPostsAuthors(){
	$.ajax({
		url: 'https://eldeforma.com/api/get_recent_posts/?include=author,url,slug&count=200',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			var wp_posts=data.posts;
			
			wp_posts_authors = wp_posts.map(function(obj) { 
			   var rObj = {};
			   rObj[obj.slug] = obj.author.nickname;
			   return rObj;
			});
		}
	});
}
function getLiveTopPostAuthors(urls){
	//console.log(urls);
	
	var top =[];

	for(var i=0;i<urls.length;i++){
		if(urls[i][0].indexOf('p1=true') != -1 || urls[i][0].indexOf('promoted=true') != -1 || urls[i][0].indexOf('GUA=1') != -1 || urls[i][0].indexOf('SRE=1') != -1 || urls[i][0].indexOf('GUIA=1') != -1 || urls[i][0].indexOf('CST=1') != -1){
			
		}
		else{
			top.push(urls[i]);
		}
	}

	var link;
	var slug;
	var author;
	
	for(var j=0;j<4;j++){
		link = top[j][0];
		slug = link.substr(12,link.length).substr(0,link.substr(12,link.length).indexOf('/'));
	
		for(var i=0;i<wp_posts_authors.length;i++){
			if(typeof wp_posts_authors[i][slug] !== 'undefined'){
				author = wp_posts_authors[i][slug];
				break;
			}
		}
	
		$('#no'+j)[0].src = 'assets/images/avatars/'+author.replace(' ','_').replace('.','')+'.png';
        $("#no"+j+"_status").html(getAvatarName(author,j));
		if(top[j][1]>1500){
			$('#box_'+j).addClass('rainbow');
		}
		else{
			$('#box_'+j).removeClass('rainbow');
		}
	}
}
function getAvatarName(author,position){
    //console.log(author);
    /*switch(author){
        case "PAQ":
            return "I am Petroot";
            break;
        case "aldeni":
            return "Iron Miaden";
            break;
        case "Rulo":
            return "Ruax";
            break;
        case "jovinhoestrada":
            return "Jürgen Strange";
            break;
        case "Gazapotl":
            return "Thorlaloc";
            break;
        case "Yado":
            return "Yairon spider";
            break;
        case "Alan":
            return "T'Chalan";
            break;
        case "El Deforma":
            return "Jake Fury";
            break;
        default:*/
            switch(position){
                case 0:
                    return '<i class="zmdi zmdi-star" style="color: #FFD700; font-size: 30px;"></i>';
                    break;
                case 1:
                    return '<i class="zmdi zmdi-cocktail" style="color: #C0C0C0; font-size: 30px;"></i>';
                    break;
                case 2:
                    return '<i class="zmdi zmdi-pizza" style="color: #CD7F32; font-size: 30px;"></i>';
                    break;
                case 3:
                    return '<i class="zmdi zmdi-thumb-up" style="color: #000000; font-size: 30px;"></i>';
                    break;
                default:
                    return '<i class="zmdi zmdi-thumb-up" style="color: #000000; font-size: 30px;"></i>';
                    break;
            }
            //break;
    //}
}
//Index avatar Leaderboard

//unpublished posts promote
var access_tokens = [];
var selected_unpublished_post;
function getUnpublishedPosts(){
    access_tokens = [];
	var selector=document.getElementById("unpublished-posts-select");
	while(selector.length>0){
		selector.remove(0);
	}
    
    FB.api(
        '/me/accounts',
        'GET',
        {},
        function(response) {
            for(var i=0;i<response.data.length;i++){
                access_tokens.push({
                    page: response.data[i].name,
                    access_token: response.data[i].access_token,
                    id: response.data[i].id
                })
            }
            populateUnpublishedPosts(0);
        }
    );
    
    function populateUnpublishedPosts(I){
        FB.api(
            '/'+access_tokens[I].id+'/scheduled_posts',
            'GET',
            {"fields":"id,name,message,scheduled_publish_time",access_token: access_tokens[I].access_token},
            function(resp) {
               //console.log(resp);
                if(typeof resp.data !== 'undefined'){
                   for(var p=0;p<resp.data.length;p++){
                        if(typeof resp.data[p].scheduled_publish_time === 'undefined'){
                            resp.data.splice(p,1);
                        }
                    }
                    if(resp.data.length>0){
                        var posts_unpublished=[];
                        var selector=document.getElementById("unpublished-posts-select");
                        for(var k=0;k<resp.data.length;k++){
                            posts_unpublished.push(resp.data[k]);
                        }
                        var option = document.createElement("option");
                        option.text = "----------- "+access_tokens[I].page+" -----------";
                        option.value = "";
                        option.disabled = true;
                        selector.add(option);
                        for(var j=0;j<posts_unpublished.length;j++){
                            if(typeof posts_unpublished[j].scheduled_publish_time !== 'undefined'){
                            var option = document.createElement("option");
                            option.text = posts_unpublished[j].name == undefined ? (posts_unpublished[j].message == undefined ? 'post '+access_tokens[I].page+'...'+'\t'+new Date(posts_unpublished[j].scheduled_publish_time*1000).toLocaleString() : posts_unpublished[j].message.substr(0,30)+'...'+'\t'+new Date(posts_unpublished[j].scheduled_publish_time*1000).toLocaleString() ) : posts_unpublished[j].name.substr(0,30)+'...'+'\t'+new Date(posts_unpublished[j].scheduled_publish_time*1000).toLocaleString();
                            option.value = posts_unpublished[j].id;
                            selector.add(option);
                            }
                        }
                    }
                    if(I+1 < access_tokens.length){
                        populateUnpublishedPosts(I+1);
                    }
               }else{
                   if(I+1 < access_tokens.length){
                        populateUnpublishedPosts(I+1);
                   }
               }
            }
        );
    }
}
function getUnpublishedPostId(){
	getPromotePostId($('#unpublished-posts-select').val());
    $.ajax({
        url: 'https://graph.facebook.com/v2.12/'+$('#unpublished-posts-select').val()+'?fields=type,name,created_time'+"&"+access_token,
		async: false,
		method: 'get',
		success: function(response){
			selected_unpublished_post=response;
		}
    });
}
//unpublished posts promote

//posts quickview
var data_quickview=[];
var metric_quickview=0;
var formating_quickview;
var quickview_currency;
var quickview_post_id;
var quickview_campaign;
function UpdateDataQuickview(){
	var time_lapse = document.getElementById("time_lapse_quickview").value;
	$.ajax({
		url: "php_scripts/get_post_data.php",
		dataType: "json",
		data: {post_id: quickview_post_id, time_lapse: time_lapse, campaign: quickview_campaign},
		method: 'get',
		async: false,
		success: function(response){
			data_quickview=response;
			for(var g=0;g<data_quickview.length;g++)
			{
				if(parseFloat(data_quickview[g].spend)>0){
					var paid_cels = document.getElementsByClassName("paid");
					for(var i=0;i<paid_cels.length;i++){
						paid_cels.item(i).style.visibility = "visible";
					}
					break;
				}
				else{
					var paid_cels = document.getElementsByClassName("paid");
					for(var i=0;i<paid_cels.length;i++){
						paid_cels.item(i).style.visibility = "hidden";
					}
				}
			}
		}
	});
	$.ajax({
		url: "php_scripts/get_post_currency.php",
		dataType: "text",
		data: {post_id: quickview_post_id},
		method: 'get',
		async: false,
		success: function(response){
			quickview_currency=response;
		}
	});
}
function selectPostQuickview(post_id, name, campaign){
	quickview_post_id = post_id;
	quickview_campaign = campaign;
	$('#quickview_title')[0].innerHTML = name;
	$('#quickview_post_id')[0].innerHTML = post_id;
	UpdateDataQuickview();
	drawChartQuickview(metric_quickview);
}
function drawChartQuickview(measure){
  metric_quickview=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(quickview_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', 'Gasto');
		break;
	case 2:
		data_table.addColumn('number', 'CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', 'Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', 'Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', 'Penetration');
		break;
	case 6:
		data_table.addColumn('number', 'CXW');
		break;
	case 7:
		data_table.addColumn('number', 'CPM');
		break;
	case 8:
		data_table.addColumn('number', 'CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', 'Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', 'Shares');
		break;
	case 11:
		data_table.addColumn('number', 'Likes');
		break;
	case 12:
		data_table.addColumn('number', 'Comments');
		break;
	case 13:
		data_table.addColumn('number', 'Link CPM');
		break;
	case 14:
		data_table.addColumn('number', 'RT CXW');
		break;
	default:
		data_table.addColumn('number', 'CTR Organico');
		break;
  }

  for(var n=0;n<data_quickview.length;n++){
	data_quickview[n].record_time=data_quickview[n].record_time.replace(' ','T');
	data_quickview[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_quickview[n].record_time)

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].spend)*currency_factor]);
		  formating_quickview = {
			title: 'Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_quickview[n].organic_ctr)/100, f: data_quickview[n].organic_ctr+'%'}]);
		  formating_quickview = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].paid_link_clicks)]);
		  formating_quickview = {
			title: 'Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].viral_amp), f: data_quickview[n].viral_amp+'x'}]);
		  formating_quickview = {
			title: 'Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].penetration)/100, f: data_quickview[n].penetration+'%'}]);
		  formating_quickview = {
			title: 'Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].cxw)*currency_factor]);
		  formating_quickview = {
			title: 'CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].cpm)*currency_factor]);
		  formating_quickview = {
			title: 'CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].paid_ctr)/100, f:data_quickview[n].paid_ctr+'%'}]);
		  formating_quickview = {
			title: 'CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].link_clicks)]);
		  formating_quickview = {
			title: 'Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].shares)]);
		  formating_quickview = {
			title: 'Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].likes)]);
		  formating_quickview = {
			title: 'Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].comments)]);
		  formating_quickview = {
			title: 'Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].link_cpm)*currency_factor]);
		  formating_quickview = {
			title: 'Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 14:
		  if(n<data_quickview.length-1){
			  var rt_cxw=(parseFloat(data_quickview[n+1].spend)-parseFloat(data_quickview[n].spend))/(parseFloat(data_quickview[n+1].paid_link_clicks)-parseFloat(data_quickview[n].paid_link_clicks))*currency_factor;
			  data_table.addRow([
				  data_date,
				  rt_cxw
			  ]);
			  formating_quickview = {
				title: 'RT CXW',
				//curveType: 'function',
				vAxis: {
					format: 'currency'
				},
				hAxis: {
					format: 'M/d HH:mm',
					gridlines:{
						count: -1
					},
					title: 'Hora'
				},
				legend:{
					position: 'none'
				},
				pointSize: 3,
				trendlines: { 
					0: {
						lineWidth: 10,
						color: 'green',
						opacity: 1
					} 
				}
			  };
		  }
		  break;
	  default:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].organic_ctr)/100, f: data_quickview[n].organic_ctr+'%'}]);
		  formating_quickview = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_quickview'));

  table.draw(data_table,formating_quickview);
}
function drawDeltaChartQuickview(measure){
  metric_quickview=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(quickview_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', '\u0394 Gasto');
		break;
	case 2:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', '\u0394 Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', '\u0394 Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', '\u0394 Penetration');
		break;
	case 6:
		data_table.addColumn('number', '\u0394 CXW');
		break;
	case 7:
		data_table.addColumn('number', '\u0394 CPM');
		break;
	case 8:
		data_table.addColumn('number', '\u0394 CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', '\u0394 Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', '\u0394 Shares');
		break;
	case 11:
		data_table.addColumn('number', '\u0394 Likes');
		break;
	case 12:
		data_table.addColumn('number', '\u0394 Comments');
		break;
	case 13:
		data_table.addColumn('number', '\u0394 Link CPM');
		break;
	case 14:
		data_table.addColumn('number', '\u0394 RT CXW');
		break;
	default:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
  }

  for(var n=1;n<data_quickview.length;n++){
	data_quickview[n].record_time=data_quickview[n].record_time.replace(' ','T');
	data_quickview[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_quickview[n].record_time)

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].spend)*currency_factor-parseFloat(data_quickview[n-1].spend)*currency_factor]);
		  formating_quickview = {
			title: '\u0394 Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_quickview[n].organic_ctr)/100-parseFloat(data_quickview[n-1].organic_ctr)/100, f: (parseFloat(data_quickview[n].organic_ctr)-parseFloat(data_quickview[n-1].organic_ctr))+'%'}]);
		  formating_quickview = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].paid_link_clicks)-parseFloat(data_quickview[n-1].paid_link_clicks)]);
		  formating_quickview = {
			title: '\u0394 Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].viral_amp)-parseFloat(data_quickview[n-1].viral_amp), f: (parseFloat(data_quickview[n].viral_amp)-parseFloat(data_quickview[n-1].viral_amp))+'x'}]);
		  formating_quickview = {
			title: '\u0394 Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].penetration)/100-parseFloat(data_quickview[n-1].penetration)/100, f: (parseFloat(data_quickview[n].penetration)-parseFloat(data_quickview[n-1].penetration))+'%'}]);
		  formating_quickview = {
			title: '\u0394 Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,(parseFloat(data_quickview[n].cxw)-parseFloat(data_quickview[n-1].cxw))*currency_factor]);
		  formating_quickview = {
			title: '\u0394 CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,(parseFloat(data_quickview[n].cpm)-parseFloat(data_quickview[n-1].cpm))*currency_factor]);
		  formating_quickview = {
			title: '\u0394 CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].paid_ctr)/100-parseFloat(data_quickview[n-1].paid_ctr)/100, f: (parseFloat(data_quickview[n].paid_ctr)-parseFloat(data_quickview[n-1].paid_ctr))+'%'}]);
		  formating_quickview = {
			title: '\u0394 CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].link_clicks)-parseFloat(data_quickview[n-1].link_clicks)]);
		  formating_quickview = {
			title: '\u0394 Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].shares)-parseFloat(data_quickview[n-1].shares)]);
		  formating_quickview = {
			title: '\u0394 Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].likes)-parseFloat(data_quickview[n-1].likes)]);
		  formating_quickview = {
			title: '\u0394 Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].comments)-parseFloat(data_quickview[n-1].comments)]);
		  formating_quickview = {
			title: '\u0394 Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,(parseFloat(data_quickview[n].link_cpm)-parseFloat(data_quickview[n-1].link_cpm))*currency_factor]);
		  formating_quickview = {
			title: '\u0394 Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  default:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].organic_ctr)/100-parseFloat(data_quickview[n-1].organic_ctr)/100, f: (parseFloat(data_quickview[n].organic_ctr)-parseFloat(data_quickview[n].organic_ctr))+'%'}]);
		  formating_quickview = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_quickview'));

  table.draw(data_table,formating_quickview);
}
//posts quickview

//Edit Post
function wpPostEdit(id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link;
			var slug = link.substr(32,link.length).substr(0,link.substr(32,link.length).indexOf('/'));
			
			getPostIdForEdit(slug);
			//removeInstant(link);
		}
	);
}
function getPostIdForEdit(slug){
	$.ajax({
		url: 'https://eldeforma.com/api/get_post/?slug='+slug+'&include=id',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			if(!data.hasOwnProperty('error')){
				console.log(data.post.id);
				window.open('https://eldeforma.com/wp-admin/post.php?post='+data.post.id+'&action=edit');
			}else{
				alert('find post: '+data.error);
			}
		}
	});
}
//Edit Post

//Rules Datacenter
function loadData_rules(){
	var ordered_rules={};
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestADXRulesRange = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: rules_from.value,
			endDate: rules_to.value,			
			dimension: ["WINNING_BID_RULE_NAME","DATE"],
			metric: ["AD_REQUESTS_COVERAGE","Earnings"],
			sort: ["WINNING_BID_RULE_NAME","DATE"],
			fields: "rows"
		});
		requestADXRulesRange.execute(function(resp) {
			//console.log(resp);
			var rules=resp.rows;
			for(var i=0;i<rules.length;i++){
				ordered_rules[rules[i][0]] = [];
			}
			for(var i=0;i<rules.length;i++){
				ordered_rules[rules[i][0]].push({'date': rules[i][1],'coverage': parseFloat(rules[i][2])*100,'revenue': parseFloat(rules[i][3])});
			}
			//console.log(ordered_rules);
			drawChart_rules(ordered_rules)
		});
	});
}
function drawChart_rules(rules) {
	//console.log(rules);
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Rule');
	data.addColumn('number', 'Previous Revenue');
	data.addColumn('number', 'Previous Coverage');
	data.addColumn('number', 'Latest Revenue');
	data.addColumn('number', 'Latest Coverage');
	
	$.each(rules, function(key, value) {
		data.addRow([key+' '+value[0].date,value[0].revenue,value[0].coverage,value[value.length-1].revenue,value[value.length-1].coverage]);
	});
	
	var options = {
	  chart: {
		title: 'ADX Rules',
		subtitle: 'Revenue vs. coverage'
	  },
	  series: {
		0: {targetAxisIndex: 0, pointShape: 'star', pointSize: 12},
		1: {targetAxisIndex: 1, pointShape: 'square', pointSize: 12},
		2: {targetAxisIndex: 0, pointShape: 'star', pointSize: 12},
		3: {targetAxisIndex: 1, pointShape: 'square', pointSize: 12}
	  },
	  vAxes: {
	  	0: {
			viewWindowMode:'explicit',
			viewWindow:{
				max:5000,
				min:0
			}
		},
		1: {
			viewWindowMode:'explicit',
			viewWindow:{
				max:100,
				min:0
			}
		}
	  },
	  hAxis: {gridlines: {count: 30}},
	  colors: ['red','red','green','green']
	  /*axes: {
		y: {
		  'previous revenue': {label: 'Previous Revenue'},
		  'previous coverage': {label: 'Previous Coverage'},
		  'latest revenue': {label: 'Latest Revenue'},
		  'latest coverage': {label: 'Latest Coverage'}
		}
	  }*/
	};

	var chart = new google.visualization.ScatterChart(document.getElementById('chart_div_rules'));	
	
	chart.draw(data, options);
}
//Rules Datacenter

//Organic Bonus
var bonus_videos_deforma, bonus_videos_repsodia, bonus_videos_techcult;
var total_bonus_organic, total_bonus_comercial, total_bonuses;
function getMonthBonus(){
    total_bonus_organic=0; 
    total_bonus_comercial=0; 
    total_bonuses=0;
	getArticlesWPBonus();
}
function getArticlesWPBonus(){
	gapi.client.load('analytics','v3');

	$.ajax({
		url: 'https://eldeforma.com/?json=get_date_posts&date='+bonus_month.value+'&count=800&status=published&include=author,url,date,status,categories,title',
		dataType: 'jsonp',
		async: false,
		success: function(response){

			bonus_posts=response.posts;

			//i=posts.length-1;
			if(bonus_posts.length>0){
				getPageviewsBonus();
			}

		}
	});
}
function getPageviewsBonus(){
	var now = new Date();
    var this_month = new Date(new Date($('#bonus_month').val()+'-01').getTime()+new Date().getTimezoneOffset()*1000*60);
    var next_month_2days = new Date(this_month.getFullYear(),this_month.getMonth()+1,2);
	var requestPageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": $('#bonus_month').val()+'-01',
		"end-date": next_month_2days.toISOString().substr(0,10),
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath",
		filters: "ga:pagePath=@"+bonus_month.value.replace("-","/")+";ga:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@SRE=1;ga:pagePath!@GUIA=1;ga:pagePath!@CST=1",
		fields: 'rows',
		sort: '-ga:pageviews'
	});
	requestPageviews.execute(function(resp){
		var pages=resp.rows;
		//console.log(resp);  

		for(var i=0;i<bonus_posts.length;i++){
			bonus_posts[i].pageviews=0;
			for(var j=0;j<pages.length;j++){
				if(bonus_posts[i].url.indexOf(pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/'))) > -1 && pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/')).length >= 6){
					bonus_posts[i].pageviews+=parseInt(pages[j][1]);
				}
			}
		}
		sortPostsBonus();
	});
}
function sortPostsBonus(){
	
	for(var i=0;i<bonus_posts.length;i++){
		var nickname = bonus_posts[i].author.nickname;
		bonus_authors[nickname] = [];
	}

	for(var i=0;i<bonus_posts.length;i++){
		if(bonus_posts[i].pageviews >= 80000){
			bonus_authors[bonus_posts[i].author.nickname].push({"title": bonus_posts[i].title, "pageviews": bonus_posts[i].pageviews, "FullBonus": FullBonus(bonus_posts[i].categories), "type": postType(bonus_posts[i].categories), url: bonus_posts[i].url});
		}
	}
	
	$.each(bonus_authors, function(key, value) {
		//console.log(authors[key]);
		bonus_authors[key].bonus=0
		for(var j=0;j<bonus_authors[key].length;j++)
		{
			if(bonus_authors[key][j].FullBonus){
				bonus_authors[key].bonus+=1000;
			}else{
				bonus_authors[key].bonus+=500;
			}
		}
	});
	
	drawTableLeaderboardBonus();
}
function drawTableLeaderboardBonus(){
	var bonus_data = new google.visualization.DataTable();
	var bonus_posts = new google.visualization.DataTable();
  
	bonus_data.addColumn({type: 'string', label: 'Autor'});
	bonus_data.addColumn({type: 'number', label: 'Bonus'});
	
	bonus_posts.addColumn({type: 'string', label: 'Autor'});
	bonus_posts.addColumn({type: 'string', label: 'Nota'});
	bonus_posts.addColumn({type: 'number', label: 'Pageviews'});
	bonus_posts.addColumn({type: 'string', label: 'Categoria'});
  
	var total_bonus=0;
	$.each(bonus_authors, function(key, value) {
		var Office = ["Yado","Gazapotl","PAQ","Pamela","Alan","Dany Trejo","Maestro Bergstrom","jovinhoestrada","majo","Lui","Rulo", "Nanu", "aldeni","Are"];
		if($.inArray(key,Office) != -1 && bonus_authors[key].bonus > 0){
			bonus_data.addRow([
				key,
				{v: bonus_authors[key].bonus, f: '$ '+numberWithCommas(bonus_authors[key].bonus)}
			]);
			total_bonus+=bonus_authors[key].bonus;
			for(var i=0;i<bonus_authors[key].length;i++){
				bonus_posts.addRow([
				key,
				(bonus_authors[key][i].title).link(bonus_authors[key][i].url),
				{v: bonus_authors[key][i].pageviews, f: numberWithCommas(bonus_authors[key][i].pageviews)},
				bonus_authors[key][i].type
			]);
			}
		}
		else{
		
		}
	});
	bonus_data.addRow([
		'',
		null
	]);
	bonus_data.addRow([
		'Total',
		{v: total_bonus, f: '$ '+numberWithCommas(total_bonus)}
	]);

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var bonus_table = new google.visualization.Table(document.getElementById('chart_div_bonus_authors'));
	var posts_table = new google.visualization.Table(document.getElementById('chart_div_bonus_posts'));
  
  	google.visualization.events.addListener(posts_table, 'ready', function () {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
        $('#notifySlackBonus').show();
        $('#printTemplate').show();
	});
	
	google.visualization.events.addListener(posts_table, 'sort', function() {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var o=0;o < links.length;o++){
			links[o].setAttribute('target','_blank');
		}
	});
  
	bonus_table.draw(bonus_data, options);
	posts_table.draw(bonus_posts, options);
    
    total_bonus_organic+=total_bonus;
    total_bonuses+=total_bonus;
    
    getDeformaVideoBonus();
	//console.log("done");
}
function FullBonus(categories){
	if(typeof categories === 'undefined'){
		return true;
	}
	for(var i=0;i<categories.length;i++){
		if(categories[i].slug == 'mis-memes'){
			return true;
		}
	}
	for(var i=0;i<categories.length;i++){
		if(categories[i].slug == 'videos' || categories[i].slug == 'la-duda' || categories[i].slug == 'memes'){
			return false;
		}
	}
	return true;
}
function postType(categories){
	if(typeof categories === 'undefined'){
		return 'Nota';
	}
	for(var i=0;i<categories.length;i++){
		if(categories[i].slug == 'mis-memes'){
			return 'Mis Memes';
		}
	}
	for(var i=0;i<categories.length;i++){
		if(categories[i].slug == 'videos'){
			return 'Video';
		}
		if(categories[i].slug == 'la-duda'){
			return 'IPC';
		}
		if(categories[i].slug == 'memes'){
			return 'Memes';
		}
	}
	return 'Nota';
}
function getDeformaVideoBonus(){
	var firstDay = new Date(new Date(bonus_month.value+"-01").getTime()+(new Date().getTimezoneOffset()+60)*60*1000), y = firstDay.getFullYear(), m = firstDay.getMonth();
	var lastDay = new Date(y, m + 1, 1);
	var bonus_deforma_videos=[];
	FB.api(
	  '/eldeforma/posts',
	  'GET',
	  {
		"fields":"type,created_time,message,link,name,object_id,insights.metric(post_video_views_organic).period(lifetime).fields(values)",
		"since":firstDay.toISOString().substr(0,10)+"T06:00",
		"until":lastDay.toISOString().substr(0,10)+"T06:00",
		"limit":"100",
        "access_token": pageTokens['El Deforma']
	  },
	  function(response) {
	    //console.log(response);
		for(var i=0;i<response.data.length;i++){
            //console.log(response.data[i].insights.data[0].values[0].value);
			if(response.data[i].insights.data[0].values[0].value > 1000000){
				if(response.data[i].name !== '' && typeof response.data[i].name !== 'undefined'){
					bonus_deforma_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/Eldeforma/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].name, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
				}else if(response.data[i].message !== '' && typeof response.data[i].message !== 'undefined'){
					bonus_deforma_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/Eldeforma/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].message, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
				}else{
					bonus_deforma_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/Eldeforma/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": 'Video Deforma', "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
				}
			}
		}
		
		//console.log(bonus_deforma_videos);
		if(typeof response.paging.next !== 'undefined'){
			nextPageBonusVideosDeforma(response.paging.next,bonus_deforma_videos);
		}else{
            //Sort Video Author
            for(var i=0;i<bonus_deforma_videos.length;i++){
                getVideosAuthor(bonus_deforma_videos[i]);   
            }
            //Sort Video Author
            setTimeout(function(){drawTableLeaderboardDeformaBonus(bonus_deforma_videos)},5000);
        }
	   }
	);
}
function nextPageBonusVideosDeforma(nextURL,videos){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
                //console.log(response.data[i].insights.data[0].values[0].value);
				if(response.data[i].insights.data[0].values[0].value > 1000000){
                    //console.log(response.data[i]);
					if(response.data[i].name !== '' && typeof response.data[i].name !== 'undefined'){
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/eldeforma/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].name, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}else if(response.data[i].message !== '' && typeof response.data[i].message !== 'undefined'){
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/eldeforma/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].message, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}else{
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/eldeforma/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": 'Video Deforma', "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}
				}
			}
			//console.log(videos);
			
			if(typeof response.paging.next !== 'undefined'){
				nextPageBonusVideosDeforma(response.paging.next,videos);
			}
			else{
                //Sort Video Author
                for(var i=0;i<videos.length;i++){
                    getVideosAuthor(videos[i]);   
                }
                //Sort Video Author
				setTimeout(function(){drawTableLeaderboardDeformaBonus(videos)},5000);
			}
		}
	});
}
function drawTableLeaderboardDeformaBonus(videos){
    var bonus_data_deforma = new google.visualization.DataTable();
    
	bonus_data_deforma.addColumn({type: 'string', label: 'Video'});
	bonus_data_deforma.addColumn({type: 'number', label: 'Bonus'});
    bonus_data_deforma.addColumn({type: 'string', label: 'Autor'});
  
  	for(var i=0;i<videos.length;i++){
  		bonus_data_deforma.addRow([
			(videos[i].Titulo).link(videos[i].link),
			{v: videos[i].views, f: numberWithCommas(videos[i].views)},
            videos[i].author.join(", ")
		]);
  	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var bonus_table_deforma = new google.visualization.Table(document.getElementById('chart_div_bonus_video_deforma'));
  
  	google.visualization.events.addListener(bonus_table_deforma, 'ready', function () {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
        bonus_videos_deforma=videos;
	});
	
	google.visualization.events.addListener(bonus_table_deforma, 'sort', function() {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var o=0;o < links.length;o++){
			links[o].setAttribute('target','_blank');
		}
	});
  
	bonus_table_deforma.draw(bonus_data_deforma, options);
    
    total_bonus_organic+=videos.length*1000;
    total_bonuses+=videos.length*1000;
	//console.log("done");
    
    getRepsodiaVideoBonus();
}
function getRepsodiaVideoBonus(){
	var firstDay = new Date(new Date(bonus_month.value+"-01").getTime()+(new Date().getTimezoneOffset()+60)*60*1000), y = firstDay.getFullYear(), m = firstDay.getMonth();
	var lastDay = new Date(y, m + 1, 1);
	var bonus_repsodia_videos=[];
	FB.api(
        '/repsodia/posts',
        'GET',
        {
            "fields":"shares,type,created_time,message,object_id,name,link,insights.metric(post_video_views_organic).period(lifetime).fields(values)",
            "since":firstDay.toISOString().substr(0,10)+"T06:00",
            "until":lastDay.toISOString().substr(0,10)+"T06:00",
            "limit":"100",
            "access_token": pageTokens['Repsodia']
        },
        function(response) {
            //console.log(response);
            for(var i=0;i<response.data.length;i++){
                //if(response.data[i].insights.data[0].values[0].value > 1000000){
                if(typeof response.data[i].shares === 'undefined'){response.data[i].shares = {count: 0};}
                if(response.data[i].insights.data[0].values[0].value > 500000 || response.data[i].shares.count >= 8000){
                    if(response.data[i].name !== '' && typeof response.data[i].name !== 'undefined'){
                        bonus_repsodia_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/repsodia/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].name, "views": response.data[i].insights.data[0].values[0].value, "shares": response.data[i].shares.count, "object_id": response.data[i].object_id});
                    }else if(response.data[i].message !== '' && typeof response.data[i].message !== 'undefined'){
                        bonus_repsodia_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/repsodia/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].message, "views": response.data[i].insights.data[0].values[0].value, "shares": response.data[i].shares.count, "object_id": response.data[i].object_id});
                    }else{
                        bonus_repsodia_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/repsodia/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": 'Video Repsodia', "views": response.data[i].insights.data[0].values[0].value, "shares": response.data[i].shares.count, "object_id": response.data[i].object_id});
                    }
                }
            }
            if(typeof response.paging.next !== 'undefined'){
                nextPageBonusVideosRepsodia(response.paging.next,bonus_repsodia_videos);
            }
            else{
                //Sort Video Author
                for(var i=0;i<bonus_repsodia_videos.length;i++){
                    getVideosAuthor(bonus_repsodia_videos[i]);   
                }
                //Sort Video Author
                setTimeout(function(){drawTableLeaderboardRepsodiaBonus(bonus_repsodia_videos)},5000);	
            }
        }
	);
}
function nextPageBonusVideosRepsodia(nextURL,videos){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				if(typeof response.data[i].shares === 'undefined'){response.data[i].shares = {count: 0};}
                if(response.data[i].insights.data[0].values[0].value > 500000 || response.data[i].shares.count >= 8000){
					if(response.data[i].name !== '' && typeof response.data[i].name !== 'undefined'){
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/repsodia/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].name, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}else if(response.data[i].message !== '' && typeof response.data[i].message !== 'undefined'){
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/repsodia/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].message, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}else{
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/repsodia/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": 'Video Deforma', "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}
				}
			}
			console.log(videos);
			
			if(typeof response.paging.next !== 'undefined'){
                //Sort Video Author
                for(var i=0;i<videos.length;i++){
                    getVideosAuthor(videos[i]);   
                }
                //Sort Video Author
                setTimeout(function(){drawTableLeaderboardRepsodiaBonus(videos)},5000);
			}
			else{
				drawTableLeaderboardRepsodiaBonus(videos);	
			}
		}
	});
}
function drawTableLeaderboardRepsodiaBonus(videos){
	var bonus_data_repsodia = new google.visualization.DataTable();
    
	bonus_data_repsodia.addColumn({type: 'string', label: 'Video'});
	bonus_data_repsodia.addColumn({type: 'number', label: 'Views'});
    bonus_data_repsodia.addColumn({type: 'number', label: 'Shares'});
    bonus_data_repsodia.addColumn({type: 'string', label: 'Autor'});
    
  
  	for(var i=0;i<videos.length;i++){
  		bonus_data_repsodia.addRow([
			(videos[i].Titulo).link(videos[i].link),
			{v: videos[i].views, f: numberWithCommas(videos[i].views)},
            {v: videos[i].shares, f: numberWithCommas(videos[i].shares)},
            videos[i].author.join(", ")
		]);
  	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var bonus_table_repsodia = new google.visualization.Table(document.getElementById('chart_div_bonus_video_repsodia'));
  
  	google.visualization.events.addListener(bonus_table_repsodia, 'ready', function () {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
        bonus_videos_repsodia=videos;
	});
	
	google.visualization.events.addListener(bonus_table_repsodia, 'sort', function() {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var o=0;o < links.length;o++){
			links[o].setAttribute('target','_blank');
		}
	});
  
	bonus_table_repsodia.draw(bonus_data_repsodia, options);
    
    total_bonus_organic+=videos.length*1000;
    total_bonuses+=videos.length*1000;
	//console.log("done");
    
    getTechCultVideoBonus();
}
function getTechCultVideoBonus(){
	var firstDay = new Date(new Date(bonus_month.value+"-01").getTime()+(new Date().getTimezoneOffset()+60)*60*1000), y = firstDay.getFullYear(), m = firstDay.getMonth();
	var lastDay = new Date(y, m + 1, 1);
	var bonus_cult_videos=[];
	FB.api(
	  '/techcult/posts',
	  'GET',
	  {
		"fields":"shares,type,created_time,message,name,link,object_id,insights.metric(post_video_views_organic).period(lifetime).fields(values)",
		"since":firstDay.toISOString().substr(0,10)+"T06:00",
		"until":lastDay.toISOString().substr(0,10)+"T06:00",
		"limit":"100",
        "access_token": pageTokens['Tech Cult']
	  },
	  function(response) {
	    //console.log(response);
		for(var i=0;i<response.data.length;i++){
			//if(response.data[i].insights.data[0].values[0].value > 1000000){
            if(typeof response.data[i].shares === 'undefined'){response.data[i].shares = {count: 0};}
            if(response.data[i].insights.data[0].values[0].value > 500000 || response.data[i].shares.count >= 8000){
				if(response.data[i].name !== '' && typeof response.data[i].name !== 'undefined'){
					bonus_cult_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/techcult/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].name, "views": response.data[i].insights.data[0].values[0].value, "shares": response.data[i].shares.count, "object_id": response.data[i].object_id});
				}else if(response.data[i].message !== '' && typeof response.data[i].message !== 'undefined'){
					bonus_cult_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/techcult/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].message, "views": response.data[i].insights.data[0].values[0].value, "shares": response.data[i].shares.count, "object_id": response.data[i].object_id});
				}else{
					bonus_cult_videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/techcult/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": 'Video Deforma', "views": response.data[i].insights.data[0].values[0].value, "shares": response.data[i].shares.count, "object_id": response.data[i].object_id});
				}
			}
		}
		if(typeof response.paging.next !== 'undefined'){
			nextPageBonusVideosCult(response.paging.next,bonus_cult_videos);
		}else{
            //Sort Video Author
            for(var i=0;i<bonus_cult_videos.length;i++){
                getVideosAuthor(bonus_cult_videos[i]);   
            }
            //Sort Video Author
            setTimeout(function(){drawTableLeaderboardCultBonus(bonus_cult_videos)},5000);
        }
	   }
	);
}
function nextPageBonusVideosCult(nextURL,videos){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				if(typeof response.data[i].shares === 'undefined'){response.data[i].shares = {count: 0};}
                if(response.data[i].insights.data[0].values[0].value > 500000 || response.data[i].shares.count >= 8000){
					if(response.data[i].name !== '' && typeof response.data[i].name !== 'undefined'){
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/techcult/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].name, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}else if(response.data[i].message !== '' && typeof response.data[i].message !== 'undefined'){
						video.push({"id": response.data[i].id, "link": 'https://www.facebook.com/techcult/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": response.data[i].message, "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}else{
						videos.push({"id": response.data[i].id, "link": 'https://www.facebook.com/techcult/posts/'+response.data[i].id.substr(response.data[i].id.indexOf('_')+1,response.data[i].id.length), "Titulo": 'Video Deforma', "views": response.data[i].insights.data[0].values[0].value, "object_id": response.data[i].object_id});
					}
				}
			}
			//console.log(videos);
			
			if(typeof response.paging.next !== 'undefined'){
				nextPageBonusVideosCult(response.paging.next,videos);
			}
			else{
                //Sort Video Author
                for(var i=0;i<videos.length;i++){
                    getVideosAuthor(videos[i]);   
                }
                //Sort Video Author
                setTimeout(function(){drawTableLeaderboardCultBonus(videos)},5000);
			}
		}
	});
}
function drawTableLeaderboardCultBonus(videos){
	var bonus_data_cult = new google.visualization.DataTable();
  
    //Sort Video Author
    for(var i=0;i<videos.length;i++){
        getVideosAuthor(videos[i]);   
    }
    //Sort Video Author
    
	bonus_data_cult.addColumn({type: 'string', label: 'Video'});
	bonus_data_cult.addColumn({type: 'number', label: 'Views'});
    bonus_data_cult.addColumn({type: 'number', label: 'Shares'});
    bonus_data_cult.addColumn({type: 'string', label: 'Autor'});
  
  	for(var i=0;i<videos.length;i++){
  		bonus_data_cult.addRow([
			(videos[i].Titulo).link(videos[i].link),
			{v: videos[i].views, f: numberWithCommas(videos[i].views)},
            {v: videos[i].shares, f: numberWithCommas(videos[i].shares)},
            videos[i].author.join(", ")
		]);
  	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var bonus_table_cult = new google.visualization.Table(document.getElementById('chart_div_bonus_video_cult'));
  
  	google.visualization.events.addListener(bonus_table_cult, 'ready', function () {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
        bonus_videos_techcult=videos;
	});
	
	google.visualization.events.addListener(bonus_table_cult, 'sort', function() {
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var o=0;o < links.length;o++){
			links[o].setAttribute('target','_blank');
		}
	});
  
	bonus_table_cult.draw(bonus_data_cult, options);
    
    total_bonus_organic+=videos.length*1000;
    total_bonuses+=videos.length*1000;
	//console.log("done");
    
    getBonosComercialDeforma();
}
function getVideosAuthor(video){
    FB.api(
      '/'+video.object_id,
      'GET',
      {"fields":"custom_labels"},
      function(response) {
          video['author'] = typeof response.custom_labels !== 'undefined' ? response.custom_labels : ['No Author'];
      }
    );
}
function notifySlackBonus(){
    var authors_in_slack = ['Alan','Gazapotl','Lui','Maestro Bergstrom','PAQ','Yado','jovinhoestrada','Rulo', 'Nanu', 'aldeni','Are'];
    var authors_slack_handle = {'Alan': '@alan','Gazapotl':"@jugaldeg",'Lui':"@luis",'Maestro Bergstrom':"@yered",'PAQ':"@pedroaqv",'Yado':"@yado",'jovinhoestrada':"@jorgeluis",'Rulo':"@rulo", 'aldeni': "@U75F5860L", 'Are': "@UB4Q7KHFZ",};
    var video_deforma_slack_handle = {'MARCO': '@marco','PÚAS': '@puas_13','MIKE':"@miguel", 'JORGE':"@jorgeluis"};
    var video_repsodia_slack_handle = {'Karla': '@karlav04', 'Patrick': '@U9JPE13RV'};
    var heads = {
        '@jorgeluis': bonus_comercial_deforma,
        "@yado": bonus_comercial_deforma,
        '@karlav04': bonus_comercial_videos_repsodia.concat(bonus_comercial_videos_techcult),
        '@puas_13': bonus_comercial_videos_deforma
    };
    
    $.each(bonus_authors,function(key, value){
        if($.inArray(key,authors_in_slack) != -1){
            var text='Tus bonos del mes son:\n\n';
            for(var i=0;i<bonus_authors[key].length;i++){
                text+=(i+1)+'.-  '+'<'+bonus_authors[key][i].url+'|'+bonus_authors[key][i].title+'>'+'\t'+numberWithCommas(bonus_authors[key][i].pageviews)+'\t'+bonus_authors[key][i].type+'\n';
            }
            text+='\n\n';
            text+='Tu bono total es:\t$ '+numberWithCommas(bonus_authors[key].bonus);
            
            if(bonus_authors[key].length > 0){
                //console.log(key,authors_slack_handle[key]);
                $.post(
                    "https://hooks.slack.com/services/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",
                    JSON.stringify({
                        "channel": authors_slack_handle[key],
                        "username": 'Bonus Bot',
                        "attachments": [{
                            "fallback": 'Bonos Mensuales',
                            "color": "success",
                            "author_name": 'Bonus Bot',
                            "title": 'Bonos Mensuales',
                            "text": text
                        }]
                    })
                );//Send Slack Bonus
            }else{
                //console.log('no posts',key,authors_slack_handle[key]);
                $.post(
                    "https://hooks.slack.com/services/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",
                    JSON.stringify({
                        "channel": authors_slack_handle[key],
                        "username": 'Bonus Bot',
                        "attachments": [{
                            "fallback": 'Bonos Mensuales',
                            "color": "success",
                            "author_name": 'Bonus Bot',
                            "title": 'Bonos Mensuales',
                            "text": 'No se encontrarom bonos este mes. Si crees que hay un error comunícalo a <@dan>'
                        }]
                    })
                );//Send Slack Bonus
            }
        }
	});
    
    $.each(video_deforma_slack_handle,function(key, value){
        var i=0;
        var author_bonus=0;
        var text='Tus bonos del mes son:\n\n';
        $.each(bonus_videos_deforma,function(index,video){
            if($.inArray(key,video.author) > -1){
                text+=(++i)+'.-  '+'<'+video.link+'|'+video.Titulo+'>'+'\t'+numberWithCommas(video.views)+'\n';
                author_bonus+=1/video.author.length;
            }
        });
        
        text+='\n\n';
        text+='El bono total es:\t$ '+numberWithCommas(author_bonus*1000)+'\n\n';
        text+='Si no ves algún video revisa que tenga el tag con tu nombre('+key+'). Si aún crees que hay un error favor de indicarle a <@dan>';

        if(i > 0){
            $.post(
                "https://hooks.slack.com/services/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",
                JSON.stringify({
                    "channel": value,
                    "username": 'Bonus Bot',
                    "attachments": [{
                        "fallback": 'Bonos Mensuales',
                        "color": "success",
                        "author_name": 'Bonus Bot',
                        "title": 'Bonos Mensuales',
                        "text": text
                    }]
                })
            );//Send Slack Bonus
        }else{
            $.post(
                "https://hooks.slack.com/services/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",
                JSON.stringify({
                    "channel": value,
                    "username": 'Bonus Bot',
                    "attachments": [{
                        "fallback": 'Bonos Mensuales',
                        "color": "success",
                        "author_name": 'Bonus Bot',
                        "title": 'Bonos Mensuales',
                        "text": 'No se encontraron videos con bonos este mes. Si crees que alguno(s) debería tener bono revisa que tenga el tag con tu nombre('+key+'). Si aún crees que hay un error favor de indicarle a <@dan>'
                    }]
                })
            );//Send Slack Bonus
        }
	});
    
    $.each(video_repsodia_slack_handle,function(key, value){
        var i=0;
        var author_bonus=0;
        var text='Tus bonos del mes son:\n\n';
        $.each(bonus_videos_repsodia.concat(bonus_videos_techcult),function(index,video){
            if($.inArray(key,video.author) > -1){
                text+=(++i)+'.-  '+'<'+video.link+'|'+video.Titulo+'>'+'\t'+numberWithCommas(video.views)+'\n';
                author_bonus+=1/video.author.length;
            }
        });
        
        text+='\n\n';
        text+='El bono total es:\t$ '+numberWithCommas(author_bonus*1000)+'\n\n';
        text+='Si no ves algún video revisa que tenga el tag con tu nombre('+key+'). Si aún crees que hay un error favor de indicarle a <@dan>';

        if(i > 0){
            $.post(
                "https://hooks.slack.com/services/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",
                JSON.stringify({
                    "channel": value,
                    "username": 'Bonus Bot',
                    "attachments": [{
                        "fallback": 'Bonos Mensuales',
                        "color": "success",
                        "author_name": 'Bonus Bot',
                        "title": 'Bonos Mensuales',
                        "text": text
                    }]
                })
            );//Send Slack Bonus
        }else{
            $.post(
                "https://hooks.slack.com/services/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",
                JSON.stringify({
                    "channel": value,
                    "username": 'Bonus Bot',
                    "attachments": [{
                        "fallback": 'Bonos Mensuales',
                        "color": "success",
                        "author_name": 'Bonus Bot',
                        "title": 'Bonos Mensuales',
                        "text": 'No se encontraron videos con bonos este mes. Si crees que alguno(s) debería tener bono revisa que tenga el tag con tu nombre('+key+'). Si aún crees que hay un error favor de indicarle a <@dan>'
                    }]
                })
            );//Send Slack Bonus
        }
	});
    
    $.each(heads,function(head, bonuses){
        var text='Los bonos comerciales del mes son:\n\n';
        $.each(bonuses,function(index,bonus){
            text+=(index+1)+'.-  '+bonus.title+'\n';
        });
        
        text+='\n\n';
        text+='El bono total es:\t$ '+numberWithCommas(bonuses.length*500)+'\n\n';
        text+='Si no ves algún bono revisa que tenga el tag \'Comercial\'. Si aún crees que hay un error favor de indicarle a <@dan>';

        if(bonuses.length > 0){
            $.post(
                "https://hooks.slack.com/services/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",
                JSON.stringify({
                    "channel": head,
                    "username": 'Bonus Bot',
                    "attachments": [{
                        "fallback": 'Bonos Comerciales',
                        "color": "success",
                        "author_name": 'Bonus Bot',
                        "title": 'Bonos Comerciales',
                        "text": text
                    }]
                })
            );//Send Slack Bonus
        }else{
            $.post(
                "https://hooks.slack.com/services/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",
                JSON.stringify({
                    "channel": head,
                    "username": 'Bonus Bot',
                    "attachments": [{
                        "fallback": 'Bonos Comerciales',
                        "color": "success",
                        "author_name": 'Bonus Bot',
                        "title": 'Bonos Comerciales',
                        "text": 'No se encontraron bonos comerciales este mes. Si crees que debería haberlos revisa que tenga el tag \'Comercial\'. Si aún crees que hay un error favor de indicarle a <@dan>'
                    }]
                })
            );//Send Slack Bonus
        }
	});
    
    
}
function showTotalBonuses(){
    var bonus_data = new google.visualization.DataTable();
    
	bonus_data.addColumn({type: 'string', label: 'Fuente'});
	bonus_data.addColumn({type: 'number', label: 'Monto'});
  
  	
    bonus_data.addRow([
        'Bonos Organicos',
        {v: total_bonus_organic, f: '$ '+numberWithCommas(total_bonus_organic)}
    ]);
    bonus_data.addRow([
        'Bonos Comercial',
        {v: total_bonus_comercial, f: '$ '+numberWithCommas(total_bonus_comercial)}
    ]);
    var notas_colaboradores = bonus_posts.filter(post => post.author.id == 66).length;
    var notas_colaboradores_bonus = bonus_posts.filter(post => (post.author.id == 66 && post.pageviews >= 80000)).length;
    total_bonuses += determineAuthorsPay(notas_colaboradores)*notas_colaboradores;
    total_bonuses += notas_colaboradores_bonus*1000;
    bonus_data.addRow([
        'Bonos Colaboradores',
        {v: determineAuthorsPay(notas_colaboradores)*notas_colaboradores+notas_colaboradores_bonus*1000, f: '$ '+numberWithCommas(determineAuthorsPay(notas_colaboradores)*notas_colaboradores+notas_colaboradores_bonus*1000)}
    ]);
    bonus_data.addRow([
        '',
        null
    ]);
    bonus_data.addRow([
        'Bonos Total',
        {v: total_bonuses, f: '$ '+numberWithCommas(total_bonuses)}
    ]);

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': false,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var bonus_table = new google.visualization.Table(document.getElementById('chart_div_bonus_totals'));
  
	bonus_table.draw(bonus_data, options);
    
    getGUA();
}
function printTemplate(){
    var template ='';
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    var options_month = { year: 'numeric', month: 'long' };
    var today  = new Date();
    var lastMonth  = new Date(today.getFullYear(),today.getMonth()-1,today.getDate());
    var fullnames = {
        'Alan': 'Alan Miguel Concha Vargas',
        'Gazapotl': 'José Juan Ugalde Garcia',
        'aldeni': 'Laura Aldeni Correa Fraga',
        'jovinhoestrada': 'Jorge Luis Martinez Martinez',
        'Yado': 'Luis Armando Cobo Vazquez',
        'Rulo': 'Raul Alejandro Ramirez Hernandez',
        'PAQ': 'Pedro Alfonso Quintero Valadez',
        'Lui': 'Luis Flores Romero (Recibe Dan Becker)',
        'Maestro Bergstrom': 'Yered Garcia Oseguera',
        'Are': 'Areli Vianey Méndez Ceballos',
    };
    var video_deforma_slack_handle = {
        'MARCO': 'Marco Antonio Gonzalez Peñaloza',
        'MIKE': 'Miguel Becerril Gomez',
        'PÚAS': 'Alejandro Garcia Dominguez',
        'JORGE': 'Jorge Luis Martinez Martinez'
    };
    var video_repsodia_slack_handle = {
        'Karla': 'Karla Itzel Velazquez Hernandez', 
        'Patrick': 'Carlos Zaragoza Castrejón'
    };
    var heads = {
        'Jorge Luis Martinez Martinez': bonus_comercial_deforma,
        'Karla Itzel Velazquez Hernandez': bonus_comercial_videos_repsodia.concat(bonus_comercial_videos_techcult),
        'Alejandro Garcia Dominguez': bonus_comercial_videos_deforma
    };
    
    $.each(bonus_authors,function(key,notes){
        if(notes.bonus > 0 && fullnames[key] != undefined){
            template+='\t\t\t\t\t'+today.toLocaleDateString("es-ES", options)+'\n\n';
            template+='Recibí la cantidad de $ '+numberWithCommas(notes.bonus)+' por incentivos de productividad en el mes de '+lastMonth.toLocaleDateString("es-ES", options_month)+'.\n\n\n\n'+'\t'+fullnames[key]+'\n\n\n';   
        }
    });
    
    $.each(video_deforma_slack_handle,function(key, value){
        var i=0;
        var author_bonus=0;
        $.each(bonus_videos_deforma,function(index,video){
            if($.inArray(key,video.author) > -1){
                i++;
                author_bonus+=1/video.author.length;
            }
        });

        if(i > 0){
            template+='\t\t\t\t\t'+today.toLocaleDateString("es-ES", options)+'\n\n';
            template+='Recibí la cantidad de $ '+numberWithCommas(author_bonus*1000)+' por incentivos de productividad en el mes de '+lastMonth.toLocaleDateString("es-ES", options_month)+'.\n\n\n\n'+'\t'+value+'\n\n\n';   
        }
	});
    
    $.each(video_repsodia_slack_handle,function(key, value){
        var i=0;
        var author_bonus=0;
        var text='Tus bonos del mes son:\n\n';
        $.each(bonus_videos_repsodia.concat(bonus_videos_techcult),function(index,video){
            if($.inArray(key,video.author) > -1){
                i++;
                author_bonus+=1/video.author.length;
            }
        });

        if(i > 0){
            template+='\t\t\t\t\t'+today.toLocaleDateString("es-ES", options)+'\n\n';
            template+='Recibí la cantidad de $ '+numberWithCommas(author_bonus*1000)+' por incentivos de productividad en el mes de '+lastMonth.toLocaleDateString("es-ES", options_month)+'.\n\n\n\n'+'\t'+value+'\n\n\n';   
        }
	});
    
    $.each(heads,function(head, bonuses){
        
        if(bonuses.length > 0){
            template+='\t\t\t\t\t'+today.toLocaleDateString("es-ES", options)+'\n\n';
            template+='Recibí la cantidad de $ '+numberWithCommas(bonuses.length*500)+' por incentivos de material comercial en el mes de '+lastMonth.toLocaleDateString("es-ES", options_month)+'.\n\n\n\n'+'\t'+head+'\n\n\n';      
        }        
	});
    
    console.log(template);
    printSimpleTemplate();
}
function printSimpleTemplate(){
    var total=0;
    var template='';
    var fullnames = {
        'Alan': 'Alan Miguel Concha Vargas',
        'Gazapotl': 'José Juan Ugalde Garcia',
        'aldeni': 'Laura Aldeni Correa Fraga',
        'jovinhoestrada': 'Jorge Luis Martinez Martinez',
        'Yado': 'Luis Armando Cobo Vazquez',
        'Rulo': 'Raul Alejandro Ramirez Hernandez',
        'PAQ': 'Pedro Alfonso Quintero Valadez',
        'Lui': 'Luis Flores Romero (Recibe Dan Becker)',
        'Maestro Bergstrom': 'Yered Garcia Oseguera',
        'Are': 'Areli Vianey Méndez Ceballos',
    };
    var video_deforma_slack_handle = {
        'MARCO': 'Marco Antonio Gonzalez Peñaloza',
        'MIKE': 'Miguel Becerril Gomez',
        'PÚAS': 'Alejandro Garcia Dominguez',
        'JORGE': 'Jorge Luis Martinez Martinez'
    };
    var video_repsodia_slack_handle = {
        'Karla': 'Karla Itzel Velazquez Hernandez', 
        'Patrick': 'Carlos Zaragoza Castrejón'
    };
    var heads = {
        'Jorge Luis Martinez Martinez': bonus_comercial_deforma,
        'Karla Itzel Velazquez Hernandez': bonus_comercial_videos_repsodia.concat(bonus_comercial_videos_techcult),
        'Alejandro Garcia Dominguez': bonus_comercial_videos_deforma
    };
    
    $.each(bonus_authors,function(key,notes){
        if(notes.bonus > 0 && fullnames[key] != undefined){
            template+=fullnames[key]+'\t\t\t$ '+numberWithCommas(notes.bonus)+'\n';
			total+=notes.bonus;
        }
    });
    
    $.each(video_deforma_slack_handle,function(key, value){
        var i=0;
        var author_bonus=0;
        $.each(bonus_videos_deforma,function(index,video){
            if($.inArray(key,video.author) > -1){
                i++;
                author_bonus+=1/video.author.length;
            }
        });

        if(i > 0){
            template+=value+'\t\t\t$ '+numberWithCommas(author_bonus*1000)+'\n';
			total+=author_bonus*1000;
        }
	});
    
    $.each(video_repsodia_slack_handle,function(key, value){
        var i=0;
        var author_bonus=0;
        var text='Tus bonos del mes son:\n\n';
        $.each(bonus_videos_repsodia.concat(bonus_videos_techcult),function(index,video){
            if($.inArray(key,video.author) > -1){
                i++;
                author_bonus+=1/video.author.length;
            }
        });

        if(i > 0){
            template+=value+'\t\t\t$ '+numberWithCommas(author_bonus*1000)+'\n';   
			total+=author_bonus*1000;
        }
	});
    
    $.each(heads,function(head, bonuses){
        
        if(bonuses.length > 0){
            template+='Bonos comerciales\t\t\t$ '+numberWithCommas(bonuses.length*500)+'\n';      
			total+=bonuses.length*500;
        }        
	});
    
    console.log(template);
    console.log(numberWithCommas(total));
}
//Organic Bonus

//Comercial Bonus
var bonus_comercial_deforma=[], bonus_comercial_videos_deforma=[], bonus_comercial_videos_repsodia=[], bonus_comercial_videos_techcult=[];
function getBonosComercialDeforma(){
    $.ajax({
		url: 'https://eldeforma.com/?json=get_date_posts&date='+bonus_month.value+'&count=800&status=published&include=author,url,date,status,categories,title',
		dataType: 'jsonp',
		async: false,
		success: function(response){
            var posts=response.posts;
            $.each(posts,function(post_index,post){
                post.category_slugs=[];
                $.each(post.categories,function(category_index,category){
                   posts[post_index]['category_slugs'].push(category.slug); 
                }) ;
            });
            
            bonus_comercial_deforma = posts.filter(post => $.inArray('comercial',post.category_slugs) > -1);

			//i=posts.length-1;
			if(bonus_comercial_deforma.length>0){
				total_bonus_comercial+=bonus_comercial_deforma.length*500;
                total_bonuses+=bonus_comercial_deforma.length*500;
                console.log(total_bonus_comercial,total_bonuses);
			}
            getBonusVideoComercialDeforma();
		}
	});
}
function getBonusVideoComercialDeforma(){
    var firstDay = new Date(new Date(bonus_month.value+"-01").getTime()+(new Date().getTimezoneOffset()+60)*60*1000), y = firstDay.getFullYear(), m = firstDay.getMonth();
	var lastDay = new Date(y, m + 1, 1);
    bonus_comercial_videos_deforma=[];
	FB.api(
	  '/eldeforma/videos',
	  'GET',
	  {
		"fields":"custom_labels,created_time,title",
		"since":firstDay.toISOString().substr(0,10)+"T06:00",
		"until":lastDay.toISOString().substr(0,10)+"T06:00",
		"limit":"100"
	  },
	  function(response) {
            bonus_comercial_videos_deforma = response.data.filter(video => $.inArray('COMERCIAL',video.custom_labels) > -1);
            total_bonus_comercial+=bonus_comercial_videos_deforma.length*500;
            total_bonuses+=bonus_comercial_videos_deforma.length*500;
            console.log(total_bonus_comercial,total_bonuses);
            getBonusVideoComercialRepsodia();
	   }
	);
}
function getBonusVideoComercialRepsodia(){
    var firstDay = new Date(new Date(bonus_month.value+"-01").getTime()+(new Date().getTimezoneOffset()+60)*60*1000), y = firstDay.getFullYear(), m = firstDay.getMonth();
	var lastDay = new Date(y, m + 1, 1);
	bonus_comercial_videos_repsodia=[];
	FB.api(
	  '/repsodia/videos',
	  'GET',
	  {
		"fields":"custom_labels,created_time,title",
		"since":firstDay.toISOString().substr(0,10)+"T06:00",
		"until":lastDay.toISOString().substr(0,10)+"T06:00",
		"limit":"100"
	  },
	  function(response) {
            bonus_comercial_videos_repsodia = response.data.filter(video => $.inArray('Comercial',video.custom_labels) > -1);
            console.log(bonus_comercial_videos_repsodia);
            total_bonus_comercial+=bonus_comercial_videos_repsodia.length*500;
            total_bonuses+=bonus_comercial_videos_repsodia.length*500;
            console.log(total_bonus_comercial,total_bonuses);
            getBonusVideoComercialTechCult();
	   }
	);
}
function getBonusVideoComercialTechCult(){
    var firstDay = new Date(new Date(bonus_month.value+"-01").getTime()+(new Date().getTimezoneOffset()+60)*60*1000), y = firstDay.getFullYear(), m = firstDay.getMonth();
	var lastDay = new Date(y, m + 1, 1);
    bonus_comercial_videos_techcult=[];
	FB.api(
	  '/techcult/videos',
	  'GET',
	  {
		"fields":"custom_labels,created_time,title",
		"since":firstDay.toISOString().substr(0,10)+"T06:00",
		"until":lastDay.toISOString().substr(0,10)+"T06:00",
		"limit":"100"
	  },
	  function(response) {
            bonus_comercial_videos_techcult = response.data.filter(video => $.inArray('Comercial',video.custom_labels) > -1); 
            total_bonus_comercial+=bonus_comercial_videos_techcult.length*500;
            total_bonuses+=bonus_comercial_videos_techcult.length*500;   
            console.log(total_bonus_comercial,total_bonuses);
            drawTableBonusComercial();
	   }
	);
}
function drawTableBonusComercial(){
    var bonus_data = new google.visualization.DataTable();
    
	bonus_data.addColumn({type: 'string', label: 'Autor'});
	bonus_data.addColumn({type: 'string', label: 'Título'});
  	
    $.each(bonus_comercial_deforma,function(index,post){
        bonus_data.addRow([
            post.author.name,
            post.title.link(post.url)
        ]);
    });
    
    $.each(bonus_comercial_videos_deforma,function(index,post){
        bonus_data.addRow([
            post.custom_labels.filter(label => label.toUpperCase() != 'COMERCIAL').join(", "),
            post.title.link('https://facebook.com/eldeforma/videos/'+post.id)
        ]);
    });
    
    $.each(bonus_comercial_videos_repsodia,function(index,post){
        bonus_data.addRow([
            post.custom_labels.filter(label => label.toUpperCase() != 'COMERCIAL').join(", "),
            post.title.link('https://facebook.com/repsodia/videos/'+post.id)
        ]);
    });
    
    $.each(bonus_comercial_videos_techcult,function(index,post){
        bonus_data.addRow([
            post.custom_labels.filter(label => label.toUpperCase() != 'COMERCIAL').join(", "),
            post.title.link('https://facebook.com/techcult/videos/'+post.id)
        ]);
    });
    
    var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var bonus_table = new google.visualization.Table(document.getElementById('chart_div_bonus_comercial'));
  
	bonus_table.draw(bonus_data, options);
    
    showTotalBonuses();
}
//Comercial Bonus

//GUA month notes
function getGUA(){
    var startOfMonth = bonus_month.value+'-01';
    var endOfMonth = new Date(bonus_month.value.split('-')[0],(parseInt(bonus_month.value.split('-')[1])),0).toISOString().substr(0,10);
    var requestGUA = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": startOfMonth,
		"end-date": endOfMonth,
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath",
		filters: "ga:pagePath=@GUA=1;ga:pageviews>1000",
		sort: '-ga:pageviews',
		fields: 'rows',
	});
	requestGUA.execute(function(resp){
		drawTableGua(resp.rows);
	});
}
function drawTableGua(pages){
    var gua_data = new google.visualization.DataTable();
    
    gua_data.addColumn({type: 'string', label: 'Page'});
    gua_data.addColumn({type: 'number', label: 'Pageviews'});
  
  	for(var i=0;i<pages.length;i++){
  		gua_data.addRow([
			(pages[i][0]).link(pages[i][0]),
            {v: parseInt(pages[i][1]), f: numberWithCommas(pages[i][1])},
		]);
  	}
    
    gua_data.addRow([
        null,
        null,
    ]);
    
    gua_data.addRow([
        'Total',
        {v: pages.length*500, f: '$ '+numberWithCommas(pages.length*500)},
    ]);
    
    var options = {
		'allowHtml': true,
		'showRowNumber': false,
		'width':'100%',
		'height':'100%'
	};
	
	// Instantiate and draw our chart, passing in some options.
	var gua_table = new google.visualization.Table(document.getElementById('chart_div_gua'));
  
	gua_table.draw(gua_data, options);
}
//GUA month notes


//Open Leaderboard
function getOpenArticlesWPSelect(){
	
    $('#chart_div_select_posts').empty();
    $('#chart_div_select_authors').empty();

    $('#chart_div_select_posts').append($('<div/>',{'id':'myloader1','style':'display:none'}).append([$('<img/>',{'class':'loader_logo','src':'https://deforma2.com/assets/images/users/logodeforma.png'}),$('<div/>',{'class':'myloader'})]));
    $('#chart_div_select_authors').append($('<div/>',{'id':'myloader2','style':'display:none'}).append([$('<img/>',{'class':'loader_logo','src':'https://deforma2.com/assets/images/users/logodeforma.png'}),$('<div/>',{'class':'myloader'})]));

    document.getElementById("myloader1").style.display = "block";
    document.getElementById("myloader2").style.display = "block";
    
	select_authors={};
	select_posts=[];
	fb_posts=[];
	dates=[];  

	gapi.client.load('analytics','v3');
	
	var start=new Date($('#from')[0].value+'T00:00:00');
	var end=new Date($('#to')[0].value+'T00:00:00');
	
	//console.log(start);
	
	var current_date;
	var start_cushion=new Date(start.getTime()-1000*60*60*24*0), end_cushion=new Date(end.getTime()+1000*60*60*24*0);

	current_date=start_cushion;
	while(current_date <= end_cushion){
		//console.log(current_date.toISOString().substr(0,10));
		dates.push(current_date.toISOString().substr(0,10));
		current_date.setTime(current_date.getTime()+1000*60*60*24);
	}
	
    //console.log(dates);

	for(var j=0;j<dates.length;j++){
		$.ajax({
			url: 'https://eldeforma.com/?json=get_date_posts&date='+dates[j]+'&count=100&status=published&include=author,url,categories,title',
			dataType: 'jsonp',
			async: false,
			indexValue: j,
			success: function(response){
				for(var i=0;i<response.posts.length;i++){
					select_posts.push(response.posts[i]);
				}
				//console.log(this.indexValue);
				if(this.indexValue==dates.length-1){
					getOpenMetricsSelect(start,end);
				}
			}
		});
	}
}
function getOpenMetricsSelect(start,end){
	//console.log(start,end);
	var end_cushion=new Date(end.getTime()+1000*60*60*24*7)
	var now = new Date();
	var requestPageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": start.toISOString().substr(0,10),
		"end-date": end_cushion.toISOString().substr(0,10),
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath",
		filters: "ga:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@SRE=1;ga:pagePath!@GUIA=1;ga:pagePath!@CST=1",
		sort: '-ga:pageviews',
		fields: 'rows',
	});
	requestPageviews.execute(function(resp){
		var pages=resp.rows;
		//console.log(pages);  

		for(var i=0;i<select_posts.length;i++){
			select_posts[i].pageviews=0;
			for(var j=0;j<pages.length;j++){
				if(select_posts[i].url.indexOf(pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/'))) > -1 && pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/')).length >= 6){
					select_posts[i].pageviews+=parseInt(pages[j][1]);
				}
			}
		}
		
		//console.log(start);
		since=new Date(start).toISOString().substr(0,19);
		//console.log(since);
		until=new Date(end.getTime()+1000*60*60*24).toISOString().substr(0,10)+'T0'+(new Date().getTimezoneOffset()/60)+':00:00';
		//console.log(until);
		FB.api(
		  '/eldeforma/posts',
		  'GET',
		  {"fields":"created_time,link,type,shares,insights.metric(post_video_views,post_impressions,post_consumptions_by_type,post_impressions_unique).period(lifetime),likes.limit(1).summary(true),comments.limit(1).summary(true)","limit":"100","since":since,"until":until,"access_token":pageTokens['El Deforma']},
		  function(response) {
		  	  console.log(response);
			  //fb_posts.push(response.data);
		  
			  for(var i=0;i<response.data.length;i++){
				fb_posts.push(response.data[i]);
			  }
			  if(typeof response.paging.next !== 'undefined'){
				nextPageOpenFBPosts(response.paging.next);
			  }else{
                //console.log('Marry')
				//sortOpenPostsSelect();
				MarryPageviewsWithFBMetrics();  
              }
		  }
		);
		//sortOpenPostsSelect(start,end);
	});
}
function nextPageOpenFBPosts(nextURL){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				fb_posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPageOpenFBPosts(response.paging.next);
			}
			else{
                //console.log('Marry')
				//sortOpenPostsSelect();
				MarryPageviewsWithFBMetrics();
			}
		}
	});
}
function MarryPageviewsWithFBMetrics(){
    //console.log(fb_posts);
	for(var j=0;j<select_posts.length;j++){
        select_posts[j].facebook=[];
        for(var i=0;i<fb_posts.length;i++){
            if(typeof fb_posts[i].link !== 'undefined'){
                if(fb_posts[i].link.indexOf(select_posts[j].url.substr(9,select_posts[j].url.length-1)) != -1){
                    /*fb_posts[i].pageviews=select_posts[j].pageviews;
                    fb_posts[i].categories=select_posts[j].categories;
                    fb_posts[i].author=select_posts[j].author;
                    fb_posts[i].title=select_posts[j].title;
                    fb_posts[i].url=select_posts[j].url;*/
                    select_posts[j].facebook.push(fb_posts[i]);
                }
            }
		}
	}
    
    for(var i=0;i<fb_posts.length;i++){
        for(var j=0;j<select_posts.length;j++){
            if(typeof fb_posts[i].link !== 'undefined'){
                if(fb_posts[i].link.indexOf(select_posts[j].url.substr(9,select_posts[j].url.length-1)) != -1){
                    fb_posts[i].pageviews=select_posts[j].pageviews;
                    fb_posts[i].categories=select_posts[j].categories;
                    fb_posts[i].author=select_posts[j].author;
                    fb_posts[i].title=select_posts[j].title;
                    fb_posts[i].url=select_posts[j].url;
                }
            }
		}
	}
	sortOpenPostsSelect();
}
function sortOpenPostsSelect(){
	var pageviews_totals, shares_totals, likes_totals, comments_totals, impressions_totals, link_clicks_totals, reach_totals;
	var nonZero;
	
	/*var tmp_fb_posts = fb_posts;
	
	fb_posts=[];
	
	function removeDuplicates(arr, prop) {
		 var new_arr = [];
		 var lookup  = {};
 
		 for (var i in arr) {
			 lookup[arr[i][prop]] = arr[i];
		 }
 
		 for (i in lookup) {
			 new_arr.push(lookup[i]);
		 }
 
		 return new_arr;
	}
	 
	fb_posts = removeDuplicates(tmp_fb_posts, "url");*/
	
	for(var i=0;i<fb_posts.length;i++){
		if(typeof fb_posts[i].author !== 'undefined'){
			var nickname = fb_posts[i].author.nickname;
			select_authors[nickname] = [];
		}
	}

	for(var i=0;i<fb_posts.length;i++){
		if(typeof fb_posts[i].author !== 'undefined'){
			select_authors[fb_posts[i].author.nickname].push({
				"url": fb_posts[i].url, 
				"pageviews": fb_posts[i].pageviews, 
				"likes": fb_posts[i].likes.summary.total_count, 
				"shares": typeof fb_posts[i].shares !== 'undefined' ? fb_posts[i].shares.count : 0, 
				"comments": fb_posts[i].comments.summary.total_count,
				"impressions": fb_posts[i].insights.data[1].values[0].value,
				"link_clicks": fb_posts[i].insights.data[2].values[0].value["link clicks"],
				"reach": fb_posts[i].insights.data[3].values[0].value
			});
		}
	}
    
    for(var i=0;i<select_posts.length;i++){
		if(typeof select_posts[i].author !== 'undefined'){
			var nickname = select_posts[i].author.nickname;
			select_authors[nickname] = [];
		}
	}
    
    for(var i=0;i<select_posts.length;i++){
		if(typeof select_posts[i].author !== 'undefined'){
			select_authors[select_posts[i].author.nickname].push({
				"url": select_posts[i].url, 
				"pageviews": select_posts[i].pageviews, 
				"likes": select_posts[i].facebook.length > 0 ? select_posts[i].facebook[0].likes.summary.total_count : 0, 
				"shares": select_posts[i].facebook.length > 0 ? (typeof select_posts[i].facebook[0].shares !== 'undefined' ? select_posts[i].facebook[0].shares.count : 0) : 0, 
				"comments": select_posts[i].facebook.length > 0 ? select_posts[i].facebook[0].comments.summary.total_count : 0,
				"impressions": select_posts[i].facebook.length > 0 ? select_posts[i].facebook[0].insights.data[1].values[0].value : 0,
				"link_clicks": select_posts[i].facebook.length > 0 ? select_posts[i].facebook[0].insights.data[2].values[0].value["link clicks"] : 0,
				"reach": select_posts[i].facebook.length > 0 ? select_posts[i].facebook[0].insights.data[3].values[0].value: 0
			});
		}
	}
	
	$.each(select_authors, function(key, value) {
		//console.log(authors[key]);
		pageviews_totals=0;
		likes_totals=0;
		shares_totals=0;
		comments_totals=0;
		impressions_totals=0;
		link_clicks_totals=0;
		reach_totals=0;
		nonZero=0;
		for(var j=0;j<select_authors[key].length;j++)
		{
			pageviews_totals+=select_authors[key][j].pageviews;
			likes_totals+=select_authors[key][j].likes;
			shares_totals+=select_authors[key][j].shares;
			comments_totals+=select_authors[key][j].comments;
			impressions_totals+=select_authors[key][j].impressions;
			link_clicks_totals+=select_authors[key][j].link_clicks;
			reach_totals+=select_authors[key][j].reach;
			if(select_authors[key][j].pageviews>0){
				nonZero++;
			}
		}
		select_authors[key].pageviews_total=pageviews_totals;
		select_authors[key].likes_total=likes_totals;
		select_authors[key].comments_total=comments_totals;
		select_authors[key].shares_total=shares_totals;
		if(nonZero>0){
			select_authors[key].pageviews_average=pageviews_totals/nonZero;
			select_authors[key].likes_average=likes_totals/nonZero;
			select_authors[key].shares_average=shares_totals/nonZero;
			select_authors[key].comments_average=comments_totals/nonZero;
			select_authors[key].CTR_average=1.0*link_clicks_totals/impressions_totals*100;
			select_authors[key].reach_average=1.0*reach_totals/nonZero;
		}else{
			select_authors[key].pageviews_average=0;
			select_authors[key].likes_average=0;
			select_authors[key].shares_average=0;
			select_authors[key].comments_average=0;
			select_authors[key].CTR_average=0;
			select_authors[key].reach_average=0;
		}
	});
    
    $('#refresh_btn').prop("disabled",false);
    $('#refresh_btn').removeClass("disabled");
    document.getElementById("myloader1").style.display = "none";
    document.getElementById("myloader2").style.display = "none";
    
	drawOpenTableSelect();
}
function drawOpenTableSelect(){
	var posts_data = new google.visualization.DataTable();
    
    var authors_data = new google.visualization.DataTable();
  
	posts_data.addColumn({type: 'string', label: 'Autor'});
	posts_data.addColumn({type: 'string', label: 'Tipo'});
	posts_data.addColumn({type: 'string', label: 'Post'});
	posts_data.addColumn({type: 'number', label: 'PV/VV'});
	posts_data.addColumn({type: 'number', label: 'Likes'});
	posts_data.addColumn({type: 'number', label: 'Shares'});
	posts_data.addColumn({type: 'number', label: 'Comments'});
	posts_data.addColumn({type: 'number', label: 'CTR/VTR'});
	posts_data.addColumn({type: 'number', label: 'Reach'});
  
  	var Colabs = ['Adolfo Santino','Ddtorresf','Gordolobo','Jos','Omar','Ugesaurio','Emisan','Valenzuela','Fercosta','Alde','DonPotasio'];
	for(var n=0;n<fb_posts.length;n++){
		if($('#collabs_only')[0].checked ){
			if(typeof fb_posts[n].author !== 'undefined' && $.inArray(fb_posts[n].author.nickname,Colabs) > -1){
				posts_data.addRow([
					typeof fb_posts[n].author !== 'undefined' ? fb_posts[n].author.nickname : '----',
					fb_posts[n].type,
					OpenPostTitle(fb_posts[n]),
					{v: OpenPostResults(fb_posts[n]), f: numberWithCommas(OpenPostResults(fb_posts[n]))},
					{v: fb_posts[n].likes.summary.total_count, f: numberWithCommas(fb_posts[n].likes.summary.total_count)},
					{v: typeof fb_posts[n].shares !== 'undefined' ? fb_posts[n].shares.count : 0, f: typeof fb_posts[n].shares !== 'undefined' ? numberWithCommas(fb_posts[n].shares.count) : '0'},
					{v: fb_posts[n].comments.summary.total_count, f: numberWithCommas(fb_posts[n].comments.summary.total_count)},
					{v: fb_posts[n].insights.data[2].values[0].value["link clicks"]/fb_posts[n].insights.data[1].values[0].value, f: (fb_posts[n].insights.data[2].values[0].value["link clicks"]/fb_posts[n].insights.data[1].values[0].value*100).toFixed(2)+'%'},
					{v: fb_posts[n].insights.data[3].values[0].value, f: numberWithCommas(fb_posts[n].insights.data[3].values[0].value)}
				]);
			}
		}
		if(!$('#collabs_only')[0].checked){
			posts_data.addRow([
				typeof fb_posts[n].author !== 'undefined' ? fb_posts[n].author.nickname : '----',
				fb_posts[n].type,
				OpenPostTitle(fb_posts[n]),
				{v: OpenPostResults(fb_posts[n]), f: numberWithCommas(OpenPostResults(fb_posts[n]))},
				{v: fb_posts[n].likes.summary.total_count, f: numberWithCommas(fb_posts[n].likes.summary.total_count)},
				{v: typeof fb_posts[n].shares !== 'undefined' ? fb_posts[n].shares.count : 0, f: typeof fb_posts[n].shares !== 'undefined' ? numberWithCommas(fb_posts[n].shares.count) : '0'},
				{v: fb_posts[n].comments.summary.total_count, f: numberWithCommas(fb_posts[n].comments.summary.total_count)},
				{v: fb_posts[n].type === 'video' ? fb_posts[n].insights.data[0].values[0].value/fb_posts[i].insights.data[1].values[0].value : fb_posts[n].insights.data[2].values[0].value["link clicks"]/fb_posts[n].insights.data[1].values[0].value, f: fb_posts[n].type === 'video' ? (fb_posts[n].insights.data[0].values[0].value/fb_posts[n].insights.data[1].values[0].value*100).toFixed(2)+' %' : (fb_posts[n].insights.data[2].values[0].value["link clicks"]/fb_posts[n].insights.data[1].values[0].value*100).toFixed(2)+'%'},
				{v: fb_posts[n].insights.data[3].values[0].value, f: numberWithCommas(fb_posts[n].insights.data[3].values[0].value)}
			]);
		}
	}

	authors_data.addColumn({type: 'string', label: 'Autor'});
	authors_data.addColumn({type: 'number', label: 'Notas'});
	authors_data.addColumn({type: 'number', label: 'PV'});
	authors_data.addColumn({type: 'number', label: 'Likes'});
	authors_data.addColumn({type: 'number', label: 'Shares'});
	authors_data.addColumn({type: 'number', label: 'Comments'});
	authors_data.addColumn({type: 'number', label: 'Avg PV'});
	authors_data.addColumn({type: 'number', label: 'Avg Likes'});
	authors_data.addColumn({type: 'number', label: 'Avg Shares'});
	authors_data.addColumn({type: 'number', label: 'Avg Comments'});
	authors_data.addColumn({type: 'number', label: 'CTR'});
	authors_data.addColumn({type: 'number', label: 'Reach'});

	$.each(select_authors, function(key, value) {
		if($('#collabs_only')[0].checked ){
			if($.inArray(key,Colabs) > -1){
				authors_data.addRow([
					key,
					select_authors[key].length,
					{v: select_authors[key].pageviews_total, f: numberWithCommas(select_authors[key].pageviews_total)},
					{v: select_authors[key].likes_total, f: numberWithCommas(select_authors[key].likes_total)},
					{v: select_authors[key].shares_total, f: numberWithCommas(select_authors[key].shares_total)},
					{v: select_authors[key].comments_total, f: numberWithCommas(select_authors[key].comments_total)},
					{v: select_authors[key].pageviews_average, f: numberWithCommas(select_authors[key].pageviews_average.toFixed(2))},
					{v: select_authors[key].likes_average, f: numberWithCommas(select_authors[key].likes_average.toFixed(2))},
					{v: select_authors[key].shares_average, f: numberWithCommas(select_authors[key].shares_average.toFixed(2))},
					{v: select_authors[key].comments_average, f: numberWithCommas(select_authors[key].comments_average.toFixed(2))},
					{v: select_authors[key].CTR_average, f: select_authors[key].CTR_average.toFixed(2)+'%'},
					{v: select_authors[key].reach_average, f: numberWithCommas(select_authors[key].reach_average.toFixed(0))}
				]);
			}
		}
		if(!$('#collabs_only')[0].checked){
			authors_data.addRow([
				key,
				select_authors[key].length,
				{v: select_authors[key].pageviews_total, f: numberWithCommas(select_authors[key].pageviews_total)},
				{v: select_authors[key].likes_total, f: numberWithCommas(select_authors[key].likes_total)},
				{v: select_authors[key].shares_total, f: numberWithCommas(select_authors[key].shares_total)},
				{v: select_authors[key].comments_total, f: numberWithCommas(select_authors[key].comments_total)},
				{v: select_authors[key].pageviews_average, f: numberWithCommas(select_authors[key].pageviews_average.toFixed(2))},
				{v: select_authors[key].likes_average, f: numberWithCommas(select_authors[key].likes_average.toFixed(2))},
				{v: select_authors[key].shares_average, f: numberWithCommas(select_authors[key].shares_average.toFixed(2))},
				{v: select_authors[key].comments_average, f: numberWithCommas(select_authors[key].comments_average.toFixed(2))},
				{v: select_authors[key].CTR_average, f: select_authors[key].CTR_average.toFixed(2)+'%'},
				{v: select_authors[key].reach_average, f: numberWithCommas(select_authors[key].reach_average.toFixed(0))}
			]);
		}
	});

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 3,
		'sortAscending': false
	};
	
	var options_a = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 2,
		'sortAscending': false
	};

	// Instantiate and draw our chart, passing in some options.
	var posts_table = new google.visualization.Table(document.getElementById('chart_div_select_posts'));  
	var authors_table = new google.visualization.Table(document.getElementById('chart_div_select_authors'));  
	
	google.visualization.events.addListener(posts_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(posts_table, 'sort', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(authors_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(authors_table, 'sort', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
    
    //console.log(posts_data,authors_data);
    
    //console.log(posts_table,authors_table);
    
    //console.log(posts_table.nL,authors_table.nL);
    
    posts_table.draw(posts_data, options);
    authors_table.draw(authors_data, options_a);	
	
	//console.log("done");
}
function OpenPostResults(post){
	if(typeof post.pageviews !== 'undefined'){
		return post.pageviews;
	}
	if(post.type == 'video'){
		return post.insights.data[0].values[0].value;
	}
	return 0;
}
function OpenPostTitle(post){
	if(typeof post.title !== 'undefined'){
		return (post.title.substring(0,100)+'...').link(post.url);
	}
	if(typeof post.name !== 'undefined' && post.name !== ''){
		return (post.name.substring(0,100)+'...').link(post.link);
	}
	if(typeof post.message !== 'undefined' && post.message !== ''){
	 	return (post.message.substring(0,100)+'...').link(post.link);
	}
	return ('N/A').link(post.link);
}
//Open Leaderboard

//Invoicing Manuals
function renderSelectedPDF(){
	var pdf_name = document.getElementById('pdf_selector').value;
	document.getElementById('pdf_viewer').src = 'https://docs.google.com/gview?url=104.155.177.47/responsive_data_center/assets/invoicing_manuals/'+pdf_name+'&embedded=true';
}
//Invoicing Manuals

//Report Repository
function renderSelectedReport(file){
	console.log(file);
	console.log(encodeURIComponent(file));
	document.getElementById('pdf_viewer').src = 'https://docs.google.com/gview?url=104.155.177.47'+encodeURIComponent(file).replace(/#/g,'%23')+'&embedded=true';
}
//Report Repository

//Buffer Timeline
function getBuffer(){
	$.ajax({
		url: 'https://api.bufferapp.com/1/profiles/50cbcbb4c42d1b013d000037/updates/pending.json?access_token=1/6c90fe19499672b0ec5c4125cb1a29de',
		type: 'GET',
		dataType: 'jsonp',
		success: function(response){
			var updates=[];
			updates = response.updates;
			
			FB.api(
				'/eldeforma/promotable_posts',
				'GET',
				{"is_published":"false","fields":"name,scheduled_publish_time"},
				function(response) {
					var posts=[];
					
					for(var i=0;i<response.data.length;i++){
						if(new Date(response.data[i].scheduled_publish_time*1000).getDate() == new Date().getDate()){
							posts.push(response.data[i]);
						}
					}
					
					google.charts.load('current', {'packages':['timeline']});
					google.charts.setOnLoadCallback(drawBufferChart(updates,posts));
				}
			);
			
		}
	});
}
function drawBufferChart(updates,posts){
	//console.log(updates,posts);
	var  updates_rows=[];
	var today=new Date();
	
	var updates_data = new google.visualization.DataTable();
    updates_data.addColumn({ type: 'string', id: 'Fecha' });
    updates_data.addColumn({ type: 'string', id: 'Publicacion' });
    updates_data.addColumn({ type: 'date', id: 'Start' });
    updates_data.addColumn({ type: 'date', id: 'End' });
    
    updates_data.addRow([
		'-',
		'',
		new Date(today.getFullYear(),today.getMonth(),today.getDate(),today.getHours(),today.getMinutes()),
		new Date(today.getFullYear(),today.getMonth(),today.getDate(),today.getHours(),today.getMinutes()+1)
	]);
    	
    updates_data.addRow([
		'Today',
		'',
		new Date(today.getFullYear(),today.getMonth(),today.getDate(),today.getHours(),today.getMinutes()),
		new Date(today.getFullYear(),today.getMonth(),today.getDate(),today.getHours(),today.getMinutes()+1)
	]);
	
	for(var i=0;i<posts.length;i++){
    	var time = new Date(posts[i].scheduled_publish_time*1000);
		var end_time = new Date(posts[i].scheduled_publish_time*1000+30*60*1000);
		
    	updates_data.addRow([
    		'Facebook Today',
    		typeof posts[i].name !== 'undefined' ? posts[i].name : '',
    		new Date(today.getFullYear(),today.getMonth(),today.getDate(),time.getHours(),time.getMinutes()),
    		new Date(today.getFullYear(),today.getMonth(),time.getDate()!=end_time.getDate() ? end_time.getDate() : today.getDate(),end_time.getHours(),end_time.getMinutes())
    	]);
    }
    
    for(var i=0;i<updates.length;i++){
    	var time = new Date(updates[i].scheduled_at*1000);
		var end_time = new Date(updates[i].scheduled_at*1000+30*60*1000);
		
    	updates_data.addRow([
    		updates[i].day,
    		updates[i].text,
    		new Date(today.getFullYear(),today.getMonth(),today.getDate(),time.getHours(),time.getMinutes()),
    		new Date(today.getFullYear(),today.getMonth(),time.getDate()!=end_time.getDate() ? end_time.getDate() : today.getDate(),end_time.getHours(),end_time.getMinutes())
    	]);
    }
    
    console.log(updates_data);
	
	var options = {
		timeline: {
			colorByRowLabel: true,
			showRowLabels: true,
			showBarLabels: true
		}
	};
	
	var chart = new google.visualization.Timeline(document.getElementById('buffer_table'));
	
	google.visualization.events.addListener(chart, 'ready', function () {
		rects = document.getElementsByTagName('rect');
		 for(var i=0;i<rects.length;i++){
		 	if(rects[i].attributes.fill.value.indexOf('4285f4') > 0){
				rects[i].style.height = '100%';
			}
		}
	});
	
	google.visualization.events.addListener(chart, 'onmousover', function () {
		rects = document.getElementsByTagName('rect');
		 for(var i=0;i<rects.length;i++){
		 	if(rects[i].attributes.fill.value.indexOf('4285f4') > 0){
				rects[i].style.height = '100%';
			}
		}
	});

	chart.draw(updates_data, options);
}
//Buffer Timeline

//Predictor
function selectPredictCampaign(post_id,campaign,name,ctr,paid_imp){
    $('#predict_title')[0].innerHTML = name;
    $('#predict_campaign')[0].innerHTML = campaign;
    $('#predict_post_id')[0].innerHTML = post_id;
    $('#predict_paid_imp')[0].innerHTML = paid_imp;
    $('#predict_paid_ctr')[0].innerHTML = ctr;
    updateDataPredict(post_id,campaign,ctr,paid_imp);
}
function updateDataPredict(post_id,campaign,ctr,paid_imp){
   $.ajax({
          url: 'php_scripts/predict_start_date.php',
          type: 'get',
          dataType: 'json',
          async: false,
          data: {'campaign': campaign, 'post_id': post_id},
          success: function(response){
              var start_date = new Date(response);
              var days_running = parseFloat(((new Date().getTime()-start_date.getTime())/(1000*60*60*24)).toFixed(4));
              var sigma = parseFloat($('#sigma').val());
              var x_plus = parseFloat($('#x_plus').val());
              var y_plus = parseFloat($('#y_plus').val());
              var delta_x = parseFloat($('#d_x').val());
              var data=[];
              var days_forward = parseFloat($('#time_forward').val());
              for(var x=0;x<=days_forward;x+=0.007){
                  data.push([x, predictedClicks(x,sigma,parseFloat(ctr),parseInt(paid_imp),days_running,days_forward,x_plus,y_plus,delta_x)]);
              }
              setTimeout(function(){drawChartPredict(data,post_id,campaign)},1000);
          }
   });
}
function predictedClicks(x,s,C,i,d,D,X,Y,dx){
    var y=0;
    for(var k=1;k<=Math.ceil(D);k++){
        y+=(i/d)*(C/100)*(1/(1+Math.exp(-s*((x-X)-k+0.5+DeltaX(x,dx)*(k-1)))))*Delta(x,D)+Y;
    }
    return y;
}
function Delta(x,D){
    return x >= D ? 1 : 0.9;
}
function DeltaX(x,dx){
    return x < 0.5 ? 0 : dx;
}
function drawChartPredict(data_array,post_id,campaign){
       //console.log(data_array);
           var real_data;
       $.ajax({
          url: 'php_scripts/predict_get_real_data.php',
          type: 'get',
          async: false,
          dataType: 'json',
          data: {'campaign': campaign, 'post_id': post_id},
          success: function(response){
              real_data = response;
          }
      });
       //console.log(real_data);
       var data = new google.visualization.DataTable();
       data.addColumn('number', 'Días');
       data.addColumn('number', 'Predicted Link Clicks');
       data.addColumn('number', 'Real Link Clicks');
       
       for(var i=0;i<data_array.length;i++){
           if(i<real_data.length){
               data.addRow([
                    data_array[i][0],
                    Math.trunc(data_array[i][1]),
                    parseInt(real_data[i])
                ]);
           }else{
               data.addRow([
                   data_array[i][0],
                    Math.trunc(data_array[i][1]),
                    null
               ]);
           }
       }
       
           
   var options = {
       title: 'Predicción',
       curveType: 'function'
   };
   var chart = new google.visualization.LineChart(document.getElementById('chart_div_predict'));
   
   chart.draw(data, options);
}
function updateSigmaPredict(){
   updateDataPredict($('#predict_post_id').html(),$('#predict_campaign').html(),$('#predict_paid_ctr').html(),$('#predict_paid_imp').html());
}
//Predictor