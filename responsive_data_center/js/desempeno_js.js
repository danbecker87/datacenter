var performance_authors, performance_posts, performance_fb_posts, performance_dates;
var Authors = {};
function getPerformanceArticlesWPSelect(){
	//$('#chart_div_performance_posts')[0].removeChild($('#chart_div_performance_posts')[0].firstChild);
    
    $('#creativos_chart_div').empty();

    $('#creativos_chart_div').append($('<div/>',{'id':'myloader','style':'display:none'}).append([$('<img/>',{'class':'loader_logo','src':'http://deforma2.com/assets/images/users/logodeforma.png'}),$('<div/>',{'class':'myloader'})]));

    document.getElementById("myloader").style.display = "block";
    
	performance_authors={};
	performance_posts=[];
	performance_fb_posts=[];
	performance_dates=[];  

	gapi.client.load('analytics','v3');
	
	var start=new Date($('#creativos_from')[0].value+'T00:00:00');
	var end=new Date($('#creativos_to')[0].value+'T00:00:00');
	
	//console.log(start);
	
	var current_date;
	var start_cushion=new Date(start.getTime()-1000*60*60*24*0), end_cushion=new Date(end.getTime()+1000*60*60*24*0);

	current_date=start_cushion;
	while(current_date <= end_cushion){
		//console.log(current_date.toISOString().substr(0,10));
		performance_dates.push(current_date.toISOString().substr(0,10));
		current_date.setTime(current_date.getTime()+1000*60*60*24);
	}
	

	for(var j=0;j<performance_dates.length;j++){
		$.ajax({
			url: 'http://eldeforma.com/?json=get_date_posts&date='+performance_dates[j]+'&count=300&status=published&include=author,url,title,date',
			dataType: 'jsonp',
			async: false,
			indexValue: j,
			success: function(response){
                //console.log(response);
				for(var i=0;i<response.posts.length;i++){
					performance_posts.push(response.posts[i]);
				}
				//console.log(this.indexValue);
				if(this.indexValue==performance_dates.length-1){
					getPerformanceMetricsPerformance(start,end);
				}
			}
		});
	}
}
function getPerformanceMetricsPerformance(start,end){
	//console.log(start,end);
	var end_cushion=new Date(end.getTime()+1000*60*60*24*7)
	var now = new Date();
	var requestPageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": start.toISOString().substr(0,10),
		"end-date": end_cushion.toISOString().substr(0,10),
		metrics: "ga:pageviews",
		dimensions: "ga:pagePath,ga:date",
		filters: "ga:pagePath!@p1=true;ga:pagePath!@GUA=1;ga:pagePath!@GUIA=1;ga:pagePath!@SRE=1;ga:pagePath!@CST=1",
        'max-results': 10000,
		sort: '-ga:pageviews',
		fields: 'rows',
	});
	requestPageviews.execute(function(resp){
		var pages=resp.rows;
		//console.log(pages);  

		for(var i=0;i<performance_posts.length;i++){
			performance_posts[i].pageviews=0;
            performance_posts[i].dates={};
			for(var j=0;j<pages.length;j++){
				if(performance_posts[i].url.indexOf(pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/'))) > -1 && pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/')).length >= 6){
					performance_posts[i].pageviews+=parseInt(pages[j][2]);
                    if(typeof performance_posts[i].dates[pages[j][1].substr(0,4)+pages[j][1].substr(4,2)+pages[j][1].substr(6,2)] === 'undefined'){
                        performance_posts[i].dates[pages[j][1].substr(0,4)+'-'+pages[j][1].substr(4,2)+'-'+pages[j][1].substr(6,2)]={};
                        performance_posts[i].dates[pages[j][1].substr(0,4)+'-'+pages[j][1].substr(4,2)+'-'+pages[j][1].substr(6,2)].pageviews = parseInt(pages[j][2]);
                    }else{
                        performance_posts[i].dates[pages[j][1].substr(0,4)+'-'+pages[j][1].substr(4,2)+'-'+pages[j][1].substr(6,2)].pageviews+=parseInt(pages[j][2]);
                    }
				}
			}
		}
		
		//console.log(start);
		since=new Date(start).toISOString().substr(0,19);
		//console.log(since);
		until=new Date(end.getTime()+1000*60*60*24).toISOString().substr(0,10)+'T0'+(new Date().getTimezoneOffset()/60)+':00:00';
		//console.log(until);
		FB.api(
		  '/eldeforma/posts',
		  'GET',
		  {"fields":"created_time,link,type,shares,insights.metric(post_video_views,post_impressions,post_consumptions_by_type,post_impressions_unique).period(lifetime),likes.limit(1).summary(true),comments.limit(1).summary(true)","limit":"100","since":since,"until":until,"access_token":pageTokens['El Deforma']},
		  function(response) {
		  	  //console.log(response.insights);
			  //fb_posts.push(response.data);
		  
			  for(var i=0;i<response.data.length;i++){
				performance_fb_posts.push(response.data[i]);
			  }
			  if(typeof response.paging.next !== 'undefined'){
				nextPagePerformanceFBPosts(response.paging.next);
			  }else{
                //console.log('Marry')
				//sortOpenPostsSelect();
				MarryPerformancePageviewsWithFBMetrics();
              }
		  }
		);
		//sortOpenPostsSelect(start,end);
	});
}
function nextPagePerformanceFBPosts(nextURL){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				performance_fb_posts.push(response.data[i]);
			}
			if(typeof response.paging.next !== 'undefined'){
				nextPagePerformanceFBPosts(response.paging.next);
			}
			else{
                //console.log('Marry')
				//sortOpenPostsSelect();
				MarryPerformancePageviewsWithFBMetrics();
			}
		}
	});
}
function MarryPerformancePageviewsWithFBMetrics(){
	for(var i=0;i<performance_fb_posts.length;i++){
		for(var j=0;j<performance_posts.length;j++){
            if(typeof performance_fb_posts[i].link !== 'undefined'){
                if(performance_fb_posts[i].link.indexOf(performance_posts[j].url) != -1){
                    performance_fb_posts[i].pageviews=performance_posts[j].pageviews;
                    performance_fb_posts[i].categories=performance_posts[j].categories;
                    performance_fb_posts[i].author=performance_posts[j].author;
                    performance_fb_posts[i].title=performance_posts[j].title;
                    performance_fb_posts[i].url=performance_posts[j].url;
                }
            }
		}
	}
	sortPerformancePostsSelect();
}
function sortPerformancePostsSelect(){
	var pageviews_totals, shares_totals, likes_totals, comments_totals, impressions_totals, link_clicks_totals, reach_totals;
	var nonZero;
	
	var tmp_fb_posts = performance_fb_posts;
	
    //console.log(performance_fb_posts);
    
	performance_fb_posts=[];
	
	function removeDuplicates(arr, prop) {
		 var new_arr = [];
		 var lookup  = {};
 
		 for (var i in arr) {
			 lookup[arr[i][prop]] = arr[i];
		 }
 
		 for (i in lookup) {
			 new_arr.push(lookup[i]);
		 }
 
		 return new_arr;
	}
	 
	performance_fb_posts = removeDuplicates(tmp_fb_posts, "url");
	
	for(var i=0;i<performance_fb_posts.length;i++){
		if(typeof performance_fb_posts[i].author !== 'undefined'){
			var author_nickname = performance_fb_posts[i].author.nickname;
            var author_id = performance_fb_posts[i].author.id;
            var author_slug = performance_fb_posts[i].author.slug;
            var author = performance_fb_posts[i].author.name;
			Authors[author] = {
                nickname: author_nickname,
                id: author_id,
                slug: author_slug,
                wp_posts: [],
                fb_posts: [],
                dates: {},
                likes: 0,
                link_clicks: 0,
                shares: 0,
                comments: 0,
                pageviews: 0,
                count_fb_posts: 0,
                count_wp_posts: 0
            };
		}
	}

	for(var i=0;i<performance_fb_posts.length;i++){
		if(typeof performance_fb_posts[i].author !== 'undefined'){
			Authors[performance_fb_posts[i].author.name].fb_posts.push({
				'id': performance_fb_posts[i].id,
                'date': new Date(new Date(performance_fb_posts[i].created_time).toLocaleDateString()).toISOString().substr(0,10),
                "link": performance_fb_posts[i].link, 
				"likes": performance_fb_posts[i].likes.summary.total_count, 
				"shares": typeof performance_fb_posts[i].shares !== 'undefined' ? performance_fb_posts[i].shares.count : 0, 
				"comments": performance_fb_posts[i].comments.summary.total_count,
				"link_clicks": performance_fb_posts[i].insights.data[2].values[0].value["link clicks"],
				"reach": performance_fb_posts[i].insights.data[3].values[0].value
			});
            Authors[performance_fb_posts[i].author.name].likes+=performance_fb_posts[i].likes.summary.total_count;
            Authors[performance_fb_posts[i].author.name].shares+=typeof performance_fb_posts[i].shares !== 'undefined' ? performance_fb_posts[i].shares.count : 0;
            Authors[performance_fb_posts[i].author.name].comments+=performance_fb_posts[i].comments.summary.total_count;
            Authors[performance_fb_posts[i].author.name].link_clicks+=performance_fb_posts[i].insights.data[2].values[0].value["link clicks"];
            Authors[performance_fb_posts[i].author.name].pageviews+=performance_fb_posts[i].pageviews;
            Authors[performance_fb_posts[i].author.name].count_fb_posts++;
            
            var date = new Date(new Date(performance_fb_posts[i].created_time).toLocaleDateString()).toISOString().substr(0,10);
            if(typeof Authors[performance_fb_posts[i].author.name].dates[date] === 'undefined'){
                Authors[performance_fb_posts[i].author.name].dates[date] = {};
                Authors[performance_fb_posts[i].author.name].dates[date].fb_posts = 1;
                Authors[performance_fb_posts[i].author.name].dates[date].likes = performance_fb_posts[i].likes.summary.total_count;
                Authors[performance_fb_posts[i].author.name].dates[date].pageviews = performance_fb_posts[i].pageviews;
                Authors[performance_fb_posts[i].author.name].dates[date].link_clicks = performance_fb_posts[i].insights.data[2].values[0].value["link clicks"];
                Authors[performance_fb_posts[i].author.name].dates[date].shares = typeof performance_fb_posts[i].shares !== 'undefined' ? performance_fb_posts[i].shares.count : 0;
                Authors[performance_fb_posts[i].author.name].dates[date].comments = performance_fb_posts[i].comments.summary.total_count;
            }else{
                Authors[performance_fb_posts[i].author.name].dates[date].fb_posts++;
                Authors[performance_fb_posts[i].author.name].dates[date].likes += performance_fb_posts[i].likes.summary.total_count;
                Authors[performance_fb_posts[i].author.name].dates[date].pageviews += performance_fb_posts[i].pageviews;
                Authors[performance_fb_posts[i].author.name].dates[date].link_clicks += performance_fb_posts[i].insights.data[2].values[0].value["link clicks"];
                Authors[performance_fb_posts[i].author.name].dates[date].shares += typeof performance_fb_posts[i].shares !== 'undefined' ? performance_fb_posts[i].shares.count : 0;
                Authors[performance_fb_posts[i].author.name].dates[date].comments += performance_fb_posts[i].comments.summary.total_count;
            }
		}
	}
    
    for(var i=0;i<performance_posts.length;i++){
		if(typeof performance_posts[i].author !== 'undefined'){
            if(typeof Authors[performance_posts[i].author.name] !== 'undefined'){
                Authors[performance_posts[i].author.name].wp_posts.push({
                    'id': performance_posts[i].id,
                    'date': new Date(new Date(performance_posts[i].date).toLocaleDateString()).toISOString().substr(0,10),
                    "url": performance_posts[i].url, 
                    'pageviews': performance_posts[i].pageviews,
                    'dates': performance_posts[i].dates
                });

                var date = new Date(new Date(performance_posts[i].date).toLocaleDateString()).toISOString().substr(0,10);
                if(typeof Authors[performance_posts[i].author.name].dates[date] === 'undefined'){
                    Authors[performance_posts[i].author.name].dates[date] = {};
                    Authors[performance_posts[i].author.name].dates[date].wp_posts = 1;
                }else{
                    if(typeof Authors[performance_posts[i].author.name].dates[date].wp_posts === 'undefined'){
                        Authors[performance_posts[i].author.name].dates[date].wp_posts = 1;
                    }else{
                       Authors[performance_posts[i].author.name].dates[date].wp_posts++;
                    }
                }
            }
		}
	}
    
    $.each(Authors,function(key, value){
        Authors[key].count_wp_posts = Authors[key].wp_posts.length;
    });
    
    var start=new Date($('#creativos_from')[0].value+'T00:00:00');
	var end=new Date($('#creativos_to')[0].value+'T00:00:00');
    
    var start_cushion=new Date(start.getTime()-1000*60*60*24*0), end_cushion=new Date(end.getTime()+1000*60*60*24*0);
    
    var requestPageviews = gapi.client.analytics.data.ga.get({
		ids: "ga:41142760",
		"start-date": start.toISOString().substr(0,10),
		"end-date": end_cushion.toISOString().substr(0,10),
		metrics: "ga:pageviews",
		dimensions: "ga:date",
		filters: "ga:pagePath=@p1=true,ga:pagePath=@GUA=1,ga:pagePath=@GUIA=1,ga:pagePath=@CST=1,ga:pagePath=@SRE=1",
        'max-results': 10000,
		sort: 'ga:date',
		fields: 'rows',
	});
    
    requestPageviews.execute(function(resp){
        Authors['Promoted']={wp_posts:[]};
        Authors['Promoted'].wp_posts[0] = {dates: {}};
        
		var pages=resp.rows;
		//console.log(pages);  

		for(var i=0;i<pages.length;i++){
            Authors.Promoted.wp_posts[0].dates[pages[i][0].substr(0,4)+'-'+pages[i][0].substr(4,2)+'-'+pages[i][0].substr(6,2)]={};
            Authors.Promoted.wp_posts[0].dates[pages[i][0].substr(0,4)+'-'+pages[i][0].substr(4,2)+'-'+pages[i][0].substr(6,2)].pageviews = parseInt(pages[i][1]);
		}
        
        $.each(Authors,function(key, value){
            Authors[key].avg_pageviews = Authors[key].pageviews/Authors[key].count_wp_posts;
            Authors[key].avg_likes = Authors[key].likes/Authors[key].count_fb_posts;
            Authors[key].avg_shares = Authors[key].shares/Authors[key].count_fb_posts;
            Authors[key].avg_link_clicks = Authors[key].link_clicks/Authors[key].count_fb_posts;
        });
    
        $('#creativos_refresh_btn').prop("disabled",false);
        $('#creativos_refresh_btn').removeClass("disabled");
        $('#creativos_refresh_btn_3way').prop("disabled",false);
        $('#creativos_refresh_btn_3way').removeClass("disabled");
        $('#TotalesCreativos_refresh_btn').prop("disabled",false);
        $('#TotalesCreativos_refresh_btn').removeClass("disabled");
        document.getElementById("myloader").style.display = "none";    
    });   
	
	/*$.each(performance_authors, function(key, value) {
		//console.log(authors[key]);
		pageviews_totals=0;
		likes_totals=0;
		shares_totals=0;
		comments_totals=0;
		impressions_totals=0;
		link_clicks_totals=0;
		reach_totals=0;
		nonZero=0;
		for(var j=0;j<performance_authors[key].length;j++)
		{
			pageviews_totals+=performance_authors[key][j].pageviews;
			likes_totals+=performance_authors[key][j].likes;
			shares_totals+=performance_authors[key][j].shares;
			comments_totals+=performance_authors[key][j].comments;
			impressions_totals+=performance_authors[key][j].impressions;
			link_clicks_totals+=performance_authors[key][j].link_clicks;
			reach_totals+=performance_authors[key][j].reach;
			if(performance_authors[key][j].pageviews>0){
				nonZero++;
			}
		}
		performance_authors[key].pageviews_total=pageviews_totals;
		performance_authors[key].likes_total=likes_totals;
		performance_authors[key].comments_total=comments_totals;
		performance_authors[key].shares_total=shares_totals;
		if(nonZero>0){
			performance_authors[key].pageviews_average=pageviews_totals/nonZero;
			performance_authors[key].likes_average=likes_totals/nonZero;
			performance_authors[key].shares_average=shares_totals/nonZero;
			performance_authors[key].comments_average=comments_totals/nonZero;
			performance_authors[key].CTR_average=1.0*link_clicks_totals/impressions_totals*100;
			performance_authors[key].reach_average=1.0*reach_totals/nonZero;
		}else{
			performance_authors[key].pageviews_average=0;
			performance_authors[key].likes_average=0;
			performance_authors[key].shares_average=0;
			performance_authors[key].comments_average=0;
			performance_authors[key].CTR_average=0;
			performance_authors[key].reach_average=0;
		}
	});*/
	//drawPerformanceTableSelect();
}
function PerformancePostResults(post){
	if(typeof post.pageviews !== 'undefined'){
		return post.pageviews;
	}
	if(post.type == 'video'){
		return post.insights.data[0].values[0].value;
	}
	return 0;
}
function PerformancePostTitle(post){
	if(typeof post.title !== 'undefined'){
		return (post.title.substring(0,100)+'...').link(post.url);
	}
	if(typeof post.name !== 'undefined' && post.name !== ''){
		return (post.name.substring(0,100)+'...').link(post.link);
	}
	if(typeof post.message !== 'undefined' && post.message !== ''){
	 	return (post.message.substring(0,100)+'...').link(post.link);
	}
	return ('N/A').link(post.link);
}
function AuthorsLineChartDataArray(metric){
    var header_row = Object.keys(Authors);
    header_row.unshift('Date')
    data_array = [header_row];
    var dates=new Set();
    $.each(Authors,function(key,values){
        if(key != 'Promoted'){
            $.each(Authors[key].dates,function(key2,values2){
                dates.add(key2);
            });
        }
    });
    var rows_values=[...dates].sort();///beware of new year these values will not sort well
    for(var i=0;i<rows_values.length;i++){
        data_array.push([rows_values[i]]);
        for(var j=0;j<header_row.length-1;j++){
            data_array[i+1].push(0);
        }
    }
    $.each(Authors,function(key, value){
        if(key != 'Promoted'){
            $.each(Authors[key].dates,function(key2,values2){
                var column=header_row.indexOf(key);
                var row=rows_values.indexOf(key2)+1;
                data_array[row][column]+=typeof Authors[key].dates[key2][metric] === 'undefined' ? 0 : Authors[key].dates[key2][metric];    
            });
        }
    });
    AuthorsDrawLineChart(data_array,metric);
}
function AuthorsDrawLineChart(data_array,metric) {
    var data = google.visualization.arrayToDataTable(data_array);

    var chart = new google.visualization.ChartWrapper({
        chartType: 'LineChart',
        containerId: 'creativos_chart_div',
        dataTable: data,
        options: {
            title: metric,
            legend: { position: 'right' }
        }
    });
    
    ///////Show hide series
    var columns = [0];
    /* the series map is an array of data series
     * "column" is the index of the data column to use for the series
     * "roleColumns" is an array of column indices corresponding to columns with roles that are associated with this data series
     * "display" is a boolean, set to true to make the series visible on the initial draw
     */
    /*var seriesMap = [{
        column: 1,
        roleColumns: [2],
        display: true
    }, {
        column: 3,
        roleColumns: [4, 5],
        display: true
    }, {
        column: 6,
        roleColumns: [7],
        display: false
    }];*/
    
    var seriesMap=[];
    for(var i=1; i<data_array[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}
function AuthorsBarChartDataArray(metric){
    var header_row = Object.keys(Authors);
    header_row.unshift('Date');
    data_array = [header_row];
    var dates=new Set();
    $.each(Authors,function(key,values){
        if(key != 'Promoted'){
            $.each(Authors[key].dates,function(key2,values2){
                dates.add(key2);
            });
        }
    });
    var rows_values=[...dates].sort();///beware of new year these values will not sort well
    for(var i=0;i<rows_values.length;i++){
        data_array.push([rows_values[i]]);
        for(var j=0;j<header_row.length-1;j++){
            data_array[i+1].push(0);
        }
    }
    $.each(Authors,function(key, value){
        if(key != 'Promoted'){
            $.each(Authors[key].dates,function(key2,values2){
                var column=header_row.indexOf(key);
                var row=rows_values.indexOf(key2)+1;
                data_array[row][column]+=typeof Authors[key].dates[key2][metric] === 'undefined' ? 0 : Authors[key].dates[key2][metric];    
            });
        }
    });
    AuthorsDrawBarChart(data_array,metric);
}
function AuthorsDrawBarChart(data_array,metric){
    var data = google.visualization.arrayToDataTable(data_array);

    var chart = new google.visualization.ChartWrapper({
        chartType: 'ColumnChart',
        containerId: 'creativos_chart_div',
        dataTable: data,
        options: {
            title: metric,
            legend: { position: 'right' },
            isStacked: $('#creativos_percentage')[0].checked ? 'percent' : true
        }
    });
    
    ///////Show hide series
    var columns = [0];
    /* the series map is an array of data series
     * "column" is the index of the data column to use for the series
     * "roleColumns" is an array of column indices corresponding to columns with roles that are associated with this data series
     * "display" is a boolean, set to true to make the series visible on the initial draw
     */
    /*var seriesMap = [{
        column: 1,
        roleColumns: [2],
        display: true
    }, {
        column: 3,
        roleColumns: [4, 5],
        display: true
    }, {
        column: 6,
        roleColumns: [7],
        display: false
    }];*/
    
    var seriesMap=[];
    for(var i=1; i<data_array[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}

//Creatives
function AuthorsLineChartPVDataArray(){    
    var header_row = Object.keys(Authors);
    header_row.unshift('Date')
    data_array = [header_row];
    var dates=new Set();
    $.each(Authors,function(key,values){
        for(var i=0;i<Authors[key].wp_posts.length;i++){
                $.each(Authors[key].wp_posts[i].dates,function(key2,values2){
                dates.add(key2);
            });
        }
    });
    var rows_values=[...dates].sort();
    for(var i=0;i<rows_values.length;i++){
        data_array.push([rows_values[i]]);
        for(var j=0;j<header_row.length-1;j++){
            data_array[i+1].push(0);
        }
    }
    $.each(Authors,function(key, value){
        for(var i=0;i<Authors[key].wp_posts.length;i++){
            $.each(Authors[key].wp_posts[i].dates,function(key2,values2){
                var column=header_row.indexOf(key);
                var row=rows_values.indexOf(key2)+1;
                data_array[row][column]+=typeof Authors[key].wp_posts[i].dates[key2]['pageviews'] === 'undefined' ? 0 : Authors[key].wp_posts[i].dates[key2]['pageviews'];    
            });
        }
    });
    AuthorsPVDrawLineChart(data_array);
}
function AuthorsPVDrawLineChart(data_array) {
    var data = google.visualization.arrayToDataTable(data_array);

    var chart = new google.visualization.ChartWrapper({
        chartType: 'LineChart',
        containerId: 'creativos_chart_div',
        dataTable: data,
        options: {
            title: 'Pageviews',
            legend: { position: 'right' }
        }
    });
    
    ///////Show hide series
    var columns = [0];
    /* the series map is an array of data series
     * "column" is the index of the data column to use for the series
     * "roleColumns" is an array of column indices corresponding to columns with roles that are associated with this data series
     * "display" is a boolean, set to true to make the series visible on the initial draw
     */
    /*var seriesMap = [{
        column: 1,
        roleColumns: [2],
        display: true
    }, {
        column: 3,
        roleColumns: [4, 5],
        display: true
    }, {
        column: 6,
        roleColumns: [7],
        display: false
    }];*/
    
    var seriesMap=[];
    for(var i=1; i<data_array[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}
function AuthorsBarChartPVDataArray(){        
    var header_row = Object.keys(Authors);
    header_row.unshift('Date');
    data_array = [header_row];
    var dates=new Set();
    $.each(Authors,function(key,values){
        for(var i=0;i<Authors[key].wp_posts.length;i++){
                $.each(Authors[key].wp_posts[i].dates,function(key2,values2){
                dates.add(key2);
            });
        }
    });
    var rows_values=[...dates].sort();
    for(var i=0;i<rows_values.length;i++){
        data_array.push([rows_values[i]]);
        for(var j=0;j<header_row.length-1;j++){
            data_array[i+1].push(0);
        }
    }
    $.each(Authors,function(key, value){
        for(var i=0;i<Authors[key].wp_posts.length;i++){
            $.each(Authors[key].wp_posts[i].dates,function(key2,values2){
                var column=header_row.indexOf(key);
                var row=rows_values.indexOf(key2)+1;
                data_array[row][column]+=typeof Authors[key].wp_posts[i].dates[key2]['pageviews'] === 'undefined' ? 0 : Authors[key].wp_posts[i].dates[key2]['pageviews'];    
            });
        }
    });
    AuthorsPVDrawBarChart(data_array);
}
function AuthorsPVDrawBarChart(data_array){
    var data = google.visualization.arrayToDataTable(data_array);

    var chart = new google.visualization.ChartWrapper({
        chartType: 'ColumnChart',
        containerId: 'creativos_chart_div',
        dataTable: data,
        options: {
            title: 'Pageviews',
            legend: { position: 'right' },
            isStacked: $('#creativos_percentage')[0].checked ? 'percent' : true
        }
    });
    
    ///////Show hide series
    var columns = [0];
    /* the series map is an array of data series
     * "column" is the index of the data column to use for the series
     * "roleColumns" is an array of column indices corresponding to columns with roles that are associated with this data series
     * "display" is a boolean, set to true to make the series visible on the initial draw
     */
    /*var seriesMap = [{
        column: 1,
        roleColumns: [2],
        display: true
    }, {
        column: 3,
        roleColumns: [4, 5],
        display: true
    }, {
        column: 6,
        roleColumns: [7],
        display: false
    }];*/
    
    var seriesMap=[];
    for(var i=1; i<data_array[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}
function GraphCreativos(){
    $('#select_legend').remove();
    if($('#creativos_kpi')[0].value == 'pageviews'){
        if($('#creativos_bar')[0].checked){
            AuthorsBarChartPVDataArray();
        }
        else{
            AuthorsLineChartPVDataArray();
        }
    }
    else{
        if($('#creativos_bar')[0].checked){
            AuthorsBarChartDataArray($('#creativos_kpi')[0].value);
        }
        else{
            AuthorsLineChartDataArray($('#creativos_kpi')[0].value);
        }
    }
}
function GraphTotalesCreativos(){
    $('#select_legend').remove();
    var Totales = [];
    Totales.push(['Metricas','Comments','FB Posts','WP Posts','Likes','Link Clicks','Pageviews','Shares']);
    $.each(Authors,function(key, value){
        if(key!='Promoted'){
            Totales.push([key,Authors[key].comments,Authors[key].count_fb_posts,Authors[key].count_wp_posts,Authors[key].likes,Authors[key].link_clicks,Authors[key].pageviews,Authors[key].shares]);
        }
    });
    
    Totales = Totales[0].map((col, i) => Totales.map(row => row[i]));
           
    var data = google.visualization.arrayToDataTable(Totales);
    
    var chart = new google.visualization.ChartWrapper({
        chartType: 'ColumnChart',
        containerId: 'creativos_chart_div',
        dataTable: data,
        options: {
            title: 'Metricas',
            legend: { position: 'right' },
            isStacked: 'percent'
        }
    });
    
    ///////Show hide series
    var columns = [0];
    
    var seriesMap=[];
    for(var i=1; i<Totales[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}

var global_chart;
function GraphTotalesCreativos3Way(){
    if(typeof $('#select_legend')[0] == 'undefined'){
        $('#creativos_chart').append($('<div/>').attr('id','select_legend').attr('class','col-lg-2'));
        $.each(Authors,function(key, value){
            if(key!='Promoted'){
                $('#select_legend').append($('<div/>').attr('class','checkbox checkbox-primary').append($('<input/>').attr('id',key.replace(/\s/g, '')).attr('type','checkbox').attr('checked','').attr('onChange','DrawGraphTotalesCreativos3Way()'),$('<label/>').attr('for',key.replace(/\s/g, '')).html(key)));
            }
        });
    }
    DrawGraphTotalesCreativos3Way();
}
function DrawGraphTotalesCreativos3Way(){
    var x=$('#creativos_kpi_1'), y=$('#creativos_kpi_2'), bubble=$('#creativos_kpi_3');
    
    var Totales = [];
    //Totales.push(['ID',x[0][x[0].selectedIndex].innerHTML, y[0][y[0].selectedIndex].innerHTML, 'Author' , bubble[0][bubble[0].selectedIndex].innerHTML]);//size gradient
    Totales.push(['ID',x[0][x[0].selectedIndex].innerHTML, y[0][y[0].selectedIndex].innerHTML , bubble[0][bubble[0].selectedIndex].innerHTML]);//color gradient
    $.each(Authors,function(key, value){
        if(key!='Promoted' && $('#'+key.replace(/\s/g, ''))[0].checked){
            //Totales.push([key,Authors[key][x[0].value],Authors[key][y[0].value],key,Authors[key][bubble[0].value]]);//size gradient
            Totales.push([key,Authors[key][x[0].value],Authors[key][y[0].value],Authors[key][bubble[0].value]]);//color gradient
        }
    });
           
    //console.log(Totales);
    
    var data = google.visualization.arrayToDataTable(Totales);
    
    global_chart = new google.visualization.ChartWrapper({
        chartType: 'BubbleChart',
        containerId: 'creativos_chart_div',
        dataTable: data,
        options: {
            title: 'Metricas Comparativas',
            hAxis: {title: x[0][x[0].selectedIndex].innerHTML},
            vAxis: {title: y[0][y[0].selectedIndex].innerHTML},
            bubble: {
                textStyle: {fontSize: 11}
            },
            legend: {position: 'none'}
        }
    });
    
    ///////Show hide series
    var columns = [0];
    
    var seriesMap=[];
    for(var i=1; i<Totales[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    global_chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = global_chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                global_chart.setView(view);
                global_chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(global_chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    ///////Show hide series
    
    global_chart.draw();
    
    /*<div class="checkbox checkbox-primary">
        <input id="checkbox2" type="checkbox" checked="">
        <label for="checkbox2">
            Primary
        </label>
    </div>*/
}

//Ventas
var ventas;
function handleVentas(arr_ventas){
	ventas = [];
	for(var i=0;i<arr_ventas.length;i++){
        if(arr_ventas[i][3] != '' && typeof arr_ventas[i][3] !== 'undefined'){
            ventas.push(
                {
                    'client': arr_ventas[i][0],
                    'campaign': arr_ventas[i][1],
                    'agency': arr_ventas[i][2],
                    'agent': arr_ventas[i][3],
                    'sell_date': arr_ventas[i][4],
                    'sell_year': arr_ventas[i][5],
                    'sell_q': arr_ventas[i][6],
                    'run_date': arr_ventas[i][7],
                    'invoice_date': arr_ventas[i][8],
                    'pay_date': arr_ventas[i][9],
                    'amount': arr_ventas[i][10], 
                    'status': arr_ventas[i][11], 
                    'days_due': arr_ventas[i][12], 
                    'days_left': arr_ventas[i][13],
                    'invoice_num': arr_ventas[i][14],
                    'odc': arr_ventas[i][15],
                    'note': arr_ventas[i][16],
                    'contact': arr_ventas[i][17],
                    'cc': arr_ventas[i][18],
                    'rebate': arr_ventas[i][20]
                }
            )
        }
    }
}

var data_array;
function VentasLineChartDataArray(category,x){
    var header_row = [x,...new Set(ventas.map(item => item[category]))];
    let rows_values = [...new Set(ventas.map(item => item[x]))];
    data_array = [header_row];
    for(var i=0;i<rows_values.length;i++){
        data_array.push([rows_values[i]]);
        for(var j=0;j<header_row.length-1;j++){
            data_array[i+1].push(0);
        }
    }
    for(var i=0;i<ventas.length;i++){
        var column=header_row.indexOf(ventas[i][category]);
        var row=rows_values.indexOf(ventas[i][x])+1;
        data_array[row][column]+=ventas[i].amount;
    }
    VentasDrawLineChart(data_array,category,x);
}
function VentasDrawLineChart(data_array,category,variable) {
    var data = google.visualization.arrayToDataTable(data_array);

    var chart = new google.visualization.ChartWrapper({
        chartType: 'LineChart',
        containerId: 'ventas_chart_div',
        dataTable: data,
        options: {
            title: category+' vs. '+variable,
            legend: { position: 'right' },
        }
    });
    
    ///////Show hide series
    var columns = [0];
    /* the series map is an array of data series
     * "column" is the index of the data column to use for the series
     * "roleColumns" is an array of column indices corresponding to columns with roles that are associated with this data series
     * "display" is a boolean, set to true to make the series visible on the initial draw
     */
    /*var seriesMap = [{
        column: 1,
        roleColumns: [2],
        display: true
    }, {
        column: 3,
        roleColumns: [4, 5],
        display: true
    }, {
        column: 6,
        roleColumns: [7],
        display: false
    }];*/
    
    var seriesMap=[];
    for(var i=1; i<data_array[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}
function VentasBarChartDataArray(category,x){
   var header_row = [x,...new Set(ventas.map(item => item[category]))];
    header_row.push({role: 'annotation'});
    let rows_values = [...new Set(ventas.map(item => item[x]))];
    data_array = [header_row];
    for(var i=0;i<rows_values.length;i++){
        data_array.push([rows_values[i]]);
        for(var j=0;j<header_row.length-2;j++){
            data_array[i+1].push(0);
        }
        data_array[i+1].push('');
    }
    for(var i=0;i<ventas.length;i++){
        var column=header_row.indexOf(ventas[i][category]);
        var row=rows_values.indexOf(ventas[i][x])+1;
        data_array[row][column]+=ventas[i].amount;
    }
    VentasDrawBarChart(data_array,category,x);
}
function VentasDrawBarChart(data_array,category,variable){
    var data = google.visualization.arrayToDataTable(data_array);

    var chart = new google.visualization.ChartWrapper({
        chartType: 'ColumnChart',
        containerId: 'ventas_chart_div',
        dataTable: data,
        options: {
            title: category+' vs. '+variable,
            legend: { position: 'right' },
            isStacked: $('#ventas_percentage')[0].checked ? 'percent' : true
        }
    });
    
    ///////Show hide series
    var columns = [0];
    /* the series map is an array of data series
     * "column" is the index of the data column to use for the series
     * "roleColumns" is an array of column indices corresponding to columns with roles that are associated with this data series
     * "display" is a boolean, set to true to make the series visible on the initial draw
     */
    /*var seriesMap = [{
        column: 1,
        roleColumns: [2],
        display: true
    }, {
        column: 3,
        roleColumns: [4, 5],
        display: true
    }, {
        column: 6,
        roleColumns: [7],
        display: false
    }];*/
    
    var seriesMap=[];
    for(var i=1; i<data_array[0].length;i++){
        seriesMap.push(
            {
                column: i,
                roleColumns: [],
                display: true
            }
        )
    }
    
    var columnsMap = {};
    var series = [];
    for (var i = 0; i < seriesMap.length; i++) {
        var col = seriesMap[i].column;
        columnsMap[col] = i;
        // set the default series option
        series[i] = {};
        if (seriesMap[i].display) {
            // if the column is the domain column or in the default list, display the series
            columns.push(col);
        }
        else {
            // otherwise, hide it
            columns.push({
                label: data.getColumnLabel(col),
                type: data.getColumnType(col),
                sourceColumn: col,
                calc: function () {
                    return null;
                }
            });
            // backup the default color (if set)
            if (typeof(series[i].color) !== 'undefined') {
                series[i].backupColor = series[i].color;
            }
            series[i].color = '#CCCCCC';
        }
        for (var j = 0; j < seriesMap[i].roleColumns.length; j++) {
            columns.push(seriesMap[i].roleColumns[j]);
        }
    }
    
    chart.setOption('series', series);
    
    function showHideSeries () {
        var sel = chart.getChart().getSelection();
        // if selection length is 0, we deselected an element
        if (sel.length > 0) {
            // if row is undefined, we clicked on the legend
            if (sel[0].row == null) {
                var col = sel[0].column;
                if (typeof(columns[col]) == 'number') {
                    var src = columns[col];
                    
                    // hide the data series
                    columns[col] = {
                        label: data.getColumnLabel(src),
                        type: data.getColumnType(src),
                        sourceColumn: src,
                        calc: function () {
                            return null;
                        }
                    };
                    
                    // grey out the legend entry
                    series[columnsMap[src]].color = '#CCCCCC';
                }
                else {
                    var src = columns[col].sourceColumn;
                    
                    // show the data series
                    columns[col] = src;
                    series[columnsMap[src]].color = null;
                }
                var view = chart.getView() || {};
                view.columns = columns;
                chart.setView(view);
                chart.draw();
            }
        }
    }
    
    google.visualization.events.addListener(chart, 'select', showHideSeries);
    
    // create a view with the default columns
    var view = {
        columns: columns
    };
    
    chart.draw();
}
function GraphVentas(){
    if($('#ventas_bar')[0].checked){
        VentasBarChartDataArray($('#ventas_kpi1')[0].value,$('#ventas_kpi2')[0].value);
    }
    else{
        VentasLineChartDataArray($('#ventas_kpi1')[0].value,$('#ventas_kpi2')[0].value);
    }
}