//active page
function getActivePage(){
	$(document).ready(function(){
		var page_url=window.location.href;
		//console.log($('#sidebar-menu')[0].childNodes[1].childNodes[1]);
		for(var i=3;i<$('#sidebar-menu')[0].childNodes[1].childNodes.length;i+=2){
			var sidebar_tag=$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].href;
			//console.log(sidebar_tag,page_url);
			if(sidebar_tag.indexOf(page_url) > -1){
				$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].className = "waves-effect active";
			}
			//console.log(i);
		}
	});
}
//active page


//FAN, ADX & ADMAN Revenue
//Total Revenue
function totalRevenue(){
	$('#totalRevenue')[0].innerHTML = numberWithCommas((parseFloat($('#taboolaRevenue')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#admanRevenue')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#FBRevenue')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#adx_revenue_amount')[0].innerHTML.replace(/,/g,''))).toFixed(2));
	$('#month_totalRevenue')[0].innerHTML = numberWithCommas((parseFloat($('#month_taboolaRevenue')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#month_admanRevenue')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#month_FBRevenue')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#adx_month_revenue')[0].innerHTML.replace(/,/g,''))).toFixed(2));
	$('#totalRevenue_last_month')[0].innerHTML = numberWithCommas((parseFloat($('#taboolaRevenue_last_month')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#admanRevenue_last_month')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#FBRevenue_last_month')[0].innerHTML.replace(/,/g,''))*19.5+parseFloat($('#adx_revenue_last_month')[0].innerHTML.replace(/,/g,''))).toFixed(2));
}
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
//ADX Revenue
function getNetADXRevenue(since,until){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: since,
			endDate: until,
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_CTR","AD_REQUESTS_RPM"],
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			document.getElementById("adx_revenue_amount").innerHTML=numberWithCommas(parseFloat(resp.totals[0]).toFixed(2));
			document.getElementById("ADX_coverage").innerHTML=numberWithCommas((100*parseFloat(resp.totals[1])).toFixed(2));
			document.getElementById("ADX_ads").innerHTML=numberWithCommas(parseInt(resp.totals[2]));
			document.getElementById("ADX_CTR").innerHTML=numberWithCommas((100*parseFloat(resp.totals[3])).toFixed(2));
			document.getElementById("ADX_CPM").innerHTML=numberWithCommas(parseFloat(resp.totals[4]).toFixed(2));
		});
	});
}
function getPrivateAuctionADXRevenue(since,until){	
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: since,
			endDate: until,
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_RPM"],
			filter: "TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction",
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			//console.log(resp);
			document.getElementById("private_ADX_revenue").innerHTML= !isNaN(parseFloat(resp.totals[0])) ? numberWithCommas(parseFloat(resp.totals[0]).toFixed(2)) : '0.00';
			document.getElementById("private_ADX_coverage").innerHTML= !isNaN(parseFloat(resp.totals[1])) ? numberWithCommas(100*parseFloat(resp.totals[1]).toFixed(2)) : 'NA';
			document.getElementById("private_ADX_ads").innerHTML= !isNaN(parseFloat(resp.totals[2])) ? numberWithCommas(parseInt(resp.totals[2])) : '0';
			document.getElementById("private_ADX_CPM").innerHTML= !isNaN(parseFloat(resp.totals[3])) ? numberWithCommas(parseFloat(resp.totals[3]).toFixed(2)) : 'NA';
		});
	});
}
function getPreferredDealsADXRevenue(since,until){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: since,
			endDate: until,
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_RPM"],
			filter: "TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal",
			fields: "totals"
		});
		requestRevenue.execute(function(resp) {
			//console.log(resp);
			document.getElementById("preferred_ADX_revenue").innerHTML= !isNaN(parseFloat(resp.totals[0])) ? numberWithCommas(parseFloat(resp.totals[0]).toFixed(2)) : '0.00';
			document.getElementById("preferred_ADX_coverage").innerHTML= !isNaN(parseFloat(resp.totals[1])) ? numberWithCommas(parseFloat(resp.totals[1]).toFixed(2)) : 'NA';
			document.getElementById("preferred_ADX_ads").innerHTML= !isNaN(parseFloat(resp.totals[2])) ? numberWithCommas(parseInt(resp.totals[2]))  : '0';
			document.getElementById("preferred_ADX_CPM").innerHTML= !isNaN(parseFloat(resp.totals[3])) ? numberWithCommas(parseFloat(resp.totals[3]).toFixed(2))  : 'NA';
		});
	});
}
//FAN Revenue
function getFBRevenue(since,until){
	//console.log(since);
	FB.api(
		'/980493918670682/app_insights/app_event/',
		'GET',
		{"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":since,"until":until},
		function(response) {
			var revenue=0;
			for(var i=0;i<response.data.length;i++){
				revenue+=parseFloat(response.data[i].value);
			}
			$('#FBRevenue')[0].innerHTML = numberWithCommas(revenue.toFixed(2));
		}
	);
	totalRevenue();
}
//ADMAN Revenue
function admanRevenue(since,until){
	var adman_revenue=0;
    $.ajax({
        url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+since.replace(/-/g,'')+'&to='+until.replace(/-/g,'')+'&page=1&per_page=40',
        type: 'get',
        async: true,
        success: function(response){
            if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
                adman_revenue = response.totals.impression_USD/1000;
                $('#admanRevenue')[0].innerHTML = numberWithCommas(adman_revenue.toFixed(2));
            }
        }
    });
	totalRevenue();
}
function admanRevenueMonth(){
	var adman_month_revenue=0;
	var adman_revenue_last_month=0;
	var adman_revenue_last_month_day=0;
	var nowDate = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var now=parseInt(new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000).toISOString().replace(/-/g,''));
	var startOfMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth(),1).toISOString().replace(/-/g,''));
	var startOfLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth()-1,1).toISOString().replace(/-/g,''));
	var nowLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth()-1,nowDate.getDate()).toISOString().replace(/-/g,''));
	var endOfLastMonth = parseInt(new Date(nowDate.getFullYear(),nowDate.getMonth(),0).toISOString().replace(/-/g,''));
	$.ajax({
        url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+startOfMonth+'&to='+now+'&page=1&per_page=40',
        type: 'get',
        async: true,
        success: function(response){
            if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
                adman_month_revenue = response.totals.impression_USD/1000;
                $('#month_admanRevenue')[0].innerHTML = numberWithCommas(adman_month_revenue.toFixed(2));
            }
        }
    });
    $.ajax({
        url: 'https://irvine.admanmedia.com/api/v1/pablo_charlies?signature=Wx17zAee%2B94zsTP7Lhcczaf1e27o%2BhIIH7hSaeTWq1UvKU4rR2cQ3Ty4yeni%0AFJeSh%2FZj9AjNweXugeBE%2B%2FhJ%2BoX%2Fn3K3kW2aD9L5bDszjMlwQImbDlWHuzsv%0Ah4XjyThNjNL%2B53A%2FCyCdUAKgkVcUgMSmJxXONfeXF0o5m0fAqiAZ70FRSeHt%0ACszTLgLlqlHg0Q17pOaII%2BsrPtsofDJ47RbS8SZ3kIuyDqf%2BLKX3TluRe4Vo%0AtKCbBkzeYY4WlKcY3upNTul9ds5vP%2BQVnLI%2FIA2YFQ4UQcBCdv2gNpmsWh5n%0ATDurY%2FpJHXBekOlS6dEj82XrUJaglCS2fmIOWSDHDO1c3EXP4zikWYHqT0yM%0Akxo%3D%0A&pmu=1dedb463&counts=preimpression,gross_impression&amounts=impression_USD&from='+startOfLastMonth+'&to='+endOfLastMonth+'&page=1&per_page=40',
        type: 'get',
        async: true,
        success: function(response){
            if(!typeof response.errors !== 'undefined' && typeof response.totals.impression_USD !== 'undefined'){
                adman_revenue_last_month = response.totals.impression_USD/1000;
                $('#admanRevenue_last_month')[0].innerHTML = numberWithCommas(adman_revenue_last_month.toFixed(2));
                totalRevenue();
            }
        }
    });
}
//FAN, ADX & ADMAN Revenue

//Taboola Revenue
var taboola_month, taboola_last_month, taboola_range;
var taboola_month_revenue, taboola_last_month_revenue, taboola_range_revenue;
function getMonthTaboolaRevenue(){
	var nowDate = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var now=nowDate.toISOString().substr(0,10);
	var startOfMonth = new Date(nowDate.getFullYear(),nowDate.getMonth(),1).toISOString().substr(0,10);
	var startOfLastMonth = new Date(nowDate.getFullYear(),nowDate.getMonth()-1,1).toISOString().substr(0,10);
	var endOfLastMonth = new Date(nowDate.getFullYear(),nowDate.getMonth(),0).toISOString().substr(0,10);
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				data: {'start_date': startOfMonth, 'end_date': now, 'access_token': response.access_token},
				success: function(response){
					taboola_month = JSON.parse(response);
					//console.log(taboola_month);
					taboola_month_revenue=0;
					for(var i=0;i<taboola_month.results.length;i++){
						taboola_month_revenue+=taboola_month.results[i].ad_revenue;
					}
					$('#month_taboolaRevenue')[0].innerHTML = numberWithCommas(taboola_month_revenue.toFixed(2));
				}
			});
		}
	});
		
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				data: {'start_date': startOfLastMonth, 'end_date': endOfLastMonth, 'access_token': response.access_token},
				success: function(response){
					taboola_last_month = JSON.parse(response);
					//console.log(taboola_last_month);
					taboola_last_month_revenue=0;
					for(var i=0;i<taboola_last_month.results.length;i++){
						taboola_last_month_revenue+=taboola_last_month.results[i].ad_revenue;
					}
					$('#taboolaRevenue_last_month')[0].innerHTML = numberWithCommas(taboola_last_month_revenue.toFixed(2));
				}
			});
		}
	});
}
function getTaboolaRevenue(since,until){
	$.ajax({
		url: 'php_scripts/taboola_auth.php',
		type: 'GET',
		dataType: 'json',
		success: function(response){
			$.ajax({
				url: 'php_scripts/taboola_request.php',
				type: 'GET',
				data: {'start_date': since, 'end_date': until, 'access_token': response.access_token},
				success: function(response){
					taboola_range = JSON.parse(response);
					//console.log(taboola_last_month);
					taboola_range_revenue=0;
					for(var i=0;i<taboola_range.results.length;i++){
						taboola_range_revenue+=taboola_range.results[i].ad_revenue;
					}
					$('#taboolaRevenue')[0].innerHTML = numberWithCommas(taboola_range_revenue.toFixed(2));
				}
			});
		}
	});
}
//Taboola Revenue

//FAN month revenue
function getMonthsFBRevenue(){
	var FBrevenue=0;
	var last_month_total_FBrevenue=0;
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var start = new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+'02';
	var last_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+1)+'-'+'02';
	var this_month_start = new Date(now.getFullYear(), now.getMonth(), 1).toISOString().substr(0,10);
	var last_month_day = new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000+24*60*60*1000);
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":start},
	  function(response) {
		for(var i=0;i<response.data.length;i++){
			FBrevenue+=parseFloat(response.data[i].value);
		}
		//console.log(FBrevenue);
		$('#month_FBRevenue')[0].innerHTML = numberWithCommas(FBrevenue.toFixed(2));
	  }
	);
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': this_month_start},
	  function(response) {
	  	for(var i=0;i<response.data.length;i++){
			last_month_total_FBrevenue+=parseFloat(response.data[i].value);
		}
		document.getElementById("FBRevenue_last_month").innerHTML = numberWithCommas(last_month_total_FBrevenue.toFixed(2));
	  }
	);	
	totalRevenue();
}
function getMonthADXRevenue(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	
	var start_this_month = now;
	start_this_month.setDate(1);
	var this_month_start = start_this_month.toISOString().substr(0,10);
	
	var end_last_month = now;
	end_last_month.setDate(0);
	var last_month_end=end_last_month.toISOString().substr(0,10);
	
	var start_last_month = end_last_month;
	start_last_month.setDate(1);
	var last_month_start=start_last_month.toISOString().substr(0,10);
	

	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestMonthRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: 'startOfMonth',
			endDate: 'today',
			metric: ["Earnings"],
			fields: "totals"
		});
		requestMonthRevenue.execute(function(resp) {
			$('#adx_month_revenue')[0].innerHTML=numberWithCommas(parseFloat(resp.totals[0]).toFixed(2));
		});
		var requestLastMonthRevenue = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: last_month_start,
			endDate: last_month_end,
			metric: ["Earnings"],
			fields: "totals"
		});
		requestLastMonthRevenue.execute(function(resp) {
			$('#adx_revenue_last_month')[0].innerHTML=numberWithCommas(parseFloat(resp.totals[0]).toFixed(2));
		});
	});
	totalRevenue();
}
//FAN month revenue

//FB Reach
function getFBReach(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var this_month=new Date(now.getFullYear(),now.getMonth(),28);
	var last_month=new Date(now.getFullYear(),now.getMonth()-1,28);
	//var year=now.getFullYear();
	//var month=now.getMonth();
	FB.api(
		'/eldeforma/insights/page_impressions_by_paid_non_paid_unique/days_28/',
		'GET',
		{'fields':'values','since':this_month.toISOString().substr(0,10),'until':this_month.toISOString().substr(0,10)+'+5days'},
		function(response){
			//console.log(response);
			var reach = response.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value.total;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
			//console.log(numberWithCommas(reach[max].value.total));
	    	$("#reach_month")[0].innerHTML = numberWithCommas(reach[max].value.total);
		}
	);
	FB.api(
		'/eldeforma/insights/page_impressions_by_paid_non_paid_unique/days_28/',
		'GET',
		{'fields':'values','since':last_month.toISOString().substr(0,10),'until':last_month.toISOString().substr(0,10)+'+5days'},
		function(response){
			//console.log(response);
			var reach = response.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value.total;
				return values;
			});
			//console.log(values_array);
			var max = values_array.indexOf(Math.max(...values_array));
			//console.log(numberWithCommas(reach[max].value.total));
	    	$("#reach_last_month")[0].innerHTML = numberWithCommas(reach[max].value.total);
		}
	);
}
function getFBReachRange(since,until){
	//var year=now.getFullYear();
	//var month=now.getMonth();
	FB.api(
		'/eldeforma/insights/page_impressions_by_paid_non_paid_unique/day/',
		'GET',
		{'fields':'values','since':since+'-1days','until':until+'+1days'},
		function(response){
			//console.log(response);
			var reach = response.data[0].values;
	    	//console.log(reach);
	    	var values_array = reach.map(function(obj){
				var values = obj.value.total;
				return values;
			});
			console.log(values_array);
			//var max = values_array.indexOf(Math.max(...values_array));
			//console.log(numberWithCommas(reach[max].value.total));
	    	$("#max_reach")[0].innerHTML = numberWithCommas(math.max(values_array));
	    	$("#min_reach")[0].innerHTML = numberWithCommas(math.max(values_array));
	    	$("#mean_reach")[0].innerHTML = numberWithCommas(math.mean(values_array).toFixed(0));
	    	$("#sd_reach")[0].innerHTML = numberWithCommas(math.std(values_array).toFixed(0));
		}
	);
}
//FB Reach

//Month Shares
function rangeShares(since,until){
	since+='T0'+new Date().getTimezoneOffset()/60+':00:00';
	until+='T0'+new Date().getTimezoneOffset()/60+':00:00+1day';
	
	FB.api(
		'/eldeforma/posts',
		'GET',
		{"fields":"shares,created_time","since":since,"until":until,"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				SharesRangenextPage(response.paging.next,posts);
			}
		}
	);
}
function SharesRangenextPage(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				SharesnextPage(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$('#shares_amount')[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}
function monthShares(){
	var now = new Date();
	var first = new Date(now.getFullYear(), now.getMonth()-0, 1);
	var last = new Date(now.getFullYear(), now.getMonth()+1, 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/eldeforma/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				SharesnextPage(response.paging.next,posts);
			}
		}
	);
}
function SharesnextPage(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				SharesnextPage(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$('#shares_month')[0].innerHTML = numberWithCommas(shares);
				lastmonthShares();
			}
		}
	});
}
function lastmonthShares(){
	var now = new Date();
	var first = new Date(now.getFullYear(), now.getMonth()-1, 1);
	var last = new Date(now.getFullYear(), now.getMonth()+0, 0);
	last = new Date(last.getTime() + 1000*60*60*24);
	FB.api(
		'/eldeforma/posts',
		'GET',
		{"fields":"shares,created_time","since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var posts=[];
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				lastMonthSharesnextPage(response.paging.next,posts);
			}
		}
	);
}
function lastMonthSharesnextPage(nextURL,posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				lastMonthSharesnextPage(response.paging.next,posts);
			}
			else{
				var shares=0;
				for(var j=0;j<posts.length;j++){
					if(typeof posts[j].shares !== 'undefined'){
						shares+=posts[j].shares.count;
						//console.log(posts[j].shares.count);	
					}
				}
				$('#shares_last_month')[0].innerHTML = numberWithCommas(shares);
			}
		}
	});
}
//Month Shares

//Facebook Spend
function getFBSpend(){
	FB.api(
		'/act_986080914767243/insights',
		'GET',
		{"date_preset":"this_month","fields":"spend,account_name"},
		function(response) {
			$('#fb_spend_content')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
	FB.api(
		'/act_986080914767243/insights',
		'GET',
		{"date_preset":"yesterday","fields":"spend,account_name"},
		function(response) {
			$('#yesterday_fb_spend_content')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
	FB.api(
		'/act_986080914767243/insights',
		'GET',
		{"date_preset":"last_month","fields":"spend,account_name"},
		function(response) {
			$('#fb_spend_content_last_month')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
	FB.api(
		'/act_955415581167110/insights',
		'GET',
		{"date_preset":"this_month","fields":"spend,account_name"},
		function(response) {
			$('#fb_spend_growth')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
	FB.api(
		'/act_955415581167110/insights',
		'GET',
		{"date_preset":"yesterday","fields":"spend,account_name"},
		function(response) {
			$('#yesterday_fb_spend_growth')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
	FB.api(
		'/act_955415581167110/insights',
		'GET',
		{"date_preset":"last_month","fields":"spend,account_name"},
		function(response) {
			$('#fb_spend_growth_last_month')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
}
function getFBSpendRange(since,until){
	FB.api(
		'/act_986080914767243/insights/',
		'GET',
		{"time_range":{"since":since,"until":until}},
		function(response) {
		  $('#yesterday_fb_spend_content')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
	FB.api(
		'/act_955415581167110/insights/',
		'GET',
		{"time_range":{"since":since,"until":until}},
		function(response) {
		  $('#yesterday_fb_spend_growth')[0].innerHTML = numberWithCommas((typeof response.data[0] === 'undefined' ? (0).toFixed(2) : response.data[0].spend));
		}
	);
}
//Facebook Spend

//Month Video Views
function monthVideoViews(){
	var now = new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var first = new Date(now.getFullYear(), now.getMonth()-0, 0);
	var last = new Date(now.getFullYear(), now.getMonth()+1, 1);
	var last_month_first = new Date(now.getFullYear(), now.getMonth()-1, 0);
	var last_month_last = new Date(now.getFullYear(), now.getMonth()+0, 1);
	FB.api(
		'/eldeforma/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('deforma_video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/eldeforma/insights/page_video_views/day',
		'GET',
		{"since":last_month_first.toISOString().substring(0,10),"until":last_month_last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('deforma_video_views_last_month').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/repsodia/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('repsodia_video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/repsodia/insights/page_video_views/day',
		'GET',
		{"since":last_month_first.toISOString().substring(0,10),"until":last_month_last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('repsodia_video_views_last_month').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/techcult/insights/page_video_views/day',
		'GET',
		{"since":first.toISOString().substring(0,10),"until":last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('techcult_video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/techcult/insights/page_video_views/day',
		'GET',
		{"since":last_month_first.toISOString().substring(0,10),"until":last_month_last.toISOString().substring(0,10),"limit":"100"},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('techcult_video_views_last_month').innerHTML = numberWithCommas(total_videos);
		}
	);
}
function rangeVideoViews(since,until){
	FB.api(
		'/eldeforma/insights/page_video_views/day',
		'GET',
		{"since":since,"until":until+'+1days'},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('range_deforma_video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/repsodia/insights/page_video_views/day',
		'GET',
		{"since":since,"until":until+'+1days'},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('range_repsodia_video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
	FB.api(
		'/techcult/insights/page_video_views/day',
		'GET',
		{"since":since,"until":until+'+1days'},
		function(response) {
			//console.log(response);
			var total_videos=0;
			for(var i=0;i<response.data[0].values.length;i++){
				total_videos+=response.data[0].values[i].value;
			}
			document.getElementById('range_techcult_video_views_amount').innerHTML = numberWithCommas(total_videos);
		}
	);
}
//Month Video Views

//ADX Month Metrics
function getADXMonthMetrics(){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestADX = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: 'startOfMonth',
			endDate: 'today',
			metric: ["AD_REQUESTS_COVERAGE","AD_REQUESTS_RPM","AD_IMPRESSIONS","AD_REQUESTS_CTR"],
			fields: "totals"
		});
		requestADX.execute(function(resp) {
			//console.log(resp.totals);
			$('#month_ADX_coverage')[0].innerHTML = (100*parseFloat(resp.totals[0])).toFixed(2);
			$('#month_ADX_CPM')[0].innerHTML = numberWithCommas(parseFloat(resp.totals[1]).toFixed(2));
			$('#month_ADX_ads')[0].innerHTML = numberWithCommas(parseFloat(resp.totals[2]));
			$('#month_ADX_CTR')[0].innerHTML = (100*parseFloat(resp.totals[3])).toFixed(2);
			
		});
		var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
		var requestADXFullLastMonth = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			endDate: new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10),
			metric: ["AD_REQUESTS_COVERAGE","AD_REQUESTS_RPM","AD_IMPRESSIONS","AD_REQUESTS_CTR"],
			fields: "totals"
		});
		requestADXFullLastMonth.execute(function(resp) {
			//console.log(resp.totals);
			$('#last_month_ADX_coverage')[0].innerHTML = (100*parseFloat(resp.totals[0])).toFixed(2);
			$('#last_month_ADX_CPM')[0].innerHTML = numberWithCommas(parseFloat(resp.totals[1]).toFixed(2));
			$('#last_month_ADX_ads')[0].innerHTML = numberWithCommas(parseFloat(resp.totals[2]));
			$('#last_month_ADX_CTR')[0].innerHTML = (100*parseFloat(resp.totals[3])).toFixed(2);
		});
	});
}
function getPrivateAuctionMonthADXMetrics(){	
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestMonthMetrics = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: 'startOfMonth',
			endDate: 'today',
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_RPM"],
			filter: "TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction",
			fields: "totals"
		});
		requestMonthMetrics.execute(function(resp) {
			//console.log(resp);
			document.getElementById("private_month_ADX_revenue").innerHTML= !isNaN(parseFloat(resp.totals[0])) ? numberWithCommas(parseFloat(resp.totals[0]).toFixed(2)) : '0.00';
			document.getElementById("private_month_ADX_coverage").innerHTML= !isNaN(parseFloat(resp.totals[1])) ? numberWithCommas(100*parseFloat(resp.totals[1]).toFixed(2)) : 'NA';
			document.getElementById("private_month_ADX_ads").innerHTML= !isNaN(parseFloat(resp.totals[2])) ? numberWithCommas(parseInt(resp.totals[2])) : '0';
			document.getElementById("private_month_ADX_CPM").innerHTML= !isNaN(parseFloat(resp.totals[3])) ? numberWithCommas(parseFloat(resp.totals[3]).toFixed(2)) : 'NA';
		});
		var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
		var requestLastMonthMetrics = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			endDate: new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10),
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_RPM"],
			filter: "TRANSACTION_TYPE_NAME==Subasta privada,TRANSACTION_TYPE_NAME==Private auction",
			fields: "totals"
		});
		requestLastMonthMetrics.execute(function(resp) {
			//console.log(resp);
			document.getElementById("private_last_month_ADX_revenue").innerHTML= !isNaN(parseFloat(resp.totals[0])) ? numberWithCommas(parseFloat(resp.totals[0]).toFixed(2)) : '0.00';
			document.getElementById("private_last_month_ADX_coverage").innerHTML= !isNaN(parseFloat(resp.totals[1])) ? numberWithCommas(100*parseFloat(resp.totals[1]).toFixed(2)) : 'NA';
			document.getElementById("private_last_month_ADX_ads").innerHTML= !isNaN(parseFloat(resp.totals[2])) ? numberWithCommas(parseInt(resp.totals[2])) : '0';
			document.getElementById("private_last_month_ADX_CPM").innerHTML= !isNaN(parseFloat(resp.totals[3])) ? numberWithCommas(parseFloat(resp.totals[3]).toFixed(2)) : 'NA';
		});
	});
}
function getPreferredDealsMonthADXMetrics(){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestMonthMetrics = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: 'startOfMonth',
			endDate: 'today',
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_RPM"],
			filter: "TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal",
			fields: "totals"
		});
		requestMonthMetrics.execute(function(resp) {
			//console.log(resp);
			document.getElementById("preferred_month_ADX_revenue").innerHTML= !isNaN(parseFloat(resp.totals[0])) ? numberWithCommas(parseFloat(resp.totals[0]).toFixed(2)) : '0.00';
			document.getElementById("preferred_month_ADX_coverage").innerHTML= !isNaN(parseFloat(resp.totals[1])) ? numberWithCommas(100*parseFloat(resp.totals[1]).toFixed(2)) : 'NA';
			document.getElementById("preferred_month_ADX_ads").innerHTML= !isNaN(parseFloat(resp.totals[2])) ? numberWithCommas(parseInt(resp.totals[2]))  : '0';
			document.getElementById("preferred_month_ADX_CPM").innerHTML= !isNaN(parseFloat(resp.totals[3])) ? numberWithCommas(parseFloat(resp.totals[3]).toFixed(2))  : 'NA';
		});
		var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
		var requestLastMonthMetrics = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			endDate: new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10),
			metric: ["Earnings","AD_REQUESTS_COVERAGE","AD_IMPRESSIONS","AD_REQUESTS_RPM"],
			filter: "TRANSACTION_TYPE_NAME==Acuerdo preferente,TRANSACTION_TYPE_NAME==Preferred deal",
			fields: "totals"
		});
		requestLastMonthMetrics.execute(function(resp) {
			//console.log(resp);
			document.getElementById("preferred_last_month_ADX_revenue").innerHTML= !isNaN(parseFloat(resp.totals[0])) ? numberWithCommas(parseFloat(resp.totals[0]).toFixed(2)) : '0.00';
			document.getElementById("preferred_last_month_ADX_coverage").innerHTML= !isNaN(parseFloat(resp.totals[1])) ? numberWithCommas(100*parseFloat(resp.totals[1]).toFixed(2)) : 'NA';
			document.getElementById("preferred_last_month_ADX_ads").innerHTML= !isNaN(parseFloat(resp.totals[2])) ? numberWithCommas(parseInt(resp.totals[2]))  : '0';
			document.getElementById("preferred_last_month_ADX_CPM").innerHTML= !isNaN(parseFloat(resp.totals[3])) ? numberWithCommas(parseFloat(resp.totals[3]).toFixed(2))  : 'NA';
		});
	});
}
//ADX Month Metrics

//FAN Month Metrics
function getMonthsFANMetrics(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var start = new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+'02';
	var last_month_start = new Date(now.getFullYear(),now.getMonth(),0).getFullYear()+'-'+(new Date(now.getFullYear(),now.getMonth(),0).getMonth()+1)+'-'+'02';
	var this_month_start = new Date(now.getFullYear(), now.getMonth(), 1).toISOString().substr(0,10);
	var last_month_day = new Date(now.getTime()-new Date(now.getFullYear(), now.getMonth(), 0).getDate()*24*60*60*1000+24*60*60*1000);
	//Coverage
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"COUNT","event_name":"fb_ad_network_request","summary":"true","since":start},
	  function(response) {
	  	var requests=0;
		for(var i=0;i<response.data.length;i++){
			requests+=parseInt(response.data[i].value);
		}
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_request","summary":"true","since":start},
		  function(response) {
		  	var filled=0;
			for(var i=0;i<response.data.length;i++){
				filled+=parseInt(response.data[i].value);
			}
			$('#month_FAN_coverage')[0].innerHTML = (100*filled/requests).toFixed(2);
		  }
		);
	  }
	);
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"COUNT","event_name":"fb_ad_network_request","summary":"true","since":last_month_start, 'until': this_month_start},
	  function(response) {
	  	var last_requests=0;
		for(var i=0;i<response.data.length;i++){
			last_requests+=parseInt(response.data[i].value);
		}
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_request","summary":"true","since":last_month_start, 'until': this_month_start},
		  function(response) {
		  	var last_filled=0;
			for(var i=0;i<response.data.length;i++){
				last_filled+=parseInt(response.data[i].value);
			}
			$('#last_month_FAN_coverage')[0].innerHTML = (100*last_filled/last_requests).toFixed(2);
		  }
		);
	  }
	);
	//impressions, CTR, CPM
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"COUNT","event_name":"fb_ad_network_imp","summary":"true","since":start},
	  function(response) {
	  	var imp=0;
		for(var i=0;i<response.data.length;i++){
			imp+=parseInt(response.data[i].value);
		}
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"COUNT","event_name":"fb_ad_network_click","summary":"true","since":start},
		  function(response) {
		  	var clicks=0;
			for(var i=0;i<response.data.length;i++){
				clicks+=parseInt(response.data[i].value);
			}
			$('#month_FAN_CTR')[0].innerHTML = (100*clicks/imp).toFixed(2);
			$('#month_FAN_ads')[0].innerHTML = numberWithCommas(imp);
		  }
		);
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":start},
		  function(response) {
		  	var revenue=0;
			for(var i=0;i<response.data.length;i++){
				revenue+=parseInt(response.data[i].value);
			}
			$('#month_FAN_CPM')[0].innerHTML = (1000*revenue/imp).toFixed(2);
		  }
		);
	  }
	);
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"COUNT","event_name":"fb_ad_network_imp","summary":"true","since":last_month_start, 'until': this_month_start},
	  function(response) {
	  	var last_imp=0;
		for(var i=0;i<response.data.length;i++){
			last_imp+=parseInt(response.data[i].value);
		}
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"COUNT","event_name":"fb_ad_network_click","summary":"true","since":last_month_start, 'until': this_month_start},
		  function(response) {
		  	var last_clicks=0;
			for(var i=0;i<response.data.length;i++){
				last_clicks+=parseInt(response.data[i].value);
			}
			$('#last_month_FAN_CTR')[0].innerHTML = (100*last_clicks/last_imp).toFixed(2);
			$('#last_month_FAN_ads')[0].innerHTML = numberWithCommas(last_imp);
		  }
		);
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":last_month_start, 'until': this_month_start},
		  function(response) {
		  	var last_revenue=0;
			for(var i=0;i<response.data.length;i++){
				last_revenue+=parseInt(response.data[i].value);
			}
			$('#last_month_FAN_CPM')[0].innerHTML = (1000*last_revenue/last_imp).toFixed(2);
		  }
		);
	  }
	);
}
//FAN Month Metrics

//FAN Metrics
function getFANMetrics(since,until){
	//Coverage
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"COUNT","event_name":"fb_ad_network_request","summary":"true","since":since,'until':until},
	  function(response) {
	  	var requests=0;
		for(var i=0;i<response.data.length;i++){
			requests+=parseInt(response.data[i].value);
		}
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_request","summary":"true","since":since,'until':until},
		  function(response) {
		  	var filled=0;
			for(var i=0;i<response.data.length;i++){
				filled+=parseInt(response.data[i].value);
			}
			$('#FAN_coverage')[0].innerHTML = (100*filled/requests).toFixed(2);
		  }
		);
	  }
	);
	//impressions, CTR, CPM
	FB.api(
	  '/980493918670682/app_insights/app_event/',
	  'GET',
	  {"aggregateBy":"COUNT","event_name":"fb_ad_network_imp","summary":"true","since":since, 'until': until},
	  function(response) {
	  	var imp=0;
		for(var i=0;i<response.data.length;i++){
			imp+=parseInt(response.data[i].value);
		}
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"COUNT","event_name":"fb_ad_network_click","summary":"true","since":since, 'until': until},
		  function(response) {
		  	var clicks=0;
			for(var i=0;i<response.data.length;i++){
				clicks+=parseInt(response.data[i].value);
			}
			$('#FAN_CTR')[0].innerHTML = (100*clicks/imp).toFixed(2);
			$('#FAN_ads')[0].innerHTML = numberWithCommas(imp);
		  }
		);
		FB.api(
		  '/980493918670682/app_insights/app_event/',
		  'GET',
		  {"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":since, 'until': until},
		  function(response) {
		  	var revenue=0;
			for(var i=0;i<response.data.length;i++){
				revenue+=parseInt(response.data[i].value);
			}
			$('#FAN_CPM')[0].innerHTML = (1000*revenue/imp).toFixed(2);
		  }
		);
	  }
	);
}
//FAN Metrics


//Analytics Metrics
function getAudienceMetrics(since,until){
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": since,
			"end-date": until,
			metrics: "ga:pageviews,ga:Users",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestPageviews.execute(function(resp){
			$('#pageviews_range')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:pageviews"]);
			$('#users_range')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:Users"]);
		});
	});
}
function getAudienceMonthMetrics(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	gapi.client.load('analytics','v3', function(){
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": new Date(now.getFullYear(), now.getMonth(), 1).toISOString().substr(0,10),
			"end-date": "today",
			metrics: "ga:pageviews,ga:Users",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestPageviews.execute(function(resp){
			$('#pageviews_month')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:pageviews"]);
			$('#users_month')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:Users"]);
		});
		var requestLastFullMonthPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": new Date(now.getFullYear(), now.getMonth()-1, 1).toISOString().substr(0,10),
			"end-date": new Date(now.getFullYear(), now.getMonth(), 0).toISOString().substr(0,10),
			metrics: "ga:pageviews,ga:Users",
			fields: "totalsForAllResults",
			output: 'json'
		});
		requestLastFullMonthPageviews.execute(function(resp){
			$('#pageviews_last_month')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:pageviews"]);
			$('#users_last_month')[0].innerHTML = numberWithCommas(resp.totalsForAllResults["ga:Users"]);
		});
	});
}
//analytics Metrics

//Authors
//WP publications
function WPPublications(since,until){
	var dates=[];
	var range_posts=0;
	var start=new Date(new Date(since)-new Date().getTimezoneOffset()*60*1000);
	var end=new Date(new Date(until)-new Date().getTimezoneOffset()*60*1000);
	
	var current_date=new Date(start);

	while(current_date <= end){
		//console.log(current_date.toISOString().substr(0,10));
		dates.push(current_date.toISOString().substr(0,10));
		current_date.setTime(current_date.getTime()+1000*60*60*24);
	}
	
	for(var j=0;j<dates.length;j++){
		$.ajax({
			url: 'http://eldeforma.com/?json=get_date_posts&date='+dates[j]+'&count=800&status=published&include=id',
			dataType: 'jsonp',
			async: false,
			indexValue: j,
			success: function(response){
				range_posts+=response.count_total;
				if(this.indexValue==dates.length-1){
					$('#WP_publications')[0].innerHTML = range_posts;
				}
			}
		});
	}
}
function WPMonthsPublications(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	$.ajax({
		url: 'http://eldeforma.com/?json=get_date_posts&date='+now.toISOString().substr(0,7)+'&count=800&status=published&include=id',
		dataType: 'jsonp',
		async: true,
		success: function(response){
			$('#WP_month_publications')[0].innerHTML = numberWithCommas(response.count_total);
		}
	});
	now=new Date(now.getFullYear(),now.getMonth()-1,1);
	$.ajax({
		url: 'http://eldeforma.com/?json=get_date_posts&date='+now.toISOString().substr(0,7)+'&count=800&status=published&include=id',
		dataType: 'jsonp',
		async: true,
		success: function(response){
			$('#WP_last_month_publications')[0].innerHTML = numberWithCommas(response.count_total);
		}
	});
}
//FB publications
function FBMonthsPosts(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var since=new Date(now);
	since.setDate(1);
	var until=new Date(now);
	until.setMonth(now.getMonth()+1);
	until.setDate(0);
	
	var month_posts=0;
	FB.api(
		'/eldeforma/posts',
		'GET',
		{'since': since.toISOString().substr(0,10)+'T0'+new Date().getTimezoneOffset()/60+':00:00', 'until': until.toISOString().substr(0,10)+'T0'+new Date().getTimezoneOffset()/60+':00:00','limit':100},
		function(response){
			month_posts+=response.data.length;
			if(typeof response.paging !== 'undefined'){
				nextPageMonthsFBPosts(response.paging.next,month_posts);
			}
		}
	);
}
function nextPageMonthsFBPosts(nextURL,months_posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			months_posts+=response.data.length;
			if(typeof response.paging !== 'undefined'){
				nextPageMonthsFBPosts(response.paging.next,months_posts);
			}
			else{
				$('#FB_month_posts')[0].innerHTML = numberWithCommas(months_posts);
			}
		}
	});
}
function FBLastMonthsPosts(){
	var now=new Date(new Date().getTime()-new Date().getTimezoneOffset()*60*1000);
	var since=new Date(now);
	since.setMonth(now.getMonth()-1);
	since.setDate(1);
	var until=new Date(now);
	until.setDate(0);
	
	var month_posts=0;
	FB.api(
		'/eldeforma/posts',
		'GET',
		{'since': since.toISOString().substr(0,10)+'T0'+new Date().getTimezoneOffset()/60+':00:00', 'until': until.toISOString().substr(0,10)+'T0'+new Date().getTimezoneOffset()/60+':00:00','limit':100},
		function(response){
			month_posts+=response.data.length;
			if(typeof response.paging !== 'undefined'){
				nextPageLastMonthsFBPosts(response.paging.next,month_posts);
			}
		}
	);
}
function nextPageLastMonthsFBPosts(nextURL,months_posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			months_posts+=response.data.length;
			if(typeof response.paging !== 'undefined'){
				nextPageLastMonthsFBPosts(response.paging.next,months_posts);
			}
			else{
				$('#FB_last_month_posts')[0].innerHTML = numberWithCommas(months_posts);
			}
		}
	});
}
function FBPosts(since,until){
	since+='T0'+new Date().getTimezoneOffset()/60+':00:00';
	until+='T0'+new Date().getTimezoneOffset()/60+':00:00+1day';
	
	var range_posts=0;
	FB.api(
		'/eldeforma/posts',
		'GET',
		{'since': since, 'until': until,'limit':100},
		function(response){
			range_posts+=response.data.length;
			if(typeof response.paging !== 'undefined'){
				nextPageFBPosts(response.paging.next,range_posts);
			}
		}
	);
}
function nextPageFBPosts(nextURL,range_posts){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			range_posts+=response.data.length;
			if(typeof response.paging !== 'undefined'){
				nextPageMonthsFBPosts(response.paging.next,range_posts);
			}
			else{
				$('#FB_posts')[0].innerHTML = numberWithCommas(range_posts);
			}
		}
	});
}

//Author results
var select_authors, select_posts, fb_posts, dates;  
function getOpenArticlesWPSelect(since,until){
	//$('#chart_div_select_posts')[0].removeChild($('#chart_div_select_posts')[0].firstChild);
	select_authors={};
	select_posts=[];
	fb_posts=[];
	dates=[];  

	//gapi.client.load('analytics','v3');
	
	//var start=new Date($('#from_report_day')[0].value+'T0'+new Date().getTimezoneOffset()/60+':00:00');
	//var end=new Date($('#to_report_day')[0].value+'T0'+new Date().getTimezoneOffset()/60+':00:00');
	
	var start=new Date(since+'T0'+new Date().getTimezoneOffset()/60+':00:00');
	var end=new Date(until+'T0'+new Date().getTimezoneOffset()/60+':00:00');
	
	//console.log(start);
	
	var current_date;
	var start_cushion=new Date(start.getTime()-1000*60*60*24*7), end_cushion=new Date(end.getTime()+1000*60*60*24*0);

	current_date=start_cushion;
	while(current_date <= end_cushion){
		//console.log(current_date.toISOString().substr(0,10));
		dates.push(current_date.toISOString().substr(0,10));
		current_date.setTime(current_date.getTime()+1000*60*60*24);
	}
	

	for(var j=0;j<dates.length;j++){
		$.ajax({
			url: 'http://eldeforma.com/?json=get_date_posts&date='+dates[j]+'&count=200&status=published&include=author,url,title',
			dataType: 'jsonp',
			async: false,
			indexValue: j,
			success: function(response){
				for(var i=0;i<response.posts.length;i++){
					select_posts.push(response.posts[i]);
				}
				//console.log(this.indexValue);
				if(this.indexValue==dates.length-1){
					getOpenMetricsSelect(start,end);
				}
			}
		});
	}
}
function getOpenMetricsSelect(start,end){
	gapi.client.load('analytics','v3',function(){
		//console.log(start,end);
		var end_cushion=new Date(end.getTime()+1000*60*60*24*7)
		var now = new Date();
		var requestPageviews = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			"start-date": start.toISOString().substr(0,10),
			"end-date": end_cushion.toISOString().substr(0,10),
			metrics: "ga:pageviews",
			dimensions: "ga:pagePath",
			filters: "ga:pagePath!@p1=true",
			sort: '-ga:pageviews',
			fields: 'rows',
		});
		requestPageviews.execute(function(resp){
			var pages=resp.rows;
			//console.log(pages);  

			for(var i=0;i<select_posts.length;i++){
				select_posts[i].pageviews=0;
				for(var j=0;j<pages.length;j++){
					if(select_posts[i].url.indexOf(pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/'))) > -1 && pages[j][0].substr(12,pages[j][0].substr(12,pages[j][0].length).indexOf('/')).length >= 6){
						select_posts[i].pageviews+=parseInt(pages[j][1]);
					}
				}
			}
		
			//console.log(start);
			var since=new Date(start).toISOString().substr(0,19);
			//console.log(since);
			var until=new Date(end.getTime()+1000*60*60*24).toISOString().substr(0,10)+'T0'+(new Date().getTimezoneOffset()/60)+':00:00';
			//console.log(until);
			FB.api(
			  '/eldeforma/posts',
			  'GET',
			  {"fields":"created_time,link,type,insights.metric(post_impressions,post_consumptions_by_type).period(lifetime),likes.limit(1).summary(true)","limit":"100","since":since,"until":until},
			  function(response) {
				  //console.log(response.insights);
				  //fb_posts.push(response.data);
		  
				  for(var i=0;i<response.data.length;i++){
					fb_posts.push(response.data[i]);
				  }
				  if(typeof response.paging !== 'undefined'){
					nextPageOpenFBPosts(response.paging.next);
				  }
			  }
			);
			//sortOpenPostsSelect(start,end);
		});
	});
}
function nextPageOpenFBPosts(nextURL){
	$.ajax({
		url: nextURL,
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			for(var i=0;i<response.data.length;i++){
				fb_posts.push(response.data[i]);
			}
			if(typeof response.paging !== 'undefined'){
				nextPageOpenFBPosts(response.paging.next);
			}
			else{
				//sortOpenPostsSelect();
				MarryPageviewsWithFBMetrics();
			}
		}
	});
}
function MarryPageviewsWithFBMetrics(){
	for(var i=0;i<fb_posts.length;i++){
		for(var j=0;j<select_posts.length;j++){
			if(fb_posts[i].link.indexOf(select_posts[j].url) != -1){
				fb_posts[i].pageviews=select_posts[j].pageviews;
				fb_posts[i].author=select_posts[j].author;
				fb_posts[i].title=select_posts[j].title;
				fb_posts[i].url=select_posts[j].url;
			}
		}
	}
	sortOpenPostsSelect();
}
function sortOpenPostsSelect(){
	var pageviews_totals, likes_totals, impressions_totals, link_clicks_totals;
	var nonZero;
	
	var tmp_fb_posts = fb_posts;
	
	fb_posts=[];
	
	function removeDuplicates(arr, prop) {
		 var new_arr = [];
		 var lookup  = {};
 
		 for (var i in arr) {
			 lookup[arr[i][prop]] = arr[i];
		 }
 
		 for (i in lookup) {
			 new_arr.push(lookup[i]);
		 }
 
		 return new_arr;
	}
	 
	fb_posts = removeDuplicates(tmp_fb_posts, "url");
	
	for(var i=0;i<fb_posts.length;i++){
		if(typeof fb_posts[i].author !== 'undefined'){
			var nickname = fb_posts[i].author.nickname;
			select_authors[nickname] = [];
			select_authors[nickname].name = fb_posts[i].author.name;
		}
	}

	for(var i=0;i<fb_posts.length;i++){
		if(typeof fb_posts[i].author !== 'undefined'){
			select_authors[fb_posts[i].author.nickname].push({
				"url": fb_posts[i].url, 
				"pageviews": fb_posts[i].pageviews, 
				"likes": fb_posts[i].likes.summary.total_count, 
				"impressions": fb_posts[i].insights.data[0].values[0].value,
				"link_clicks": fb_posts[i].insights.data[1].values[0].value["link clicks"]
			});
		}
	}
	
	$.each(select_authors, function(key, value) {
		//console.log(authors[key]);
		pageviews_totals=0;
		likes_totals=0;
		impressions_totals=0;
		link_clicks_totals=0;
		nonZero=0;
		for(var j=0;j<select_authors[key].length;j++)
		{
			pageviews_totals+=select_authors[key][j].pageviews;
			likes_totals+=select_authors[key][j].likes;
			impressions_totals+=select_authors[key][j].impressions;
			link_clicks_totals+=select_authors[key][j].link_clicks;
			if(select_authors[key][j].pageviews>0){
				nonZero++;
			}
		}
		select_authors[key].pageviews_total=pageviews_totals;
		select_authors[key].likes_total=likes_totals;
		if(nonZero>0){
			select_authors[key].pageviews_average=pageviews_totals/nonZero;
			select_authors[key].likes_average=likes_totals/nonZero;
			select_authors[key].CTR_average=1.0*link_clicks_totals/impressions_totals*100;
		}else{
			select_authors[key].pageviews_average=0;
			select_authors[key].likes_average=0;
			select_authors[key].CTR_average=0;
		}
	});
	drawOpenTableSelect();
}
function drawOpenTableSelect(){
	/*var posts_data = new google.visualization.DataTable();
  
	posts_data.addColumn({type: 'string', label: 'Autor'});
	posts_data.addColumn({type: 'string', label: 'Post'});
	posts_data.addColumn({type: 'number', label: 'PV'});
	posts_data.addColumn({type: 'number', label: 'Likes'});
	posts_data.addColumn({type: 'number', label: 'CTR'});
  
  	//var Colabs = ['Adolfo Santino','Ddtorresf','Gordolobo','Jos','Omar','Ugesaurio','Emisan','Valenzuela','Fercosta','Alde','DonPotasio'];
	for(var n=0;n<fb_posts.length;n++){
		if(fb_posts[n].type == 'link'){
			posts_data.addRow([
				typeof fb_posts[n].author !== 'undefined' ? fb_posts[n].author.name : '----',
				OpenPostTitle(fb_posts[n]),
				{v: OpenPostResults(fb_posts[n]), f: numberWithCommas(OpenPostResults(fb_posts[n]))},
				{v: fb_posts[n].likes.summary.total_count, f: numberWithCommas(fb_posts[n].likes.summary.total_count)},
				{v: fb_posts[n].insights.data[1].values[0].value["link clicks"]/fb_posts[n].insights.data[0].values[0].value, f: (fb_posts[n].insights.data[1].values[0].value["link clicks"]/fb_posts[n].insights.data[0].values[0].value*100).toFixed(2)+'%'}
			]);
		}
	}*/
	
	var authors_data = new google.visualization.DataTable();

	authors_data.addColumn({type: 'string', label: 'Autor'});
	authors_data.addColumn({type: 'number', label: 'Notas'});
	authors_data.addColumn({type: 'number', label: 'PV'});
	authors_data.addColumn({type: 'number', label: 'Likes'});
	authors_data.addColumn({type: 'number', label: 'Avg PV'});
	authors_data.addColumn({type: 'number', label: 'Avg Likes'});
	authors_data.addColumn({type: 'number', label: 'CTR'});

	$.each(select_authors, function(key, value) {
		authors_data.addRow([
			select_authors[key].name,
			select_authors[key].length,
			{v: select_authors[key].pageviews_total, f: numberWithCommas(select_authors[key].pageviews_total)},
			{v: select_authors[key].likes_total, f: numberWithCommas(select_authors[key].likes_total)},
			{v: select_authors[key].pageviews_average, f: numberWithCommas(select_authors[key].pageviews_average.toFixed(2))},
			{v: select_authors[key].likes_average, f: numberWithCommas(select_authors[key].likes_average.toFixed(2))},
			{v: select_authors[key].CTR_average, f: select_authors[key].CTR_average.toFixed(2)+'%'}
		]);
	});

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 2,
		'sortAscending': false
	};
	
	var options_a = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'400px',
		'sortColumn': 2,
		'sortAscending': false
	};

	// Instantiate and draw our chart, passing in some options.
	//var posts_table = new google.visualization.Table(document.getElementById('posts'));  
	var authors_table = new google.visualization.Table(document.getElementById('FB_authors'));  
	
	/*google.visualization.events.addListener(posts_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(posts_table, 'sort', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});*/
	
	google.visualization.events.addListener(authors_table, 'ready', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
	
	google.visualization.events.addListener(authors_table, 'sort', function () {
		$('.google-visualization-table').css('z-index',0);
		var links = $('.google-visualization-table>div>table>tbody>tr>td>a');
		for(var i=0;i<links.length;i++){
			links[i].setAttribute('target','_blank');
		}
	});
    
	//posts_table.draw(posts_data, options);
	authors_table.draw(authors_data, options_a);	
	
	//console.log("done");
}
function OpenPostResults(post){
	if(typeof post.pageviews !== 'undefined'){
		return post.pageviews;
	}
	return 0;
}
function OpenPostTitle(post){
	if(typeof post.title !== 'undefined'){
		return (post.title.substring(0,100)+'...').link(post.url);
	}
	if(typeof post.name !== 'undefined' && post.name !== ''){
		return (post.name.substring(0,100)+'...').link(post.link);
	}
	if(typeof post.message !== 'undefined' && post.message !== ''){
	 	return (post.message.substring(0,100)+'...').link(post.link);
	}
	return ('N/A').link(post.link);
}
//Authors