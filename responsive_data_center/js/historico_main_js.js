//active page
function getActivePage(){
	$(document).ready(function(){
		var page_url=window.location.href;
		//console.log($('#sidebar-menu')[0].childNodes[1].childNodes[1]);
		for(var i=3;i<$('#sidebar-menu')[0].childNodes[1].childNodes.length;i+=2){
			var sidebar_tag=$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].href;
			//console.log(sidebar_tag,page_url);
			if(sidebar_tag.indexOf(page_url) > -1){
				$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].className = "waves-effect active";
			}
			//console.log(i);
		}
	});
}
//active page

function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

//Campaigns Interno
var campaigns_interno=[];

function loadInternoCampaigns(){
	$.ajax({
		url: "php_scripts/get_campaigns_interno.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			campaigns_interno=response;
            //console.log(response);
		}
	});
    drawInternoCampaigns();  
}
function drawInternoCampaigns(){
	var data_table = new google.visualization.DataTable();

	data_table.addColumn({type: 'number', label: 'Orden'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Created at'});
	data_table.addColumn({type: 'number', label: 'Average RT CXW'});
    data_table.addColumn({type: 'number', label: 'CTR Organico'});
	data_table.addColumn({type: 'number', label: 'Shares/Likes'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pagado'});
	data_table.addColumn({type: 'number', label: 'FB CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Actions'});
	data_table.addColumn({type: 'string', label: 'Quickview'});
	data_table.addColumn({type: 'string', label: 'Edit'});
    
    for(var n=0;n<campaigns_interno.length;n++){
         data_table.addRow([
             n+1,
             (titleShortener(campaigns_interno[n].name).link("https://facebook.com/"+campaigns_interno[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+campaigns_interno[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
             new Date(campaigns_interno[n].posted_date),
             {v: campaigns_interno[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);}).length > 0 ? math.mean(campaigns_interno[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);})) : NaN, f: campaigns_interno[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);}).length > 0 ? "$"+(math.mean(campaigns_interno[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);}))).toFixed(5) : 'NaN'},
             {v: isNaN(parseFloat(campaigns_interno[n].metrics[0].organic_ctr)) ? 0 : parseFloat(campaigns_interno[n].metrics[0].organic_ctr), f: isNaN(parseFloat(campaigns_interno[n].metrics[0].organic_ctr)) ? 0 : parseFloat(campaigns_interno[n].metrics[0].organic_ctr)+"%"},
             {v: parseInt(campaigns_interno[n].metrics[0].shares)+parseInt(campaigns_interno[n].metrics[0].likes), f: numberWithCommas(campaigns_interno[n].metrics[0].likes)+' / '+numberWithCommas(campaigns_interno[n].metrics[0].shares)},
             {v: parseInt(campaigns_interno[n].metrics[0].link_clicks), f: goalClicks(campaigns_interno[n])},
             {v: isNaN(parseFloat(campaigns_interno[n].metrics[0].paid_ctr)) ? 0 : parseFloat(campaigns_interno[n].metrics[0].paid_ctr), f: isNaN(parseFloat(campaigns_interno[n].metrics[0].paid_ctr)) ? 0 : parseFloat(campaigns_interno[n].metrics[0].paid_ctr)+"%"},
             {v: parseFloat(campaigns_interno[n].metrics[0].cxw), f: "$"+(parseFloat(campaigns_interno[n].metrics[0].cxw)).toFixed(5)},
             {v: parseFloat(campaigns_interno[n].metrics[0].cpm), f: "$"+(parseFloat(campaigns_interno[n].metrics[0].cpm)).toFixed(2)},
             {v: parseFloat(campaigns_interno[n].metrics[0].spend), f: goalSpend(campaigns_interno[n])},
             (campaignIsCurrentlyActive(campaigns_interno[n].campaign).status ? '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+campaigns_interno[n].id+'\',\''+campaigns_interno[n].campaign+'\')">Kill Promote</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="restartCampaign(\''+campaigns_interno[n].id+'\',\''+campaigns_interno[n].campaign+'\')">Restart Promote</button>')+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+campaigns_interno[n].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+campaigns_interno[n].id+'\')">New Promote</button>'+(parseFloat(campaigns_interno[n].metrics[0].paid_ctr) > 0 ? '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#predict-modal" onclick="selectPredictCampaign(\''+campaigns_interno[n].id+'\',\''+campaigns_interno[n].campaign+'\',\''+campaigns_interno[n].name+'\',\''+parseFloat(campaigns_interno[n].metrics[0].paid_ctr)+'\',\''+parseInt(campaigns_interno[n].metrics[0].paid_imp)+'\')">Predict</button>' : ''),
             '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+campaigns_interno[n].id+'\',\''+campaigns_interno[n].name.replace(/\"/g,'')+'\',\''+campaigns_interno[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
             '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+campaigns_interno[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
       ]);
    }

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 2
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('interno_table'));

	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});
    var formatter_date = new google.visualization.DateFormat({pattern: "MMM-d-yy H:mm"});

    formatter_date.format(data_table, 2);
	formatter_ctr.format(data_table, 3);
	formatter_paid_ctr.format(data_table, 7);
	formatter_cxw.format(data_table, 8);
	formatter_cpm.format(data_table, 9);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
    for(var n=0;n<campaigns_interno.length;n++){
        links[n].childNodes[0].setAttribute('title',campaigns_interno[n].name);
    }
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}

//Campaigns Comercial
var campaigns_comercial=[];

function loadComercialCampaigns(){
	$.ajax({
		url: "php_scripts/get_campaigns_comercial.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			campaigns_comercial=response;
            //console.log(response);
		}
	});
    drawComercialCampaigns();  
}
function drawComercialCampaigns(){
	var data_table = new google.visualization.DataTable();

	data_table.addColumn({type: 'number', label: 'Orden'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'datetime', label: 'Created at'});
	data_table.addColumn({type: 'number', label: 'Average RT CXW'});
    data_table.addColumn({type: 'number', label: 'CTR Organico'});
	data_table.addColumn({type: 'number', label: 'Shares/Likes'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pagado'});
	data_table.addColumn({type: 'number', label: 'FB CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Actions'});
	data_table.addColumn({type: 'string', label: 'Quickview'});
	data_table.addColumn({type: 'string', label: 'Edit'});
    
    for(var n=0;n<campaigns_comercial.length;n++){
         data_table.addRow([
             n+1,
             (titleShortener(campaigns_comercial[n].name).link("https://facebook.com/"+campaigns_comercial[n].id))+'<button class="btn btn-inverse btn-xs btn-rounded" style="float: right;" data-toggle="modal" data-target="#name-modal" waves-effect waves-light" onclick="setNamePostId(\''+campaigns_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 15px;"></i></button>',
             new Date(campaigns_comercial[n].posted_date),
             {v: campaigns_comercial[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);}).length > 0 ? math.mean(campaigns_comercial[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);})) : NaN, f: campaigns_comercial[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);}).length > 0 ? "$"+(math.mean(campaigns_comercial[n].metrics.map(getRT_CXW).filter(function(x){return (typeof x !== 'undefined' && x!=false);}))).toFixed(5) : 'NaN'},
             {v: isNaN(parseFloat(campaigns_comercial[n].metrics[0].organic_ctr)) ? 0 : parseFloat(campaigns_comercial[n].metrics[0].organic_ctr), f: isNaN(parseFloat(campaigns_comercial[n].metrics[0].organic_ctr)) ? 0 : parseFloat(campaigns_comercial[n].metrics[0].organic_ctr)+"%"},
             {v: parseInt(campaigns_comercial[n].metrics[0].shares)+parseInt(campaigns_comercial[n].metrics[0].likes), f: goalLikes(campaigns_comercial[n])},
             {v: parseInt(campaigns_comercial[n].metrics[0].link_clicks), f: goalClicks(campaigns_comercial[n])},
             {v: isNaN(parseFloat(campaigns_comercial[n].metrics[0].paid_ctr)) ? 0 : parseFloat(campaigns_comercial[n].metrics[0].paid_ctr), f: isNaN(parseFloat(campaigns_comercial[n].metrics[0].paid_ctr)) ? 0 : parseFloat(campaigns_comercial[n].metrics[0].paid_ctr)+"%"},
             {v: parseFloat(campaigns_comercial[n].metrics[0].cxw), f: "$"+(parseFloat(campaigns_comercial[n].metrics[0].cxw)).toFixed(5)},
             {v: parseFloat(campaigns_comercial[n].metrics[0].cpm), f: "$"+(parseFloat(campaigns_comercial[n].metrics[0].cpm)).toFixed(2)},
             {v: parseFloat(campaigns_comercial[n].metrics[0].spend), f: goalSpend(campaigns_comercial[n])},
             (campaignIsCurrentlyActive(campaigns_comercial[n].metrics[0].campaign).status ? '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+campaigns_comercial[n].id+'\',\''+campaigns_comercial[n].metrics[0].campaign+'\')">Kill Promote</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="restartCampaign(\''+campaigns_comercial[n].id+'\',\''+campaigns_comercial[n].metrics[0].campaign+'\')">Restart Promote</button>')+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+campaigns_comercial[n].metrics[0].campaign+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+campaigns_comercial[n].id+'\')">New Promote</button>'+'<button class="btn btn-inverse waves-effect waves-light" data-toggle="modal" data-target="#client-modal" onclick="getClientKomfo(\''+campaigns_comercial[n].id+'\')">'+campaigns_comercial[n].client+'</button>',
             '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+campaigns_comercial[n].id+'\',\''+campaigns_comercial[n].name.replace(/\"/g,'')+'\',\''+campaigns_comercial[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
             '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+campaigns_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
       ]);
    }

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 2
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('comercial_table'));

	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});
    var formatter_date = new google.visualization.DateFormat({pattern: "MMM-d-yy H:mm"});

    formatter_date.format(data_table, 2);
	formatter_ctr.format(data_table, 4);
	formatter_paid_ctr.format(data_table, 7);
	formatter_cxw.format(data_table, 8);
	formatter_cpm.format(data_table, 9);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
    for(var n=0;n<campaigns_comercial.length;n++){
        links[n].childNodes[0].setAttribute('title',campaigns_comercial[n].name);
    }
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}


function titleShortener(text){
	if(text.length<50){
		return text;
	}
	else{
		return text.substring(0,20)+'...'+text.substring(text.length-20,text.length);
	}
}
function goalClicks(post_data){
	if(post_data.goal!==null){
		return numberWithCommas(post_data.metrics[0].link_clicks)+' / '+numberWithCommas(post_data.goal)+' / '+numberWithCommas(post_data.metrics[0].paid_link_clicks)+' '+'<button class="btn btn-success" data-toggle="modal" data-target="#goal-modal" waves-effect waves-light" onclick="setGoalPostId(\''+post_data.id+'\')">Goal</button>';
	}else{
		return numberWithCommas(post_data.metrics[0].link_clicks)+' / '+numberWithCommas(post_data.metrics[0].paid_link_clicks)+' '+'<button class="btn btn-success" data-toggle="modal" data-target="#goal-modal" waves-effect waves-light" onclick="setGoalPostId(\''+post_data.id+'\')">Goal</button>';
	}
}
function goalLikes(post_data){
	if(post_data.likes_goal!==null){
		return numberWithCommas(post_data.metrics[0].likes)+' / '+numberWithCommas(post_data.likes_goal)+'   -   '+numberWithCommas(post_data.metrics[0].shares);
	}else{
		return numberWithCommas(post_data.metrics[0].likes)+'   -   '+numberWithCommas(post_data.metrics[0].shares);
	}
}
function goalSpend(post_data){
	if(post_data.spend_limit!==null){
		return '$'+numberWithCommas((parseFloat(post_data.metrics[0].spend)).toFixed(2))+' / '+'$'+numberWithCommas((parseFloat(post_data.spend_limit)).toFixed(2));
	}else{
		return '$'+numberWithCommas((parseFloat(post_data.metrics[0].spend)).toFixed(2));
	}
}
function campaignIsCurrentlyActive(campaign){
	var flag;
	$.ajax({
		url: 'https://graph.facebook.com/v'+version+'/'+campaign+'?fields=effective_status,ads.fields(effective_status)&'+access_token,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(response){
            //console.log(response);
            if(response.effective_status != 'ACTIVE'){
               flag={status: false, effective_status: 'not active'};
            }
            else{
                switch(response.ads.data[0].effective_status){
                    case 'ACTIVE':
                        flag={status: true, effective_status: 'active'};
                        break;
                    case 'PENDING_REVIEW':
                        flag={status: true, effective_status: 'pending'};
                        break;
                    default:
                        flag={status: false, effective_status: 'not active'};
                        break;      
                }
            }		
        }
	});
	return flag;	
}
function getDayCXW(campaign){
    var day_cxw;
    $.ajax({
		url: 'https://graph.facebook.com/v'+version+'/'+campaign+'?fields=insights.fields(spend,actions,objective).date_preset(today),name&'+access_token,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(response){
            if(typeof response.insights !== 'undefined'){
                for(var i=0;i<response.insights.data[0].actions.length;i++){
                    if(response.insights.data[0].objective == 'LINK_CLICKS' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'POST_ENGAGEMENT' && response.insights.data[0].actions[i].action_type == 'post_engagement'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'PAGE_LIKES' && response.insights.data[0].actions[i].action_type == 'like'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'VIDEO_VIEWS' && response.insights.data[0].actions[i].action_type == 'video_view'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    day_spend = response.insights.data[0].spend;
                }
            }
            else{
                day_cxw=0;
                day_spend=0;
            }
		}
	});
	return {cxw: day_cxw, spend: day_spend};
}
function getLinkClicksVSPageviewsPromoted(campaigns){
    var promises=[];
    campaigns[campaigns.length]={click_percent_array: []};
    for(var j=0;j<campaigns.length-1;j++){
       promises.push(
            $.ajax({
                url: 'https://graph.facebook.com/v'+version+'/'+campaigns[j].campaign+'?fields=insights.fields(actions).date_preset(yesterday)&'+access_token,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function(response){
                    var click_percent;
                    if(typeof response.insights === 'undefined'){
                        campaigns[campaigns.length-1].click_percent_array.push(
                            {
                                pageviews: 0,
                                id: campaigns[j].id
                            }
                        );
                    }
                    else{
                        for(var i=0;i<response.insights.data[0].actions.length;i++){
                            if(response.insights.data[0].actions[i].action_type == 'link_click'){
                                campaigns[j].yesterdays_paid_clicks = parseInt(response.insights.data[0].actions[i].value);
                            }
                        }
                        $.ajax({
                            url: 'https://graph.facebook.com/v'+version+'/'+campaigns[j].id+'?fields=link&'+access_token,
                            type: 'GET',
                            dataType: 'json',
                            async: false,
                            success: function(response) {
                                var link = response.link.substr(32,response.link.length-32-1);
                                var id = response.id;
                                //console.log(j);
                                gapi.client.load('analytics','v3').then(function(){
                                   var requestPageviews = gapi.client.analytics.data.ga.get({
                                        ids: "ga:41142760",
                                        "start-date": 'yesterday',
                                        "end-date": "yesterday",
                                        metrics: "ga:pageviews",
                                        dimensions: "ga:pagePath",
                                        filters: "ga:pagePath=@p1=true;ga:pagePath=@"+link,
                                        fields: 'totalsForAllResults'
                                    });
                                    requestPageviews.execute(function(resp){
                                        //console.log(j);
                                        campaigns[campaigns.length-1].click_percent_array.push(
                                            {
                                                pageviews: parseInt(resp.totalsForAllResults['ga:pageviews']),
                                                id: id
                                            }
                                        );
                                    });
                                });
                            }
                       });
                    }
                }
            })
        ); 
    }
    $.when.apply($,promises).done(function(){
        //drawTableKomfo();
        function marryClickPercentage(campaigns){
            if(campaigns[campaigns.length-1].click_percent_array.length != campaigns.length-1){
                setTimeout(function(){marryClickPercentage(campaigns)},2000);
            }
            else{
                drawInternoCampaigns();
                drawComercialCampaigns();
            }
        }
        marryClickPercentage(campaigns);
        console.log('promise success')
    });
}
function getRT_CXW(item,index) {
    if(typeof item.rt_cxw !== 'undefined'){
        return item.rt_cxw;
    }
}

//Stop restart campaign
function stopCampaign(post_id,campaign){
	updateStoppedCampaign(post_id);
    FB.api(
        '/'+campaign,
        'POST',
        {status: 'PAUSED'},
        function(response) {
            if(!response.hasOwnProperty('error')){
                console.log(response);
                alert('Campaign successfully paused!');
                setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
            }else{
                alert(response.error.message);
            }
        }
    );
}
function updateStoppedCampaign(id){
	$.ajax({
		url: 'php_scripts/update_stopped_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			post_id: id
		},
		success: function(response){
			console.log(response);
		}
	});
}
function restartCampaign(post_id,campaign){
	updateRestartedCampaign(post_id);
    FB.api(
        '/'+campaign,
        'POST',
        {status: 'ACTIVE'},
        function(response) {
            if(!response.hasOwnProperty('error')){
                console.log(response);
                alert('Campaign successfully activated!');
                setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
            }else{
                alert(response.error.message);
            }
        }
    );
}
function updateRestartedCampaign(id){
	$.ajax({
		url: 'php_scripts/update_restarted_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			post_id: id
		},
		success: function(response){
			console.log(response);
		}
	});
}
//Stop restart campaign

function setGoalPostId(id){
	$('#goal_post_id')[0].innerHTML = id;
}
function changeGoal(){
	var id = $('#goal_post_id')[0].innerHTML;
	var goal = $('#goal_post_goal')[0].value;
	$.ajax({
		url: 'php_scripts/updateGoal.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'goal': goal,
			'type': $('[name=radioGoal]:checked').val()
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
}
function removeGoal(){
	var id = $('#goal_post_id')[0].innerHTML;
	var goal = $('#goal_post_goal')[0].value;
	$.ajax({
		url: 'php_scripts/removeGoal.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'goal': goal,
			'type': $('[name=radioGoal]:checked').val()
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
}
function setNamePostId(id){
	$('#name_post_id')[0].innerHTML = id;
}
function changeName(){
	var id = $('#name_post_id')[0].innerHTML;
	var name = $('#post_name')[0].value;
	$.ajax({
		url: 'php_scripts/updateName.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'name': name
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
}

//Edit Promote
var campaign_to_edit;
var editAdSet = {};
var editAdAd = {};
var editPromoteParameters = {editAdSet: {'targeting':{}}, editAdAd:{}};
function getCurrentCampaignParameters(campaign){
    FB.api(
        '/'+campaign,
        'GET',
        {"fields":"adsets.fields(daily_budget,bid_amount,targeting,excluded_custom_audiences),ads.fields(name,creative.fields(effective_object_story_id)),account_id"},
        function(response){
            campaign_to_edit=response;
            $('#edit-ad-name').attr("placeholder", response.ads.data[0].name);
            $('#edit-budget').attr("placeholder", parseFloat(response.adsets.data[0].daily_budget)/100);
            $('#edit-bid').attr("placeholder", response.adsets.data[0].bid_amount/100);
            $('#edit-min-age').attr("placeholder", response.adsets.data[0].targeting.age_min);
            $('#edit-max-age').attr("placeholder", response.adsets.data[0].targeting.age_max);
            var selector=document.getElementById("edit-exclude");
            while(selector.length>0){
                selector.remove(0);
            }
            switch(response.account_id){
                case '986080914767243':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                case '31492866':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                case '958749654167036':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                default:
                    var audiences = [];
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    break;
            }
            var selector=document.getElementById("edit-exclude");
            if(typeof response.adsets.data[0].targeting.excluded_custom_audiences !== 'undefined'){
                $('#edit-exclude').val(response.adsets.data[0].targeting.excluded_custom_audiences[0].id);    
            }else{
                $('#edit-exclude').val('');
            }
            
            if(typeof response.adsets.data[0].targeting.connections !== 'undefined'){
                $('#edit-connections').val(response.adsets.data[0].targeting.connections[0].id);
            }else{
                $('#edit-connections').val(null);
            }
            
            if(typeof response.adsets.data[0].targeting.excluded_connections !== 'undefined'){
                $('#edit-excluded-connections').val(response.adsets.data[0].targeting.excluded_connections[0].id);
            }else{
                $('#edit-excluded-connections').val(null);
            }
            
            $('#edit-genders').prop('selectedIndex',typeof response.adsets.data[0].targeting.genders === 'undefined' ? 0 : response.adsets.data[0].targeting.genders[0]);
            
            if($.inArray('desktop', response.adsets.data[0].targeting.device_platforms) > -1){
                $('#edit-desktop-placement')[0].checked = true;
            }else{
                $('#edit-desktop-placement')[0].checked = false;
            }
        
            if($.inArray('mobile', response.adsets.data[0].targeting.device_platforms) > -1){
                $('#edit-mobile-placement')[0].checked = true;
            }else{
                $('#edit-mobile-placement')[0].checked = false;
            }
            
            if(typeof response.adsets.data[0].targeting.device_platforms === 'undefined'){
                $('#edit-mobile-placement')[0].checked = true;
                $('#edit-desktop-placement')[0].checked = true;
            }
            
            if($.inArray('MX', response.adsets.data[0].targeting.geo_locations.countries) > -1){
                $('#edit-mexico')[0].checked = true;
            }else{
                $('#edit-mexico')[0].checked = false;
            }
            
            if($.inArray('BZ', response.adsets.data[0].targeting.geo_locations.countries) > -1){
                $('#edit-latam')[0].checked = true;
            }else{
                $('#edit-latam')[0].checked = false;
            }
        }
    );
}
function setEditPromoteParameters(){
    var ad_change_flag = false;
    var adset_change_flag = false;
    editPromoteParameters = {editAdSet: {'targeting':{}}, editAdAd:{}};
    
    if($('#edit-bid').val() !== $('#edit-bid').attr('placeholder') && $('#edit-bid').val() !== ''){
        editPromoteParameters.editAdSet.bid_amount = parseFloat(document.getElementById('edit-bid').value)*100;
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.bid_amount = campaign_to_edit.adsets.data[0].bid_amount;
    }
    if($('#edit-budget').val() !== $('#edit-budget').attr('placeholder') && $('#edit-budget').val() !== ''){
        editPromoteParameters.editAdSet.daily_budget = parseFloat(document.getElementById('edit-budget').value)*100;
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.daily_budget = campaign_to_edit.adsets.data[0].daily_budget;
    }
    if($('#edit-min-age').val() !== $('#edit-min-age').attr('placeholder') && $('#edit-min-age').val() !== ''){
       editPromoteParameters.editAdSet.targeting.age_min = parseInt(document.getElementById('edit-min-age').value);
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.targeting.age_min = campaign_to_edit.adsets.data[0].targeting.age_min;
    }
    if($('#edit-max-age').val() !== $('#edit-max-age').attr('placeholder') && $('#edit-max-age').val() !== ''){
       editPromoteParameters.editAdSet.targeting.age_max = parseInt(document.getElementById('edit-max-age').value);
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.targeting.age_max = campaign_to_edit.adsets.data[0].targeting.age_max;
    }
       
    if(typeof campaign_to_edit.adsets.data[0].targeting.genders === 'undefined'){
        if($('#edit-genders').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.genders = [$('#edit-genders').prop('selectedIndex')];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-genders').prop('selectedIndex') != campaign_to_edit.adsets.data[0].targeting.genders){
            if($('#edit-genders').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.genders = [$('#edit-genders').prop('selectedIndex')];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.genders = [];
                adset_change_flag = true;
            }
        }
    }
        
    if(typeof campaign_to_edit.adsets.data[0].targeting.excluded_custom_audiences === 'undefined'){
        if($('#edit-exclude').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [{id: $('#edit-exclude').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-exclude').val() != campaign_to_edit.adsets.data[0].targeting.excluded_custom_audiences[0].id){
            if($('#edit-exclude').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [{id: $('#edit-exclude').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.connections === 'undefined'){
        if($('#edit-connections').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.connections = [{id: $('#edit-connections').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-connections').val() != campaign_to_edit.adsets.data[0].targeting.connections[0].id){
            if($('#edit-connections').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.connections = [{id: $('#edit-connections').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.connections = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.excluded_connections === 'undefined'){
        if($('#edit-excluded-connections').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.excluded_connections = [{id: $('#edit-excluded-connections').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-excluded-connections').val() != campaign_to_edit.adsets.data[0].targeting.excluded_connections[0].id){
            if($('#edit-excluded-connections').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.excluded_connections = [{id: $('#edit-excluded-connections').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.excluded_connections = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.device_platforms !== 'undefined'){
        
    }
    else{
        editPromoteParameters.editAdSet.targeting.device_platforms = getEditPlacements();
        editPromoteParameters.editAdSet.targeting.publisher_platforms = ['facebook'];
        editPromoteParameters.editAdSet.targeting.facebook_positions = ['feed'];
        adset_change_flag = true;
    }
    
    if($('#edit-ad-name').val() !== $('#edit-ad-name').attr('placeholder') && $('#edit-ad-name').val() !== ''){
        editPromoteParameters.editAdAd.name = $('#edit-ad-name').val();   
        ad_change_flag = true;
    }
    
    var edit_countries=[];
    if($('#edit-mexico')[0].checked){
       edit_countries.push('MX');
    }
    if($('#edit-latam')[0].checked){
        edit_countries.push('BZ', 'PA', 'AR', 'CL', 'PE', 'CO', 'EC', 'BO', 'GT', 'CR', 'SV', 'HN', 'NI', 'PY', 'UY');
    }
    if(edit_countries.length == 0){
        edit_countries.push('MX');
    }
    if(campaign_to_edit.adsets.data[0].targeting.geo_locations.countries.length != edit_countries.length){
        if($('#leave-targeting')[0].checked){
           editPromoteParameters.editAdSet.targeting.geo_locations = {countries : campaign_to_edit.adsets.data[0].targeting.geo_locations.countries};
        }
        else{
            editPromoteParameters.editAdSet.targeting.geo_locations = {countries : edit_countries};
        }
        adset_change_flag = true;
    }
    
    if(adset_change_flag){
        if($.isEmptyObject(editPromoteParameters.editAdSet.targeting)){
            delete editPromoteParameters.editAdSet.targeting;
        }
        else{
            editPromoteParameters.editAdSet.targeting.geo_locations = {countries : edit_countries};
            editPromoteParameters.editAdSet.targeting.publisher_platforms = ['facebook'];
            editPromoteParameters.editAdSet.targeting.facebook_positions = ['feed'];
        }
        FB.api(
            '/'+campaign_to_edit.adsets.data[0].id,
            'POST',
            editPromoteParameters.editAdSet,
            function(response) {
                console.log(response);
                if(response.success){
                    alert('Adset updated succesfully');
                    resetEditForm();
                    editPromoteParameters.editAdSet = {'targeting':{}};
                    setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
                }else{
                    alert('Adset update failed');
                    setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
                }
            }
        );
    }
    
    if(ad_change_flag){
        FB.api(
            '/'+campaign_to_edit.ads.data[0].id,
            'POST',
            editPromoteParameters.editAdAd,
            function(response) {
                console.log(response);
                if(response.success){
                    alert('Ad updated succesfully');
                    resetEditForm();
                    editPromoteParameters.editAdAd = {};
                }else{
                    alert('Ad update failed');
                }
            }
        );
    }
}
function getEditPlacements(){
	var placements = [];
    if($('#edit-mobile-placement')[0].checked){
		placements.push('mobile');
	}
	if($('#edit-desktop-placement')[0].checked){
		placements.push('desktop');
	}
	if(placements.length == 0){
		placements = ['mobile','desktop'];
	}
	return placements;
}
function resetEditForm(){
	document.getElementById('edit-ad-name').value = '';
	document.getElementById('edit-budget').value = '';
	document.getElementById('edit-bid').value = '';
	document.getElementById('edit-min-age').value = '';
	document.getElementById('edit-max-age').value = '';
	
//	$('#bid_label')[0].innerHTML = 'Bid (USD)';
//	$('#budget_label')[0].innerHTML = 'Budget (USD)';
//	var budget = $('#budget').parsley();
//	budget.options.range = [20,2000];
//	var bid = $('#bid').parsley();
//	bid.options.range = [0.01,0.05];
}
function hideTargeting(){
    if($('#leave-targeting')[0].checked){
        $('#edit-locations').hide();
    }else{
        $('#edit-locations').show();
    }
}
function getClientKomfo(id){
    $('#client_post_id').html(id);
    $.ajax({
        url: "php_scripts/get_client.php",
		dataType:"json",
		async:false,
		method:'get',
        data: {'id': id},
		success: function(response){
			$('#client_name').val(response.client);
            $('#client_campaign_name').val(response.client_campaign);
		}
    });
}
function setClient(){
    $.ajax({
        url: "php_scripts/set_client.php",
		dataType:"json",
		async:false,
		method:'POST',
        data: {client: $('#client_name').val(), client_campaign: $('#client_campaign_name').val(), id: $('#client_post_id').html()},
		success: function(response){
			console.log('client update succesful');
            setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
		}
    });
}
//Edit Promote

//posts quickview
var data_quickview=[];
var metric_quickview=0;
var formating_quickview;
var quickview_currency;
var quickview_post_id;
var quickview_campaign;
function UpdateDataQuickview(){
	var time_lapse = document.getElementById("time_lapse_quickview").value;
	$.ajax({
		url: "php_scripts/get_post_data.php",
		dataType: "json",
		data: {post_id: quickview_post_id, time_lapse: time_lapse, campaign: quickview_campaign},
		method: 'get',
		async: false,
		success: function(response){
			data_quickview=response;
			for(var g=0;g<data_quickview.length;g++)
			{
				if(parseFloat(data_quickview[g].spend)>0){
					var paid_cels = document.getElementsByClassName("paid");
					for(var i=0;i<paid_cels.length;i++){
						paid_cels.item(i).style.visibility = "visible";
					}
					break;
				}
				else{
					var paid_cels = document.getElementsByClassName("paid");
					for(var i=0;i<paid_cels.length;i++){
						paid_cels.item(i).style.visibility = "hidden";
					}
				}
			}
		}
	});
	$.ajax({
		url: "php_scripts/get_post_currency.php",
		dataType: "text",
		data: {post_id: quickview_post_id},
		method: 'get',
		async: false,
		success: function(response){
			quickview_currency=response;
		}
	});
}
function selectPostQuickview(post_id, name, campaign){
	quickview_post_id = post_id;
	quickview_campaign = campaign;
	$('#quickview_title')[0].innerHTML = name;
	$('#quickview_post_id')[0].innerHTML = post_id;
	UpdateDataQuickview();
	drawChartQuickview(metric_quickview);
}
function drawChartQuickview(measure){
  metric_quickview=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(quickview_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', 'Gasto');
		break;
	case 2:
		data_table.addColumn('number', 'CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', 'Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', 'Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', 'Penetration');
		break;
	case 6:
		data_table.addColumn('number', 'CXW');
		break;
	case 7:
		data_table.addColumn('number', 'CPM');
		break;
	case 8:
		data_table.addColumn('number', 'CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', 'Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', 'Shares');
		break;
	case 11:
		data_table.addColumn('number', 'Likes');
		break;
	case 12:
		data_table.addColumn('number', 'Comments');
		break;
	case 13:
		data_table.addColumn('number', 'Link CPM');
		break;
	case 14:
		data_table.addColumn('number', 'RT CXW');
		break;
	default:
		data_table.addColumn('number', 'CTR Organico');
		break;
  }

  for(var n=0;n<data_quickview.length;n++){
	data_quickview[n].record_time=data_quickview[n].record_time.replace(' ','T');
	data_quickview[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_quickview[n].record_time)

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].spend)*currency_factor]);
		  formating_quickview = {
			title: 'Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_quickview[n].organic_ctr)/100, f: data_quickview[n].organic_ctr+'%'}]);
		  formating_quickview = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].paid_link_clicks)]);
		  formating_quickview = {
			title: 'Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].viral_amp), f: data_quickview[n].viral_amp+'x'}]);
		  formating_quickview = {
			title: 'Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].penetration)/100, f: data_quickview[n].penetration+'%'}]);
		  formating_quickview = {
			title: 'Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].cxw)*currency_factor]);
		  formating_quickview = {
			title: 'CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].cpm)*currency_factor]);
		  formating_quickview = {
			title: 'CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].paid_ctr)/100, f:data_quickview[n].paid_ctr+'%'}]);
		  formating_quickview = {
			title: 'CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].link_clicks)]);
		  formating_quickview = {
			title: 'Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].shares)]);
		  formating_quickview = {
			title: 'Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].likes)]);
		  formating_quickview = {
			title: 'Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].comments)]);
		  formating_quickview = {
			title: 'Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].link_cpm)*currency_factor]);
		  formating_quickview = {
			title: 'Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 14:
		  if(n<data_quickview.length-1){
			  var rt_cxw=(parseFloat(data_quickview[n+1].spend)-parseFloat(data_quickview[n].spend))/(parseFloat(data_quickview[n+1].paid_link_clicks)-parseFloat(data_quickview[n].paid_link_clicks))*currency_factor;
			  data_table.addRow([
				  data_date,
				  rt_cxw
			  ]);
			  formating_quickview = {
				title: 'RT CXW',
				//curveType: 'function',
				vAxis: {
					format: 'currency'
				},
				hAxis: {
					format: 'M/d HH:mm',
					gridlines:{
						count: -1
					},
					title: 'Hora'
				},
				legend:{
					position: 'none'
				},
				pointSize: 3,
				trendlines: { 
					0: {
						lineWidth: 10,
						color: 'green',
						opacity: 1
					} 
				}
			  };
		  }
		  break;
	  default:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].organic_ctr)/100, f: data_quickview[n].organic_ctr+'%'}]);
		  formating_quickview = {
			title: 'CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_quickview'));

  table.draw(data_table,formating_quickview);
}
function drawDeltaChartQuickview(measure){
  metric_quickview=measure;
  var data_table = new google.visualization.DataTable();
  var currency_factor;

  if(quickview_currency=="MXN"){
	currency_factor=1/19.5;
  }else{
	currency_factor=1;
  }

  data_table.addColumn('datetime', 'Hora');
  switch(measure){
	case 1:
		data_table.addColumn('number', '\u0394 Gasto');
		break;
	case 2:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
	case 3:
		data_table.addColumn('number', '\u0394 Link Clicks Pagados');
		break;
	case 4:
		data_table.addColumn('number', '\u0394 Viral AMP');
		break;
	case 5:
		data_table.addColumn('number', '\u0394 Penetration');
		break;
	case 6:
		data_table.addColumn('number', '\u0394 CXW');
		break;
	case 7:
		data_table.addColumn('number', '\u0394 CPM');
		break;
	case 8:
		data_table.addColumn('number', '\u0394 CTR Pagado');
		break;
	case 9:
		data_table.addColumn('number', '\u0394 Link Clicks');
		break;
	case 10:
		data_table.addColumn('number', '\u0394 Shares');
		break;
	case 11:
		data_table.addColumn('number', '\u0394 Likes');
		break;
	case 12:
		data_table.addColumn('number', '\u0394 Comments');
		break;
	case 13:
		data_table.addColumn('number', '\u0394 Link CPM');
		break;
	case 14:
		data_table.addColumn('number', '\u0394 RT CXW');
		break;
	default:
		data_table.addColumn('number', '\u0394 CTR Organico');
		break;
  }

  for(var n=1;n<data_quickview.length;n++){
	data_quickview[n].record_time=data_quickview[n].record_time.replace(' ','T');
	data_quickview[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
	var data_date=new Date(data_quickview[n].record_time)

	switch(measure)
	{
	  case 1:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].spend)*currency_factor-parseFloat(data_quickview[n-1].spend)*currency_factor]);
		  formating_quickview = {
			title: '\u0394 Gasto',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 2:
		  data_table.addRow([data_date,
		  {v: parseFloat(data_quickview[n].organic_ctr)/100-parseFloat(data_quickview[n-1].organic_ctr)/100, f: (parseFloat(data_quickview[n].organic_ctr)-parseFloat(data_quickview[n-1].organic_ctr))+'%'}]);
		  formating_quickview = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 3:
		  data_table.addRow([data_date,
		  parseFloat(data_quickview[n].paid_link_clicks)-parseFloat(data_quickview[n-1].paid_link_clicks)]);
		  formating_quickview = {
			title: '\u0394 Link Clicks Pagados',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 4:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].viral_amp)-parseFloat(data_quickview[n-1].viral_amp), f: (parseFloat(data_quickview[n].viral_amp)-parseFloat(data_quickview[n-1].viral_amp))+'x'}]);
		  formating_quickview = {
			title: '\u0394 Viral AMP',
			//curveType: 'function',
			vAxis: {
				format: '#.##x'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 5:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].penetration)/100-parseFloat(data_quickview[n-1].penetration)/100, f: (parseFloat(data_quickview[n].penetration)-parseFloat(data_quickview[n-1].penetration))+'%'}]);
		  formating_quickview = {
			title: '\u0394 Penetration',
			//curveType: 'function',
			vAxis: {
				format: '##.####%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 6:
		  data_table.addRow([data_date,(parseFloat(data_quickview[n].cxw)-parseFloat(data_quickview[n-1].cxw))*currency_factor]);
		  formating_quickview = {
			title: '\u0394 CXW',
			//curveType: 'function',
			vAxis: {
				format: '$0.######'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 7:
		  data_table.addRow([data_date,(parseFloat(data_quickview[n].cpm)-parseFloat(data_quickview[n-1].cpm))*currency_factor]);
		  formating_quickview = {
			title: '\u0394 CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 8:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].paid_ctr)/100-parseFloat(data_quickview[n-1].paid_ctr)/100, f: (parseFloat(data_quickview[n].paid_ctr)-parseFloat(data_quickview[n-1].paid_ctr))+'%'}]);
		  formating_quickview = {
			title: '\u0394 CTR Pagado',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 9:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].link_clicks)-parseFloat(data_quickview[n-1].link_clicks)]);
		  formating_quickview = {
			title: '\u0394 Link Clicks',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 10:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].shares)-parseFloat(data_quickview[n-1].shares)]);
		  formating_quickview = {
			title: '\u0394 Shares',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 11:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].likes)-parseFloat(data_quickview[n-1].likes)]);
		  formating_quickview = {
			title: '\u0394 Likes',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 12:
		  data_table.addRow([data_date,parseFloat(data_quickview[n].comments)-parseFloat(data_quickview[n-1].comments)]);
		  formating_quickview = {
			title: '\u0394 Comments',
			//curveType: 'function',
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  case 13:
		  data_table.addRow([data_date,(parseFloat(data_quickview[n].link_cpm)-parseFloat(data_quickview[n-1].link_cpm))*currency_factor]);
		  formating_quickview = {
			title: '\u0394 Link CPM',
			//curveType: 'function',
			vAxis: {
				format: 'currency'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	  default:
		  data_table.addRow([data_date,{v: parseFloat(data_quickview[n].organic_ctr)/100-parseFloat(data_quickview[n-1].organic_ctr)/100, f: (parseFloat(data_quickview[n].organic_ctr)-parseFloat(data_quickview[n].organic_ctr))+'%'}]);
		  formating_quickview = {
			title: '\u0394 CTR Orgánico',
			//curveType: 'function',
			vAxis: {
				format: '##.##%'
			},
			hAxis: {
				format: 'M/d HH:mm',
				gridlines:{
					count: -1
				},
				title: 'Hora'
			},
			legend:{
				position: 'none'
			},
			pointSize: 3,
		  };
		  break;
	}
  }

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.LineChart(document.getElementById('chart_div_quickview'));

  table.draw(data_table,formating_quickview);
}
//posts quickview

//One click promote
var adCreative = {};
var adCampaign = {};
var adSet = {};
var adAd = {};
var promoteParameters = {adCampaign, adSet, adCreative, adAd};
function getPromotePostId(id){
	var now =new Date();
	$('#promote-datepicker').val(now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate());
	//console.log(id);
	$('#promote-timepicker').timepicker('setTime', new Date());
	if(!document.getElementById('mobile_placement').checked){console.log('reset mobile');document.getElementById('mobile_placement').click();}
	if(!document.getElementById('desktop_placement').checked){document.getElementById('desktop_placement').click();}
	if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
	promoteParameters.adCreative.object_story_id = id;
	promoteParameters.adCreative.url_tags = 'p1=true';
	var selector=document.getElementById("exclude");
	selector.selectedIndex = getExcludedDate();
	getObjective(id);
}
function deleteLimit(){
	$('#limit_value')[0].value = '';
}
function resetForm(){
	document.getElementById('name').value = '';
	document.getElementById('budget').value = '';
	document.getElementById('bid').value = '0.01';
	document.getElementById('min_age').value = '16';
	document.getElementById('max_age').value = '65';
	$('#ad_account')[0].selectedIndex = 0;
	$('#excluded_group')[0].style.visibility = 'visible';
	$('#gcs_group')[0].style.visibility = 'visible';
	if(!document.getElementById('mobile_placement').checked){console.log('reset mobile');document.getElementById('mobile_placement').click();}
	if(!document.getElementById('desktop_placement').checked){document.getElementById('desktop_placement').click();}
	if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
	$('#bid_label')[0].innerHTML = 'Bid (USD)';
	$('#budget_label')[0].innerHTML = 'Budget (USD)';
	var budget = $('#budget').parsley();
	budget.options.range = [20,5000];
	var bid = $('#bid').parsley();
	bid.options.range = [0.01,0.05];
	$('#no_limit')[0].click();
    $('#do-not-notify')[0].checked = false;
}
function adAccountSelected(id){
	typeof id === 'undefined' ? id=promoteParameters.adCreative.object_story_id : id=id;
	//console.log('In adAccountSelected',id);
	switch(true){
		case ($('#ad_account').val() == 'act_958749654167036' || $('#ad_account').val() == 'act_986080914767243' || $('#ad_account').val() == 'act_191281401594032'):
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'hidden';
            $('#do-not-notify')[0].checked = true;
			if(!isVideo(id)){
				if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
				$('#gcs_group')[0].style.visibility = 'visible';
			}else{
				document.getElementById('radio_GCS_None').checked = true;
				$('#gcs_group')[0].style.visibility = 'hidden';
			}
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : ($('#ad_account').val() == 'act_191281401594032' ? [{name: 'El deforma 5 days', id: '23842843732900399'},{name: 'El deforma 10 days', id: '23842843733770399'},{name: 'El deforma 15 days',id: '23842843733790399'},{name: 'El deforma 20 days', id: '23842843733950399'},{name: 'El deforma 25 days', id: '23842843734300399'},{name: 'El deforma 30 days', id: '23842843734330399'}] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]);
            var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 0;
			var budget = $('#budget').parsley();
			budget.options.range = [20,5000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = false;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case ($('#ad_account').val() == 'act_31492866' && $('#ad_account')[0].selectedIndex < 3):
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'visible';
            $('#do-not-notify')[0].checked = false;
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 1;
			var budget = $('#budget').parsley();
			budget.options.range = [20,2000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true&b1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = false;
            $('#latam')[0].checked = true;
			break;
		case ($('#ad_account').val() == 'act_955415581167110'):
			$('#bid_label')[0].innerHTML = 'Bid (MXN)';
			$('#budget_label')[0].innerHTML = 'Budget (MXN)';
            $('#notify-group')[0].style.visibility = 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			selector.selectedIndex = 0;
			//$('#excluded_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			var budget = $('#budget').parsley();
			budget.options.range = [500,8000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.2,0.5];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case ($('#ad_account').val() == 'act_1813540365354623'):
			$('#bid_label')[0].innerHTML = 'Bid (MXN)';
			$('#budget_label')[0].innerHTML = 'Budget (MXN)';
            $('#notify-group')[0].style.visibility = 'hidden';
			//$('#excluded_group')[0].style.visibility = !isVideo ? 'visible' : 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
            $('#objective')[0].selectedIndex = 2;
            $('#billing_event')[0].selectedIndex = 2;
            $('#optimization')[0].selectedIndex = 2;
			/*var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6034155584927'},{name: 'El deforma 10 days', id: '6034155576527'},{name: 'El deforma 15 days',id: '6034155568527'},{name: 'El deforma 20 days', id: '6034155515127'},{name: 'El deforma 25 days', id: '6034155490727'},{name: 'El deforma 30 days', id: '6020024675727'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = getExcludedDate();*/
			var budget = $('#budget').parsley();
			budget.options.range = [20,40000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,5];
			promoteParameters.adCreative.url_tags = 'p1=true&b1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case ($('#ad_account').val() == 'act_31492866' && $('#ad_account')[0].selectedIndex >= 3):
            $('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'hidden';
            $('#do-not-notify')[0].checked = true;
			if(!isVideo(id)){
				if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
				$('#gcs_group')[0].style.visibility = 'visible';
			}else{
				document.getElementById('radio_GCS_None').checked = true;
				$('#gcs_group')[0].style.visibility = 'hidden';
			}
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 0;
			var budget = $('#budget').parsley();
			budget.options.range = [20,5000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = false;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
	}
}
function getPromoteParameters(){
	promoteParameters.adCampaign.name = document.getElementById('name').value;
	promoteParameters.adCampaign.buying_type = 'AUCTION';
	promoteParameters.adCampaign.objective = document.getElementById('objective').value;
	promoteParameters.adCampaign.status = 'ACTIVE';	
	
	promoteParameters.adSet.name = document.getElementById('name').value;
	promoteParameters.adSet.bid_amount = parseFloat(document.getElementById('bid').value)*100;
	promoteParameters.adSet.billing_event = document.getElementById('billing_event').value;
	promoteParameters.adSet.campaign_id = '';
	promoteParameters.adSet.daily_budget = parseFloat(document.getElementById('budget').value)*100;
	promoteParameters.adSet.is_autobid= false;
	promoteParameters.adSet.optimization_goal = document.getElementById('optimization').value;
	promoteParameters.adSet.status= 'ACTIVE';
	promoteParameters.adSet.start_time =new Date($('#promote-datepicker').val()+' '+$('#promote-timepicker').data('timepicker').hour.toString()+':'+$('#promote-timepicker').data('timepicker').minute.toString()+' '+$('#promote-timepicker').data('timepicker').meridian).getTime()/1000;
	promoteParameters.adSet.targeting = {
		geo_locations: {
			countries: getCountries()
		},
		age_min: parseInt(document.getElementById('min_age').value),
		age_max: parseInt(document.getElementById('max_age').value),
		publisher_platforms: ['facebook'],
		device_platforms: getPlacements(),
		facebook_positions: ['feed']
	};
	if($("#exclude")[0].value !== ''){
		promoteParameters.adSet.targeting.excluded_custom_audiences = [{'id': document.getElementById('exclude').value}];
	}
    if($("#connections")[0].value !== ''){
		promoteParameters.adSet.targeting.connections = [{'id': document.getElementById('connections').value}];
	}
    if($('#objective')[0].value == 'CONVERSIONS'){
        promoteParameters.adSet.attribution_spec = [{
            event_type: "CLICK_THROUGH",
            window_days: 1
        }];
        promoteParameters.adSet.promoted_object = {
            pixel_id: $('#ad_account').val() == 'act_191281401594032' ? '201069333861826' : "286492281506212",
            custom_event_type: "CONTENT_VIEW"
        };
    }
    
	promoteParameters.adAd.name = document.getElementById('name').value;
	promoteParameters.adAd.adset_id = '';
	promoteParameters.adAd.creative = {'creative_id': ''};
	promoteParameters.adAd.status = 'ACTIVE';
	
	createCampaign();
}
function createCampaign(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/campaigns',
		'POST',
		promoteParameters.adCampaign,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adSet.campaign_id = response.id;
				setTimeout(createAdset(),2000);
			}else{
				alert(response.error.error_user_msg);
			}
		}
	);
}
function createAdset(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/adsets',
		'POST',
		promoteParameters.adSet,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adAd.adset_id = response.id;
				setTimeout(createCreative(),500);
			}else{
				alert(response.error.error_user_msg);
			}
		}
	);
}
function createCreative(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/adcreatives',
		'POST',
		promoteParameters.adCreative,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adAd.creative.creative_id = response.id;
				setTimeout(createAd(),500);
			}
			else{
				alert(response.error.error_user_msg);
			}
		}
	);
}
function createAd(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/ads',
		'POST',
		promoteParameters.adAd,
		function(response) {
            console.log(response);
			if(response.hasOwnProperty('id')){
				alert('Campaign successfully created!');
				updatePromotedPosts();
				setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
				if($('input[name=radio]:checked').val() == 'full'){
					activateGCS(promoteParameters.adCreative.object_story_id);
				}else if($('input[name=radio]:checked').val() == 'hybrid'){
					activateHybridGCS(promoteParameters.adCreative.object_story_id);
				}
				if($('#ad_account')[0].value == 'act_31492866' && !$('#do-not-notify')[0].checked){
					$.post(
						"https://hooks.slack.com/services/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",
						JSON.stringify({
							"channel": '#implementar_comercial',
							"username": 'Alerta de Campaña',
							"attachments": [{
								"fallback": 'Campaña iniciada',
								"color": "green",
								"author_name": 'Campaign Bot',
								"title": 'Campaña iniciada',
								"text": 'La campaña para: '+promoteParameters.adCampaign.name+'\nha empezado a promoverse\n'
							}]
						})
					);
				}
			}else{
				alert(response.error.error_user_msg);
			}
			resetForm();
            selected_unpublished_post = {};
		}
	);
}
function activateGCS(id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link;
			var slug = link.substr(32,link.length).substr(0,link.substr(32,link.length).indexOf('/'));
			console.log(slug);
			getPostIdForGCS(slug,id);
		}
	);
}
function getPostIdForGCS(slug,id){
	$.ajax({
		url: 'http://eldeforma.com/api/get_post/?slug='+slug+'&include=id',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			if(!data.hasOwnProperty('error')){
				console.log(data.post.id);
				updatePostGCS(data.post.id,id);
			}else{
				alert('find post: '+data.error);
			}
		}
	});
}
function updatePostGCS(id,fb_id){
	var wp_nonce;
	$.ajax({
		url: 'http://eldeforma.com/api/get_nonce/?controller=posts&method=update_post',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			wp_nonce = data.nonce;
			$.ajax({
				url: 'http://eldeforma.com/api/get_post/?post_id='+id,
				headers: {
					'Authorization':'Basic dan:Db5471033187'
				},
				method: 'GET',
				async: false,
				dataType: 'jsonp',
				success: function(data){
					var previousContent = data.post.content;
					$.ajax({
						url: 'http://eldeforma.com/api/posts/update_post/?id='+id,
						headers: {
							'Authorization':'Basic dan:Db5471033187'
						},
						method: 'POST',
						async: false,
						dataType: 'jsonp',
						data: {
							content: '<div class="p402_premium">\n'+previousContent+'<\/div>\n<script type="text\/javascript">\n\ttry { _402_Show(); } catch(e) {}\n<\/script>',
							nonce: wp_nonce
						},
						success: function(data){
							removeInstant(fb_id);
							alert('GCS successfully added');
							updateDatabaseGCS(fb_id);
						}
					});
				}
			});
		}
	});
}
function updateDatabaseGCS(id){
	$.ajax({
		url: 'php_scripts/updateGCS.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			id: id
		},
		success: function(response){
			
		}
	});
	setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
}
function removeInstant(fb_id){
	FB.api(
		'/'+fb_id,
		'GET',
		{"fields":"link"},
		function(response) {
			console.log('in remove instant',response);
			var link = response.link.substr(0,response.link.indexOf('?'));
			FB.api(
				'/',
				'GET',
				{"id":link,"fields":"instant_article"},
				function(answer) {
				  console.log('in getting instant article for deletion',answer);
				  unpublish(answer.instant_article);
				}
			);
		}
	);
}
function unpublish(instant_article){
	console.log(instant_article,' in instant articles deletion');
	setTimeout(function(){
		FB.api(
			'/eldeforma/instant_articles',
			'POST',
			{"html_source": instant_article.html_source,'published':false,'development_mode':false},
			function(result) {
			  console.log('unpublished ',result.canonical_url);
			}		
		);
	},10000);
}
function getPlacements(){
	var placements = [];
	if(document.getElementById('desktop_placement').checked){
		placements.push('desktop');
	}
	if(document.getElementById('mobile_placement').checked){
		placements.push('mobile');
	}
	if(placements.length == 0){
		placements = ['desktop','mobile'];
	}
	return placements;
}
function stopCampaign(post_id,campaign){
	updateStoppedCampaign(post_id);
	/*$.ajax({
		url: 'php_scripts/get_campaign.php',
		method: 'GET',
		async: false,
		dataType: 'json',
		data: {post_id: post_id},
		success: function(response){
			console.log(response);
			/*if(response.ad_account == 'act_31492866'){
				$.post(
					"https://hooks.slack.com/services/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",
					JSON.stringify({
						"channel": '#implementar_comercial',
						"username": 'Alerta de Campaña',
						"attachments": [{
							"fallback": 'Campaña interrumpida',
							"color": "red",
							"author_name": 'Campaign Bot',
							"title": 'Campaña interrumpida',
							"text": 'La campaña para: '+response.name+'\nse ha interrumpido\n'
						}]
					})
				);
			}*/
			FB.api(
				'/'+campaign,
				'POST',
				{status: 'PAUSED'},
				function(response) {
					if(!response.hasOwnProperty('error')){
						console.log(response);
						alert('Campaign successfully paused!');
						setTimeout(function(){$('.nav-pills>li.active>a')[0].click();},1000);
					}else{
						alert(response.error.message);
					}
				}
			);
		/*}
	});*/
}
function getExcludedDate(){
	var now = new Date().getDate();
	switch(true){
		case now<=5:
			return 1;
			break;
		case now<=10:
			return 2;
			break;
		case now<=15:
			return 3;
			break;
		case now<=20:
			return 4;
			break;
		case now<=25:
			return 5;
			break;
		case now<=31:
			return 6;
			break;
	}
}
function getObjective(id){
	var selector_objective = document.getElementById("objective");
	var selector_billing = document.getElementById("billing_event");
	var selector_optimization = document.getElementById("optimization");
	if(isVideo(id)){
		selector_objective.selectedIndex = 2;
		selector_billing.selectedIndex = 2;
		selector_optimization.selectedIndex = 2;
		document.getElementById('radio_GCS_None').checked = true;
		$('#gcs_group')[0].style.visibility = 'hidden';
	}else{
		selector_objective.selectedIndex = 0;
		selector_billing.selectedIndex = 0;
		selector_optimization.selectedIndex = 0;
	}
	console.log('In get Objective',id);
	adAccountSelected(id);
}
function updatePromotedPosts(){
	$.ajax({
		url: 'php_scripts/update_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
            account: $('#ad_account').val(),
            unpublished: $.isEmptyObject(selected_unpublished_post) ? 0 : selected_unpublished_post,
			campaign_id: promoteParameters.adSet.campaign_id,
			post_id: promoteParameters.adCreative.object_story_id,
            client: document.getElementById('client').value,
            client_campaign: document.getElementById('client_campaign').value
		},
		success: function(response){
			console.log(response);
            console.log('successfully inserted unpublished post');
		}
	});
	if($('input[name=radioLimit]:checked', '#form').val() == 'spend_limit' || $('input[name=radioLimit]:checked', '#form').val() == 'goal'){
		$.ajax({
			url: 'php_scripts/update_campaign_goal.php',
			method: 'POST',
			async: false,
			dataType: 'json',
			data: {
				post_id: promoteParameters.adCreative.object_story_id,
				type: $('input[name=radioLimit]:checked', '#form').val(),
				value: parseInt($('#limit_value')[0].value)
			},
			success: function(response){
				console.log(response);
			}
		});
	}
}
function updateStoppedCampaign(id){
	$.ajax({
		url: 'php_scripts/update_stopped_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			post_id: id
		},
		success: function(response){
			console.log(response);
		}
	});
}
function isVideo(id){
	var	flag;
	$.ajax({
		url: 'https://graph.facebook.com/v2.12/'+id+'?fields=type'+"&"+access_token,
		async: false,
		method: 'get',
		success: function(response){
			if(response.type == 'video'){
				flag = true;
			}else{
				flag = false;
			}
		}
	});
	return flag;
}
function changeObjective(){
	var index = document.getElementById('objective').selectedIndex;
	document.getElementById('billing_event').selectedIndex = index;
	document.getElementById('optimization').selectedIndex = index;
}
function getCountries(){
    var countries=[];
    if($('#mexico')[0].checked){
       countries.push('MX');
    }
    if($('#latam')[0].checked){
        countries.push('BZ', 'PA', 'AR', 'CL', 'PE', 'CO', 'EC', 'BO', 'GT', 'CR', 'SV', 'HN', 'NI', 'PY', 'UY');
    }
    if(countries.length == 0){
        return ['MX'];
    }
    else{
        return countries;
    }
}
//One click promote

//Predictor
function selectPredictCampaign(post_id,campaign,name,ctr,paid_imp){
    $('#predict_title')[0].innerHTML = name;
    $('#predict_campaign')[0].innerHTML = campaign;
    $('#predict_post_id')[0].innerHTML = post_id;
    $('#predict_paid_imp')[0].innerHTML = paid_imp;
    $('#predict_paid_ctr')[0].innerHTML = ctr;
    updateDataPredict(post_id,campaign,ctr,paid_imp);
}
function updateDataPredict(post_id,campaign,ctr,paid_imp){
    $.ajax({
       url: 'php_scripts/predict_start_date.php',
       type: 'get',
       dataType: 'json',
       async: false,
       data: {'campaign': campaign, 'post_id': post_id},
       success: function(response){
           var start_date = new Date(response);
           var days_running = parseFloat(((new Date().getTime()-start_date.getTime())/(1000*60*60*24)).toFixed(4));
           var sigma = parseFloat($('#sigma').val());
           var x_plus = parseFloat($('#x_plus').val());
           var y_plus = parseFloat($('#y_plus').val());
           var delta_x = parseFloat($('#d_x').val());
           var data=[];
           var days_forward = parseFloat($('#time_forward').val());
           for(var x=0;x<=days_forward;x+=0.007){
               data.push([x, predictedClicks(x,sigma,parseFloat(ctr),parseInt(paid_imp),days_running,days_forward,x_plus,y_plus,delta_x)]);
           }
           setTimeout(function(){drawChartPredict(data,post_id,campaign)},1000);
       }
   });
}
function predictedClicks(x,s,C,i,d,D,X,Y,dx){
    var y=0;
    for(var k=1;k<=Math.ceil(D);k++){
        y+=(i/d)*(C/100)*(1/(1+Math.exp(-s*((x-X)-k+0.5+DeltaX(x,dx)*(k-1)))))*Delta(x,D)+Y;
    }
    return y;
}
function Delta(x,D){
    return x >= D ? 1 : 0.9;
}
function DeltaX(x,dx){
    return x < 0.5 ? 0 : dx;
}
function drawChartPredict(data_array,post_id,campaign){
    //console.log(data_array);
    var real_data;
    $.ajax({
       url: 'php_scripts/predict_get_real_data.php',
       type: 'get',
       async: false,
       dataType: 'json',
       data: {'campaign': campaign, 'post_id': post_id},
       success: function(response){
           real_data = response;
       }
    });
    //console.log(real_data);
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Días');
    data.addColumn('number', 'Predicted Link Clicks');
    data.addColumn('number', 'Real Link Clicks');

    for(var i=0;i<data_array.length;i++){
        if(i<real_data.length){
            data.addRow([
                data_array[i][0],
                Math.trunc(data_array[i][1]),
                parseInt(real_data[i])
            ]);
        }else{
            data.addRow([
                data_array[i][0],
                Math.trunc(data_array[i][1]),
                null
            ]);
        }
    }

    var options = {
        title: 'Predicción',
        curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('chart_div_predict'));

    chart.draw(data, options);
}
function updateSigmaPredict(){
    updateDataPredict($('#predict_post_id').html(),$('#predict_campaign').html(),$('#predict_paid_ctr').html(),$('#predict_paid_imp').html());
}
//Predictor
