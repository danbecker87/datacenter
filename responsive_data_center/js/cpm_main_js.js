//active page
function getActivePage(){
	$(document).ready(function(){
		var page_url=window.location.href;
		//console.log($('#sidebar-menu')[0].childNodes[1].childNodes[1]);
		for(var i=3;i<$('#sidebar-menu')[0].childNodes[1].childNodes.length;i+=2){
			var sidebar_tag=$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].href;
			//console.log(sidebar_tag,page_url);
			if(sidebar_tag.indexOf(page_url) > -1){
				$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].className = "waves-effect active";
			}
			//console.log(i);
		}
	});
}
//active page

//General
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
//General

var arbitraje={};
var start, end;
//get all dates
function getCPM(since,until){
	arbitraje={};
	start = new Date(since);
	end = new Date(until);
	var current = new Date(start.getTime());
	while (current.getTime() <= end.getTime()){
		arbitraje[current.toISOString().substr(0,10)] = {};
		arbitraje[current.toISOString().substr(0,10)].spend = 0;
		arbitraje[current.toISOString().substr(0,10)].FAN = 0;
		arbitraje[current.toISOString().substr(0,10)].ADX = 0;
		arbitraje[current.toISOString().substr(0,10)].clicks = 0;
		arbitraje[current.toISOString().substr(0,10)].paid_clicks = 0;
		arbitraje[current.toISOString().substr(0,10)].paid_likes = 0;
        arbitraje[current.toISOString().substr(0,10)].pageviews = 0;
		current.setTime(current.getTime()+1000*60*60*24);
	}
	getFBSpend();
}
//get all dates

//Facebook Spend
function getFBSpend(){
	
	var tomorrow=new Date(end.getTime()+1000*60*60*24*2);
	
	FB_spend=0;
	FB.api(
		'/act_986080914767243/insights/',
		'GET',
		{"time_range":{"since":start.toISOString().substr(0,10),"until":tomorrow.toISOString().substr(0,10)},"fields":"spend",'time_increment':'1','limit':100},
		function(response) {
			//console.log(response);
			for(var i=0;i<response.data.length;i++){
				arbitraje[response.data[i].date_start].spend += parseFloat(response.data[i].spend)*18;
			}
			getADXRevenue();
		}
	);
}
//Facebook Spend

//ADX Range Metrics
function getADXRevenue(){
	gapi.client.load('adexchangeseller','v2.0', function() {
		var requestADX = gapi.client.adexchangeseller.accounts.reports.generate({
			accountId: "pub-7894268851903152",
			startDate: start.toISOString().substr(0,10),
			endDate: end.toISOString().substr(0,10),
			dimension: 'date',
			metric: ["Earnings"],
			fields: "rows"
		});
		requestADX.execute(function(response) {
			//console.log(response);
			for(var i=0;i<response.rows.length;i++){
				arbitraje[response.rows[i][0]].ADX += parseFloat(response.rows[i][1]);
			}
			getAnalytics();
            //getFBRevenue();
		});
	});
}
//ADX Range Metrics

//Analyrtics Range Metrics
function getAnalytics(){
	gapi.client.load('analytics','v3', function() {
		var requestAnalytics = gapi.client.analytics.data.ga.get({
			ids: "ga:41142760",
			'start-date': start.toISOString().substr(0,10),
			'end-date': end.toISOString().substr(0,10),
			dimensions: 'ga:date',
			metrics: ["ga:pageviews"],
			fields: "rows"
		});
		requestAnalytics.execute(function(response) {
			console.log(response);
			for(var i=0;i<response.rows.length;i++){
				arbitraje[parseAnalyticsDate(response.rows[i][0])].pageviews += parseFloat(response.rows[i][1]);
                //console.log(parseAnalyticsDate(response.rows[i][0]));
			}
			getFBRevenue();
		});
	});
}
function parseAnalyticsDate(date){
    return date.substr(0,4)+'-'+date.substr(4,2)+'-'+date.substr(6,2);
}
//Analytics Range Metrics

//FB Revenue
function getFBRevenue(){
	FB.api(
		'/980493918670682/app_insights/app_event/',
		'GET',
		{"aggregateBy":"SUM","event_name":"fb_ad_network_revenue","summary":"true","since":start.toISOString().substr(0,10)+'+1days','until':end.toISOString().substr(0,10)+'+1days'},
		function(response) {
			//console.log(response);
			for(var i=0;i<response.data.length;i++){
				arbitraje[response.data[i].time.substr(0,response.data[i].time.indexOf('T'))].FAN += parseFloat(response.data[i].value)*18;
			}
			getFBClicks();
		}
	);
}
//FB Revenue

//FB Clicks
function getFBClicks(){
	
	//Clicks
	FB.api(
		'/eldeforma/insights/page_consumptions_by_consumption_type/day',
		'GET',
		{"fields":"values.fields(value,end_time)","since":start.toISOString().substr(0,10),'until':end.toISOString().substr(0,10)+'+2days'},
		function(response) {		
			//console.log(response);
			for(var i=0;i<response.data[0].values.length;i++){
				var key = new Date(new Date(response.data[0].values[i].end_time).getTime()-new Date().getTimezoneOffset()*60*1000-3*60*60*1000).toISOString().substr(0,10);
				//console.log(key);
				arbitraje[key].clicks += response.data[0].values[i].value['link clicks'];
			}
		}
	);
	//Paid likes
	FB.api(
		'/eldeforma/insights/page_fan_adds_by_paid_non_paid_unique/day',
		'GET',
		{"fields":"values.fields(value,end_time)","since":start.toISOString().substr(0,10),'until':end.toISOString().substr(0,10)+'+2days'},
		function(response) {
			for(var i=0;i<response.data[0].values.length;i++){
				var key = new Date(new Date(response.data[0].values[i].end_time).getTime()-new Date().getTimezoneOffset()*60*1000-3*60*60*1000).toISOString().substr(0,10);
				//console.log(key);
				arbitraje[key].paid_likes += response.data[0].values[i].value['paid'];
			}
		}
	);
	
	//Paid Clicks
	FB.api(
		'/act_986080914767243/campaigns',
		'GET',
		{"fields":"created_time","limit":"1000"},
		function(response) {
			var campaigns=[];	
			var flag=true;
			var i=0;
			while(flag){
				if(new Date(response.data[i].created_time).getTime() > start.getTime()){
					campaigns.push(response.data[i].id)
					i++;
				}else{
					//console.log(campaigns);
					flag=false;
				}
			}
			//make all dates
			var dates={};
			
			var date_iterator = new Date(start.getTime());
			while(date_iterator.getTime() <= end.getTime()){
				dates[date_iterator.toISOString().substr(0,10)]={};
				date_iterator = new Date(date_iterator.getTime()+24*60*60*1000);
			}
			//console.log(dates);
			clicks_promises=[];
			for(var i=0;i<campaigns.length;i++){
				$.each(dates,function(key,value){
					clicks_promises.push(
						$.ajax({
							url: 'https://graph.facebook.com/v'+version+'/'+campaigns[i]+'/insights?fields=actions&time_range[since]='+key+'&time_range[until]='+key+'&'+access_token,
							type: 'get',
							async: true,
							success: function(response){
								if(response.data.length>0){
									for(var j=0;j<response.data[0].actions.length;j++){
										if(response.data[0].actions[j].action_type == 'link_click'){
											arbitraje[key].paid_clicks += parseInt(response.data[0].actions[j].value);
										}
									}
								}
							}
						})
					);
				});
				//console.log(i+'/'+campaigns.length);
			}
			$.when.apply($, clicks_promises).then(function(){
				analyzeData();
			},function(){
				console.log('promises failed');
			})
			//console.log(i);
			/*if(i == campaigns.length){
				setTimeout(function(){analyzeData()},1000*3);
			}*/
		}
	);
	
}
//FB Clicks

//Analyze Data
function analyzeData(){
	$.each(arbitraje,function(key,value){
		arbitraje[key].total_revenue = arbitraje[key].FAN + arbitraje[key].ADX;
		arbitraje[key].ADX_RPM = arbitraje[key].ADX/arbitraje[key].clicks*1000;
		arbitraje[key].FAN_RPM = arbitraje[key].FAN/arbitraje[key].clicks*1000;
		arbitraje[key].RPM = arbitraje[key].total_revenue/arbitraje[key].clicks*1000;
        arbitraje[key].RPMpageviews = arbitraje[key].total_revenue/arbitraje[key].pageviews*1000;
		arbitraje[key].CPM = arbitraje[key].spend/arbitraje[key].paid_clicks*1000;
		arbitraje[key].CPL = arbitraje[key].spend/arbitraje[key].paid_likes*1000;
	});
	drawCPMTable();
}
//Analyze Data

//Draw CPM Table
function drawCPMTable(){
	var cpm_data = new google.visualization.DataTable();	
  
	cpm_data.addColumn({type: 'date', label: 'Fecha'});
	cpm_data.addColumn({type: 'number', label: 'CPM'});
	cpm_data.addColumn({type: 'number', label: 'FAN RPM'});
	cpm_data.addColumn({type: 'number', label: 'ADX RPM'});
	cpm_data.addColumn({type: 'number', label: 'RPM'});
    cpm_data.addColumn({type: 'number', label: 'RPM/pageviews'});
	//cpm_data.addColumn({type: 'number', label: 'CPL'});
	cpm_data.addColumn({type: 'number', label: 'RPM/CPM'});
	cpm_data.addColumn({type: 'number', label: 'Costo RPM-CPM'});
    cpm_data.addColumn({type: 'number', label: 'Total Spend'});
    cpm_data.addColumn({type: 'number', label: 'Total Revenue'});
    cpm_data.addColumn({type: 'number', label: 'Net Revenue'});

	$.each(arbitraje, function(key, value) {
		cpm_data.addRow([
			new Date(key+'T00:00:00'),
			{v: arbitraje[key].CPM, f: '$ '+(arbitraje[key].CPM).toFixed(2)},
			{v: arbitraje[key].FAN_RPM, f: '$ '+(arbitraje[key].FAN_RPM).toFixed(2)},
			{v: arbitraje[key].ADX_RPM, f: '$ '+(arbitraje[key].ADX_RPM).toFixed(2)},
			{v: arbitraje[key].RPM, f: '$ '+(arbitraje[key].RPM).toFixed(2)},
            {v: arbitraje[key].RPMpageviews, f: '$ '+(arbitraje[key].RPMpageviews).toFixed(2)},
			//{v: arbitraje[key].CPL, f: '$ '+(arbitraje[key].CPL).toFixed(2)},
			{v: arbitraje[key].CPM/arbitraje[key].RPM, f: (arbitraje[key].RPM/arbitraje[key].CPM*100).toFixed(2)+'%'},
			{v: -arbitraje[key].CPM+arbitraje[key].RPM, f: '$ '+(-arbitraje[key].CPM+arbitraje[key].RPM).toFixed(2)},
            {v: arbitraje[key].spend, f: '$ '+numberWithCommas((arbitraje[key].spend).toFixed(2))},
            {v: arbitraje[key].total_revenue, f: '$ '+numberWithCommas((arbitraje[key].total_revenue).toFixed(2))},
            {v: arbitraje[key].total_revenue-arbitraje[key].spend, f: '$ '+numberWithCommas((arbitraje[key].total_revenue-arbitraje[key].spend).toFixed(2))}
            
		]);
	});

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'sortColumn': 0,
		'sortAscending': true
	};
	
	// Instantiate and draw our chart, passing in some options.
	var cpm_table = new google.visualization.Table(document.getElementById('cpm_table'));
  
	cpm_table.draw(cpm_data, options);
}
//Draw CPM Table