//active page
function getActivePage(){
	$(document).ready(function(){
		var page_url=window.location.href;
		//console.log($('#sidebar-menu')[0].childNodes[1].childNodes[1]);
		for(var i=3;i<$('#sidebar-menu')[0].childNodes[1].childNodes.length;i+=2){
			var sidebar_tag=$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].href;
			//console.log(sidebar_tag,page_url);
			if(sidebar_tag.indexOf(page_url) > -1){
				$('#sidebar-menu')[0].childNodes[1].childNodes[i].childNodes[1].className = "waves-effect active";
			}
			//console.log(i);
		}
	});
}
//active page

//General
function numberWithCommas(x) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}
//General

//get all rt_cxw
function getRTCXW(){
	$.ajax({
		url: "php_scripts/CXW_time_analysis.php",
		dataType:"json",
		async:false,
		method:'get',
		success: function(response){
			analyzeData(response);
		}
	});
}
//get all rt_cxw

//Analyze Data
var cxw_data;
function analyzeData(data){
	cxw_data=data;
    var filtered_cxw={};
    for(var j=0;j<24;j++){
        filtered_cxw[String(j)]={};
        filtered_cxw[String(j)].values=[];
    }
    for(var i=0;i<data.length;i++){
        if(new Date(data[i].record_time).getDay() != 6 && new Date(data[i].record_time).getDay()!=0 && data[i].rt_cxw!=false){
            filtered_cxw[String(new Date(data[i].record_time).getHours())].values.push(data[i].rt_cxw);
        }
    }
    $.each(filtered_cxw,function(key, value){
        if(filtered_cxw[key].values.length>0){
           filtered_cxw[key].average = math.mean(filtered_cxw[key].values)
        }
        else{
            filtered_cxw[key].average=0;
        }
    });
    //console.log(filtered_cxw);
	drawCXWTable(filtered_cxw);
}
//Analyze Data

//Draw CPM Table
function drawCXWTable(data){
	var cxw_data = new google.visualization.DataTable();	
  
	cxw_data.addColumn({type: 'string', label: 'Hora'});
	cxw_data.addColumn({type: 'number', label: 'CXW'});
	

	$.each(data, function(key, value) {
		cxw_data.addRow([
			key,
			data[key].average
		]);
	});

	// Set chart options
	var options = {
		//'allowHtml': true,
		//'showRowNumber': true,
		'width':'100%',
		'height':'100%'
		//'sortColumn': 0,
		//'sortAscending': true
	};
	
	// Instantiate and draw our chart, passing in some options.
	var cxw_table = new google.visualization.LineChart(document.getElementById('chart_div_cxw'));
  
	cxw_table.draw(cxw_data, options);
}
//Draw CPM Table