function numberWithCommas(x) {
	if(typeof x === 'undefined'){
        return '0';
    }
    var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

//Better komfo live
var posts=[];
var posts_repsodia=[];
var posts_repsodia_posts=[];
var adgroups=[];
var adgroups_comercial = [];
var posts_cult=[];
var posts_cult_posts=[];
var posts_partido_videos=[];
var posts_partido_posts=[];
var pageFans={};

var accounts=[];

function loadKomfo(){
    
    //get Fans for all pages
    $.each(pageTokens,function(page,token){
           if(!isNaN(page)){
           //console.log(page,token);
           query.edge=page+"/insights/page_fans";
           query.fields="&until=today+2days";
           $.ajax({
                  url: buildURL(query,token,1),
                  type: 'get',
                  dataType: 'json',
                  async: false,
                  success: function(response) {
                      pageFans[page] = response.data[0].values[response.data[0].values.length-1].value;
                  }
          });
       }
    });
    //get Fans for all pages
    
    FB.api('/me', function(response) {
		//
		//El Deforma and promoted Implemmentation
		//
		adgroups = [];
		posts=[];

		//Get adgroups
        
		query.edge ="953559291352739/owned_ad_accounts";//query adgroups
		query.fields = "currency,id";//find active delivering campaigns
		$.ajax({
			url: buildURL(query,access_token,100),
			type: 'get',
			dataType: 'json',
			async: false,
			success: function(account){
                //console.log(account);
				accounts=account.data;
				for(var m=0;m<account.data.length;m++){
                    if(account.data[m].id != 'act_1824216990953627' && account.data[m].id != 'act_1813540365354623'){
                        query.edge = account.data[m].id+"/ads";//query adgroups
                        query.fields = "creative.fields(effective_object_story_id,object_type),conversion_specs,campaign.fields(account_id,name,insights.fields(spend,cost_per_action_type).date_preset(lifetime)),name,effective_status&effective_status=['ACTIVE','PENDING_REVIEW']";//find active delivering campaigns
                        $.ajax({
                            url: buildURL(query,access_token,30),
                            type: 'get',
                            dataType: 'json',
                            async: false,
                            success: function(active){
                                //console.log(active);
                                for(var l=0;l<active.data.length;l++){
                                    adgroups.push(active.data[l]);
                                    adgroups[adgroups.length-1].currency = accounts[m].currency;
                                    adgroups[adgroups.length-1].post_id = active.data[l].creative.effective_object_story_id;
                                    adgroups[adgroups.length-1].type = active.data[l].creative.object_type;
                                    adgroups[adgroups.length-1].campaign_id = active.data[l].campaign.id;
                                    adgroups[adgroups.length-1].account_id = 'act_'+active.data[l].campaign.account_id;
                                }
                            }
                        });
                    }
				}
			}
		});

		//adgroups promises
        var deforma_adgroups_promises = [];
        function getDeformaAdgroupsMetrics(k){
            if(adgroups[k].conversion_specs !== undefined && adgroups[k].campaign.name.indexOf('Espresso') == -1){
                if(adgroups[k].effective_status == 'ACTIVE'){
                    if(adgroups[k].conversion_specs[0]["action.type"][0]=="link_click" || adgroups[k].conversion_specs[0]["action.type"][0]=="post_engagement" || adgroups[k].conversion_specs[0]["action.type"][0]=="offsite_conversion"){
                        query.edge = adgroups[k].id+"/insights";//query active adgroups insights
                        query.fields = "spend,actions,cost_per_action_type,impressions&date_preset=lifetime";//find active delivering campaigns
                        $.ajax({
                            url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                            type: 'get',
                            dataType: 'json',
                            async: true,
                            success: function(response){
                                if(response.data[0]!=null){
                                    adgroups[k].spend=response.data[0].spend;
                                    adgroups[k].paid_link_clicks=0;
                                    adgroups[k].paid_imp=response.data[0].impressions;
                                    if(adgroups[k].type == 'VIDEO'){
                                        for(var o=0;o<response.data[0].actions.length;o++){
                                            if(response.data[0].actions[o].action_type=='video_view'){
                                                adgroups[k].paid_link_clicks=parseInt(response.data[0].actions[o].value);
                                                break;
                                            }
                                        }
                                        adgroups[k].cxw=0;
                                        for(var o=0;o<response.data[0].cost_per_action_type.length;o++){
                                            if(response.data[0].cost_per_action_type[o].action_type=='video_view'){
                                                adgroups[k].cxw=response.data[0].cost_per_action_type[o].value;
                                                break;
                                            }
                                        }  
                                    }else{
                                        for(var o=0;o<response.data[0].actions.length;o++){
                                            if(response.data[0].actions[o].action_type=='link_click'){
                                                adgroups[k].paid_link_clicks=parseInt(response.data[0].actions[o].value);
                                                break;
                                            }
                                        }
                                        adgroups[k].cxw=0;
                                        for(var o=0;o<response.data[0].cost_per_action_type.length;o++){
                                            if(response.data[0].cost_per_action_type[o].action_type=='link_click'){
                                                adgroups[k].cxw=response.data[0].cost_per_action_type[o].value;
                                                break;
                                            }
                                        }  		    
                                    }
                                }
                                adgroups[k].ad_id=adgroups[k].id;
                                delete adgroups[k].id;
                                query.edge=adgroups[k].post_id;
                                query.fields="name"+
                                    ",type"+
                                    ",message"+
                                    ",created_time"+
                                    ",shares"+
                                    ",likes.limit(0).summary(1).as(likes)"+
                                    ",comments.limit(0).summary(1).as(comments)"+
                                    ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                                    ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                                    ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                                    ",insights.metric(post_impressions).fields(values).as(impressions)"+
                                    ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                                    ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                                    ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                                    ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
                                $.ajax({
                                    url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                                    type: 'get',
                                    dataType: 'json',
                                    async: true,
                                    success: function(response) {
                                        //console.log(response);
                                        adgroups[k].name=response.name;
                                        adgroups[k].message=response.message;
                                        adgroups[k].type=response.type;
                                        adgroups[k].created_time = new Date(new Date(response.created_time.replace('+0000','')).getTime()-(new Date().getTimezoneOffset()/60*60*60*1000)).toISOString().substr(0,19);
                                        adgroups[k].shares= typeof response.shares === 'undefined' ? 0 : response.shares.count;
                                        adgroups[k].viral_imp=response.viral_imp.data[0].values[0].value;
                                        adgroups[k].organic_imp=response.organic_imp.data[0].values[0].value;
                                        adgroups[k].imp=response.impressions.data[0].values[0].value;
                                        adgroups[k].reach=response.fan_reach.data[0].values[0].value;
                                        adgroups[k].unique_imp=response.reach.data[0].values[0].value;
                                        adgroups[k].unpaid_imp=adgroups[k].imp-response.impressions_paid.data[0].values[0].value;
                                        //adgroups[k].unpaid_imp=0;
                                        if(adgroups[k].type == 'link' || adgroups[k].type == 'photo'){
                                            adgroups[k].link_clicks=response.consumptions.data[0].values[0].value['link clicks'];
                                        }
                                        if(adgroups[k].type == 'video'){
                                            adgroups[k].link_clicks=response.video_views.data[0].values[0].value;
                                        }
                                        adgroups[k].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                                        adgroups[k].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                                        adgroups[k].fans = typeof pageFans[adgroups[k].post_id.substr(0,adgroups[k].post_id.indexOf('_'))] === 'undefined' ? 0 : pageFans[adgroups[k].post_id.substr(0,adgroups[k].post_id.indexOf('_'))];
                                        adgroups[k].viral_amp=(adgroups[k].viral_imp/adgroups[k].organic_imp).toFixed(2);
                                        adgroups[k].organic_ctr= (adgroups[k].link_clicks-adgroups[k].paid_link_clicks) < 0 ? 0 : ((adgroups[k].link_clicks-adgroups[k].paid_link_clicks)/adgroups[k].unpaid_imp*100).toFixed(2);
                                        adgroups[k].paid_ctr=(adgroups[k].paid_link_clicks/adgroups[k].paid_imp*100).toFixed(2);
                                        adgroups[k].penetration=(adgroups[k].reach/adgroups[k].fans*100).toFixed(2);
                                        adgroups[k].engagement=adgroups[k].likes+adgroups[k].comments+adgroups[k].shares+adgroups[k].link_clicks;
                                        adgroups[k].cpm=(adgroups[k].cxw*1000).toFixed(2);
                                        if(adgroups[k].name == ''){
                                            adgroups[k].name = adgroups[k].message;
                                        }
                                    }
                                });//ajax query for post name
                            }
                        });//ajax query for adgroup insights
                    }
                    if(adgroups[k].conversion_specs[0]["action.type"][0]=="like"){
                        query.edge = adgroups[k].id+"/insights";//query active adgroups insights
                        query.fields = "spend,actions,cost_per_action_type";//find active delivering campaigns
                        $.ajax({
                            url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                            type: 'get',
                            dataType: 'json',
                            async: true,
                            success: function(response){
                                if(response.data[0]!=null){
                                    adgroups[k].spend=response.data[0].spend;
                                    adgroups[k].paid_link_clicks=0;
                                    for(var o=0;o<response.data[0].actions.length;o++){
                                        if(response.data[0].actions[o].action_type=='like'){
                                            adgroups[k].paid_link_clicks=parseInt(response.data[0].actions[o].value);
                                            break;
                                        }
                                    }
                                    adgroups[k].cxw=0;
                                    for(var o=0;o<response.data[0].cost_per_action_type.length;o++){
                                        if(response.data[0].cost_per_action_type[o].action_type=='like'){
                                            adgroups[k].cxw=response.data[0].cost_per_action_type[o].value;
                                            break;
                                        }
                                    }  		    
                                  }
                                adgroups[k].ad_id=adgroups[k].id;
                                delete adgroups[k].id;
                                query.edge=adgroups[k].post_id;
                                query.fields="name"+
                                    ",type"+
                                    ",message"+
                                    ",created_time"+
                                    ",shares"+
                                    ",likes.limit(0).summary(1).as(likes)"+
                                    ",comments.limit(0).summary(1).as(comments)"+
                                    ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                                    ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                                    ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                                    ",insights.metric(post_impressions).fields(values).as(impressions)"+
                                    ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                                    ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                                    ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                                    ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
                                $.ajax({
                                    url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                                    type: 'get',
                                    dataType: 'json',
                                    async: true,
                                    success: function(response) {
                                        adgroups[k].name=response.name;
                                        adgroups[k].message=response.message;
                                        adgroups[k].type=response.type;
                                        adgroups[k].created_time = new Date(new Date(response.created_time.replace('+0000','')).getTime()-(new Date().getTimezoneOffset()/60*60*60*1000)).toISOString().substr(0,19);
                                        adgroups[k].shares= typeof response.shares === 'undefined' ? 0 : response.shares.count;
                                        adgroups[k].viral_imp=response.viral_imp.data[0].values[0].value;
                                        adgroups[k].organic_imp=response.organic_imp.data[0].values[0].value;
                                        adgroups[k].imp=response.impressions.data[0].values[0].value;
                                        adgroups[k].reach=response.fan_reach.data[0].values[0].value;
                                        adgroups[k].unique_imp=response.reach.data[0].values[0].value;
                                        adgroups[k].unpaid_imp=adgroups[k].imp-response.impressions_paid.data[0].values[0].value;
                                        if(adgroups[k].type == 'link' || adgroups[k].type == 'photo'){
                                            adgroups[k].link_clicks=consumptions.data[0].values[0].value['link clicks'];
                                        }
                                        if(adgroups[k].type == 'video'){
                                            adgroups[k].link_clicks=response.video_views.data[0].values[0].value;
                                        }
                                        adgroups[k].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                                        adgroups[k].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                                        adgroups[k].fans = typeof pageFans[adgroups[k].post_id.substr(0,indexOf('_'))] === 'undefined' ? 0 : pageFans[adgroups[k].post_id.substr(0,indexOf('_'))];
                                        adgroups[k].viral_amp=(adgroups[k].viral_imp/adgroups[k].organic_imp).toFixed(2);
                                        adgroups[k].organic_ctr= (adgroups[k].link_clicks-adgroups[k].paid_link_clicks) < 0 ? 0 : ((adgroups[k].link_clicks-adgroups[k].paid_link_clicks)/adgroups[k].unpaid_imp*100).toFixed(2);
                                        adgroups[k].paid_ctr=(adgroups[k].paid_link_clicks/adgroups[k].paid_imp*100).toFixed(2);
                                        adgroups[k].penetration=(adgroups[k].reach/adgroups[k].fans*100).toFixed(2);
                                        adgroups[k].engagement=adgroups[k].likes+adgroups[k].comments+adgroups[k].shares+adgroups[k].link_clicks;
                                        adgroups[k].cpm=(adgroups[k].spend/adgroups[k].paid_imp*1000).toFixed(2);
                                        if(adgroups[k].name == ''){
                                            adgroups[k].name = adgroups[k].message;
                                        }
                                    }
                                });//ajax query for post name
                            }
                        });//ajax query for adgroup insights
                    }
                    if(adgroups[k].conversion_specs[0]["action.type"][0]=="video_view"){
                        query.edge = adgroups[k].id+"/insights";//query active adgroups insights
                        query.fields = "spend,actions,cost_per_action_type";//find active delivering campaigns
                        $.ajax({
                            url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                            type: 'get',
                            dataType: 'json',
                            async: true,
                            success: function(response){
                                if(response.data[0]!=null){
                                    adgroups[k].spend=response.data[0].spend;
                                    adgroups[k].paid_link_clicks=0;
                                    for(var o=0;o<response.data[0].actions.length;o++){
                                        if(response.data[0].actions[o].action_type=='video_view'){
                                            adgroups[k].paid_link_clicks=parseInt(response.data[0].actions[o].value);
                                            break;
                                        }
                                    }
                                    adgroups[k].cxw=0;
                                    for(var o=0;o<response.data[0].cost_per_action_type.length;o++){
                                        if(response.data[0].cost_per_action_type[o].action_type=='video_view'){
                                            adgroups[k].cxw=response.data[0].cost_per_action_type[o].value;
                                            break;
                                        }
                                    }  		    
                                }
                                adgroups[k].ad_id=adgroups[k].id;
                                delete adgroups[k].id;
                                query.edge=adgroups[k].post_id;
                                query.fields="name"+
                                    ",type"+
                                    ",message"+
                                    ",created_time"+
                                    ",shares"+
                                    ",likes.limit(0).summary(1).as(likes)"+
                                    ",comments.limit(0).summary(1).as(comments)"+
                                    ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                                    ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                                    ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                                    ",insights.metric(post_impressions).fields(values).as(impressions)"+
                                    ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                                    ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                                    ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                                    ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
                                $.ajax({
                                    url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                                    type: 'get',
                                    dataType: 'json',
                                    async: true,
                                    success: function(response) {
                                        adgroups[k].name=response.name;
                                        adgroups[k].message=response.message;
                                        adgroups[k].type=response.type;
                                        adgroups[k].created_time = new Date(new Date(response.created_time.replace('+0000','')).getTime()-(new Date().getTimezoneOffset()/60*60*60*1000)).toISOString().substr(0,19);
                                        adgroups[k].shares= typeof response.shares === 'undefined' ? 0 : response.shares.count;
                                        adgroups[k].viral_imp=response.viral_imp.data[0].values[0].value;
                                        adgroups[k].organic_imp=response.organic_imp.data[0].values[0].value;
                                        adgroups[k].imp=response.impressions.data[0].values[0].value;
                                        adgroups[k].reach=response.fan_reach.data[0].values[0].value;
                                        adgroups[k].unique_imp=response.reach.data[0].values[0].value;
                                        adgroups[k].unpaid_imp=adgroups[k].imp-response.impressions_paid.data[0].values[0].value;
                                        if(adgroups[k].type == 'link' || adgroups[k].type == 'photo'){
                                            adgroups[k].link_clicks=consumptions.data[0].values[0].value['link clicks'];
                                        }
                                        if(adgroups[k].type == 'video'){
                                            adgroups[k].link_clicks=response.video_views.data[0].values[0].value;
                                        }
                                        adgroups[k].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                                        adgroups[k].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                                        adgroups[k].fans = typeof pageFans[adgroups[k].post_id.substr(0,indexOf('_'))] === 'undefined' ? 0 : pageFans[adgroups[k].post_id.substr(0,indexOf('_'))];
                                        adgroups[k].viral_amp=(adgroups[k].viral_imp/adgroups[k].organic_imp).toFixed(2);
                                        adgroups[k].organic_ctr= (adgroups[k].link_clicks-adgroups[k].paid_link_clicks) <0 ? 0 : ((adgroups[k].link_clicks-adgroups[k].paid_link_clicks)/adgroups[k].unpaid_imp*100).toFixed(2);
                                        adgroups[k].paid_ctr=(adgroups[k].paid_link_clicks/adgroups[k].paid_imp*100).toFixed(2);
                                        adgroups[k].penetration=(adgroups[k].reach/adgroups[k].fans*100).toFixed(2);
                                        adgroups[k].engagement=adgroups[k].likes+adgroups[k].comments+adgroups[k].shares+adgroups[k].link_clicks;
                                        adgroups[k].cpm=(adgroups[k].spend/adgroups[k].paid_imp*1000).toFixed(2);
                                        if(adgroups[k].name == ''){
                                            adgroups[k].name = adgroups[k].message;
                                        }
                                    }
                                });//ajax query for post name							
                            }
                        });//ajax query for adgroup insights
                    }
                }
                else{
                    adgroups[k].spend=0;
                    adgroups[k].paid_link_clicks=0;
                    adgroups[k].paid_imp=0;
                    adgroups[k].cxw=0;
                    query.edge = adgroups[k].id+"/insights";//query active adgroups insights
                    query.fields = "spend,actions,cost_per_action_type,impressions&date_preset=lifetime";//find active delivering campaigns
                    adgroups[k].ad_id=adgroups[k].id;
                    delete adgroups[k].id;
                    query.edge=adgroups[k].post_id;
                    query.fields="name"+
                        ",type"+
                        ",message"+
                        ",created_time"+
                        ",shares"+
                        ",likes.limit(0).summary(1).as(likes)"+
                        ",comments.limit(0).summary(1).as(comments)"+
                        ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                        ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                        ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                        ",insights.metric(post_impressions).fields(values).as(impressions)"+
                        ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                        ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                        ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                        ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
                    $.ajax({
                        url: buildURL(query,pageTokens[adgroups[k].creative.effective_object_story_id.substr(0,adgroups[k].creative.effective_object_story_id.indexOf('_'))],1),
                        type: 'get',
                        dataType: 'json',
                        async: true,
                        success: function(response) {
                            adgroups[k].name=response.name;
                            adgroups[k].message=response.message;
                            adgroups[k].type=response.type;
                            adgroups[k].created_time = new Date(new Date(response.created_time.replace('+0000','')).getTime()-(new Date().getTimezoneOffset()/60*60*60*1000)).toISOString().substr(0,19);
                            adgroups[k].shares= typeof response.shares === 'undefined' ? 0 : response.shares.count;
                            adgroups[k].viral_imp=response.viral_imp.data[0].values[0].value;
                            adgroups[k].organic_imp=response.organic_imp.data[0].values[0].value;
                            adgroups[k].imp=response.impressions.data[0].values[0].value;
                            adgroups[k].reach=response.fan_reach.data[0].values[0].value;
                            adgroups[k].unique_imp=response.reach.data[0].values[0].value;
                            adgroups[k].unpaid_imp=adgroups[k].imp-response.impressions_paid.data[0].values[0].value;
                            if(adgroups[k].type == 'link' || adgroups[k].type == 'photo'){
                                adgroups[k].link_clicks=response.consumptions.data[0].values[0].value['link clicks'];
                            }
                            if(adgroups[k].type == 'video'){
                                adgroups[k].link_clicks=response.video_views.data[0].values[0].value;
                            }
                            adgroups[k].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                            adgroups[k].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                            adgroups[k].fans = typeof pageFans[adgroups[k].post_id.substr(0,indexOf('_'))] === 'undefined' ? 0 : pageFans[adgroups[k].post_id.substr(0,indexOf('_'))];
                            adgroups[k].viral_amp=(adgroups[k].viral_imp/adgroups[k].organic_imp).toFixed(2);
                            adgroups[k].organic_ctr= (adgroups[k].link_clicks-adgroups[k].paid_link_clicks) < 0 ? 0 : ((adgroups[k].link_clicks-adgroups[k].paid_link_clicks)/adgroups[k].unpaid_imp*100).toFixed(2);
                            adgroups[k].paid_ctr=0;
                            adgroups[k].penetration=(adgroups[k].reach/adgroups[k].fans*100).toFixed(2);
                            adgroups[k].engagement=adgroups[k].likes+adgroups[k].comments+adgroups[k].shares+adgroups[k].link_clicks;
                            adgroups[k].cpm=(adgroups[k].cxw*1000).toFixed(2);
                            if(adgroups[k].name == ''){
                                adgroups[k].name = adgroups[k].message;
                            }
                        }
                    });//ajax query for post name
                }
            }
        }
        for(k=0;k<adgroups.length;k++){
            deforma_adgroups_promises.push(
                getDeformaAdgroupsMetrics(k)
            );
        }
        $.when.apply($, deforma_adgroups_promises).then(function() {
            console.log('Ads promises succeded');
        },function() {
            console.log('Ads promises failed');
        });

        //Get Deforma posts
        query.edge = "eldeforma/posts";//query latest posts
        query.fields = "id,name,message,created_time,link,type,shares&since=3daysAgo";
        $.ajax({
            url: buildURL(query,pageTokens['El Deforma'],100),
            type: 'get',
            dataType: 'json',
            async: false,
            success: function(response){
                posts=response.data;
            }
        });//ajax query to find latest posts
        now=new Date();
        var post_date;
        for(j=0;j<posts.length;j++){
            if(posts[j].type=="status"){
                posts.splice(j,1);
                j--;
            }
        }//for loop to remove all status posts
        for(j=0;j<posts.length;j++){
          posts[j].post_id=posts[j].id;
          delete posts[j].id;
          //console.log(posts[j]);
          if(posts[j].link.indexOf('?utm') != -1){
              posts[j].link = posts[j].link.substring(0,posts[j].link.indexOf('?utm')-1);
          }
          for(k=0;k<adgroups.length;k++){
            if(posts[j].post_id.localeCompare(adgroups[k].post_id)==0){
              posts[j].promoted=true;
            }
          }
        }//for loop to flag promotted posts

        //deforma posts promises
        var deforma_posts_promises = [];		
        function getDeformaPostsMetrics(j){
            if(posts[j].promoted==true){

            }
            else{
                posts[j].created_time = new Date(new Date(posts[j].created_time.replace('+0000','')).getTime()-(new Date().getTimezoneOffset()/60*60*60*1000)).toISOString().substr(0,19);
                post_date = new Date(posts[j].created_time.replace('+0000',''));
                posts[j].shares = typeof posts[j].shares === 'undefined' ? 0 : posts[j].shares.count;
                if(now-post_date<1000*60*60*24*3){
                    //console.log(posts[j].name);
                    query.edge=posts[j].post_id;
                    query.fields="likes.limit(0).summary(1).as(likes)"+
                        ",comments.limit(0).summary(1).as(comments)"+
                        ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                        ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                        ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                        ",insights.metric(post_impressions).fields(values).as(impressions)"+
                        ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                        ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                        ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                        ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
                    $.ajax({
                        url: buildURL(query,pageTokens['El Deforma'],1),
                        type: 'get',
                        dataType: 'json',
                        async: true,
                        success: function(response) {
                            posts[j].viral_imp=response.viral_imp.data[0].values[0].value;
                            posts[j].organic_imp=response.organic_imp.data[0].values[0].value;
                            posts[j].imp=response.impressions.data[0].values[0].value;
                            posts[j].reach=response.fan_reach.data[0].values[0].value;
                            posts[j].unique_imp=response.reach.data[0].values[0].value;
                            posts[j].paid_imp=response.impressions_paid.data[0].values[0].value;
                            posts[j].unpaid_imp=posts[j].imp-posts[j].paid_imp;
                            if(posts[j].type == 'link' || posts[j].type == 'photo'){
                                posts[j].link_clicks=response.consumptions.data[0].values[0].value['link clicks'];
                            }
                            if(posts[j].type == 'video'){
                                posts[j].link_clicks=response.video_views.data[0].values[0].value;
                            }
                            posts[j].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                            posts[j].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                            posts[j].fans = typeof pageFans[posts[j].post_id.substr(0,posts[j].post_id.indexOf('_'))] === 'undefined' ? 0 : pageFans[posts[j].post_id.substr(0,posts[j].post_id.indexOf('_'))];
                            posts[j].viral_amp=(posts[j].viral_imp/posts[j].organic_imp).toFixed(2);
                            posts[j].organic_ctr=(posts[j].link_clicks/posts[j].imp*100).toFixed(2);
                            posts[j].ad_id="";
                            posts[j].spend=0;
                            posts[j].cxw=0;
                            posts[j].paid_link_clicks=0;
                            posts[j].paid_ctr=0;
                            posts[j].penetration=(posts[j].reach/posts[j].fans*100).toFixed(2);
                            posts[j].engagement=posts[j].likes+posts[j].comments+posts[j].shares+posts[j].link_clicks;
                            posts[j].cpm=0;
                            posts[j].num=j;
                            if(posts[j].name == ''){
                                posts[j].name = posts[j].message;
                            }
                        }
                    });//ajax query to find all metrics with a switch
                }
            }
        }
        for(j=0;j<posts.length;j++){	
            deforma_posts_promises.push(
                getDeformaPostsMetrics(j)
            );
        }//for loop to set all promises
        $.when.apply($, deforma_posts_promises).then(
            setTimeout(function() {
                console.log('Deforma posts promises succeded');
                setTimeout(function(){
                    adgroups_comercial = adgroups.filter(ad => ad.account_id == 'act_31492866');
                    adgroups=adgroups.filter(ad => ad.account_id != 'act_31492866');
                    if(adgroups!=null){
                        for(var j=0;j<posts.length;j++){
                            for(var k=0;k<adgroups.length;k++){
                                if(posts[j].post_id.localeCompare(adgroups[k].post_id)==0){
                                    posts[j].promoted=true;
                                }
                            }
                        }
                        drawTableKomfo();
                    }else{
                        drawTableKomfo();
                    }
                    if(adgroups_comercial!=null){
                        for(var j=0;j<posts.length;j++){
                            for(var k=0;k<adgroups_comercial.length;k++){
                                if(posts[j].post_id.localeCompare(adgroups_comercial[k].post_id)==0){
                                    posts[j].promoted=true;
                                }
                            }
                        }
                    }
                },1000);
            },5000),
            function() {
                console.log('Deforma posts promises failed');
            }
        );
		
		//El Deforma
	});
    
}
function drawTableKomfo(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	data_table.addColumn({type: 'number', label: 'Orden'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'string', label: 'Posted at'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    //data_table.addColumn({type: 'number', label: 'Yest $clicks/PV'});
	data_table.addColumn({type: 'number', label: 'Penetration'});
	data_table.addColumn({type: 'number', label: 'Viral Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Organico'});
	data_table.addColumn({type: 'number', label: 'Likes/Shares'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pagado'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
    
	if(adgroups!=null){
        /*for(var r=0;r<adgroups.length-1;r++){
            for(var s=0;s<adgroups[adgroups.length-1].click_percent_array.length;s++){
                if(adgroups[r].id == adgroups[adgroups.length-1].click_percent_array[s].id){
                    adgroups[r].click_percent = adgroups[adgroups.length-1].click_percent_array[s].pageviews === 0 ? 0 : adgroups[r].yesterdays_paid_clicks/adgroups[adgroups.length-1].click_percent_array[s].pageviews;
                }
            }
        }*/
        
        //adgroups.pop();
        
		for(var n=0;n<adgroups.length;n++){
            //console.log(adgroups[n]);
            if(adgroups[n].account_id !== 'act_31492866'){
                if(adgroups[n].currency=="MXN"){
                    currency_factor=1/19.5;
                }
                else{
                    currency_factor=1;
                }
                if(campaignIsCurrentlyActive(adgroups[n].campaign_id).status && campaignIsCurrentlyActive(adgroups[n].campaign_id).effective_status == 'active'){
                     data_table.addRow([
                         n+1,
                         (titleShortener(typeof adgroups[n].name === 'undefined' ? '' : adgroups[n].name).link("https://facebook.com/"+adgroups[n].post_id)),
                         (new Date(adgroups[n].created_time.replace('+0000',''))).toLocaleString(),
                         {v: getDayCXW(adgroups[n].campaign_id).cxw*currency_factor, f: "$"+(getDayCXW(adgroups[n].campaign_id).cxw*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups[n].campaign_id).spend*currency_factor, f: "$"+(getDayCXW(adgroups[n].campaign_id).spend*currency_factor).toFixed(2)},
                         //{v: adgroups[n].click_percent, f: "%"+(100*adgroups[n].click_percent).toFixed(2)},
                         {v: parseFloat(adgroups[n].penetration), f: parseFloat(adgroups[n].penetration)+"%"},
                         {v: parseFloat(adgroups[n].viral_amp), f: parseFloat(adgroups[n].viral_amp)+"x"},
                         {v: isNaN(parseFloat(adgroups[n].organic_ctr)) ? 0 : parseFloat(adgroups[n].organic_ctr), f: isNaN(parseFloat(adgroups[n].organic_ctr)) ? 0 : parseFloat(adgroups[n].organic_ctr)+"%"},
                         {v: parseInt(adgroups[n].shares)+parseInt(adgroups[n].likes), f: numberWithCommas(adgroups[n].likes)+' / '+numberWithCommas(adgroups[n].shares)},
                         {v: parseInt(adgroups[n].link_clicks), f: numberWithCommas(adgroups[n].link_clicks)},
                         {v: isNaN(parseFloat(adgroups[n].paid_ctr)) ? 0 : parseFloat(adgroups[n].paid_ctr), f: isNaN(parseFloat(adgroups[n].paid_ctr)) ? '0.0 %' : parseFloat(adgroups[n].paid_ctr)+"%"},
                         {v: parseFloat(adgroups[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups[n].cxw)*currency_factor).toFixed(5)},
                         {v: parseFloat(adgroups[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups[n].cpm)*currency_factor).toFixed(2)},
                         {v: parseFloat(adgroups[n].spend)*currency_factor, f: '$'+numberWithCommas((parseFloat(adgroups[n].spend)*currency_factor).toFixed(2))},
                         '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups[n].post_id+'\',\''+adgroups[n].campaign_id+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups[n].campaign_id+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups[n].post_id+'\')">New Promote</button>'
                   ]);
                }else{
                    if(campaignIsCurrentlyActive(adgroups[n].campaign_id).status && campaignIsCurrentlyActive(adgroups[n].campaign_id).effective_status == 'pending'){
                        data_table.addRow([
                            n+1,
                            (titleShortener(typeof adgroups[n].name === 'undefined' ? '' : adgroups[n].name).link("https://facebook.com/"+adgroups[n].post_id)),
                            (new Date(adgroups[n].created_time.replace('+0000',''))).toLocaleString(),
                            null,
                            null,
                            //null,
                            {v: parseFloat(adgroups[n].penetration), f: parseFloat(adgroups[n].penetration)+"%"},
                            {v: parseFloat(adgroups[n].viral_amp), f: parseFloat(adgroups[n].viral_amp)+"x"},
                            {v: isNaN(parseFloat(adgroups[n].organic_ctr)) ? 0 : parseFloat(adgroups[n].organic_ctr), f: isNaN(parseFloat(adgroups[n].organic_ctr)) ? 0 : parseFloat(adgroups[n].organic_ctr)+"%"},
                            {v: parseInt(adgroups[n].shares)+parseInt(adgroups[n].likes), f: numberWithCommas(adgroups[n].likes)+' / '+numberWithCommas(adgroups[n].shares)},
                            {v: parseInt(adgroups[n].link_clicks), f: numberWithCommas(adgroups[n].link_clicks)},
                            {v: isNaN(parseFloat(adgroups[n].paid_ctr)) ? 0 : parseFloat(adgroups[n].paid_ctr), f: isNaN(parseFloat(adgroups[n].paid_ctr)) ? '0.0 %' : parseFloat(adgroups[n].paid_ctr)+"%"},
                            {v: parseFloat(adgroups[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups[n].cxw)*currency_factor).toFixed(5)},
                            {v: parseFloat(adgroups[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups[n].cpm)*currency_factor).toFixed(2)},
                            {v: parseFloat(adgroups[n].spend)*currency_factor, f: '$'+numberWithCommas((parseFloat(adgroups[n].spend)*currency_factor).toFixed(2))},
                            '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups[n].post_id+'\',\''+adgroups[n].campaign_id+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups[n].campaign_id+'\')">Edit Promote</button>'
                       ]);  
                    }else{
                        adgroups.splice(n,1);
                        n--;  
                    }
                }
            }
		}
	}

	for(var n=0;n<posts.length;n++){
		if(posts[n].promoted==true){

		}
		else
		{		
			data_table.addRow([
				n+(adgroups!=null ?  adgroups.length : 0)+1,
				(titleShortener(typeof posts[n].name === 'undefined' ? '' : posts[n].name).link("https://facebook.com/"+posts[n].post_id)),
				(new Date(posts[n].created_time.replace('+0000',''))).toLocaleString(),
                null,
                null,
                //null,
				{v: parseFloat(posts[n].penetration), f: parseFloat(posts[n].penetration)+"%"},
				{v: parseFloat(posts[n].viral_amp), f: parseFloat(posts[n].viral_amp)+"x"},
				{v: parseFloat(posts[n].organic_ctr), f: parseFloat(posts[n].organic_ctr)+"%"},
				{v: parseInt(posts[n].shares)+parseInt(posts[n].likes), f: numberWithCommas(posts[n].likes)+' / '+numberWithCommas(posts[n].shares)},
				{v: parseInt(posts[n].link_clicks), f: numberWithCommas(posts[n].link_clicks)},
				{v: parseFloat(posts[n].paid_ctr), f: parseFloat(posts[n].paid_ctr)+"%"},
				{v: parseFloat(posts[n].cxw), f: "$"+parseFloat(posts[n].cxw)},
				{v: parseFloat(posts[n].cpm), f: "$"+parseFloat(posts[n].cpm)},
				null,
				'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts[n].post_id+'\')">Promote</button>'
			]);
		}
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 2
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_deforma'));

	var formatter_penetration = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 50});
	var formatter_viral_amp = new google.visualization.BarFormat({width: 30, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});

	formatter_penetration.format(data_table, 5);
	formatter_viral_amp.format(data_table, 6);
	formatter_ctr.format(data_table, 7);
	formatter_paid_ctr.format(data_table, 10);
	formatter_cxw.format(data_table, 11);
	formatter_cpm.format(data_table, 12);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	if(adgroups!=null){
		for(var n=0;n<adgroups.length;n++){
			links[n].childNodes[0].setAttribute('title',adgroups[n].name);
		}
	}
	
	var l=0;
	for(var n=0;n<posts.length;n++){
		if(posts[n].promoted==true){
			l++;
		}
		else
		{
			links[n+(adgroups!=null ?  adgroups.length : 0)-l].childNodes[0].setAttribute('title',posts[n].name);
		}
	}
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}
function campaignIsCurrentlyActive(campaign){
	var flag;
	$.ajax({
		url: 'https://graph.facebook.com/v'+version+'/'+campaign+'?fields=effective_status,ads.fields(effective_status)&access_token='+access_token,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(response){
            //console.log(response);
            if(response.effective_status != 'ACTIVE'){
               flag={status: false, effective_status: 'not active'};
            }
            else{
                switch(response.ads.data[0].effective_status){
                    case 'ACTIVE':
                        flag={status: true, effective_status: 'active'};
                        break;
                    case 'PENDING_REVIEW':
                        flag={status: true, effective_status: 'pending'};
                        break;
                    default:
                        flag={status: false, effective_status: 'not active'};
                        break;      
                }
            }		
        }
	});
	return flag;	
}
function getDayCXW(campaign){
    var day_cxw;
    $.ajax({
		url: 'https://graph.facebook.com/v'+version+'/'+campaign+'?fields=insights.fields(spend,actions,objective).date_preset(today),name&access_token='+access_token,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(response){
            if(typeof response.insights !== 'undefined'){
                for(var i=0;i<response.insights.data[0].actions.length;i++){
                    if(response.insights.data[0].objective == 'LINK_CLICKS' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'POST_ENGAGEMENT' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'CONVERSIONS' && response.insights.data[0].actions[i].action_type == 'link_click'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'PAGE_LIKES' && response.insights.data[0].actions[i].action_type == 'like'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    if(response.insights.data[0].objective == 'VIDEO_VIEWS' && response.insights.data[0].actions[i].action_type == 'video_view'){
                        day_cxw = parseFloat(response.insights.data[0].spend)/parseFloat(response.insights.data[0].actions[i].value);
                    }
                    day_spend = response.insights.data[0].spend;
                }
            }
            else{
                day_cxw=0;
                day_spend=0;
            }
		}
	});
	return {cxw: day_cxw, spend: day_spend};
}
function getLinkClicksVSPageviewsPromoted(ads,next){
    var promises=[];
    ads[ads.length]={click_percent_array: []};
    for(var j=0;j<ads.length-1;j++){
       promises.push(
            $.ajax({
                url: 'https://graph.facebook.com/v'+version+'/'+ads[j].campaign_id+'?fields=insights.fields(actions).date_preset(yesterday)&access_token='+access_token,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function(response){
                    var click_percent;
                    if(typeof response.insights === 'undefined'){
                        ads[ads.length-1].click_percent_array.push(
                            {
                                pageviews: 0,
                                id: ads[j].id
                            }
                        );
                    }
                    else{
                        for(var i=0;i<response.insights.data[0].actions.length;i++){
                            if(response.insights.data[0].actions[i].action_type == 'link_click'){
                                ads[j].yesterdays_paid_clicks = parseInt(response.insights.data[0].actions[i].value);
                            }
                        }
                        $.ajax({
                            url: 'https://graph.facebook.com/v'+version+'/'+ads[j].post_id+'?fields=link&access_token='+access_token,
                            type: 'GET',
                            dataType: 'json',
                            async: false,
                            success: function(response) {
                                //console.log(response);
                                var link = response.link.substr(32,response.link.length-32-1);
                                var id = response.id;
                                //console.log(link);
                                if(link.length > 0){
                                    gapi.client.load('analytics','v3').then(function(){
                                       var requestPageviews = gapi.client.analytics.data.ga.get({
                                            ids: "ga:41142760",
                                            "start-date": 'yesterday',
                                            "end-date": "yesterday",
                                            metrics: "ga:pageviews",
                                            dimensions: "ga:pagePath",
                                            filters: "ga:pagePath=@p1=true;ga:pagePath=@"+link,
                                            fields: 'totalsForAllResults'
                                        });
                                        requestPageviews.execute(function(resp){
                                            //console.log(resp);
                                            ads[ads.length-1].click_percent_array.push(
                                                {
                                                    pageviews: parseInt(resp.totalsForAllResults['ga:pageviews']),
                                                    id: id
                                                }
                                            );
                                        });
                                    });
                                }else{
                                    ads[ads.length-1].click_percent_array.push(
                                        {
                                            pageviews: 0,
                                            id: id
                                        }
                                    );
                                }
                            }
                       });
                    }
                }
            })
        ); 
    }
    $.when.apply($,promises).done(function(){
        //drawTableKomfo();
        function marryClickPercentage(){
            if(ads[ads.length-1].click_percent_array.length != ads.length-1){
                setTimeout(function(){marryClickPercentage()},2000);
            }
            else{
                next();
            }
        }
        marryClickPercentage();
        //console.log('promise success')
    });
}

function loadKomfo_comercial(){
    if(adgroups_comercial!=null){
        getLinkClicksVSPageviewsPromoted(adgroups_comercial,drawTableKomfoComercial);
	}else{
        drawTableKomfoComercial();      
    }
}
function drawTableKomfoComercial(){
	var data_table = new google.visualization.DataTable();
	var currency_factor;

	data_table.addColumn({type: 'number', label: 'Orden'});
	data_table.addColumn({type: 'string', label: 'Nombre'});
    data_table.addColumn({type: 'string', label: 'Posted at'});
//	data_table.addColumn({type: 'number', label: 'RT CXW'});
    data_table.addColumn({type: 'number', label: 'Day CXW'});
    data_table.addColumn({type: 'number', label: 'Day Spend'});
    data_table.addColumn({type: 'number', label: 'Yest $clicks/PV'});
	data_table.addColumn({type: 'number', label: 'Penetration'});
	data_table.addColumn({type: 'number', label: 'Viral Amp'});
	data_table.addColumn({type: 'number', label: 'CTR Organico'});
	data_table.addColumn({type: 'number', label: 'Likes/Shares'});
	data_table.addColumn({type: 'number', label: 'Clicks'});
	data_table.addColumn({type: 'number', label: 'CTR Pagado'});
	data_table.addColumn({type: 'number', label: 'CXW'});
	data_table.addColumn({type: 'number', label: 'CPM'});
	data_table.addColumn({type: 'number', label: 'Spend'});
	data_table.addColumn({type: 'string', label: 'Promote'});
//	data_table.addColumn({type: 'string', label: 'GCS'});
//	data_table.addColumn({type: 'string', label: 'Quickview'});
//	data_table.addColumn({type: 'string', label: 'Edit'});
    
	if(adgroups_comercial!=null){
        for(var r=0;r<adgroups_comercial.length-1;r++){
            for(var s=0;s<adgroups_comercial[adgroups_comercial.length-1].click_percent_array.length;s++){
                if(adgroups_comercial[r].id == adgroups_comercial[adgroups_comercial.length-1].click_percent_array[s].id){
                    adgroups_comercial[r].click_percent = adgroups_comercial[adgroups_comercial.length-1].click_percent_array[s].pageviews === 0 ? 0 : adgroups_comercial[r].yesterdays_paid_clicks/adgroups_comercial[adgroups_comercial.length-1].click_percent_array[s].pageviews;
                }
            }
        }
        
        adgroups_comercial.pop();
        
		for(var n=0;n<adgroups_comercial.length;n++){
                currency_factor=1;
                if(campaignIsCurrentlyActive(adgroups_comercial[n].campaign_id).status && campaignIsCurrentlyActive(adgroups_comercial[n].campaign_id).effective_status == 'active'){
                     data_table.addRow([
                         n+1,
                         (titleShortener(adgroups_comercial[n].name).link("https://facebook.com/"+adgroups_comercial[n].post_id)),
                         (new Date(adgroups_comercial[n].posted_date)).toLocaleString(),
//                         {v: parseFloat(adgroups_comercial[n].rt_cxw)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].rt_cxw)*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_comercial[n].campaign_id).cxw*currency_factor, f: "$"+(getDayCXW(adgroups_comercial[n].campaign_id).cxw*currency_factor).toFixed(5)},
                         {v: getDayCXW(adgroups_comercial[n].campaign_id).spend*currency_factor, f: "$"+(getDayCXW(adgroups_comercial[n].campaign_id).spend*currency_factor).toFixed(2)},
                         {v: adgroups_comercial[n].click_percent, f: "%"+(100*adgroups_comercial[n].click_percent).toFixed(2)},
                         {v: parseFloat(adgroups_comercial[n].penetration), f: parseFloat(adgroups_comercial[n].penetration)+"%"},
                         {v: parseFloat(adgroups_comercial[n].viral_amp), f: parseFloat(adgroups_comercial[n].viral_amp)+"x"},
                         {v: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? 0 : parseFloat(adgroups_comercial[n].organic_ctr), f: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? '0' : parseFloat(adgroups_comercial[n].organic_ctr)+"%"},
                         {v: parseInt(adgroups_comercial[n].shares)+parseInt(adgroups_comercial[n].likes), f: numberWithCommas(adgroups_comercial[n].likes)+' / '+numberWithCommas(adgroups_comercial[n].shares)},
                         {v: typeof adgroups_comercial[n].link_clicks !== 'undefined' ? parseInt(adgroups_comercial[n].link_clicks) : 0, f: typeof adgroups_comercial[n].link_clicks !== 'undefined' ? numberWithCommas(adgroups_comercial[n].link_clicks) : '0'},
                         {v: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? 0 : parseFloat(adgroups_comercial[n].paid_ctr), f: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? '0' : parseFloat(adgroups_comercial[n].paid_ctr)+"%"},
                         {v: parseFloat(adgroups_comercial[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cxw)*currency_factor).toFixed(5)},
                         {v: parseFloat(adgroups_comercial[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cpm)*currency_factor).toFixed(2)},
                         {v: parseFloat(adgroups_comercial[n].spend)*currency_factor, f: '$'+numberWithCommas((parseFloat(adgroups_comercial[n].spend)*currency_factor).toFixed(2))},
                         '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_comercial[n].post_id+'\',\''+adgroups_comercial[n].campaign_id+'\')">Kill Promote</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_comercial[n].campaign_id+'\')">Edit Promote</button>'+'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+adgroups_comercial[n].post_id+'\')">New Promote</button>',
//                         adgroups_comercial[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_comercial[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_comercial[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_comercial[n].id+'\')">Hybrid GCS</button>',
//                         //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
//                         '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\',\''+adgroups_comercial[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
//                         '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                   ]);
                }else{
                    if(campaignIsCurrentlyActive(adgroups_comercial[n].campaign_id).status && campaignIsCurrentlyActive(adgroups_comercial[n].campaign_id).effective_status == 'pending'){
                        data_table.addRow([
                            n+1,
                            (titleShortener(adgroups_comercial[n].name).link("https://facebook.com/"+adgroups_comercial[n].id)),
                            (new Date(adgroups_comercial[n].posted_date)).toLocaleString(),
//                            null,
                            null,
                            null,
                            null,
                            {v: parseFloat(adgroups_comercial[n].penetration), f: parseFloat(adgroups_comercial[n].penetration)+"%"},
                            {v: parseFloat(adgroups_comercial[n].viral_amp), f: parseFloat(adgroups_comercial[n].viral_amp)+"x"},
                            {v: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? 0 : parseFloat(adgroups_comercial[n].organic_ctr), f: isNaN(parseFloat(adgroups_comercial[n].organic_ctr)) ? '0.00%' : parseFloat(adgroups_comercial[n].organic_ctr)+"%"},
                            {v: parseInt(adgroups_comercial[n].shares)+parseInt(adgroups_comercial[n].likes), f: numberWithCommas(adgroups_comercial[n].likes)+' / '+numberWithCommas(adgroups_comercial[n].shares)},
                            {v: typeof adgroups_comercial[n].link_clicks !== 'undefined' ? parseInt(adgroups_comercial[n].link_clicks) : 0, f: typeof adgroups_comercial[n].link_clicks !== 'undefined' ? numberWithCommas(adgroups_comercial[n].link_clicks) : '0'},
                            {v: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? 0 : parseFloat(adgroups_comercial[n].paid_ctr), f: isNaN(parseFloat(adgroups_comercial[n].paid_ctr)) ? '0%' : parseFloat(adgroups_comercial[n].paid_ctr)+"%"},
                            {v: parseFloat(adgroups_comercial[n].cxw)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cxw)*currency_factor).toFixed(5)},
                            {v: parseFloat(adgroups_comercial[n].cpm)*currency_factor, f: "$"+(parseFloat(adgroups_comercial[n].cpm)*currency_factor).toFixed(2)},
                            {v: parseFloat(adgroups_comercial[n].spend)*currency_factor, f: '$'+numberWithCommas((parseFloat(adgroups_comercial[n].spend)*currency_factor).toFixed(2))},
                            '<button class="btn btn-danger waves-effect waves-light" onclick="stopCampaign(\''+adgroups_comercial[n].post_id+'\',\''+adgroups_comercial[n].campaign_id+'\')">Kill Promote (Pending Review)</button>'+'<button class="btn btn-pink waves-effect waves-light" data-toggle="modal" data-target="#edit-promote-modal" onclick="getCurrentCampaignParameters(\''+adgroups_comercial[n].campaign_id+'\')">Edit Promote</button>',
//                            adgroups_comercial[n].GCS === '1' ? '<button class="btn btn-danger waves-effect waves-light" onclick="removeGCS(\''+adgroups_comercial[n].id+'\')">Remove GCS</button>' : '<button class="btn btn-success waves-effect waves-light" onclick="activateGCS(\''+adgroups_comercial[n].id+'\')">GCS</button><button class="btn btn-warning waves-effect waves-light" onclick="activateHybridGCS(\''+adgroups_comercial[n].id+'\')">Hybrid GCS</button>',
//                            //'<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
//                            '<button class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#quickview-modal" onclick="selectPostQuickview(\''+adgroups_comercial[n].id+'\',\''+adgroups_comercial[n].name.replace(/\"/g,'')+'\',\''+adgroups_comercial[n].campaign+'\')"><i class="zmdi zmdi-eye" style="color: #FFFFFF; font-size: 30px;"></i></button>',
//                            '<button class="btn btn-inverse waves-effect waves-light" onclick="wpPostEdit(\''+adgroups_comercial[n].id+'\')"><i class="zmdi zmdi-edit" style="color: #FFFFFF; font-size: 30px;"></i></button>'
                       ]);  
                    }
                }
		}
	}

	// Set chart options
	var options = {
		'allowHtml': true,
		'showRowNumber': true,
		'width':'100%',
		'height':'100%',
		'frozenColumns': 2
	};

	// Instantiate and draw our chart, passing in some options.
	var table = new google.visualization.Table(document.getElementById('komfo_table_comercial'));

	var formatter_penetration = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 50});
	var formatter_viral_amp = new google.visualization.BarFormat({width: 30, colorPositive: 'blue', max: 1.5});
	var formatter_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_paid_ctr = new google.visualization.BarFormat({width: 30, colorPositive: 'green', max: 5});
	var formatter_cxw = new google.visualization.BarFormat({width: 30, colorPositive: 'red', max: 0.01, colorNegative: 'blue', base: 0.002});
	var formatter_cpm = new google.visualization.BarFormat({width: 30, colorPositive: 'green'});

	formatter_penetration.format(data_table, 6);
	formatter_viral_amp.format(data_table, 7);
	formatter_ctr.format(data_table, 8);
	formatter_paid_ctr.format(data_table, 11);
	formatter_cxw.format(data_table, 12);
	formatter_cpm.format(data_table, 13);

	table.draw(data_table, options);

	links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  
	if(adgroups_comercial!=null){
		for(var n=0;n<adgroups_comercial.length;n++){
			links[n].childNodes[0].setAttribute('title',adgroups_comercial[n].name);
		}
	}
			
	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}

	google.visualization.events.addListener(table, 'sort', function() {
		for(var o=0;o < links.length;o++){
			links[o].childNodes[0].setAttribute('target','_blank');
		}
	});
}

function setGoalPostId(id){
	$('#goal_post_id')[0].innerHTML = id;
}
function changeGoal(){
	var id = $('#goal_post_id')[0].innerHTML;
	var goal = $('#goal_post_goal')[0].value;
	$.ajax({
		url: 'php_scripts/updateGoal.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'goal': goal,
			'type': $('[name=radioGoal]:checked').val()
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){loadKomfo();},1000);
}
function removeGoal(){
	var id = $('#goal_post_id')[0].innerHTML;
	var goal = $('#goal_post_goal')[0].value;
	$.ajax({
		url: 'php_scripts/removeGoal.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'goal': goal,
			'type': $('[name=radioGoal]:checked').val()
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){loadKomfo();},1000);
}
function setNamePostId(id){
	$('#name_post_id')[0].innerHTML = id;
}
function changeName(){
	var id = $('#name_post_id')[0].innerHTML;
	var name = $('#post_name')[0].value;
	$.ajax({
		url: 'php_scripts/updateName.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			'id': id,
			'name': name
		},
		success: function(response){
			console.log(response);
		}
	});
	setTimeout(function(){loadKomfo();},1000);
}

function loadKomfo_repsodia(){
	posts_repsodia=[];
    query.edge = "repsodia/posts";//query latest posts
    query.fields = "id,message,name,created_time,type,shares&since=3daysAgo";
    $.ajax({
        url: buildURL(query,pageTokens['Repsodia'],100),
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(response){
            videos=response.data;
            //console.log(videos);
        }
    });//ajax query to find videos of the past 20 days
    now=new Date();
    var video_date;
    for(j=0;j<videos.length;j++){
        if(videos[j].type!="video"){
            posts_repsodia.push(videos[j]);
            videos.splice(j,1);
            j--;
        }
    }
    for(j=0;j<videos.length;j++){
      videos[j].post_id=videos[j].id;
      delete videos[j].id;
      //console.log(posts[j]);
      for(k=0;k<adgroups.length;k++){
        if(videos[j].post_id.localeCompare(adgroups[k].post_id)==0){
          videos[j].promoted=true;
        }
      }
    }//for loop to flag promotted posts

    //Repsodia video promises
    var repsodia_video_promises = [];
    function getRepsodiaVideoMetrics(j){
        if(videos[j].promoted==true){

        }
        else{
            videos[j].created_time = new Date(new Date(videos[j].created_time).getTime()-(new Date().getTimezoneOffset()/60)*60*60*1000).toISOString().substr(0,19).replace('T',' ');
            post_date = new Date(videos[j].created_time);
            videos[j].shares = typeof videos[j].shares === 'undefined' ? 0 : videos[j].shares.count;
            //console.log(posts[j].name);
            query.edge=videos[j].post_id;
            query.fields="likes.limit(0).summary(1).as(likes)"+
                    ",comments.limit(0).summary(1).as(comments)"+
                    ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                    ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                    ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                    ",insights.metric(post_impressions).fields(values).as(impressions)"+
                    ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                    ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                    ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                    ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)"+
                    ",insights.metric(post_video_views_clicked_to_play).period(lifetime).fields(values).as(video_views_click_to_play)"+
                    ",insights.metric(post_video_views_10s_sound_on).period(lifetime).fields(values).as(video_views_sound)"+
                    ",insights.metric(post_video_retention_graph).period(lifetime).fields(values).as(graph)"+
                    ",insights.metric(post_reactions_by_type_total).period(lifetime).fields(values).as(reactions_total)";
            $.ajax({
                url: buildURL(query,pageTokens['Repsodia'],1),
                type: 'get',
                dataType: 'json',
                async: true,
                success: function(response) {	
                        //console.log('in metrics');				  
                        videos[j].viral_imp=response.viral_imp.data[0].values[0].value;

                        videos[j].organic_imp=response.organic_imp.data[0].values[0].value;

                        videos[j].imp=response.impressions.data[0].values[0].value;

                        videos[j].reach=response.fan_reach.data[0].values[0].value;

                        videos[j].unique_imp=response.reach.data[0].values[0].value;

                        videos[j].paid_imp=response.impressions_paid.data[0].values[0].value;
                        videos[j].unpaid_imp=videos[j].imp-videos[j].paid_imp;

                        videos[j].link_clicks=response.video_views.data[0].values[0].value;

                        videos[j].clicked_to_play=response.video_views_click_to_play.data[0].values[0].value;
                        videos[j]['10s_sound']=response.video_views_sound.data[0].values[0].value;

                        videos[j].retention_20=response.graph.data[0].values[0].value['8'];
                        videos[j].retention_40=response.graph.data[0].values[0].value['16'];
                        videos[j].retention_60=response.graph.data[0].values[0].value['24'];
                        videos[j].retention_80=response.graph.data[0].values[0].value['32'];
                        videos[j].retention_100=response.graph.data[0].values[0].value['40'];

                        videos[j].reaction_like=response.reactions_total.data[0].values[0].value.like;
                        videos[j].reaction_love=response.reactions_total.data[0].values[0].value.love;
                        videos[j].reaction_wow=response.reactions_total.data[0].values[0].value.wow;
                        videos[j].reaction_haha=response.reactions_total.data[0].values[0].value.haha;
                        videos[j].reaction_sorry=response.reactions_total.data[0].values[0].value.sorry;
                        videos[j].reaction_anger=response.reactions_total.data[0].values[0].value.anger;
                        videos[j].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                        videos[j].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                        videos[j].fans = typeof pageFans[videos[j].post_id.substr(0,videos[j].post_id.indexOf('_'))] === 'undefined' ? 0 : pageFans[videos[j].post_id.substr(0,videos[j].post_id.indexOf('_'))];
                        videos[j].viral_amp=(videos[j].viral_imp/videos[j].organic_imp).toFixed(2);
                        videos[j].organic_ctr=(videos[j].link_clicks/videos[j].imp*100).toFixed(2);
                        videos[j].ad_id="";
                        videos[j].spend=0;
                        videos[j].cxw=0;
                        videos[j].paid_link_clicks=0;
                        videos[j].paid_ctr=0;
                        videos[j].penetration=(videos[j].reach/videos[j].fans*100).toFixed(2);
                        videos[j].engagement=videos[j].likes+videos[j].comments+videos[j].shares+videos[j].link_clicks;
                        videos[j].cpm=0;
                        videos[j].num=j;
                    }
            });//ajax query to find all metrics with a switch			
        }
    }
    for(j=0;j<videos.length;j++){
        repsodia_video_promises.push(
            getRepsodiaVideoMetrics(j)
        );
    }//for loop to record all videos  ///////Promisified
    $.when.apply($, repsodia_video_promises).then(function() {
        console.log('Repsodia Video promises succeded');
    },function() {
        console.log('Repsodia video promises failed');
    });

    for(j=0;j<posts_repsodia.length;j++){
      posts_repsodia[j].post_id=posts_repsodia[j].id;
      //delete posts_repsodia[j].id;
      //console.log(posts[j]);
      for(k=0;k<adgroups.length;k++){
        if(posts_repsodia[j].post_id.localeCompare(adgroups[k].post_id)==0){
          posts_repsodia[j].promoted=true;
        }
      }
    }//for loop to flag promotted posts

    //Repsodia posts promises				
    var repsodia_posts_promises = [];
    function getRepsodiaPostsMetrics(j){
        if(posts_repsodia[j].promoted==true){

        }else{
            posts_repsodia[j].created_time = new Date(new Date(posts_repsodia[j].created_time).getTime()-(new Date().getTimezoneOffset()/60)*60*60*1000).toISOString().substr(0,19).replace('T',' ');
            post_date = new Date(posts_repsodia[j].created_time);
            posts_repsodia[j].shares= typeof posts_repsodia[j].shares === 'undefined' ? 0 : posts_repsodia[j].shares.count;
            //console.log(posts[j].name);

            query.edge=posts_repsodia[j].post_id;
            query.fields="likes.limit(0).summary(1).as(likes)"+
                ",comments.limit(0).summary(1).as(comments)"+
                ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                ",insights.metric(post_impressions).fields(values).as(impressions)"+
                ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
            $.ajax({
                url: buildURL(query,pageTokens['Repsodia'],1),
                type: 'get',
                dataType: 'json',
                async: true,
                success: function(response) {
                    posts_repsodia[j].viral_imp=response.viral_imp.data[0].values[0].value;
                    posts_repsodia[j].organic_imp=response.organic_imp.data[0].values[0].value;
                    posts_repsodia[j].imp=response.impressions.data[0].values[0].value;
                    posts_repsodia[j].reach=response.fan_reach.data[0].values[0].value;
                    posts_repsodia[j].unique_imp=response.reach.data[0].values[0].value;
                    posts_repsodia[j].paid_imp=response.impressions_paid.data[0].values[0].value;
                    posts_repsodia[j].unpaid_imp=posts_repsodia[j].imp-posts_repsodia[j].paid_imp;
                    if(posts_repsodia[j].type == 'link' || posts_repsodia[j].type == 'photo'){
                        posts_repsodia[j].link_clicks=response.consumptions.data[0].values[0].value['link clicks'];
                    }
                    if(posts_repsodia[j].type == 'video'){
                        posts_repsodia[j].link_clicks=response.video_views.data[0].values[0].value;
                    }
                    posts_repsodia[j].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                    posts_repsodia[j].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                    posts_repsodia[j].fans = typeof pageFans[posts_repsodia[j].post_id.substr(0,posts_repsodia[j].post_id.indexOf('_'))] === 'undefined' ? 0 : pageFans[posts_repsodia[j].post_id.substr(0,posts_repsodia[j].post_id.indexOf('_'))];
                    posts_repsodia[j].viral_amp=(posts_repsodia[j].viral_imp/posts_repsodia[j].organic_imp).toFixed(2);
                    posts_repsodia[j].organic_ctr=(posts_repsodia[j].link_clicks/posts_repsodia[j].imp*100).toFixed(2);
                    posts_repsodia[j].ad_id="";
                    posts_repsodia[j].spend=0;
                    posts_repsodia[j].cxw=0;
                    posts_repsodia[j].paid_link_clicks=0;
                    posts_repsodia[j].paid_ctr=0;
                    posts_repsodia[j].penetration=(posts_repsodia[j].reach/posts_repsodia[j].fans*100).toFixed(2);
                    posts_repsodia[j].engagement=posts_repsodia[j].likes+posts_repsodia[j].comments+posts_repsodia[j].shares+posts_repsodia[j].link_clicks;
                    posts_repsodia[j].cpm=0;
                    posts_repsodia[j].num=j;
                }
            });//ajax query to find all metrics with a switch
        }
    }//for loop to record all repsodia posts
    for(j=0;j<posts_repsodia.length;j++){
        repsodia_posts_promises.push(
            getRepsodiaPostsMetrics(j)
        );
    }//for loop to record all posts  ///////Promisified
    $.when.apply($, repsodia_posts_promises).then(function() {
        console.log('Repsodia posts promises succeded');
        setTimeout(function(){drawTableKomfo_repsodia()},1000);
    },function() {
        console.log('Repsodia posts promises failed');
    });
}
function drawTableKomfo_repsodia(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});
  data_table.addColumn({type: 'string', label: 'Promote'});

    
  for(var n=0;n<videos.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(videos[n].name)).link("https://facebook.com/"+videos[n].id),
		{v: parseFloat(videos[n].organic_ctr), f: parseFloat(videos[n].organic_ctr)+"%"},
		{v: parseFloat(videos[n].penetration), f: parseFloat(videos[n].penetration)+"%"},
		{v: parseFloat(videos[n].viral_amp), f: parseFloat(videos[n].viral_amp)+"x"},
		parseInt(videos[n].link_clicks),
		parseInt(videos[n].shares),
		parseInt(videos[n].likes),
		parseInt(videos[n].reach),
		parseInt(videos[n].comments),
		parseInt(videos[n].engagement),
		parseInt(videos[n].clicked_to_play),
		parseInt(videos[n]['10s_sound']),
		parseInt(videos[n].reaction_like),
		parseInt(videos[n].reaction_love),
		parseInt(videos[n].reaction_wow),
		parseInt(videos[n].reaction_haha),
		parseInt(videos[n].reaction_sorry),
		parseInt(videos[n].reaction_anger),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+videos[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_repsodia'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<videos.length;n++){
		links[n].childNodes[0].setAttribute('title',videos[n].name);				
		links[n].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_deformatv(){
	$.ajax({
		url: "php_scripts/get_data_deformatv.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_deformatv=response;
		}
	});	
	drawTableKomfo_deformatv();
}
function drawTableKomfo_deformatv(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_deformatv.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_deformatv[n].name)).link("https://facebook.com/"+posts_deformatv[n].id),
		{v: parseFloat(posts_deformatv[n].penetration), f: parseFloat(posts_deformatv[n].penetration)+"%"},
		{v: parseFloat(posts_deformatv[n].viral_amp), f: parseFloat(posts_deformatv[n].viral_amp)+"x"},
		{v: parseFloat(posts_deformatv[n].organic_ctr), f: parseFloat(posts_deformatv[n].organic_ctr)+"%"},
		parseInt(posts_deformatv[n].link_clicks),
		parseInt(posts_deformatv[n].shares),
		parseInt(posts_deformatv[n].likes),
		parseInt(posts_deformatv[n].comments),
		parseInt(posts_deformatv[n].reach),
		parseInt(posts_deformatv[n].engagement),
        '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_deformatv[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_deformatv'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_boom(){
	$.ajax({
		url: "php_scripts/get_data_boom.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_boom=response;
		}
	});	
	drawTableKomfo_boom();
}
function drawTableKomfo_boom(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  
  

  for(var n=0;n<posts_boom.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_boom[n].name)).link("https://facebook.com/"+posts_boom[n].id),
		{v: parseFloat(posts_boom[n].penetration), f: parseFloat(posts_boom[n].penetration)+"%"},
		{v: parseFloat(posts_boom[n].viral_amp), f: parseFloat(posts_boom[n].viral_amp)+"x"},
		{v: parseFloat(posts_boom[n].organic_ctr), f: parseFloat(posts_boom[n].organic_ctr)+"%"},
		parseInt(posts_boom[n].link_clicks),
		parseInt(posts_boom[n].shares),
		parseInt(posts_boom[n].likes),
		parseInt(posts_boom[n].comments),
		parseInt(posts_boom[n].reach),
		parseInt(posts_boom[n].engagement)
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_boom'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_repsodiaPosts(){
	posts_repsodia=[];
    query.edge = "repsodia/posts";//query latest posts
    query.fields = "id,message,name,created_time,type,shares&since=3daysAgo";
    $.ajax({
        url: buildURL(query,access_token,100),
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(response){
            videos=response.data;
            //console.log(videos);
        }
    });//ajax query to find videos of the past 20 days
    now=new Date();
    var video_date;
    for(j=0;j<videos.length;j++){
        if(videos[j].type!="video"){
            posts_repsodia.push(videos[j]);
            videos.splice(j,1);
            j--;
        }
    }
    for(j=0;j<videos.length;j++){
      videos[j].post_id=videos[j].id;
      delete videos[j].id;
      //console.log(posts[j]);
      for(k=0;k<adgroups.length;k++){
        if(videos[j].post_id.localeCompare(adgroups[k].post_id)==0){
          videos[j].promoted=true;
        }
      }
    }//for loop to flag promotted posts

    //Repsodia video promises
    var repsodia_video_promises = [];
    function getRepsodiaVideoMetrics(j){
        if(videos[j].promoted==true){

        }
        else{
            videos[j].created_time = new Date(new Date(videos[j].created_time).getTime()-(new Date().getTimezoneOffset()/60)*60*60*1000).toISOString().substr(0,19).replace('T',' ');
            post_date = new Date(videos[j].created_time);
            videos[j].shares = typeof videos[j].shares === 'undefined' ? 0 : videos[j].shares.count;
            //console.log(posts[j].name);
            query.edge=videos[j].post_id;
            query.fields="likes.limit(0).summary(1).as(likes)"+
                    ",comments.limit(0).summary(1).as(comments)"+
                    ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                    ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                    ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                    ",insights.metric(post_impressions).fields(values).as(impressions)"+
                    ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                    ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                    ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                    ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)"+
                    ",insights.metric(post_video_views_clicked_to_play).period(lifetime).fields(values).as(video_views_click_to_play)"+
                    ",insights.metric(post_video_views_10s_sound_on).period(lifetime).fields(values).as(video_views_sound)"+
                    ",insights.metric(post_video_retention_graph).period(lifetime).fields(values).as(graph)"+
                    ",insights.metric(post_reactions_by_type_total).period(lifetime).fields(values).as(reactions_total)";
            $.ajax({
                url: buildURL(query,pageTokens['Repsodia'],1),
                type: 'get',
                dataType: 'json',
                async: true,
                success: function(response) {	
                        //console.log('in metrics');				  
                        videos[j].viral_imp=response.viral_imp.data[0].values[0].value;

                        videos[j].organic_imp=response.organic_imp.data[0].values[0].value;

                        videos[j].imp=response.impressions.data[0].values[0].value;

                        videos[j].reach=response.fan_reach.data[0].values[0].value;

                        videos[j].unique_imp=response.reach.data[0].values[0].value;

                        videos[j].paid_imp=response.impressions_paid.data[0].values[0].value;
                        videos[j].unpaid_imp=videos[j].imp-videos[j].paid_imp

                        videos[j].link_clicks=response.video_views.data[0].values[0].value;

                        videos[j].clicked_to_play=response.video_views_click_to_play.data[0].values[0].value;
                        videos[j]['10s_sound']=response.video_views_sound.data[0].values[0].value;

                        videos[j].retention_20=response.graph.data[0].values[0].value['8'];
                        videos[j].retention_40=response.graph.data[0].values[0].value['16'];
                        videos[j].retention_60=response.graph.data[0].values[0].value['24'];
                        videos[j].retention_80=response.graph.data[0].values[0].value['32'];
                        videos[j].retention_100=response.graph.data[0].values[0].value['40'];

                        videos[j].reaction_like=response.reactions_total.data[0].values[0].value.like;
                        videos[j].reaction_love=response.reactions_total.data[0].values[0].value.love;
                        videos[j].reaction_wow=response.reactions_total.data[0].values[0].value.wow;
                        videos[j].reaction_haha=response.reactions_total.data[0].values[0].value.haha;
                        videos[j].reaction_sorry=response.reactions_total.data[0].values[0].value.sorry;
                        videos[j].reaction_anger=response.reactions_total.data[0].values[0].value.anger;
                        videos[j].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                        videos[j].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                        videos[j].fans = typeof pageFans[videos[j].post_id.substr(0,videos[j].post_id.indexOf('_'))] === 'undefined' ? 0 : pageFans[videos[j].post_id.substr(0,videos[j].post_id.indexOf('_'))];
                        videos[j].viral_amp=(videos[j].viral_imp/videos[j].organic_imp).toFixed(2);
                        videos[j].organic_ctr=(videos[j].link_clicks/videos[j].imp*100).toFixed(2);
                        videos[j].ad_id="";
                        videos[j].spend=0;
                        videos[j].cxw=0;
                        videos[j].paid_link_clicks=0;
                        videos[j].paid_ctr=0;
                        videos[j].penetration=(videos[j].reach/videos[j].fans*100).toFixed(2);
                        videos[j].engagement=videos[j].likes+videos[j].comments+videos[j].shares+videos[j].link_clicks;
                        videos[j].cpm=0;
                        videos[j].num=j;
                    }
            });//ajax query to find all metrics with a switch
        }
    }
    for(j=0;j<videos.length;j++){
        repsodia_video_promises.push(
            getRepsodiaVideoMetrics(j)
        );
    }//for loop to record all videos  ///////Promisified
    $.when.apply($, repsodia_video_promises).then(function() {
        console.log('Repsodia Video promises succeded');
    },function() {
        console.log('Repsodia video promises failed');
    });

    for(j=0;j<posts_repsodia.length;j++){
      posts_repsodia[j].post_id=posts_repsodia[j].id;
      //delete posts_repsodia[j].id;
      //console.log(posts[j]);
      for(k=0;k<adgroups.length;k++){
        if(posts_repsodia[j].post_id.localeCompare(adgroups[k].post_id)==0){
          posts_repsodia[j].promoted=true;
        }
      }
    }//for loop to flag promotted posts

    //Repsodia posts promises				
    var repsodia_posts_promises = [];
    function getRepsodiaPostsMetrics(j){
        if(posts_repsodia[j].promoted==true){

        }else{
            posts_repsodia[j].created_time = new Date(new Date(posts_repsodia[j].created_time).getTime()-(new Date().getTimezoneOffset()/60)*60*60*1000).toISOString().substr(0,19).replace('T',' ');
            post_date = new Date(posts_repsodia[j].created_time);
            posts_repsodia[j].shares= typeof posts_repsodia[j].shares === 'undefined' ? 0 : posts_repsodia[j].shares.count;
            //console.log(posts[j].name);

            query.edge=posts_repsodia[j].post_id;
            query.fields="likes.limit(0).summary(1).as(likes)"+
                ",comments.limit(0).summary(1).as(comments)"+
                ",insights.metric(post_impressions_viral).fields(values).as(viral_imp)"+
                ",insights.metric(post_impressions_organic).fields(values).as(organic_imp)"+
                ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
                ",insights.metric(post_impressions).fields(values).as(impressions)"+
                ",insights.metric(post_fan_reach).fields(values).as(fan_reach)"+
                ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
                ",insights.metric(post_impressions_paid).fields(values).as(impressions_paid)"+
                ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)";
            $.ajax({
                url: buildURL(query,pageTokens['Repsodia'],1),
                type: 'get',
                dataType: 'json',
                async: true,
                success: function(response) {
                    posts_repsodia[j].viral_imp=response.viral_imp.data[0].values[0].value;
                    posts_repsodia[j].organic_imp=response.organic_imp.data[0].values[0].value;
                    posts_repsodia[j].imp=response.impressions.data[0].values[0].value;
                    posts_repsodia[j].reach=response.fan_reach.data[0].values[0].value;
                    posts_repsodia[j].unique_imp=response.reach.data[0].values[0].value;
                    posts_repsodia[j].paid_imp=response.impressions_paid.data[0].values[0].value;
                    posts_repsodia[j].unpaid_imp=posts_repsodia[j].imp-posts_repsodia[j].paid_imp
                    if(posts_repsodia[j].type == 'link' || posts_repsodia[j].type == 'photo'){
                        posts_repsodia[j].link_clicks=response.consumptions.data[0].values[0].value['link clicks'];
                    }
                    if(posts_repsodia[j].type == 'video'){
                        posts_repsodia[j].link_clicks=response.video_views.data[0].values[0].value;
                    }
                    posts_repsodia[j].likes=typeof response.likes.summary === 'undefined' ? 0 : response.likes.summary.total_count;
                    posts_repsodia[j].comments=typeof response.comments.summary === 'undefined' ? 0 : response.comments.summary.total_count;
                    posts_repsodia[j].fans = typeof pageFans[posts_repsodia[j].post_id.substr(0,posts_repsodia[j].post_id.indexOf('_'))] === 'undefined' ? 0 : pageFans[posts_repsodia[j].post_id.substr(0,posts_repsodia[j].post_id.indexOf('_'))];
                    posts_repsodia[j].viral_amp=(posts_repsodia[j].viral_imp/posts_repsodia[j].organic_imp).toFixed(2);
                    posts_repsodia[j].organic_ctr=(posts_repsodia[j].link_clicks/posts_repsodia[j].imp*100).toFixed(2);
                    posts_repsodia[j].ad_id="";
                    posts_repsodia[j].spend=0;
                    posts_repsodia[j].cxw=0;
                    posts_repsodia[j].paid_link_clicks=0;
                    posts_repsodia[j].paid_ctr=0;
                    posts_repsodia[j].penetration=(posts_repsodia[j].reach/posts_repsodia[j].fans*100).toFixed(2);
                    posts_repsodia[j].engagement=posts_repsodia[j].likes+posts_repsodia[j].comments+posts_repsodia[j].shares+posts_repsodia[j].link_clicks;
                    posts_repsodia[j].cpm=0;
                    posts_repsodia[j].num=j;
                }
            });//ajax query to find all metrics with a switch
        }
    }//for loop to record all repsodia posts
    for(j=0;j<posts_repsodia.length;j++){
        repsodia_posts_promises.push(
            getRepsodiaPostsMetrics(j)
        );
    }//for loop to record all posts  ///////Promisified
    $.when.apply($, repsodia_posts_promises).then(function() {
        console.log('Repsodia posts promises succeded');
        setTimeout(function(){drawTableKomfo_repsodia_posts()},1000);
    },function() {
        console.log('Repsodia posts promises failed');
    });
}
function drawTableKomfo_repsodia_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_repsodia_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_repsodia_posts[n].name)).link("https://facebook.com/"+posts_repsodia_posts[n].id),
		{v: parseFloat(posts_repsodia_posts[n].penetration), f: parseFloat(posts_repsodia_posts[n].penetration)+"%"},
		{v: parseFloat(posts_repsodia_posts[n].viral_amp), f: parseFloat(posts_repsodia_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_repsodia_posts[n].organic_ctr), f: parseFloat(posts_repsodia_posts[n].organic_ctr)+"%"},
		parseInt(posts_repsodia_posts[n].link_clicks),
		parseInt(posts_repsodia_posts[n].shares),
		parseInt(posts_repsodia_posts[n].likes),
		parseInt(posts_repsodia_posts[n].comments),
		parseInt(posts_repsodia_posts[n].reach),
		parseInt(posts_repsodia_posts[n].engagement),
        '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_repsodia_posts[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_repsodia_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_open(){
	$.ajax({
		url: "php_scripts/get_data_promoted.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_open=response;
		}
	});	
	drawTableKomfo_open();
}
function drawTableKomfo_open(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'string', label: 'Activa'});

  for(var n=0;n<posts_open.length;n++){
	if(posts_open[n].promoted==true){
		data_table.addRow([
			n+1,
			((posts_open[n].name).link("https://facebook.com/"+posts_open[n].id)),
			'<span class="label label-success">Active</span>'
		]);
	}
	else
	{		
		data_table.addRow([
			n+1,
			((posts_open[n].name).link("https://facebook.com/"+posts_open[n].id)),
			''
		]);
	}
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': false,
	'width':'100%',
	'height':'100%',
	'sortColumn': 2,
	'sortAscending': false
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_deforma_open'));

  table.draw(data_table, options);
    
  var pre_links = document.getElementsByClassName('google-visualization-table-td');
  
  links=[];
  
  for(var i=0;i<pre_links.length;i++){
  	if(i%3 == 1){
  		links.push(pre_links[i]);
  	}
  }
  	
  for(var n=0;n<posts_open.length;n++){
	links[n].childNodes[0].setAttribute('title',posts_open[n].name);
  }
			
  for(var o=0;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=0;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_cult(){
	$.ajax({
		url: "php_scripts/get_data_cult.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_cult=response;
		}
	});	
	drawTableKomfo_cult();
}
function drawTableKomfo_cult(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_cult.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_cult[n].name)).link("https://facebook.com/"+posts_cult[n].id),
		{v: parseFloat(posts_cult[n].organic_ctr), f: parseFloat(posts_cult[n].organic_ctr)+"%"},
		{v: parseFloat(posts_cult[n].penetration), f: parseFloat(posts_cult[n].penetration)+"%"},
		{v: parseFloat(posts_cult[n].viral_amp), f: parseFloat(posts_cult[n].viral_amp)+"x"},
		parseInt(posts_cult[n].link_clicks),
		parseInt(posts_cult[n].shares),
		parseInt(posts_cult[n].likes),
		parseInt(posts_cult[n].reach),
		parseInt(posts_cult[n].comments),
		parseInt(posts_cult[n].engagement),
		parseInt(posts_cult[n].clicked_to_play),
		parseInt(posts_cult[n]['10s_sound']),
		parseInt(posts_cult[n].reaction_like),
		parseInt(posts_cult[n].reaction_love),
		parseInt(posts_cult[n].reaction_wow),
		parseInt(posts_cult[n].reaction_haha),
		parseInt(posts_cult[n].reaction_sorry),
		parseInt(posts_cult[n].reaction_anger),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_cult[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_cult'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_cult.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_cult[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_cultPosts(){
	$.ajax({
		url: "php_scripts/get_data_cult_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_cult_posts=response;
		}
	});	
	drawTableKomfo_cult_posts();
}
function drawTableKomfo_cult_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});
  
  

  for(var n=0;n<posts_cult_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_cult_posts[n].name)).link("https://facebook.com/"+posts_cult_posts[n].id),
		{v: parseFloat(posts_cult_posts[n].penetration), f: parseFloat(posts_cult_posts[n].penetration)+"%"},
		{v: parseFloat(posts_cult_posts[n].viral_amp), f: parseFloat(posts_cult_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_cult_posts[n].organic_ctr), f: parseFloat(posts_cult_posts[n].organic_ctr)+"%"},
		parseInt(posts_cult_posts[n].link_clicks),
		parseInt(posts_cult_posts[n].shares),
		parseInt(posts_cult_posts[n].likes),
		parseInt(posts_cult_posts[n].comments),
		parseInt(posts_cult_posts[n].reach),
		parseInt(posts_cult_posts[n].engagement),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_cult_posts[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_cult_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_holixir(){
	$.ajax({
		url: "php_scripts/get_data_holixir.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_holixir=response;
		}
	});	
	drawTableKomfo_holixir();//
}
function drawTableKomfo_holixir(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});

  for(var n=0;n<posts_holixir.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_holixir[n].name)).link("https://facebook.com/"+posts_holixir[n].id),
		{v: parseFloat(posts_holixir[n].organic_ctr), f: parseFloat(posts_holixir[n].organic_ctr)+"%"},
		{v: parseFloat(posts_holixir[n].penetration), f: parseFloat(posts_holixir[n].penetration)+"%"},
		{v: parseFloat(posts_holixir[n].viral_amp), f: parseFloat(posts_holixir[n].viral_amp)+"x"},
		parseInt(posts_holixir[n].link_clicks),
		parseInt(posts_holixir[n].shares),
		parseInt(posts_holixir[n].likes),
		parseInt(posts_holixir[n].reach),
		parseInt(posts_holixir[n].comments),
		parseInt(posts_holixir[n].engagement),
		parseInt(posts_holixir[n].clicked_to_play),
		parseInt(posts_holixir[n]['10s_sound']),
		parseInt(posts_holixir[n].reaction_like),
		parseInt(posts_holixir[n].reaction_love),
		parseInt(posts_holixir[n].reaction_wow),
		parseInt(posts_holixir[n].reaction_haha),
		parseInt(posts_holixir[n].reaction_sorry),
		parseInt(posts_holixir[n].reaction_anger)
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_holixir'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_holixir.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_holixir[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_holixirPosts(){
	$.ajax({
		url: "php_scripts/get_data_holixir_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_holixir_posts=response;
		}
	});	
	drawTableKomfo_holixir_posts();
}
function drawTableKomfo_holixir_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  
  

  for(var n=0;n<posts_holixir_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_holixir_posts[n].name)).link("https://facebook.com/"+posts_holixir_posts[n].id),
		{v: parseFloat(posts_holixir_posts[n].penetration), f: parseFloat(posts_holixir_posts[n].penetration)+"%"},
		{v: parseFloat(posts_holixir_posts[n].viral_amp), f: parseFloat(posts_holixir_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_holixir_posts[n].organic_ctr), f: parseFloat(posts_holixir_posts[n].organic_ctr)+"%"},
		parseInt(posts_holixir_posts[n].link_clicks),
		parseInt(posts_holixir_posts[n].shares),
		parseInt(posts_holixir_posts[n].likes),
		parseInt(posts_holixir_posts[n].comments),
		parseInt(posts_holixir_posts[n].reach),
		parseInt(posts_holixir_posts[n].engagement)
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_holixir_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_partidoPosts(){
	$.ajax({
		url: "php_scripts/get_data_partido_posts.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_partido_posts=response;
		}
	});	
	drawTableKomfo_partido_posts();
}
function drawTableKomfo_partido_posts(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'CTR Organico'});
  data_table.addColumn({type: 'number', label: 'Clicks'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'string', label: 'Promote'});
  
  

  for(var n=0;n<posts_partido_posts.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_partido_posts[n].name)).link("https://facebook.com/"+posts_partido_posts[n].id),
		{v: parseFloat(posts_partido_posts[n].penetration), f: parseFloat(posts_partido_posts[n].penetration)+"%"},
		{v: parseFloat(posts_partido_posts[n].viral_amp), f: parseFloat(posts_partido_posts[n].viral_amp)+"x"},
		{v: parseFloat(posts_partido_posts[n].organic_ctr), f: parseFloat(posts_partido_posts[n].organic_ctr)+"%"},
		parseInt(posts_partido_posts[n].link_clicks),
		parseInt(posts_partido_posts[n].shares),
		parseInt(posts_partido_posts[n].likes),
		parseInt(posts_partido_posts[n].comments),
		parseInt(posts_partido_posts[n].reach),
		parseInt(posts_partido_posts[n].engagement),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_partido_posts[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_partido_posts'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 5});

  formatter_penetration.format(data_table, 2);
  formatter_viral_amp.format(data_table, 3);
  formatter_ctr.format(data_table, 4);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
			
  for(var o=1;o < links.length;o++){
	links[o].childNodes[0].setAttribute('target','_blank');
  }
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function loadKomfo_partidoVideos(){
	$.ajax({
		url: "php_scripts/get_data_partido.php",
		type: 'get',
		dataType: 'json',
		async: false,
		success: function(response){
			posts_partido_videos=response;
		}
	});	
	drawTableKomfo_partidoVideos();
}
function drawTableKomfo_partidoVideos(){
  var data_table = new google.visualization.DataTable();

  data_table.addColumn({type: 'number', label: 'Orden'});
  data_table.addColumn({type: 'string', label: 'Nombre'});
  data_table.addColumn({type: 'number', label: 'VTR Organico'});
  data_table.addColumn({type: 'number', label: 'Penetration'});
  data_table.addColumn({type: 'number', label: 'Viral Amp'});
  data_table.addColumn({type: 'number', label: 'Video Views'});
  data_table.addColumn({type: 'number', label: 'Shares'});
  data_table.addColumn({type: 'number', label: 'Likes'});
  data_table.addColumn({type: 'number', label: 'Reach'});
  data_table.addColumn({type: 'number', label: 'Comments'});
  data_table.addColumn({type: 'number', label: 'Engagement'});
  data_table.addColumn({type: 'number', label: 'Clicked to play'});
  data_table.addColumn({type: 'number', label: '10s Sound'});
  data_table.addColumn({type: 'number', label: 'Like'});
  data_table.addColumn({type: 'number', label: 'Love'});
  data_table.addColumn({type: 'number', label: 'Wow'});
  data_table.addColumn({type: 'number', label: 'Haha'});
  data_table.addColumn({type: 'number', label: 'Sorry'});
  data_table.addColumn({type: 'number', label: 'Anger'});
  data_table.addColumn({type: 'string', label: 'Promote'});

  for(var n=0;n<posts_partido_videos.length;n++){
	data_table.addRow([
		n+1,
		(titleShortener(posts_partido_videos[n].name)).link("https://facebook.com/"+posts_partido_videos[n].id),
		{v: parseFloat(posts_partido_videos[n].organic_ctr), f: parseFloat(posts_partido_videos[n].organic_ctr)+"%"},
		{v: parseFloat(posts_partido_videos[n].penetration), f: parseFloat(posts_partido_videos[n].penetration)+"%"},
		{v: parseFloat(posts_partido_videos[n].viral_amp), f: parseFloat(posts_partido_videos[n].viral_amp)+"x"},
		parseInt(posts_partido_videos[n].link_clicks),
		parseInt(posts_partido_videos[n].shares),
		parseInt(posts_partido_videos[n].likes),
		parseInt(posts_partido_videos[n].reach),
		parseInt(posts_partido_videos[n].comments),
		parseInt(posts_partido_videos[n].engagement),
		parseInt(posts_partido_videos[n].clicked_to_play),
		parseInt(posts_partido_videos[n]['10s_sound']),
		parseInt(posts_partido_videos[n].reaction_like),
		parseInt(posts_partido_videos[n].reaction_love),
		parseInt(posts_partido_videos[n].reaction_wow),
		parseInt(posts_partido_videos[n].reaction_haha),
		parseInt(posts_partido_videos[n].reaction_sorry),
		parseInt(posts_partido_videos[n].reaction_anger),
		'<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick="getPromotePostId(\''+posts_partido_videos[n].id+'\')">Promote</button>'
	]);
  }

  // Set chart options
  var options = {
	'allowHtml': true,
	'showRowNumber': true,
	'width':'100%',
	'height':'80%',
	'frozenColumns': 2
  };
  

  // Instantiate and draw our chart, passing in some options.
  var table = new google.visualization.Table(document.getElementById('komfo_table_partido_videos'));

  var formatter_penetration = new google.visualization.BarFormat({width: 60, colorPositive: 'red', max: 50});
  var formatter_viral_amp = new google.visualization.BarFormat({width: 60, colorPositive: 'blue', max: 1.5});
  var formatter_ctr = new google.visualization.BarFormat({width: 60, colorPositive: 'green', max: 25});

  formatter_penetration.format(data_table, 3);
  formatter_viral_amp.format(data_table, 4);
  formatter_ctr.format(data_table, 2);

  table.draw(data_table, options);
    
  links = document.getElementsByClassName('google-visualization-table-td frozen-column last-frozen-column');
  	
	for(var n=0;n<posts_partido_videos.length;n++){
		links[n+1].childNodes[0].setAttribute('title',posts_partido_videos[n].name);				
		links[n+1].childNodes[0].setAttribute('target','_blank');				
	}
	
	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	}
  
  google.visualization.events.addListener(table, 'sort', function() {
  	for(var o=1;o < links.length;o++){
		links[o].childNodes[0].setAttribute('target','_blank');
	  }
  });
}

function titleShortener(text){
	if(text.length<50){
		return text;
	}
	else{
		return text.substring(0,20)+'...'+text.substring(text.length-20,text.length);
	}
}
//better komfo

//One click promote
var adCreative = {};
var adCampaign = {};
var adSet = {};
var adAd = {};
var promoteParameters = {adCampaign, adSet, adCreative, adAd};
function getPromotePostId(id){
	var now =new Date();
	$('#promote-datepicker').val(now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate());
	//console.log(id);
	$('#promote-timepicker').timepicker('setTime', new Date());
	if(!document.getElementById('mobile_placement').checked){console.log('reset mobile');document.getElementById('mobile_placement').click();}
	if(!document.getElementById('desktop_placement').checked){document.getElementById('desktop_placement').click();}
	if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
	promoteParameters.adCreative.object_story_id = id;
	promoteParameters.adCreative.url_tags = 'p1=true';
	var selector=document.getElementById("exclude");
	selector.selectedIndex = getExcludedDate();
	getObjective(id);
}
function deleteLimit(){
	$('#limit_value')[0].value = '';
}
function resetForm(){
	document.getElementById('name').value = '';
	document.getElementById('budget').value = '';
	document.getElementById('bid').value = '0.01';
	document.getElementById('min_age').value = '16';
	document.getElementById('max_age').value = '65';
	$('#ad_account')[0].selectedIndex = 0;
	$('#excluded_group')[0].style.visibility = 'visible';
	$('#gcs_group')[0].style.visibility = 'visible';
	if(!document.getElementById('mobile_placement').checked){console.log('reset mobile');document.getElementById('mobile_placement').click();}
	if(!document.getElementById('desktop_placement').checked){document.getElementById('desktop_placement').click();}
	if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
	$('#bid_label')[0].innerHTML = 'Bid (USD)';
	$('#budget_label')[0].innerHTML = 'Budget (USD)';
	var budget = $('#budget').parsley();
	budget.options.range = [20,5000];
	var bid = $('#bid').parsley();
	bid.options.range = [0.01,0.05];
	$('#no_limit')[0].click();
    $('#do-not-notify')[0].checked = false;
}
function adAccountSelected(id){
	typeof id === 'undefined' ? id=promoteParameters.adCreative.object_story_id : id=id;
	//console.log('In adAccountSelected',id);
	switch($('#ad_account')[0].selectedIndex){
		case 0:
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'hidden';
			//$('#excluded_group')[0].style.visibility = !isVideo ? 'visible' : 'hidden';
			if(!isVideo(id)){
				if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
				$('#gcs_group')[0].style.visibility = 'visible';
			}else{
				document.getElementById('radio_GCS_None').checked = true;
				$('#gcs_group')[0].style.visibility = 'hidden';
			}
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = getExcludedDate();
			var budget = $('#budget').parsley();
			budget.options.range = [20,5000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = false;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
		case 1:
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'visible';
			//$('#excluded_group')[0].style.visibility = !isVideo ? 'visible' : 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = 1;
			var budget = $('#budget').parsley();
			budget.options.range = [20,2000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true&b1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = false;
            $('#latam')[0].checked = true;
			break;
		case 2:
			$('#bid_label')[0].innerHTML = 'Bid (MXN)';
			$('#budget_label')[0].innerHTML = 'Budget (MXN)';
            $('#notify-group')[0].style.visibility = 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			selector.selectedIndex = 0;
			//$('#excluded_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			var budget = $('#budget').parsley();
			budget.options.range = [500,8000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.2,0.5];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
		case 3:
			$('#bid_label')[0].innerHTML = 'Bid (USD)';
			$('#budget_label')[0].innerHTML = 'Budget (USD)';
            $('#notify-group')[0].style.visibility = 'hidden';
			//$('#excluded_group')[0].style.visibility = !isVideo ? 'visible' : 'hidden';
			document.getElementById('radio_GCS_Hybrid').checked = true;
			if(!isVideo(id)){
				if(!document.getElementById('radio_GCS_None').checked){document.getElementById('radio_GCS_None').checked = true;}
				$('#gcs_group')[0].style.visibility = 'visible';
			}else{
				document.getElementById('radio_GCS_None').checked = true;
				$('#gcs_group')[0].style.visibility = 'hidden';
			}
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
			var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = getExcludedDate();
			var budget = $('#budget').parsley();
			budget.options.range = [20,5000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,0.05];
			promoteParameters.adCreative.url_tags = 'p1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
        case 4:
			$('#bid_label')[0].innerHTML = 'Bid (MXN)';
			$('#budget_label')[0].innerHTML = 'Budget (MXN)';
            $('#notify-group')[0].style.visibility = 'hidden';
			//$('#excluded_group')[0].style.visibility = !isVideo ? 'visible' : 'hidden';
			document.getElementById('radio_GCS_None').checked = true;
			$('#gcs_group')[0].style.visibility = 'hidden';
			var selector=document.getElementById("exclude");
			while(selector.length>0){
				selector.remove(0);
			}
            $('#objective')[0].selectedIndex = 2;
            $('#billing_event')[0].selectedIndex = 2;
            $('#optimization')[0].selectedIndex = 2;
			/*var audiences = isVideo(id) ? [] : [{name: 'El deforma 5 days', id: '6034155584927'},{name: 'El deforma 10 days', id: '6034155576527'},{name: 'El deforma 15 days',id: '6034155568527'},{name: 'El deforma 20 days', id: '6034155515127'},{name: 'El deforma 25 days', id: '6034155490727'},{name: 'El deforma 30 days', id: '6020024675727'}]
			var option = document.createElement("option");
			option.text = "";
			option.value = ""; 
			selector.add(option);
			for(var j=0;j<audiences.length;j++)
			{
				var option = document.createElement("option");
				option.text = audiences[j].name;
				option.value = audiences[j].id; 
				selector.add(option);
			}
			selector.selectedIndex = getExcludedDate();*/
			var budget = $('#budget').parsley();
			budget.options.range = [20,40000];
			var bid = $('#bid').parsley();
			bid.options.range = [0.01,5];
			promoteParameters.adCreative.url_tags = 'p1=true&b1=true';
            $('#desktop_placement')[0].checked = true;
            $('#mobile_placement')[0].checked = true;
            $('#mexico')[0].checked = true;
            $('#latam')[0].checked = false;
			break;
	}
}
function getPromoteParameters(){
	promoteParameters.adCampaign.name = document.getElementById('name').value;
	promoteParameters.adCampaign.buying_type = 'AUCTION';
	promoteParameters.adCampaign.objective = document.getElementById('objective').value;
	promoteParameters.adCampaign.status = 'ACTIVE';	
	
	promoteParameters.adSet.name = document.getElementById('name').value;
	promoteParameters.adSet.bid_amount = parseFloat(document.getElementById('bid').value)*100;
	promoteParameters.adSet.billing_event = document.getElementById('billing_event').value;
	promoteParameters.adSet.campaign_id = '';
	promoteParameters.adSet.daily_budget = parseFloat(document.getElementById('budget').value)*100;
	promoteParameters.adSet.is_autobid= false;
	promoteParameters.adSet.optimization_goal = document.getElementById('optimization').value;
	promoteParameters.adSet.status= 'ACTIVE';
	promoteParameters.adSet.start_time =new Date($('#promote-datepicker').val()+' '+$('#promote-timepicker').data('timepicker').hour.toString()+':'+$('#promote-timepicker').data('timepicker').minute.toString()+' '+$('#promote-timepicker').data('timepicker').meridian).getTime()/1000;
	promoteParameters.adSet.targeting = {
		geo_locations: {
			countries: getCountries()
		},
		age_min: parseInt(document.getElementById('min_age').value),
		age_max: parseInt(document.getElementById('max_age').value),
		publisher_platforms: ['facebook'],
		device_platforms: getPlacements(),
		facebook_positions: ['feed']
	};
	if($("#exclude")[0].value !== ''){
		promoteParameters.adSet.targeting.excluded_custom_audiences = [{'id': document.getElementById('exclude').value}];
	}
    if($("#connections")[0].value !== ''){
		promoteParameters.adSet.targeting.connections = [{'id': document.getElementById('connections').value}];
	}
    if($('#objective')[0].value == 'CONVERSIONS'){
        promoteParameters.adSet.attribution_spec = [{
            event_type: "CLICK_THROUGH",
            window_days: 1
        }];
        promoteParameters.adSet.promoted_object = {
            pixel_id: "286492281506212",
            custom_event_type: "CONTENT_VIEW"
        };
    }
    
	promoteParameters.adAd.name = document.getElementById('name').value;
	promoteParameters.adAd.adset_id = '';
	promoteParameters.adAd.creative = {'creative_id': ''};
	promoteParameters.adAd.status = 'ACTIVE';
	
	createCampaign();
}
function createCampaign(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/campaigns',
		'POST',
		promoteParameters.adCampaign,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adSet.campaign_id = response.id;
				setTimeout(createAdset(),2000);
			}else{
				alert(response.error.message);
			}
		}
	);
}
function createAdset(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/adsets',
		'POST',
		promoteParameters.adSet,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adAd.adset_id = response.id;
				setTimeout(createCreative(),500);
			}else{
				alert(response.error.message);
			}
		}
	);
}
function createCreative(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/adcreatives',
		'POST',
		promoteParameters.adCreative,
		function(response) {
			console.log(response);
			if(response.hasOwnProperty('id')){
				promoteParameters.adAd.creative.creative_id = response.id;
				setTimeout(createAd(),500);
			}
			else{
				alert(response.error.message);
			}
		}
	);
}
function createAd(){
	FB.api(
		'/'+$('#ad_account')[0].value+'/ads',
		'POST',
		promoteParameters.adAd,
		function(response) {
			if(response.hasOwnProperty('id')){
				alert('Campaign successfully created!');
				updatePromotedPosts();
				loadKomfo();
				if($('input[name=radio]:checked').val() == 'full'){
					activateGCS(promoteParameters.adCreative.object_story_id);
				}else if($('input[name=radio]:checked').val() == 'hybrid'){
					activateHybridGCS(promoteParameters.adCreative.object_story_id);
				}
				if($('#ad_account')[0].value == 'act_31492866' && !$('#do-not-notify')[0].checked){
					$.post(
						"https://hooks.slack.com/services/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",
						JSON.stringify({
							"channel": '#implementar_comercial',
							"username": 'Alerta de Campaña',
							"attachments": [{
								"fallback": 'Campaña iniciada',
								"color": "green",
								"author_name": 'Campaign Bot',
								"title": 'Campaña iniciada',
								"text": 'La campaña para: '+promoteParameters.adCampaign.name+'\nha empezado a promoverse\n'
							}]
						})
					);
				}
			}else{
				alert(response.error.message);
			}
			resetForm();
		}
	);
}
function activateGCS(id){
	FB.api(
		'/'+id,
		'GET',
		{"fields":"link"},
		function(response) {
			var link = response.link;
			var slug = link.substr(32,link.length).substr(0,link.substr(32,link.length).indexOf('/'));
			console.log(slug);
			getPostIdForGCS(slug,id);
		}
	);
}
function getPostIdForGCS(slug,id){
	$.ajax({
		url: 'http://eldeforma.com/api/get_post/?slug='+slug+'&include=id',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			if(!data.hasOwnProperty('error')){
				console.log(data.post.id);
				updatePostGCS(data.post.id,id);
			}else{
				alert('find post: '+data.error);
			}
		}
	});
}
function updatePostGCS(id,fb_id){
	var wp_nonce;
	$.ajax({
		url: 'http://eldeforma.com/api/get_nonce/?controller=posts&method=update_post',
		headers: {
			'Authorization':'Basic dan:Db5471033187'
		},
		method: 'GET',
		async: false,
		dataType: 'jsonp',
		success: function(data){
			wp_nonce = data.nonce;
			$.ajax({
				url: 'http://eldeforma.com/api/get_post/?post_id='+id,
				headers: {
					'Authorization':'Basic dan:Db5471033187'
				},
				method: 'GET',
				async: false,
				dataType: 'jsonp',
				success: function(data){
					var previousContent = data.post.content;
					$.ajax({
						url: 'http://eldeforma.com/api/posts/update_post/?id='+id,
						headers: {
							'Authorization':'Basic dan:Db5471033187'
						},
						method: 'POST',
						async: false,
						dataType: 'jsonp',
						data: {
							content: '<div class="p402_premium">\n'+previousContent+'<\/div>\n<script type="text\/javascript">\n\ttry { _402_Show(); } catch(e) {}\n<\/script>',
							nonce: wp_nonce
						},
						success: function(data){
							removeInstant(fb_id);
							alert('GCS successfully added');
							updateDatabaseGCS(fb_id);
						}
					});
				}
			});
		}
	});
}
function updateDatabaseGCS(id){
	$.ajax({
		url: 'php_scripts/updateGCS.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			id: id
		},
		success: function(response){
			
		}
	});
	loadKomfo();
}
function removeInstant(fb_id){
	FB.api(
		'/'+fb_id,
		'GET',
		{"fields":"link"},
		function(response) {
			console.log('in remove instant',response);
			var link = response.link.substr(0,response.link.indexOf('?'));
			FB.api(
				'/',
				'GET',
				{"id":link,"fields":"instant_article"},
				function(answer) {
				  console.log('in getting instant article for deletion',answer);
				  unpublish(answer.instant_article);
				}
			);
		}
	);
}
function unpublish(instant_article){
	console.log(instant_article,' in instant articles deletion');
	setTimeout(function(){
		FB.api(
			'/eldeforma/instant_articles',
			'POST',
			{"html_source": instant_article.html_source,'published':false,'development_mode':false},
			function(result) {
			  console.log('unpublished ',result.canonical_url);
			}		
		);
	},10000);
}
function getPlacements(){
	var placements = [];
	if(document.getElementById('desktop_placement').checked){
		placements.push('desktop');
	}
	if(document.getElementById('mobile_placement').checked){
		placements.push('mobile');
	}
	if(placements.length == 0){
		placements = ['desktop','mobile'];
	}
	return placements;
}
function stopCampaign(post_id,campaign){
	updateStoppedCampaign(post_id);
	/*$.ajax({
		url: 'php_scripts/get_campaign.php',
		method: 'GET',
		async: false,
		dataType: 'json',
		data: {post_id: post_id},
		success: function(response){
			console.log(response);
			/*if(response.ad_account == 'act_31492866'){
				$.post(
					"https://hooks.slack.com/services/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",
					JSON.stringify({
						"channel": '#implementar_comercial',
						"username": 'Alerta de Campaña',
						"attachments": [{
							"fallback": 'Campaña interrumpida',
							"color": "red",
							"author_name": 'Campaign Bot',
							"title": 'Campaña interrumpida',
							"text": 'La campaña para: '+response.name+'\nse ha interrumpido\n'
						}]
					})
				);
			}*/
			FB.api(
				'/'+campaign,
				'POST',
				{status: 'PAUSED'},
				function(response) {
					if(!response.hasOwnProperty('error')){
						console.log(response);
						alert('Campaign successfully paused!');
						loadKomfo();
					}else{
						alert(response.error.message);
					}
				}
			);
		/*}
	});*/
}
function getExcludedDate(){
	var now = new Date().getDate();
	switch(true){
		case now<=5:
			return 1;
			break;
		case now<=10:
			return 2;
			break;
		case now<=15:
			return 3;
			break;
		case now<=20:
			return 4;
			break;
		case now<=25:
			return 5;
			break;
		case now<=31:
			return 6;
			break;
	}
}
function getObjective(id){
	var selector_objective = document.getElementById("objective");
	var selector_billing = document.getElementById("billing_event");
	var selector_optimization = document.getElementById("optimization");
	if(isVideo(id)){
		selector_objective.selectedIndex = 2;
		selector_billing.selectedIndex = 2;
		selector_optimization.selectedIndex = 2;
		document.getElementById('radio_GCS_None').checked = true;
		$('#gcs_group')[0].style.visibility = 'hidden';
	}else{
		selector_objective.selectedIndex = 0;
		selector_billing.selectedIndex = 0;
		selector_optimization.selectedIndex = 0;
	}
	console.log('In get Objective',id);
	adAccountSelected(id);
}
function updatePromotedPosts(){
	$.ajax({
		url: 'php_scripts/update_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			campaign_id: promoteParameters.adSet.campaign_id,
			post_id: promoteParameters.adCreative.object_story_id,
		},
		success: function(response){
			console.log(response);
		}
	});
	if($('input[name=radioLimit]:checked', '#form').val() == 'spend_limit' || $('input[name=radioLimit]:checked', '#form').val() == 'goal'){
		$.ajax({
			url: 'php_scripts/update_campaign_goal.php',
			method: 'POST',
			async: false,
			dataType: 'json',
			data: {
				post_id: promoteParameters.adCreative.object_story_id,
				type: $('input[name=radioLimit]:checked', '#form').val(),
				value: parseInt($('#limit_value')[0].value)
			},
			success: function(response){
				console.log(response);
			}
		});
	}
}
function updateStoppedCampaign(id){
	$.ajax({
		url: 'php_scripts/update_stopped_campaign.php',
		method: 'POST',
		async: false,
		dataType: 'json',
		data: {
			post_id: id
		},
		success: function(response){
			console.log(response);
		}
	});
}
function isVideo(id){
	var	flag;
	$.ajax({
		url: 'https://graph.facebook.com/v2.10/'+id+'?fields=type'+"&"+access_token,
		async: false,
		method: 'get',
		success: function(response){
			if(response.type == 'video'){
				flag = true;
			}else{
				flag = false;
			}
		}
	});
	return flag;
}
function changeObjective(){
	var index = document.getElementById('objective').selectedIndex;
	document.getElementById('billing_event').selectedIndex = index;
	document.getElementById('optimization').selectedIndex = index;
}
function getCountries(){
    var countries=[];
    if($('#mexico')[0].checked){
       countries.push('MX');
    }
    if($('#latam')[0].checked){
        countries.push('BZ', 'PA', 'AR', 'CL', 'PE', 'CO', 'EC', 'BO', 'GT', 'CR', 'SV', 'HN', 'NI', 'PY', 'UY');
    }
    if(countries.length == 0){
        return ['MX'];
    }
    else{
        return countries;
    }
}
//One click promote

//Edit Promote
var campaign_to_edit;
var editAdSet = {};
var editAdAd = {};
var editPromoteParameters = {editAdSet: {'targeting':{}}, editAdAd:{}};
function getCurrentCampaignParameters(campaign){
    FB.api(
        '/'+campaign,
        'GET',
        {"fields":"adsets.fields(daily_budget,bid_amount,targeting,excluded_custom_audiences),ads.fields(name,creative.fields(effective_object_story_id)),account_id"},
        function(response){
            campaign_to_edit=response;
            $('#edit-ad-name').attr("placeholder", response.ads.data[0].name);
            $('#edit-budget').attr("placeholder", parseFloat(response.adsets.data[0].daily_budget)/100);
            $('#edit-bid').attr("placeholder", response.adsets.data[0].bid_amount/100);
            $('#edit-min-age').attr("placeholder", response.adsets.data[0].targeting.age_min);
            $('#edit-max-age').attr("placeholder", response.adsets.data[0].targeting.age_max);
            var selector=document.getElementById("edit-exclude");
            while(selector.length>0){
                selector.remove(0);
            }
            switch(response.account_id){
                case '986080914767243':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                case '31492866':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                case '958749654167036':
                    var audiences = isVideo(response.ads.data[0].creative.effective_object_story_id) ? [] : [{name: 'El deforma 5 days', id: '6041870320076'},{name: 'El deforma 10 days', id: '6041870300476'},{name: 'El deforma 15 days',id: '6041870289476'},{name: 'El deforma 20 days', id: '6041870274476'},{name: 'El deforma 25 days', id: '6041870248876'},{name: 'El deforma 30 days', id: '6041870223276'}]
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    for(var j=0;j<audiences.length;j++)
                    {
                        var option = document.createElement("option");
                        option.text = audiences[j].name;
                        option.value = audiences[j].id; 
                        selector.add(option);
                    }
                    break;
                default:
                    var audiences = [];
                    var option = document.createElement("option");
                    option.text = "";
                    option.value = ""; 
                    selector.add(option);
                    break;
            }
            var selector=document.getElementById("edit-exclude");
            if(typeof response.adsets.data[0].targeting.excluded_custom_audiences !== 'undefined'){
                $('#edit-exclude').val(response.adsets.data[0].targeting.excluded_custom_audiences[0].id);    
            }else{
                $('#edit-exclude').val('');
            }
            
            if(typeof response.adsets.data[0].targeting.connections !== 'undefined'){
                $('#edit-connections').val(response.adsets.data[0].targeting.connections[0].id);
            }else{
                $('#edit-connections').val(null);
            }
            
            if(typeof response.adsets.data[0].targeting.excluded_connections !== 'undefined'){
                $('#edit-excluded-connections').val(response.adsets.data[0].targeting.excluded_connections[0].id);
            }else{
                $('#edit-excluded-connections').val(null);
            }
            
            $('#edit-genders').prop('selectedIndex',typeof response.adsets.data[0].targeting.genders === 'undefined' ? 0 : response.adsets.data[0].targeting.genders[0]);
            
            if($.inArray('desktop', response.adsets.data[0].targeting.device_platforms) > -1){
                $('#edit-desktop-placement')[0].checked = true;
            }else{
                $('#edit-desktop-placement')[0].checked = false;
            }
        
            if($.inArray('mobile', response.adsets.data[0].targeting.device_platforms) > -1){
                $('#edit-mobile-placement')[0].checked = true;
            }else{
                $('#edit-mobile-placement')[0].checked = false;
            }
            
            if(typeof response.adsets.data[0].targeting.device_platforms === 'undefined'){
                $('#edit-mobile-placement')[0].checked = true;
                $('#edit-desktop-placement')[0].checked = true;
            }
            
            if($.inArray('MX', response.adsets.data[0].targeting.geo_locations.countries) > -1){
                $('#edit-mexico')[0].checked = true;
            }else{
                $('#edit-mexico')[0].checked = false;
            }
            
            if($.inArray('BZ', response.adsets.data[0].targeting.geo_locations.countries) > -1){
                $('#edit-latam')[0].checked = true;
            }else{
                $('#edit-latam')[0].checked = false;
            }
        }
    );
}
function setEditPromoteParameters(){
    var ad_change_flag = false;
    var adset_change_flag = false;
    editPromoteParameters = {editAdSet: {'targeting':{}}, editAdAd:{}};
    
    if($('#edit-bid').val() !== $('#edit-bid').attr('placeholder') && $('#edit-bid').val() !== ''){
        editPromoteParameters.editAdSet.bid_amount = parseFloat(document.getElementById('edit-bid').value)*100;
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.bid_amount = campaign_to_edit.adsets.data[0].bid_amount;
    }
    if($('#edit-budget').val() !== $('#edit-budget').attr('placeholder') && $('#edit-budget').val() !== ''){
        editPromoteParameters.editAdSet.daily_budget = parseFloat(document.getElementById('edit-budget').value)*100;
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.daily_budget = campaign_to_edit.adsets.data[0].daily_budget;
    }
    if($('#edit-min-age').val() !== $('#edit-min-age').attr('placeholder') && $('#edit-min-age').val() !== ''){
       editPromoteParameters.editAdSet.targeting.age_min = parseInt(document.getElementById('edit-min-age').value);
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.targeting.age_min = campaign_to_edit.adsets.data[0].targeting.age_min;
    }
    if($('#edit-max-age').val() !== $('#edit-max-age').attr('placeholder') && $('#edit-max-age').val() !== ''){
       editPromoteParameters.editAdSet.targeting.age_max = parseInt(document.getElementById('edit-max-age').value);
        adset_change_flag = true;
    }
    else{
        editPromoteParameters.editAdSet.targeting.age_max = campaign_to_edit.adsets.data[0].targeting.age_max;
    }
       
    if(typeof campaign_to_edit.adsets.data[0].targeting.genders === 'undefined'){
        if($('#edit-genders').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.genders = [$('#edit-genders').prop('selectedIndex')];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-genders').prop('selectedIndex') != campaign_to_edit.adsets.data[0].targeting.genders){
            if($('#edit-genders').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.genders = [$('#edit-genders').prop('selectedIndex')];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.genders = [];
                adset_change_flag = true;
            }
        }
    }
        
    if(typeof campaign_to_edit.adsets.data[0].targeting.excluded_custom_audiences === 'undefined'){
        if($('#edit-exclude').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [{id: $('#edit-exclude').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-exclude').val() != campaign_to_edit.adsets.data[0].targeting.excluded_custom_audiences[0].id){
            if($('#edit-exclude').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [{id: $('#edit-exclude').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.excluded_custom_audiences = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.connections === 'undefined'){
        if($('#edit-connections').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.connections = [{id: $('#edit-connections').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-connections').val() != campaign_to_edit.adsets.data[0].targeting.connections[0].id){
            if($('#edit-connections').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.connections = [{id: $('#edit-connections').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.connections = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.excluded_connections === 'undefined'){
        if($('#edit-excluded-connections').prop('selectedIndex') != 0){
            editPromoteParameters.editAdSet.targeting.excluded_connections = [{id: $('#edit-excluded-connections').val()}];
            adset_change_flag = true;
        }
    }
    else{
        if($('#edit-excluded-connections').val() != campaign_to_edit.adsets.data[0].targeting.excluded_connections[0].id){
            if($('#edit-excluded-connections').prop('selectedIndex') != 0){
                editPromoteParameters.editAdSet.targeting.excluded_connections = [{id: $('#edit-excluded-connections').val()}];
                adset_change_flag = true;
            }else{
                editPromoteParameters.editAdSet.targeting.excluded_connections = [];
                adset_change_flag = true;
            }
        }
    }
    
    if(typeof campaign_to_edit.adsets.data[0].targeting.device_platforms !== 'undefined'){
        
    }
    else{
        editPromoteParameters.editAdSet.targeting.device_platforms = getEditPlacements();
        editPromoteParameters.editAdSet.targeting.publisher_platforms = ['facebook'];
        editPromoteParameters.editAdSet.targeting.facebook_positions = ['feed'];
        adset_change_flag = true;
    }
    
    if($('#edit-ad-name').val() !== $('#edit-ad-name').attr('placeholder') && $('#edit-ad-name').val() !== ''){
        editPromoteParameters.editAdAd.name = $('#edit-ad-name').val();   
        ad_change_flag = true;
    }
    
    var edit_countries=[];
    if($('#edit-mexico')[0].checked){
       edit_countries.push('MX');
    }
    if($('#edit-latam')[0].checked){
        edit_countries.push('BZ', 'PA', 'AR', 'CL', 'PE', 'CO', 'EC', 'BO', 'GT', 'CR', 'SV', 'HN', 'NI', 'PY', 'UY');
    }
    if(edit_countries.length == 0){
        edit_countries.push('MX');
    }
    if(campaign_to_edit.adsets.data[0].targeting.geo_locations.countries.length != edit_countries.length){
        if($('#leave-targeting')[0].checked){
           editPromoteParameters.editAdSet.targeting.geo_locations = {countries : campaign_to_edit.adsets.data[0].targeting.geo_locations.countries};
        }
        else{
            editPromoteParameters.editAdSet.targeting.geo_locations = {countries : edit_countries};
        }
        adset_change_flag = true;
    }
    
    if(adset_change_flag){
        if($.isEmptyObject(editPromoteParameters.editAdSet.targeting)){
            delete editPromoteParameters.editAdSet.targeting;
        }
        else{
            editPromoteParameters.editAdSet.targeting.geo_locations = {countries : edit_countries};
            editPromoteParameters.editAdSet.targeting.publisher_platforms = ['facebook'];
            editPromoteParameters.editAdSet.targeting.facebook_positions = ['feed'];
        }
        FB.api(
            '/'+campaign_to_edit.adsets.data[0].id,
            'POST',
            editPromoteParameters.editAdSet,
            function(response) {
                console.log(response);
                if(response.success){
                    alert('Adset updated succesfully');
                    resetEditForm();
                    editPromoteParameters.editAdSet = {'targeting':{}};
                }else{
                    alert('Adset update failed');
                }
            }
        );
    }
    
    if(ad_change_flag){
        FB.api(
            '/'+campaign_to_edit.ads.data[0].id,
            'POST',
            editPromoteParameters.editAdAd,
            function(response) {
                console.log(response);
                if(response.success){
                    alert('Ad updated succesfully');
                    resetEditForm();
                    editPromoteParameters.editAdAd = {};
                }else{
                    alert('Ad update failed');
                }
            }
        );
    }
}
function getEditPlacements(){
	var placements = [];
    if($('#edit-mobile-placement')[0].checked){
		placements.push('mobile');
	}
	if($('#edit-desktop-placement')[0].checked){
		placements.push('desktop');
	}
	if(placements.length == 0){
		placements = ['mobile','desktop'];
	}
	return placements;
}
function resetEditForm(){
	document.getElementById('edit-ad-name').value = '';
	document.getElementById('edit-budget').value = '';
	document.getElementById('edit-bid').value = '';
	document.getElementById('edit-min-age').value = '';
	document.getElementById('edit-max-age').value = '';
	
//	$('#bid_label')[0].innerHTML = 'Bid (USD)';
//	$('#budget_label')[0].innerHTML = 'Budget (USD)';
//	var budget = $('#budget').parsley();
//	budget.options.range = [20,2000];
//	var bid = $('#bid').parsley();
//	bid.options.range = [0.01,0.05];
}
function hideTargeting(){
    if($('#leave-targeting')[0].checked){
        $('#edit-locations').hide();
    }else{
        $('#edit-locations').show();
    }
}
//Edit Promote
