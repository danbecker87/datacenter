var Authors:{
    Author Schema:{
        nickname: '',
        id: 0,
        wp_posts:[
            {
                id: 0,
                url: '',
                date: '',
                pageviews: 0
            }
        ],
        fb_posts:[
            {
                id: '',
                link: '',
                time_created: '',
                pageviews: 0,
                likes: 0,
                shares: 0,
                comments: 0,
                link_clicks: 0
            }
        ],
        dates:{
           DateSchema: {
                wp_posts: 0,
                fb_posts: 0,
                likes: 0,
                pageviews: 0,
                link_clicks: 0,
                shares: 0,
                comments: 0
            }
        },
        likes: 0,
        shares: 0,
        comments: 0,
        pageviews: 0,
        count_fb_posts: 0,
        count_wp_posts: 0,
    }
}

var Videos:{
    [
        {
            id: '',
            time_created: '',
            video_views: 0,
            retention_graph: [],
            views_to_25: 0,
            views_to_50: 0,
            views_to_75: 0,
            views_to_100: 0,
            likes: 0,
            shares: 0,
            comments: 0
        }
    ]
}

var Selesmen
    [
        'persons series vs year',
        'persons series vs q',
        'persons series vs month',
        'agencies series vs year',
        'agencies series vs q',
        'agencies series vs month',
        'clients series vs year',
        'clients series vs q',
        'clients series vs month',
        'agency bar chart',
        'client bar chart',
        'person bar chart per q',
        'person bar chart per year',
        'person bar chart per month',
        'delta persons vs month',
        'delta persons vs q',
        'delta persons vs year',
        'delta agencies vs q',
        'stacked bar person composed of client investment per {year, q}',
        'paymnets a {month, q, year}',
        'sales a {month, q, year}'
    ]
}