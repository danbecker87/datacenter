<?
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: https://$host$path/$file");
	exit;	
}

if(isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='fb'){
    redirect("reporte.php");
    exit();
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='deforma') || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
	redirect("login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Open Data Center Dashboard">
        <meta name="Dan Becker" content="Deforma Open Data Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Open Responsive Data Center - Start</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script type="text/javascript" src="js/main_js.js"></script>
    
    <!--Google Flow-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var me={'id':'eldeforma','innerHTML':'El Deforma'};
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		authorizeButton.style.visibility = 'hidden';
	  } else {
		authorizeButton.style.visibility = 'visible';
		authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var access_token;
    var version = '2.12';
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {
			var cutoffTime=new Date();
			if(cutoffTime.getHours()==11 && cutoffTime.getMinutes()>=30 && cutoffTime.getMinutes()<=40){
				//makeApiCall();
			}		
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->
    
        <!-- Begin page -->
        <div id="wrapper">
        
        	<!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Data Center</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <!--<div class="col-lg-4 col-md-4">
                        		<div class="card-box">
                        			<h4 class="header-title m-t-0 m-b-10">Active Users</h4>
                                    <div class="widget-box-2">
                                        <div class="widget-detail-2">
                                            <span class="badge badge-success pull-left m-t-20" id="active_users_badge"><span id="active_users_change">-</span><i class="zmdi zmdi-trending-up" id="active_users_trend"></i> </span>
                                            <h2 class="m-b-0" id="active_users" style='font-size: 60px'> - </h2>
                                            <p class="text-muted m-b-5">Active Users</p>
                                        </div>
                                    </div>
                        		</div>
                            </div>--><!-- end col --><!-- Active Users -->
                            
                            <div class="col-lg-4 col-md-4">
                        		<div class="card-box">
                        			<h4 class="header-title m-t-0 m-b-10">Organic Users</h4>
                                    <div class="widget-box-2">
                                        <div class="widget-detail-2">
                                            <!--<div class='col-md-6 col-lg-6'>-->
                                            	<div class='row'>
													<span class="badge badge-success pull-left m-t-20" id="active_users_organic_badge"><span id="active_users_organic_change">-</span><i class="zmdi zmdi-trending-up" id="active_users_organic_trend"></i> </span>
													<h2 class="m-b-0 pull-left" id="active_users_organic" style='font-size: 60px'> - </h2></br>
												</div>
												<!--<div class='row'>
													<p class="text-muted m-b-5">Organic Active Users</p>
												</div>-->
                                            <!--</div>-->
                                            <!--<div class='col-md-6 col-lg-6'>-->
                                            	<!--<div class='row'>
													<span class="badge badge-success pull-left m-t-20" id="active_users_paid_badge"><span id="active_users_paid_change">-</span><i class="zmdi zmdi-trending-up" id="active_users_paid_trend"></i> </span>
													<h2 class="m-b-0 pull-left" id="active_users_paid" style='font-size: 60px'> - </h2></br>
												</div>
												<div class='row'>
													<p class="text-muted m-b-5">Paid Active Users</p>
												</div>-->
                                            <!--</div>-->
                                        </div>
                                    </div>
                        		</div>
                            </div><!-- end col --><!-- Organic-Paid Active Users -->
                            
							<div class="col-lg-4 col-md-4">
                        		<div class="card-box">

                        			<h4 class="header-title m-t-0 m-b-10">Video Views Diarios</h4>

                                    <div class="widget-box-2">
                                        <div class="widget-detail-2">
                                            <h2 class="m-b-0"><span id="daily_video_views_amount" style='font-size: 60px'></span> </h2>
                                            <p class="text-muted m-b-5" id="daily_video_views_page">Page</p>
                                        </div>
                                    </div>
                        		</div>
                            </div><!-- end col --><!-- Daily Video Views -->   
                            <div class="col-lg-4 col-md-4">
                        		<button class='btn btn-success' onclick='startActiveUsers()'>REFRESH</button>
                            </div><!-- end col --><!-- Big Refresh Button -->
                                                          
                        </div>
                        <!-- end row -->                       

                        <div class="row">
                            <div class="col-lg-5">
                                <div class="card-box">

                        			<h4 class="header-title m-t-0 m-b-10">Active Sites</h4>

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Path</th>
                                                <th>Active Users</th>
                                            </tr>
                                            </thead>
                                            <tbody id="active_table_rows">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class='row'>
									<div class="user-box col-lg-3" id='box_0'>
										<div class="user-img">
											<img id='no0' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive">
											<div class="user-status offline"><i class="zmdi zmdi-star" style='color: #FFD700; font-size: 30px;'></i></div>
										</div>
									</div>
									<div class="user-box col-lg-3" id='box_1'>
										<div class="user-img">
											<img id='no1' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive">
											<div class="user-status offline"><i class="zmdi zmdi-cocktail" style='color: #C0C0C0; font-size: 30px;'></i></div>
										</div>
									</div>
									<div class="user-box col-lg-3" id='box_2'>
										<div class="user-img">
											<img id='no2' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive">
											<div class="user-status offline"><i class="zmdi zmdi-pizza" style='color: #CD7F32; font-size: 30px;'></i></div>
										</div>
									</div>
									<div class="user-box col-lg-3" id='box_3'>
										<div class="user-img">
											<img id='no3' src='/assets/images/users/avatar-1.jpg' alt="user-img" title="El Deforma" class="img-circle img-thumbnail img-responsive">
											<div class="user-status offline"><i class="zmdi zmdi-thumb-up" style='color: #000000; font-size: 30px;'></i></div>
										</div>
									</div>
								</div>
                            </div><!-- end col --><!-- Top Sites -->
                            
                            <div class="col-lg-2">
                                <div class="card-box">
                        			<h4 class="header-title m-t-0 m-b-10">Twitter</h4>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Trending Topics Mexico</th>
                                            </tr>
                                            </thead>
                                            <tbody id="twitter_table_rows">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- end col --><!-- Twitter Trends -->
                            
                            <div class='col-lg-5 col-md-5' >
								<div class='card-box'>
									<div id='chart_div_pageviews' style='height: 250px'>
									</div>
								</div>

								<div class='card-box'>
									<div id='chart_div_users' style='height: 250px'>
									</div>
								</div>
							</div><!-- end col --><!-- Ananlytics Graphs -->
                        </div><!-- end row -->
                        <button id='authorize-button'>Authorize</button>
                    </div> <!-- container --> 
                                       
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        <!-- Numerator animation -->
		<script type="text/javascript" src="js/jquery-numerator.js"></script>
        
        <script>
        	jq1102 = jQuery.noConflict( true );
		</script>
        
        <script>
			//interval calls
			var initialCallLive = setTimeout(function(){
				startActiveUsers();
				getTwitterTrends();
				dailyVideoViews();
			},3000);//initial call to getActiveUsers
			
			
			function startActiveUsers(){
				getActiveUsersOrganic();
				getActivePageviewsOrganic();
				getActiveUsersTodayOrganic();
			}
			
			
			var refreshDailyVideoViews = setInterval(function(){
				dailyVideoViews();
			},1000*60*5);//recurring call to getFBSpend

			var refreshLive = setInterval(function(){
				getTwitterTrends();
				monthVideoViews(me);
			},1000*60*10);//recurring call to getActiveUsers
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
			},1000*60*30);
			getActivePage();
        </script>
    </body>
</html>