<?php
session_start();

function redirect($file){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file");
	exit;	
}

if(isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin' || isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin_jr'){
	
}else{
	if(isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true){
		if(isset($_SESSION['admin']) && $_SESSION['admin']==true){
		
			}else{
				redirect("login.php");
			}
	}
	else{
		redirect("login.php");
		exit();
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Invoicing Manuals</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

		 <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Leaderboard</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box" style='height: 90vh'>
                                	<select id='pdf_selector' onchange='renderSelectedPDF()'></select>
                                	<iframe id='pdf_viewer' src="" style="width:100%; height:100%;" frameborder="0"></iframe>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		
		<!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <script src="https://apis.google.com/js/client.js"></script><!--Google API library-->
        
        <script>
        $("input[name='weeks']").TouchSpin({
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary"
		});
        </script>
        
        </script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script>
			jq1102 = jQuery.noConflict( true );
			jq1102(function() {
				jq1102( "#from" ).datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				jq1102( "#to" ).datepicker({
					defaultDate: "+1w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						jq1102( "#from" ).datepicker( "option", "maxDate", selectedDate );
						//console.log($('#refresh_btn'));
						$('#refresh_btn')[0].className = "btn btn-success btn-rounded w-md waves-effect waves-light";
						$('#refresh_btn')[0].onclick = function(){getArticlesWPSelect()};
						getArticlesWPSelect();
					}
				});
				jq1102( "#pay_date" ).datepicker({
					defaultDate: "-4w",
					dateFormat: "yy-mm-dd",
					changeMonth: true,
					numberOfMonths: 1,
					onClose: function( selectedDate ) {
						
					}
				});
			});
			jq1102(function() {
				jq1102( "#month" ).datepicker({
					defaultDate: 0,
					dateFormat: "yy-mm",
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					onChangeMonthYear: function(year, month, widget) {
						setTimeout(function() {
						   jq1102('.ui-datepicker-calendar').hide();
						});
					},
					onClose: function( dateText, inst ) {
						jq1102(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
						//getArticlesWP( $("#month")[0].value );
					}
				}).click(function(){
					jq1102('.ui-datepicker-calendar').hide();
				});
			});
			jq1102(function() {
				jq1102( "#pay_month" ).datepicker({
					defaultDate: "-1m",
					dateFormat: "yy-mm",
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					numberOfMonths: 1,
					onChangeMonthYear: function(year, month, widget) {
						setTimeout(function() {
						   jq1102('.ui-datepicker-calendar').hide();
						});
					},
					onClose: function( dateText, inst  ) {
						jq1102(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
					}
				}).click(function(){
					jq1102('.ui-datepicker-calendar').hide();
				});	
			});
			jq1102(function() {
				jq1102( "#bonus_month" ).datepicker({
					defaultDate: 0,
					dateFormat: "yy-mm",
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					onChangeMonthYear: function(year, month, widget) {
						setTimeout(function() {
						   jq1102('.ui-datepicker-calendar').hide();
						});
					},
					onClose: function( dateText, inst ) {
						jq1102(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
						//getArticlesWPBonus( $("#bonus_month")[0].value );
					}
				}).click(function(){
					jq1102('.ui-datepicker-calendar').hide();
				});
			});
			//$('.ui-datepicker-calendar')[0].css('display', 'none');
			
			//interval calls
			var initialCalls = setTimeout(function(){
				$.ajax({
					url: 'php_scripts/get_manuals.php',
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response){
						var selector=document.getElementById('pdf_selector');
						while(selector.length>0){
							selector.remove(0);
						}
						var empty = document.createElement("option");
						empty.text = '';
						empty.value = ''; 
						selector.add(empty);
						for(var i=2;i<response.length;i++){
							var option = document.createElement("option");
							option.text = response[i];
							option.value = response[i]; 
							selector.add(option);						
						}
					}
				})
			},1000);
			
			var refreshData = setInterval(function(){
				
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				//handleClientLoad();
				//checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>