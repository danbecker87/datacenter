<!DOCTYPE html>
<html>
<head>
    <title>Deforma Facebook Query</title>
    <meta charset="UTF-8">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="responsive_data_center/js/math.min.js"></script>

    <link rel="shortcut icon" href="http://deforma2.com/responsive_data_center/assets/images/users/logodeforma_favicon.png">
    
</head>
<body>
    <script>
        var mssgURL="https://hooks.slack.com/services/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";
        
        $.post(
            "https://hooks.slack.com/services/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",
            JSON.stringify({
                "channel": '@dan',
                "username": 'Buttons',
                "text": "Press me?",
                "attachments": [
                    {
                        "text": "Choose a game to play",
                        "fallback": "You are unable to choose a game",
                        "callback_id": "wopr_game",
                        "color": "#3AA3E3",
                        "attachment_type": "default",
                        "actions": [
                            {
                                "name": "game",
                                "text": "Chess",
                                "type": "button",
                                "value": "chess"
                            },
                            {
                                "name": "game",
                                "text": "Falken's Maze",
                                "type": "button",
                                "value": "maze"
                            },
                            {
                                "name": "game",
                                "text": "Thermonuclear War",
                                "style": "danger",
                                "type": "button",
                                "value": "war",
                                "confirm": {
                                    "title": "Are you sure?",
                                    "text": "Wouldn't you prefer a good game of chess?",
                                    "ok_text": "Yes",
                                    "dismiss_text": "No"
                                }
                            }
                        ]
                    }
                ]
            })
        );
    </script>
</body>
</html>