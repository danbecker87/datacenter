<?
 	session_start();//enable sessions
	
	function redirect($file)
	{
		$host=$_SERVER['HTTP_HOST'];
		$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
		return header("Location: https://$host$path/$file");
		exit;	
	}

	$_SESSION['authenticated']=false;
	unset($_SESSION['admin']);
		
	setcookie('user_id',time()-3600) ;
	
	redirect("login.php");
	exit();
?>