<?php

// Load the Google API PHP Client Library.
require_once __DIR__ . '/vendor/autoload.php';

use DateTime;
use DateTimeZone;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\Dfp\DfpServices;
use Google\AdsApi\Dfp\DfpSession;
use Google\AdsApi\Dfp\DfpSessionBuilder;
use Google\AdsApi\Dfp\Util\v201805\ReportDownloader;
use Google\AdsApi\Dfp\Util\v201805\StatementBuilder;
use Google\AdsApi\Dfp\v201805\ExportFormat;
use Google\AdsApi\Dfp\v201805\ReportJob;
use Google\AdsApi\Dfp\v201805\ReportQueryAdUnitView;
use Google\AdsApi\Dfp\v201805\ReportService;
use UnexpectedValueException;
use Google\AdsApi\Dfp\Util\v201805\DfpDateTimes;
use Google\AdsApi\Dfp\v201805\Column;
use Google\AdsApi\Dfp\v201805\DateRangeType;
use Google\AdsApi\Dfp\v201805\Dimension;
use Google\AdsApi\Dfp\v201805\DimensionAttribute;
use Google\AdsApi\Dfp\v201805\ReportQuery;

class RunSavedQuery
{
    public static function runExampleQuery(
        DfpServices $dfpServices,
        DfpSession $session
    ) {
        $reportService = $dfpServices->get($session, ReportService::class);
        $reportQuery = new ReportQuery();
        $reportQuery->setDimensions(
          [
              Dimension::AD_EXCHANGE_TRANSACTION_TYPE,
              Dimension::DATE
          ]
        );
        $reportQuery->setColumns(
          [
              Column::AD_EXCHANGE_ESTIMATED_REVENUE
          ]
        );

        // Create statement to filter for an order.
        $statementBuilder = (new StatementBuilder());

        // Set the filter statement.
        $reportQuery->setStatement($statementBuilder->toStatement());

        // Set the start and end dates or choose a dynamic date range type.
        $reportQuery->setDateRangeType(DateRangeType::LAST_MONTH);
        /*$reportQuery->setDateRangeType(DateRangeType::CUSTOM_DATE);
        $reportQuery->setStartDate(
          DfpDateTimes::fromDateTime(
              new DateTime(
                  date("Y-m-d", strtotime("first day of previous month")),
                  new DateTimeZone('America/New_York')
              )
          )
              ->getDate()
        );
        $reportQuery->setEndDate(
          DfpDateTimes::fromDateTime(
              new DateTime(
                  date("Y-m-d", strtotime("last day of previous month")),
                  new DateTimeZone('America/New_York')
              )
          )
              ->getDate()
        );*/
        
        $reportJob = new ReportJob();
        $reportJob->setReportQuery($reportQuery);
        $reportJob = $reportService->runReportJob($reportJob);
        
        $reportDownloader = new ReportDownloader($reportService, $reportJob->getId());
        if ($reportDownloader->waitForReportToFinish()) {
            // Write to system temp directory by default.
            //$filePath = sprintf(
              //'%s.csv.gz',
              //tempnam('.', 'deals-report-')
            //);
            $filePath = 'ADX_data/adx_deals.csv.gz';
            //printf("Downloading report to %s ...%s", $filePath, PHP_EOL);
            $reportDownloader->downloadReport(ExportFormat::CSV_DUMP, $filePath);
            //print "done.\n";
        } else {
          print "Report failed.\n";
        }
        
        // Raising this value may increase performance
        $buffer_size = 4096; // read 4kb at a time
        $out_file_name = 'ADX_data/adx_deals.csv';

        // Open our files (in binary mode)
        $file = gzopen($filePath, 'rb');
        $out_file = fopen($out_file_name, 'wb'); 
        while (!gzeof($file)) {
            fwrite($out_file, gzread($file, $buffer_size));
        }
        fclose($out_file);
        gzclose($file);
        
        //Read out_file csv
        if (($handle = fopen($out_file_name, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $deals[]=$data;
            }
            fclose($handle);
            //print("<pre>".print_r($deals,true)."</pre>");
            echo json_encode($deals,true);
        }else{
            echo 'couldnt open csv';
        }
        
    }
    public static function main()
    {
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()
            ->build();
        $session = (new DfpSessionBuilder())->fromFile()
            ->withOAuth2Credential($oAuth2Credential)
            ->build();
        echo 'hi im authenticated';
        //self::runExampleQuery(new DfpServices(), $session);
    }
}
RunSavedQuery::main();
?>