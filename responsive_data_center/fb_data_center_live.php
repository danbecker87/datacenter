<?
session_start();

function redirect($file,$redirect_after_login){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file?red=$redirect_after_login");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
    $tokens = ( explode ( '/', $_SERVER['PHP_SELF'] ));
	redirect("login.php",$tokens[count($tokens)-1]);
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js_live.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Responsive Data Center - Facebook Live</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

		<!-- Add to home screen for Safari on iOS -->
	    <meta name="apple-mobile-web-app-capable" content="no">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="apple-mobile-web-app-title" content="Deforma FB Datacenter">
	    <link rel="apple-touch-icon" href="assets/images/users/logodeforma_fb_favicon_192.png">
		
    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var version='2.12';
    var access_token;
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
    var pageTokens={};
	function buildURL(query,token,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&access_token="+token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token=response.authResponse.accessToken;
		if (response.status === 'connected') {
			getAccessTokens();
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
        
    function getAccessTokens(){
        FB.api(
            '/me/accounts',
            'GET',
            {},
            function(response) {
                for(var i=0;i<response.data.length;i++){
                    pageTokens[response.data[i].name] = response.data[i].access_token;
                    pageTokens[response.data[i].id] = response.data[i].access_token;
                }
                loadKomfo();
            }
        );
    }

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page" style='margin-left: 0px'>
                <!-- Start content -->
                <div class="content" style='margin-top: 5px'>
                    <div class="container">
                    
                    	<div class='row'>    
							<div class="col-lg-12 col-md-12">
                        		<div class="panel panel-color panel-tabs panel-purple">
                        		
                        			<!-- Modal Promote -->
                                    <?php require 'views/modal-promote.html'; ?>
                                    
                                    <!-- Modal edit Promote -->
                                    <?php require 'views/modal-edit.html'; ?>
                                    
                                    <!-- Modal Quickview -->
                                    <?php require 'views/modal-quickview.html'; ?>
                                    
                                    <!-- Modal Cahnge Goal -->
                                    <?php require 'views/modal-goalchange.html'; ?>
                                    
                                    <!-- Modal Change Name -->
                                    <?php require 'views/modal-namechange.html'; ?>
                        		
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-deforma" data-toggle="tab" aria-expanded="true" onClick="loadKomfo()">El Deforma</a>
											</li>
                                            <li class="">
												<a href="#navpills-comercial" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_comercial()">Comercial</a>
											</li>
                                            <li class="">
												<a href="#navpills-repsodia-videos" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_repsodia()">Repsodia Videos</a>
											</li>
											<li class="">
												<a href="#navpills-repsodia-posts" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_repsodiaPosts()">Repsodia Posts</a>
											</li>
											<li class="">
												<a href="#navpills-tech-videos" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_cult()">Tech Cult</a>
											</li>
											<li class="">
												<a href="#navpills-tech-posts" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_cultPosts()">Tech Cult Posts</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Better Komfo</h3>
                                    </div>
                                    <div class="panel-body">
                                        <a href="fb_data_center.php"><button class='btn btn-rounded btn-success'>Go Full Datacenter</button></a>
										<div class="tab-content">
											<div id="navpills-deforma" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo()">Refresh</button>
                                                        <div id="komfo_table_deforma" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
                                            <div id="navpills-comercial" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_comercial()">Refresh</button>
														<div id="komfo_table_comercial" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-repsodia-videos" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_repsodia()">Refresh</button>
														<div id="komfo_table_repsodia" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-repsodia-posts" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_repsodiaPosts()">Refresh</button>
                                                        <div id="komfo_table_repsodia_posts" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-tech-videos" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_cult()">Refresh</button>
                                                        <div id="komfo_table_cult" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-tech-posts" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_cultPosts()">Refresh</button>
                                                        <div id="komfo_table_cult_posts" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col --><!-- Better Komfo -->
						</div>

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        
        <!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

		<!-- Validation js (Parsleyjs) -->
        <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <!-- Modal Behavior and validation -->
        <script type="text/javascript">
            //Change the action of forms on submit to call functions
			function validatePromote(){
                $('#form').parsley().validate();
            }
            $(document).ready(function() {
				$('#form').parsley();
			});
			$('#form').parsley().on('form:success', function() {
				getPromoteParameters();
			  $("#promote-modal").modal("hide");
			});
            
            function validateEditPromote(){
                $('#edit-form').parsley().validate();
            }
            $(document).ready(function() {
				$('#edit-form').parsley();
			});
			$('#edit-form').parsley().on('form:success', function() {
				setEditPromoteParameters();
			  $("#edit-promote-modal").modal("hide");
			});
		</script>
        
        <script>	
			//posts query
			</script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
        jq1102 = jQuery.noConflict( true );
        //posts query	
        var data_query=[];
        var metric_query=0;
        var formating_query;
        var post_id_query;
        var query_currency;
        var delta_link_clicks=[];
        var post;

        //promote timepicker datepicker
        $('#promote-timepicker').timepicker({
            defaultTime : new Date(),
            minuteStep : 5
        });
        $('#promote-datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(),
            format: 'yyyy/mm/dd'
        });
        //promote timepicker datepicker

        //interval calls
        var initialCalls = setTimeout(function(){
            //UpdatePostsDashboard();
        },3000);

        var refreshData = setInterval(function(){
            loadKomfo();
        },1000*60*10);


        var authGoogle = setInterval(function(){
            handleClientLoad();
            checkLoginState();
        },1000*60*30);
        //getActivePage();
</script>
<fb:login-button scope="ads_read,ads_management,pages_manage_instant_articles,publish_pages,manage_pages,business_management" onlogin="checkLoginState();">
</fb:login-button>
    </body>
</html>