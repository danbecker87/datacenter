<?
session_start();

function redirect($file,$redirect_after_login){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file?red=$redirect_after_login");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
    $tokens = ( explode ( '/', $_SERVER['PHP_SELF'] ));
	redirect("login.php",$tokens[count($tokens)-1]);
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/ventas_main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Ventas</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

		 <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        
        <!-- Treeview css -->
        <link href="assets/plugins/jstree/style.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
     <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart','controls']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	
    function makeApiCall() {
      var params = {

        spreadsheetId: '15-wy-vmUlDNK7aD8wYXMbuG0CnuxLLQbo1xT6NxE-So',
        range: 'B3:X600',
        valueRenderOption: 'UNFORMATTED_VALUE',  
        dateTimeRenderOption: 'FORMATTED_STRING',        
        majorDimension: 'ROWS'
      };

      var request = gapi.client.sheets.spreadsheets.values.get(params);
      request.then(function(response) {
        //console.log(response.result);
        handleVentas(response.result.values);
      }, function(reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function initClient() {
		//console.log('in init client');    
		var API_KEY = apiKey;  

		var CLIENT_ID = clientId;  

		var SCOPE = 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/gmail.compose';

		gapi.client.init({
			'apiKey': API_KEY,
			'clientId': CLIENT_ID,
			'scope': SCOPE,
			'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4', 'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'],
		}).then(function() {
			//console.log('In thenable client init');
			gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
			updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
		});
    }

    function handleClientLoad() {
      gapi.load('client:auth2', initClient);
    }

    function updateSignInStatus(isSignedIn) {
      if (isSignedIn) {
      	//console.log('making api call');
        makeApiCall();
      }else{
      	//console.log('Not signed in');
      	handleSignInClick();
      }
    }

    function handleSignInClick(event) {
      gapi.auth2.getAuthInstance().signIn();
    }

    function handleSignOutClick(event) {
      gapi.auth2.getAuthInstance().signOut();
    }
    </script><!--Google Auth Flow-->
    
    <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
    
   <!--<script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>--><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Ventas</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    
                                    <!-- Modal Send Emails -->
                        			<div id="mail-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h4 class="modal-title">Agencies emails</h4>
												</div>
												<div class="modal-body" id='emails-modal-body'>
												    
												</div>
												
												<div class="modal-footer">
													<button type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">Close</button>
												</div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal send Emails-->
                                    
                                	<div class='row'>
										<button class='btn btn-rounded btn-success' id='slack' style="display: none;" onclick='notifySlack()'>Send Slack</button>
                                        <button class='btn btn-rounded btn-danger' id="dickWithAgencies" data-toggle="modal" data-target="#mail-modal" style="display: none;" onclick='dickWithAgencies()'>Dick with agencies</button>
                                	</div>
                                    <div id='dashboard_div'>
                                        <div class='row'>
                                            <div id='filters_div'>
                                                <div class='col-lg-4'>
                                                </div>
                                                <div class='col-lg-4'>
                                                </div>
                                                <div class='col-lg-4'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div id='ventas_table'>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            Monto Total: <span id='monto_total'></span>
                                        </div>
                                        <div class='row' id='summary_rows'>
                                            <div class='col-lg-2'>
                                                <span id='status_div'></span><br>
                                                <span id='source_div'></span><br>
                                                Venta por estatus pago de rebates<br>
                                                <span id='rebate_status_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='agency_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='executive_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='year_div'></span><br>
                                                <span id='q_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='sellMonth_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='client_div'></span>
                                            </div>
                                        </div>
                                        <div class='row' style='height: 100px'></div>
                                        <div class='row'>
                                            Rebate Total: <span id='rebate_total'></span>
                                        </div>
                                        <div class='row' id='rebate_summary_rows'>
                                            <div class='col-lg-2'>
                                                <span id='rebate_status_div'></span><br>
                                                <span id='rebate_source_div'></span><br>
                                                Pago de rebates<br>
                                                <span id='rebate_status_status_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='rebate_agency_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='rebate_executive_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='rebate_year_div'></span><br>
                                                <span id='rebate_q_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='rebate_sellMonth_div'></span>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='rebate_client_div'></span>
                                            </div>
                                        </div>
                                        
                                        <div class='row'>
                                            Bonos
                                        </div>
                                        <div class='row' id='bonus_summary_rows'>
                                            <div class='col-lg-2'>
                                                <span id='bonus_year_div'></span><br>
                                            </div>
                                            <div class='col-lg-2'>
                                                <span id='bonus_q_div'></span>
                                            </div>
                                            <div class='col-lg-4'>
                                                <span id='bonus_executive_div'></span>
                                            </div>
                                            <div class='col-lg-4'>
                                                <span id='bonus_executive_q_div'></span>
                                            </div>
                                        </div>
                                        
                                         <div class='row' id='bonusTree'>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>
		
		<!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

         <!-- Tree view js -->
        <script src="assets/plugins/jstree/jstree.min.js"></script>
        <script src="assets/pages/jquery.tree.js"></script>
        
        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <script>
            jQuery(document).ready(function() {
                // SelectTokenizer
                $(".select2").select2();
            });
        </script>
            
		<script>
			//interval calls
			var initialCalls = setTimeout(function(){
				
			},4000);
			
			var refreshData = setInterval(function(){
				
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				//handleClientLoad();
				//checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>