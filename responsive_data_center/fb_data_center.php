<?
session_start();

function redirect($file,$redirect_after_login){
	$host=$_SERVER['HTTP_HOST'];
	$path=rtrim(dirname($_SERVER['PHP_SELF']),"/\\");
	return header("Location: http://$host$path/$file?red=$redirect_after_login");
	exit;	
}

if((isset($_SESSION['authenticated']) && $_SESSION['authenticated']==true) || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']=='admin')){
	
}
else{
    $tokens = ( explode ( '/', $_SERVER['PHP_SELF'] ));
	redirect("login.php",$tokens[count($tokens)-1]);
	exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="Dan Becker" content="Deforma Data-Center">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<script type="text/javascript" src="js/main_js.js"></script>

        <link rel="shortcut icon" href="assets/images/users/logodeforma_favicon.png">

        <title>Deforma - Responsive Data Center - Facebook</title>

        <!--Morris Chart CSS -->
		<!--<link rel="stylesheet" href="assets/plugins/morris/morris.css">-->

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        
        <!-- Plugins css-->
        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
		<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

		<!-- Add to home screen for Safari on iOS -->
	    <meta name="apple-mobile-web-app-capable" content="no">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="apple-mobile-web-app-title" content="Deforma FB Datacenter">
	    <link rel="apple-touch-icon" href="assets/images/users/logodeforma_fb_favicon_192.png">
		
    </head>


    <body class="fixed-left">
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    
    <!--Google Flow--><script type="text/javascript">
    google.load('visualization', '1', {'packages':['line','table', 'corechart']});
    var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
	var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';
	var scopes = ['https://www.googleapis.com/auth/adexchange.seller','https://www.googleapis.com/auth/adexchange.seller.readonly','https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly'];
	var links;
    function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('authorize-button');
	  if (authResult && !authResult.error) {
		//authorizeButton.style.visibility = 'hidden';

	  } else {
		//authorizeButton.style.visibility = '';
		//authorizeButton.onclick = handleAuthClick;
	  }
	}
	function handleAuthClick(event) {
		gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
		return false;
	  }
    </script><!--Google Auth Flow-->
    
    <!--FB Flow--><script>
    var pageTokens = [];
    var version='2.12';
    var access_token;
	var url_base = "https://graph.facebook.com/v"+version+"/";
	var query = {edge: "", fields: ""};
	var url="";
	function buildURL(query,limit){
		url="";
		url+=url_base;
		url+=query.edge;
		url+="?fields=";
		url+=query.fields;
		url+="&limit="+limit;
		url+="&"+access_token;
		return url;
	}
	
	function statusChangeCallback(response) {
		access_token="access_token="+response.authResponse.accessToken;
		if (response.status === 'connected') {
			getUnpublishedPosts();
			loadKomfo();
            getAccessTokens();
		}
		else if (response.status === 'not_authorized') {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
		 }
		else {
		  document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
		 }
	}

	function checkLoginState() {
		FB.getLoginStatus(function(response) {
		  statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '957927100941199',
			cookie     : true,
			xfbml      : true,
			version    : 'v'+version
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
        
    function getAccessTokens(){
        FB.api(
            '/me/accounts',
            'GET',
            {"limit": 100},
            function(response) {
                for(var i=0;i<response.data.length;i++){
                    pageTokens[response.data[i].name] = response.data[i].access_token;
                    pageTokens[response.data[i].id] = response.data[i].access_token;
                }
            }
        );
    }

    </script><!--FB Flow-->
    
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script><!--Google API library-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.php" class="logo"><span>El<span>Deforma</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Facebook Data</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                           <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <!--<ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>-->
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <!--<li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>-->
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <?php require 'views/logo.html'; ?>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <?php require 'views/sidebar.html'; ?>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                    
                    	<div class='row'>    
							<div class="col-lg-12 col-md-12">
                        		<div class="panel panel-color panel-tabs panel-purple">
                        		
                        			<!-- Modal Promote -->
                                    <?php require 'views/modal-promote.html'; ?>
                                    
                                    <!-- Modal edit Promote -->
                                    <?php require 'views/modal-edit.html'; ?>
                                    
                                    <!-- Modal Quickview -->
                                    <?php require 'views/modal-quickview.html'; ?>
                                    
                                    <!-- Modal Cahnge Goal -->
                                    <?php require 'views/modal-goalchange.html'; ?>
                                    
                                    <!-- Modal Change Name -->
                                    <?php require 'views/modal-namechange.html'; ?>
                                    
                                    <!-- Modal Change Client -->
                                    <?php require 'views/modal-client.html'; ?>
                                   
                                   <!-- Modal Predict -->
                                   <?php require 'views/modal-predict.html'; ?>
                        		
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right" id='komfo-pills'>
											<li class="active">
												<a href="#navpills-deforma" data-toggle="tab" aria-expanded="true" onClick="loadKomfo()">El Deforma</a>
											</li>
                                            <li class="">
												<a href="#navpills-starred" data-toggle="tab" aria-expanded="false" onClick="loadKomfoStarred()">Favs</a>
											</li>
                                            <li class="">
												<a href="#navpills-comercial" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_comercial()">Comercial</a>
											</li>
											<li class="">
												<a href="#navpills-repsodia-videos" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_repsodia()">Repsodia Videos</a>
											</li>
											<li class="">
												<a href="#navpills-repsodia" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_repsodiaPosts()">Repsodia Posts</a>
											</li>
											<li class="">
												<a href="#navpills-techcult-videos" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_cult()">Tech Cult</a>
											</li>
											<li class="">
												<a href="#navpills-techcult" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_cultPosts()">Tech Cult Posts</a>
											</li>
                                            <li class="">
												<a href="#navpills-partido" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_partidoPosts()">Partido Posts</a>
											</li>
                                            <li class="">
												<a href="#navpills-partido-videos" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_partidoVideos()">Partido</a>
											</li>
                                            <li class="">
												<a href="#navpills-chingonas" data-toggle="tab" aria-expanded="false" onClick="loadKomfo_chingonas()">Chingonas</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Better Komfo</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-deforma" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo()">Refresh</button>
                                                        <a href="standalone_fb_data_center.php"><button class='btn btn-rounded btn-success'>Go to standalone</button></a>
                                                        <div id="komfo_table_deforma" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
                                            <div id="navpills-starred" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfoStarred()">Refresh</button>
                                                        <div id="komfo_table_starred" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
                                            <div id="navpills-comercial" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_comercial()">Refresh</button>
														<div id="komfo_table_comercial" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-repsodia-videos" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_repsodia()">Refresh</button>
														<div id="komfo_table_repsodia" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-repsodia" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_repsodiaPosts()">Refresh</button>
                                                        <div id="komfo_table_repsodia_posts" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-techcult-videos" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_cult()">Refresh</button>
                                                        <div id="komfo_table_cult" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-techcult" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_cultPosts()">Refresh</button>
                                                        <div id="komfo_table_cult_posts" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
                                            <div id="navpills-partido" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_partidoPosts()">Refresh</button>
                                                        <div id="komfo_table_partido_posts" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-partido-videos" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_partidoVideos()">Refresh</button>
                                                        <div id="komfo_table_partido_videos" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
                                            <div id="navpills-chingonas" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_chingonas()">Refresh</button>
                                                        <div id="komfo_table_chingonas" class="col-lg-12" style="height: 300px;"></div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col --><!-- Better Komfo -->
						</div>

						<div class='row' id='unpublished-posts'>
							<div class="col-lg-6 col-md-6">
								<label for="unpublished-posts" class="control-label">Unpublished posts</label>
								<select id="unpublished-posts-select" class="form-control">
								</select>
							</div>
							<div  class="col-lg-3 col-md-3">
								<button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#promote-modal" onClick="getUnpublishedPostId()">Promote</button>
							</div>
						</div>
                                                
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-color panel-tabs panel-purple">
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-query" data-toggle="tab" aria-expanded="false">Query</a>
											</li>
											<li class="">
												<a href="#navpills-promoted" data-toggle="tab" aria-expanded="false">Promoted</a>
											</li>
											<li class="">
												<a href="#navpills-latest" data-toggle="tab" aria-expanded="false">Latest</a>
											</li>
											<li class="">
												<a href="#navpills-repsodia-query" data-toggle="tab" aria-expanded="false">Repsodia Query</a>
											</li>
                                            <li class="">
												<a href="#navpills-techcult-query" data-toggle="tab" aria-expanded="false">TechCult Query</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Facebook Posts - Data Center</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">
											<div id="navpills-query" class="tab-pane fade in active">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="query_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="select_posts" id="select_posts_label">Posts: </label>
																	<div class="col-sm-9">
																		<input id="select_posts" class="ui-widget form-control">
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_query" onchange="selectPostQuery()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="36">36 horas</option>
																			<option value="48">48 horas</option>
																			<option value="60">60 horas</option>
																			<option value="72">72 horas</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="query_chart">
																<div id="chart_div_query" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(2);">CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(9);">Link Clicks/<br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(10);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(11);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(5);">Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawChartQuery(12);">Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(1);">Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(3);">Link Clicks/<br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(6);">CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(14);">RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(7);">CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(8);">CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawChartQuery(13);">Link CPM</button>
																	</div>
																</div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(2);">&#916; CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(9);">&#916; Link Clicks/<br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(10);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(11);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(5);">&#916; Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery(); drawDeltaChartQuery(12);">&#916; Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(1);">&#916; Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(3);">&#916; Link Clicks/<br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(6);">&#916; CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(7);">&#916; CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(8);">&#916; CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataQuery(); drawDeltaChartQuery(13);">&#916; Link CPM</button>
																	</div>
																</div>
															</div>
		
															<!-- Metricas -->
															<div class="row">
																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30">Site Statistics</h4>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-primary" id="site_pageviews">-</span>
																			Pageviews:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-primary" id="time_on_page">-</span>
																			Time on Page:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="site_shares">-</span>
																			Shares: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="site_tweets">-</span>
																			Tweets: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-success" id="site_whatsapp">-</span>
																			Whatsapps: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-warning" id="site_users">-</span>
																			Unique Users: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-inverse" id="site_shares_per_pageview">-</span>
																			Shares/Pageviews: 
																		</li>
																	</ul>
																</div>
																<!-- end col -->

																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30" >Facebook Statistics</h4>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-primary" id="fb_link_clicks">-</span>
																			Link Clicks:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-primary" id="fb_video_views">-</span>
																			Video Views:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_shares">-</span>
																			Shares:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_likes">-</span>
																			Likes:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_comments">-</span>
																			Comments: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="fb_reach">-</span>
																			Reach: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="fb_impressions">-</span>
																			impressions: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="fb_interactions">-</span>
																			interactions: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-warning" id="fb_hide">-</span>
																			Hide post: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-danger" id="fb_spam">-</span>
																			Spam: 
																		</li>
																	</ul>
																</div>
																
																<div class="col-md-2">
																	<h4 class="header-title m-t-0 m-b-30" >Facebook Reactions</h4>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-pink" id="reactions">-</span>
																			Total:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-blue" id="reactions_like">-</span>
																			<img src='assets/images/reactions/like.png' width='25px'>
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-danger" id="reactions_love">-</span>
																			<img src='assets/images/reactions/love.png' width='25px'>
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-success" id="reactions_wow">-</span>
																			<img src='assets/images/reactions/wow.png' width='25px'>
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-warning" id="reactions_haha">-</span>
																			<img src='assets/images/reactions/haha.png' width='25px'>
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="reactions_sad">-</span>
																			<img src='assets/images/reactions/sad.png' width='25px'>
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-inverse" id="reactions_angry">-</span>
																			<img src='assets/images/reactions/angry.png' width='25px'>
																		</li>
																	</ul>
																</div>
																
																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30" id="post_id">-</h4>
																	<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick='getPromotePostId(post_id.childNodes[0].innerHTML)'>Promote</button>
																</div>
																<!-- end col -->
															</div>
															<!-- end row -->

														</div>
	
													</div><!-- end col-->
												</div>
											</div>
											<div id="navpills-promoted" class="tab-pane fade">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="promoted_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="promoted_posts" id="promoted_posts_label">Posts: </label>
																	<div class="col-sm-9">
																		<select class="form-control" id="combo_box_promoted" onchange="selectPostPromoted();" onclick="UpdatePostsPromoted();">
																		</select>
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_promoted" onchange="selectPostPromoted()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="36">36 horas</option>
																			<option value="48">48 horas</option>
																			<option value="60">60 horas</option>
																			<option value="72">72 horas</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="promoted_chart">
																<div id="chart_div_promoted" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(2);">CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(9);">Link Clicks/<br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(10);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(11);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(5);">Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawChartPromoted(12);">Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(1);">Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(3);">Link Clicks/<br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(6);">CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(14);">RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(7);">CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(8);">CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawChartPromoted(13);">Link CPM</button>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(2);">&#916; CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(9);">&#916; Link Clicks/<br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(10);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(11);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(5);">&#916; Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(12);">&#916; Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(1);">&#916; Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(3);">&#916; Link Clicks/<br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(6);">&#916; CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(14);">&#916; RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(7);">&#916; CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(8);">&#916; CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataPromoted(); drawDeltaChartPromoted(13);">&#916; Link CPM</button>
																	</div>
																</div>
															</div>
															<div class="row m-t-20">
																<div class='col-lg-6'>
																	<div id="chart_div_age_gender">
																	
																	</div>
																</div>
																<div class='col-lg-6'>
																	<div id="chart_div_gender">
																	
																	</div>
																	<div id="chart_div_age">
																	
																	</div>
																	<div id="chart_div_device">
																	
																	</div>
																</div>
															</div>
														</div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-latest" class="tab-pane fade">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="latest_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="combo_box_latest" id="latest_posts_label">Posts: </label>
																	<div class="col-sm-9">
																		<select class="form-control" id="combo_box_latest" onchange="selectPostDashboard();" onclick="UpdatePostsDashboard();">
																		</select>
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_latest" onchange="selectPostDashboard()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="36">36 horas</option>
																			<option value="48">48 horas</option>
																			<option value="60">60 horas</option>
																			<option value="72">72 horas</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="latest_chart">
																<div id="chart_div_latest" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(2);">CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(9);">Link Clicks/<br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(10);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(11);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(5);">Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawChartDashboard(12);">Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(1);">Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(3);">Link Clicks/<br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(6);">CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(14);">RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(7);">CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(8);">CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawChartDashboard(13);">Link CPM</button>
																	</div>
																</div>
																<div class="col-md-2">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(2);">&#916; CTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(9);">&#916; Link Clicks/<br>Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(10);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(11);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(5);">&#916; Penetration</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(12);">&#916; Comments</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(1);">&#916; Gasto</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(3);">&#916; Link Clicks/<br>Video Views Pagados</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(6);">&#916; CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(14);">&#916; RT CXW</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(7);">&#916; CPM</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(8);">&#916; CTR Pagado</button>
																		<button type="button" class="btn btn-danger waves-effect paid" onClick="UpdateDataDashboard(); drawDeltaChartDashboard(13);">&#916; Link CPM</button>
																	</div>
																</div>
															</div>
														</div>
													</div><!-- end col-->
												</div>
											</div>
											<div id="navpills-repsodia-query" class="tab-pane fade">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="query_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="select_posts_repsodia" id="select_posts_label_repsoida">Video: </label>
																	<div class="col-sm-9">
																		<input id="select_posts_repsodia" class="ui-widget form-control">
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_query_repsodia" onchange="selectPostQuery_repsodia()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="48">2 días</option>
																			<option value="72">3 días</option>
																			<option value="120">5 días</option>
																			<option value="240">10 días</option>
																			<option value="360">15 días</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="query_chart_repsodia">
																<div id="chart_div_query_repsodia" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(2);">VTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(9);">Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(8);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(7);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(10);">Comments</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(5);">Reach</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(6);">Clicked to play</button>
																		<button type="button" class="btn btn-primary waves-effect paid" onClick="UpdateDataQuery_repsodia(); drawChartQuery_repsodia(1);">Engagement</button>
																		<button type="button" class="btn btn-primary waves-effect paid" onClick="UpdateDataQuery_repsodia(); drawChartQueryRetention_repsodia();">Retention Graph</button>
																		
																	</div>
																</div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(2);">&#916; VTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(9);">&#916; Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(8);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(7);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(10);">&#916; Comments</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(5);">&#916; Reach</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(6);">&#916; Clicked to play</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_repsodia(); drawDeltaChartQuery_repsodia(1);">&#916; Engagement</button>
																	</div>
																</div>
															</div><!-- Metricas -->
															<div class="row">
																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30">Facebook Statistics</h4>
																	<h4 class="header-title m-t-0 m-b-30" id="post_id_repsodia"></h4>
																	<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick='getPromotePostId(post_id_repsodia.childNodes[0].innerHTML)'>Promote</button>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_views_repsodia">-</span>
																			Views:
																		</li>																	
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_shares_repsodia">-</span>
																			Shares:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_likes_repsodia">-</span>
																			Likes:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_comments_repsodia">-</span>
																			Comments: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="fb_reach_repsodia">-</span>
																			Reach: 
																		</li>
																	</ul>
																</div>
																<!-- end col -->
															</div>
															<!-- end row -->

														</div>
	
													</div><!-- end col-->
												</div>
											</div>
                                            <div id="navpills-techcult-query" class="tab-pane fade">
												<div class="row">
													<div class="col-lg-12">
														<div class="card-box">
															<div class="row" id="query_selectors">
																<div class="form-group">
																	<label class="col-sm-1" for="select_posts_techcult" id="select_posts_label_techcult">Video: </label>
																	<div class="col-sm-9">
																		<input id="select_posts_techcult" class="ui-widget form-control">
																	</div>
																	<div class="col-sm-2">
																		<select id="time_lapse_query_techcult" onchange="selectPostQuery_techcult()" class="form-control">
																			<option value="2">2 horas</option>
																			<option value="6">6 horas</option>
																			<option value="12">12 horas</option>
																			<option value="24">24 horas</option>
																			<option value="48">2 días</option>
																			<option value="72">3 días</option>
																			<option value="120">5 días</option>
																			<option value="240">10 días</option>
																			<option value="360">15 días</option>
																			<option value="all">Lifetime</option>
																		</select>
																	</div>
																</div>
															</div>

															<div class="row" id="query_chart_techcult">
																<div id="chart_div_query_techcult" style="height: 500px;" class="col-sm-8"></div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(2);">VTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(9);">Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(8);">Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(4);">Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(7);">Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(10);">Comments</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(5);">Reach</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(6);">Clicked to play</button>
																		<button type="button" class="btn btn-primary waves-effect paid" onClick="UpdateDataQuery_techcult(); drawChartQuery_techcult(1);">Engagement</button>
																		<button type="button" class="btn btn-primary waves-effect paid" onClick="UpdateDataQuery_techcult(); drawChartQueryRetention_techcult();">Retention Graph</button>
																		
																	</div>
																</div>
																<div class="col-md-2 m-t-100">
																	<div class="btn-group-vertical m-b-6">
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(2);">&#916; VTR Organico</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(9);">&#916; Video Views</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(8);">&#916; Shares</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(4);">&#916; Viral Amp</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(7);">&#916; Likes</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(10);">&#916; Comments</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(5);">&#916; Reach</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(6);">&#916; Clicked to play</button>
																		<button type="button" class="btn btn-primary waves-effect" onClick="UpdateDataQuery_techcult(); drawDeltaChartQuery_techcult(1);">&#916; Engagement</button>
																	</div>
																</div>
															</div><!-- Metricas -->
															<div class="row">
																<div class="col-md-3">
																	<h4 class="header-title m-t-0 m-b-30">Facebook Statistics</h4>
																	<h4 class="header-title m-t-0 m-b-30" id="post_id_techcult"></h4>
																	<button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#promote-modal" onclick='getPromotePostId(post_id_techcult.childNodes[0].innerHTML)'>Promote</button>

																	<ul class="list-group">
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_views_techcult">-</span>
																			Views:
																		</li>																	
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_shares_techcult">-</span>
																			Shares:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_likes_techcult">-</span>
																			Likes:
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-purple" id="fb_comments_techcult">-</span>
																			Comments: 
																		</li>
																		<li class="list-group-item">
																			<span class="badge badge-info" id="fb_reach_techcult">-</span>
																			Reach: 
																		</li>
																	</ul>
																</div>
																<!-- end col -->
															</div>
															<!-- end row -->

														</div>
	
													</div><!-- end col-->
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- Charts -->
                        
                        <div class='row'>    
							<div class="col-lg-12 col-md-12">
                        		<div class="panel panel-color panel-tabs panel-purple">
                                    <div class="panel-heading">
                                        <ul class="nav nav-pills pull-right">
											<li class="active">
												<a href="#navpills-deformatv" data-toggle="tab" aria-expanded="false">El DeformaTV</a>
											</li>
											<li class="">
												<a href="#navpills-boom" data-toggle="tab" aria-expanded="false">Boom</a>
											</li>
										</ul>
                                        <h3 class="panel-title">Others Komfo</h3>
                                    </div>
                                    <div class="panel-body">
										<div class="tab-content">											
											<div id="navpills-deformatv" class="tab-pane fade in active">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_deformatv()">Refresh</button>
                                                        <div id="komfo_table_deformatv" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
											<div id="navpills-boom" class="tab-pane fade">
												<div class="row">
													<div class="wrap_div col-lg-12">
														<button class='btn btn-rounded btn-primary' onClick="loadKomfo_boom()">Refresh</button>
                                                        <div id="komfo_table_boom" class="col-lg-12" style="height: 500px;"></div>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div><!-- end col --><!-- Others Komfo -->
						</div>

                    </div> <!-- container -->
                    
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © ElDeforma.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        
        <!-- Plugins Js -->
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
     	<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     	<script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
     	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

        <!--Morris Chart-->
		<!--<script src="assets/plugins/morris/morris.min.js"></script>-->
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Dashboard init -->
        <!--<script src="assets/pages/jquery.dashboard.js"></script>-->

		<!-- Validation js (Parsleyjs) -->
        <script type="text/javascript" src="assets/plugins/parsleyjs/dist/parsley.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <!-- Modal Behavior and validation -->
        <script type="text/javascript">
            //Change the action of forms on submit to call functions
			function validatePromote(){
                $('#form').parsley().validate();
            }
            $(document).ready(function() {
				$('#form').parsley();
			});
			$('#form').parsley().on('form:success', function() {
				getPromoteParameters();
			  $("#promote-modal").modal("hide");
			});
            
            function validateEditPromote(){
                $('#edit-form').parsley().validate();
            }
            $(document).ready(function() {
				$('#edit-form').parsley();
			});
			$('#edit-form').parsley().on('form:success', function() {
				setEditPromoteParameters();
			  $("#edit-promote-modal").modal("hide");
			});
		</script>
        
        <script>	
			//posts query
			</script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
			<script src="//code.jquery.com/jquery-1.10.2.js"></script>
			<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			
			<script>
			jq1102 = jQuery.noConflict( true );
			//posts query	
			var data_query=[];
			var metric_query=0;
			var formating_query;
			var post_id_query;
			var query_currency;
			var delta_link_clicks=[];
			var post;

			jq1102(function() {
				var select_posts=[];
				$.ajax({
					url: "php_scripts/get_all_post_names.php",
					dataType: 'json',
					method: 'get',
					async: false,
					success: function(response){
						select_posts=response;
						//console.log(select_posts);
					}
				});
 
				jq1102( "#select_posts" ).autocomplete({
				  minLength: 0,
				  source: select_posts,
				  focus: function( event, ui ) {
					jq1102( "#select_posts" ).val( ui.item.label );
					return false;
				  },
				  select: function( event, ui ) {
					jq1102( "#select_posts" ).val( ui.item.label );
					//$( "#select_value" ).val( ui.item.value );
					post_id_query = ui.item.value;
					//console.log(post_id_query);
					var stats = document.getElementsByClassName("stats");
					for(var m=0;m<stats.length;m++)
					{
						stats.item(m).innerHTML = '';
					}
					//checkLoginState(post_id_query);
					getReportStats(post_id_query);
					selectPostQuery();
					return false;
				  }
				});
			});
			function UpdateDataQuery(){
				var time_lapse = document.getElementById("time_lapse_query").value;
				//var post_id=document.getElementById("combo_box_promoted").value;
				$.ajax({
					url: "php_scripts/get_post_data.php",
					dataType: "json",
					data: {post_id: post_id_query, time_lapse: time_lapse},
					method: 'get',
					async: false,
					success: function(response){
						data_query=response;
						for(var g=0;g<data_query.length;g++)
						{
							if(parseFloat(data_query[g].spend)>0){
								var paid_cels = document.getElementsByClassName("paid");
								for(var i=0;i<paid_cels.length;i++){
									paid_cels.item(i).style.visibility = "visible";
								}
								break;
							}
							else{
								var paid_cels = document.getElementsByClassName("paid");
								for(var i=0;i<paid_cels.length;i++){
									paid_cels.item(i).style.visibility = "hidden";
								}
							}

						}
					}
				});	
				$.ajax({
					url: "php_scripts/get_post_currency.php",
					dataType: "text",
					data: {post_id: post_id_query},
					method: 'get',
					async: false,
					success: function(response){
						query_currency=response;
					}
				});
			}
			function selectPostQuery(){
				//console.log(post_id_query);
				UpdateDataQuery();
				drawChartQuery(metric_query);
			}
			function drawChartQuery(measure){
			  metric_query=measure;
			  var data_table = new google.visualization.DataTable();
			  var currency_factor;
  
			  if(query_currency=="MXN"){
				currency_factor=1/17;
			  }else{
				currency_factor=1;
			  }
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 1:
					data_table.addColumn('number', 'Gasto');
					break;
				case 2:
					data_table.addColumn('number', 'CTR Organico');
					break;
				case 3:
					data_table.addColumn('number', 'Link Clicks Pagados');
					break;
				case 4:
					data_table.addColumn('number', 'Viral AMP');
					break;
				case 5:
					data_table.addColumn('number', 'Penetration');
					break;
				case 6:
					data_table.addColumn('number', 'CXW');
					break;
				case 7:
					data_table.addColumn('number', 'CPM');
					break;
				case 8:
					data_table.addColumn('number', 'CTR Pagado');
					break;
				case 9:
					data_table.addColumn('number', 'Link Clicks');
					break;
				case 10:
					data_table.addColumn('number', 'Shares');
					break;
				case 11:
					data_table.addColumn('number', 'Likes');
					break;
				case 12:
					data_table.addColumn('number', 'Comments');
					break;
				case 13:
					data_table.addColumn('number', 'Link CPM');
					break;
				case 14:
					data_table.addColumn('number', 'RT CXW');
					break;
				default:
					data_table.addColumn('number', 'Link Clicks');
					break;
			  }
  
			  var formating;
			  for(var n=0;n<data_query.length;n++){
				data_query[n].record_time=data_query[n].record_time.replace(' ','T');
				data_query[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
				var data_date=new Date(data_query[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].spend)*currency_factor]);
					  formating = {
						title: 'Gasto',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].organic_ctr)/100, f: data_query[n].organic_ctr+'%'}]);
					  formating = {
						title: 'CTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 3:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].paid_link_clicks)]);
					  formating = {
						title: 'Link Clicks Pagados',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,parseFloat(data_query[n].viral_amp)]);
					  formating = {
						title: 'Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,{v: parseFloat(data_query[n].penetration)/100, f: data_query[n].penetration}]);
					  formating = {
						title: 'Penetration',
						//curveType: 'function',
						vAxis: {
							format: '##.####%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,parseFloat(data_query[n].cxw)*currency_factor]);
					  formating = {
						title: 'CXW',
						//curveType: 'function',
						vAxis: {
							format: '$0.######'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,parseFloat(data_query[n].cpm)*currency_factor]);
					  formating = {
						title: 'CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,{v: parseFloat(data_query[n].paid_ctr)/100, f: data_query[n].paid_ctr}]);
					  formating = {
						title: 'CTR Pagado',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_clicks)]);
					  formating = {
						title: 'Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query[n].shares)]);
					  formating = {
						title: 'Shares',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 11:
					  data_table.addRow([data_date,parseFloat(data_query[n].likes)]);
					  formating = {
						title: 'Likes',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 12:
					  data_table.addRow([data_date,parseFloat(data_query[n].comments)]);
					  formating = {
						title: 'Comments',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 13:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_cpm)*currency_factor]);
					  formating = {
						title: 'Link CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 14:
					  if(n<data_query.length-1){
						  var rt_cxw=(parseFloat(data_query[n+1].spend)-parseFloat(data_query[n].spend))/(parseFloat(data_query[n+1].paid_link_clicks)-parseFloat(data_query[n].paid_link_clicks))*currency_factor;
						  data_table.addRow([
							  data_date,
							  rt_cxw
						  ]);
						  formating = {
							title: 'RT CXW',
							//curveType: 'function',
							vAxis: {
								format: 'currency'
							},
							hAxis: {
								format: 'M/d HH:mm',
								gridlines:{
									count: -1
								},
								title: 'Hora'
							},
							legend:{
								position: 'none'
							},
							pointSize: 3,
							trendlines: { 
								0: {
									lineWidth: 10,
									color: 'green',
									opacity: 1
								} 
							}
						  };
					  }
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_clicks)]);
					  formating = {
						title: 'Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query'));
			  table.draw(data_table,formating);
			}    
			function drawDeltaChartQuery(measure){
			  metric_query=measure;
			  var data_table = new google.visualization.DataTable();
			  var currency_factor;
  
			  if(query_currency=="MXN"){
				currency_factor=1/17;
			  }else{
				currency_factor=1;
			  }
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 1:
					data_table.addColumn('number', '\u0394 Gasto');
					break;
				case 2:
					data_table.addColumn('number', '\u0394 CTR Organico');
					break;
				case 3:
					data_table.addColumn('number', '\u0394 Link Clicks Pagados');
					break;
				case 4:
					data_table.addColumn('number', '\u0394 Viral AMP');
					break;
				case 5:
					data_table.addColumn('number', '\u0394 Penetration');
					break;
				case 6:
					data_table.addColumn('number', '\u0394 CXW');
					break;
				case 7:
					data_table.addColumn('number', '\u0394 CPM');
					break;
				case 8:
					data_table.addColumn('number', '\u0394 CTR Pagado');
					break;
				case 9:
					data_table.addColumn('number', '\u0394 Link Clicks');
					break;
				case 10:
					data_table.addColumn('number', '\u0394 Shares');
					break;
				case 11:
					data_table.addColumn('number', '\u0394 Likes');
					break;
				case 12:
					data_table.addColumn('number', '\u0394 Comments');
					break;
				case 13:
					data_table.addColumn('number', '\u0394 Link CPM');
					break;
				default:
					data_table.addColumn('number', '\u0394 Link Clicks');
					break;
			  }
  
			  var formating;
			  for(var n=1;n<data_query.length;n++){
				data_query[n].record_time=data_query[n].record_time.replace(' ','T');
				data_query[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
				var data_date=new Date(data_query[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].spend)*currency_factor-parseFloat(data_query[n-1].spend)*currency_factor]);
					  formating = {
						title: '\u0394 Gasto',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].organic_ctr)/100-parseFloat(data_query[n-1].organic_ctr)/100, f: parseFloat(data_query[n].organic_ctr)-parseFloat(data_query[n-1].organic_ctr)+'%'}]);
					  formating = {
						title: '\u0394 CTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 3:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].paid_link_clicks)-parseFloat(data_query[n-1].paid_link_clicks)]);
					  formating = {
						title: '\u0394 Link Clicks Pagados',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].viral_amp)-parseFloat(data_query[n-1].viral_amp)]);
					  formating = {
						title: '\u0394 Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].penetration)/100-parseFloat(data_query[n-1].penetration)/100, f: parseFloat(data_query[n].penetration)-parseFloat(data_query[n-1].penetration)}]);
					  formating = {
						title: '\u0394 Penetration',
						//curveType: 'function',
						vAxis: {
							format: '##.####%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].cxw)*currency_factor-parseFloat(data_query[n-1].cxw)*currency_factor]);
					  formating = {
						title: '\u0394 CXW',
						//curveType: 'function',
						vAxis: {
							format: '$0.######'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].cpm)*currency_factor-parseFloat(data_query[n-1].cpm)*currency_factor]);
					  formating = {
						title: '\u0394 CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query[n].paid_ctr)/100-parseFloat(data_query[n-1].paid_ctr)/100, f: parseFloat(data_query[n].paid_ctr)-parseFloat(data_query[n-1].paid_ctr)}]);
					  formating = {
						title: '\u0394 CTR Pagado',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,
					  parseFloat(data_query[n].link_clicks)-parseFloat(data_query[n-1].link_clicks)]);
					  delta_link_clicks[n]=parseFloat(data_query[n].link_clicks)-parseFloat(data_query[n-1].link_clicks);
					  formating = {
						title: '\u0394 Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query[n].shares)-parseFloat(data_query[n-1].shares)]);
					  formating = {
						title: '\u0394 Shares',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 11:
					  data_table.addRow([data_date,parseFloat(data_query[n].likes)-parseFloat(data_query[n-1].likes)]);
					  formating = {
						title: '\u0394 Likes',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 12:
					  data_table.addRow([data_date,parseFloat(data_query[n].comments)-parseFloat(data_query[n-1].comments)]);
					  formating = {
						title: '\u0394 Comments',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 13:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_cpm)*currency_factor-parseFloat(data_query[n-1].link_cpm)*currency_factor]);
					  formating = {
						title: '\u0394 Link CPM',
						//curveType: 'function',
						vAxis: {
							format: 'currency'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query[n].link_clicks)-parseFloat(data_query[n-1].link_clicks)]);
					  formating = {
						title: '\u0394 Link Clicks',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query'));
			  table.draw(data_table,formating);
			}		
			
			//Report
			function getReportStats(post_id) {
				post={};
				FB.api(
				  post_id,
				  'GET',
				  {
					  "fields":"likes.limit(0).summary(1).as(likes)"+
						  ",comments.limit(0).summary(1).as(comments)"+
						  ",shares"+
						  ",link"+
						  ",reactions.type(LIKE).limit(0).summary(1).as(like)"+
						  ",reactions.type(WOW).limit(0).summary(1).as(wow)"+
						  ",reactions.type(HAHA).limit(0).summary(1).as(haha)"+
						  ",reactions.type(SAD).limit(0).summary(1).as(sad)"+
						  ",reactions.type(ANGRY).limit(0).summary(1).as(angry)"+
						  ",reactions.type(LOVE).limit(0).summary(1).as(love)"+
						  ",reactions.limit(0).summary(1).as(total_reactions)"+
						  ",insights.metric(post_impressions_unique).fields(values).as(reach)"+
						  ",insights.metric(post_impressions).fields(values).as(impressions)"+
						  ",insights.metric(post_consumptions_by_type).fields(values).as(consumptions)"+
						  ",insights.metric(post_negative_feedback_by_type).fields(values).as(negative_feedback)"+
						  ",insights.metric(post_consumptions).fields(values).as(interactions)"+
						  ",insights.metric(post_video_views).period(lifetime).fields(values).as(video_views)",
                      'access_token': pageTokens[post_id.split('_')[0]]
				  },
				  function(response) {
				  	  console.log(response);
					  post.likes = typeof response.likes.summary.total_count === 'undefined' ? 0 : response.likes.summary.total_count;
					  document.getElementById("fb_likes").innerHTML = numberWithCommas(post.likes);
					  post.comments = typeof response.comments.summary.total_count === 'undefined' ? 0 : response.comments.summary.total_count;
					  document.getElementById("fb_comments").innerHTML = numberWithCommas(post.comments);
					  post.shares = typeof response.shares === 'undefined' ? 0 : response.shares.count;
					  document.getElementById("fb_shares").innerHTML = numberWithCommas(post.shares);
					  post.link = response.link;
					  document.getElementById("post_id").innerHTML = '';
					  var aTag=document.createElement('a');
					  aTag.setAttribute('href',"http://www.facebook.com/"+post_id);
					  aTag.setAttribute('target','_blank');
					  aTag.innerHTML = post_id;
					  document.getElementById("post_id").appendChild(aTag);
					  queryAnalytics(post);
					  document.getElementById("reactions_like").innerHTML = numberWithCommas(response.like.summary.total_count);
					  document.getElementById("reactions_love").innerHTML = numberWithCommas(response.love.summary.total_count);
					  document.getElementById("reactions_wow").innerHTML = numberWithCommas(response.wow.summary.total_count);
					  document.getElementById("reactions_haha").innerHTML = numberWithCommas(response.haha.summary.total_count);
					  document.getElementById("reactions_sad").innerHTML = numberWithCommas(response.sad.summary.total_count);
					  document.getElementById("reactions_angry").innerHTML = numberWithCommas(response.angry.summary.total_count);
					  document.getElementById("reactions").innerHTML = numberWithCommas(response.total_reactions.summary.total_count);
					  post.unique_imp = response.reach.data[0].values[0].value;
					  document.getElementById("fb_reach").innerHTML = numberWithCommas(post.unique_imp);
					  post.imp = response.impressions.data[0].values[0].value;
					  document.getElementById("fb_impressions").innerHTML = numberWithCommas(post.imp);
					  post.link_clicks = response.consumptions.data[0].values[0].value['link clicks'] != undefined ? response.consumptions.data[0].values[0].value['link clicks'] : 0;
					  document.getElementById("fb_link_clicks").innerHTML = numberWithCommas(post.link_clicks);
					  post.hide = typeof response.negative_feedback.data[0].values[0].value.hide_clicks === 'undefined' ? 0 : response.negative_feedback.data[0].values[0].value.hide_clicks;
					  post.spam = typeof response.negative_feedback.data[0].values[0].value.report_spam_clicks === 'undefined' ? 0 : response.negative_feedback.data[0].values[0].value.report_spam_clicks;
					  document.getElementById("fb_hide").innerHTML = numberWithCommas(post.hide);
					  document.getElementById("fb_spam").innerHTML = numberWithCommas(post.spam);
					  document.getElementById("fb_interactions").innerHTML = numberWithCommas(response.interactions.data[0].values[0].value);
					  post.video_views= response.video_views.data[0].values[0].value != undefined ? response.video_views.data[0].values[0].value : 0;
					  document.getElementById("fb_video_views").innerHTML = numberWithCommas(post.video_views);
				  }
				);
			}
			function getSeconds(seconds){
				var sec;
				if(Math.floor(parseFloat(post.time_on_page)%60)>9){
					return  Math.floor(parseFloat(post.time_on_page)%60);
				}else{
					return "0"+Math.floor(parseFloat(post.time_on_page)%60);
				}
			}
			function queryAnalytics(post){
				post.link = post.link.replace('http://eldeforma.com','');
				if(post.link.indexOf('?utm')!=-1){
					post.link = post.link.substring(0,post.link.indexOf('?utm'));
				}
				var date= new Date();
				var month = date.getMonth()>=9 ? (date.getMonth()+1) : "0"+(date.getMonth()+1);
				var day = date.getDate()>=10 ? (date.getDate()) : "0"+(date.getDate());
				gapi.client.load('analytics','v3', function(){
					var requestPageviews = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:pageviews",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestPageviews.execute(function(resp){
						console.log(resp);
						post.pageviews = resp.totalsForAllResults["ga:pageviews"];
						document.getElementById("site_pageviews").innerHTML = numberWithCommas(post.pageviews);
						var requestSocial = gapi.client.analytics.data.ga.get({
							ids: "ga:41142760",
							"start-date": "60daysAgo",
							"end-date": "today",
							metrics: "ga:totalEvents",
							dimensions: "ga:eventAction",
							fields: "rows",
							output: 'json',
							filters: "ga:eventLabel=@"+post.link
						});
						requestSocial.execute(function(resp){
							console.log(resp);
							post.site_shares = 0;
							post.site_tweets = 0
							post.site_whatsapp = 0;
							for(var l=0;l<resp.rows.length;l++){
								switch(resp.rows[l][0])
								{
									case "Facebook Share Class":
										post.site_shares = resp.rows[l][1];
										break;
									case "Twitter Tweet Class":
										post.site_tweets = resp.rows[l][1];
										break;
									case "WhatsApp Message Class":
										post.site_whatsapp = resp.rows[l][1];
										break;
									default:
										break;
								}
							}
							document.getElementById("site_shares").innerHTML = numberWithCommas(post.site_shares);
							document.getElementById("site_tweets").innerHTML = numberWithCommas(post.site_tweets);
							document.getElementById("site_whatsapp").innerHTML = numberWithCommas(post.site_whatsapp);
							document.getElementById("site_shares_per_pageview").innerHTML = parseFloat(((parseFloat(post.site_shares)/parseFloat(post.pageviews)*100))).toFixed(4)+'%';
							//console.log(post);
						});
					});
					var requestUsers = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:users",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestUsers.execute(function(resp){
						//console.log(resp);
						post.users = resp.totalsForAllResults["ga:users"];
						document.getElementById("site_users").innerHTML = numberWithCommas(post.users);	
					});
					var requestTime = gapi.client.analytics.data.ga.get({
						ids: "ga:41142760",
						"start-date": "365daysAgo",
						"end-date": "today",
						metrics: "ga:avgTimeOnPage",
						fields: "totalsForAllResults",
						output: 'json',
						filters: "ga:pagePath=@"+post.link
					});
					requestTime.execute(function(resp){
						//console.log(resp);
						post.time_on_page = resp.totalsForAllResults["ga:avgTimeOnPage"];
						document.getElementById("time_on_page").innerHTML = Math.floor(parseFloat(post.time_on_page)/60)+":"+getSeconds(parseFloat(post.time_on_page));
					});
				});
				//console.log(post);	
			}
			//report
			
			//repsodia query	
			var data_query_repsodia=[];
			var metric_query_repsodia=0;
			var formating_query_repsodia;
			var post_id_query_repsodia;
			var delta_link_clicks_repsodia=[];
			var post_repsodia;

			jq1102(function() {
				var select_posts=[];
				$.ajax({
					url: "php_scripts/get_all_post_names_repsodia.php",
					dataType: 'json',
					method: 'get',
					async: false,
					success: function(response){
						select_posts=response;
						//console.log(select_posts);
					}
				});
 
				jq1102( "#select_posts_repsodia" ).autocomplete({
				  minLength: 0,
				  source: select_posts,
				  focus: function( event, ui ) {
					jq1102( "#select_posts_repsodia" ).val( ui.item.label );
					return false;
				  },
				  select: function( event, ui ) {
					jq1102( "#select_posts_repsodia" ).val( ui.item.label );
					//$( "#select_value" ).val( ui.item.value );
					post_id_query_repsodia = ui.item.value;
					//console.log(post_id_query);
					var stats = document.getElementsByClassName("stats");
					for(var m=0;m<stats.length;m++)
					{
						stats.item(m).innerHTML = '';
					}
					//checkLoginState(post_id_query);
					getReportStats_repsodia(post_id_query_repsodia);
					selectPostQuery_repsodia();
					return false;
				  }
				});
			});
			function UpdateDataQuery_repsodia(){
				var time_lapse_repsodia = document.getElementById("time_lapse_query_repsodia").value;
				//var post_id=document.getElementById("combo_box_promoted").value;
				$.ajax({
					url: "php_scripts/get_post_data_repsodia.php",
					dataType: "json",
					data: {post_id: post_id_query_repsodia, time_lapse: time_lapse_repsodia},
					method: 'get',
					async: false,
					success: function(response){
						data_query_repsodia=response;
					}
				});	
			}
			function selectPostQuery_repsodia(){
				//console.log(post_id_query);
				UpdateDataQuery_repsodia();
				drawChartQuery_repsodia(metric_query_repsodia);
			}
			function drawChartQuery_repsodia(measure){
			  metric_query_repsodia=measure;
			  var data_table = new google.visualization.DataTable();
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 2:
					data_table.addColumn('number', 'VTR Organico');
					break;
				case 4:
					data_table.addColumn('number', 'Viral AMP');
					break;
				case 9:
					data_table.addColumn('number', 'Video Views');
					break;
				case 8:
					data_table.addColumn('number', 'Shares');
					break;
				case 7:
					data_table.addColumn('number', 'Likes');
					break;
				case 10:
					data_table.addColumn('number', 'Comments');
					break;
				case 5:
					data_table.addColumn('number', 'reach');
					break;
				case 6:
					data_table.addColumn('number', 'Clicked to play');
					break;
				case 1:
					data_table.addColumn('number', 'Engagement');
					break;
				default:
					data_table.addColumn('number', 'Vide Views');
					break;
			  }
  
			  var formating;
			  for(var n=0;n<data_query_repsodia.length;n++){
				data_query_repsodia[n].record_time=data_query_repsodia[n].record_time.replace(' ','T');
				data_query_repsodia[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
				var data_date=new Date(data_query_repsodia[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query_repsodia[n].engagement)]);
					  formating = {
						title: 'Engagement',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_repsodia[n].organic_ctr)/100, f: data_query_repsodia[n].organic_ctr+'%'}]);
					  formating = {
						title: 'VTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].viral_amp)]);
					  formating = {
						title: 'Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend: {
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,{v: parseFloat(data_query_repsodia[n].reach)/100, f: data_query_repsodia[n].reach}]);
					  formating = {
						title: 'Reach',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].clicked_to_play)]);
					  formating = {
						title: 'Clicked to play',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].likes)]);
					  formating = {
						title: 'Likes',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,{v: parseFloat(data_query_repsodia[n].shares), f: data_query_repsodia[n].shares}]);
					  formating = {
						title: 'Shares',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].link_clicks)]);
					  formating = {
						title: 'Video Views',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].comments)]);
					  formating = {
						title: 'Comments',
						//curveType: 'function',
						vAxis:{
							format: 'number'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].link_clicks)]);
					  formating = {
						title: 'Video Views',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query_repsodia'));
			  table.draw(data_table,formating);
			}
			function drawChartQueryRetention_repsodia(){
				var data_table = new google.visualization.DataTable();

				data_table.addColumn('number', 'Seconds');
				data_table.addColumn('number', '% Retention');

				var video_length;

				FB.api(
					'/'+post_id_query_repsodia+'/insights/post_video_length',
					'GET',
					{"fields":"values"},
					function(response) {
					  video_length = response.data[0].values[0].value/1000;
					}
				);
			  
				var retention;
				var formating;
				var table = new google.visualization.LineChart(document.getElementById('chart_div_query_repsodia'));
			  
				formating = {
					title: 'Retention Graph',
					curveType: 'function',
					hAxis: {
						gridlines:{
							count: -1
						},
						title: 'Seconds'
					},
					vAxis: {
						format: '%',
						gridlines:{
							count: -1
						},
						title: '% Retention'
					},
					legend:{
						position: 'none'
					},
					pointSize: 3,
				};
  
				FB.api(
				  '/'+post_id_query_repsodia+'/insights/post_video_retention_graph/lifetime',
				  'GET',
				  {'fields':'values'},
				  function(response) {
					retention = response.data[0].values[0].value;
					for(var index in retention){
						data_table.addRow([video_length/40*(parseInt(index)+1),
						retention[index]]);
					}
					table.draw(data_table,formating);
				  }
				);
			}    
			function drawDeltaChartQuery_repsodia(measure){
			  metric_query_repsodia=measure;
			  var data_table = new google.visualization.DataTable();
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 1:
					data_table.addColumn('number', '\u0394 Engagement');
					break;
				case 2:
					data_table.addColumn('number', '\u0394 VTR Organico');
					break;
				case 4:
					data_table.addColumn('number', '\u0394 Viral AMP');
					break;
				case 5:
					data_table.addColumn('number', '\u0394 Reach');
					break;
				case 6:
					data_table.addColumn('number', '\u0394 Clicked to play');
					break;
				case 7:
					data_table.addColumn('number', '\u0394 Likes');
					break;
				case 8:
					data_table.addColumn('number', '\u0394 Shares');
					break;
				case 9:
					data_table.addColumn('number', '\u0394 Video Views');
					break;
				case 10:
					data_table.addColumn('number', '\u0394 Comments');
					break;
				default:
					data_table.addColumn('number', '\u0394 Video Views');
					break;
			  }
  
			  var formating;
			  for(var n=1;n<data_query_repsodia.length;n++){
				data_query_repsodia[n].record_time=data_query_repsodia[n].record_time.replace(' ','T');
				data_query_repsodia[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
				var data_date=new Date(data_query_repsodia[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query_repsodia[n].engagement)-parseFloat(data_query_repsodia[n-1].engagement)]);
					  formating = {
						title: '\u0394 Engagement',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_repsodia[n].organic_ctr)/100-parseFloat(data_query_repsodia[n-1].organic_ctr)/100, f: parseFloat(data_query_repsodia[n].organic_ctr)-parseFloat(data_query_repsodia[n-1].organic_ctr)+'%'}]);
					  formating = {
						title: '\u0394 VTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,
					  parseFloat(data_query_repsodia[n].viral_amp)-parseFloat(data_query_repsodia[n-1].viral_amp)]);
					  formating = {
						title: '\u0394 Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_repsodia[n].reach)-parseFloat(data_query_repsodia[n-1].reach), f: (parseFloat(data_query_repsodia[n].reach)-parseFloat(data_query_repsodia[n-1].reach)).toFixed(0)}]);
					  formating = {
						title: '\u0394 Reach',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,
					  parseFloat(data_query_repsodia[n].clicked_to_play)-parseFloat(data_query_repsodia[n-1].clicked_to_play)]);
					  formating = {
						title: '\u0394 Clicked to play',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,
					  parseFloat(data_query_repsodia[n].likes)-parseFloat(data_query_repsodia[n-1].likes)]);
					  formating = {
						title: '\u0394 Likes',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_repsodia[n].shares)-parseFloat(data_query_repsodia[n-1].shares), f: (parseFloat(data_query_repsodia[n].shares)-parseFloat(data_query_repsodia[n-1].shares)).toFixed(0)}]);
					  formating = {
						title: '\u0394 Shares',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,
					  parseFloat(data_query_repsodia[n].link_clicks)-parseFloat(data_query_repsodia[n-1].link_clicks)]);
					  delta_link_clicks_repsodia[n]=parseFloat(data_query_repsodia[n].link_clicks)-parseFloat(data_query_repsodia[n-1].link_clicks);
					  formating = {
						title: '\u0394 Video Views',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].comments)-parseFloat(data_query_repsodia[n-1].comments)]);
					  formating = {
						title: '\u0394 Comments',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query_repsodia[n].link_clicks)-parseFloat(data_query_repsodia[n-1].link_clicks)]);
					  formating = {
						title: '\u0394 Video Views',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query_repsodia'));
			  table.draw(data_table,formating);
			}
			//repsodia query
                
            //techcult query	
			var data_query_techcult=[];
			var metric_query_techcult=0;
			var formating_query_techcult;
			var post_id_query_techcult;
			var delta_link_clicks_techcult=[];
			var post_techcult;

			jq1102(function() {
				var select_posts=[];
				$.ajax({
					url: "php_scripts/get_all_post_names_techcult.php",
					dataType: 'json',
					method: 'get',
					async: false,
					success: function(response){
						select_posts=response;
						//console.log(select_posts);
					}
				});
 
				jq1102( "#select_posts_techcult" ).autocomplete({
				  minLength: 0,
				  source: select_posts,
				  focus: function( event, ui ) {
					jq1102( "#select_posts_techcult" ).val( ui.item.label );
					return false;
				  },
				  select: function( event, ui ) {
					jq1102( "#select_posts_techcult" ).val( ui.item.label );
					//$( "#select_value" ).val( ui.item.value );
					post_id_query_techcult = ui.item.value;
					//console.log(post_id_query);
					var stats = document.getElementsByClassName("stats");
					for(var m=0;m<stats.length;m++)
					{
						stats.item(m).innerHTML = '';
					}
					//checkLoginState(post_id_query);
					getReportStats_techcult(post_id_query_techcult);
					selectPostQuery_techcult();
					return false;
				  }
				});
			});
			function UpdateDataQuery_techcult(){
				var time_lapse_techcult = document.getElementById("time_lapse_query_techcult").value;
				//var post_id=document.getElementById("combo_box_promoted").value;
				$.ajax({
					url: "php_scripts/get_post_data_techcult.php",
					dataType: "json",
					data: {post_id: post_id_query_techcult, time_lapse: time_lapse_techcult},
					method: 'get',
					async: false,
					success: function(response){
						data_query_techcult=response;
					}
				});	
			}
			function selectPostQuery_techcult(){
				//console.log(post_id_query);
				UpdateDataQuery_techcult();
				drawChartQuery_techcult(metric_query_techcult);
			}
			function drawChartQuery_techcult(measure){
			  metric_query_techcult=measure;
			  var data_table = new google.visualization.DataTable();
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 2:
					data_table.addColumn('number', 'VTR Organico');
					break;
				case 4:
					data_table.addColumn('number', 'Viral AMP');
					break;
				case 9:
					data_table.addColumn('number', 'Video Views');
					break;
				case 8:
					data_table.addColumn('number', 'Shares');
					break;
				case 7:
					data_table.addColumn('number', 'Likes');
					break;
				case 10:
					data_table.addColumn('number', 'Comments');
					break;
				case 5:
					data_table.addColumn('number', 'reach');
					break;
				case 6:
					data_table.addColumn('number', 'Clicked to play');
					break;
				case 1:
					data_table.addColumn('number', 'Engagement');
					break;
				default:
					data_table.addColumn('number', 'Vide Views');
					break;
			  }
  
			  var formating;
			  for(var n=0;n<data_query_techcult.length;n++){
				data_query_techcult[n].record_time=data_query_techcult[n].record_time.replace(' ','T');
				data_query_techcult[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
				var data_date=new Date(data_query_techcult[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query_techcult[n].engagement)]);
					  formating = {
						title: 'Engagement',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_techcult[n].organic_ctr)/100, f: data_query_techcult[n].organic_ctr+'%'}]);
					  formating = {
						title: 'VTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].viral_amp)]);
					  formating = {
						title: 'Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend: {
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,{v: parseFloat(data_query_techcult[n].reach)/100, f: data_query_techcult[n].reach}]);
					  formating = {
						title: 'Reach',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].clicked_to_play)]);
					  formating = {
						title: 'Clicked to play',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].likes)]);
					  formating = {
						title: 'Likes',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,{v: parseFloat(data_query_techcult[n].shares), f: data_query_techcult[n].shares}]);
					  formating = {
						title: 'Shares',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].link_clicks)]);
					  formating = {
						title: 'Video Views',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].comments)]);
					  formating = {
						title: 'Comments',
						//curveType: 'function',
						vAxis:{
							format: 'number'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].link_clicks)]);
					  formating = {
						title: 'Video Views',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query_techcult'));
			  table.draw(data_table,formating);
			}
			function drawChartQueryRetention_techcult(){
				var data_table = new google.visualization.DataTable();

				data_table.addColumn('number', 'Seconds');
				data_table.addColumn('number', '% Retention');

				var video_length;

				FB.api(
					'/'+post_id_query_techcult+'/insights/post_video_length',
					'GET',
					{"fields":"values"},
					function(response) {
					  video_length = response.data[0].values[0].value/1000;
					}
				);
			  
				var retention;
				var formating;
				var table = new google.visualization.LineChart(document.getElementById('chart_div_query_techcult'));
			  
				formating = {
					title: 'Retention Graph',
					curveType: 'function',
					hAxis: {
						gridlines:{
							count: -1
						},
						title: 'Seconds'
					},
					vAxis: {
						format: '%',
						gridlines:{
							count: -1
						},
						title: '% Retention'
					},
					legend:{
						position: 'none'
					},
					pointSize: 3,
				};
  
				FB.api(
				  '/'+post_id_query_techcult+'/insights/post_video_retention_graph/lifetime',
				  'GET',
				  {'fields':'values'},
				  function(response) {
					retention = response.data[0].values[0].value;
					for(var index in retention){
						data_table.addRow([video_length/40*(parseInt(index)+1),
						retention[index]]);
					}
					table.draw(data_table,formating);
				  }
				);
			}    
			function drawDeltaChartQuery_techcult(measure){
			  metric_query_techcult=measure;
			  var data_table = new google.visualization.DataTable();
	
			  data_table.addColumn('datetime', 'Hora');
			  switch(measure){
				case 1:
					data_table.addColumn('number', '\u0394 Engagement');
					break;
				case 2:
					data_table.addColumn('number', '\u0394 VTR Organico');
					break;
				case 4:
					data_table.addColumn('number', '\u0394 Viral AMP');
					break;
				case 5:
					data_table.addColumn('number', '\u0394 Reach');
					break;
				case 6:
					data_table.addColumn('number', '\u0394 Clicked to play');
					break;
				case 7:
					data_table.addColumn('number', '\u0394 Likes');
					break;
				case 8:
					data_table.addColumn('number', '\u0394 Shares');
					break;
				case 9:
					data_table.addColumn('number', '\u0394 Video Views');
					break;
				case 10:
					data_table.addColumn('number', '\u0394 Comments');
					break;
				default:
					data_table.addColumn('number', '\u0394 Video Views');
					break;
			  }
  
			  var formating;
			  for(var n=1;n<data_query_techcult.length;n++){
				data_query_techcult[n].record_time=data_query_techcult[n].record_time.replace(' ','T');
				data_query_techcult[n].record_time+='-0'+(new Date().getTimezoneOffset()/60).toString()+':00';
				var data_date=new Date(data_query_techcult[n].record_time);
	
				switch(measure)
				{
				  case 1:
					  data_table.addRow([data_date,
					  parseFloat(data_query_techcult[n].engagement)-parseFloat(data_query_techcult[n-1].engagement)]);
					  formating = {
						title: '\u0394 Engagement',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 2:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_techcult[n].organic_ctr)/100-parseFloat(data_query_techcult[n-1].organic_ctr)/100, f: parseFloat(data_query_techcult[n].organic_ctr)-parseFloat(data_query_techcult[n-1].organic_ctr)+'%'}]);
					  formating = {
						title: '\u0394 VTR Orgánico',
						//curveType: 'function',
						vAxis: {
							format: '##.##%'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 4:
					  data_table.addRow([data_date,
					  parseFloat(data_query_techcult[n].viral_amp)-parseFloat(data_query_techcult[n-1].viral_amp)]);
					  formating = {
						title: '\u0394 Viral AMP',
						//curveType: 'function',
						vAxis: {
							format: '#.##x'
						},
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 5:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_techcult[n].reach)-parseFloat(data_query_techcult[n-1].reach), f: (parseFloat(data_query_techcult[n].reach)-parseFloat(data_query_techcult[n-1].reach)).toFixed(0)}]);
					  formating = {
						title: '\u0394 Reach',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 6:
					  data_table.addRow([data_date,
					  parseFloat(data_query_techcult[n].clicked_to_play)-parseFloat(data_query_techcult[n-1].clicked_to_play)]);
					  formating = {
						title: '\u0394 Clicked to play',
						//curveType: 'function',
						
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 7:
					  data_table.addRow([data_date,
					  parseFloat(data_query_techcult[n].likes)-parseFloat(data_query_techcult[n-1].likes)]);
					  formating = {
						title: '\u0394 Likes',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 8:
					  data_table.addRow([data_date,
					  {v: parseFloat(data_query_techcult[n].shares)-parseFloat(data_query_techcult[n-1].shares), f: (parseFloat(data_query_techcult[n].shares)-parseFloat(data_query_techcult[n-1].shares)).toFixed(0)}]);
					  formating = {
						title: '\u0394 Shares',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 9:
					  data_table.addRow([data_date,
					  parseFloat(data_query_techcult[n].link_clicks)-parseFloat(data_query_techcult[n-1].link_clicks)]);
					  delta_link_clicks_techcult[n]=parseFloat(data_query_techcult[n].link_clicks)-parseFloat(data_query_techcult[n-1].link_clicks);
					  formating = {
						title: '\u0394 Video Views',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  case 10:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].comments)-parseFloat(data_query_techcult[n-1].comments)]);
					  formating = {
						title: '\u0394 Comments',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				  default:
					  data_table.addRow([data_date,parseFloat(data_query_techcult[n].link_clicks)-parseFloat(data_query_techcult[n-1].link_clicks)]);
					  formating = {
						title: '\u0394 Video Views',
						//curveType: 'function',
						hAxis: {
							format: 'M/d HH:mm',
							gridlines:{
								count: -1
							},
							title: 'Hora'
						},
						legend:{
							position: 'none'
						},
						pointSize: 3,
					  };
					  break;
				}
			  }

			  // Instantiate and draw our chart, passing in some options.
			  var table = new google.visualization.LineChart(document.getElementById('chart_div_query_techcult'));
			  table.draw(data_table,formating);
			}
			//techcult query
			
			//Report repsodia
			function getReportStats_repsodia(post_id) {
				post_repsodia={};
   
				query.edge = post_id;//query latest posts
				query.fields = "link";
				$.ajax({
					url: buildURL(query,access_token,1),
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response){
						post_repsodia.link=response.link;
					}
				});
				query.edge=post_id+"/insights";
				query.fields="values,name,period&metric=['post_impressions_unique','post_stories_by_action_type','post_video_views']";
				$.ajax({
					url: buildURL(query,access_token,1),
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response) {
					  var metrics=response.data;
					  for(var p=0;p<metrics.length;p++){
						switch(metrics[p].name){
						  case "post_impressions_unique":
							post_repsodia.unique_imp=metrics[p].values[0].value;
							break;
						  case "post_stories_by_action_type":
							post_repsodia.likes=metrics[p].values[0].value.like;
							post_repsodia.comments=metrics[p].values[0].value.comment;
							post_repsodia.shares=metrics[p].values[0].value.share;
							break;
						  case "post_video_views":
						  	if(metrics[p].period=='lifetime'){
						  		post_repsodia.video_views=metrics[p].values[0].value;
						  	}
						  	break;
						  default:
							break;
						}
					  }
					}
				});//ajax query to find all metrics with a switch
				document.getElementById("fb_shares_repsodia").innerHTML = numberWithCommas(typeof post_repsodia.shares === 'undefined' ? 0 : post_repsodia.shares);
				document.getElementById("fb_likes_repsodia").innerHTML = numberWithCommas(typeof post_repsodia.likes === 'undefined' ? 0 : post_repsodia.likes);
				document.getElementById("fb_comments_repsodia").innerHTML = numberWithCommas(typeof post_repsodia.comments === 'undefined' ? 0 : post_repsodia.comments);
				document.getElementById("fb_reach_repsodia").innerHTML = numberWithCommas(typeof post_repsodia.unique_imp === 'undefined' ? 0 : post_repsodia.unique_imp);
				document.getElementById("fb_views_repsodia").innerHTML = numberWithCommas(typeof post_repsodia.video_views === 'undefined' ? 0 : post_repsodia.video_views);
				document.getElementById("post_id_repsodia").innerHTML = '';
				var aTag=document.createElement('a');
				aTag.setAttribute('href',"http://www.facebook.com/"+post_id);
				aTag.setAttribute('target','_blank');
				aTag.innerHTML = post_id;
				document.getElementById("post_id_repsodia").appendChild(aTag);
			}
			//report repsodia
                
            //Report techcult
			function getReportStats_techcult(post_id) {
				post_techcult={};
   
				query.edge = post_id;//query latest posts
				query.fields = "link";
				$.ajax({
					url: buildURL(query,access_token,1),
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response){
						post_techcult.link=response.link;
					}
				});
				query.edge=post_id+"/insights";
				query.fields="values,name,period&metric=['post_impressions_unique','post_stories_by_action_type','post_video_views']";
				$.ajax({
					url: buildURL(query,access_token,1),
					type: 'get',
					dataType: 'json',
					async: false,
					success: function(response) {
					  var metrics=response.data;
					  for(var p=0;p<metrics.length;p++){
						switch(metrics[p].name){
						  case "post_impressions_unique":
							post_techcult.unique_imp=metrics[p].values[0].value;
							break;
						  case "post_stories_by_action_type":
							post_techcult.likes=metrics[p].values[0].value.like;
							post_techcult.comments=metrics[p].values[0].value.comment;
							post_techcult.shares=metrics[p].values[0].value.share;
							break;
						  case "post_video_views":
						  	if(metrics[p].period=='lifetime'){
						  		post_techcult.video_views=metrics[p].values[0].value;
						  	}
						  	break;
						  default:
							break;
						}
					  }
					}
				});//ajax query to find all metrics with a switch
				document.getElementById("fb_shares_techcult").innerHTML = numberWithCommas(typeof post_techcult.shares === 'undefined' ? 0 : post_techcult.shares);
				document.getElementById("fb_likes_techcult").innerHTML = numberWithCommas(typeof post_techcult.likes === 'undefined' ? 0 : post_techcult.likes);
				document.getElementById("fb_comments_techcult").innerHTML = numberWithCommas(typeof post_techcult.comments === 'undefined' ? 0 : post_techcult.comments);
				document.getElementById("fb_reach_techcult").innerHTML = numberWithCommas(typeof post_techcult.unique_imp === 'undefined' ? 0 : post_techcult.unique_imp);
				document.getElementById("fb_views_techcult").innerHTML = numberWithCommas(typeof post_techcult.video_views === 'undefined' ? 0 : post_techcult.video_views);
				document.getElementById("post_id_techcult").innerHTML = '';
				var aTag=document.createElement('a');
				aTag.setAttribute('href',"http://www.facebook.com/"+post_id);
				aTag.setAttribute('target','_blank');
				aTag.innerHTML = post_id;
				document.getElementById("post_id_techcult").appendChild(aTag);
			}
			//report techcult
			
			//promote timepicker datepicker
			$('#promote-timepicker').timepicker({
                defaultTime : new Date(),
                minuteStep : 5
            });
            $('#promote-datepicker').datepicker({
                autoclose: true,
                todayHighlight: true,
                startDate: new Date(),
                format: 'yyyy/mm/dd'
            });
            //promote timepicker datepicker
			
			//interval calls
			var initialCalls = setTimeout(function(){
				UpdatePostsDashboard();
				//loadKomfo();
				UpdatePostsPromoted();
			},3000);
			
			var refreshData = setInterval(function(){
				selectPostDashboard();
				$('.nav-pills>li.active>a')[0].click();
				selectPostPromoted();
				getUnpublishedPosts();
			},1000*60*10);
			
			
			var authGoogle = setInterval(function(){
				handleClientLoad();
				checkLoginState();
			},1000*60*30);
			getActivePage();
        </script>

    </body>
</html>
