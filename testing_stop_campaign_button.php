<!DOCTYPE html>
<html>
<head>
    <title>Deforma Facebook Query</title>
    <meta charset="UTF-8">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="responsive_data_center/js/math.min.js"></script>

    <link rel="shortcut icon" href="http://deforma2.com/responsive_data_center/assets/images/users/logodeforma_favicon.png">
    
</head>
<body>
    <script>        
        var campaigns=[
            {
                'campaign': '6095818346076',
                'name': 'testing campaign promote interno' 
            },
            {
                'campaign': '6090185178927',
                'name': 'doouble xx 2' 
            }
        ]
        
         $.post(
            "https://hooks.slack.com/services/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",
            JSON.stringify({                
                "channel": '@dan',
                'text': 'La campaña '+campaigns[0].name+' excede el cxw de 0.005',
                "attachments": [{
                    "text": '¿Que quieres hacer?',
                    "fallback": 'No pude imprimir botones de acción!',
                    "callback_id": "stop campaign test",
                    "color": "#FF0000",
                    "attachment_type": "default",
                    "actions": [
                        {
                            "name": "stop",
                            "text": "Detener",
                            "style": "danger",
                            "type": "button",
                            "value": campaigns[0].campaign
                        },
                        {
                            "name": "nothing",
                            "text": "Nada",
                            "type": "button",
                            "value": null
                        }
                    ]
                }]
            })
        );
    </script>
</body>
</html>