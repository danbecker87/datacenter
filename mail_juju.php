<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Juju mail service">
        <meta name="Juju" content="Juju mail">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <link rel="shortcut icon" href="">

        <title>Juju Mail</title>

    </head>


    <body>
    
        <div id='status'></div>
        
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

         <!--Google Flow--><script type="text/javascript">
        google.load('visualization', '1', {'packages':['line','table', 'corechart','controls']});
        var clientId = '351937849601-uvsfmgjv93sqhtp2o7u57c33i61e3gha.apps.googleusercontent.com';
        var apiKey = 'AIzaSyAUJ66hcBfIV7SxjbjaJCl1Uubngc247jc';

        function makeApiCall() {
//            if(new Date().getHours()==12 && new Date().getMinutes() < 30){
            if(1){
                var RFCmessage = '';
//                RFCmessage = 'to: Deadbeats <deadbeats@dicks.com>\r\n';
                RFCmessage = 'to: Dan Becker <danbecker87@gmail.com>\r\n';
                RFCmessage += 'from: me\r\n';
                RFCmessage += 'Subject: Gimme my money\r\n\r\n';
                RFCmessage += 'Buenas tardes,\r\n\r\n Esta recibiendo este correo porque me deben lana\r\n\r\n';
                RFCmessage += 'No se molesten en contestar si no van a pagar';
                var email = gapi.client.gmail.users.messages.send({
                    'userId': 'me',
                    'resource': {
                        'raw': window.btoa(RFCmessage).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '')
                    }
                });
                email.execute(function(resp){
                    if (resp.error && resp.error.status) {
                        // The API encountered a problem before the script started executing.
                        //alert('Mail failed to send');
                        console.log('Error calling API: ' + JSON.stringify(resp, null, 2));
                        console.log('Failed to sent mail');
                    } else if (resp.error) {
                        // The API executed, but the script returned an error.
                        //alert('Mail failed to send');
                        console.log('Script error! Message: ' + error.errorMessage);
                        console.log('Failed to sent mail');
                    } else {
                        console.log('Succesfully sent mail on'+new Date().toLocaleString());
                        $('#status').innerHTML = 'Succesfully sent email on '+new Date().toLocaleString();
                    }
                });
            }else{
                console.log('not yet');
            }
        }

        function initClient() {
            //console.log('in init client');    
            var API_KEY = apiKey;  

            var CLIENT_ID = clientId;  

            var SCOPE = 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/gmail.compose';

            gapi.client.init({
                'apiKey': API_KEY,
                'clientId': CLIENT_ID,
                'scope': SCOPE,
                'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4', 'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'],
            }).then(function() {
                //console.log('In thenable client init');
                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
                updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            });
        }

        function handleClientLoad() {
          gapi.load('client:auth2', initClient);
        }

        function updateSignInStatus(isSignedIn) {
          if (isSignedIn) {
            //console.log('making api call');
            makeApiCall();
          }else{
            //console.log('Not signed in');
            handleSignInClick();
          }
        }

        function handleSignInClick(event) {
          gapi.auth2.getAuthInstance().signIn();
        }

        function handleSignOutClick(event) {
          gapi.auth2.getAuthInstance().signOut();
        }
        </script><!--Google Auth Flow-->

        <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>

        <script>
            var authGoogle = setInterval(function(){
                handleClientLoad();
            },1000*60*30);
        </script>

    </body>
</html>